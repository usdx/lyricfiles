#TITLE:A Forest
#ARTIST:The Cure
#MP3:The Cure - A Forest.mp3
#VIDEO:The Cure - A Forest.avi
#COVER:The Cure - A Forest[CO].jpg
#BPM:244
#GAP:48300
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Post-Punk
#YEAR:1980
: 0 1 9 Come
: 2 1 16  clo
: 4 2 16 ser
: 7 3 16  and
: 13 13 16  see
- 28
: 49 2 14 See
: 52 1 12  in
: 54 2 14 to
: 57 2 16  the
: 61 12 12  trees
- 75
: 97 4 12 Find
: 103 2 12  the
: 106 12 11  girl
- 120
: 137 2 9 While
: 140 4 9  you
: 146 10 9  can
- 158
: 192 1 9 Come
: 194 1 16  clo
: 196 2 16 ser
: 199 3 16  and
: 205 14 16  see
- 221
: 241 2 14 See
* 244 2 12  in
* 247 2 14 to
* 251 1 16  the
: 254 10 12  dark
- 266
: 288 1 12 Just
: 290 2 12  fol
: 293 2 12 low
: 296 4 12  your
: 303 11 11  eyes
- 316
: 337 1 12 Just
: 339 2 12  fol
: 342 1 12 low
: 344 7 12  your
: 354 14 11  eyes
- 370
: 964 1 9 I
: 966 3 16  hear
: 970 5 16  her
: 979 11 16  voice
- 992
: 1014 2 14 Cal
: 1017 2 14 ling
: 1020 4 14  my
: 1026 10 16  name
- 1038
: 1061 5 12 Sound
: 1069 2 12  is
: 1074 9 11  deep
- 1085
: 1101 2 9 In
: 1104 2 9  the
: 1107 11 9  dark
- 1120
: 1156 1 9 I
: 1158 4 16  hear
: 1163 4 16  her
: 1171 12 16  voice
- 1185
: 1206 4 14 Start
* 1212 2 16  to
* 1216 9 12  run
- 1227
: 1255 2 12 In
: 1258 2 12 to
: 1261 2 12  the
: 1264 14 11  trees
- 1280
: 1303 2 12 In
: 1306 2 12 to
: 1309 5 12  the
: 1319 25 11  trees
- 1346
: 1532 2 16 In
: 1535 2 16 to
: 1538 2 16  the
: 1541 21 16  trees
- 1564
: 1927 1 16 Sud
: 1929 2 16 den
: 1932 2 16 ly
: 1935 1 16  I
: 1937 9 16  stop
- 1948
: 1970 2 14 But
: 1973 2 14  know
: 1976 2 12  it's
: 1979 3 14  too
* 1984 2 16  late
* 1987 6 12 ~
- 1995
: 2019 1 9 I'm
: 2021 4 12  lost
: 2027 2 12  in
: 2030 2 12  a
* 2033 2 12  fo
* 2036 6 11 rest
- 2044
: 2060 3 9 All
: 2064 2 9  a
: 2067 7 9 lone
- 2076
: 2114 2 16 The
: 2117 2 16  girl
: 2120 2 16  was
: 2123 1 16  ne
: 2125 2 16 ver
: 2128 11 16  there
- 2141
: 2162 2 12 It's
: 2165 2 14  al
: 2168 2 14 ways
: 2171 1 14  the
* 2173 11 16  same
- 2186
: 2209 2 9 I'm
: 2212 2 16  run
: 2215 2 16 ning
: 2218 2 16  to
: 2221 2 16 wards
: 2224 2 16  no
: 2228 9 16 thing
- 2239
: 2258 1 14 A
: 2260 6 16 gain
: 2267 2 16  and
: 2270 1 14  a
: 2272 6 16 gain
: 2279 2 16  and
: 2282 1 14  a
: 2284 6 16 gain
: 2291 2 16  and
: 2294 1 14  a
: 2296 3 16 gain
- 2301
: 2303 2 16 and
: 2306 1 14  a
: 2308 6 16 gain
: 2315 2 16  and
: 2318 1 14  a
: 2320 6 16 gain
: 2327 2 16  and
: 2330 1 14  a
: 2332 6 16 gain
: 2339 2 16  and
: 2342 1 14  a
: 2344 3 16 gain
- 2349
: 2351 2 16 and
: 2354 1 14  a
: 2356 6 16 gain
: 2363 2 16  and
: 2366 1 14  a
: 2368 6 16 gain
: 2375 2 16  and
: 2378 1 14  a
: 2380 6 16 gain
: 2387 2 16  and
: 2390 1 14  a
: 2392 3 16 gain
- 2397
: 2399 2 16 and
: 2402 1 14  a
: 2404 6 16 gain
: 2411 2 16  and
: 2414 1 14  a
: 2416 6 16 gain
: 2423 2 16  and
: 2426 1 14  a
: 2428 6 16 gain
: 2435 2 16  and
: 2438 1 14  a
: 2440 3 16 gain
- 2445
: 2447 2 16 and
: 2450 1 14  a
: 2452 6 16 gain
: 2459 2 16  and
: 2462 1 14  a
: 2464 6 16 gain
: 2471 2 16  and
: 2474 1 14  a
: 2476 6 16 gain
: 2483 2 16  and
: 2486 1 14  a
: 2488 3 16 gain
- 2493
* 2495 2 16 and
* 2498 1 14  a
* 2500 12 16 gain
E