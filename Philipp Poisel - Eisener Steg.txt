#TITLE:Eiserner Steg (Klavier Version)
#ARTIST:Philipp Poisel
#MP3:Philipp Poisel - Eiserner Steg.mp3
#VIDEO:Philipp Poisel - Eiserner Steg.mp4
#COVER:Philipp Poisel - Eiserner Steg [CO].jpg
#BPM:243,4
#GAP:324
#MedleyStartBeat:788
#MedleyEndBeat:1336
#ENCODING:UTF8
#PREVIEWSTART:48,886
#LANGUAGE:German
#GENRE:Pop
#EDITION:None
#YEAR:2011
#CREATOR:bohning and Fepo
: 0 2 3 Ich
: 4 10 8  at
: 16 2 7 me
: 19 2 8  dich
: 24 4 6  ein
- 30
: 66 4 6 Und
: 73 5 8  nie
: 83 2 6  wie
: 86 2 8 der
: 90 5 6  aus
- 97
: 132 4 10 Schließ'
: 139 3 10  dich
: 144 4 13  in
: 150 2 10  mein
: 155 10 13  Herz
- 167
: 199 4 10 Lass'
: 205 3 13  dich
: 211 4 10  nicht
: 217 3 10  mehr
: 222 5 10  raus
- 229
: 267 2 6 Ich
: 271 4 15  tra
: 277 4 13 ge
: 283 4 10  dich
: 289 4 10  bei
: 295 5 6  mir
- 302
: 337 4 15 In
: 343 4 13  mei
: 349 4 10 ner
: 356 7 10  Brust
- 365
: 390 3 6 Hätt'
: 395 2 6  al
: 398 2 6 le
: 404 3 15  We
: 409 2 13 ge
: 414 4 10  ver
: 420 4 10 än
: 427 4 10 dert
- 433
: 456 2 6 Hätt'
: 460 2 6  ich
: 464 2 6  sie
: 468 6 8  vor
: 476 3 6 her
: 482 2 6  ge
: 486 6 6 wusst
- 494
: 531 3 -6 Jetzt
: 536 5 3  steh'
: 544 3 1  ich
: 551 3 -2  am
: 556 3 -2  U
: 561 4 -6 fer
- 567
: 600 2 6 Die
: 604 6 15  Flut
: 612 3 13  un
: 617 3 10 ter
: 623 6 10  mir
- 631
: 667 3 6 Das
: 673 3 15  Was
: 678 3 13 ser
: 683 3 10  zum
: 689 6 10  Hal
: 697 3 6 se
- 702
: 731 2 6 Wa
: 734 2 6 rum
: 738 2 8  bist
: 742 2 6  du
: 747 4 6  nicht
: 755 6 6  hier?
- 763
: 788 2 6 Ich
: 793 2 6  will
: 797 2 6  dich
: 801 4 15  ein
: 808 4 13 mal
: 814 4 10  noch
: 821 3 10  lie
: 826 3 6 ben
- 831
: 860 2 6 Wie
: 863 2 6  beim
: 866 2 13  al
: 869 2 13 ler
: 873 5 13 ers
: 880 3 10 ten
: 885 7 10  Mal
- 894
: 926 2 6 Will
: 929 2 6  dich
: 933 3 15  ein
: 937 4 13 mal
: 943 2 10  noch
: 947 6 10  küs
: 955 4 8 sen
- 961
: 983 2 10 In
: 987 2 10  dei
: 991 2 10 nen
: 996 2 10  of
: 1000 5 8 fe
: 1007 4 6 nen
: 1015 8 6  Haar'n
- 1025
: 1053 2 6 Ich
: 1056 2 6  will
: 1059 4 15  ein
: 1065 3 13 mal
: 1070 2 10  noch
: 1074 4 10  schla
: 1081 5 8 fen
- 1088
: 1121 5 15 Schla
: 1128 5 13 fen
: 1135 5 10  bei
: 1142 5 10  dir
- 1149
: 1181 4 6 Dir
: 1187 3 15  ein
: 1192 3 13 mal
: 1197 3 10  noch
: 1202 3 10  nah
: 1208 3 8  sein
: 1213 2 6  be
: 1217 5 10 vor
: 1224 2 8  ich
: 1228 5 10  dich
- 1235
: 1242 3 10 Für
: 1248 2 10  im
: 1252 2 8 mer
: 1257 5 6  ver
: 1266 4 6 lier'
- 1272
: 1307 4 6 Für
: 1313 2 10  im
: 1318 2 8 mer
: 1324 3 6  ver
: 1331 5 6 lier'
- 1338
: 1624 2 6 Wer
: 1628 3 15  ach
: 1634 3 13 tet
: 1640 3 10  auf
: 1646 3 10  mich
: 1652 7 6  jetzt
- 1661
: 1688 2 6 Dass
: 1691 2 6  ich
: 1694 4 15  mich
: 1700 4 13  nicht
: 1707 3 10  ver
: 1712 8 10 lauf'
- 1722
: 1754 2 6 Und
: 1758 4 15  wenn
: 1764 4 13  ich
: 1770 4 10  jetzt
: 1776 6 10  fall'
- 1784
: 1803 2 10 Wer
: 1808 5 8  fängt
: 1816 8 8  mich
: 1836 3 6  dann
: 1841 4 6  auf?
- 1847
: 1885 2 6 In
: 1889 4 15  all
: 1895 4 13  die
: 1901 2 10 sen
: 1905 5 10  Stra
: 1913 4 8 ßen
- 1919
: 1949 2 6 Kenn'
: 1953 2 6  ich
: 1956 4 15  mich
: 1962 4 13  nicht
: 1968 2 10  mehr
: 1972 6 10  aus
- 1980
: 2015 2 6 Da
: 2018 2 6  ist
: 2021 3 15  nie
: 2026 3 13 mand
: 2031 2 13  mehr
: 2034 2 10  der
: 2038 4 10  war
: 2044 4 8 tet
- 2050
: 2057 2 10 Der
: 2061 2 10  auf
: 2065 4 10  mich
: 2071 4 8  war
: 2077 4 8 tet
- 2083
* 2096 4 8 Zu
* 2102 7 6 haus
- 2111
: 2137 2 6 Ich
: 2141 2 6  will
: 2145 2 6  dich
: 2148 4 15  ein
: 2156 3 13 mal
: 2161 3 10  noch
: 2168 3 10  lie
: 2172 3 6 ben
- 2177
: 2207 2 8 Wie
: 2210 2 6  beim
: 2213 2 13  al
: 2216 2 13 ler
: 2220 5 13 ers
: 2227 3 10 ten
: 2232 7 10  Mal
- 2241
: 2271 2 6 Will
: 2274 2 6  dich
: 2278 2 15  ein
: 2282 4 13 mal
: 2288 3 10  noch
: 2293 4 10  küs
: 2299 4 8 sen
- 2305
: 2327 2 10 In
: 2331 2 10  dei
: 2335 2 10 nen
: 2340 2 10  of
: 2344 5 8 fe
: 2351 4 6 nen
: 2359 8 6  Haar'n
- 2369
: 2396 2 6 Ich
: 2399 2 6  will
: 2402 4 15  ein
: 2408 3 13 mal
: 2413 2 10  noch
: 2417 4 10  schla
: 2424 5 8 fen
- 2431
: 2464 5 15 Schla
: 2471 5 13 fen
: 2478 5 10  bei
: 2485 5 10  dir
- 2492
: 2524 4 6 Dir
: 2530 3 15  ein
: 2535 3 13 mal
: 2540 3 10  noch
: 2545 3 10  nah
: 2551 3 8  sein
: 2556 2 6  be
: 2560 5 10 vor
: 2567 2 8  ich
: 2571 5 10  dich
- 2578
: 2586 4 10 Für
: 2592 2 10  im
: 2596 2 8 mer
: 2602 5 6  ver
: 2611 5 6 lier'
- 2618
: 2649 4 10 Für
: 2655 2 10  im
: 2660 2 8 mer
: 2664 4 6  ver
: 2672 4 6 lier'
- 2678
: 2917 4 13 Für
: 2923 3 13  im
: 2928 3 10 mer
- 2932
: 2933 4 13 Für
: 2939 3 13  im
: 2944 3 10 mer
- 2948
: 2949 4 13 Für
: 2955 3 13  im
: 2960 3 10 mer
- 2964
: 2964 4 13 Für
: 2970 3 13  im
: 2975 3 10 mer
- 2979
: 2980 4 13 Für
: 2986 3 13  im
: 2991 3 10 mer
- 2996
: 3043 4 13 Für
: 3049 3 13  im
: 3054 3 10 mer
- 3058
: 3058 4 13 Für
: 3064 3 13  im
: 3069 3 10 mer
- 3073
: 3073 4 13 Für
: 3079 3 13  im
: 3084 3 10 mer
- 3088
: 3089 4 10 Für
: 3095 2 10  im
: 3099 2 8 mer
: 3104 4 8  ver
: 3111 6 6 lier
- 3119
: 3149 2 6 Ich
: 3152 2 6  will
: 3155 4 15  ein
: 3161 3 13 mal
: 3166 2 10  noch
: 3170 4 10  schla
: 3177 5 8 fen
- 3184
: 3217 5 15 Schla
* 3224 5 13 fen
* 3231 5 10  bei
: 3238 5 10  dir
- 3245
: 3275 4 6 Dir
: 3281 3 15  ein
: 3286 3 13 mal
: 3291 3 10  noch
: 3296 3 10  nah
: 3302 3 8  sein
: 3307 2 6  be
: 3311 5 10 vor
: 3318 2 8  ich
: 3322 5 10  dich
- 3329
: 3337 4 10 Für
: 3343 2 10  im
: 3347 2 8 mer
: 3352 4 6  ver
: 3360 6 6 lier
- 3368
: 3399 4 6 Für
: 3405 2 10  im
: 3409 2 8 mer
: 3414 6 8  ver
: 3423 6 6 lier
E