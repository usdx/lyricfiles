#TITLE:Ich Fang Dir Den Mond
#ARTIST:Andreas Martin
#MP3:Andreas Martin - Ich fang dir den Mond.mp3
#VIDEO:Andreas Martin - Ich fang dir den Mond.mp4
#COVER:Andreas Martin - Ich fang dir den Mond.jpg
#BPM:252,1
#GAP:14659
#MedleyStartBeat:444
#MedleyEndBeat:983
#ENCODING:UTF8
#PREVIEWSTART:42,067
#LANGUAGE:German
#GENRE:Schlager
#EDITION:SingStar Apres-Ski Party 2
#YEAR:2008
: 0 7 52 Du
: 8 7 54  ich
: 16 3 56  schie
: 20 3 56 be
- 24
: 24 3 54 Die
: 28 6 56  Wol
: 35 3 56 ken
: 40 3 54  jetzt
: 44 4 56  weg
- 50
: 60 2 56 Ich
: 64 4 56  brauch
: 71 5 52  'ne
: 80 3 52  frei
: 84 3 54 e
: 92 4 54  Bahn
- 98
: 144 3 57 Nie
: 148 3 57 mand
: 152 3 56  und
: 156 5 57  nichts
: 164 3 57  hält
: 168 3 56  mich
: 172 6 52  auf
- 180
: 192 7 52 Denn
* 200 7 54  für
: 208 4 56  mich
: 220 2 56  gibt's
: 224 3 56  nur
: 228 3 54  ei
: 232 3 52 ne
: 236 6 54  Frau
- 244
: 255 8 52 Ja
: 264 7 54  Du
: 272 3 56  hast
: 276 3 56  auch
: 280 3 54  an
: 284 5 56  mich
: 296 3 54  geg
: 300 5 56 laubt
- 307
: 315 3 56 Als
: 320 6 59  wir
: 328 6 61 klich
: 336 4 61  nichts
: 344 2 59  mehr
: 348 6 59  ging
- 356
* 384 5 59 Da
: 392 6 59 rum
: 400 2 57  schenk
: 404 3 57  ich
- 408
: 408 3 57 Dir
: 412 6 57  hier
: 420 4 56  und
: 428 6 54  heut
- 436
: 444 5 54 Den
: 452 5 52  Haupt
: 460 3 51 ge
: 464 6 52 winn
- 472
: 504 3 52 Du
: 508 3 52  ich
: 512 3 52  fang
: 516 3 54  Dir
: 520 3 56  den
: 524 6 56  Mond
- 532
: 564 2 56 Mit
: 568 3 56  ei
: 572 3 56 nem
: 576 4 59  gold
: 584 3 52 'nen
: 589 5 52  Las
: 596 3 54 so
: 600 5 54  ein
- 607
: 636 3 59 Dann
: 640 3 59  wird
: 644 3 59  es
: 648 3 61  um
: 652 4 63  Dich
- 658
* 696 7 64 Nie
: 704 4 63  wie
: 708 3 61 ~
: 712 7 61 der
: 720 4 61  dun
: 724 3 59 ~
: 728 2 59 kel
: 732 6 59  sein
- 740
: 760 3 59 Dei
: 764 3 59 ne
: 768 3 59  Haa
: 772 3 59 re
: 776 3 59  leuch
: 780 3 59 ten
: 784 4 59  gren
: 792 2 61 zen
: 796 4 61 los
- 802
: 820 3 61 Und
: 824 3 61  dei
: 828 3 61 ne
: 832 2 66  Au
: 836 3 66 gen
: 840 3 64  tun
: 844 2 61  es
- 847
: 848 4 61 So
: 852 3 59 ~
: 856 3 59 wie
: 860 5 59 so
- 867
: 888 3 59 Du
: 892 2 59  ich
: 896 3 59  fang
: 900 3 59  Dir
: 904 2 52  den
: 908 6 52  Mond
: 916 2 54  heut
: 920 4 54  ein
- 926
: 952 3 54 Nur
: 956 3 56  für
: 960 3 57  Dich
: 964 3 56  ganz
: 968 3 54  al
: 972 4 54 lein
: 976 7 52 ~
- 985
: 1024 5 52 Hast
: 1032 7 54  mit
: 1040 3 56  mir
: 1044 3 56  nie
- 1048
: 1048 3 54 Den
: 1052 6 56  Mol
: 1060 3 56 li
: 1064 3 54  ge
: 1068 4 56 macht
- 1074
: 1084 2 56 Und
: 1088 4 56  da
: 1096 4 52 für
: 1104 3 52  dank
: 1112 2 54  ich
: 1116 4 54  Dir
- 1122
: 1152 3 54 Hast
* 1160 6 56  mir
: 1168 3 57  so
: 1172 3 57 viel
- 1176
: 1176 3 56 Im
: 1180 5 57  Le
: 1188 3 57 ben
: 1192 3 56  ge
: 1196 6 52 schenkt
- 1204
: 1216 6 52 Hältst
: 1224 7 54  mich
: 1232 9 56  warm
- 1242
: 1244 2 56 Auch
: 1248 3 56  wenn
: 1252 3 54  ich
: 1256 3 52  nicht
: 1260 6 54  frier
- 1268
: 1280 7 52 Denn
: 1288 7 54  Du
- 1296
: 1296 2 56 Hast
: 1300 3 56  auch
: 1304 3 54  an
: 1308 7 56  mich
: 1320 3 54  geg
: 1324 5 56 laubt
- 1331
: 1339 3 56 Als
: 1344 6 59  wir
* 1352 6 61 klich
: 1360 4 61  nichts
: 1368 2 59  mehr
: 1372 6 59  ging
- 1380
: 1408 5 59 Da
: 1416 6 59 für
: 1424 2 57  schenk
: 1428 3 57  ich
: 1432 3 57  Dir
: 1436 6 57  hier
- 1443
: 1444 4 56 Und
: 1452 6 54  heut
- 1460
: 1468 5 54 Den
: 1476 5 52  Haupt
: 1484 3 51 ge
: 1488 6 52 winn
- 1496
: 1528 3 52 Du
: 1532 3 52  ich
: 1536 3 52  fang
: 1540 3 52  Dir
: 1544 2 56  den
: 1548 6 56  Mond
- 1556
: 1588 2 56 Mit
: 1592 3 56  ei
: 1596 3 56 nem
: 1600 4 59  gold
: 1608 3 52 'nen
: 1613 5 52  Las
: 1620 3 54 so
: 1624 5 54  ein
- 1631
: 1660 3 59 Dann
: 1664 3 59  wird
: 1668 3 59  es
: 1672 3 61  um
: 1676 4 63  Dich
- 1682
: 1720 7 64 Nie
: 1728 4 63  wie
: 1732 3 61 ~
: 1736 7 61 der
: 1744 4 61  dun
: 1748 3 59 ~
: 1752 3 59 kel
: 1756 5 59  sein
- 1763
: 1784 3 59 Dei
: 1788 3 59 ne
: 1792 3 59  Haa
: 1796 3 59 re
: 1800 3 59  leuch
: 1804 3 59 ten
: 1808 4 59  gren
: 1816 3 61 zen
: 1820 4 61 los
- 1826
: 1844 3 61 Und
: 1848 3 61  dei
: 1852 3 61 ne
: 1856 3 66  Au
: 1860 3 66 gen
: 1864 3 64  tun
: 1868 3 61  es
- 1872
: 1872 4 61 So
: 1876 3 59 ~
: 1880 3 59 wie
: 1884 5 59 so
- 1891
: 1912 3 59 Du
: 1916 3 59  ich
: 1920 3 59  fang
: 1924 3 59  Dir
: 1928 2 52  den
: 1932 6 52  Mond
: 1940 2 54  heut
: 1944 4 54  ein
- 1950
: 1976 3 54 Nur
: 1980 3 56  für
: 1984 3 57  Dich
: 1988 3 56  ganz
: 1992 3 54  al
: 1996 4 54 lein
: 2000 7 52 ~
- 2009
: 2040 3 54 Du
: 2044 3 54  ich
: 2048 3 54  fang
: 2052 3 54  Dir
: 2056 2 58  den
: 2060 6 58  Mond
- 2068
: 2100 3 58 Mit
: 2104 3 58  ei
: 2108 3 58 nem
: 2112 4 61  gold
: 2120 3 54 'nen
: 2124 5 54  Las
: 2132 3 56 so
: 2136 5 56  ein
- 2143
: 2172 3 61 Dann
: 2176 3 61  wird
: 2180 3 61  es
: 2184 3 63  um
* 2188 4 65  Dich
- 2194
: 2232 7 66 Nie
: 2240 7 65  wie
* 2248 7 63 der
: 2256 4 63  dun
: 2260 3 61 ~
: 2264 3 61 kel
: 2268 6 61  sein
- 2276
: 2296 3 61 Dei
: 2300 3 61 ne
: 2304 3 61  Haa
: 2308 3 61 re
: 2312 3 61  leuch
: 2316 3 61 ten
: 2320 4 61  gren
: 2328 2 63 zen
: 2332 4 63 los
- 2338
: 2356 3 63 Und
: 2360 3 63  dei
: 2364 3 63 ne
: 2368 3 68  Au
: 2372 3 68 gen
: 2376 3 66  tun
: 2380 3 63  es
- 2384
: 2384 4 63 So
* 2388 3 61 ~
: 2392 3 61 wie
: 2396 5 61 so
- 2403
: 2424 3 61 Du
: 2428 3 61  ich
: 2432 3 61  fang
: 2436 3 61  Dir
: 2440 3 54  den
: 2444 6 54  Mond
: 2452 3 56  heut
: 2456 4 56  ein
- 2462
: 2488 3 56 Nur
: 2492 3 58  für
: 2496 3 59  Dich
: 2500 3 58  ganz
: 2504 3 56  al
: 2508 4 56 lein
* 2512 7 54 ~
- 2521
: 2552 3 61 Dei
: 2556 3 61 ne
: 2560 3 61  Haa
: 2564 3 61 re
: 2568 3 61  leuch
: 2572 3 61 ten
: 2576 4 61  gren
: 2584 3 63 zen
: 2588 5 63 los
- 2595
: 2612 3 63 Und
: 2616 3 63  dei
: 2620 3 63 ne
* 2624 3 68  Au
: 2628 3 68 gen
: 2632 3 66  tun
: 2636 3 63  es
: 2640 4 63  so
: 2644 3 61 ~
: 2648 3 61 wie
: 2652 5 61 so
- 2659
: 2680 3 61 Du
: 2684 3 61  ich
: 2688 3 61  fang
: 2692 3 61  Dir
: 2696 3 54  den
: 2700 6 54  Mond
: 2708 3 56  heut
: 2712 4 56  ein
- 2718
: 2744 3 56 Nur
: 2748 3 58  für
: 2752 3 59  Dich
: 2756 3 58  ganz
: 2760 3 56  al
: 2764 4 56 lein
: 2768 7 54 ~
E