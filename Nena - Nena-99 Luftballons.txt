#TITLE:Nena-99 Luftballons
#ARTIST:Nena
#MP3:Nena - Nena-99 Luftballons.mp3
#COVER:Nena - Nena-99 Luftballons [CO].jpg
#BACKGROUND:Nena - Nena-99 Luftballons [BG].jpg
#BPM:96,11
#GAP:3800
: 0 2 78 Hast 
: 2 2 80 Du 
: 4 2 76 et
: 6 2 80 was 
: 8 3 78 Zeit 
: 11 2 76 für 
: 13 7 73 mich 
- 17
: 18 2 73 Dann 
: 20 3 81 sin
: 23 1 81 ge 
: 24 2 81 ich 
: 27 2 81 ein 
: 29 3 81 Lied 
: 32 2 80 für 
: 34 3 78 Dich 
- 36
: 37 2 76 Von 
: 39 4 78 neun-
: 43 1 80 und-
: 44 3 76 neun
: 47 2 80 zig 
: 49 3 78 Luft
: 52 1 76 bal
: 53 5 73 lons  
- 58
: 60 2 73 Auf  
: 62 1 76 ih
: 63 2 73 rem  
: 65 4 76 Weg  
: 69 3 73 zum   
: 72 1 76 Ho
: 73 2 73 ri
: 75 6 76 zont  
- 81
: 82 3 78 Denkst  
: 85 1 80 Du  
: 86 2 76 viel
: 88 5 80 leicht  
: 94 2 78 grad'  
: 96 1 76 an   
: 97 5 73 mich  
- 102
: 103 2 73 Dann   
: 105 2 81 sin
: 107 1 81 ge 
: 108 3 81 ich  
: 111 2 81 ein  
: 113 3 81 Lied  
: 116 2 80 für  
: 118 3 78 Dich  
- 121
: 122 2 76 Von 
: 124 3 78 neun
: 127 1 80 und
: 128 3 76 neun
: 131 3 80 zig  
: 134 2 78  Luft
: 136 1 76 bal
: 137 5 73 lons 
- 142
: 143 3 73 Und   
: 146 3 76 dass   
: 149 2 73 so   
: 151 3 76 was   
: 154 3 73 von   
: 157 2 78 so   
: 159 2 78 was   
: 161 8 78 kommt     
- 400
: 429 3 78 neun-
: 432 1 80 und-
: 433 2 76 neun
: 435 3 80 zig  
: 438 1 78  Luft
: 439 2 76 bal
: 441 5 73 lons 
- 443
: 444 2 73 Auf 
: 446 1 76 ih
: 447 2 73 rem 
: 449 2 76 Weg 
: 451 2 81 zum 
: 453 2 81 Ho
: 455 1 80 ri
: 456 4 78 zont  
- 459
: 460 2 78 Hielt  
: 462 2 78  man  
: 464 1 80  für  
: 465 1 76  U
: 466 3 80 FOs  
: 469 3 78  aus  
: 472 1 76  dem  
: 473 4 73  All  
- 475
: 476 2 73 Dar
: 478 1 76 um  
: 479 2 73  schick
: 481 2 76 te  
: 483 2 76  ein  
: 485 2 76  Ge
: 487 1 78 ne
: 488 5 78 ral 
- 492
: 493 1 78 'ne  
: 494 1 78  Flie
: 495 1 80 ger
: 496 2 76 staf
: 498 4 80 fel 
: 502 1 78  hin
: 503 2 76 ter  
: 505 4 73 her
- 507
: 508 1 73 A
: 509 2 81 larm 
: 511 2 81  zu  
: 513 2 81  ge
: 515 2 81 ben,  
: 517 2 81  wenn's  
: 519 2 80  so  
: 521 2 78  wär  
- 522
: 523 2 78 Da
: 525 2 78 bei  
: 527 2 80  war'n  
: 529 2 76  da  
: 531 2 80  am  
: 533 2 78  Ho
: 535 1 76 ri
: 536 4 73 zont  
- 540
: 541 1 73 Nur  
: 542 2 81  neun-
: 544 1 81 und-
: 545 2 81 neun
: 547 1 81 zig  
: 548 2 81  Luft
: 550 2 80 bal
: 552 6 78 lons   
- 560
: 620 3 78 neun-
: 623 1 80 und-
: 624 3 76 neun
: 627 2 80 zig
: 629 2 78  Dü
: 631 2 76 sen
: 633 2 73 jae
: 635 1 73 ger
- 636
: 638 2 76 Je
: 640 1 73 der
: 641 2 76  war
: 643 2 73  ein
: 645 2 76  gros
: 647 1 73 ser
: 648 2 76  Krie
: 651 2 78 ger
- 652
: 653 2 78 Hiel
: 655 1 80 ten
: 656 3 76  sich
: 659 2 80  für
: 661 2 78  Cap
: 663 1 76 tain
: 664 5 73  Kirk
- 667
: 668 1 73 Das 
: 669 2 81 gab
: 672 1 81  ein
: 674 2 81  gros
: 676 1 81 ses
: 677 2 81  Feu
: 679 1 80 er
: 680 4 78 werk
- 683
: 684 1 76 Die 
: 685 2 76  Nach
: 687 3 76 barn 
: 690 1 76  ha
: 691 2 76 ben 
: 693 3 76  nichts 
: 696 1 73  ge
: 697 3 73 rafft 
- 699
: 700 2 73 Und 
: 701 2 76  fuehl
: 703 1 76 ten 
: 704 2 76 sich 
: 706 4 76  gleich
: 709 2 76 an
: 711 2 78 ge
: 713 2 78 macht 
- 714
: 715 2 78 Da
: 717 1 78 bei 
: 718 3 80  schoss
: 721 2 76  man 
: 724 1 80  am 
: 725 2 78  Ho
: 727 1 76 ri
: 728 4 73 zont 
- 731
: 732 2 73 Auf
: 733 3 81  neun-
: 736 1 81 und-
: 737 2 81 neun
: 739 1 81 zig
: 740 3 81  Luft
: 743 1 80 bal
: 744 6 78 lons 
- 800
: 941 3 78 neun-
: 944 1 80 und-
: 945 2 76 neun
: 947 2 80 zig
: 949 3 78  Kriegs
: 952 1 76 mi
: 953 1 73 ni
: 954 2 73 ster
- 955
: 956 4 76 Streich
: 960 2 73 holz
: 962 1 76  und
: 963 2 81  Ben
: 965 3 81 zin
: 968 1 80 ka
: 969 1 78 ni
: 970 3 76 ster
- 972
: 973 1 78 Hiel
: 974 2 80 ten
: 976 2 76  sich
: 978 2 80  für
: 980 2 78  schla
: 982 2 76 ü
: 984 2 73  Leu
: 986 3 73 te
- 988
: 989 2 81 Wit
: 991 2 81 ter
: 993 2 81 ten
: 995 2 81  schon
: 997 2 81  fet
: 999 1 80 te
: 1000 3 78  Beu
: 1003 2 76 te
- 1004
: 1005 2 78 Rie
: 1007 1 80 fen:
: 1008 4 76  Krieg
: 1012 2 76  und
: 1014 2 76  woll
: 1015 2 73 ten
: 1017 4 73  Macht
- 1019
: 1020 4 76 Mann,
: 1023 2 76  wer
: 1025 2 76  haet
: 1027 1 76 te
: 1028 2 76  das
: 1030 3 78  ge
: 1033 4 78 dacht
- 1036
: 1037 2 78 Dass
: 1039 1 80  es
: 1041 2 76  ein
: 1043 2 80 mal
: 1045 2 78  so
: 1047 1 76 weit
: 1048 3 73  kommt
- 1051
: 1051 2 81 Wegen 
: 1053 2 81 neun-
: 1055 1 81 und-
: 1056 2 81 neun
: 1058 2 81 zig
: 1060 2 81  Luft
: 1062 2 80 bal
: 1064 4 78 lons
- 1070
: 1083 3 81 Wegen
: 1086 2 81  neun-
: 1088 1 81 und-
: 1089 2 81 neun
: 1091 2 81 zig
: 1093 2 81  Luft
: 1095 1 80 bal
: 1096 6 78 lons
- 1130
: 1150 1 83  neun-
: 1151 1 83 und-
: 1152 2 83 neun
: 1154 2 83 zig
: 1156 2 83  Luft
: 1158 2 80 bal
: 1160 6 78 lons 
- 1180
: 1217 5 78 neun-
: 1222 1 80 und-
: 1223 3 76 neun
: 1226 3 80 zig
: 1229 2 78  Jah
: 1231 1 76 re
: 1232 6 73  Krieg
- 1238
: 1239 4 76 Lies
: 1243 3 73 sen
: 1246 2 76  kei
: 1248 2 73 nen
: 1250 4 76  Platz
: 1254 1 73  für
: 1255 2 76  Sie
: 1257 4 78 ger
- 1261
: 1262 3 78 Kriegs
: 1265 2 80 mini 
: 1267 4 80 ster 
: 1271 3 78  gibt's
: 1274 2 76  nicht
: 1276 5 73  mehr
- 1281
: 1283 2 81 Und
: 1285 3 81  auch
: 1288 3 81  kei
: 1291 1 81 ne
: 1292 2 81  Dü
: 1294 3 80 sen
: 1297 3 76 flie
: 1300 3 76 ger
- 1303
: 1304 2 78 Heu
: 1306 2 80 te
: 1308 3 76  zieh
: 1311 4 80  ich
: 1315 2 78  mei
: 1317 2 76 ne
: 1319 3 73  Run
: 1322 4 73 den
- 1326
: 1327 5 76 Seh'
: 1332 1 73  die
: 1333 3 76  Welt
: 1336 2 73  in
: 1338 2 76  Trüm
: 1340 2 73 mern
: 1342 3 76  lie
: 1345 2 78 gen
- 1349
: 1350 4 78 Hab'
: 1354 1 80  'nen
: 1355 3 76  Luft
: 1358 2 80 bal
: 1360 3 78 lon
: 1363 1 76  ge
: 1364 2 73 fun
: 1366 4 73 den
- 1372
: 1373 4 76 Denk'
: 1377 2 73  an
: 1379 2 76  Dich
: 1381 2 73  und
: 1383 3 78  lass'
: 1386 3 78  ihn
: 1389 4 78  flie
: 1393 9 76 gen
E
