#TITLE:Albachiara
#ARTIST:Vasco Rossi
#MP3:Vasco Rossi - Albachiara.mp3
#COVER:Vasco Rossi - Albachiara [CO].jpg
#BACKGROUND:Vasco Rossi - Albachiara [BG].jpg
#BPM:350,00
#GAP:17700,00
#ENCODING:UTF8
#LANGUAGE:Italian
#EDITION:Singstar - Italian Party
: 0 2 60 Re
: 6 3 67 spi
: 11 2 67 ri
: 16 4 67  pia
: 22 2 67 no
- 25
: 27 3 67 Per
: 32 2 67  non
: 38 4 65  far
: 44 2 65  ru
: 50 13 65 mo
: 65 3 65 re
- 69
: 71 3 65 Ti~ ad
: 76 3 64 dor
: 81 3 64 men
: 88 3 64 ti
: 93 2 64  di
: 99 8 64  se
: 109 3 64 ra
- 113
: 115 3 64 Ti
: 120 3 64  ri
: 126 3 62 sve
* 131 3 64 gli
: 137 3 64  col
: 143 6 64  so
: 149 2 64 le
- 153
: 177 2 60 Sei
: 182 4 60  chia
: 188 2 60 ra
: 193 2 60  co
: 198 3 62 me~ un'
: 203 12 62 al
: 219 3 60 ba
- 224
: 265 2 55 Sei
: 270 2 62  fre
: 275 2 62 sca
: 280 3 62  co
: 285 3 64 me
: 289 12 64  l'a
: 303 4 62 ria
- 309
: 354 2 60 Di
: 359 2 67 ven
: 364 4 67 ti
: 370 3 67  ros
: 376 3 67 sa
: 381 2 67  se
- 385
: 387 3 67 Qual
: 393 2 65 cu
: 398 3 65 no
: 403 3 65  ti
: 408 10 65  guar
: 421 3 65 da~ e
- 425
: 427 2 65 Sei
: 432 3 65  fan
: 438 3 64 ta
: 444 2 64 sti
: 449 3 64 ca
: 454 3 64  quan
: 459 3 64 do
- 463
: 464 4 64 Sei~ as
: 473 5 62 sor
: 482 5 64 ta
- 489
: 537 2 60 Nei
: 542 7 60  tuoi
: 553 2 60  pro
: 558 13 62 ble
: 574 2 64 mi
- 578
: 621 2 62 Nei
: 626 7 62  tuoi
: 636 3 64  pen
* 642 9 64 sie
: 652 3 62 ri
- 657
: 701 2 55 Ti
: 706 2 57  ve
: 711 2 59 sti
: 716 3 60  svo
: 721 2 62 glia
: 726 3 64 ta
: 731 6 60 men
: 741 4 62 te
- 746
: 746 2 64 Non
: 751 6 60  met
: 762 4 62 ti
: 768 3 64  mai
: 773 7 60  nien
: 784 3 62 te
- 788
: 789 2 64 Che
: 794 7 60  pos
: 805 2 62 sa~ at
: 810 2 64 ti
: 815 7 60 ra
: 822 2 60 re
- 825
: 826 2 62 At
* 831 2 64 ten
: 836 8 60 zio
: 844 2 60 ne
- 848
: 879 2 57 Un
: 884 2 60  par
: 889 2 62 ti
: 894 3 64 co
: 899 8 60 la
: 908 5 55 re
- 915
: 951 3 60 So
: 956 2 60 lo
: 961 2 60  per
: 966 2 60  far
: 971 2 62 ti
: 976 2 64  guar
: 981 9 60 da
: 992 8 62 re
- 1002
: 1410 2 60 Re
: 1415 3 67 spi
: 1419 2 67 ri
: 1424 3 67  pia
: 1429 2 67 no
- 1432
: 1434 2 67 Per
: 1438 3 67  non
: 1443 3 65  far
: 1448 2 65  ru
: 1453 14 65 mo
: 1468 2 65 re
- 1472
: 1474 2 65 Ti~ ad
: 1478 2 65 dor
: 1483 2 64 men
: 1488 2 64 ti
: 1492 2 64  di
: 1497 9 64  se
: 1508 2 64 ra
- 1511
: 1513 3 64 Ti
: 1517 2 64  ri
: 1522 2 62 sve
: 1527 2 64 gli
: 1531 2 64  col
: 1537 6 64  so
: 1543 2 64 le
- 1547
: 1566 2 57 Sei
: 1571 3 60  chia
: 1575 3 60 ra
: 1580 2 60  co
: 1585 3 62 me~ un'
: 1590 11 62 al
: 1605 2 60 ba
- 1609
: 1643 2 57 Sei
: 1648 2 62  fre
: 1653 2 62 sca
: 1658 3 62  co
: 1663 3 64 me
* 1667 12 64  l'a
: 1682 6 62 ria
- 1690
: 1721 2 60 Di
: 1726 2 67 ven
: 1731 3 67 ti
: 1736 2 67  ros
: 1741 2 67 sa
: 1746 2 67  se
- 1749
: 1751 2 67 Qual
: 1756 2 65 cu
: 1761 2 65 no
: 1766 2 65  ti
: 1770 6 65  guar
: 1780 2 65 da~ e
- 1783
: 1785 2 65 Sei
: 1789 2 65  fan
: 1794 3 64 ta
: 1800 2 64 sti
: 1804 2 64 ca
: 1808 3 64  quan
: 1813 2 64 do
- 1816
* 1818 5 64 Sei~ as
: 1828 5 62 sor
: 1837 6 64 ta
- 1845
: 1882 2 57 Nei
: 1887 5 60  tuoi
: 1896 2 60  pro
: 1901 13 62 ble
: 1916 3 64 mi
- 1921
: 1959 3 62 Nei
: 1965 6 62  tuoi
: 1974 2 64  pen
: 1979 13 64 sie
: 1993 6 62 ri
- 2001
: 2033 2 55 Ti
: 2037 3 57  ve
: 2042 2 59 sti
: 2047 3 60  svo
: 2052 2 62 glia
: 2056 2 64 ta
: 2061 5 60 men
: 2070 3 62 te
- 2074
: 2075 2 64 Non
: 2080 6 60  met
: 2089 3 62 ti
: 2094 3 64  mai
: 2099 6 60  nien
: 2108 3 62 te
- 2112
: 2113 2 64 Che
: 2118 5 60  pos
: 2127 2 62 sa~ at
: 2132 3 64 ti
: 2137 7 60 rare
- 2145
: 2146 2 62 At
: 2151 2 64 ten
: 2156 6 60 zione
- 2164
: 2193 2 60 Un
: 2198 3 60  par
: 2202 3 62 ti
: 2207 3 64 co
: 2212 7 60 la
: 2221 4 55 re
- 2227
: 2260 2 60 Per
: 2265 2 60  far
: 2269 3 62 ti
: 2274 2 64  guar
: 2278 8 60 da
: 2288 11 62 re
- 2301
: 2336 2 55 E
* 2340 3 67  con
: 2345 2 67  la
: 2350 2 67  fac
: 2355 2 67 cia
: 2360 3 67  pu
: 2365 5 65 li
: 2374 2 64 ta
- 2377
: 2379 2 62 Cam
: 2384 6 65 mi
: 2393 2 64 ni
: 2398 2 62  per
: 2403 7 64  stra
: 2413 2 62 da
- 2416
: 2417 2 60 Man
: 2422 5 64 gian
: 2431 2 62 do~ u
: 2436 2 60 na
: 2441 7 64  me
: 2450 2 62 la
- 2453
: 2455 3 60 Coi
: 2460 5 64  li
: 2469 3 62 bri
: 2474 2 60  di
: 2479 7 64  scuo
: 2488 2 60 la
- 2492
: 2498 2 60 Ti
: 2503 2 64  pia
: 2508 2 62 ce
: 2512 2 60  stu
: 2517 7 64 dia
: 2526 4 60 re
- 2532
: 2545 2 60 Non
: 2550 3 62  te
* 2555 2 64  ne
: 2559 3 62  de
: 2564 3 60 vi
- 2568
: 2570 2 57 Ver
: 2574 2 60 go
: 2578 8 59 gna
: 2588 8 55 re
- 2598
: 2640 2 55 E
: 2645 2 57  quan
: 2650 3 59 do
: 2655 2 60  guar
: 2660 2 62 di
- 2663
: 2664 2 64 Con
: 2669 2 65  que
* 2674 4 67 gli~ oc
: 2681 4 65 chi
: 2688 5 64  gran
: 2697 5 62 di
- 2704
: 2718 2 60 For
: 2723 2 60 se~ un
: 2727 2 60  po'
: 2732 2 60  trop
: 2737 2 62 po
- 2740
: 2742 2 64 Sin
: 2747 8 60 ce
: 2756 2 62 ri,
: 2761 2 64  sin
: 2766 7 60 ce
: 2775 3 62 ri
- 2780
: 2795 2 60 Si
: 2799 3 60  ve
: 2804 2 60 de
: 2809 2 60  quel
: 2813 2 62 lo
: 2818 2 64  che
: 2823 5 60  pen
: 2832 3 55 si
- 2837
: 2871 3 60 Quel
: 2876 2 62 lo
: 2880 2 64  che
: 2885 6 60  so
: 2894 8 62 gni
- 2904
: 2954 3 67 Qual
: 2959 2 67 che
* 2963 2 67  vol
: 2968 2 67 ta
: 2973 3 67  fai
- 2977
: 2978 2 67 Pen
: 2984 4 65 sie
: 2990 2 65 ri
: 2997 9 65  stra
: 3008 6 64 ni
- 3016
: 3029 3 60 Con
: 3034 2 60  u
: 3039 3 62 na
: 3043 4 64  ma
: 3048 3 60 no
- 3052
: 3053 2 60 U
: 3057 3 62 na
: 3062 4 64  ma
: 3067 3 62 no
: 3072 2 60  ti
* 3077 8 62  sfio
: 3087 9 64 ri
- 3098
: 3112 6 65 Tu
: 3121 7 60  so
: 3130 3 60 la
: 3136 5 62  den
: 3145 3 64 tro~ u
: 3150 2 62 na
: 3155 5 60  stan
: 3165 5 55 za
- 3172
: 3185 2 60 E
: 3190 2 62  tut
: 3194 2 62 to~ il
: 3198 2 62  mon
: 3203 2 64 do
: 3207 15 64  fuo
: 3223 21 62 ri
E