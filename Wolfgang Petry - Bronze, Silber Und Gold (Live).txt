#TITLE:Bronze, Silber Und Gold (Live)
#ARTIST:Wolfgang Petry
#MP3:Wolfgang Petry - Bronze, Silber Und Gold (Live).mp3
#COVER:Wolfgang Petry - Bronze, Silber Und Gold (Live) [CO].jpg
#BACKGROUND:Wolfgang Petry - Bronze, Silber Und Gold (Live) [BG].jpg
#BPM:258
#GAP:34534,88
#ENCODING:UTF8
#LANGUAGE:German
#GENRE:Schlager
#EDITION:SingStar Made In Germany
#YEAR:1995
: 0 2 53 Wir
: 3 6 62  warn
: 11 6 62  wie
: 19 5 62  Pech
: 24 6 58  und
: 31 6 60  Schwe
: 38 9 57 fel
- 49
: 66 5 58 Leb
: 72 8 58 ten
: 82 4 62  in
: 86 8 58  den
: 94 6 57  Tag
- 102
: 124 4 53 Zum
: 128 7 55  Teu
: 136 4 55 fel
: 140 3 58 ~
: 144 8 58  mit
* 152 4 55  der
: 156 7 58  Lie
: 164 9 58 be
- 175
: 188 2 53 Die
: 190 7 62  Ge
: 198 4 62 füh
: 202 2 62 le
: 206 7 62  hab'n
: 214 9 63  ver
: 224 7 60 sagt
- 233
: 251 3 53 Dein
: 255 5 62  Herz
: 262 6 62  es
: 269 8 62  schreit
: 277 5 58  nach
: 282 6 60  Lie
: 290 8 57 be
- 300
: 314 4 53 Ich
: 318 3 58  ließ
: 322 2 58  dich
: 326 2 58  viel
: 330 3 58  zu
: 334 6 62  oft
: 342 4 58  al
: 346 6 57 lein
- 354
: 376 4 53 Erst
* 380 7 55  heu
: 388 4 57 te
: 392 3 58 ~
: 396 8 58  bin
: 404 5 55  ich
: 412 6 58  stark
: 420 4 58  ge
: 424 5 58 nug
- 431
: 440 3 55 Mir
: 443 3 62  al
: 447 5 62 les
: 454 7 62  zu
: 462 6 63  ver
: 470 7 60 zeihn
- 478
: 480 3 62 Bron
: 484 2 62 ze
: 488 3 62  Sil
: 492 4 62 ber
: 496 4 60  und
: 500 5 62  Gold
- 507
: 512 3 62 Hab
: 516 4 62  ich
: 520 6 65  nie
: 528 4 58  ge
: 532 5 60 wollt
- 539
: 546 4 58 Ich
: 550 8 60  will
: 558 7 62  nur
: 566 7 63  dich
: 574 7 63  nur
: 582 6 63  dich
: 588 6 62  al
: 594 5 58 lein
- 601
: 606 3 62 Bron
: 610 3 62 ze
: 614 3 62  Sil
: 618 4 62 ber
: 622 4 60  und
: 626 8 62  Gold
- 636
: 638 2 62 Hab
: 641 5 62  ich
: 646 8 65  nie
: 654 4 58  ge
: 658 8 60 wollt
- 668
: 674 3 60 Ich
: 678 7 60  will
: 686 7 62  nur
: 694 7 63  eins
- 702
: 702 6 63 Nur
: 709 6 63  dich
: 716 3 63  nur
: 720 7 63  dich
: 727 4 62  al
: 731 6 58 lein
- 739
: 742 4 58 Es
: 746 4 60  ist
: 750 4 62  e
: 754 13 67 gal
: 768 8 65  was
: 776 8 63  auch
: 784 11 62  kommt
- 797
: 804 4 62 Es
: 808 3 65  ist
: 812 5 65  e
: 818 9 65 gal
: 828 3 65  wie's
: 832 8 65  wei
: 840 3 67 ter
: 844 6 67 geht
- 852
: 856 3 62 Bron
: 860 3 62 ze
: 864 3 62  Sil
: 868 4 62 ber
: 872 4 60  und
: 876 6 62  Gold
- 884
: 886 3 62 Hab
: 890 4 62  ich
: 894 8 65  nie
: 902 4 58  ge
: 906 6 60 wollt
- 914
: 920 4 60 Ich
: 924 4 58  will
: 928 3 57  nur
: 932 4 57 ~
: 936 7 58  dich
- 945
: 992 3 53 Du
: 996 4 53  bist
: 1000 3 62  bes
: 1004 7 62 ser
: 1012 6 62  als
: 1020 7 58  das
: 1028 5 60  Le
: 1033 4 60 ben
- 1039
: 1054 3 57 Und
: 1058 4 57  das
: 1062 6 58  kann
: 1070 6 58  kein
* 1078 7 58  Zu
: 1086 4 55 fall
: 1090 9 57  sein
- 1101
: 1120 4 53 Doch
: 1124 8 55  in
: 1132 7 58  den
: 1140 6 58  letz
: 1146 5 55 ten
: 1152 4 55  Ta
: 1156 6 58 gen
- 1164
: 1178 3 58 Zieht
: 1182 3 58  ein
: 1186 3 60  Sturm
: 1189 4 62 ~
: 1194 5 62  an
: 1200 7 62  uns
: 1208 3 62  vor
: 1211 2 63 ~
: 1214 6 60 bei
- 1222
: 1244 4 53 Dein
: 1248 6 62  Herz
: 1256 5 62  es
: 1264 6 62  schreit
: 1272 4 58  nach
: 1276 7 60  Lie
: 1284 5 57 be
- 1291
: 1308 3 53 Ich
: 1311 3 58  ließ
: 1315 3 58  dich
: 1318 4 57  viel
: 1323 5 58  zu
: 1328 5 62  oft
: 1334 5 58  al
: 1339 6 57 lein
- 1347
* 1368 4 53 Erst
: 1372 7 55  heu
: 1380 4 57 te
: 1384 3 58 ~
: 1388 8 58  bin
: 1396 5 55  ich
: 1402 9 58  stark
: 1412 3 58  ge
: 1416 6 58 nug
- 1424
: 1430 4 58 Mir
: 1434 4 62  al
: 1439 6 62 les
: 1447 8 62  zu
: 1455 6 63  ver
: 1463 8 60 zeihn
- 1472
: 1474 3 62 Bron
: 1478 2 62 ze
: 1482 3 62  Sil
: 1486 4 62 ber
: 1490 4 60  und
: 1494 5 62  Gold
- 1501
: 1506 3 62 Hab
: 1510 3 62  ich
: 1513 5 65  nie
: 1518 3 58 ~
: 1522 4 58  ge
: 1526 7 60 wollt
- 1535
: 1540 4 58 Ich
: 1544 8 60  will
: 1552 7 62  nur
: 1560 7 63  dich
: 1568 7 63  nur
: 1576 6 63  dich
: 1582 6 62  al
: 1588 5 58 lein
- 1595
: 1598 3 62 Bron
: 1602 3 62 ze
: 1606 3 62  Sil
: 1610 4 62 ber
: 1614 4 60  und
: 1618 8 62  Gold
- 1628
: 1630 2 62 Hab
: 1633 5 62  ich
: 1638 4 65  nie
: 1642 3 58 ~
: 1646 4 58  ge
: 1650 8 60 wollt
- 1660
: 1666 3 60 Ich
: 1670 7 60  will
: 1678 7 62  nur
: 1686 7 63  eins
- 1694
: 1694 6 63 Nur
: 1701 6 63  dich
: 1708 3 63  nur
: 1712 6 63  dich
: 1718 5 62  al
: 1723 6 58 lein
- 1731
: 1734 4 58 Es
: 1738 4 60  ist
: 1742 4 62  e
: 1746 4 65 gal
* 1750 7 67 ~
: 1758 8 65  was
: 1768 8 63  auch
: 1776 6 62  kommt
- 1784
: 1798 4 63 Es
: 1802 3 65  ist
: 1806 3 65  e
: 1810 9 65 gal
: 1820 3 65  wie's
: 1824 9 65  wei
: 1834 3 67 ter
: 1838 6 67 geht
- 1846
: 1848 3 62 Bron
: 1852 3 62 ze
: 1856 3 62  Sil
: 1860 4 62 ber
: 1864 4 60  und
: 1868 6 62  Gold
- 1876
: 1878 3 62 Hab
: 1882 4 62  ich
: 1886 5 65  nie
: 1891 2 58 ~
: 1894 4 58  ge
: 1898 6 60 wollt
- 1906
: 1914 4 60 Ich
: 1918 4 58  will
: 1922 3 57  nur
: 1926 4 57 ~
: 1930 7 58  dich
- 1939
: 1948 7 55 Ah
- 1957
: 2190 4 62 Bron
: 2194 3 60 ze
: 2198 3 62  Sil
: 2202 4 62 ber
: 2206 4 60  und
: 2211 5 62  Gold
- 2218
: 2223 3 62 Hab
: 2227 3 62  ich
* 2230 4 65  nie
: 2234 3 58 ~
: 2238 4 58  ge
: 2242 7 60 wollt
- 2251
: 2258 4 58 Ich
: 2262 8 60  will
: 2270 7 62  nur
: 2278 7 63  dich
: 2286 5 63  nur
: 2292 6 63  dich
: 2298 6 62  al
: 2304 5 58 lein
- 2311
: 2316 3 62 Bron
: 2320 3 62 ze
: 2324 3 62  Sil
: 2328 4 62 ber
: 2332 4 60  und
: 2336 8 62  Gold
- 2346
: 2348 2 62 Hab
: 2351 5 62  ich
: 2356 4 65  nie
: 2360 3 58 ~
: 2364 4 58  ge
: 2368 6 60 wollt
- 2376
: 2382 3 60 Ich
* 2386 7 60  will
: 2394 7 62  nur
: 2402 7 63  eins
- 2410
: 2410 6 63 Nur
: 2417 6 63  dich
: 2424 3 63  nur
: 2428 7 63  dich
: 2435 4 62  al
: 2439 6 58 lein
- 2447
: 2452 4 58 Es
: 2456 4 60  ist
: 2460 4 62  e
: 2464 4 65 gal
: 2468 7 67 ~
: 2476 8 65  was
* 2486 8 63  auch
: 2494 6 62  kommt
- 2502
: 2514 4 63 Es
: 2518 3 65  ist
: 2522 3 65  e
: 2526 9 65 gal
: 2536 3 65  wie's
: 2540 9 65  wei
: 2550 3 67 ter
: 2554 6 67 geht
- 2562
: 2564 3 62 Bron
: 2568 3 62 ze
: 2572 3 62  Sil
: 2576 4 62 ber
: 2580 4 60  und
: 2584 6 62  Gold
- 2592
: 2596 3 62 Hab
: 2600 4 62  ich
: 2604 8 65  nie
* 2612 4 58  ge
: 2616 6 60 wollt
- 2624
: 2630 4 58 Ich
: 2634 8 60  will
: 2642 7 62  nur
: 2650 7 63  dich
: 2658 7 63  nur
: 2666 7 63  dich
: 2674 4 62  al
: 2678 5 58 lein
- 2685
: 2688 3 62 Bron
: 2692 3 62 ze
: 2696 3 62  Sil
: 2700 4 62 ber
: 2704 4 60  und
: 2708 6 62  Gold
- 2716
: 2720 2 62 Hab
: 2723 5 62  ich
: 2728 4 65  nie
: 2732 3 58 ~
: 2736 4 58  ge
: 2740 6 60 wollt
- 2748
: 2754 3 60 Ich
: 2758 7 60  will
: 2766 7 62  nur
: 2774 7 63  eins
- 2782
: 2782 7 63 Nur
: 2790 6 63  dich
: 2797 3 63  nur
: 2802 7 63  dich
: 2809 4 62  al
: 2813 6 58 lein
- 2821
: 2824 4 58 Es
: 2828 4 60  ist
: 2832 4 62  e
: 2836 4 65 gal
: 2840 7 67 ~
* 2848 10 65  was
: 2860 8 63  auch
: 2868 6 62  kommt
- 2876
: 2886 4 63 Es
: 2890 3 65  ist
: 2894 3 65  e
: 2898 9 65 gal
: 2910 3 65 wie's
: 2914 6 65  wei
: 2920 3 67 ter
: 2924 6 67 geht
- 2932
: 2936 2 62 Bron
: 2940 2 62 ze
: 2944 2 62  Sil
: 2948 4 62 ber
: 2952 4 60  und
: 2956 6 62  Gold
- 2964
: 2968 2 62 Hab
: 2971 3 62  ich
: 2974 4 65  nie
: 2978 3 58 ~
: 2982 4 58  ge
: 2986 6 60 wollt
- 2994
: 3000 6 60 Ich
: 3006 6 58  will
: 3012 4 57  nur
: 3016 7 58  dich
E