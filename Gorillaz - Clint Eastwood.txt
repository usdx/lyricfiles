#TITLE:Clint Eastwood
#ARTIST:Gorillaz
#MP3:Gorillaz - Clint Eastwood.mp3
#VIDEO:Gorillaz - Clint Eastwood.avi
#COVER:Gorillaz - Clint Eastwood [CO].jpg
#BPM:85
#GAP:13770
#ENCODING:UTF8
#LANGUAGE:Englisch
: 74 2 25 Oh 
: 76 1 23 oh 
: 77 2 22 oh 
: 79 2 20 oh 
: 81 4 18 oh
: 93 2 15  I 
: 95 3 15 hap
: 98 4 10 py
- 103
: 103 1 3 I'm 
: 104 1 3 feel
: 105 2 6 ing 
: 107 2 10 glad
: 109 1 9  I 
: 110 2 10 got 
: 112 2 15 sun
: 114 4 10 shine 
- 119
: 120 1 3 in 
: 121 2 6 a 
: 123 3 10 bag
: 126 2 10  I'm 
: 128 2 10 use
: 130 4 10 less
- 135
: 135 1 6 but 
: 136 2 8 not 
: 138 1 8 for 
: 139 3 8 long
: 142 2 8  the 
: 144 2 6 fu
: 146 4 3 ture 
- 151
: 151 1 3 is 
: 152 2 1 com
: 154 1 3 ing 
: 155 2 3 on
- 158
: 158 2 15 I 
: 160 2 15 hap
: 162 4 10 py
: 167 1 3  I'm 
: 168 2 3 feel
: 170 1 6 ing 
: 171 2 10 glad
- 174
: 174 1 9 I 
: 175 1 10 got 
: 176 2 15 sun
: 178 5 10 shine 
: 185 1 3  in 
: 186 2 6 a 
: 188 3 10 bag
- 192
: 192 1 10 I'm 
: 193 2 10 use
: 195 4 10 less
- 200
: 200 1 6 but 
: 201 2 8 not 
: 203 1 8 for 
: 204 3 8 long
: 207 1 8  the 
: 208 2 6 fu
: 210 4 3 ture 
- 215
: 215 2 3 is 
: 217 2 1 com
: 219 1 3 ing 
: 220 3 3 on
: 223 2 3  it's 
: 225 2 1 com
: 227 1 3 ing 
: 228 3 3 on
- 232
: 232 1 3 it's 
: 233 2 1 com
: 235 2 3 ing 
: 237 2 3 on
: 239 2 3  it's 
: 241 2 1 com
: 243 1 3 ing 
: 244 3 3 on
- 248
: 248 1 3 it's 
: 249 2 1 com
: 251 1 3 ing 
: 252 4 3 on
- 257
: 257 2 6 Fi
: 259 1 6 nal
: 260 1 6 ly 
: 261 1 6 so
: 262 1 6 meo
: 263 1 6 ne 
: 264 1 6 let 
: 265 1 6 me 
: 266 1 6 out
: 267 1 6  of 
: 268 1 6 my 
: 269 2 6 cage
- 272
: 272 1 6 Now 
: 273 2 6 time 
: 275 1 6 for 
: 276 1 6 me 
: 277 1 6 is 
: 278 1 6 no
: 279 1 6 thin'
: 280 1 6 'cos 
: 281 1 6 I'm 
: 282 1 6 coun
: 283 1 6 ting 
: 284 1 6 no 
: 285 2 6 age
- 288
: 288 1 6 Now 
: 289 1 6 I 
: 290 2 6 couldn't 
: 292 1 6 be 
: 293 1 6 the
: 294 1 6 re
- 296
: 296 1 6 now 
: 297 1 6 you 
: 298 2 6 shouldn't 
: 300 1 6 be 
: 301 1 6 sca
: 302 1 6 red
- 304
: 304 2 6 I'm 
: 306 1 6 good 
: 307 1 6 at 
: 308 1 6 re
: 309 3 6 pairs
: 312 1 6  and 
: 313 1 6 I'm 
: 314 2 6 under 
: 316 1 6 each 
: 317 2 6 snare
- 320
: 320 1 6 In
: 321 2 6 tan
: 323 1 6 gi
: 324 1 6 ble
- 326
: 326 1 6 I 
: 327 1 6 bet 
: 328 1 6 you 
: 329 1 6 didn't 
: 330 1 6 think 
: 331 1 6 so
: 332 1 6 I 
: 333 2 6 com
: 335 1 6 mand 
: 336 1 6 you 
: 337 1 6 to
- 339
: 339 1 6 pano
: 340 1 6 ra
: 341 1 6 mic 
: 342 1 6 view 
- 344
: 344 1 6 look 
: 345 1 6 I'll 
: 346 1 6 make 
: 347 1 6 it 
: 348 2 6 all 
: 350 1 6 ma
: 351 1 6 na
: 352 3 6 geable
- 356
: 356 1 6 Pick 
: 357 1 6 and 
: 358 3 6 choose 
: 361 1 6 sit 
: 362 1 6 and 
: 363 2 6 lose
: 365 1 6  all 
: 366 1 6 you 
: 367 1 6 dif
: 368 1 6 ferent 
: 369 2 6 crews
- 372
: 372 2 6 Chicks 
: 374 1 6 and 
: 375 2 6 dudes
: 377 1 6  who 
: 378 1 6 you 
: 379 1 6 think 
: 380 1 6 is 
: 381 1 6 real
: 382 1 6 ly
: 383 1 6  kick
: 384 1 6 ing 
: 385 2 6 tunes
- 388
: 388 2 6 pic
: 390 1 6 ture 
: 391 2 6 you 
: 393 1 6 get
: 394 1 6 ting 
: 395 2 6 down
: 397 1 6  and 
: 398 1 6 I'll 
: 399 1 6 pic
: 400 1 6 ture 
: 401 3 6 too
- 405
: 405 1 6 like 
: 406 1 6 you 
: 407 1 6 lit 
: 408 1 6 the 
: 409 3 6 fuse
: 412 1 6  you 
: 413 1 6 think 
: 414 1 6 it's 
: 415 1 6 fic
: 416 1 6 tio
: 417 1 6 nal
- 419
: 419 1 6 my
: 420 1 6 sti
: 421 2 6 cal 
: 423 2 6 may
: 425 1 6 be
- 427
: 427 1 6 spi
: 428 1 6 ri
: 429 2 6 tual 
: 431 1 6 he
: 432 1 6 ro 
- 434
: 434 1 6 who 
: 435 1 6 ap
: 436 1 6 pears 
: 437 1 6 on 
: 438 1 6 you
- 440
: 440 1 6 clear 
: 441 1 6 your 
: 442 1 6 view
: 443 1 6  when 
: 444 2 6 you're 
: 446 2 6 too 
: 448 1 6 cra
: 449 2 6 zy
- 452
: 452 1 6 Life
: 453 1 6 less 
- 455
: 455 1 6 for 
: 456 1 6 whose 
: 457 1 6 de
: 458 1 6 fi
: 459 1 6 ni
: 460 1 6 tion 
: 461 1 6 is
: 462 1 6  for 
: 463 1 6 what 
: 464 2 6 life 
: 466 2 6 is
- 469
: 469 1 6 price
: 470 1 6 less 
- 473
: 473 1 6 be
: 474 1 6 cause
: 475 1 6  I 
: 476 1 6 put 
: 477 1 6 ya 
: 478 1 6 on 
: 479 1 6 the 
: 480 2 6 hype 
: 482 1 6 shift
- 484
: 484 1 6 Did 
: 485 3 6 like 
: 488 2 6 Gut 
: 490 1 6 smo
: 491 1 6 kin'
: 492 2 6  ri
: 494 1 6 ghteous
- 496
: 496 2 6 one 
: 498 1 6 tal
: 499 1 6 kin' 
: 500 2 6 psy
: 502 1 6 chic
: 503 1 6  but 
: 504 1 6 among 
- 506
: 506 1 6 knows 
: 507 1 6 pos
: 508 2 6 sess
: 510 1 6  you 
: 511 1 6 with 
: 512 2 6 one 
: 514 2 6 though
- 517
: 517 2 15 hap
: 519 3 10 py
- 523
: 523 1 3 I'm 
: 524 2 3 feel
: 526 2 6 ing 
: 528 2 10 glad
: 530 1 9  I 
: 531 2 10 got 
: 533 2 15 sun
: 535 4 10 shine 
- 541
: 541 1 3 in 
: 542 2 6 a 
: 544 3 10 bag
: 547 2 10  I'm 
: 549 2 10 use
: 551 4 10 less
- 556
: 556 1 6 but 
: 557 2 8 not 
: 559 1 8 for 
: 560 3 8 long
: 563 2 8  the 
: 565 2 6 fu
: 567 3 3 ture 
- 571
: 571 2 3 is 
: 573 2 1 com
: 575 1 3 ing 
: 576 2 3 on
- 579
: 579 2 15 I 
: 581 2 15 hap
: 583 3 10 py
- 587
: 587 2 3 I'm 
: 589 2 3 feel
: 591 2 6 ing 
: 593 1 10 glad
: 594 1 9  I 
: 595 2 10 got 
: 597 2 15 sun
: 599 4 10 shine 
- 605
: 605 2 3 in 
: 607 2 6 a 
: 609 3 10 bag
: 612 2 10  I'm 
: 614 2 10 use
: 616 4 10 less
- 621
: 621 1 6 but 
: 622 2 8 not 
: 624 1 8 for 
: 625 3 8 long
: 628 2 8  the 
: 630 2 6 fu
: 632 3 3 ture 
- 636
: 636 2 3 is 
: 638 2 1 com
: 640 1 3 ing 
: 641 3 3 on
: 644 2 3  it's 
: 646 2 1 com
: 648 1 3 ing 
: 649 3 3 on
- 653
: 653 1 3 it's 
: 654 2 1 com
: 656 1 3 ing 
: 657 3 3 on
: 660 2 3  it's 
: 662 2 1 com
: 664 1 3 ing 
: 665 3 3 on
- 669
: 669 1 3 it's 
: 670 2 1 com
: 672 1 3 ing 
: 673 3 3 on
- 678
: 678 1 6 The 
: 679 1 6 es
: 680 1 6 sence 
: 681 1 6 the 
: 682 2 6 ba
: 684 1 6 sics
- 686
: 686 1 6 with
: 687 1 6 out 
: 688 1 6 it 
: 689 1 6 you 
: 690 2 6 make 
: 692 1 6 it
- 694
: 694 1 6 al
: 695 1 6 low 
: 696 1 6 me 
: 697 1 6 to 
: 698 2 6 make 
: 700 2 6 this
- 703
: 703 2 6 child 
: 705 1 6 like 
: 706 1 6 a 
: 707 1 6 na
: 708 2 6 ture
- 711
: 711 1 6 rhy
: 712 1 6 thm 
- 714
: 714 1 6 you 
: 715 1 6 have 
: 716 1 6 it
: 717 1 6  or 
: 718 3 6 don't
: 721 1 6  that's 
: 722 1 6 a 
: 723 1 6 fal
: 724 1 6 la
: 725 1 6 cy
: 726 1 6  I'm 
: 727 1 6 in 
: 728 2 6 them
- 731
: 731 2 6 every 
: 733 1 6 spi
: 734 1 6 ral
: 735 1 6 ling 
: 736 1 6 tree
- 737
: 738 1 6 every 
: 739 2 6 child 
: 741 1 6 of 
: 742 1 6 peace
- 744
: 744 1 6 e
: 745 1 6 very 
: 746 1 6 cloud 
: 747 1 6 I 
: 748 1 6 see
- 749
: 750 1 6 you 
: 751 1 6 see 
: 752 1 6 with 
: 753 1 6 yo
: 754 1 6 ur 
: 755 1 6 eyes
- 757
: 757 1 6 you 
: 758 1 6 see 
: 759 1 6 de
: 760 1 6 stru
: 761 1 6 ction 
: 762 1 6 and 
: 763 1 6 de
: 764 1 6 mise
- 766
: 766 1 6 cor
: 767 2 6 rup
: 769 1 6 tion 
: 770 1 6 in 
: 771 2 6 skies
- 774
: 774 1 6 from 
: 775 1 6 this 
: 776 1 6 fuck
: 777 1 6 ing 
: 778 1 6 en
: 779 1 6 terp
: 780 1 6 rise
- 782
: 782 1 6 that 
: 783 1 6 I'm 
: 784 1 6 suck
: 785 1 6 ed 
: 786 1 6 in
: 787 1 6 to 
: 788 1 6 your 
: 789 1 6 lies
- 791
: 791 1 6 the 
: 792 1 6 Rus
: 793 1 6 sell 
: 794 1 6 that 
: 795 1 6 is 
: 796 1 6 mu
: 797 1 6 scles
- 799
: 799 1 6 the 
: 800 1 6 per
: 801 1 6 cus
: 802 1 6 sion 
: 803 1 6 he 
: 804 1 6 pro
: 805 2 6 vides
- 808
: 808 1 6 for 
: 809 1 6 me 
: 810 1 6 I 
: 811 1 6 say 
: 812 2 6 God
- 816
: 816 1 6 y'all 
: 817 1 6 can 
: 818 1 6 see 
: 819 1 6 me 
: 820 1 6 now
: 821 1 6  'Cos
- 823
: 823 1 6 you 
: 824 1 6 don't 
: 825 1 6 see
: 826 1 6  with 
: 827 1 6 your 
: 828 1 6 eye
- 830
: 830 1 6 you 
: 831 1 6 per
: 832 1 6 ceive 
: 833 1 6 with 
: 834 1 6 your 
: 835 2 6 mind
- 839
: 839 1 6 that's 
: 840 1 6 the 
: 841 1 6 end 
: 842 1 6 of 
- 844
: 844 1 6 it
: 845 1 6  so 
: 846 1 6 I'm 
: 847 1 6 gonna 
: 848 1 6 stick 
: 849 1 6 a
: 850 1 6 round
- 852
: 852 1 6 with 
: 853 1 6 Russ 
: 854 1 6 and 
: 855 1 6 be 
: 856 2 6 men
: 858 2 6 tor
- 861
: 861 1 6 Bust 
: 862 1 6 a 
: 863 1 6 few 
: 864 1 6 rounds
- 866
: 866 1 6 of 
: 867 1 6 mo
: 868 1 6 ther
: 869 1 6 fuc
: 870 1 6 kers
: 871 1 6  re
: 872 2 6 mem
: 874 1 6 ber 
- 876
: 876 1 6 but 
: 877 1 6 the 
: 878 1 6 thou
: 879 1 6 ght 
: 880 1 6 is
- 882
: 882 1 6 I 
: 883 1 6 brou
: 884 1 6 ght 
: 885 2 6 all 
: 887 1 6 this 
- 889
: 889 1 6 so
: 890 1 6  you 
: 891 1 6 can 
: 892 1 6 sur
: 893 1 6 vi
: 894 1 6 ve
- 896
: 896 1 6 when 
: 897 2 6 law 
: 899 1 6 is 
: 900 4 6 lawless
- 906
: 906 1 6 Fear
: 907 1 6 less 
: 908 1 6 sen
: 909 1 6 sa
: 910 1 6 tions
- 912
: 912 1 6 that 
: 913 1 6 you 
: 914 1 6 thou
: 915 1 6 ght 
: 916 1 6 was 
: 917 2 6 dead
- 920
: 920 1 6 no 
: 921 1 6 squea
: 922 1 6 ling 
: 923 1 6 re
: 924 1 6 mem
: 925 1 6 ber
- 927
: 927 1 6 that 
: 928 1 6 it's 
: 929 1 6 all 
: 930 1 6 in 
: 931 1 6 your 
: 932 2 6 head
- 935
: 935 2 15 I 
: 937 3 15 hap
: 940 2 10 py
- 943
: 943 2 3 I'm 
: 945 2 3 feel
: 947 2 6 ing 
: 949 2 10 glad
: 951 1 9  I 
: 952 2 10 got 
: 954 2 15 sun
: 956 4 10 shine 
- 962
: 962 1 3 in 
: 963 2 6 a 
: 965 3 10 bag
: 968 2 10  I'm 
: 970 2 10 use
: 972 4 10 less
- 977
: 977 1 6 but 
: 978 2 8 not 
: 980 1 8 for 
: 981 3 8 long
: 984 2 8  the 
: 986 2 6 fu
: 988 3 3 ture 
- 992
: 992 2 3 is 
: 994 2 1 com
: 996 1 3 ing 
: 997 2 3 on
: 999 3 15  I 
: 1002 2 15 hap
: 1004 3 10 py
- 1008
: 1008 2 3 I'm 
: 1010 1 3 feel
: 1011 2 6 ing 
: 1013 2 10 glad
: 1015 1 9  I 
: 1016 2 10 got 
: 1018 2 15 sun
: 1020 4 10 shine 
- 1026
: 1027 1 3 in 
: 1028 2 6 a 
: 1030 2 10 bag
: 1032 3 10  I'm 
: 1035 2 10 use
: 1037 3 10 less
- 1041
: 1042 1 6 but 
: 1043 2 8 not 
: 1045 1 8 for 
: 1046 3 8 long
: 1049 1 8  the 
: 1050 2 6 fu
: 1052 4 3 ture 
- 1057
: 1057 2 3 is 
: 1059 2 1 com
: 1061 1 3 ing 
: 1062 3 3 on
: 1065 2 3  it's 
: 1067 2 1 com
: 1069 1 3 ing 
: 1070 3 3 on
- 1074
: 1074 1 3 it's 
: 1075 2 1 com
: 1077 1 3 ing 
: 1078 3 3 on
: 1081 2 3  it's 
: 1083 2 1 com
: 1085 1 3 ing 
: 1086 3 3 on
- 1090
: 1090 1 3 it's 
: 1091 2 1 com
: 1093 1 3 ing 
: 1094 3 3 on
: 1098 1 8  my 
: 1099 2 6 fu
: 1101 4 3 ture 
- 1106
: 1106 1 3 it's 
: 1107 2 1 com
: 1109 1 3 ing 
: 1110 3 3 on
: 1114 2 3  it's 
: 1116 2 1 com
: 1118 1 3 ing 
: 1119 3 3 on
- 1123
: 1123 1 3 it's 
: 1124 2 1 com
: 1126 1 3 ing 
: 1127 3 3 on
: 1130 1 8  my 
: 1132 2 6 fu
: 1134 4 3 ture
- 1138
: 1139 1 3 it's 
: 1140 2 1 com
: 1142 1 3 ing 
: 1143 3 3 on
: 1146 2 3  it's 
: 1148 2 1 com
: 1150 1 3 ing 
: 1151 3 3 on
- 1155
: 1155 1 3 it's 
: 1156 2 1 com
: 1158 1 3 ing 
: 1159 3 3 on
: 1163 1 8  my 
: 1164 2 6 fu
: 1166 4 3 ture
E