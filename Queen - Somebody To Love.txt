#TITLE:Somebody To Love
#ARTIST:Queen
#LANGUAGE:English
#EDITION:SingStar Queen
#GENRE:Rock
#YEAR:1976
#MP3:Queen - Somebody To Love.mp3
#COVER:Queen - Somebody To Love [CO].jpg
#BACKGROUND:Queen - Somebody To Love [BG].jpg
#BPM:219,2
#GAP:3010,95
: 0 4 66 Can
: 4 5 67 ~
: 9 8 68 ~
- 19 20
: 37 7 68 A
: 44 4 70 ny
: 49 6 72 bo
: 55 31 75 dy
- 88 89
: 106 9 72 Find
: 116 3 70  me
: 119 3 68 ~
: 122 3 65 ~
: 125 17 63 ~
- 144 145
: 168 5 68 Some
: 176 3 68 bo
: 182 4 68 dy
: 189 5 72  to
: 194 16 70 ~
: 213 35 56  love?
- 250 290
: 310 4 72 Ooh
: 315 4 68 ~
: 319 3 70 ~
: 323 4 65 ~
: 327 12 68 ~
- 341
: 348 4 65 Ooh
: 352 4 63 ~
: 357 3 60 ~
: 361 5 58 ~
- 368
: 373 2 63 Each
: 377 2 63  morn
: 381 2 63 ing
: 385 2 63  I
: 389 2 63  get
: 393 2 63  up
- 396
: 397 2 63 I
: 401 2 60  die
: 403 2 58  a
: 405 3 56  lit
: 409 3 56 tle
- 414
: 421 4 68 Can
: 426 3 68  bare
: 430 2 68 ly
* 434 5 70  stand
: 441 2 70  on
: 443 2 70  my
: 447 2 67  feet
: 449 3 65 ~
: 452 8 63 ~
- 462
: 465 2 63 Take
: 469 2 61  a
: 472 4 60  look
: 476 3 61 ~
: 479 4 62 ~
: 483 5 63 ~
- 489
: 489 4 61 In
: 494 2 60  the
: 496 4 58  mir
: 500 2 56 ror
: 502 6 60  and
: 509 8 56  cry
- 519
: 526 4 58 Lord
: 532 1 58  what
: 535 2 58  you
: 537 1 60  do
: 539 1 60 ing
: 541 2 60  to
: 544 9 58  me?
- 555
: 561 4 58 I
: 565 3 60  have
: 569 2 60  spent
: 572 2 60  all
: 576 3 62  my
: 581 3 62  years
- 585
: 585 3 62 In
: 589 2 63  be
: 593 2 63 lie
: 597 2 63 ving
: 601 3 63  you
- 606
: 610 1 58 But
: 612 4 63  I
: 618 6 63  just
: 626 2 65  can't
: 630 2 65  get
: 634 2 65  no
: 636 2 65  re
* 638 13 67 lief
- 652
: 653 9 68 Lord
- 664
: 666 2 60 Some
: 670 2 58 bo
: 674 4 56 dy
- 680
: 682 7 61 Ooh
: 690 3 60  some
: 694 2 58 bo
: 698 2 56 dy
- 702
: 709 3 61 Can
: 714 5 60  a
: 722 3 60 ny
: 726 7 61 bo
: 734 3 61 dy
: 738 10 63  find
: 750 17 68  me
- 768
: 769 2 60 Some
: 773 2 58 bo
: 777 1 58 dy
: 779 2 58  to
: 783 3 58  love?
: 786 23 56 ~
- 811 812
: 839 5 56 Yeah
- 846 878
: 898 2 60 I
: 902 3 58  work
: 906 4 56  hard
- 912 913
: 922 5 60 E
: 929 2 60 very
: 931 2 58  day
: 934 2 58  of
: 937 2 58  my
* 940 5 56  life
- 947
: 950 4 60 I
: 956 3 60  work
: 961 2 60  till
: 963 6 62  I
: 970 2 62  ache
: 974 3 63  my
: 978 10 63  bones
- 990
: 994 3 60 At
: 998 3 58  the
: 1002 7 56  end
- 1011 1012
: 1030 2 60 I
: 1034 2 60  take
: 1038 5 56  home
- 1044
: 1046 3 58 My
: 1050 3 58  hard
: 1054 2 58  earned
: 1058 6 58  pay
: 1066 9 70  all
: 1075 3 67  on
: 1079 2 65  my
: 1081 10 63  own
- 1092
: 1092 2 58 I
: 1094 3 60  get
: 1097 3 60  down
: 1104 2 60  on
: 1106 2 62  my
: 1110 3 62  knees
- 1114
: 1114 2 62 And
: 1116 8 63  I
: 1126 2 63  start
: 1129 2 60  to
: 1131 2 63  pray
: 1133 2 60 ~
: 1135 2 58 ~
- 1139
: 1141 2 63 Till
: 1144 3 63  the
: 1148 4 63  tears
: 1154 2 65  run
: 1158 4 65  down
: 1164 2 65  from
: 1166 2 67  my
: 1170 9 67  eyes
- 1180
* 1181 8 68 Lord
- 1191
: 1193 3 60 Some
: 1197 2 58 bo
: 1201 4 56 dy
- 1207
: 1209 7 61 Ooh
: 1219 4 60  some
: 1223 5 65 bo
: 1228 3 68 dy
- 1233
: 1237 2 61 Can
: 1241 5 60  a
: 1249 3 60 ny
: 1253 5 61 bo
: 1261 3 61 dy
: 1265 8 63  find
* 1277 12 68  me
- 1291
: 1297 2 68 Some
: 1301 3 68 bo
: 1305 3 65 dy
: 1308 2 67  to
: 1310 54 68  love?
: 1364 11 70 ~
: 1375 17 68 ~
: 1392 8 65 ~
- 1402 1403
: 1420 4 65 E
: 1424 6 63 ~
: 1430 2 61 very
: 1432 12 61 day
- 1446
: 1448 8 61 I
: 1457 2 65  try
: 1461 2 65  and
: 1464 2 65  I
: 1468 2 65  try
: 1472 2 65  and
: 1476 2 65  I
: 1481 12 68  try
- 1495
: 1499 2 65 But
: 1501 17 70  e
: 1518 2 68 very
: 1521 2 65 bo
: 1523 1 63 dy
- 1525
: 1525 2 65 Wants
: 1527 1 63  to
: 1529 2 63  put
: 1532 1 61  me
: 1534 7 61  down
- 1542
: 1544 2 61 They
: 1548 2 63  say
: 1556 3 61  I'm
: 1559 2 63  go
: 1561 2 63 in'
: 1569 3 68  cra
: 1572 7 66 ~
: 1580 9 68 zy
- 1591 1592
: 1602 2 65 They
: 1605 1 65  say
: 1607 1 65  I
: 1609 1 65  got
- 1611
: 1611 1 65 A
: 1613 1 67  lot
: 1614 1 65  of
: 1616 1 67  wa
: 1617 1 65 ter
: 1619 1 67  in
: 1620 3 65  my
: 1624 8 70  brain
- 1634
: 1642 5 67 Oh
: 1648 1 67  I
: 1650 1 67  got
: 1653 1 67  no
: 1655 1 67  com
: 1657 2 65 mon
: 1660 1 65  sense
- 1663
: 1667 1 65 I
: 1669 2 67  got
: 1673 5 67  no
: 1679 1 65 bo
: 1681 2 65 dy
: 1685 4 70  left
- 1690
: 1691 1 70 To
: 1693 3 70  be
: 1697 2 67 lieve
: 1699 2 65 ~
: 1701 3 63 ~
: 1704 2 65 ~
: 1707 2 63 ~
: 1709 6 67 ~
: 1715 3 70 ~
: 1718 2 72 ~
: 1720 2 70 ~
: 1723 10 75 ~
- 1735 2038
: 2058 8 68 Ooh
: 2069 3 65  some
: 2074 4 65 bo
: 2078 4 68 dy
- 2084
: 2088 4 68 Ooh
: 2092 4 63 ~
- 2098 2099
: 2107 5 60 A
: 2114 3 60 ny
: 2119 5 61 bo
: 2126 3 61 dy
: 2130 9 63  find
: 2142 16 68  me
- 2160
: 2163 6 72 Some
: 2169 14 70 ~
: 2187 3 68 bo
: 2190 2 65 dy
: 2192 2 63  to
* 2195 29 68  love?
: 2224 4 65 ~
: 2228 5 63 ~
: 2233 4 60 ~
: 2237 4 56 ~
: 2241 3 58 ~
: 2244 2 53 ~
: 2246 10 56 ~
- 2258 2269
: 2289 3 60 Got
: 2292 2 61  no
: 2296 3 61  feel
: 2299 3 62 ~
: 2302 7 63 ~
- 2310
: 2311 2 63 I
: 2314 2 61  got
: 2317 2 60  no
: 2320 3 58  rhy
: 2324 5 56 thm
- 2331
: 2335 7 59 I
: 2342 4 60 ~
: 2348 3 60  just
: 2352 3 60  keep
: 2356 2 62  lo
: 2360 2 60 sing
: 2364 3 58  my
: 2367 14 63  beat
- 2383
: 2385 5 60 I'm
: 2391 3 58  O
: 2395 3 56 K
- 2399
: 2399 3 60 I'm
: 2403 3 58  al
: 2407 5 56 right
- 2414 2415
: 2429 5 57 I
: 2435 4 58  ain't
* 2440 1 58  gon
: 2442 1 58 na
: 2444 12 70  face
: 2460 2 67  no
: 2462 3 65  de
: 2465 9 63 feat
- 2476
: 2478 2 60 I
: 2482 2 60  just
: 2486 1 60  got
: 2488 1 60 ta
: 2490 1 60  get
: 2492 7 62  out
- 2500
: 2502 2 62 Of
: 2506 2 63  this
: 2510 2 63  pri
: 2514 2 63 son
: 2518 3 63  cell
- 2523
: 2529 2 63 Some
: 2533 6 63 day
: 2541 2 65  I'm
: 2545 2 65  gon
: 2547 1 65 na
: 2549 2 65  be
: 2553 13 67  free
- 2567
* 2569 53 68 Lord
- 2623
: 2624 2 44 Find
: 2635 2 44  me
: 2642 2 44  some
: 2646 3 44 bo
: 2650 3 43 dy
: 2654 3 44  to
: 2658 7 48  love
: 2665 3 46 ~
- 2669
: 2669 3 44 Find
: 2681 2 44  me
: 2688 2 44  some
: 2692 3 44 bo
: 2696 3 43 dy
: 2700 2 44  to
: 2704 8 48  love
: 2712 2 46 ~
- 2715
: 2716 2 44 Find
: 2727 2 44  me
: 2734 2 44  some
: 2738 3 44 bo
: 2742 2 43 dy
: 2746 3 44  to
: 2750 6 48  love
: 2758 3 46 ~
- 2762
: 2762 3 44 Find
: 2773 2 44  me
: 2780 2 44  some
: 2784 3 44 bo
: 2788 2 43 dy
: 2792 2 44  to
: 2795 7 48  love
: 2803 3 46 ~
- 2807
: 2807 3 44 Find
: 2818 3 44  me
: 2826 2 44  some
: 2830 3 44 bo
: 2834 2 43 dy
: 2837 3 44  to
: 2842 6 48  love
: 2849 3 46 ~
- 2853
: 2853 3 44 Find
: 2865 3 44  me
: 2872 2 44  some
: 2876 3 44 bo
: 2880 2 43 dy
: 2884 2 44  to
: 2888 5 48  love
: 2895 2 46 ~
- 2898
: 2899 3 44 Find
: 2910 2 44  me
: 2918 2 44  some
: 2922 2 44 bo
: 2926 2 43 dy
: 2929 2 44  to
: 2933 6 48  love
: 2940 2 46 ~
- 2943
: 2945 3 44 Find
: 2956 2 44  me
: 2963 2 44  some
: 2967 2 44 bo
: 2971 2 43 dy
: 2975 2 44  to
: 2979 7 48  love
: 2986 3 46 ~
- 2990
: 2990 6 48 Woah
: 2997 2 46 ~
: 3001 7 48  woah
: 3008 3 46 ~
- 3012
: 3013 2 44 Find
: 3023 3 44  me
: 3031 2 44  some
: 3035 3 44 bo
: 3039 3 43 dy
: 3043 2 44  to
: 3047 6 48  love
: 3054 2 46 ~
- 3057
: 3059 3 44 Find
: 3069 3 44  me
: 3076 2 44  some
: 3081 3 44 bo
: 3084 2 43 dy
: 3088 3 44  to
: 3092 5 48  love
: 3099 2 46 ~
- 3102
: 3103 1 44 Some
: 3107 2 44 bo
: 3111 2 44 dy
- 3114
: 3115 2 56 Some
: 3118 1 56 bo
: 3122 2 56 dy
- 3125
: 3126 2 44 Some
: 3130 2 44 bo
: 3133 2 44 dy
- 3136
: 3137 2 56 Some
: 3141 2 56 bo
: 3145 2 56 dy
- 3148
: 3149 2 44 Some
: 3153 2 44 bo
: 3156 2 44 dy
: 3160 7 63  find
: 3168 2 61  me
- 3171
: 3172 2 60 Some
: 3175 2 60 bo
: 3179 2 60 dy
: 3183 2 60  find
: 3187 2 58  me
- 3190
: 3191 2 56 Some
: 3194 2 60 bo
: 3198 2 61 dy
: 3202 2 63  to
: 3206 6 68  love
- 3213
: 3213 3 68 Can
: 3218 5 60  a
: 3225 3 60 ny
: 3230 6 61 bo
: 3237 3 61 dy
: 3241 9 63  find
: 3253 43 68  me
- 3298 3299
: 3311 6 72 Some
: 3319 4 72 bo
: 3323 4 70 dy
: 3328 4 73  to
: 3332 10 74 ~
* 3342 34 75 ~
- 3378
: 3386 7 70 Love?
: 3393 4 68 ~
: 3397 3 65 ~
: 3400 2 63 ~
: 3402 3 60 ~
: 3405 2 58 ~
: 3407 3 56 ~
: 3410 2 53 ~
: 3413 3 51 ~
: 3416 5 58 ~
: 3421 31 56 ~
- 3454 3455
: 3480 5 68 Find
: 3487 2 68  me
- 3491 3492
: 3504 4 65 Some
: 3511 5 68 bo
: 3519 3 70 dy
: 3528 7 72  to
: 3535 4 70 ~
: 3540 7 72 ~
: 3547 4 70 ~
: 3552 6 68  love
- 3560 3561
: 3576 5 68 Find
: 3583 2 68  me
- 3587 3588
: 3600 5 65 Some
: 3607 6 68 bo
: 3615 3 70 dy
: 3623 8 72  to
: 3631 4 70 ~
: 3636 7 72 ~
: 3643 5 70 ~
: 3648 5 68  love
- 3655 3656
: 3672 5 68 Find
: 3679 2 68  me
- 3683 3684
: 3696 5 65 Some
: 3703 5 68 bo
: 3711 2 70 dy
: 3719 8 72  to
: 3727 4 70 ~
: 3732 8 72 ~
: 3740 4 70 ~
: 3744 11 68  love
- 3757 3758
: 3768 5 68 Find
: 3775 2 68  me
- 3779 3780
: 3791 6 65 Some
: 3799 7 68 bo
: 3807 3 70 dy
: 3816 7 72  to
: 3824 3 70 ~
: 3828 7 72 ~
: 3835 4 70 ~
: 3839 6 68  love
- 3847 3848
* 3864 4 68 Find
: 3871 2 68  me
- 3875 3876
: 3887 5 65 Some
: 3895 7 68 bo
: 3903 4 70 dy
: 3911 8 72  to
: 3919 3 70 ~
: 3924 7 72 ~
: 3931 4 70 ~
: 3935 7 68  love
- 3944 3945
: 3960 5 68 Find
: 3967 2 68  me
- 3971 3972
: 3984 6 65 Some
: 3991 6 68 bo
: 3999 3 70 dy
: 4008 7 72  to
: 4016 3 70 ~
: 4020 7 72 ~
: 4027 3 70 ~
: 4031 6 68  love
- 4039 4040
: 4055 5 68 Find
: 4062 3 68  me
- 4067 4068
: 4079 4 65 Some
: 4086 6 68 bo
: 4094 3 70 dy
: 4103 7 72  to
: 4110 4 70 ~
: 4115 8 72 ~
: 4123 6 70 ~
: 4129 32 68  love
- 4163
: 4165 3 65 Find
: 4170 3 65  me
: 4175 4 63  find
- 4181 4182
: 4190 5 60 Find
E
