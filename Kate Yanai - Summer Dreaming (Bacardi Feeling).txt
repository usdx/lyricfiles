#TITLE:Summer Dreaming (Bacardi Feeling)
#ARTIST:Kate Yanai
#MP3:Kate Yanai - Summer Dreaming (Bacardi Feeling).mp3
#COVER:Kate Yanai - Summer Dreaming (Bacardi Feeling) [CO].jpg
#BACKGROUND:Kate Yanai - Summer Dreaming (Bacardi Feeling) [BG].jpg
#BPM:356,1
#GAP:10860
#ENCODING:UTF8
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
: 0 3 2 Come
: 4 6 4  on
: 12 6 4  o
: 20 3 4 ver,
: 28 5 4  have
: 36 4 4  some
: 44 6 4  fun
- 52
: 64 5 2 Dan
: 72 2 0 cing
: 76 4 0  in
: 84 3 0  the
: 92 7 4  mor
: 101 3 2 ning
: 109 5 2  sun
- 116
: 128 2 4 Look
: 132 5 7  in
: 140 4 7 to
: 148 4 7  the
: 156 5 7  bright
: 164 4 7  blue
: 173 6 9  sky
- 181
: 193 2 4 Come
: 197 4 2  and
: 204 4 2  let
: 212 3 0  your
: 220 7 4  spi
: 229 3 2 rit
: 237 7 2  fly
- 246
: 256 3 2 Li
: 260 2 4 vin'
: 264 2 4  it
: 268 4 4  up
: 276 4 4  this
: 284 6 4  brand
: 292 4 4  new
: 301 7 4  day
- 310
: 320 4 2 Sum
: 325 3 0 mer,
: 332 5 0  sun,
: 340 3 0  it's
* 348 9 4  time
: 360 2 2  to
: 364 5 2  play
- 371
: 384 3 4 Do
: 388 5 7 in'
: 396 5 7  things,
: 404 4 7  it
: 412 4 7  feels
: 420 4 7  so
: 429 7 9  good
- 438
: 448 4 4 Get
: 454 4 2  in
: 461 5 2 to
: 472 3 0  e
: 477 4 4 mo
: 484 3 2 tion
- 489
: 496 5 4 What
: 504 4 7  I'm
: 512 22 7  fee
: 540 5 4 lin'
- 547
: 572 2 4 It's
: 576 4 5  ne
: 582 3 5 ver
: 588 4 5  been
: 596 4 5  so
: 604 5 4  ea
: 612 3 2 sy
- 617
: 624 5 4 When
: 632 4 7  I'm
: 640 21 11  drea
* 668 6 12 min'
- 676
: 704 3 5 Sum
: 708 5 5 mer
: 716 7 5  drea
: 724 4 5 min'
: 732 4 4  when
: 740 4 2  you're
: 748 4 2  with
: 756 4 0  me
- 762
: 896 2 2 Just
: 900 4 4  a
: 908 6 4 no
: 916 4 4 ther
: 924 4 4  luc
: 932 4 4 ky
: 940 8 4  day
- 950
: 960 4 2 No
: 966 3 0  one
: 973 3 0  makes
: 981 3 0  me
: 988 6 4  feel
: 996 3 2  this
: 1004 5 2  way
- 1011
: 1024 4 4 Watch
: 1032 3 7  the
: 1037 5 7  waves
: 1044 5 7  and
: 1052 5 7  feel
: 1060 4 7  the
: 1068 8 9  sand
- 1078
: 1088 3 4 Kiss
: 1094 3 2  me
: 1100 9 2  now
: 1112 2 0  and
: 1117 4 4  take
: 1124 5 2  my
: 1132 7 2  hand
- 1141
: 1152 2 4 Hear
: 1156 2 4  all
: 1160 2 4  the
: 1164 5 4  laugh
: 1172 5 4 ter
: 1180 5 4  in
: 1188 3 4  the
: 1196 7 4  street
- 1205
: 1216 6 2 Smi
: 1224 2 0 lin'
: 1228 4 0  in
: 1236 3 0  the
: 1244 6 4  sum
: 1252 3 2 mer
: 1260 7 2  heat
- 1269
* 1280 8 4 Cool
: 1292 5 7  touch
: 1300 2 7  of
: 1304 2 7  your
: 1308 6 7  hand
: 1316 4 7  in
: 1324 7 9  mine
- 1333
: 1348 5 2 Can
: 1356 8 2  be
: 1368 2 0  to
: 1372 4 4 ge
: 1380 4 2 ther
- 1386
: 1392 6 4 What
: 1400 4 7  I'm
: 1408 21 7  fee
: 1436 6 4 lin'
- 1444
: 1468 2 4 It's
: 1472 2 5  ne
: 1476 3 5 ver
: 1484 4 5  been
: 1492 5 5  so
: 1500 5 4  ea
: 1508 3 2 sy
- 1513
: 1520 6 4 When
: 1528 4 7  I'm
: 1536 21 11  drea
* 1564 7 12 min'
- 1573
: 1600 2 5 Sum
: 1604 4 5 mer
: 1612 6 5  drea
: 1620 4 5 min'
: 1628 4 4  when
: 1636 4 2  you're
: 1644 4 2  with
: 1652 7 0  me
- 1661
: 1920 22 9 Drea
: 1948 6 6 min'
- 1956
: 1984 2 7 Ne
: 1988 4 7 ver
: 1996 4 7  been
: 2004 4 7  so
: 2012 4 6  ea
: 2020 6 4 sy
- 2028
: 2178 19 4 Hmmm
: 2208 8 4  Hm
: 2220 6 2 ~
: 2230 3 0 ~mm
- 2235
: 2244 5 4 All
: 2252 2 4  the
: 2256 4 4  peo
: 2262 3 2 ple
: 2268 2 2  they
: 2272 4 2  turn
: 2278 3 0  as
: 2284 2 0  we
: 2288 4 0  walk
: 2294 3 -3  on
* 2302 14 -5  by
- 2318
: 2424 4 7 No
: 2432 2 9  lo
: 2436 4 7 vin'
: 2444 4 7  you
: 2452 4 7  just
: 2460 5 7  feels
: 2468 4 7  so
: 2477 6 9  right
- 2485
: 2496 2 5 Ligh
: 2501 3 5 tin'
: 2508 5 5  up
: 2516 4 7  the
: 2524 6 4  dar
: 2532 5 2 kest
: 2540 4 2  ni
: 2546 3 0 ~ght
- 2551
: 2560 5 4 Boy
: 2568 4 7  turn
: 2574 3 7  up
: 2581 3 7  the
: 2589 5 7  ra
: 2596 5 7 di
: 2604 6 9 o
- 2612
: 2632 4 2 Don't
: 2638 4 2  e
: 2645 4 0 ver
: 2652 5 4  let
: 2660 4 2  me
: 2668 6 2  go
- 2676
: 2688 4 4 All
: 2694 3 4  the
: 2700 6 4  tears
: 2708 5 4  I've
: 2716 9 4  cried
: 2728 2 4  be
: 2732 6 4 fore
- 2740
: 2752 2 5 They
: 2756 5 5  can't
: 2764 5 7  touch
: 2772 4 5  me
: 2780 5 4  a
: 2790 4 4 ny
: 2797 2 2 mo
: 2801 3 0 ~re
- 2806
: 2816 4 4 Now
: 2824 2 7  that
: 2828 5 7  you
: 2836 4 7  are
: 2844 4 7  by
: 2852 4 7  my
: 2860 8 9  side
- 2870
: 2876 2 7 It's
: 2880 4 9  all
: 2886 3 9  I
* 2893 3 11  nee
: 2897 3 9 ~d
: 2905 2 7  to
: 2909 6 7  know
- 2917
: 2928 5 4 What
: 2936 5 7  I'm
: 2944 21 7  fee
: 2973 5 4 lin'
- 2980
: 3004 2 4 It's
: 3008 2 5  ne
: 3012 4 5 ver
: 3020 4 5  been
: 3028 5 5  so
: 3036 4 4  ea
: 3044 3 2 sy
- 3049
: 3056 5 4 When
: 3064 4 7  I'm
: 3072 22 11  drea
* 3101 6 12 min'
- 3109
: 3136 2 5 Sum
: 3140 4 5 mer
: 3148 6 5  drea
: 3156 4 5 min'
: 3164 4 4  when
: 3172 4 2  you're
: 3180 4 2  with
: 3188 4 0  me
- 3194
: 3200 2 6 Come
: 3204 5 6  on
: 3212 5 6  o
: 3220 4 6 ver
- 3226
: 3244 8 6 Have
: 3256 5 9  some
: 3264 5 6  fu
: 3272 2 4 ~
: 3276 4 2 ~n
- 3282
: 3328 2 6 We
: 3332 5 9  can
: 3340 5 9  keep
: 3348 5 9  this
: 3356 6 9  dream
: 3366 3 9  a
* 3374 14 11 live
- 3390
: 3408 7 6 Re
: 3421 5 4 try
: 3429 10 2 ~
- 3441
: 3512 5 9 You're
: 3520 5 9  with
: 3528 8 11  me
- 3538
: 3568 8 11 Yea
: 3580 3 13 ~
: 3586 18 9 ~
: 3614 5 6 ~h
- 3621
: 3637 2 2 And
: 3641 2 4  when
: 3645 2 6  I
: 3649 5 7  see
: 3659 2 6  you
: 3663 5 4  smile
- 3670
: 3696 4 6 See
: 3704 4 6  you
: 3712 5 4  smi
: 3720 4 2 ~le
- 3726
* 3760 15 14 Da
: 3779 3 11 ~y
- 3784
: 3936 12 7 What
: 3952 5 6  I
: 3959 2 4 ~'m
: 3968 23 9  fee
: 3997 6 11 li
: 4007 3 6 ~n'
- 4012
: 4029 2 6 It's
: 4033 3 7  ne
: 4038 3 7 ver
: 4045 3 7  been
: 4052 8 9  so
: 4065 4 6  ea
: 4072 3 4 sy
- 4077
: 4096 22 13 Drea
: 4126 6 14 min'
- 4134
: 4160 4 7 Sum
: 4166 4 7 mer
: 4174 5 7  drea
: 4182 5 9 min'
: 4192 12 6  when
: 4208 6 6  you're
- 4216
: 4225 21 9 Fee
: 4254 6 6 lin'
- 4262
: 4285 2 6 It's
: 4290 3 7  ne
: 4294 3 7 ver
: 4301 8 7  been
: 4313 3 2  so
: 4321 5 6  ea
: 4329 4 6 sy
- 4335
: 4354 24 13 Drea
: 4384 5 14 min'
- 4391
: 4441 2 2 We
: 4445 2 4  can
: 4449 4 6  make
: 4457 4 6  it
: 4465 5 4  ba
: 4473 5 2 by
- 4479
: 4480 23 9 Fee
: 4512 4 6 lin'
- 4518
* 4574 12 14 Hey
: 4594 4 11  yeah
- 4600
: 4610 22 9 Drea
: 4640 5 11 min'
- 4647
: 4656 12 11 Hoo
: 4669 4 11 ~
: 4674 6 11 ~
: 4681 5 11 ~
: 4687 5 11 ~
- 4693
: 4694 2 11 Come
: 4697 4 11  on
: 4704 6 11  o
: 4712 6 11 ver
: 4720 6 11  no
: 4728 4 11 ~
: 4733 5 11 ~w
E