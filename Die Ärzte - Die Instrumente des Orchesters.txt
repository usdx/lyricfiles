#TITLE:Die Instrumente des Orchesters
#ARTIST:Die Ärzte
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
#MP3:Die Ärzte - Die Instrumente des Orchesters.mp3
#COVER:Die Ärzte - Die Instrumente des Orchesters [CO].jpg
#BACKGROUND:Die Ärzte - Die Instrumente des Orchesters [BG].jpg
#BPM:252
#GAP:16340
: 0 2 2 Mit
: 4 4 6  Schlag
: 10 4 6 zeug
: 16 3 2  und
: 21 3 4  Bass,
- 26
: 58 3 4 macht
: 62 3 4  das
: 67 3 4  Mu
: 71 4 6 sik
: 77 4 4 hö
: 83 3 -1 ren
: 88 4 2  Spaß.
- 94
: 105 2 -3 Doch
: 109 3 -1  wenn
: 113 2 2  die
: 117 3 4  Gi
: 121 3 6 tar
: 126 2 6 re
: 130 3 6  spielt,
- 134
: 135 2 6  ist
: 139 4 7  al
: 145 3 6 les
: 151 2 2  zu
: 155 4 4  spät,
- 161
: 189 3 4 weil
: 193 3 4  man
: 197 3 4  dann
: 201 3 6  den
: 205 2 4  Text
- 208
: 210 2 2  nicht
: 214 3 2  mehr
: 218 3 -1  ver
: 222 3 2 steht.
- 227
: 1019 3 2 Wenn
: 1023 3 2  man
: 1027 3 6  Bass
: 1031 2 6  und
: 1035 3 6  Schlag
: 1039 2 2 zeug
: 1044 3 4  hat,
- 1049
: 1082 2 4 klingt
: 1086 2 4  die
: 1090 3 4  Mu
: 1094 2 4 sik
: 1098 3 6  schon
: 1103 3 4  ziem
: 1107 3 -1 lich
: 1111 2 2  satt.
- 1115
: 1153 2 2 Doch
: 1157 3 4  der
: 1162 3 6  Lau
: 1168 3 4 te
: 1174 3 2 ste
: 1179 2 4  ist,
- 1183
: 1208 3 2 na
: 1212 3 4 tür
: 1216 2 4 lich
: 1220 3 4  nach
: 1225 2 6  wie
: 1229 2 4  vor
: 1233 2 2  der
: 1237 3 2  Gi
: 1242 2 -1 tar
: 1245 3 2 rist.
E
