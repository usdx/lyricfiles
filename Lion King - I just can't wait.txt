#TITLE:I just can't wait
#ARTIST:Lion King
#MP3:Lion King - I just can't wait.mp3
#COVER:Lion King - I just can't wait [CO].jpg
#BACKGROUND:Lion King - I just can't wait [BG].jpg
#BPM:98,9
#GAP:24647
#ENCODING:UTF8
: 0 1 62 I'm
: 1 1 62  gon
: 2 2 62 na
: 4 3 64  be
: 7 2 67  a
: 9 2 71  migh
: 11 1 69 ty
: 12 4 67  king,
- 16
: 16 1 62 so
: 17 1 64  e
: 18 2 67 ne
: 20 2 67 mies
: 22 2 69  be
: 24 5 67 ware!
- 30
: 30 2 62 Well,
: 32 2 62  I've
: 34 1 64  ne
: 35 1 67 ver
: 36 3 67  seen
- 39
: 39 1 67 a
: 40 2 70  king
: 42 2 69  of
: 44 3 67  beasts
: 47 2 67  with
- 50
: 51 2 70 quiet
: 53 1 70  so
: 54 1 70  litt
: 55 2 69 le
: 57 6 67  hair.
- 64
: 65 1 62 I'm
: 66 1 62  gon
: 67 2 62 na
: 69 3 64  be
: 72 2 67  the
: 74 3 71  mane
: 77 1 69  e
: 78 3 67 vent
- 81
: 81 1 62 Like
: 82 1 64  no
: 83 2 67  king
: 85 2 67  was
: 87 2 69  be
: 89 4 67 fore.
- 95
: 99 1 62 I'm
: 100 1 64  bru
: 101 2 67 shing
: 103 1 67  up
: 104 2 64  on
: 106 1 70  loo
: 107 2 69 king
: 109 4 67  down,
- 114
: 115 1 67 I'm
: 116 2 71  wor
: 118 2 67 king
: 120 1 67  on
: 121 1 67  my
: 122 5 67  ROAR!
- 127
: 127 2 62 Thus
: 129 1 64  far,
: 130 1 67  a
: 131 1 67  ra
: 132 2 67 ther
: 134 2 67  un
: 136 1 67 in
: 137 3 71 spi
: 140 2 67 ring
: 142 9 69  thing!
- 152
: 154 1 64 Oh,
: 155 2 67  I
: 157 4 70  just
: 161 2 69  ca
: 163 1 67 n't
: 164 4 74  wait
- 169
: 170 1 69 to
: 171 3 67  be
: 174 10 67  king!
- 186
: 203 1 64 No
: 204 1 67  one
: 205 1 67  say
: 206 1 67 ing
: 207 2 69  do
: 209 3 67  this.
- 214
: 220 1 64 No
: 221 1 67  one
: 222 1 67  say
: 223 1 67 ing
: 224 2 71  be
: 226 4 69  there.
- 232
: 235 1 64 No
: 236 1 67  one
: 237 1 67  say
: 238 2 67 ing
: 240 4 71  stop
: 244 4 69  that.
- 249
: 251 1 64 No
: 252 1 67  one
: 253 1 67  say
: 254 1 67 ing
: 255 3 72  see
: 258 13 71  here!
- 272
: 273 2 64 Free
: 275 2 67  to
: 277 2 67  run
: 279 2 64  a
: 281 4 67  round
: 285 2 71  all
: 287 2 67 ~
: 289 3 69  day!
- 294
: 305 2 70 Free
: 307 2 70  to
: 309 2 69  do
: 311 2 67  it
: 313 4 69  all
: 317 2 69  my
: 319 2 67 ~
: 321 7 67  way.
- 330
: 354 1 62 I
: 355 2 62  think
: 357 2 62  it's
: 359 3 67  time,
: 362 1 67  that
: 363 1 71  you
: 364 1 69  and
: 365 3 67  I
- 368
: 368 2 67 ar
: 370 4 71 ranged
: 374 2 69  a
: 376 2 67  heart
: 378 2 69  to
: 380 7 67  heart.
- 387
: 387 2 64 Kings
: 389 2 67  don't
: 391 1 67  need
: 392 2 64  ad
: 394 3 67 vice
: 397 2 69  from
- 400
: 401 1 67 lit
: 402 1 69 tle
: 403 2 71  horn
: 405 1 71  bills
: 406 1 71  for
: 407 1 69  a
: 408 4 67  start.
- 412
: 412 2 62 If
: 414 3 67  this
: 417 2 67  is,
: 419 2 67  where
: 421 2 67  the
: 423 1 69  mo
: 424 2 67 nar
: 426 3 67 chy
- 430
: 430 1 69 is
: 431 2 71  head
: 433 1 71 ed,
: 434 3 71  count
: 437 1 69  me
: 438 5 67  out!
- 444
: 445 2 67 Out
: 447 2 67  of
: 449 2 64  ser
: 451 2 67 vice,
: 453 2 67  out
: 455 1 64  of
: 456 1 67  Af
: 457 1 67 ri
: 458 3 67 ca,
- 462
: 463 2 67 I
: 465 2 71  would
: 467 2 71 n't
: 469 2 71  hang
: 471 1 69  a
: 472 5 67 bout.
- 477
: 477 2 62 This
: 479 4 64  child
: 483 1 67  is
: 484 2 67  get
: 486 2 67 ting
- 489
: 489 3 67 wild
: 492 2 67 ly
: 494 2 71  out
: 496 2 67  of
: 498 10 69  wing.
- 509
: 509 3 64 Oh,
: 512 3 67  I
: 515 4 70  just
: 519 1 69  ca
: 520 2 67 n't
: 522 3 74  wait,
- 525
: 525 1 69 to
: 526 2 67  be
: 528 10 67  king.
- 540
: 625 1 65 Ev'
: 626 1 68 ry
: 627 1 68 bo
: 628 1 68 dy
: 629 3 70  look
: 632 7 68  left,
- 640
: 641 1 65 ev'
: 642 1 68 ry
: 643 1 68 bo
: 644 1 68 dy
: 645 3 72  look
: 648 4 70  right,
- 654
: 656 1 65 Ev'
: 657 1 68 ry
: 658 1 68 where
: 659 1 68  you
: 660 4 72  look,
: 664 4 70  I'm
- 670
: 673 1 68 Stan
: 674 1 68 ding
: 675 1 68  in
: 676 1 68  the
: 677 3 73  spot
: 680 6 72 light!
- 688
: 781 2 62 Oh
: 783 2 63  I
: 785 5 62  just
: 790 2 60  ca
: 792 2 57 n't
: 794 5 60  wait
- 799
: 799 1 63 to
: 800 2 62  be
: 802 11 62  king.
- 814
: 815 2 65 Oh
: 817 2 68  I
: 819 4 71  just
: 823 2 70  ca
: 825 1 68 n't
: 826 4 70  wait
- 830
: 830 1 70 to
: 831 3 68  be
: 834 10 68  king.
- 846
: 848 1 65 Oh
: 849 3 68  I
: 852 3 71  just
: 855 3 70  ca
: 858 1 68 n't
: 859 11 70  wait
- 872
: 887 2 70 to
: 889 2 68  be
: 891 18 68  king.
E