#TITLE:The Bard's Song (In The Forest)
#ARTIST:Blind Guardian
#MP3:Blind Guardian - The Bard's Song (In The Forest).mp3
#VIDEO:Blind Guardian - The Bard's Song (In The Forest).avi
#COVER:Blind Guardian - The Bard's Song (In The Forest) [CO].jpg
#BPM:340
#GAP:19650
#ENCODING:UTF8
#PREVIEWSTART:19,650
#LANGUAGE:Englisch
: 0 16 3 Now
: 20 8 1  you
: 32 16 -1  all
: 52 24 -2  know
- 78
: 96 10 -2 bards
: 112 10 -4  and
: 128 8 -6  their
: 144 20 -4  songs.
- 166
: 180 4 -6 When
: 188 2 3  hou
: 192 4 3 rs
: 200 8 1  have
: 216 4 -1  gone
: 224 12 1  by
- 238
: 260 8 -2 I'll
: 272 8 -4  close
: 288 4 -6  my
: 296 12 -4  eyes.
- 310
: 320 4 -4 In
: 326 2 -4  a
: 332 8 3  world
: 344 12 4  far
: 360 2 6  a
: 368 12 1 way
- 382
: 400 2 -2 we
: 404 8 1  may
: 420 8 3  meet
: 432 4 4  a
: 440 12 3 gain.
- 454
: 472 4 -4 But
: 478 10 3  now
: 492 8 1  hear
: 504 4 -1  my
: 514 12 1  song
- 528
: 540 4 -4 about
: 545 2 -4  the
: 552 8 -2  dawn
: 564 12 -1  of
: 580 4 1  the
: 586 4 4  ni
: 590 6 3 gh
: 596 8 1 t.
- 606
: 612 4 1 Let's
: 624 12 3  sing
: 640 16 1  the
: 660 10 -1  bards'
: 676 12 -2  song.
- 690
: 756 4 -4 To
: 764 24 1 mor
: 792 8 3 ro
: 801 5 -1 w
: 808 6 -4  will
: 818 8 4  take
: 828 4 3  us
: 834 4 1  a
: 842 10 3 way,
- 854
: 868 8 4 fa
: 878 4 3 r
: 884 6 1  from
: 892 12 3  home.
- 906
: 920 8 4 No
: 930 2 3  one
: 936 4 1  will
: 944 12 8  e
: 960 6 8 ver
: 968 12 6  know
: 982 5 4  o~
: 987 5 3 ur
: 996 24 1  names.
- 1022
: 1036 4 -4 But
: 1042 3 -6  the
: 1048 8 -4  bards'
: 1060 6 -1  songs
: 1072 12 -2  will
: 1088 6 -6  re
: 1100 26 -4 main...
- 1128
: 1140 4 -4 To
: 1148 22 1 mor
: 1176 7 3 ro
: 1184 4 -1 w
: 1192 4 -4  will
: 1200 10 4  take
: 1212 2 3  it
: 1216 4 1  a
: 1224 10 3 way,
- 1236
: 1242 2 -1 the
: 1248 10 4  fear
: 1262 2 3  of
: 1266 6 1  to
: 1276 12 3 day.
- 1290
: 1300 4 4 It
: 1310 3 3  will
: 1316 4 1  be
: 1324 52 4  gone
- 1378
: 1384 6 4 due
: 1392 6 3  to
: 1400 6 1  our
: 1408 8 -1  ma
: 1418 4 1 gic
: 1426 4 -2  so
: 1430 6 -1 ~n
: 1436 14 -2 gs.
- 1452
: 2054 2 -2 There's
: 2060 8 -1  on
: 2072 10 1 ly
: 2086 6 3  one
: 2096 12 -2  song
- 2110
: 2134 6 -2 left
: 2158 3 -2  in
: 2164 4 -2  my
: 2172 12 -1  mind.
- 2186
: 2212 10 3 Tales
: 2226 10 4  of
: 2240 6 6  a
: 2248 12 6  brave
: 2264 12 1  man
- 2278
: 2284 2 1 who
: 2288 10 1  lived
: 2304 8 3  far
: 2316 6 4  from
: 2328 12 3  here.
- 2342
: 2354 3 -4 Now
: 2360 2 -2  the
: 2364 8 3  bard
: 2380 10 1  songs
: 2392 8 -1  are
: 2404 10 1  o
: 2416 10 1 ver
- 2428
: 2436 2 -4 and
: 2440 6 -2  it's
: 2456 10 -1  time
: 2470 6 1  to
: 2480 12 -1  leave.
- 2494
: 2518 12 3 No
: 2534 8 4  one
: 2548 6 6  should
: 2558 12 8  ask
: 2572 12 6  you
- 2585
: 2586 4 4 for
: 2591 2 3  the
: 2596 12 1  name
: 2612 10 -1  of
: 2624 8 1  the
: 2634 5 4  o
: 2640 4 3 ~
: 2645 11 1 ne
- 2658
: 2664 4 -4 who
: 2672 12 -1  tells
: 2690 10 -2  the
: 2706 14 -4  sto
: 2724 4 -2 ry.
- 2730
: 2732 4 -4 To
: 2740 24 1 mor
: 2768 9 3 ro
: 2778 5 -1 w
: 2784 4 -4  will
: 2792 12 4  take
: 2806 2 3  us
: 2810 4 1  a
: 2818 10 3 way,
- 2830
: 2842 10 4 fa
: 2854 4 3 ~r
: 2860 4 1  from
: 2868 14 3  home.
- 2884
: 2892 8 4 No
: 2904 2 3  one
: 2908 6 1  will
: 2918 14 8  e
: 2936 4 8 ver
: 2944 13 6  know 
: 2958 4 4 o~
: 2963 7 3 ur
: 2972 26 1  names,
- 3000
: 3014 2 -4 but
: 3019 2 -6  the
: 3024 8 -4  bards'
: 3040 4 -1  songs
: 3048 10 -2  will
: 3064 6 -6  re
: 3072 26 -4 main...
- 3100
: 3116 4 -4 To
: 3122 22 1 mor
: 3148 7 3 ro
: 3158 6 -1 ~w
: 3172 8 4  all
: 3184 2 3  will
: 3188 4 1  be
: 3196 8 3  known
- 3206
: 3212 6 1 and
: 3220 10 4  you're
: 3232 2 3  not
: 3236 4 1  a
: 3244 12 3 lone.
- 3258
: 3262 6 1 So
: 3272 10 4  don't
: 3284 1 3  be
: 3286 4 1  a
: 3292 18 8 fraid
- 3311
: 3312 2 8 in
: 3316 2 8  the
: 3320 12 11  dark
: 3336 4 4  and
: 3346 26 1  cold.
- 3374
: 3388 2 -4 'Cause
: 3393 2 -6  the
: 3396 10 -4  bards'
: 3412 4 -1  songs
: 3420 12 -2  will
: 3438 6 -2  re
: 3448 23 -1 ma
: 3472 23 1 i
: 3496 28 3 n...
- 3526
: 3538 6 -6 They
: 3546 22 -4  all
: 3572 12 1  will
: 3588 4 -2  re
: 3594 3 -1 ma
: 3598 4 -6 i
: 3604 22 -4 n...
- 3628
: 3644 14 -1 In
: 3662 4 -1  my
: 3670 12 -2  thoughts
: 3688 4 -2  and
: 3696 8 -4  in
: 3710 6 -9  my
: 3720 8 -9  dreams,
- 3730
: 3736 4 -2 they're
: 3744 12 -1  al
: 3760 4 -1 ways
: 3768 12 1  in
: 3784 4 3  my
: 3792 26 3  mind.
- 3820
: 3834 4 1 These
: 3842 12 3  songs
: 3860 4 3  of
: 3868 14 1  hobb
: 3884 4 -2 its,
: 3892 14 -1  dwarves
: 3910 4 -4  and
: 3918 10 -4  men
- 3930
: 3934 4 -4 and
: 3942 10 3  elves
: 3958 4 3  come
: 3966 12 6  close
: 3982 4 6  your
: 3990 20 3  eyes.
- 4012
: 4032 2 -4 You
: 4036 2 -2  can
: 4042 30 -1  see
: 4076 46 1  them,
: 4126 78 -2  too...
E