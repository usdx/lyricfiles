#TITLE:Johnny B. Goode
#ARTIST:Chuck Berry
#EDITION:UltraStar 50s
#MP3:Chuck Berry - Johnny B. Goode.mp3
#COVER:Chuck Berry - Johnny B. Goode [CO].jpg
#VIDEO:Chuck Berry - Johnny B. Goode.avi
#VIDEOGAP:-11,6
#BPM:172
#GAP:17200
: 0 3 17 Deep 
: 3 3 17 down 
: 6 1 17 in 
: 7 2 17 Loui
: 9 2 17 si
: 11 2 17 a
: 13 2 17 na 
- 15
: 15 2 15 close 
: 17 2 14 to 
: 19 2 15 New 
: 21 3 14 Or
: 24 8 15 leans, 
- 32
: 33 2 17 Way 
: 35 3 17 back 
: 38 2 17 up 
: 40 2 17 in 
: 42 1 17 the 
: 43 3 17 woods 
- 45
: 45 2 14 a
: 47 3 15 mong 
: 50 1 14 the 
: 51 3 15 e
: 54 3 13 ver
: 57 8 10 greens 
- 65
: 65 2 10 There 
: 67 3 17 stood 
: 70 1 13 a 
: 71 4 15 log 
: 75 3 15 ca
: 78 2 13 bin 
- 79
: 79 3 15 made 
: 82 2 13 of 
: 84 4 15 earth 
: 88 2 13 and 
: 90 6 15 wood, 
- 97
: 98 2 17 Where 
: 100 2 17 lived 
: 102 2 17 a 
: 104 2 17 coun
: 106 2 17 try 
: 108 4 17 boy 
- 112
: 112 4 15 named 
: 116 2 15 John
: 118 2 14 ny 
: 120 3 10 B. 
: 123 7 10 Goode 
- 130
: 130 2 17 Who 
: 132 2 17 ne
: 134 2 14 ver 
: 136 2 15 e
: 138 2 14 ver 
: 140 3 15 learned 
: 143 1 14 to 
- 144
: 144 2 15 read 
: 146 2 14 or 
: 148 3 15 write 
: 151 4 14 so 
: 155 4 16 well, 
- 159
: 159 1 10 But 
: 160 2 14 he 
: 162 2 15 could 
: 164 3 17 play 
: 167 1 17 the 
: 168 3 17 gui
: 171 4 17 tar 
- 175
: 176 4 15 like 
: 180 3 15 ring
: 183 3 14 ing 
: 186 1 10 a 
: 187 5 10 bell. 
- 191
: 191 5 16 Go! 
: 196 4 16 Go, 
- 203
: 211 7 16 Go! 
: 218 2 15 John
: 220 2 14 ny 
: 222 6 17 Go! 
: 228 5 17 Go, 
- 236
: 244 7 16 Go! 
: 251 2 15 John
: 253 2 14 ny 
: 255 5 17 Go! 
: 261 5 17 Go,
- 269
: 278 5 16 Go! 
: 283 3 15 John
: 286 2 14 ny 
: 288 4 17 Go! 
: 294 5 17 Go, 
- 302
: 310 6 16 Go! 
: 316 2 15 John
: 318 2 14 ny 
: 320 4 17 Go! 
: 327 5 17 Go, 
- 335
: 352 3 15 John
: 355 2 14 ny 
: 357 2 10 B. 
: 359 7 10 Goode 
- 369
: 387 2 17 He 
: 389 3 17 used 
: 392 1 17 to 
: 393 3 17 car
: 396 2 17 ry 
: 398 2 17 his 
- 400
: 400 2 14 gui
: 402 4 17 tar 
: 405 3 15 in 
: 408 2 14 a 
: 410 2 15 gun
: 412 3 14 ny 
: 415 5 15 sack  
- 422
: 424 2 17 And 
: 426 3 17 sit 
: 429 1 17 be
: 430 3 17 neath 
: 433 1 14 the 
: 434 4 17 trees 
- 438
: 439 2 15 by 
: 441 1 14 the 
: 442 3 15 rail
: 445 4 13 road 
: 449 3 10 track.  
- 452
: 452 4 17 Oh, 
: 456 2 13 the 
: 458 2 13 eng
: 460 3 13 in
: 463 2 13 eers 
: 465 2 13 would 
: 467 2 13 see 
: 469 2 13 him 
- 470
: 470 3 13 sit
: 473 2 10 ting 
: 475 3 13 in 
: 478 2 10 the 
: 480 8 12 shade,
- 489
: 490 4 17 Strum
: 494 2 17 ming 
: 496 2 17 with 
: 498 1 14 the 
: 499 3 17 rhy
: 502 2 14 thm 
- 504
: 504 2 15 that 
: 506 2 14 the 
: 508 4 15 dri
: 512 3 10 vers 
: 515 5 10 made. 
- 522
: 524 2 17 Peo
: 526 2 14 ple 
: 528 3 15 pas
: 531 2 14 sing 
: 533 3 15 by 
- 537
: 539 1 15 would 
: 540 4 15 stop 
: 544 2 14 and 
: 546 6 16 say
- 551
: 551 4 17 Oh 
: 555 5 17 my 
: 560 2 17 that 
: 562 2 15 ti
: 564 1 14 ny 
: 565 2 15 lit
: 567 2 14 tle 
- 569
: 569 3 15 coun
: 572 2 14 try 
: 574 4 15 boy 
: 578 3 14 could 
: 581 3 10 play! 
- 584
: 584 4 16 Go! 
: 588 5 16 Go,
- 596
: 605 6 16 Go! 
: 611 2 15 John
: 613 2 14 ny 
: 615 5 17 Go! 
: 621 5 17 Go,
- 629
: 638 6 16 Go! 
: 644 2 15 John
: 646 2 14 ny 
: 648 4 17 Go! 
: 655 5 17 Go, 
- 663
: 670 7 16 Go! 
: 677 2 15 John
: 679 2 14 ny 
: 681 5 17 Go! 
: 688 4 17 Go, 
- 695
: 703 7 16 Go! 
: 710 2 15 John
: 712 2 14 ny 
: 714 5 17 Go! 
: 720 5 17 Go, 
- 728
: 747 2 15 John
: 749 2 14 ny 
: 751 3 10 B. 
: 754 7 10 Goode 
- 783
: 1182 3 17 His 
: 1185 2 17 mo
: 1187 2 17 ther 
: 1189 2 17 told 
: 1191 2 17 him 
: 1193 2 17 some
: 1195 3 17 day 
- 1197
: 1197 2 17 you 
: 1199 2 14 will 
: 1201 3 15 be 
: 1204 3 14 a 
: 1207 7 16 man,
- 1214
: 1215 3 10 And 
: 1218 2 17 you 
: 1220 2 14 will 
: 1222 2 17 be 
: 1224 2 14 the 
: 1226 2 17 lead
: 1228 2 14 er 
- 1230
: 1231 2 15 of 
: 1233 1 14 a 
: 1234 3 17 big 
: 1237 4 13 old 
: 1241 8 10 band.  
- 1249
: 1249 4 17 Ma
: 1253 2 15 ny 
: 1255 2 15 peo
: 1257 2 15 ple 
: 1259 2 15 com
: 1261 2 13 ing 
- 1263
: 1263 4 15 from 
: 1267 5 15 miles 
: 1272 2 10 a
: 1274 7 12 round
- 1281
: 1282 2 10 To 
: 1284 2 17 hear 
: 1286 2 14 you 
: 1288 3 15 play 
: 1291 1 14 your 
: 1292 2 15 mu
: 1294 3 14 sic 
- 1296
: 1296 3 15 when 
: 1299 1 14 the 
: 1300 5 15 sun 
: 1305 3 10 go 
: 1308 5 10 down 
- 1314
: 1315 4 17 May
: 1319 2 17 be 
: 1321 4 14 some
: 1325 3 17 day 
- 1328
: 1328 2 14 your 
: 1330 2 15 name 
: 1332 2 14 would 
: 1334 2 15 be 
: 1336 4 14 in 
: 1340 5 16 lights
- 1344
: 1344 3 16 Say
: 1347 3 16 ing 
: 1350 3 17 John
: 1353 2 17 ny 
: 1355 3 17 B. 
- 1357
: 1357 7 15 Goode 
: 1364 2 10 to
: 1366 4 10 night.
- 1369
: 1369 4 16 Go! 
: 1373 7 16 Go! 
- 1381
: 1382 6 16 Go! 
: 1388 2 15 John
: 1390 2 14 ny 
: 1392 4 16 Go! 
- 1399
: 1407 5 16 Go! 
: 1412 4 16 Go!
- 1416
: 1416 6 16 Go! 
: 1422 2 15 John
: 1424 2 14 ny 
: 1426 4 16 Go! 
- 1433
: 1440 5 20 Go! 
: 1445 4 12 Go! 
- 1449
: 1449 6 12 Go! 
: 1455 3 10 John
: 1458 2 10 ny 
: 1460 4 10 Go! 
- 1467
: 1474 4 20 Go! 
: 1478 4 13 Go! 
- 1482
: 1482 7 16 Go! 
: 1489 2 15 John
: 1491 2 15 ny 
: 1493 3 15 Go! 
- 1499
: 1515 9 16 Go! 
- 1527
: 1542 3 15 John
: 1545 2 14 ny 
: 1547 3 10 B. 
: 1550 6 10 Goode 
E
