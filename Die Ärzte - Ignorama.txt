#TITLE:Ignorama
#ARTIST:Die Ärzte
#LANGUAGE:German
#EDITION:[SC]-Songs
#YEAR:1998
#MP3:Die Ärzte - Ignorama.mp3
#COVER:Die Ärzte - Ignorama [CO].jpg
#BACKGROUND:Die Ärzte - Ignorama [BG].jpg
#BPM:294,24
#GAP:10070
: 0 2 2 Tod
: 3 2 4  und
: 8 3 4  Krieg
: 15 2 2  auf
: 18 3 4  der
: 24 2 2  gan
: 28 2 4 zen
: 33 4 4  Welt
- 38
: 39 1 4 Es
: 42 2 4  könnt
: 45 2 2  mir
: 48 3 4  nichts
: 54 3 4  e
: 60 2 4 ga
: 63 4 7 ler
: 70 5 4  sein
- 77 78
: 89 4 4 Im
: 95 3 2  Re
: 99 3 4 gen
: 105 4 4 wald
: 112 2 4  wer
: 115 2 4 den
: 120 2 2  Bäu
: 123 2 4 me
: 126 2 4  ge
: 129 4 4 fällt
- 134
: 135 2 2 Es
: 138 2 4  könnt
: 141 2 2  mir
: 144 3 4  nichts
: 150 3 4  e
: 156 2 4 ga
* 159 3 7 ler
: 165 6 4  sein
- 173 174
: 185 3 4 Del
: 191 5 7 fi
: 201 4 7 ne
: 210 4 7  im
: 216 2 4  Thun
: 219 2 2 fisch
: 222 2 2 sa
: 225 4 2 lat,
- 230
: 231 2 -3 wie
: 234 3 -5  ge
: 240 6 4 mein
- 248
: 254 2 4 Doch
: 257 3 4  es
: 264 4 7  könn
: 272 3 7 te
* 279 5 9  mir
: 287 3 4  nichts
: 293 4 2  e
: 299 3 2 ga
: 303 4 3 ler
: 312 5 4  sein
- 319 331
: 351 2 6 Wenn
: 354 3 7  am
: 360 3 4  Strand
: 364 3 4  der
: 369 3 4  letz
: 376 2 4 te
: 382 3 2  Wal
: 388 3 4  ver
: 393 4 4 west
- 398
: 399 1 4 Es
: 402 2 4  könnt
: 405 2 2  mir
: 408 3 4  nichts
: 414 4 2  e
: 420 2 4 ga
: 424 3 7 ler
: 429 5 4  sein
- 436 437
: 447 2 2 Ham
: 450 3 4  die
: 456 2 4  Back
: 460 3 4 street
: 466 3 4  Boys
: 472 3 4  sich
: 478 3 2  auf
: 483 3 4 ge
: 489 3 4 löst
- 493
: 495 2 2 Es
: 498 2 4  könnt
: 501 2 2  mir
: 504 3 4  nichts
: 511 3 4  e
: 516 2 4 ga
: 520 3 7 ler
: 526 5 4  sein
- 533 534
: 543 2 2 Und
: 546 2 4  ist
: 549 2 4  auch
* 552 7 7  je
: 561 4 7 der
: 570 4 7  Po
: 576 2 4 li
: 579 4 2 ti
: 585 3 2 ker
: 591 5 3  ein
: 600 7 4  Schwein
- 609 610
: 618 3 4 Es
: 624 5 7  könn
: 633 3 7 te
: 639 2 7  mi
: 642 4 9 ~r
: 648 3 4  nichts
: 654 3 2  e
: 660 2 2 ga
: 664 5 3 ler
: 672 9 4  sein
- 683 695
: 715 3 2 Du
: 721 3 4  tanzt
: 727 3 2  mit
: 733 3 4  mir,
: 739 3 2  die
: 745 3 4  Welt
: 750 3 2  ist
: 756 7 4  schön
- 765 796
: 816 2 4 Wenn
: 819 2 2  du
: 822 2 4  mit
: 825 2 2  mir
: 828 2 4  tanzt,
- 831
: 831 2 4 kann
: 834 2 4  es
: 837 2 4  mir
: 840 2 4  gar
: 844 3 2  nicht
: 849 3 -1  bes
: 855 3 2 ser
* 862 7 2  gehn'
- 871 886
: 906 3 2 Du
: 912 3 4  tanzt
: 918 3 4  mit
: 924 4 4  mir,
: 930 3 7  das
: 937 3 4  En
: 943 3 2 de
: 949 8 4  naht
- 959 989
: 1009 3 4 Wenn
: 1014 3 2  du
: 1020 3 4  mit
: 1026 3 7  mir
: 1033 3 4  tanzt,
- 1037
: 1038 4 4 bleibt
: 1044 4 4  mir
: 1050 4 2  die
: 1057 5 4  gan
: 1066 4 2 ze
: 1074 5 -1  Mi
: 1081 4 2 se
: 1090 4 -1 re
: 1098 4 -3  er
: 1105 8 -8 spart
- 1115 1132
: 1152 2 4 6
: 1155 2 4  Mil
: 1158 2 2 lio
: 1161 2 2 nen
: 1164 2 4  Ar
: 1167 2 2 beits
: 1170 2 2 lo
: 1173 2 4 se,
: 1177 2 2  grob
: 1180 3 4  ge
: 1186 3 4 schätzt
- 1190
: 1192 1 2 Es
: 1195 2 4  könnt
: 1198 2 2  mir
: 1201 3 4  nichts
: 1207 4 4  e
: 1213 2 4 ga
* 1217 3 7 ler
: 1223 5 4  sein
- 1230 1231
: 1249 2 4 Mei
: 1252 2 2 ne
: 1255 2 4  Lieb
: 1258 2 2 lings-
: 1261 2 2 Fern
: 1264 2 4 seh
: 1267 2 4 se
: 1270 2 4 rie
: 1273 2 2  ab
: 1277 3 4 ge
: 1283 3 4 setzt
- 1287
: 1288 2 4 Es
: 1291 2 4  könnt
: 1294 2 2  mir
: 1297 3 4  nichts
: 1303 3 4  e
: 1309 2 4 ga
: 1313 3 7 ler
: 1319 5 4  sein
- 1326 1327
: 1336 2 2 Und
: 1339 2 4  wenn
: 1342 2 4  sie
: 1345 4 7  mor
: 1351 3 7 gen
- 1355
: 1356 4 7 das
: 1362 4 7  Ben
: 1368 3 4 zin
: 1374 2 2  wie
: 1377 3 2 der
: 1384 3 3  ver
: 1392 5 4 blei'n
- 1399 1400
: 1410 3 4 Es
: 1417 4 7  könn
: 1426 3 7 te
: 1431 2 7  mi
* 1434 4 9 ~r
: 1441 3 4  nichts
: 1447 3 2  e
: 1453 2 2 ga
: 1456 6 3 ler
: 1465 9 4  sein
- 1476 1487
: 1507 3 2 Du
: 1513 3 4  sitzt
: 1519 3 2  auf
: 1525 3 4  mir,
: 1531 3 2  die
: 1537 3 4  Welt
: 1543 3 2  ist
: 1549 6 4  schön
- 1557 1589
: 1609 2 4 Wenn
: 1612 2 2  du
* 1615 2 -1  auf
: 1618 2 2  mir
: 1621 3 4  sitzt,
- 1624
: 1624 2 4 kann
: 1627 2 4  es
: 1630 2 4  mir
: 1633 2 4  gar
: 1637 3 2  nicht
: 1642 3 -1  bes
: 1648 3 2 ser
: 1654 7 2  gehn'
- 1663 1679
: 1699 3 2 Du
: 1705 3 4  sitzt
: 1711 3 2  auf
: 1717 3 4  mir,
: 1723 3 2  das
: 1730 3 4  En
: 1735 3 2 de
: 1741 8 4  naht
- 1751 1781
: 1801 3 4 Wenn
: 1807 3 2  du
: 1813 3 4  auf
: 1819 3 7  mir
: 1825 3 4  sitzt,
- 1829
: 1831 4 4 bleibt
: 1837 4 4  mir
: 1843 4 2  die
: 1849 6 4  gan
: 1858 6 2 ze
: 1867 3 -1  Mi
: 1874 5 2 se
: 1883 5 -1 re
: 1891 3 -3  er
: 1897 8 -8 spart
- 1907 1955
: 1975 3 -5 ist
: 1981 3 -3  mir
: 1985 4 -1  e
* 1992 9 -1 gal
- 2003 2051
: 2071 3 -5 ist
: 2077 3 -3  mir
: 2081 4 -1  e
: 2088 9 -1 gal
- 2099 2147
: 2167 3 -5 ist
: 2173 2 -3  mir
: 2177 4 -1  e
: 2184 8 -1 gal
- 2194 2205
: 2225 4 4 es
* 2232 3 7  ist
: 2238 4 4  mir
: 2245 2 4  so
: 2248 4 2  e
: 2254 12 4 gal
- 2268
: 2274 3 2 Du
: 2280 3 4  sitzt
: 2286 3 2  auf
: 2292 3 4  mir,
: 2298 3 2  die
: 2304 3 4  Welt
: 2310 3 2  ist
: 2316 7 4  schön
- 2325 2356
* 2376 2 4 Wenn
: 2379 2 4  du
: 2382 2 4  auf
: 2385 2 2  mir
: 2388 2 4  sitzt,
- 2391
: 2391 2 4 kann
: 2394 2 4  es
: 2397 2 4  mir
: 2400 2 4  gar
: 2404 3 2  nicht
: 2409 3 -1  bes
: 2415 3 2 ser
: 2422 7 2  gehn'
- 2431 2447
: 2467 3 2 Du
: 2473 3 4  sitzt
: 2479 3 2  auf
: 2485 3 4  mir,
: 2491 3 2  das
: 2497 3 4  En
: 2503 3 2 de
: 2509 7 4  naht
- 2518 2548
: 2568 4 4 Wenn
: 2575 3 2  du
: 2581 3 4  auf
: 2587 3 2  mir
: 2593 3 4  sit
: 2599 3 2 zen
: 2605 4 4  bleibst,
- 2610
: 2611 3 2 ver
: 2617 3 2 sprech
: 2623 3 -1  ich,
: 2629 3 -1  ich
: 2635 3 2  bleib
: 2641 6 2  hart
- 2649 2650
: 2658 3 2 du
* 2665 13 7  schaust
: 2682 4 7  mich
: 2689 4 2  an
- 2694
: 2694 3 2 und
: 2700 2 2  bist
: 2704 4 3  ent
: 2713 5 4 setzt
- 2720 2721
: 2731 3 4 so
: 2737 3 7  hät
: 2743 3 7 test
: 2749 4 7  du
: 2755 3 9  mich
- 2759
: 2760 4 4 nie
: 2767 4 2 mals
: 2773 3 2  ein
: 2777 4 3 ge
: 2785 4 4 schätzt
- 2791 2792
: 2803 3 4 die
: 2809 4 7  Welt
: 2815 3 7  ist
: 2821 3 7  mir
* 2827 3 9  e
: 2833 4 2 gal
- 2838
: 2839 4 2 wenn
: 2845 4 2  du
: 2851 4 3  dich
: 2857 5 4  auf
: 2866 4 4  mich
: 2876 4 4  setzt
- 2882 2883
: 2906 5 4 auf
: 2915 5 4  mich
: 2924 5 4  setzt
- 2931 2932
: 2954 5 4 auf
: 2963 5 4  dich
: 2972 5 4  setzt
- 2979 2980
: 3002 7 4 sitz!
E
