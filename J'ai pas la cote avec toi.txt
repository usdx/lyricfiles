#TITLE:J'ai pas la cote avec toi
#ARTIST:Dick Rivers
#MP3:Dick RIVERS - J'ai pas la cote avec toi.mp3
#COVER:J'ai pas la cote avec toi.jpg
#BACKGROUND:J'ai pas la cote avec toi.jpg
#BPM:246
#GAP:19230
#ENCODING:UTF8
: 0 1 2 Me
: 4 3 2  voi
: 8 5 2 la
: 16 3 0  en
: 20 1 2 core
: 24 5 2  fait
- 30
: 32 1 2 Comme
: 36 1 0  un
: 40 7 -1  rat
- 57
: 64 1 -1 Y'a
: 68 1 -1  trop
: 72 5 2  d'bruit
: 82 1 2  de
: 86 3 0  fu
: 90 5 2 mée
- 96
: 98 1 0 Dans
: 102 3 0  c't'en
: 106 7 -1 droit
- 123
: 130 2 -1 J'vois
: 134 2 -1  des
: 138 6 5  femmes
- 146
: 148 2 5 Meme
: 152 2 5  la
: 156 2 0  ou
: 160 2 2  y
: 164 2 0  en
: 168 2 2  a
: 172 8 2  pas
- 189
: 192 2 0 A
: 196 4 5  cet'heure-
: 202 4 2 ci
- 210
: 212 2 -1 Tout
: 216 2 2  le
: 220 2 2  monde
: 224 2 2  est
: 228 2 0  beau,
: 232 4 0  pour
: 238 6 -1 quoi
- 256
: 260 9 -1 Ah
: 272 1 -1  tu
: 274 1 2  tor
: 276 1 2 tilles
: 280 1 0  de
: 286 7 2  partout
- 299
: 302 3 0 T'es
: 310 1 -3  jo
: 314 1 5 lie
: 316 1 5  comme
: 318 3 5  tout
- 324
: 326 1 2 Mais
: 330 1 5  voi
: 334 3 5 la
- 340
: 342 1 0 J'ai
: 346 1 2  pas
: 350 1 2  la
: 354 1 2  cote
: 358 1 4  a
: 362 1 4 vec
: 366 5 2  toi
- 381
: 402 2 2 J'ai
: 406 2 0  pas
: 410 2 2  la
: 414 2 2  cote
: 418 2 0  a
: 422 2 0 vec
: 426 6 -1  toi
- 444
: 448 2 -1 J'ai
: 452 2 -1  pas
: 456 6 2  grand
: 464 2 -1  chose
- 466
: 468 2 2 Ŕ
: 472 2 2  te
: 476 3 2  don
: 480 2 0 ner
: 484 2 0  ce
: 488 8 -1  soir
- 506
: 518 2 -1 Juste
: 522 2 -1  un
: 526 6 5  p'tit
: 534 2 5  peu
: 541 2 5  de
: 544 2 0  bleu
- 546
: 548 1 2 Dans
: 552 3 0  l're
: 558 7 2 gard
- 576
: 580 1 2 Ma
: 584 1 0  pauv'
: 588 5 2  tete
- 594
: 596 3 -1 A
: 600 1 2 vec
: 608 1 2  ses
: 612 3 2  i
: 618 1 0 dées
: 622 3 2  noi
: 626 7 2 res
- 643
: 652 3 2 Oh
: 664 1 -3  tu
: 666 1 2  me
: 668 1 2  plais
: 672 1 0  de
: 674 7 2  partout
- 684
: 686 5 0 Un
: 694 1 -3  peu
: 698 1 5  trop
: 704 1 5  beau
: 706 3 5 coup
- 710
: 712 1 2 Mais
: 716 3 5  voi
: 720 7 5 la
- 730
: 732 1 0 J'ai
: 736 1 2  pas
: 740 1 2  la
: 744 1 0  cote
: 748 1 0  avec
: 752 7 -1  toi
- 769
: 778 1 -1 Faut
: 782 3 -1  choi
: 786 1 5 sir,
: 790 3 2  fai
: 794 1 7 re
: 798 1 7  tes
: 802 3 7  poe
: 806 1 7 mes
- 808
: 810 1 7 Et
: 814 3 7  ren
: 818 1 7 trer
: 822 3 7  chez-
: 826 1 7 soi
: 830 1 7  quand
: 834 3 7  me-
: 838 7 7 me
- 861
: 906 1 7 Faut
: 910 3 7  choi
: 914 1 7 sir
: 918 3 7  d'ai
: 922 1 7 mer
: 926 3 7  chez-
: 930 1 7 elle
- 932
: 934 5 7 Mais
: 942 7 7  dor
: 950 1 7 mir
: 954 1 7  tout
: 958 1 7  seul
: 962 1 4  quand
: 966 3 2  me-
: 970 5 2 me
- 991
: 1032 1 2 J'ai
: 1036 3 2  cho
: 1040 3 9 i
: 1044 7 5 si
- 1083
: 1232 1 6 Fond
: 1236 1 2  de
: 1240 1 2  poche
- 1242
: 1244 1 0 Et
: 1248 1 2  vieux
: 1252 1 0  billet
: 1256 3 0  frois
: 1260 7 -1 sé
- 1277
: 1294 1 -1 La
: 1296 3 -1  cham
: 1300 1 2 bre
: 1302 7 2  d'hô
: 1310 1 0 tel
- 1312
: 1314 1 2 Au
: 1318 3 2  pa
: 1322 1 2 pier
: 1326 3 0  dé
: 1330 3 0 la
: 1334 7 -1 vé
- 1351
: 1358 1 -1 T'as
: 1362 3 -1  fail
: 1366 5 5 li
: 1374 3 5  t'en
: 1378 3 5 dor
: 1382 1 0 mir
- 1389
: 1392 5 2 Dans
: 1400 1 0  ce
: 1404 7 2  lit
- 1421
: 1428 1 0 Ou
: 1432 1 5  tout
: 1436 5 2  seul
: 1444 1 -1  j'ai
: 1448 1 2  le
: 1452 5 2  corps
- 1458
: 1460 1 2 Qui
: 1464 3 0  s'en
: 1468 7 -1 nuie
- 1485
: 1494 9 -1 Ah
: 1506 1 -3  il
: 1508 1 2  te
: 1510 1 2  voit
: 1514 1 0  de
: 1516 7 2  partout
- 1528
: 1530 5 0 Un
: 1538 1 -3  peu
: 1542 1 5  trop
: 1544 1 5  beau
: 1546 5 5 coup
- 1552
: 1554 1 2 Mais
: 1558 3 5  voi
: 1562 5 5 la
- 1568
: 1570 1 0 J'ai
: 1574 1 2  pas
: 1578 1 2  la
: 1582 1 2  cote
: 1586 3 0  a
: 1590 1 0 vec
: 1594 7 -1  toi
- 1633
: 2010 1 -1 Faut
: 2014 3 -1  choi
: 2018 1 7 sir,
: 2022 3 7  fai
: 2026 1 7 re
: 2030 1 7  tes
: 2034 3 7  poe
: 2038 1 7 mes
- 2040
: 2042 1 7 Et
: 2046 3 7  ren
: 2050 1 7 trer
: 2054 3 7  chez-
: 2058 1 7 soi
: 2062 1 7  quand
: 2066 7 7  meme
- 2089
: 2136 1 7 Faut
: 2140 3 7  choi
: 2144 1 7 sir
: 2148 3 7  d'ai
: 2152 1 7 mer
: 2156 3 7  chez-
: 2160 5 7 elle
- 2166
: 2168 5 7 Mais
: 2176 3 7  dor
: 2180 1 7 mir
: 2184 1 7  tout
: 2188 1 4  seul
: 2192 1 2  quand
: 2196 3 2  meme
- 2231
: 2266 1 6 J'ai
: 2270 3 2  cho
: 2274 3 9 i
: 2278 7 5 si
- 2317
: 2458 1 6 Me
: 2462 3 2  voi
: 2466 1 2 la
: 2480 1 0  encore
: 2484 1 2  fait
- 2486
: 2488 1 0 Comme
: 2492 1 0  un
: 2496 7 -1  rat
- 2513
: 2528 1 -1 Y'a
: 2530 1 -1  trop
: 2534 7 2  d'bruit
: 2544 1 2  de
: 2548 3 0  fu
: 2552 5 2 mée
- 2558
: 2560 1 2 Dans
: 2564 3 0  c't'en
: 2568 7 -1 droit
- 2585
: 2592 1 -1 J'vois
: 2596 1 -1  des
: 2600 5 5  femmes
- 2606
: 2608 1 5 Meme
: 2612 1 5  la
: 2616 1 0  ou
: 2620 1 2  y
: 2624 1 0  en
: 2628 1 2  a
: 2632 7 2  pas
- 2649
: 2656 1 0 A
: 2660 3 5  cet'heure-
: 2664 5 2 ci
- 2672
: 2674 1 -1 Tout
: 2678 1 2  le
: 2682 1 2  monde
: 2686 1 2  est
: 2690 1 0  beau,
: 2694 3 0  pour
: 2698 7 -1 quoi
- 2715
: 2724 9 -1 Ah
: 2736 1 -3  tu
: 2738 1 2  tor
: 2740 1 2 tilles
: 2744 1 0  de
: 2746 7 2  partout
- 2759
: 2762 5 0 T'es
: 2770 3 -3  jo
: 2774 1 5 lie
: 2776 1 5  comme
: 2778 5 5  tout
- 2783
: 2784 1 2 Mais
: 2788 3 5  voi
: 2792 5 5 la
- 2797
: 2798 1 0 J'ai
: 2802 1 2  pas
: 2806 1 2  la
: 2810 1 2  cote
: 2814 3 0  a
: 2818 1 0 vec
: 2822 7 -1  toi
- 2845
: 2866 1 2 J'ai
: 2870 1 -3  pas
: 2874 1 2  la
: 2878 1 2  cote
: 2882 3 0  a
: 2886 1 0 vec
: 2890 7 -1  toi
- 2913
: 2930 1 -1 J'ai
: 2934 1 -1  pas
: 2938 1 2  la
: 2942 1 2  cote
: 2946 3 0  a
: 2950 1 0 vec
: 2954 7 2  toi
- 2977
: 2994 1 2 J'ai
: 2998 1 -3  pas
: 3002 1 2  la
: 3006 1 2  cote
: 3010 3 0  a
: 3014 1 0 vec
: 3018 3 -1  toi
E