#TITLE:Hey Paula
#ARTIST:Paul And Paula
#MP3:Paul And Paula - Hey Paula.mp3
#COVER:Paul And Paula - Hey Paula [CO].jpg
#BACKGROUND:Paul And Paula - Hey Paula [BG].jpg
#BPM:145,34
#GAP:6500
#ENCODING:UTF8
: 0 8 71 (er)Hey
: 8 9 71  Hey
: 17 3 67  Paul
: 20 3 69 a
: 41 3 64  I
: 44 2 66  wanna
: 46 6 71  marry
: 52 12 69  you
- 66
: 68 6 71 Hey
: 74 6 71  Hey
: 80 6 67  Paula
: 102 6 64  no
: 108 4 66  one
: 112 4 69  else
: 116 6 69  could
: 122 4 67  ever
: 126 12 69  do
- 138
: 138 4 62 I've
: 142 4 62  wait
: 146 6 71 ed
: 152 6 66  so
: 158 10 64  long
- 169
: 170 5 62 for
: 175 9 74  school
: 184 3 71  to
: 187 5 76  be
: 192 13 71  through
- 205
: 205 4 69 Paul
: 209 2 71 a
: 214 3 71  I
: 217 4 72  can't
: 221 4 76  wait
: 225 4 72  no
: 229 4 64  more
: 233 7 71 for
: 240 18 69  you.
- 260
: 263 10 64  My
: 273 22 67  love
: 296 9 64  my
: 305 15 67  love
- 321
: 321 11 71 (PAULA)Hey
: 332 16 67  Paul
: 352 8 64  I've
: 360 4 66  been
: 364 4 67  wait
: 368 6 71 ing
: 374 5 69  for
: 379 4 69  you
- 383
: 383 6 71 Hey
: 389 6 71  Hey
: 395 16 71  Hey Paul
: 414 7 64  I
: 421 3 66  want
: 424 2 67  to
: 426 6 69  marr
: 432 6 69 y
: 438 4 67  you
: 442 11 69  too
- 455
: 456 3 62 If
: 459 4 74  you
: 463 7 71  love
: 470 4 67  me
: 474 9 66  true
: 487 4 62  If
: 491 4 74  you
: 495 7 71  love
: 502 3 76  me
: 505 9 76  sweet
- 518
: 522 4 69 Our
: 526 9 71  love
: 535 5 72  will
: 540 5 76  al
: 545 5 72 ways
: 550 4 71  be
: 554 15 69  real
- 572
: 574 15 64 My
: 589 16 67  love
: 608 14 64  my
: 622 16 67  love
- 643
: 650 6 62 (BOTH)True
: 656 6 64  love
: 662 6 66  means
: 668 5 67  plann
: 673 6 69 ing
: 679 5 71  a
: 684 12 69  life
: 696 8 64 for
: 704 13 69  two
- 722
: 726 6 66 Being
: 732 3 67  to
: 735 4 69 geth
: 739 6 66 er
: 745 5 64  the
: 750 12 62  whole
: 762 5 64  day
: 767 11 62  through
- 780
: 781 7 59 True
: 788 7 60  love
: 795 5 62  means
: 800 5 65  wait
: 805 5 64 ing
: 810 6 62 and 
: 816 5 60 hop
: 821 5 62 ing
: 826 5 62  that
: 831 13 64  soon.
- 851
: 855 6 71 Wishes
: 861 6 69  we've
: 867 9 67  made
: 876 8 66  come
: 884 22 67  true
- 906
: 906 10 64 My
: 916 21 67  love
: 940 9 64  My
: 949 15 67  love
- 965
: 966 6 71 (PAUL)Hey
: 972 8 71  hey
: 980 7 67  Paula
: 998 7 64  I've
: 1005 5 67  been
: 1010 8 69  waiting
: 1018 7 69  for
: 1025 5 69  you
- 1030
: 1030 7 71 (PAULA)Hey
: 1037 5 71  Hey
: 1042 14 67  Paul
: 1061 7 64  I
: 1068 4 66  want
: 1072 2 67  to
: 1074 6 69  marr
: 1080 5 71 y
: 1085 5 69  you
: 1090 12 69  too
- 1103
: 1105 7 62 (BOTH)True
: 1112 6 64  love
: 1118 5 66  means
: 1123 5 67  plann
: 1128 7 69 ing
: 1135 5 71  a
: 1140 12 69  life
: 1152 6 64  for
: 1158 15 69 two
- 1176
: 1180 7 66 Being
: 1187 3 67  to
: 1190 4 69 geth
: 1194 6 66 er
: 1200 4 64  the
: 1204 12 62  whole
: 1216 5 64  day
: 1221 10 62  through
- 1234
: 1236 7 59 True
: 1243 6 60  love
: 1249 5 62  means
: 1254 5 65  wait
: 1259 7 64 ing
- 1266
: 1266 10 62 and hop
: 1276 6 60 ing
: 1282 3 62  that
: 1285 16 64  soon
: 1309 9 71 Wishes
: 1318 4 69  we've
: 1322 8 67  made
: 1330 9 66  will come
: 1339 17 69  true
- 1358
: 1362 11 67 My
: 1373 19 67  love,
: 1396 10 67  my
: 1406 21 67  love
E