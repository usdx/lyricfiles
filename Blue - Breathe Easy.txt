#TITLE:Breathe Easy
#ARTIST:Blue
#MP3:Blue - Breathe Easy.mp3
#COVER:Blue - Breathe Easy [CO].jpg
#VIDEO:Blue - Breathe Easy [VD#0].mp4
#BPM:160.05
#GAP:9330
: 0 1 -1 Cruel
: 3 2 -1  to
: 5 2 -3  the
: 7 16 -1  eye
- 25
: 36 4 -1 I
: 40 3 0  see
: 44 3 2  the
: 48 7 2  way
: 55 3 0  he
: 59 8 0  makes
: 68 3 -1  you
: 71 16 -3  smile
- 89
: 96 1 -1 Cruel
: 99 2 -1  to
: 101 2 -3  the
: 103 15 -1  eye
- 120
: 132 4 -1 Wat
: 136 4 0  ching
: 139 3 2  him
: 144 7 2  hold
: 151 3 0  what
: 156 4 -1  used
: 160 4 -3  to
: 164 4 -5  be
: 168 8 -3  mine
- 180
: 180 3 -5 Why
: 184 3 -5  did
: 187 2 -1  I
: 192 6 -1  l
: 197 2 -3  i
: 199 19 -5  e?
- 220
: 228 4 -1 What
: 232 3 0  did
: 235 3 2  I
: 239 8 2  walk
: 247 3 0  a
: 252 8 0  way
: 260 3 -1  to
: 264 2 -1  fi
: 266 2 -3  n
: 268 12 -5  d
- 284
: 284 3 -5 Oooohhh...
: 287 2 -1  W
: 290 2 -3  h
: 292 11 -5  y?
- 305
: 324 6 -5 Ooh...
: 336 24 -5  Why?
- 362
: 384 39 7 I
: 424 4 9 -
: 428 4 7  can’t
: 432 6 6  breath
: 438 2 4  ea
: 440 32 2  sy
- 476
: 476 3 2 Can’t
: 480 4 2  sleep
: 484 4 0  at
: 488 4 -1  ni
: 492 24 0  ght
- 518
: 524 2 0 Til
: 528 4 0  you’re
: 532 4 -1  bymy
: 536 30 -1  side
- 568
: 572 4 2 No
: 576 39 7  I
: 616 4 9 -
: 620 5 7  can’t
: 624 6 6  breath
: 630 2 4  ea
: 632 32 2  sy
- 668
: 668 2 2 Can’t
: 672 4 2  dream
: 676 2 0  yet
: 678 2 -1  a
: 680 2 0  no
: 682 2 -1  ther
: 684 3 0  dream
- 688
: 688 4 0 With
: 692 3 2  out
: 696 4 2  you
: 700 2 0  ly
: 702 2 -1  ing
: 704 2 0  next
: 706 2 -1  to
: 708 3 0  me
- 712
: 712 4 0 There’s
: 716 3 2  no
: 720 32 2  way
- 756
: 756 3 -5 Curse
: 760 4 -5  me
: 764 4 -1  in
: 768 12 -1  si de
- 782
: 800 3 -1 For
: 804 4 -1  e
: 808 4 0  ve
: 812 3 2  ry
: 816 8 2  word
: 823 4 0  that
: 828 3 -1  caused
: 832 3 -3  you
: 835 4 -5  to
: 839 13 -3  cry
- 852
: 852 3 -5 Curse
: 856 4 -5  me
: 860 3 -1  in
: 864 18 -1  side
- 884
: 900 4 -1 I
: 904 3 0  won’t
: 907 3 2  for
: 912 4 2  get
: 916 2 0  no
: 920 4 0  I
: 924 4 -1  won’t
: 928 4 -3  ba
: 932 4 -5  by,
- 936
: 936 10 -3 I
: 952 4 -5  don’t
: 956 2 -1  know
: 960 2 -1  w
: 962 2 -3  h
: 964 17 -5  y
- 983
: 992 2 -1 I
: 996 4 -1  left
: 1000 4 0  the
: 1003 4 2  one
- 1008
: 1008 8 2 I
: 1016 4 0  was
: 1020 4 -1  loo
: 1024 4 -3  king
: 1028 4 -5  to
: 1031 13 -3  find
- 1046
: 1052 8 7 Ooh...
: 1060 23 4  Why?
- 1085
: 1095 46 7 Ooooooh!
- 1143
: 1152 39 7 I
: 1192 4 9 -
: 1196 4 7  can’t
: 1200 6 6  breath
: 1206 2 4  ea
: 1208 29 2  sy
- 1239
: 1244 3 2 Can’t
: 1248 4 2  sleep
: 1252 4 0  at
: 1256 4 -1  ni
: 1260 24 0  ght
- 1286
: 1292 2 0 Til
: 1296 4 0  you’re
: 1300 3 -1  bymy
: 1304 30 -1  side
- 1336
: 1340 4 2 No
: 1344 12 7  I
: 1356 4 9 -
: 1360 4 7 -
: 1364 20 11 -
: 1384 4 9  can’t
: 1388 4 7  breath
: 1392 5 9  ea
: 1397 31 9  sy
- 1430
: 1436 2 2 Can’t
: 1440 4 2  dream
: 1444 2 0  yet
: 1446 2 -1  a
: 1448 2 0  no
: 1450 2 -1  ther
: 1452 3 0  dream
- 1456
: 1456 4 0 With
: 1460 3 2  out
: 1464 4 2  you
: 1468 2 0  ly
: 1470 2 -1  ing
: 1472 2 0  next
: 1474 2 -1  to
: 1476 3 0  me
- 1480
: 1480 4 0 There’s
: 1484 3 2  no
: 1488 36 2  way
- 1526
: 1532 4 7 No
: 1536 32 11  I
: 1568 4 9 -
: 1572 20 7 -
: 1592 4 11 -
: 1600 4 14  can't
: 1604 3 12  breath
: 1608 4 11  ea
: 1612 8 7  sy
- 1622
: 1628 2 2 Can’t
: 1632 4 2  dream
: 1636 2 0  yet
: 1638 2 -1  a
: 1640 2 0  no
: 1642 2 -1  ther
: 1644 3 0  dream
- 1648
: 1648 4 0 With
: 1652 3 2  out
: 1656 4 2  you
: 1660 2 0  ly
: 1662 2 -1  ing
: 1664 2 0  next
: 1666 2 -1  to
: 1668 3 0  me
- 1672
: 1672 4 0 There’s
: 1676 3 2  no
: 1680 24 2  way
- 1706
: 1728 4 2 Out
: 1732 3 0  of
: 1735 4 -1  my
: 1740 13 0  mind
- 1755
: 1764 3 0 No
: 1768 2 0  thing
: 1772 3 0  make
: 1776 8 2  sense
: 1784 2 0  a
: 1786 2 -1  ny
: 1788 16 0  more
- 1806
: 1812 2 0 I
: 1816 2 0  want
: 1820 2 0  you
: 1824 4 4  back
: 1828 4 2  in
: 1832 4 0  my
: 1836 18 2  life
- 1856
: 1860 3 4 That’s
: 1864 4 4  all
: 1868 3 7  I'm
: 1872 8 7  brea
: 1880 3 6  thing
: 1884 23 6  for
- 1908
: 1908 4 9 Oo
: 1912 4 7  oo
: 1916 3 6  oo
: 1920 23 7  oo
: 1944 48 7  hh
: 1992 6 9  hh!
- 2000
: 2087 12 9 Tell
: 2100 11 11  me
: 2112 4 9  wh
: 2116 32 7  y!
- 2150
: 2164 4 2 Oh
: 2168 3 -1  won’t
: 2176 4 2  you
: 2179 4 -1  tell
: 2184 4 4  me
: 2187 8 2  why
- 2197
: 2200 2 -1 I
: 2204 3 -1  can’t
: 2208 4 -3  dream
: 2212 2 -5  yet
: 2214 2 -8  a
: 2216 2 -5  no
: 2218 2 -8  ther
: 2220 4 -5  dream
- 2224
: 2224 4 -3 With
: 2228 4 -1  out
: 2232 3 -3  you
: 2236 2 -5  ly
: 2238 2 -8  ing
: 2240 2 -5  next
: 2242 2 -8  to
: 2244 4 -5  me
- 2248
: 2248 3 -3 There’s
: 2251 4 -1  no
: 2256 16 -3  air
- 2274
: 2280 5 2 No,
: 2286 5 0  no,
: 2292 3 -1  no,
: 2296 3 -3  no,
: 2300 3 -5  no
- 2304
: 2304 20 11 I
: 2323 3 9 -
: 2326 2 7 -
: 2328 2 9 -
: 2330 13 7 -
: 2344 4 11  can’t
: 2348 4 7  breath
: 2352 3 9  ea
: 2355 23 9  sy
- 2380
: 2396 3 2 Can’t
: 2400 4 2  sleep
: 2404 3 0  at
: 2408 28 0  night
- 2438
: 2444 3 7 Til
: 2448 5 12  you’re
: 2464 3 9  by
: 2468 4 11  my
: 2472 3 9  si
: 2476 14 7  de
- 2491
: 2492 4 7 No
: 2496 35 14  I
: 2536 4 11  can’t
: 2540 3 7  breath
: 2544 4 7  ea
: 2548 30 7  sy
- 2580
: 2588 3 2 Can’t
: 2592 4 2  dream
: 2596 2 0  yet
: 2598 2 -1  a
: 2600 2 0  no
: 2602 2 -1  ther
: 2604 2 0  dream
- 2607
: 2607 4 0 With
: 2612 3 2  out
: 2616 4 2  you
: 2620 2 0  ly
: 2621 2 -1  ing
: 2623 3 0  next
: 2626 2 -1  to
: 2628 3 0  me
- 2632
: 2632 4 0 There’s
: 2636 10 4  no
: 2646 15 2  way
- 2663
: 2672 3 -5 There’s
: 2676 8 -3  n
: 2684 3 -5  o
: 2688 47 -5  way...
E
