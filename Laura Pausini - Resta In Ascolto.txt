#TITLE:Resta In Ascolto
#ARTIST:Laura Pausini
#EDITION:Singstar - Italian Party
#LANGUAGE:Italian
#COVER:Laura Pausini - Resta In Ascolto [CO].jpg
#BACKGROUND:Laura Pausini - Resta In Ascolto [BG].jpg
#MP3:Laura Pausini - Resta In Ascolto.mp3
#BPM:328,00
#GAP:36402,44
: 0 4 59 O
: 4 4 59 gni
: 12 4 60  tan
: 19 4 59 to
: 27 5 55  pen
: 36 4 59 so~ a
: 44 3 57  te
- 49 113
: 133 7 62 Č~ u
: 140 6 64 na
* 149 11 67  vi
: 164 3 59 ta
: 171 5 57  che
- 178 236
: 256 3 59 Non
: 259 4 59  ti
: 267 9 60  chia
: 276 3 59 mo~ o
- 281
: 284 5 55 Chia
: 292 4 59 mi
: 299 8 57  me
- 309 310
* 328 3 55 Eh
- 333 369
: 389 2 62 Puň
: 396 3 64  suc
: 405 12 67 ce
: 420 7 59 de
: 428 4 57 ~re
- 434 476
: 496 5 67 Ma
: 501 3 66  nes
: 507 3 64 sun'
: 512 3 67  al
: 518 3 66 tro
: 523 4 64  chia
: 528 7 67 mai
- 537 538
: 551 2 64 A
* 556 7 69 mo
: 563 4 71 re
- 569 570
: 584 2 67 A
: 588 8 69 mo
: 596 5 71 re
- 603 604
: 613 3 67 Io
: 618 2 67  da~ al
: 624 5 67 lo
: 629 3 66 ra
- 633
: 635 2 64 Nes
: 640 5 67 su
: 645 2 66 no
: 652 2 64  tro
: 656 7 67 vai
- 665 666
: 680 2 64 Che~ as
: 688 4 69 so
: 692 8 67 mi
: 700 3 69 glias
: 708 3 71 se~ a
: 715 2 69  te
: 717 2 67 ~
: 719 12 64 ~
- 733 734
: 745 2 64 Che~ as
: 752 5 69 so
: 757 8 67 mi
: 765 2 69 glias
: 772 5 71 se~ a
: 780 9 69  me
- 791
: 793 4 64 Nel
* 800 8 67  cuo
: 808 5 64 re
- 814
: 816 4 71 Res
: 824 8 71 ta~ in
: 832 3 71  a
: 840 5 72 scol
: 848 5 71 to
- 854
: 856 3 67 Che
: 864 5 69  c'č~ un
: 872 4 66  mes
: 880 4 66 sag
: 888 4 66 gio
: 896 4 67  per
: 904 5 67  te
- 911 912
: 920 3 71 E
: 925 5 66  dim
: 933 3 67 mi
: 940 9 64  se
: 949 2 62 ~
: 951 4 59 ~
- 957 958
: 984 2 62 Ci
* 992 51 69  sei
- 1045 1046
: 1064 3 62 Per
: 1072 4 71 ché
: 1080 4 71  ti
: 1088 8 71  co
: 1096 3 71 no
: 1104 4 69 sco~ e
: 1112 4 67  so
- 1118
: 1120 8 69 Be
: 1128 3 67 ne
: 1136 4 66  che~ or
: 1144 4 71 mai
: 1152 3 66  per
: 1160 10 67  te
- 1172 1173
: 1200 5 62 Al
: 1208 4 64 ter
: 1216 5 71 na
: 1224 7 69 ti
: 1231 16 67 ~
: 1248 4 67 va~ a
: 1256 3 66  me
: 1259 3 65 ~
: 1262 4 66 ~
: 1266 3 64 ~
: 1269 17 62 ~
: 1286 2 64 ~
: 1288 2 62 ~
: 1290 2 59 ~
- 1294 1295
: 1312 3 62 Non
: 1320 6 64  c'č
- 1328 1358
: 1378 3 59 Non
: 1384 5 57  c'č
- 1391 1420
: 1440 8 64 Per
: 1453 8 64  te
: 1461 19 67 ~
- 1482 1483
: 1504 3 58 Non
: 1510 4 57  c'č
- 1516 1580
: 1600 2 59 Ma
: 1605 7 59  sa
: 1612 3 60 reb
: 1621 7 59 be~ u
: 1628 4 55 na
: 1637 5 59  bu
: 1644 9 57 gia
- 1655 1656
: 1672 5 55 Mia
- 1679 1715
: 1735 3 62 Dir
: 1742 8 64 ti~ a
: 1752 8 67 des
: 1766 4 59 so
: 1772 9 57  che
- 1783 1793
: 1813 3 59 No,
: 1825 3 59  no,
: 1837 6 59  no
- 1845 1846
: 1857 3 59 Non
: 1862 6 59  ho~ a
: 1870 3 60 vu
: 1878 3 59 to
: 1885 4 55  com
: 1894 8 59 pa
: 1902 7 57 gnia
- 1911 1912
* 1926 4 71 Ah
: 1930 4 69 ~
: 1934 10 67 ~
: 1944 3 64 ~
- 1949 1971
: 1991 5 62 So
: 1996 6 64 no~ u
: 2005 13 67 gua
: 2021 4 71 le~ a
: 2029 4 69  te
: 2037 5 66 ~
: 2042 4 64 ~
: 2046 2 66 ~
: 2048 2 64 ~
: 2050 8 62 ~
- 2060 2071
: 2091 3 64 Io
: 2094 2 66 ~
: 2098 3 67  so
: 2104 6 66 pra~ o
: 2110 2 64 gni
- 2113
: 2114 2 67 Boc
: 2120 2 66 ca
: 2125 2 64  cer
: 2131 7 67 cai
- 2140 2141
: 2150 2 62 Il
: 2154 2 64  tuo
: 2159 5 69  no
: 2166 3 71 me
- 2171 2172
: 2181 2 67 Il
: 2186 2 67  tuo
: 2191 5 69  no
* 2198 4 71 me
- 2204 2205
: 2214 2 67 Ho~ a
: 2219 2 67 spet
: 2225 3 67 ta
: 2230 4 66 to~ an
: 2236 2 64 che
- 2239
: 2241 3 67 Trop
: 2248 4 66 po~ e
: 2252 2 64  lo
: 2258 9 67  sai
- 2269 2270
: 2281 3 64 Che~ ho
: 2289 4 69  can
: 2297 5 67 cel
: 2302 5 69 la
: 2310 4 71 to
: 2317 4 67  te
: 2321 12 64 ~
- 2335 2336
: 2345 5 64 Ho~ al
: 2353 4 69 lon
: 2361 5 67 ta
: 2366 4 69 na
: 2373 4 71 to
: 2381 5 71  te
: 2386 4 69 ~
- 2392
: 2394 3 64 Dal
: 2401 8 67  cuo
: 2409 5 64 re
- 2415
: 2417 4 71 Res
: 2425 8 71 ta~ in
: 2433 3 71  a
: 2441 5 72 scol
: 2449 5 71 to
- 2455
: 2457 3 67 Che
: 2465 5 69  c'č~ un
: 2473 4 66  mes
: 2481 4 66 sag
: 2489 4 66 gio
: 2497 4 67  per
: 2505 5 67  te
- 2512 2513
: 2522 2 71 E
: 2527 4 66  dim
: 2534 3 67 mi
: 2541 11 64  se
: 2552 2 62 ~
: 2554 2 59 ~
- 2558 2559
: 2585 2 62 Ci
: 2593 54 69  sei
: 2647 2 71 ~
- 2651 2652
: 2665 3 62 Per
: 2673 4 71 ché
: 2681 4 71  ti
: 2690 7 74  co
: 2697 3 72 no
: 2706 5 71 sco~ e~ il
: 2714 3 67  mio
- 2719
: 2721 5 69 Po
: 2730 2 67 sto
: 2732 4 66 ~
: 2738 6 66  non
: 2745 5 71  č
: 2753 3 66  con
: 2761 11 67  te
: 2772 3 64 ~
: 2775 3 62 ~
- 2780 2781
: 2801 5 62 Di
: 2809 4 64 pen
: 2817 5 71 do
: 2825 7 69  giŕ
: 2832 15 67 ~
- 2848
: 2849 4 67 Da
: 2857 3 66  me
: 2860 3 64 ~
: 2863 4 66 ~
: 2867 3 64 ~
: 2870 19 62 ~
: 2889 2 64 ~
: 2891 2 62 ~
: 2893 2 59 ~
- 2897 2898
: 2907 3 62 Ah
- 2912 2913
: 2930 6 62 Rim
: 2938 4 64 pian
: 2946 8 71 ge
* 2954 12 69 rai
: 2966 3 67 ~
: 2969 5 64 ~
- 2976 2977
: 2994 6 62 Co
: 3002 4 64 se
: 3010 8 71  di
: 3018 13 69  noi
: 3031 4 71 ~
- 3037 3038
: 3050 4 62 Che~ hai
: 3058 4 64  per
: 3066 4 64 so
: 3074 3 64  per
: 3082 10 66  sem
: 3098 8 67 pre
- 3107
: 3107 3 67 Or
: 3115 27 67 mai
: 3147 27 69 ~
: 3174 3 67 ~
: 3177 2 64 ~
- 3181 3182
* 3203 7 71 Oh
- 3212 3213
: 3235 3 69 Oh
: 3242 7 67  no
- 3251 3416
: 3436 4 62 Tu
: 3442 4 71  res
: 3450 7 71 ta~ in
: 3466 3 71  a
* 3474 4 72 scol
: 3482 6 72 to
- 3490
: 3498 4 67 Per
: 3506 5 66 ché~ or
: 3514 5 71 mai
: 3522 6 66  per
: 3530 11 67  te
: 3541 2 64 ~
: 3543 2 62 ~
- 3547 3548
: 3571 3 62 Al
: 3579 5 64 ter
: 3586 4 71 na
: 3594 7 69 ti
: 3601 15 67 ~
: 3619 4 67 va~ a
: 3627 5 66  me
: 3632 3 64 ~
: 3635 3 66 ~
: 3638 3 64 ~
: 3641 15 62 ~
: 3656 2 64 ~
: 3658 2 62 ~
: 3660 2 59 ~
- 3664 3665
: 3685 2 62 Non
: 3691 6 64  c'č
- 3699 3728
: 3748 3 59 Non
: 3755 4 57  c'č
- 3761 3778
: 3798 2 62 Al
: 3802 2 64 ter
: 3806 2 62 na
: 3810 3 64 ti
: 3814 3 62 va~ a
: 3823 7 64  me
: 3830 21 67 ~
- 3853 3854
: 3875 4 59 Non
: 3882 4 57  c'č
- 3888 3954
: 3974 4 59 O
: 3978 4 59 gni
: 3987 6 60  tan
: 3995 4 59 to
: 4004 4 55  pen
: 4013 3 59 so~ a
: 4020 5 57  te
- 4027 4098
: 4118 3 62 Puň
: 4125 3 64  suc
: 4134 21 67 ce
: 4159 7 59 de
: 4167 5 57 re

