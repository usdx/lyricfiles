#TITLE:Il est libre Max
#ARTIST:Hervé Christiani
#MP3:Hervé Christiani - Il est libre Max.mp3
#VIDEO:Hervé Christiani - Il est libre Max.mp4
#COVER:Hervé Christiani - Il est libre Max.jpg
#BPM:267,59
#GAP:14530
#ENCODING:UTF8
#LANGUAGE:French
#GENRE:Pop
#YEAR:1981
#CREATOR:mustangfred
: 0 2 8 Il
: 4 3 10  met
: 9 2 10  de
: 12 2 10  la
: 16 2 10  ma
: 20 2 10 gie
: 24 2 8  mine
: 28 3 5  de
: 32 1 1  rien
- 34
: 36 3 -2 dans
: 42 1 -2  tout'ce
: 46 1 -2  qu'il
: 49 5 10  fait
- 62
: 65 2 10 Il
: 68 1 12  a
: 73 2 12  l'sou
: 76 1 12 rire
: 80 1 12  fa
: 84 2 12 cile
: 88 6 12  męme
: 97 2 13  pour
: 100 3 12  les
: 105 1 10  im
: 107 2 10 bé
: 113 6 10 ciles
- 125
: 128 1 8 Il
* 131 3 10  s'a
* 136 5 10 muse
: 144 6 10  bien
: 151 1 5  il
: 154 2 3  n'tombe
- 157
: 159 2 1 ja
: 162 4 -2 mais
: 167 2 -2  dans
: 170 1 -2  les
: 175 8 10  pičges
- 189
: 192 1 13 Il
: 195 2 12  n's'laisse
: 200 2 12  pas
: 203 1 12  é
: 206 3 12 tour
: 210 2 12 dir
: 214 3 12  par
: 218 2 12  les
- 221
* 223 2 13 né
* 226 3 12 ons
: 231 1 10  des
: 233 3 10  ma
: 238 6 10 nčges
- 252
: 255 1 8 Il
: 258 2 10  vit
: 262 6 10  sa
: 271 3 10  vie
: 278 1 8  sans
: 281 2 5  s'oc
: 286 2 1 cu
: 290 2 -2 per
: 293 2 -2  des
* 296 3 -2  gri
* 300 3 10 maces
- 313
: 317 1 13 Que
: 321 2 12  font
: 326 1 12  au
: 328 4 12 tour
: 333 3 12  de
: 337 3 12  lui
: 341 3 12  les
- 346
* 348 2 13 pois
* 353 2 12 sons
: 357 2 10  dans
: 360 2 10  la
: 364 5 10  nasse
- 371
* 373 2 8 Il
* 376 2 10  est
* 381 29 13  li
* 412 1 10 bre
* 415 3 10  Max
- 428
: 437 1 8 Il
: 439 2 10  est
: 445 29 13  li
: 476 2 10 bre
: 479 2 10  Max
- 491
: 508 3 13 Y'en
: 512 3 12  a
: 517 4 12  męme
: 525 2 12  qui
: 528 4 12  disent
* 536 2 12  qu'ils
* 540 2 13  l'ont
: 545 2 12  vu
: 549 6 10  vo
: 556 16 10 ler
- 582
: 603 1 8 Il
: 606 4 10  tra
: 611 3 10 vaille
: 615 2 10  un
: 619 1 10  p'tit
: 623 2 10  peu
- 626
: 628 3 5 quand
: 635 1 1  son
: 639 3 -2  corps
: 643 2 -2  est
: 646 2 -2  d'ac
: 651 5 10 cord
- 665
: 668 2 10 Pour
: 671 3 12  lui
: 676 1 12  faut
: 679 2 12  pas
: 683 2 12  s'en
: 688 3 12  faire
: 692 1 12  il
: 696 2 12  sait
* 699 3 13  do
* 703 3 12 ser
- 707
: 709 1 10 son
: 711 2 10  ef
: 716 6 10 fort
- 728
: 731 1 8 Dans
: 735 2 10  l'pa
: 739 3 10 nier
: 744 1 10  de
: 747 8 10  crabes
- 756
: 758 2 5 il
: 762 1 1  n'joue
: 766 4 -2  pas
: 771 2 -2  les
: 774 3 -2  ho
: 778 6 10 mards
- 792
: 795 1 13 Il
: 799 1 12  n'cherche
: 803 3 12  pas
: 807 1 12  ŕ
: 810 1 12  tout
: 814 3 12  prix
: 818 1 12  ŕ
: 821 2 12  faire
- 824
: 826 2 13 des
: 829 3 12  bulles
: 834 2 10  dans
: 837 2 10  la
: 842 6 10  mare
- 849
: 851 2 8 Il
: 854 2 10  est
: 859 28 13  li
: 889 1 10 bre
: 892 3 10  Max
- 905
: 913 2 8 Il
: 916 3 10  est
: 922 29 13  li
: 953 2 10 bre
: 957 3 10  Max
- 970
: 985 2 13 Y'en
: 988 3 12  a
: 993 5 12  męme
: 1001 2 12  qui
: 1004 4 12  disent
: 1012 2 12  qu'ils
* 1016 2 13  l'ont
* 1020 3 12  vu
: 1026 4 10  vo
: 1033 15 10 ler
- 1083
: 1176 2 8 Il
: 1179 4 10  r'garde
: 1185 2 10  au
: 1189 2 10 tour
: 1192 2 10  de
: 1195 3 10  lui
: 1199 1 8  a
: 1202 1 5 vec
- 1204
: 1206 1 1 les
: 1210 1 -2  yeux
: 1213 3 -2  de
: 1217 2 -2  l'a
* 1221 7 10 mour
- 1234
: 1237 2 13 A
: 1240 3 12 vant
: 1245 2 12  qu't'aies
: 1248 3 12  rien
: 1254 1 12  pu
: 1257 3 12  dire
: 1261 1 12  il
: 1264 2 12  t'aime
- 1267
: 1269 2 13 dé
: 1273 4 12 jŕ
: 1278 1 10  au
: 1281 1 10  dé
: 1285 8 10 part
- 1299
: 1302 1 8 Il
: 1305 2 10  n'fait
: 1309 4 10  pas
: 1316 5 10  d'bruit,
: 1328 2 5  il
: 1332 1 1  n'joue
: 1335 3 -2  pas
: 1340 1 -2  du
: 1343 3 -2  tam
: 1348 8 10 bour
- 1362
: 1364 2 13 Mais
: 1367 2 12  la
: 1372 1 12  sta
: 1376 2 12 tue
: 1379 2 12  de
: 1382 5 12  marbre
- 1388
: 1390 2 12 lui
: 1396 2 13  sou
: 1399 3 12 rit
: 1403 2 10  dans
: 1406 2 10  la
: 1411 6 10  cour
- 1418
: 1420 1 8 Il
: 1422 2 10  est
: 1428 27 13  li
: 1457 2 10 bre
: 1461 3 10  Max
- 1474
: 1482 2 8 Il
: 1485 1 10  est
: 1490 30 13  li
: 1521 2 10 bre
: 1525 2 10  Max
- 1537
: 1553 2 13 Y'en
: 1556 3 12  a
: 1561 5 12  męme
: 1569 1 12  qui
: 1572 4 12  disent
: 1580 2 12  qu'ils
: 1584 2 13  l'ont
: 1588 3 12  vu
: 1594 4 10  vo
: 1601 11 10 ler
- 1647
: 2092 1 8 Et
: 2095 2 10  bien
: 2099 1 10  sűr
- 2104
: 2106 1 10 toutes
: 2108 1 10  les
: 2111 3 10  filles
: 2116 1 8  lui
: 2119 2 5  font
: 2122 2 1  leurs
: 2127 2 -2  yeux
: 2131 1 -2  de
: 2133 2 -2  ve
: 2137 7 10 lours
- 2151
: 2154 1 13 Lui
: 2157 3 12  pour
: 2161 2 12  leur
: 2165 2 12  faire
: 2170 2 12  plai
: 2173 2 12 sir
- 2176
: 2178 1 12 il
: 2180 3 12  leur
: 2185 1 13  ra
: 2189 2 12 conte
: 2193 2 10  des
: 2197 1 10  his
: 2202 8 10 toires
- 2216
: 2218 1 8 Il
: 2220 3 10  les
: 2225 6 10  em
: 2233 7 10 mčne
: 2244 3 5  par
: 2249 2 1  de
: 2252 3 -2 lŕ
: 2257 1 -2  les
: 2259 2 -2  la
: 2264 5 10 bours
- 2277
: 2280 1 13 Che
: 2283 2 12 vau
: 2288 1 12 cher
: 2291 2 12  des
: 2294 1 12  li
: 2298 4 12 cornes
* 2303 6 12  ŕ
: 2310 1 13  la
: 2315 2 12  tom
: 2319 1 10 bée
: 2322 1 10  du
: 2326 6 10  soir
- 2333
: 2335 1 8 Il
: 2337 2 10  est
: 2343 28 13  li
: 2373 1 10 bre
: 2376 2 10  Max
- 2388
: 2398 1 8 Il
: 2400 2 10  est
: 2405 29 13  li
: 2435 2 10 bre
: 2439 2 10  Max
- 2451
: 2467 2 13 Y'en
: 2471 2 12  a
: 2475 5 12  męme
: 2484 1 12  qui
: 2486 6 12  disent
: 2494 2 12  qu'ils
: 2498 2 13  l'ont
: 2502 2 12  vu
: 2507 5 10  vo
: 2514 21 10 ler
- 2570
: 2656 2 8 Comme
: 2659 2 10  il
: 2663 2 10  n'a
: 2666 3 10  pas
: 2671 2 10  d'ar
: 2675 2 10 gent
- 2678
: 2680 1 1 pour
: 2683 3 3  faire
: 2687 1 1  le
: 2690 3 -2  grand
: 2695 2 -2  vo
: 2698 2 -2 ya
: 2702 6 10 geur
- 2715
: 2718 2 13 Il
: 2721 3 12  va
: 2726 2 12  par
: 2729 1 12 ler
: 2734 2 12  sou
: 2737 2 12 vent
- 2740
: 2742 1 12 aux
: 2744 4 12  ha
: 2749 1 13 bi
: 2752 3 12 tants
: 2757 1 10  de
: 2760 2 10  son
: 2765 8 10  coeur
- 2779
: 2781 1 10 Qu'estce
: 2784 2 10  qu'ils
: 2792 3 10  s'ra
: 2800 2 10 content
- 2803
: 2805 1 5 c'est
: 2808 2 3  ça
: 2813 1 1  qu'il
: 2815 3 -2  fau
: 2819 1 -2 drait
: 2822 3 -2  sa
: 2829 5 10 voir
- 2842
: 2845 1 13 Pour
: 2847 3 12  a
: 2851 2 12 voir
: 2856 2 12  comme
: 2860 3 12  lui
- 2865
: 2867 2 12 au
: 2870 2 12 tant
: 2874 2 13  d'a
: 2880 2 12 mour
: 2883 2 10  dans
: 2886 1 10  le
: 2888 1 10  re
: 2891 5 10 gard
- 2897
: 2899 1 8 Il
: 2901 3 10  est
: 2906 28 13  li
: 2936 2 10 bre
: 2940 2 10  Max
- 2952
: 2961 1 8 Il
: 2963 3 10  est
: 2969 29 13  li
: 3000 1 10 bre
: 3003 2 10  Max
- 3015
: 3032 1 13 Y'en
: 3034 4 12  a
: 3040 4 12  męme
: 3048 1 12  qui
: 3050 5 12  disent
: 3058 2 12  qu'ils
: 3062 3 13  l'ont
: 3067 2 12  vu
: 3071 4 10  vo
: 3078 22 10 ler
E