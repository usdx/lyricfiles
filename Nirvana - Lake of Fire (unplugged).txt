#TITLE:Lake of Fire (unplugged)
#ARTIST:Nirvana
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
#MP3:Nirvana - Lake of Fire (unplugged).mp3
#COVER:Nirvana - Lake of Fire (unplugged) [CO].jpg
#BACKGROUND:Nirvana - Lake of Fire (unplugged) [BG].jpg
#BPM:278
#GAP:22270
: 1 2 16 Where
: 5 2 18  do
: 9 4 18  bad
: 17 4 18  folks
: 25 2 16  go
: 28 2 16  when
: 32 3 16  they
* 40 8 21  die?
- 49
: 51 2 15 They
: 55 3 18  don't
: 61 2 18  go
: 65 2 18  to
: 69 2 18  hea
: 73 2 18 ven
- 76
: 77 2 13  where
: 81 2 11  the
* 84 4 15  an
: 91 3 15 gels
: 100 2 15  fly
: 103 3 13  ~ ,
- 108
: 114 3 18 go
: 118 2 18  to
: 121 2 18  a
: 126 5 18  lake
: 134 5 18  of
: 144 3 16  fi
: 148 2 16 re
: 151 4 16  and
: 159 5 9  fry,
- 166
: 173 3 23 see
: 178 2 23  them
: 181 3 23  a
: 186 7 23 gain
: 196 3 16  till
: 202 2 16  the
: 205 2 16  fourth
: 209 2 16  of
: 212 5 18  Ju
: 221 7 18 ly.
- 230 334
: 354 3 13 I
: 359 2 13  knew
: 362 2 13  a
* 366 5 16  la
: 374 7 13 dy,
: 385 3 11  came
: 390 2 13  from
: 393 2 13  Du
: 397 6 13 luth,
- 405 406
: 415 4 13 bit
: 423 2 13  by
: 426 2 11  a
: 431 5 13  dog
: 438 2 13  with
: 442 2 11  a
: 446 3 12  ra
: 451 3 12 bbit
: 458 6 12  tooth.
- 466
: 472 2 13 She
: 476 4 13  went
: 484 2 13  to
: 488 2 13  her
: 492 4 13  grave
: 499 2 13  just
: 503 2 13  a
: 508 2 15  li
: 511 2 15 ttle
: 515 3 15  too
: 522 6 9  soon,
- 530
: 538 2 18 flew
: 542 2 18  how
: 546 4 18 ling
: 553 5 15  out
: 560 3 13  on
: 564 2 11  the
: 569 3 13  ye
: 573 6 13 llow
: 585 21 13  moon.
- 608 618
: 638 2 16 Where
: 642 2 18  do
: 645 4 18  bad
: 653 5 18  folks
: 661 2 16  go
: 665 2 16  when
: 668 4 16  they
* 676 8 21  die?
- 686
: 688 2 16 They
: 692 4 18  don't
: 700 2 18  go
: 703 2 18  to
: 707 3 18  hea
: 711 2 18 ven
- 714
: 714 2 13  where
: 718 2 11  the
: 722 5 15  an
: 729 3 15 gels
: 737 3 15  fly
: 741 4 13  ~ ,
- 747
: 753 3 18 go
: 757 2 18  to
: 760 2 18  a
: 764 4 18  lake
: 772 4 18  of
: 783 3 16  fi
: 787 2 16 re
: 790 4 16  and
: 799 6 9  fry,
- 807
: 813 3 23 see
: 818 2 23  them
: 821 2 23  a
* 825 6 23 gain
: 836 2 16  till
: 840 2 16  the
: 843 3 16  fourth
: 848 2 16  of
: 852 6 18  Ju
: 860 9 18 ly.
- 871 976
: 996 3 13 Peo
: 1001 2 13 ple
: 1009 6 16  cry
: 1017 6 13  and
: 1027 3 13  peo
: 1032 2 13 ple
: 1039 8 13  moan,
- 1049 1050
: 1058 3 13 look
: 1063 2 13  for
: 1066 2 13  a
: 1070 4 13  dry
: 1079 4 13  place
: 1086 2 11  to
: 1090 4 12  call
: 1097 2 12  their
: 1102 6 12  home,
- 1110 1111
: 1121 2 13 try
: 1124 2 13  to
: 1127 3 13  find
: 1132 2 13  some
: 1136 5 13  place
: 1144 2 13  to
: 1148 6 15  rest
: 1159 3 15  their
: 1167 4 9  bones,
- 1173
: 1175 2 9 while
: 1178 2 9  the
* 1182 3 18  an
: 1187 2 18 gels
: 1190 2 18  and
: 1193 2 18  the
: 1197 3 15  de
: 1202 2 15 vils
- 1205
: 1206 2 11 try
: 1209 2 11  to
: 1213 2 11  make
: 1217 2 13  them
: 1220 5 13  their
: 1229 17 13  own.
- 1248 1263
: 1283 2 16 Where
: 1287 2 18  do
: 1291 4 18  bad
: 1299 4 18  folks
: 1307 2 16  go
: 1310 2 16  when
: 1314 3 16  they
* 1322 8 21  die?
- 1331
: 1333 2 16 They
: 1337 4 18  don't
: 1345 2 18  go
: 1348 2 18  to
: 1352 3 18  hea
: 1357 2 18 ven
- 1360
: 1360 2 13  where
: 1363 2 11  the
: 1367 5 15  an
: 1375 4 15 gels
: 1383 3 15  fly
: 1387 3 13  ~ ,
- 1392
: 1400 2 18 go
: 1403 2 18  to
: 1406 2 18  a
: 1411 4 18  lake
: 1418 6 18  of
: 1430 2 16  fi
: 1433 2 16 re
: 1437 4 16  and
: 1446 5 9  fry,
- 1453
: 1460 3 23 see
: 1464 2 23  them
: 1467 2 23  a
: 1472 6 23 gain
: 1483 2 16  till
: 1487 2 16  the
: 1491 2 16  fourth
: 1495 2 16  of
: 1499 4 18  Ju
* 1507 8 18 ly.
E
