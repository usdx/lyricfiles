#TITLE:Danger - Keep Away
#ARTIST:Slipknot
#MP3:Slipknot - Danger - Keep Away.mp3
#VIDEO:Slipknot - Danger - Keep Away.mp4
#COVER:Slipknot - Danger - Keep Away.jpg
#BPM:206.48
#GAP:38209
#ENCODING:UTF8
: 0 7 9 That
: 7 5 7 ~
: 12 7 5  place
: 19 5 2  is
: 24 3 5  in
: 27 7 9  my
: 34 2 9  mind
: 36 3 8 ~
: 39 5 9 ~
- 68
: 92 4 2 Is
: 96 7 9  that
: 103 4 7 ~
: 107 8 5  space
: 115 4 2  that
: 119 4 5  you
: 123 7 5  call
: 130 9 5  mine
- 165
: 191 7 9 That
: 198 5 7 ~
: 203 8 5  place
: 211 4 2  is
: 215 4 5  in
: 219 7 9  my
: 226 3 9  mind
: 229 2 8 ~
: 231 7 9 ~
- 260
: 282 5 2 Is
: 287 6 9  that
: 293 6 7 ~
: 299 7 5  space
: 306 4 2  that
: 310 4 5  you
: 314 7 5  call
: 321 9 5  mine
- 356
: 383 6 9 Where
: 389 6 7  have
: 395 4 5  I
: 399 12 2  been
: 411 4 2  all
: 415 4 0  this
: 419 4 2  time
: 423 6 5 ~?
- 454
: 479 6 9 Lost
: 485 5 5  en
: 490 6 7 slaved
: 496 19 7 ~
: 515 3 5  fa
: 518 2 2 tal
: 520 4 2  de
: 524 7 2 cline
- 552
: 575 6 9 I've
: 581 6 7  been
: 587 4 5  wai
: 591 4 2 ting
: 595 3 2  for
: 598 4 2  this
: 602 4 2  to
: 606 4 0  un
: 610 3 2 fold
: 613 4 5 ~
- 642
: 668 4 2 The
: 672 3 9  pie
: 675 4 7 ces
: 679 4 5  are
: 683 3 5  on
: 686 4 2 ly
: 690 4 2  as
: 694 4 2  good
: 698 4 2  as
: 702 4 0  the
: 706 6 2  whole
- 741
* 765 4 14 Se
* 769 4 12 vered
* 773 5 9  my
* 778 7 9 self
* 785 4 9  from
* 789 4 9  my
* 793 9 9  own
* 802 11 9  life
- 837
: 861 4 9 Cut
: 865 4 7  out
: 869 4 5  the
: 873 4 5  on
: 877 4 2 ~
: 881 4 2 ly
: 885 5 2  thing
: 890 4 2  that
: 894 4 0  was
: 898 4 2  right
: 902 5 5 ~
- 933
: 959 4 2 What
: 963 4 2  if
: 967 4 0  i
: 971 3 2  ne
: 974 4 2 ver
: 978 4 2  saw
: 982 3 5  you
: 985 4 5  a
: 989 2 9 gain
: 991 3 7 ~
: 994 7 9 ~?
- 1025
: 1050 5 2 I'd
: 1055 5 9  die
: 1060 6 7  right
: 1066 4 5  next
: 1070 4 2  to
: 1074 8 2  you
: 1082 4 2  in
: 1086 4 0  the
: 1090 6 2  end
- 1123
: 1151 7 9 That
: 1158 4 7 ~
: 1162 8 5  place
: 1170 4 2  is
: 1174 4 5  in
: 1178 7 9  my
: 1185 2 9  mind
: 1187 3 7 ~
: 1190 6 9 ~
- 1227
: 1242 5 2 Is
: 1247 5 9  that
: 1252 6 7 ~
: 1258 8 5  space
: 1266 4 2  that
: 1270 4 5  you
: 1274 7 5  call
: 1281 9 5  mine
- 1319
: 1342 4 9 That
: 1346 8 7 ~
: 1354 8 5  place
: 1362 4 2  is
: 1366 4 5  in
: 1370 6 9  my
: 1376 2 9  mind
: 1378 2 7 ~
: 1380 5 9 ~
- 1420
: 1434 5 2 Is
: 1439 4 9  that
: 1443 7 7 ~
: 1450 7 5  space
: 1457 4 2  that
: 1461 4 5  you
: 1465 7 5  call
: 1472 8 5  mine
- 1511
: 1535 5 9 I
: 1540 6 7  won't
: 1546 4 5  let
: 1550 10 2  you
: 1560 4 2  walk
: 1564 4 0  a
: 1568 4 2 way
: 1572 4 5 ~
- 1607
: 1626 4 2 With
: 1630 6 9 out
: 1636 6 7 ~
: 1642 4 5  hear
: 1646 4 2 ing
: 1650 3 2  what
: 1653 4 2  I
: 1657 4 2  have
: 1661 4 0  to
: 1665 7 2  say
- 1696
: 1721 4 2 With
: 1725 6 9 out
: 1731 5 7 ~
: 1736 4 5  hear
: 1740 4 2 ing
: 1744 4 2  what
: 1748 4 2  I
: 1752 4 2  have
: 1756 4 0  to
: 1760 6 2  say
- 1791
: 1817 4 2 With
: 1821 6 9 out
: 1827 4 7 ~
: 1831 4 5  hear
: 1835 4 2 ing
: 1839 4 2  what
: 1843 4 2  I
: 1847 4 2  have
: 1851 4 0  to
: 1855 7 2  say
E