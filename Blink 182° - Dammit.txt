#TITLE:Dammit
#ARTIST:Blink 182째
#MP3:Blink 182째 - Dammit.mp3
#COVER:Blink 182째 - Dammit [CO].jpg
#BACKGROUND:Blink 182째 - Dammit [BG].jpg
#BPM:109,8
#GAP:18400
: 0 2 60 It's 
: 2 2 62 al
: 4 2 62 right 
: 6 2 64 to 
: 8 2 64 tell 
: 10 2 64 me 
- 14
: 16 2 60 What 
: 18 2 62 you 
: 20 2 62 think 
: 22 3 64 about 
: 25 3 64 me. 
- 30
: 32 2 60 I 
: 34 2 62 won't 
: 36 2 62 try 
: 38 2 64 to 
: 40 4 64 argue
- 46
: 48 2 60 or 
: 50 2 62 hold 
: 52 2 62 it 
: 54 2 64 ag
: 56 2 64 ainst 
: 58 2 64 you
- 62
: 64 2 60 I 
: 66 2 62 know 
: 68 2 62 that 
: 70 2 64 you're 
: 72 2 64 leav
: 74 2 64 ing
- 78
: 80 2 60 You 
: 82 2 62 must 
: 84 2 62 have 
: 86 2 64 your 
: 88 4 64 reasons
- 94
: 96 2 60 The 
: 98 4 62 season 
: 102 2 64 is 
: 104 4 64 calling
- 110
: 112 1 60 and 
: 113 1 60 your 
: 114 4 64 pictures 
: 118 2 62 are 
: 120 2 62 falling 
: 122 4 62 down
- 130
: 192 2 60 The 
: 194 2 62 steps 
: 196 2 62 that 
: 198 2 64 I 
: 200 4 64 retrace
- 206
: 208 2 60 The 
: 210 2 62 sad 
: 212 2 62 look 
: 214 2 64 on 
: 216 2 64 your 
: 218 2 64 face 
- 222
: 224 2 60 The 
: 226 4 62 timing 
: 230 2 64 and 
: 232 4 64 structure
- 238
: 240 2 60 did 
: 242 2 65 you 
: 244 4 65 hear 
: 248 2 60 he 
: 250 2 60 fucked 
: 252 2 60 her?
- 256
: 258 2 60 A 
: 260 2 62 day 
: 262 2 62 late 
: 264 2 64 a 
: 266 2 64 buck 
: 268 2 64 short
- 272
: 274 2 60 I'm 
: 276 4 62 writing 
: 280 2 64 the 
: 282 4 64 report
- 288
: 290 2 60 On 
: 292 2 62 los
: 294 2 62 ing 
: 296 2 64 and 
: 298 4 64 failing
- 306
: 308 2 60 When 
: 310 2 62 I 
: 312 2 62 move 
: 314 2 64 I'm 
: 316 2 64 flailing 
: 318 2 64 now
- 324
: 326 2 60 And 
: 328 2 65 it's 
: 330 2 65 happ
: 332 2 65 ened 
: 334 2 65 once 
: 336 2 64 again 
- 341
: 342 2 64 I'll 
: 344 2 65 turn 
: 346 2 62 to 
: 348 2 62 a 
: 350 2 64 friend
- 354
: 356 2 60 Some
: 358 2 60 one 
: 360 2 62 that 
: 362 2 64 under
: 364 4 64 stands
- 370
: 372 2 62 See's 
: 374 2 62 through 
: 376 2 64 the 
: 378 2 64 master 
: 380 3 64 plan
- 386
: 388 2 62 But 
: 390 4 62 every
: 394 4 62 body's 
: 398 4 64 gone
- 402
: 406 1 64 And 
: 407 1 64 I've 
: 408 1 64 been 
: 409 1 64 here 
: 410 2 60 for 
: 412 2 62 too 
: 414 4 62 long 
- 420
: 422 2 64 To 
: 424 2 64 face 
: 426 2 64 this 
: 428 2 60 on 
: 430 2 62 my 
: 432 2 64 own 
- 434
: 434 2 64 Well 
: 436 2 64 I 
: 438 2 64 guess 
: 440 2 60 this 
: 442 2 60 is 
: 444 2 60 growing 
: 446 2 60 up 
- 454
: 464 2 60 Well 
: 466 2 64 I 
: 468 2 64 guess 
: 470 2 60 this 
: 472 2 60 is 
: 474 2 60 growing 
: 476 2 60 up 
- 490
: 516 2 62 And 
: 518 4 62 maybe 
: 522 2 64 I'll 
: 524 2 64 see 
: 526 2 64 you
- 529
: 530 2 60 at 
: 532 2 62 a 
: 534 4 62 movie 
: 538 2 64 sneak 
: 540 2 64 pre
: 542 2 64 view
- 546
: 548 2 60 You'll 
: 550 2 62 show 
: 552 2 62 up 
: 554 2 64 and 
: 556 2 64 walk 
: 558 2 64 by
- 562
: 564 2 64 on 
: 566 2 64 the 
: 568 2 64 arm 
: 570 2 62 of 
: 572 2 62 that 
: 574 2 60 guy
- 579
: 580 2 64 and 
: 582 3 64 I'll 
: 585 3 64 smile 
: 588 2 62 and 
: 590 2 64 I'll 
: 592 2 64 wave 
- 596
: 598 6 64 We'll 
: 600 4 64 pretend 
: 604 2 60 it's 
: 606 2 60 okay
- 609
: 610 3 62 The 
: 613 4 62 charade 
: 617 3 64 it 
: 620 2 64 won't 
: 622 3 64 last
- 624
: 626 2 60 When 
: 628 2 65 he's 
: 630 3 65 gone 
: 633 2 60 I 
: 635 3 60 won't 
: 638 3 60 come 
: 641 3 60 back 
- 646
: 646 4 60 And 
: 650 2 60 it'll 
: 652 2 60 happen 
: 654 2 67 once 
: 656 4 67 again 
- 663
: 663 2 65 I'll 
: 665 3 64 turn 
: 668 2 62 to 
: 670 2 67 a 
: 672 5 67 friend
- 678
: 680 2 65 Some
: 682 2 64 one 
: 684 2 60 that 
: 686 2 60 under
: 688 2 60 stands
- 691
: 692 2 65 and  
: 694 2 64 see's 
: 696 4 64 through 
: 700 2 60 the 
: 702 2 60 master 
: 704 2 60 plan
- 710
: 712 4 60 But 
: 716 2 60 every
: 718 2 67 body's 
: 720 4 65 gone
- 725
: 726 1 64 And 
: 727 1 64 you've 
: 728 2 64 been 
: 730 2 60 there 
: 732 2 60 for 
: 734 2 60 too 
: 736 2 60 long
- 739
: 740 3 64 To 
: 743 2 64 face 
: 745 2 67 this 
: 747 2 67 on 
: 749 2 67 your 
: 751 2 67 own 
- 754
: 755 2 64 Well 
: 757 2 64 I 
: 759 2 64 guess 
: 761 2 60 this 
: 763 2 60 is 
: 765 2 60 growing 
: 767 2 60 up 
- 796
: 876 2 64 Well 
: 878 2 64 I 
: 880 2 64 guess 
: 882 2 60 this 
: 884 2 60 is 
: 886 3 60 growing 
: 889 2 60 up 
- 906
: 908 2 64 Well 
: 910 2 64 I 
: 912 2 64 guess 
: 914 2 60 this 
: 916 2 60 is 
: 918 3 60 growing 
: 921 2 60 up 
- 930
: 939 2 64 Well 
: 941 2 64 I 
: 943 2 64 guess 
: 945 2 60 this 
: 947 2 60 is 
: 949 3 60 growing 
: 952 2 60 up 
- 960
: 971 2 64 Well 
: 973 2 64 I 
: 975 2 64 guess 
: 977 2 60 this 
: 979 2 60 is 
: 981 3 60 growing 
: 984 2 60 up 
- 996
: 1004 2 64 Well 
: 1006 2 64 I 
: 1008 2 64 guess 
: 1010 2 60 this 
: 1012 2 60 is 
: 1014 3 60 growing 
: 1017 2 60 up!
E
