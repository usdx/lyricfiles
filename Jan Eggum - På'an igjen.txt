#TITLE:Pĺ'an igjen
#ARTIST:Jan Eggum
#MP3:Jan Eggum - Pĺ'an igjen.mp3
#COVER:Jan Eggum - Pĺ'an igjen [CO].jpg
#BACKGROUND:Jan Eggum - Pĺ'an igjen [BG].jpg
#RELATIVE:YES
#BPM:154
#GAP:14980
: 0 2 49 De 
: 2 3 50 e 
: 5 3 52 blikk 
: 8 2 54 i 
: 11 4 52 blikk 
- 25 37
: 0 2 49 Beg - 
: 2 2 50 ge 
: 4 3 52 kjen - 
: 7 2 50 ner 
: 10 3 54 sř - 
: 14 2 52 te 
: 17 2 52 pi - 
: 19 3 50 le - 
: 23 2 50 stikk 
- 25 26
: 0 2 49 Det'e 
: 2 2 49 A - 
: 5 2 49 mor 
: 9 2 49 som 
: 12 2 49 regj - 
: 14 3 49 er - 
: 17 2 50 er 
- 25 32
: 0 2 47 Men 
: 3 2 47 fřr 
: 5 3 47 det 
: 8 2 47 eks - 
: 10 3 47 plo - 
: 13 2 47 der - 
: 15 3 49 er 
- 25 33
: 0 2 52 Vil 
: 2 2 52 O - 
: 4 2 50 le 
: 7 3 49 ha 
: 10 3 50 en 
: 13 5 47 dans 
- 22 27
: 0 2 45 Men 
: 2 2 47 hon 
: 4 2 45 e 
: 6 2 47 al - 
: 8 4 45 le - 
: 12 3 47 red - 
: 15 3 45 e 
: 18 5 49 hans 
- 23 25
: 0 3 49 De 
: 3 2 50 e 
: 5 4 52 hĺnd 
: 9 3 54 i 
: 12 6 52 hĺnd 
- 25 32
: 0 2 49 De 
: 2 2 50 syns 
: 4 3 52 li - 
: 7 2 49 vets 
: 11 4 54 vei 
: 15 2 52 skal 
: 18 2 52 ve - 
: 20 2 50 re 
: 23 3 50 sĺnn 
- 26 27
: 0 2 49 La 
: 2 2 49 no 
: 5 2 49 al - 
: 8 2 49 vor 
: 10 2 49 kom - 
: 13 1 49 me 
: 14 3 49 si - 
: 17 3 50 den 
- 26 33
: 0 1 47 Vi 
: 1 2 47 vil 
: 4 2 47 ko - 
: 7 2 47 se 
: 9 2 47 he - 
: 12 2 47 le 
: 14 2 47 ti - 
: 16 2 49 den 
- 24 31
: 0 2 52 Men 
: 2 2 52 en 
: 5 3 52 lřr - 
: 8 1 50 dag 
: 10 3 49 e 
: 13 3 50 det 
: 17 5 47 slutt 
- 26 30
: 0 1 47 For 
: 1 1 47 hon 
: 2 3 45 har 
: 5 2 47 fĺtt 
: 8 2 49 en 
: 10 9 47 annen 
: 20 2 45 gutt 
- 26 31
: 0 3 45 Ja 
: 4 1 42 sĺ 
: 5 2 45 e 
: 7 1 45 det 
: 9 3 45 pĺ - 
: 12 1 47 'an 
: 13 2 49 i - 
: 16 3 49 gjen 
- 29 41
: 0 2 49 Pĺ - 
: 2 2 49 'an 
: 4 3 47 i - 
: 7 3 47 gjen 
- 20 33
: 0 2 49 Pĺ - 
: 2 1 49 'an 
: 3 3 47 i - 
: 6 3 47 gjen 
- 19 31
: 0 2 47 Pĺ - 
: 2 2 47 'an 
: 4 3 45 i - 
: 7 3 45 gjen 
- 18 27
: 0 1 49 Kor - 
: 1 3 50 dan 
: 5 4 52 skjer 
: 9 2 54 det 
: 12 3 52 tro 
- 21 28
: 0 3 49 O - 
: 3 3 49 le 
: 7 2 50 e 
: 10 3 52 kropp 
: 14 2 49 i 
: 17 4 54 kropp 
- 21 23
: 0 2 52 Med 
: 2 2 52 num - 
: 5 2 50 mer 
: 9 1 50 to 
- 10 12
: 0 2 49 Ja 
: 2 2 49 for 
: 5 2 49 du 
: 7 2 49 har 
: 9 2 49 vel 
: 12 3 50 hřrd 
: 17 2 50 det 
- 25 32
: 0 1 47 De 
: 2 2 47 kan 
: 5 2 47 mer 
: 8 1 47 enn 
: 10 2 47 ba - 
: 12 2 47 re 
: 16 3 47 flřr - 
: 20 2 49 te 
- 27 32
: 0 2 52 Slikk 
: 2 2 50 og 
: 4 3 52 bit 
: 8 2 50 et - 
: 10 3 49 ter 
: 13 1 50 be - 
: 14 4 47 hag 
- 23 29
: 0 1 47 Om - 
: 2 4 47 slyn - 
: 7 2 49 get 
: 10 3 47 natt 
: 15 2 45 og 
: 17 4 45 dag 
- 23 25
: 0 3 49 Han 
: 3 1 50 e 
: 4 3 52 hakk 
: 7 3 54 i 
: 10 5 52 hel 
- 23 32
: 0 1 49 Nĺr 
: 1 1 49 hon 
: 2 1 49 vil 
: 4 2 52 ve - 
: 6 2 49 re 
: 8 4 54 hans 
- 13 15
: 0 2 52 Med 
: 2 3 52 kropp 
: 6 2 50 og 
: 8 5 50 sjel 
- 13 14
: 0 2 49 Sam - 
: 2 2 49 men 
: 5 2 49 skal 
: 8 2 49 vi 
: 11 2 49 vć - 
: 14 2 50 re 
- 23 30
: 0 2 47 Til 
: 2 2 47 dřd - 
: 4 2 47 en 
: 7 2 47 skil - 
: 10 3 47 ler 
: 14 2 47 kjć - 
: 16 3 49 re 
- 23 28
: 0 2 52 Litt 
: 2 2 52 mye 
: 4 2 52 ĺ 
: 7 2 50 bli 
: 11 3 49 en - 
: 15 2 49 ig 
: 17 3 47 om 
- 25 31
: 0 2 47 Men 
: 2 2 45 no 
: 5 3 47 ven - 
: 8 2 49 ter 
: 12 3 47 pres - 
: 16 2 45 ten 
: 18 4 45 kom 
- 24 26
: 0 7 49 Kom 
: 8 5 52 kom 
: 14 3 57 kom 
- 17 18
: 0 2 57 Sĺ 
: 2 2 57 har 
: 4 1 57 hon 
: 6 4 57 hĺnd 
: 10 2 56 om 
: 13 3 56 hals 
- 16 17
: 0 1 57 Rock 
: 1 2 57 n' 
: 4 2 57 roll 
: 7 3 57 ble 
: 10 1 56 til 
: 12 3 56 vals 
: 16 1 57 og 
: 17 1 57 det 
: 18 1 57 e 
- 21 23
: 0 3 57 Slutt 
: 5 1 59 pĺ 
: 6 3 59 punk 
: 10 1 61 og 
: 12 5 61 pils 
: 17 2 59 og 
: 21 3 59 fjas 
- 24 25
: 0 2 57 Hon 
: 2 2 57 ser 
: 4 1 57 pĺ 
: 7 2 57 Tan - 
: 9 1 56 de 
: 11 3 56 P 
- 14 16
: 0 1 57 Og 
: 1 1 57 e 
: 2 1 57 visst 
: 4 1 57 for - 
: 5 3 57 nřyd 
: 9 1 56 med 
: 11 3 56 det 
- 14 15
: 0 2 57 Sĺ 
: 2 2 57 O - 
: 4 1 57 le 
: 6 4 57 drar 
: 10 1 59 sin 
: 12 5 59 kos 
- 17 18
: 0 2 61 Og 
: 3 4 61 bryr 
: 8 1 62 seg 
: 10 3 62 knapt 
: 15 2 64 om 
: 19 12 64 det 
- 36 42
: 0 1 60 At 
: 2 2 60 to 
: 5 2 62 skal 
: 9 2 64 bli 
: 13 10 64 tre 
- 26 29
: 0 1 52 Sĺ 
: 1 2 52 e 
: 4 1 52 det 
: 5 3 57 pĺ - 
: 8 2 59 'an 
: 10 2 61 i - 
: 12 3 61 gjen 
- 25 38
: 0 2 61 Pĺ - 
: 2 1 61 'an 
: 3 3 59 i - 
: 6 5 59 gjen 
- 21 32
: 0 2 61 Pĺ - 
: 2 2 61 'an 
: 4 2 59 i - 
: 6 4 59 gjen 
- 20 31
: 0 2 59 Pĺ - 
: 2 2 59 'an 
: 4 2 57 i - 
: 6 4 57 gjen 
- 19 28
: 0 2 54 Ja 
: 2 1 54 det - 
: 3 2 54 'e 
: 5 2 57 pĺ - 
: 7 1 59 'an 
: 8 3 61 i - 
: 11 4 61 gjen 
- 25 36
: 0 2 61 Pĺ - 
: 2 2 61 'an 
: 4 3 59 i - 
: 7 4 59 gjen 
- 21 33
: 0 2 61 Pĺ - 
: 2 1 61 'an 
: 3 3 59 i - 
: 6 4 59 gjen 
- 20 31
: 0 2 59 Pĺ - 
: 2 2 59 'an 
: 4 2 57 i - 
: 6 6 57 gjen 
- 19 26
: 0 4 61 Side 
: 4 2 62 by 
: 6 4 64 side 
: 12 2 66 en 
: 15 7 64 klem 
- 27 33
: 0 3 61 Ens - 
: 3 2 62 lig 
: 5 4 64 mor 
: 10 2 61 har 
- 12 13
: 0 4 66 O - 
: 4 2 64 le 
: 7 2 64 i 
: 10 2 62 sitt 
: 14 3 61 hjem 
- 17 18
: 0 2 61 Hon 
: 3 2 61 e 
: 6 3 61 varm 
: 10 2 61 og 
: 13 3 61 kjćr - 
: 17 6 62 lig 
- 28 33
: 0 1 59 Han 
: 1 1 59 e 
: 3 1 59 bĺ - 
: 5 4 59 de 
: 10 1 59 řm 
: 12 1 59 og 
: 13 2 59 ćr - 
: 16 8 61 lig 
- 26 28
: 0 2 64 Alt 
: 3 2 64 kan 
: 7 2 64 de 
: 10 3 62 visst 
: 14 3 61 snak - 
: 18 3 62 ke 
: 21 5 59 om 
- 29 32
: 0 1 57 Og 
: 2 2 59 mid - 
: 4 2 57 dels 
: 7 3 59 dei - 
: 11 1 61 lig' 
: 13 3 59 mid - 
: 16 3 57 dels 
: 20 2 61 dum 
- 22 24
: 0 1 61 Gĺr 
: 1 2 62 de 
: 4 3 64 arm 
: 8 2 66 i 
: 10 5 64 arm 
- 21 27
: 0 2 61 Ut 
: 3 2 62 pĺ 
: 6 2 64 jobb 
: 9 3 61 i 
: 13 2 66 ver - 
: 16 3 64 dens 
: 20 4 64 liv 
: 25 3 62 og 
: 29 2 61 larm 
- 31 32
: 0 2 61 Men 
: 2 2 61 mor 
: 5 2 61 vil 
: 7 2 61 gĺ 
: 10 2 61 pĺ 
: 14 2 61 sko - 
: 17 6 62 ler 
- 27 32
: 0 2 59 Og 
: 2 2 59 pĺ 
: 5 2 59 fest 
: 8 2 59 i 
: 10 2 59 tran - 
: 12 1 59 ge 
: 14 3 59 kjo - 
: 18 5 61 ler 
- 24 26
: 0 1 61 Og 
: 2 2 64 far 
: 5 2 61 fĺr 
: 8 2 64 nyss 
: 11 3 61 om 
: 15 3 61 fyll 
: 19 2 62 og 
: 21 1 62 hor - 
: 22 2 59 - 
- 29 35
: 0 1 61 Ja 
: 1 2 59 der - 
: 4 2 57 med 
: 6 2 59 e 
: 8 1 61 det 
: 10 6 59 ut 
: 17 2 57 med 
: 20 11 57 mor 
- 33 36
: 0 2 56 Og 
: 2 2 57 o - 
: 5 3 57 le 
: 9 3 57 e 
: 14 3 57 bar - 
: 17 2 56 ne - 
: 20 2 56 vakt 
- 22 24
: 0 1 57 El - 
: 1 2 57 ler 
: 4 1 57 pĺ 
: 6 3 57 by'n 
: 9 2 56 som 
: 12 2 56 slakt 
- 14 16
: 0 2 57 Men 
: 2 2 57 pĺ 
: 4 1 57 en 
: 7 3 57 bar 
- 10 11
: 0 1 59 Fĺr 
: 2 3 59 han 
: 6 1 61 en 
: 8 4 61 ny 
: 13 2 59 kon - 
: 16 2 59 takt 
- 18 20
: 0 1 57 Og 
: 1 3 57 hon 
: 5 1 57 e 
: 6 2 57 lik - 
: 10 2 56 e 
: 13 2 56 ung 
- 15 17
: 0 2 57 Som 
: 2 2 57 gam - 
: 4 1 57 le - 
: 6 3 57 far 
: 9 2 56 e 
: 12 2 56 tung 
- 14 16
: 0 1 57 Men 
: 1 1 57 hon 
: 2 1 57 har 
: 4 3 57 sin 
: 7 2 57 eg - 
: 10 2 59 en 
: 13 4 59 vin 
- 17 19
: 0 2 61 Ba - 
: 3 3 61 re 
: 6 2 62 sĺ 
: 9 3 62 det 
: 15 3 64 e 
: 18 3 64 sagt 
- 29 37
: 0 1 60 Og 
: 2 2 60 hon 
: 5 2 60 tren - 
: 7 1 62 ger 
: 9 3 64 en 
: 13 12 64 venn 
- 28 32
: 0 1 52 Sĺ 
: 1 1 52 e 
: 3 2 52 det 
: 6 2 57 pĺ - 
: 8 1 59 'an 
: 9 3 61 i - 
: 12 4 61 gjen 
- 26 37
: 0 3 61 Pĺ - 
: 3 2 61 'an 
: 5 2 59 i - 
: 7 3 59 gjen 
- 20 32
: 0 2 61 Pĺ - 
: 2 2 61 'an 
: 4 3 59 i - 
: 7 3 59 gjen 
- 20 31
: 0 2 59 Pĺ - 
: 2 3 59 'an 
: 5 2 57 i - 
: 7 4 57 gjen 
- 21 32
: 0 2 54 Det'e 
: 2 2 57 pĺ - 
: 4 1 59 'an 
: 5 2 61 i - 
: 7 5 61 gjen 
- 22 32
: 0 2 61 Pĺ - 
: 2 3 61 'an 
: 5 3 59 i - 
: 8 5 59 gjen 
- 22 32
: 0 2 61 Pĺ - 
: 2 3 61 'an 
: 5 3 59 i - 
: 8 4 59 gjen 
- 22 33
: 0 2 59 Pĺ - 
: 2 2 59 'an 
: 4 3 57 i - 
: 7 5 57 gjen 
- 20 28
: 0 1 49 Slik 
: 2 2 50 gĺr 
: 4 2 52 ĺr 
: 7 2 54 et - 
: 9 2 49 ter 
: 12 6 52 ĺr 
- 26 35
: 0 4 49 Tid 
: 4 2 50 pĺ 
: 6 2 52 gam - 
: 8 3 49 le - 
: 12 3 54 hei - 
: 15 2 52 men 
- 17 18
: 0 3 52 For - 
: 3 2 50 es - 
: 6 3 50 tĺr 
: 10 1 49 selv 
: 11 2 49 om 
: 14 3 49 O - 
: 18 1 49 le 
- 19 20
: 0 1 49 Fin - 
: 1 1 49 ner 
: 3 3 49 and - 
: 7 4 50 re 
: 19 2 47 selv 
: 21 1 47 om 
: 23 2 47 man 
: 25 2 47 gikk 
- 27 28
: 0 3 47 Fra 
: 4 2 47 hver - 
: 7 2 47 an - 
: 10 4 49 dre 
- 20 26
: 0 1 52 Kan 
: 1 3 49 han 
: 5 4 52 her 
: 10 2 50 fĺ 
: 13 2 49 se 
: 16 1 50 i - 
: 18 3 47 gjen 
- 26 32
: 0 2 45 Sin 
: 3 2 47 fřr - 
: 5 1 49 ste 
: 7 4 47 hjer - 
: 15 3 45 te - 
: 18 4 45 venn 
- 32 79
: 0 2 64 Sĺ 
: 2 2 64 e 
: 5 2 64 det 
: 7 2 69 pĺ - 
: 9 2 71 'an 
: 11 2 73 i - 
: 13 4 73 gjen 
- 27 39
: 0 2 73 Pĺ - 
: 2 2 73 'an 
: 4 2 71 i - 
: 6 5 71 gjen 
- 21 32
: 0 2 73 Pĺ - 
: 2 2 73 'an 
: 4 2 71 i - 
: 6 5 71 gjen 
- 21 32
: 0 2 71 Pĺ - 
: 2 2 71 'an 
: 4 2 69 i - 
: 6 6 69 gjen 
- 21 30
: 0 2 66 Det'e 
: 2 2 69 pĺ - 
: 4 2 71 'an 
: 6 2 73 i - 
: 8 6 73 gjen 
- 23 33
: 0 2 73 Pĺ - 
: 2 2 73 'an 
: 4 2 71 i - 
: 6 8 71 gjen 
- 23 32
: 0 2 73 Pĺ - 
: 2 2 73 'an 
: 4 3 71 i - 
: 7 11 71 gjen 
- 25 32
: 0 2 71 Pĺ - 
: 2 2 71 'an 
: 4 2 69 i - 
: 6 6 69 gjen 
E

Converted and edited by JakobO.
Thanks to Zeildo for the PACK_EE.PAK-file.
Heia Norge!

This file was converted from XML using
GOSU converter v1.0a ENG by Cloud aka Chmurek
