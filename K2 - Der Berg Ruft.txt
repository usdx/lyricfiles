#TITLE:Der Berg Ruft
#ARTIST:K2
#LANGUAGE:Deutsch
#EDITION:SingStar Aprčs-Ski Party
#GENRE:Pop
#YEAR:1994
#MP3:K2 - Der Berg Ruft.mp3
#COVER:K2 - Der Berg Ruft [CO].jpg
#VIDEO:K2 - Der Berg Ruft [VD#0].avi
#VIDEOGAP:0
#BPM:278,8
#GAP:4573,17
F 0 1 45 I
F 2 1 45  muss
F 5 11 45  aufi
- 18 19
F 34 7 46 Vater
F 42 1 46  i
F 44 2 46  muss
F 47 13 46  aufi
- 62 63
F 74 8 45 Schau
F 83 2 45  der
F 86 7 45  Berg
- 95 96
F 122 1 46 I
F 124 1 46  muss
F 126 8 46  aufi
- 136
F 142 7 45 Aufi
F 150 4 45  aufn
F 156 6 45  Berg
- 164 183
F 203 2 47 Der
F 207 5 47  Berg
F 216 11 47  ruft
- 229 337
F 357 2 48 Der
F 361 5 48  Berg
F 369 13 48  ruft
- 383
: 385 6 60 All
: 393 5 62  I
: 401 6 63  want
: 409 2 65  is
: 413 6 67  dan
: 421 2 67 cing
- 424
: 425 2 65 With
: 429 2 65  you
: 433 6 63  ba
: 441 4 65 by
- 447
: 449 6 60 All
: 457 5 62  I
: 465 6 63  want
: 473 2 65  is
: 477 5 67  dan
: 485 5 65 cing
- 492 493
: 513 6 60 All
: 521 5 62  I
: 529 6 63  want
: 537 2 65  is
: 541 6 67  dan
: 549 2 67 cing
- 552
: 553 2 65 With
: 557 2 65  you
: 561 6 63  ba
: 569 4 65 by
- 575
: 577 6 60 All
: 585 5 62  I
: 593 6 63  want
: 601 2 65  is
: 605 5 67  dan
: 613 5 65 cing
- 620 621
* 641 4 75 Ee
: 645 3 63  oh
: 649 3 63  lo
- 653
: 653 3 75 Woo
: 657 3 63  lo
: 661 3 63  lo
: 665 4 75  woo
: 669 2 72  ee
- 672
: 673 3 75 Ee
: 677 3 63  oh
: 681 3 63  lo
- 685
: 685 3 75 Woo
: 689 3 63  lo
: 693 2 63  lo
: 697 4 75  woo
: 701 2 72  ee
- 704
: 705 3 75 Ee
: 709 3 63  oh
: 713 3 63  lo
- 717
: 717 3 75 Woo
: 721 2 63  lo
: 725 3 63  lo
: 729 4 75  woo
: 733 2 72  ee
- 736
: 737 16 80 Ah
: 753 10 79 ~
- 765
: 769 4 75 Ee
: 773 3 63  oh
: 777 3 63  lo
- 781
: 781 3 75 Woo
: 785 3 63  lo
: 789 3 63  lo
: 793 4 75  woo
: 797 2 72  ee
- 800
: 801 3 75 Ee
: 805 3 63  oh
: 809 3 63  lo
- 813
: 813 3 75 Woo
: 817 3 63  lo
: 821 2 63  lo
: 825 4 75  woo
: 829 2 72  ee
- 832
: 833 3 75 Ee
: 837 3 63  oh
: 841 3 63  lo
- 845
: 845 3 75 Woo
: 849 2 63  lo
: 853 3 63  lo
: 857 4 75  woo
: 861 2 72  ee
- 864
* 865 6 80 Ah
- 872
F 873 2 59 Da
F 877 7 59  Hosenträger
F 885 1 59  is
F 887 1 59  ma
F 889 8 59  abgrissn
- 899 1007
: 1027 2 52 Ras
: 1031 2 54 ta
: 1035 2 52  from
: 1039 2 54  Ja
: 1043 2 52 mai
: 1047 2 54 ca
- 1050
: 1051 2 52 In
: 1055 2 54  Ba
: 1059 2 52 va
: 1063 2 50 ri
: 1067 2 50 an
: 1071 2 49  ex
: 1075 7 50 ile
- 1084
: 1091 2 52 Ras
: 1095 2 54 ta
: 1099 2 52  from
: 1103 2 54  Ja
: 1107 2 52 mai
: 1111 2 54 ca
- 1114
: 1115 2 52 Do
: 1119 2 54  the
: 1123 2 52  rag
: 1127 2 50 ga
: 1131 3 50  man
: 1139 4 49  style
- 1145
: 1147 2 54 And
: 1151 2 54  the
: 1155 2 52  peo
: 1159 2 54 ple
: 1163 2 52  from
: 1167 2 54  Ja
: 1171 2 52 mai
: 1175 2 54 ca
- 1178
: 1179 2 52 Won't
: 1183 2 54  you
: 1187 2 52  come
: 1191 4 50  out
: 1199 2 50  to
: 1203 7 50  play?
- 1212
: 1219 2 52 Peo
: 1223 2 54 ple
: 1227 2 52  from
: 1231 2 54  Ja
: 1235 2 52 mai
: 1239 2 54 ca
- 1242
: 1243 2 52 We
: 1247 2 54  can
: 1251 4 52  feel
: 1255 3 50 ~
: 1259 2 50 the
: 1263 2 50  reg
: 1267 5 49 gae
- 1273
* 1275 2 61 And
: 1279 2 54  the
: 1283 2 52  Mis
: 1287 2 54 ter
: 1291 6 52  D.
: 1299 2 52  Dek
: 1303 2 54 ker
- 1306
: 1307 2 52 Sang
: 1311 2 54  ''The
: 1315 2 52  Is
: 1319 2 50 ra
: 1323 2 50 e
: 1327 6 50 lites''
- 1335
: 1343 2 54 Ah
: 1347 2 52  Mis
: 1351 2 54 ter
: 1355 5 52  Bob
: 1363 3 52  Mar
: 1367 2 54 ley
- 1370
: 1371 2 52 Said
: 1375 2 54  ''No
: 1379 2 52  Wo
: 1383 2 50 man
: 1387 2 50  No
: 1391 5 49  Cry''
- 1397
: 1399 2 54 Ah
: 1403 2 54  Mis
: 1407 2 54 ter
: 1411 4 52  hey
: 1415 2 54 ~
: 1419 2 52 Mis
: 1423 2 54 ter
: 1427 2 52  D
: 1431 2 54  J
- 1434
: 1435 2 52 I've
: 1439 2 54  been
: 1443 3 52  watch
: 1447 2 50 in'
: 1451 2 50  your
: 1455 6 50  face
- 1463 1464
: 1475 4 52 Hey
* 1479 2 54 ~
: 1483 2 52 Mis
: 1487 2 54 ter
: 1491 2 52  D
: 1495 2 54  J
- 1498
: 1499 2 52 You
: 1503 2 54  can
: 1507 2 52  turn
: 1511 2 50  up
: 1515 2 50  the
: 1519 6 50  bass
- 1527 1622
F 1642 2 59 Da
F 1646 7 59  Hosenträger
F 1654 1 59  is
F 1656 1 59  ma
F 1658 7 59  abgrissn
- 1666
: 1667 6 60 All
: 1675 5 62  I
: 1683 6 63  want
: 1691 2 65  is
: 1695 6 67  dan
: 1703 2 67 cing
- 1706
: 1707 2 65 With
: 1711 2 65  you
: 1715 6 63  ba
: 1723 4 65 by
- 1729
: 1731 6 60 All
: 1739 5 62  I
: 1747 6 63  want
: 1755 2 65  is
: 1759 5 67  dan
: 1767 5 65 cing
- 1774 1775
: 1795 6 60 All
: 1803 5 62  I
: 1811 6 63  want
: 1819 2 65  is
: 1823 6 67  dan
: 1831 2 67 cing
- 1834
: 1835 2 65 With
: 1839 2 65  you
: 1843 6 63  ba
: 1851 4 65 by
- 1857
: 1859 6 60 All
: 1867 5 62  I
: 1875 6 63  want
: 1883 2 65  is
* 1887 5 67  dan
: 1895 5 65 cing
- 1902 1903
F 1924 1 56 Wie
F 1926 3 56  heißt
F 1930 2 56  der?
- 1934
F 1936 5 56 Berg
- 1943 1944
F 1952 2 57 Den
F 1957 2 57  kenn
F 1961 2 57  ich
F 1965 2 57  gar
F 1968 2 57  nicht
- 1972 1973
F 1987 1 56 Wie
F 1989 3 56  heißt
F 1993 2 56  der?
- 1997
F 1999 6 56 Berg
- 2007
F 2015 2 57 Den
F 2020 2 57  kenn
F 2024 2 57  ich
F 2028 2 57  gar
F 2031 2 57  nicht
- 2035 2036
F 2051 1 56 Wie
F 2053 3 56  heißt
F 2057 2 56  der?
- 2061
F 2063 5 56 Rhythmus
- 2070
F 2072 4 56 Berg
- 2077
F 2079 2 57 Den
F 2084 2 57  kenn
F 2088 2 57  ich
F 2092 2 57  gar
F 2095 2 57  nicht
- 2099 2100
F 2115 1 56 Wie
F 2117 3 56  heißt
F 2121 2 56  der?
- 2124
F 2124 2 56 Der
F 2127 5 56  Rhythmus
- 2134
F 2136 4 56 Berg
- 2142
F 2145 2 57 Sein
F 2151 5 57  Brudern
F 2158 2 57  glaub
F 2161 2 57  ich
F 2166 1 57  kenn
F 2168 2 57  ich
- 2172
: 2180 5 53 Boom
: 2188 2 53 sha
: 2192 3 53 ka
: 2196 2 52 la
: 2200 3 53 ka
- 2204
: 2204 2 53 In
: 2208 2 53  the
: 2212 3 53  dance
* 2218 3 50 hall
: 2224 2 50  to
: 2228 6 50 night
- 2236
: 2244 5 53 Boom
: 2252 2 53 sha
: 2256 3 53 ka
: 2260 2 52 la
: 2264 3 53 ka
- 2268
: 2268 2 53 We
: 2272 3 53  can
: 2276 2 53  make
: 2280 3 50  it
: 2284 6 50  al
: 2292 2 50 right
- 2295
F 2296 6 53 Alright
F 2304 2 53  now
- 2307
: 2308 4 53 Boom
: 2316 2 53 sha
: 2320 2 53 ka
: 2324 2 53 la
: 2328 2 53 ka
- 2331
: 2332 2 53 You
: 2336 4 53  can
* 2344 2 50  take
: 2348 2 50  it
: 2352 2 50  a
: 2356 8 50 way
: 2364 2 48  ah
- 2368
: 2372 4 53 Boom
: 2380 2 53 sha
: 2384 2 53 ka
: 2388 2 52 la
: 2392 2 53 ka
- 2395
: 2396 2 53 Got
: 2400 2 53  my
: 2404 2 53  sa
: 2408 2 50 la
: 2412 2 50 ry
: 2416 2 50  to
: 2420 7 50 day
- 2429
F 2436 2 47 Wos
F 2440 2 47  sogt
F 2445 2 47  er
- 2449
F 2454 11 47 Sellerie?
- 2467 2480
F 2500 2 47 Wos
F 2504 2 47  sogt
F 2509 2 47  er
- 2513
F 2518 11 47 Sellerie?
- 2531
F 2533 3 50 Wos
F 2539 2 50  sogt
F 2543 2 50  er
F 2546 2 50  da
F 2550 9 50  Rasta?
- 2561
F 2563 3 54 Shaka
F 2567 3 54  shaka
F 2571 1 54  boom
- 2573
F 2573 1 54 In
F 2575 1 54  a
F 2577 7 54  Bavarian
F 2585 6 54  exile
F 2592 2 54  ah
- 2595
F 2596 3 56 Shaka
F 2600 3 56  shaka
F 2604 1 56  boom
- 2606
F 2606 1 56 In
F 2608 1 56  a
F 2610 1 56  da
F 2612 6 56  rastaman
F 2620 3 56  style
F 2624 2 56  ah
- 2627
F 2627 1 53 Ba
F 2629 1 53  ba
F 2631 1 53  ba
F 2633 1 53  ba
F 2635 1 53  ba
F 2637 1 53  ba
F 2639 1 53  ba
- 2641
F 2641 7 53 Bavarian
F 2649 5 53  exile
F 2655 3 53  ah
- 2659
F 2659 1 54 Ba
F 2661 1 54  ba
F 2663 1 54  ba
F 2665 1 54  ba
F 2667 1 54  ba
F 2669 1 54  ba
F 2671 1 54  ba
F 2673 1 54  ba
- 2675
F 2675 6 54 Rastaman
F 2683 3 54  style
F 2687 2 54  ah
- 2690
F 2691 1 53 Ba
F 2693 1 53  ba
F 2695 1 53  ba
F 2697 1 53  ba
F 2699 1 53  ba
F 2701 1 53  ba
F 2703 1 53  ba
F 2705 1 53  ba
- 2707
F 2707 1 52 Ba
F 2709 1 52  ba
F 2711 1 52  ba
F 2713 1 52  ba
F 2715 1 52  ba
F 2717 1 52  ba
F 2719 1 52  ba
F 2721 1 52  ba
- 2723
F 2723 1 51 Ba
F 2725 1 51  ba
F 2727 1 51  ba
F 2729 1 51  ba
F 2731 1 51  ba
F 2733 1 51  ba
F 2735 1 51  ba
F 2737 1 51  ba
- 2739
F 2739 1 49 Ba
F 2741 1 49  ba
F 2743 1 49  ba
F 2745 1 49  ba
F 2747 1 49  ba
F 2749 1 49  ba
F 2751 1 49  ba
F 2753 1 49  ba
- 2755
F 2755 1 48 Ba
F 2757 1 48  ba
F 2759 1 48  ba
F 2761 1 48  ba
F 2763 1 48  ba
F 2765 1 48  ba
F 2767 1 48  ba
F 2769 1 48  ba
- 2771
F 2771 1 47 Ba
F 2773 1 47  ba
F 2775 1 47  ba
F 2777 1 47  ba
F 2779 1 47  ba
F 2781 1 47  ba
F 2783 1 47  ba
F 2785 1 47  ba
- 2787
F 2787 1 51 Jo
F 2789 1 51  wos
F 2792 2 51  hot
F 2795 1 51  er
F 2798 2 51  denn?
- 2801
F 2802 1 51 Jo
F 2804 1 51  wos
F 2807 2 51  hot
F 2810 1 51  er
F 2813 2 51  denn?
- 2817 2836
F 2856 2 55 Der
F 2860 5 55  Berg
F 2868 12 55  ruft
- 2882
: 2884 6 60 All
: 2892 5 62  I
* 2900 6 63  want
: 2908 2 65  is
: 2912 6 67  dan
: 2920 2 67 cing
- 2923
: 2924 2 65 With
: 2928 2 65  you
: 2932 6 63  ba
: 2940 4 65 by
- 2946
: 2948 6 60 All
: 2956 5 62  I
: 2964 6 63  want
: 2972 2 65  is
: 2976 5 67  dan
: 2984 5 65 cing
- 2991 2992
: 3012 6 60 All
: 3020 5 62  I
: 3028 6 63  want
: 3036 2 65  is
: 3040 6 67  dan
: 3048 2 67 cing
- 3051
: 3052 2 65 With
: 3056 2 65  you
* 3060 6 63  ba
: 3068 4 65 by
- 3074
: 3076 6 60 All
* 3084 5 62  I
: 3092 6 63  want
: 3100 2 65  is
: 3104 5 67  dan
: 3112 5 65 cing
- 3118
F 3119 2 59 Da
F 3123 7 59  Hosenträger
F 3131 1 59  is
F 3133 1 59  ma
F 3135 5 59  abgrissn
- 3141
F 3142 13 68 Woo
- 3157
F 3162 2 67 He
F 3166 8 67  he
- 3176 3177
F 3201 2 65 Die
F 3205 6 65  Platte
F 3213 2 65  rott
F 3217 2 65  ich
F 3221 2 65  aus
- 3225
F 3232 1 61 Es
F 3234 2 61  geht
F 3237 3 61  immer
F 3241 2 61  wieder
F 3244 2 61  von
F 3248 2 61  vorn
F 3252 3 61  an
- 3257
F 3265 2 63 Die
F 3269 6 63  Platte
F 3277 2 63  rott
F 3281 2 63  ich
F 3285 2 63  aus
- 3289
F 3297 2 62 Die
F 3301 6 62  Platte
F 3309 2 62  rott
F 3313 2 62  ich
F 3317 2 62  aus
- 3321
F 3329 2 61 Die
F 3333 6 61  Platte
F 3341 2 61  rott
F 3345 2 61  ich
F 3349 2 61  aus
- 3353
F 3361 2 60 Die
F 3365 6 60  Platte
F 3373 2 60  rott
F 3377 2 60  ich
F 3381 2 60  aus
- 3385 3386
F 3394 2 61 Die
F 3399 1 61  kann
F 3401 1 61  ich
F 3403 2 61  nicht
F 3406 2 61  mehr
F 3409 3 61  hören
- 3413
F 3413 1 62 Die
F 3415 2 62  wächst
F 3418 2 62  mir
F 3421 1 62  zum
F 3423 3 62  Hals
F 3427 2 62  raus
- 3431
F 3435 2 63 Zum
F 3438 4 63  Kotzen
F 3443 2 63  is
F 3447 2 63  des
- 3450
F 3450 1 64 Ja
F 3452 5 64  erlauben
F 3458 1 64  sie
F 3460 1 64  mir
- 3462
F 3462 1 65 A
F 3463 4 65  schönes
F 3468 3 65  Lied
- 3472
F 3473 3 66 Aber
F 3477 1 66  wenn
F 3479 2 66  ma's
F 3482 1 66  so
F 3484 2 66  oft
F 3488 3 66  hört
- 3493 3494
F 3504 3 67 Und
F 3509 3 67  glei
F 3513 7 67  nochmal
E
