#TITLE:Goodbye my lover
#ARTIST:James Blunt
#MP3:James Blunt - Goodbye my lover.mp3
#COVER:James Blunt - Goodbye my lover [CO].jpg
#VIDEO:James Blunt - Goodbye my lover [VD#0].mp4
#VIDEOGAP:0
#BPM:184
#GAP:21170
: 1 2 4 Did 
: 4 1 4 I 
: 5 2 4 dis
: 8 1 4 ap
: 9 3 6 point 
: 15 3 4 you 
- 21
: 31 1 4 or 
: 32 3 4 let 
: 36 1 3 you 
: 38 4 4 down
- 45
: 62 2 1 Should 
: 64 2 3 I 
: 66 1 4 be 
: 68 3 3 feel
: 72 2 1 ing 
: 74 4 -3 guil
: 78 3 -3 ty 
- 84
: 93 1 1 or 
: 94 2 3 let 
: 97 1 3 the 
: 98 5 4 judges 
: 106 4 3 frown
- 113
: 121 1 4 Cause 
: 122 2 4 I 
: 124 6 6 saw 
: 131 1 6 the 
: 133 2 4 end 
- 138
: 147 1 -1 be
: 148 4 -1 fore 
: 153 2 3 we'd 
: 156 1 -1 be
: 158 3 -4 gun
- 164
: 172 2 -4 Yes 
: 175 2 -4 I 
: 179 4 4 saw 
: 184 3 3 you 
: 188 2 3 were 
: 191 4 -1 blinded 
- 198
: 203 2 -3 and 
: 205 4 4 I 
: 210 4 3 knew 
: 215 3 -1 I 
: 218 3 -1 had 
: 221 4 -1 won
- 228
: 237 2 -1 So 
: 239 2 -1 I 
: 241 2 4 took 
: 245 5 6 whats 
: 252 6 4 mine 
- 261
: 269 2 4 by 
: 271 3 -1 eter
: 276 6 4 nal 
: 284 4 3 right
- 291
: 310 2 1 Took 
: 313 2 1 your 
: 317 4 4 soul 
: 324 3 1 out 
- 330
: 334 2 3 in
: 337 3 3 to 
: 341 2 -1 the 
: 344 4 -1 night
- 351
: 369 2 4 It 
: 373 2 4 may 
: 376 1 4 be 
: 379 3 6 o
: 383 2 4 ver 
- 388
: 393 1 3 but 
: 394 1 3 it 
: 397 2 -1 wont 
: 403 3 -1 stop 
: 409 2 -4 there
- 414
: 420 2 -4 I 
: 423 2 4 am 
: 427 3 3 here 
: 432 5 3 for 
: 440 2 -1 you 
- 445
: 450 2 -1 if 
: 454 3 -4 you'd 
: 458 3 4 on
: 462 4 3 ly 
: 470 4 -1 care
- 477
: 488 1 4 You 
: 489 3 4 touched 
: 494 2 4 my 
: 496 5 4 heart 
: 501 1 4 you 
: 503 3 4 touched 
: 507 1 4 my 
: 510 5 4 soul
- 517
: 520 1 -1 You 
: 521 3 4 changed 
: 525 2 4 my 
: 528 3 4 life 
: 533 1 3 and 
: 535 2 4 all 
: 538 2 4 my 
: 542 3 4 goals
- 547
: 550 1 4 And 
: 552 2 3 love 
: 555 1 4 is 
: 556 4 4 blind 
: 561 2 3 and 
: 564 2 4 that 
: 567 2 4 I 
: 570 2 4 knew 
: 573 2 -1 when
- 578
: 586 2 3 My 
: 589 3 4 heart 
: 594 1 4 was 
: 596 3 4 blind
: 599 2 4 ed 
: 602 5 4 by 
: 609 1 6 you
- 610
: 611 1 -1 I've 
: 613 2 4 kissed 
: 616 2 4 your 
: 619 2 4 lips 
: 624 1 4 and 
: 626 4 4 held 
: 631 2 4 your 
: 634 4 4 head
- 641
: 644 2 -1 Shared 
: 647 2 4 your 
: 650 4 4 dreams 
: 655 1 4 and 
: 658 4 4 shared 
: 663 2 4 your 
: 666 3 4 bed
- 671
: 674 1 -1 I 
: 676 2 3 know 
: 679 1 3 you 
: 681 4 3 well 
: 687 1 3 I 
: 688 2 3 know 
: 692 2 4 your 
: 696 4 3 smell 
- 703
: 711 2 3 I've 
: 714 3 4 been 
: 718 1 4 ad
: 719 2 4 dict
: 722 2 -1 ed 
: 726 6 6 to 
: 735 5 4 you
- 743
: 748 1 4 Good
: 750 5 6 bye 
: 757 2 3 my 
: 760 2 3 lo
: 763 4 4 ver 
- 770
: 780 1 4 Good
: 782 6 4 bye 
: 789 1 4 my 
: 791 6 1 friend
- 800
: 807 1 6 You 
: 809 2 6 have 
: 813 1 4 been 
: 815 1 4 the 
: 816 3 4 one 
- 822
: 837 1 4 You 
: 839 3 4 have 
: 843 2 8 been 
: 846 1 6 the 
: 847 2 6 one 
: 851 5 4 for 
: 858 3 4 me
- 864
: 873 1 8 Good
: 876 5 8 bye 
: 883 1 6 my 
: 885 2 6 lo
: 888 4 4 ver 
- 895
: 903 1 4 Good
: 906 5 8 bye 
: 912 2 6 my 
: 916 5 4 friend
- 924
: 931 1 4 You 
: 933 2 8 have 
: 937 1 6 been 
: 939 1 4 the 
: 941 4 4 one 
- 948
: 962 1 4 You 
: 964 2 8 have 
: 967 2 6 been 
: 969 1 4 the 
: 971 3 4 one 
: 976 4 6 for 
: 982 5 4 me
- 1011
: 1055 1 -1 I 
: 1057 2 4 am 
: 1059 1 4 a 
: 1061 4 6 dream
: 1066 4 4 er 
- 1073
: 1082 1 -1 but 
: 1086 1 -1 when 
: 1089 1 4 I 
: 1092 3 3 wake
- 1098
: 1117 1 1 You 
: 1119 3 1 can't 
: 1123 2 4 break 
: 1127 1 4 my 
: 1130 3 1 spir
: 1135 1 4 it 
- 1139
: 1149 1 3 it's 
: 1151 1 3 my 
: 1154 3 -1 dreams 
: 1159 1 -1 you 
: 1162 3 -1 take
- 1168
: 1177 1 -1 And 
: 1180 1 4 as 
: 1181 2 4 you 
: 1185 3 6 move 
: 1190 3 4 on 
- 1196
: 1207 1 3 re
: 1209 3 3 mem
: 1214 2 -1 ber 
: 1217 3 -4 me
- 1223
: 1237 2 -4 Re
: 1241 2 4 mem
: 1244 2 4 ber 
: 1249 2 3 us 
: 1252 1 -1 and 
: 1255 3 -1 all 
: 1260 3 -4 we 
: 1268 5 4 used 
: 1275 1 3 to 
: 1278 2 -1 be
- 1283
: 1292 1 4 I've 
: 1294 3 4 seen 
: 1298 1 4 you 
: 1300 4 4 cry 
: 1306 1 4 I've 
: 1308 3 4 seen 
: 1312 1 4 you 
: 1315 4 4 smile
- 1321
: 1324 1 -1 I've 
: 1326 2 4 watched 
: 1330 1 4 you 
: 1332 3 4 sleep
: 1337 1 4 ing 
: 1340 2 4 for 
: 1343 2 4 a 
: 1347 4 4 while
- 1353
: 1356 1 4 I'd 
: 1358 1 3 be 
: 1359 1 4 the 
: 1362 2 4 fa
: 1366 2 3 ther 
: 1370 2 4 of 
: 1373 3 4 your 
: 1378 4 4 child
- 1385
: 1391 1 -1 Id 
: 1394 4 -1 spend 
: 1399 1 3 a 
: 1400 3 4 life
: 1404 3 4 time 
: 1409 5 4 with 
: 1415 1 6 you
- 1416
: 1417 1 4 I 
: 1419 2 4 know 
: 1421 2 4 your 
: 1425 3 4 fears 
: 1431 1 4 and 
: 1432 2 3 you 
: 1434 3 4 know 
: 1438 5 4 mine
- 1446
: 1449 1 -1 We've 
: 1450 1 -1 had 
: 1452 2 4 our 
: 1455 3 4 doubts 
: 1460 1 4 but 
* 1462 6 4 now 
: 1468 3 4 we're 
: 1472 5 4 fine
- 1477
: 1477 2 3 And 
: 1481 1 3 I 
: 1483 2 3 love 
: 1487 3 3 you 
: 1492 1 3 I 
: 1494 3 3 swear 
: 1498 2 4 that's 
: 1503 5 3 true
- 1511
: 1516 2 4 I 
: 1520 2 4 can
: 1522 2 4 not 
: 1525 4 4 live 
: 1535 2 -1 with
: 1539 4 6 out 
: 1545 5 4 you
- 1552
: 1554 1 4 Good
: 1557 5 6 bye 
: 1564 1 3 my 
: 1566 2 3 lo
: 1570 3 4 ver 
- 1576
: 1585 1 4 Good
: 1588 4 4 bye 
: 1594 1 4 my 
: 1598 5 4 friend
- 1606
: 1612 1 4 You 
: 1614 3 6 have 
: 1618 2 4 been 
: 1620 1 4 the 
: 1623 2 4 one 
- 1628
: 1644 1 4 You 
: 1645 2 4 have 
: 1648 2 8 been 
: 1652 1 6 the 
: 1653 2 6 one 
: 1657 5 4 for 
: 1664 3 4 me
- 1670
: 1678 1 4 Good
: 1681 5 6 bye 
: 1688 1 6 my 
: 1690 2 4 lo
: 1693 3 6 ver 
- 1699
: 1709 1 4 Good
: 1712 4 8 bye 
: 1718 1 6 my 
: 1722 4 4 friend
- 1729
: 1737 1 4 You 
: 1739 2 8 have 
: 1742 2 8 been 
: 1745 1 6 the 
: 1748 2 4 one 
- 1753
: 1768 1 4 You 
: 1770 2 8 have 
: 1774 2 6 been 
: 1776 1 4 the 
: 1778 3 4 one 
: 1783 3 6 for 
: 1789 3 4 me
- 1816
: 1920 1 11 And 
: 1923 1 11 I 
* 1927 8 8 still 
- 1938
: 1947 1 8 hold 
: 1950 3 4 your 
: 1957 4 4 hand 
: 1970 1 -4 in 
: 1974 3 -1 mine 
- 1980
: 2001 1 -1 In 
: 2006 2 -1 mine 
: 2009 1 -1 when 
: 2012 2 -1 I'm 
: 2015 1 1 a
: 2016 4 1 sleep
- 2032
: 2046 1 4 And 
: 2047 1 11 I 
* 2051 9 11 will 
: 2070 3 8 bare 
: 2074 2 8 my 
: 2081 5 4 soul 
- 2089
: 2093 2 4 in 
: 2097 6 -1 time 
- 2106
: 2124 2 -4 When 
: 2127 2 -1 I'm 
: 2132 3 -1 kneel
: 2136 1 -1 ing 
: 2138 2 -1 at 
: 2141 6 1 your 
: 2153 3 -1 feet
- 2159
: 2174 1 4 Good
: 2176 5 6 bye 
: 2183 1 3 my 
: 2186 1 3 lo
: 2188 3 4 ver 
- 2194
: 2205 1 4 Good
: 2208 4 4 bye 
: 2213 2 4 my 
: 2217 5 4 friend
- 2225
: 2232 1 4 You 
: 2234 2 6 have 
: 2239 1 4 been 
: 2241 1 4 the 
: 2244 2 4 one 
- 2249
: 2264 1 4 You 
: 2266 1 4 have 
: 2269 1 8 been 
: 2272 1 6 the 
: 2273 2 6 one 
: 2278 4 4 for 
: 2284 2 4 me
- 2289
: 2299 1 5 Good
: 2301 5 8 bye 
: 2307 2 6 my 
: 2310 2 6 lo
: 2313 5 4 ver 
- 2321
: 2329 1 4 Good
: 2331 6 8 bye 
: 2338 2 6 my 
: 2342 5 4 friend
- 2350
: 2357 1 4 You 
: 2359 1 8 have 
: 2362 1 6 been 
: 2364 1 4 the 
: 2367 2 6 one 
- 2372
: 2387 1 4 You 
: 2389 2 4 have 
: 2393 2 8 been 
: 2396 1 4 the 
: 2398 1 4 one 
: 2402 3 6 for 
: 2408 3 4 me
- 2412
: 2414 1 6 I'm 
: 2418 5 6 so 
: 2426 2 4 hol
: 2428 3 1 low 
: 2432 1 4 ba
: 2433 4 4 by 
- 2440
: 2445 1 4 I'm 
: 2449 5 8 so 
: 2457 3 6 hol
: 2461 5 4 low
- 2469
: 2476 1 4 I'm 
: 2481 5 8 so 
: 2492 1 4 I'm 
: 2496 5 8 so 
- 2504
: 2507 1 4 I'm 
: 2510 5 8 so 
: 2519 2 6 hol
: 2523 3 4 low
- 2529
: 2539 1 1 I'm 
: 2542 4 8 so 
: 2549 2 6 hol
: 2552 3 6 low 
: 2556 1 1 ba
: 2558 4 4 by 
- 2565
: 2569 1 4 I'm 
: 2573 4 8 so 
: 2582 2 6 hol
: 2585 4 4 low
- 2592
: 2600 1 8 I'm 
: 2603 4 8 so 
: 2616 1 4 I'm 
: 2620 5 8 so 
- 2628
: 2632 1 4 I'm 
: 2636 4 8 so 
: 2645 2 6 hol
: 2648 4 4 low
E
