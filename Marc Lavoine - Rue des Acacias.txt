#TITLE:Rue des Acacias
#ARTIST:Marc Lavoine
#MP3:Marc Lavoine - Rue des Acacias.mp3
#VIDEO:Marc Lavoine - Rue des Acacias.mp4
#COVER:Marc Lavoine - Rue des Acacias.jpg
#BPM:226,4
#GAP:18397
#ENCODING:UTF8
#LANGUAGE:French
#GENRE:Pop
#YEAR:2010
#CREATOR:mustangfred and dsp14
: 0 2 5 Elle
: 4 1 5  n'é
: 8 1 5 tait
: 12 1 5  pas
: 15 4 4  ma
* 20 3 5  voi
* 25 1 5 sine
* 27 2 7 ~
- 31
: 33 1 7 Elle
: 37 2 7  vi
: 40 2 7 vait
: 45 2 7  dans
: 48 1 5  les
: 53 2 7  col
: 56 2 9 lines
* 60 3 9  lŕ-
* 72 2 2 haut
- 84
: 92 2 10 Rue
: 96 2 9  des
: 101 1 7  Au
: 104 1 5 bé
: 108 8 4 pines
- 125
: 128 2 5 Je
: 132 1 5  n'é
: 136 1 5 tais
: 140 1 5  pas
: 144 3 4  fier
: 148 2 5  ŕ
* 151 3 5  bras
* 155 1 7 ~
- 158
: 160 1 7 Et
: 164 2 7  chez
: 167 3 7  nous
: 172 1 7  il
: 176 3 5  fai
: 180 1 7 sait
: 184 1 9  froid,
* 188 3 9  en
* 200 2 2  bas
- 212
* 220 3 7 Rue
* 224 3 5  des
* 228 1 4  A
* 232 1 5 ca
* 236 4 2 cias
- 249
: 252 2 10 On
: 256 1 10  se
: 260 3 10  croi
: 264 3 10 sait
: 268 1 10  de
: 272 3 10  temps
: 276 3 10  en
: 280 5 9  temps
- 300
: 316 3 7 A
: 320 3 7 lors
: 324 2 7  on
: 328 1 7  mar
: 333 1 7 chait
: 336 3 7  dans
: 340 1 7  les
: 345 3 5  champs
- 363
: 380 3 10 Au
* 384 2 10  mi
* 387 3 10 lieu
: 392 1 10  des
: 396 3 10  fleurs
: 400 1 10  du
: 404 3 10  prin
: 408 4 9 temps
- 427
: 444 1 4 Qui
: 448 1 4  chassent
: 452 2 4  au
: 455 3 4  loin
: 460 2 4  tous
: 463 2 7  les
: 468 3 5  tour
: 472 3 4 ments
- 485
: 493 2 9 Je
: 496 3 7  l'ai
: 500 3 5 mais
: 505 2 4  tant,
: 509 2 9  je
: 513 2 7  l'ai
: 516 3 5 mais
: 521 1 4  tant
- 523
: 525 2 9 Je
: 528 3 7  l'ai
: 532 2 5 mais
: 537 2 4  tant
- 569
: 672 1 -3 Elle
: 674 1 2  rę
: 676 1 5 vait
: 678 1 5  d'An
: 680 3 5 na
: 689 2 4  Ka
: 693 3 5 ré
: 698 1 5 nine
: 700 1 7 ~
- 703
: 705 1 7 Et
: 709 2 7  de
: 713 1 7  robes
: 717 2 7  ŕ
: 721 2 5  cri
: 724 3 7 no
: 728 2 9 line
: 732 3 9  lŕ-
: 745 3 2 haut
- 760
: 764 3 10 Rue
: 768 2 9  des
: 772 3 7  Au
: 776 2 5 bé
: 781 8 4 pines
- 797
: 800 2 5 Je
: 803 1 5  n'é
: 808 1 5 tais
: 813 1 5  pas
: 817 2 4  sűr
: 820 2 5  de
* 823 2 5  moi
* 826 1 7 ~
- 830
: 832 1 7 Dieu
: 837 2 7  que
: 840 1 7  j'é
: 844 3 7 tais
: 848 3 5  ma
: 852 3 7 la
: 856 2 9 droit
: 861 3 9  en
: 872 3 2  bas
- 885
: 892 3 7 Rue
: 896 3 5  des
: 900 1 4  A
: 904 1 5 ca
: 908 5 2 cias
- 920
: 923 5 10 Les
: 929 2 10  an
: 932 2 10 nées
: 936 1 10  pas
: 941 2 10 saient
: 945 1 10  dou
: 949 2 10 ce
: 952 4 9 ment
- 971
: 988 2 7 On
: 993 2 7  se
* 996 3 7  re
* 1000 1 7 gar
* 1004 3 7 dait
: 1009 2 7  gen
: 1013 2 7 ti
: 1016 4 5 ment
- 1035
: 1053 3 10 Mais
: 1057 2 10  la
: 1061 3 10  neige
: 1065 3 10  et
: 1069 1 10  le
: 1072 3 10  mau
: 1077 2 10 vais
: 1081 3 9  vent
- 1099
: 1117 1 4 Chassent
: 1121 2 4  au
: 1124 3 4  loin
: 1129 3 4  les
: 1133 2 4  rę
: 1136 3 7 ves
: 1141 1 5  d'en
: 1145 4 4 fants
- 1161
: 1165 1 9 On
: 1169 3 7  s'ai
: 1173 1 5 mait
: 1177 2 4  tant,
: 1182 1 9  on
: 1185 3 7  s'ai
: 1189 1 5 mait
: 1193 1 4  tant,
- 1195
: 1197 1 9 On
: 1200 3 7  s'ai
: 1204 1 5 mait
: 1209 3 4  tant
- 1242
: 1345 1 5 Elle
: 1348 1 5  s'est
: 1352 1 5  cou
: 1357 1 5 chée
: 1360 3 4  dans
: 1364 1 5  le
: 1368 2 5  spleen
* 1371 3 7 ~
- 1375
: 1377 2 7 D'un
: 1381 3 7  ca
: 1385 1 7 na
: 1389 2 7 pé
: 1393 2 5  bleu
: 1396 3 7  ma
: 1400 4 9 rine
- 1410
: 1413 2 9 Lŕ-
: 1416 4 2 haut
- 1430
: 1437 2 10 Rue
: 1441 2 9  des
: 1445 1 7  Au
: 1448 2 5 bé
: 1453 7 4 pines
- 1469
: 1473 3 5 Moi
: 1477 1 5  j'ai
: 1480 3 5  pleu
: 1484 2 5 ré
: 1489 1 4  plus
: 1493 2 5  que
: 1496 2 5  moi
: 1499 1 7 ~
- 1503
: 1505 1 7 Gla
: 1509 2 7 cé
: 1512 2 7  de
: 1517 2 7  peine
: 1520 4 5  et
: 1525 1 7  d'ef
: 1529 4 9 froi
- 1539
: 1541 2 9 En
: 1544 4 2  bas
- 1558
: 1565 2 7 Rue
: 1569 2 5  des
: 1573 1 4  A
: 1577 1 5 ca
: 1581 6 2 cias
- 1595
: 1598 3 10 Elle
: 1602 2 10  a
: 1605 1 10 vait
: 1609 1 10  peut
: 1613 3 10  ę
: 1617 2 10 tre
: 1621 3 10  vingt
: 1626 3 9  ans
- 1644
: 1661 1 7 Je
: 1664 2 7  m'en
: 1669 2 7  sou
: 1673 3 7 viens
: 1677 1 7  de
: 1681 2 7  temps
: 1685 2 7  en
: 1689 3 5  temps
- 1707
: 1724 3 10 Mais
: 1729 2 10  ja
: 1732 4 10 mais
: 1737 2 10  ne
: 1741 2 10  re
: 1745 3 10 vient
: 1749 1 10  le
: 1753 3 9  temps
- 1771
: 1789 1 4 Qui
: 1793 1 4  chasse
: 1797 2 4  au
: 1800 3 4  loin
: 1805 4 7  tout,
: 1817 2 5  tout
: 1821 5 4  l'temps
- 1834
: 1837 3 9 Je
: 1841 3 7  l'ai
: 1845 1 5 mais
: 1849 3 4  tant,
: 1854 2 9  je
: 1857 3 7  l'ai
: 1861 1 5 mais
: 1865 2 4  tant
- 1868
: 1870 2 9 Je
: 1873 3 7  l'ai
: 1877 1 5 mais
: 1881 3 4  tant
- 1914
: 2146 2 5 Elle
: 2149 2 5  n'é
: 2154 1 5 tait
: 2158 2 5  pas
: 2162 1 4  ma
: 2165 3 5  voi
: 2169 2 5 sine
: 2172 4 7 ~
- 2177
: 2179 1 7 Elle
: 2182 2 7  vi
: 2185 2 7 vait
: 2189 2 7  dans
: 2192 3 5  les
: 2198 2 7  col
: 2201 3 9 lines
: 2205 3 9  lŕ-
: 2217 2 2 haut
- 2229
: 2238 2 10 Rue
: 2241 3 9  des
: 2245 2 7  Au
: 2249 2 5 bé
: 2254 8 4 pines
- 2271
: 2274 2 5 Je
: 2277 1 5  n'é
: 2282 1 5 tais
: 2286 1 5  pas
: 2290 3 4  fier
: 2294 2 5  ŕ
: 2297 2 5  bras
: 2300 1 7 ~
- 2304
: 2306 1 7 Et
: 2310 2 7  chez
: 2313 3 7  nous
: 2318 2 7  il
: 2322 2 5  fai
: 2325 2 7 sait
: 2330 3 9  froid
- 2348
: 2366 2 7 Rue
: 2370 2 5  des
: 2373 1 4  A
: 2377 2 5 ca
: 2382 5 2 cias
- 2417
: 2621 3 7 Rue
: 2625 2 5  des
: 2629 2 4  A
: 2633 2 5 ca
: 2637 5 2 cias
- 2672
: 2878 2 7 Rue
: 2881 2 5  des
: 2886 1 4  A
: 2889 1 5 ca
: 2894 4 2 cias
E