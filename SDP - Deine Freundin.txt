#TITLE:Deine Freundin
#ARTIST:SDP
#MP3:SDP - Deine Freundin.mp3
#VIDEO:SDP - Deine Freundin.mp4
#COVER:SDP - Deine Freundin.jpg
#BPM:253.77
#GAP:12635
#ENCODING:UTF8
: 0 4 7 Ich
: 4 4 11  hat
: 8 4 11 te
: 12 4 11  dir
: 16 4 9  ver
: 20 4 7 sproch
: 24 4 7 en
: 28 4 7  ich
: 32 4 11  bring
: 36 4 12  dei
: 40 4 12 ne
: 44 4 12  Freun
: 48 4 11 din
: 52 6 9  heim
- 58
: 63 4 7 Das
: 67 4 9  das
: 71 4 9  hier
: 75 4 9  dann
: 79 4 9  pas
: 83 4 9 siert
: 87 4 9  ist
: 95 4 9  wirst
: 99 4 11  du
: 103 4 11  mir
: 107 4 11  nie
: 111 4 9  ver
: 115 4 7 zeih'n
- 123
: 128 4 7 Denn
: 132 4 11  du
: 136 3 11  hast
: 139 4 11  mir
: 143 5 9  ver
: 148 4 7 traut,
* 156 4 9  ich
* 160 4 11  hab
* 164 7 12  schei
* 171 4 12 ße
* 175 4 11  ge
* 179 6 9 baut
- 186
: 188 4 6 Jetzt
: 192 4 7  bring
: 196 4 9  ich
: 200 4 9  sie
: 204 4 9  dir
: 208 4 9  nach
: 212 4 9  Haus
: 220 3 7  völ
: 223 4 9 lig
: 227 4 11  fer
: 231 5 11 tig
: 236 3 11  und
: 239 5 9  zer
: 244 4 7 zaust
- 255
: 255 4 7 Wir
: 259 4 11  wa
: 263 4 11 ren
: 267 4 11  nur
: 271 4 9  zu
: 275 4 7  zweit
* 283 4 7  und
* 287 4 11  der
* 291 8 12  Weg
* 299 4 12  war
* 303 4 11  so
* 307 7 9  weit
- 317
: 319 4 7 Auf
: 323 4 9  ein
: 327 4 9 mal
: 331 4 9  blieb
: 335 4 9  sie
: 339 5 9  steh'n
: 347 4 7  und
: 351 4 9  dann
: 355 4 11  hat
: 359 4 11  sie's
: 363 4 11  mir
: 367 4 9  ge
: 371 4 7 zeigt
- 391
: 407 5 11 Dei
: 412 6 9 ne
: 418 7 7  Freun
: 425 7 2 din
: 432 6 11  die
: 438 7 9  kann
: 445 7 7  bla
: 452 5 2 sen
- 457
: 458 7 11 Die
: 465 6 9  kann
: 471 6 7  bla
: 477 6 4 sen,
: 483 6 11  bla
: 489 6 9 sen,
: 495 6 7  bla
: 501 5 4 sen
- 506
* 507 5 11 Die
* 512 6 12  kann
* 518 6 14  bla
* 524 5 14 sen,
* 529 5 14  bla
* 534 6 14 sen,
* 540 5 14  bla
* 545 5 14 sen
- 550
: 552 5 14 An
: 557 5 12  den
: 562 5 11  Fü
: 567 5 7 ßen
: 572 5 12  nicht
: 577 5 11  er
: 582 5 11 tra
: 587 5 7 gen
- 592
: 593 4 11 Dei
: 597 5 9 ne
: 602 5 7  Freun
: 607 5 2 din
: 612 5 11  ist
: 617 4 11  so
: 621 3 11  eng
- 624
: 626 5 11 Sie
: 631 4 11  ist
: 635 5 11  so
: 640 8 12  eng,
: 648 9 12  eng,
: 657 3 12  eng
- 661
: 662 4 12 Sie
: 666 4 12  ist
: 670 5 12  so
: 675 8 14  eng
: 683 8 14 e
: 691 4 14  Schu
: 695 4 12 he
: 699 4 11  nicht
: 703 4 12  ge
: 707 18 11 wöhnt
- 728
: 731 4 11 Dei
: 735 4 9 ne
: 739 4 7  Freun
: 743 4 2 din
: 747 4 11  die
: 751 4 9  kann
: 755 4 7  bla
: 759 3 2 sen
- 762
: 763 4 11 Die
: 767 4 9  kann
: 771 4 7  bla
: 775 4 2 sen,
: 779 4 11  bla
: 783 4 9 sen,
: 787 4 7  bla
: 791 3 2 sen
- 794
* 795 4 11 Die
* 799 4 12  kann
* 803 4 14  bla
* 807 4 14 sen,
* 811 4 14  bla
* 815 4 14 sen,
* 819 4 14  bla
* 823 3 14 sen
- 826
: 828 3 14 An
: 831 4 12  den
: 835 4 11  Fü
: 839 4 7 ßen
: 843 4 12  nicht
: 847 4 11  er
: 851 4 11 tra
: 855 3 7 gen
- 858
: 859 4 11 Dei
: 863 4 11 ne
: 867 4 11  Freun
: 871 4 11 din
: 875 4 11  ist
: 879 4 11  so
: 883 3 11  eng
- 887
: 887 4 11 Sie
: 891 4 11  ist
: 895 4 12  so
: 899 8 12  eng,
: 907 8 12  eng,
: 915 3 12  eng
- 918
: 919 4 12 Sie
: 923 4 12  ist
: 927 4 12  so
: 931 8 14  eng
: 939 8 14 e
: 947 4 14  Schu
: 951 4 12 he
: 955 4 11  nicht
: 959 4 12  ge
: 963 19 11 wöhnt
- 986
: 995 4 11 Ich
: 999 4 11  habs
: 1003 4 11  echt
: 1007 4 9  ver
: 1011 4 7 saut
: 1018 5 11  man
: 1023 4 11  sie
: 1027 4 12  ist
: 1031 4 12  doch
: 1035 4 12  dei
: 1039 4 11 ne
: 1043 7 9  Braut
- 1051
: 1051 4 7 Doch
: 1055 4 7  sie
: 1059 4 9  gab
: 1063 4 9  ein
: 1067 4 9 fach
: 1071 4 9  nicht
: 1075 5 9  auf
: 1083 4 7  und
: 1087 4 9  so
: 1091 4 11  nahm
: 1095 4 11  es
: 1099 4 11  sei
: 1103 4 9 nen
: 1107 6 7  Lauf
- 1118
: 1123 4 11 Klar
: 1127 4 11  bist
: 1131 4 11  du
: 1135 4 9  jetzt
: 1139 4 7  sau
: 1143 4 7 er
: 1147 4 9  a
: 1151 4 11 ber
: 1155 7 12  eins
: 1162 5 12  musst
: 1167 4 11  du
: 1171 8 9  seh'n
- 1181
: 1183 4 7 Es
: 1187 4 9  tat
: 1191 4 9  ihr
: 1195 4 9  ziem
: 1199 4 9 lich
: 1203 4 9  weh
: 1207 4 9  und
: 1211 4 7  auch
: 1215 4 9  für
: 1219 5 11  mich
: 1227 4 11  wars
: 1231 4 9  nicht
: 1235 7 7  schön
- 1243
: 1243 4 11 Ja
: 1247 4 11  der
: 1251 8 11  Weg
: 1259 4 11  war
: 1263 4 9  so
: 1267 5 7  weit
* 1274 5 9  und
* 1279 4 11  sie
* 1283 4 12  trug
* 1287 4 12  ein
* 1291 4 12  kur
* 1295 4 11 zes
* 1299 7 9  Kleid
- 1308
: 1310 4 7 Dann
: 1314 4 9  knie
: 1318 4 9 te
: 1322 4 9  sie
: 1326 4 9  vor
: 1330 5 9  mir
: 1338 4 7  und
: 1342 4 9  dann
: 1346 4 11  hat
: 1350 4 11  sie's
: 1354 4 11  mir
: 1358 4 9  ge
: 1362 5 7 zeigt
- 1382
: 1397 6 11 Dei
: 1403 7 9 ne
: 1410 6 7  Freun
: 1416 7 2 din
: 1423 6 11  die
: 1429 7 9  kann
: 1436 6 7  bla
: 1442 5 2 sen
- 1447
: 1449 6 11 Die
: 1455 7 9  kann
: 1462 6 7  bla
: 1468 6 2 sen,
: 1474 6 11  bla
: 1480 6 9 sen,
: 1486 6 7  bla
: 1492 4 2 sen
- 1496
* 1498 5 11 Die
* 1503 5 12  kann
* 1508 6 13  bla
* 1514 6 13 sen,
* 1520 5 13  bla
* 1525 6 13 sen,
* 1531 5 13  bla
* 1536 4 13 sen
- 1540
: 1543 5 14 An
: 1548 5 12  den
: 1553 6 11  Fü
: 1559 5 7 ßen
: 1564 5 12  nicht
: 1569 5 11  er
: 1574 5 11 tra
: 1579 3 7 gen
- 1582
: 1583 5 11 Dei
: 1588 5 9 ne
: 1593 5 7  Freun
: 1598 5 2 din
: 1603 5 11  ist
: 1608 5 11  so
: 1613 3 11  eng
- 1617
: 1617 5 11 Sie
: 1622 4 11  ist
: 1626 5 11  so
: 1631 9 12  eng,
: 1640 8 12  eng,
: 1648 3 12  eng
- 1651
: 1652 5 12 Sie
: 1657 4 12  ist
: 1661 4 12  so
: 1665 10 14  eng
: 1675 8 14 e
: 1683 4 14  Schu
: 1687 4 12 he
: 1691 4 11  nicht
: 1695 4 12  ge
: 1699 17 11 wöhnt
- 1718
: 1722 4 11 Dei
: 1726 4 9 ne
: 1730 4 7  Freun
: 1734 4 2 din
: 1738 4 11  die
: 1742 4 9  kann
: 1746 4 7  bla
: 1750 2 2 sen
- 1752
: 1754 4 11 Die
: 1758 4 9  kann
: 1762 4 7  bla
: 1766 4 2 sen,
: 1770 4 11  bla
: 1774 4 9 sen,
: 1778 4 7  bla
: 1782 3 2 sen
- 1785
* 1786 4 -1 Die
* 1790 4 0  kann
* 1794 4 2  bla
* 1798 4 2 sen,
* 1802 4 2  bla
* 1806 4 2 sen,
* 1810 4 2  bla
* 1814 3 2 sen
- 1817
: 1818 4 14 An
: 1822 4 12  den
: 1826 4 11  Fü
: 1830 4 7 ßen
: 1834 4 12  nicht
: 1838 4 11  er
: 1842 4 11 tra
: 1846 3 7 gen
- 1849
: 1850 4 -1 Dei
: 1854 4 -1 ne
: 1858 4 -1  Freun
: 1862 4 -1 din
: 1866 4 -1  ist
: 1870 4 -1  so
: 1874 2 -1  eng
- 1877
: 1878 4 11 Sie
: 1882 4 11  ist
: 1886 4 11  so
: 1890 8 12  eng,
: 1898 8 12  eng,
: 1906 3 12  eng
- 1909
: 1910 4 12 Sie
: 1914 4 12  ist
: 1918 4 12  so
: 1922 9 14  eng
: 1931 7 14 e
: 1938 4 14  Schu
: 1942 4 12 he
: 1946 4 11  nicht
: 1950 4 12  ge
: 1954 15 11 wöhnt
- 1972
: 1978 4 11 Nein
: 1982 4 11  der
: 1986 4 11  Song
: 1990 4 11  ist
: 1994 4 11  nicht
: 1998 4 9  ver
: 2002 5 7 saut
- 2008
: 2010 4 9 Was
: 2014 4 11  bist
: 2018 4 12  du
: 2022 4 12  denn
: 2026 4 12  für
: 2030 4 11  ein
: 2034 9 9  Schwein
- 2043
: 2043 4 6 Wo
: 2047 3 7 ran
: 2050 4 9  denkst
: 2054 4 9  du
: 2058 4 9  denn
: 2062 4 9  bei
: 2066 4 9  bla
: 2070 3 9 sen
- 2073
: 2074 4 9 Ih
: 2078 4 9 re
: 2082 4 11  Schu
: 2086 4 11 he
: 2090 4 11  war'n
: 2094 4 9  zu
: 2098 6 7  klein
- 2104
: 2106 4 11 Ja
: 2110 4 11  der
: 2114 4 11  Song
: 2118 4 11  ist
: 2122 4 11  nicht
: 2126 4 9  ver
: 2130 5 7 saut
- 2135
: 2138 4 9 Was
: 2142 4 11  bist
: 2146 4 12  du
: 2150 4 12  denn
: 2154 4 12  für
: 2158 4 11  ein
: 2162 6 9  Schwein
- 2168
: 2170 4 6 Wo
: 2174 4 7 ran
: 2178 4 9  denkst
: 2182 4 9  du
: 2186 4 9  denn
: 2190 4 9  bei
: 2194 4 9  bla
: 2198 3 9 sen
- 2201
: 2202 4 9 Ih
: 2206 4 9 re
: 2210 4 11  Schu
: 2214 4 11 he
: 2218 4 11  war'n
: 2222 4 9  zu
: 2226 6 7  klein
- 2232
: 2234 4 11 Dei
: 2238 4 9 ne
: 2242 4 7  Freun
: 2246 4 2 din
: 2250 4 11  die
: 2254 4 9  kann
: 2258 4 7  bla
: 2262 2 2 sen
- 2264
: 2266 4 11 Die
: 2270 4 9  kann
: 2274 4 7  bla
: 2278 4 2 sen,
: 2282 4 11  bla
: 2286 4 9 sen,
: 2290 4 7  bla
: 2294 2 2 sen
- 2296
* 2298 4 11 Die
* 2302 4 12  kann
* 2306 4 14  bla
* 2310 4 14 sen,
* 2314 4 14  bla
* 2318 4 14 sen,
* 2322 4 14  bla
* 2326 2 14 sen
- 2328
: 2330 4 14 An
: 2334 4 12  den
: 2338 4 11  Fü
: 2342 4 7 ßen
: 2346 4 12  nicht
: 2350 4 11  er
: 2354 4 11 tra
: 2358 2 7 gen
- 2360
: 2361 5 7 Dei
: 2366 4 9 ne
: 2370 4 11  Freun
: 2374 4 11 din
: 2378 4 11  ist
: 2382 4 9  so
: 2386 2 7  eng
- 2388
: 2390 4 9 Sie
: 2394 4 11  ist
: 2398 4 12  so
: 2402 8 12  eng,
: 2410 8 12  eng,
: 2418 2 12  eng
- 2420
: 2423 3 0 Sie
: 2426 4 0  ist
: 2430 4 0  so
: 2434 8 2  eng
: 2442 8 2 e
: 2450 4 2  Schu
: 2454 4 0 he
: 2458 4 -1  nicht
: 2462 4 0  ge
: 2466 18 -1 wöhnt
E