#TITLE:Leuchte auf, Borussia
#ARTIST:BVB Borussia Dortmund
#MP3:BVB Borussia Dortmund - Leuchte auf, Borussia.mp3
#COVER:BVB Borussia Dortmund - Leuchte auf, Borussia [CO].jpg
#BACKGROUND:BVB Borussia Dortmund - Leuchte auf, Borussia [BG].jpg
#BPM:239,4
#GAP:1670
#ENCODING:UTF8
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
: 438 4 0  Im
: 446 12 5  Jah
: 464 6 9 re
: 473 14 9  1909
: 492 8 7 ~
: 503 12 5 ~
: 522 8 2 ~
: 532 14 0 ~,
- 548
: 550 5 0  da
: 560 9 5  wurd'
: 578 4 9  ein
* 588 16 9  Stern
: 608 6 12  ge
: 618 26 12 bor'n.
- 646
: 666 3 9  Und
: 670 3 12  man
: 675 14 12  sah
: 695 6 9  so
: 704 10 9 fort
: 722 8 7  an
: 732 12 5  sei
: 752 5 2 nem
: 761 10 0  Schein,
- 773
: 780 4 0  der
: 785 4 5  kann
: 790 12 5  nur
: 810 4 9  aus
* 819 16 9  Dort
: 838 4 7 mund
: 848 21 5  sein.
- 871
: 894 4 0  Die
: 899 4 5 ser
: 905 16 5  Stern
: 924 6 9  der
: 934 14 9  heißt
: 952 6 7  Bo
: 962 14 5 ru
: 981 7 2 ssi
: 991 10 0 a
- 1003
: 1010 3 0  und
: 1015 4 5  er
: 1020 12 5  leuch
: 1038 6 9 tet
: 1048 12 9  in
* 1066 6 12  schwarz -
* 1077 20 12  gelb.
- 1099
: 1126 4 9  Als
: 1134 14 12  schön
: 1154 5 9 ster
: 1163 16 9  Stern
: 1183 6 7  von
: 1193 12 5  al
: 1211 6 2 len
: 1221 11 0  dort,
- 1234
: 1241 5 0  am
: 1250 11 5  gro
: 1268 5 9 ßen
: 1278 14 9  Him
: 1297 7 7 mels
: 1308 24 5 zelt.
- 1334
: 1354 4 0  Und
: 1360 4 5  seh'
: 1366 12 5  ich
: 1386 6 9  hi
: 1395 14 9 nauf
: 1413 4 7  zum
: 1422 14 5  Fir
: 1441 8 2 ma
: 1451 6 0 ment,
- 1459
: 1464 4 0  auf
: 1470 5 5  den
: 1479 15 5  Stern
: 1499 5 9  den
: 1508 14 9  je
: 1527 5 12 der
: 1537 26 12  kennt.
- 1565
: 1586 3 9  Spür'
: 1590 4 12  ich
: 1596 14 12  sei
: 1614 6 9 nen
* 1623 16 9  Glanz
: 1644 6 7  dann
: 1653 12 5  sag
: 1671 6 2  ich
: 1681 11 0  mir:
- 1694
: 1700 3 0  er
: 1705 2 5  ist
: 1710 12 5  auch
: 1729 6 9  ein
: 1738 14 9  Teil
: 1757 6 7  von
: 1767 19 5  dir.
- 1788
: 1815 3 0  Leuch
: 1820 3 5 te
: 1825 12 5  auf
: 1843 6 9  mein
: 1853 14 9  Stern
* 1872 6 7  Bo
* 1882 14 5 ru
* 1901 6 2 ssi
* 1910 10 0 a,
- 1922
: 1930 4 0  leuch
: 1935 3 5 te
: 1940 14 5  auf
: 1958 6 9  zeig
: 1968 16 9  mir
: 1988 4 12  den
: 1998 32 12  Weg.
- 2032
: 2045 4 9  Ganz
: 2050 3 12  e
: 2055 14 12 gal
: 2074 4 9  wo
: 2084 14 9 hin
: 2101 5 7  er
: 2112 12 5  uns
: 2131 6 2  auch
: 2141 10 0  führt,
- 2153
: 2160 3 0  ich
: 2165 3 5  werd
: 2170 12 5  im
: 2188 5 9 mer
: 2198 14 9  bei
: 2217 6 7  dir
: 2227 25 5  sein.
- 2254
: 2275 4 0  Leuch
: 2280 3 5 te
: 2285 12 5  auf
: 2304 5 9  mein
: 2314 13 9  Stern
* 2332 6 7  Bo
* 2342 14 5 ru
* 2361 7 2 ssi
* 2370 10 0 a,
- 2382
: 2390 4 0  leuch
: 2395 3 5 te
: 2400 10 5  auf
: 2418 7 9  zeig
: 2428 16 9  mir
: 2447 4 12  den
: 2456 32 12  Weg.
- 2490
: 2504 4 9  Ganz
: 2509 4 12  e
: 2515 14 12 gal
: 2533 6 9  wo
: 2543 14 9 hin
: 2562 6 7  er
: 2572 14 5  uns
: 2591 6 2  auch
: 2600 11 0  führt,
- 2613
: 2620 4 0  ich
: 2625 3 5  werd
: 2630 12 5  im
: 2648 5 9 mer
: 2657 14 9  bei
: 2677 6 7  dir
: 2687 31 5  sein.
E