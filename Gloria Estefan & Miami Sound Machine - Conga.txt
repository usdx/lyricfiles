#TITLE:Conga
#ARTIST:Gloria Estefan & Miami Sound Machine
#MP3:Gloria Estefan & Miami Sound Machine - Conga.mp3
#VIDEO:Gloria Estefan & Miami Sound Machine - Conga.mp4
#COVER:Gloria Estefan & Miami Sound Machine - Conga[CO].jpg
#BPM:240,2
#GAP:1670
#ENCODING:UTF8
#PREVIEWSTART:25,837
#LANGUAGE:English
#GENRE:Pop
#YEAR:1985
: 0 2 4 Come
: 6 1 2  shake
: 8 1 -1  your
: 10 1 4  bo
: 12 1 4 dy,
: 14 1 2  ba
: 16 1 -1 by
: 18 1 4  do
: 20 2 6  the
: 24 2 7  con
: 28 2 4 ga
- 31
: 32 2 2 ...know
: 36 1 2  you
: 38 1 -1  can't
: 40 1 -3  con
: 42 1 2 trol
: 44 1 2  your
* 46 2 -1 self
: 50 1 2  a
: 52 2 4 ny
: 56 2 6  lon
: 60 2 2 ger
- 63
: 64 2 4 Come
: 68 1 4  on
: 70 1 2  shake
: 72 1 -1  your
: 74 1 4  bo
: 76 1 4 dy,
: 78 1 2  ba
: 80 1 -1 by
: 82 1 4  do
: 84 2 6  the
: 88 2 7  con
: 92 2 4 ga
- 95
: 95 2 2 ...know
: 99 1 2  you
: 101 1 -1  can't
: 103 1 -3  con
: 105 1 2 trol
: 107 1 2  your
: 109 2 -1 self
: 113 1 2  a
: 115 2 4 ny
: 119 2 6  lon
: 123 2 2 ger
- 127
: 375 2 4 Come
: 379 1 4  on
* 381 1 2  shake
: 383 1 -1  your
: 385 1 4  bo
: 387 1 4 dy,
: 389 1 2  ba
: 391 1 -1 by
: 393 1 4  do
: 395 2 6  the
: 399 2 7  con
: 403 2 4 ga
- 406
: 406 2 2 ...know
: 410 1 2  you
: 412 1 -1  can't
: 414 1 -3  con
: 416 1 2 trol
: 418 1 2  your
: 420 2 -1 self
: 424 1 2  a
: 426 2 4 ny
: 430 2 6  lon
: 434 2 2 ger
- 437
: 438 2 4 Feel
: 442 1 4  the
: 444 1 2  rhy
: 446 1 -1 thm
: 448 1 4  of
: 450 1 4  the
: 452 1 2  mu
: 454 1 -1 sic
: 456 1 4  get
: 458 2 6 ting
: 462 2 7  stron
: 466 2 4 ger
- 469
: 469 2 2 Don't
: 473 1 2  you
: 475 1 -1  fight
: 477 1 -3  it
: 479 1 2  'til
: 481 1 2  you
* 483 1 -1  tried
: 485 1 -3  it
: 487 1 2  do
: 489 2 4  the
: 493 2 6  con
: 497 2 2 ga,
: 501 2 4  beat
- 505
: 1008 2 4 E
: 1012 2 6 very
: 1016 6 7 bo
: 1024 6 6 dy
- 1032
: 1040 2 6 Ga
: 1044 2 4 ther
: 1048 6 2  'round
* 1056 6 4  now
- 1064
: 1072 2 2 Let
: 1076 2 4  your
: 1080 2 6  bo
: 1084 1 4 dy
: 1086 2 4  feel
: 1090 2 4  the
: 1094 4 4  heat
- 1100
: 1135 2 4 Don't
: 1139 2 6  you
: 1143 6 7  wor
: 1151 6 6 ry
- 1159
: 1166 2 6 If
: 1170 2 4  you
* 1174 6 2  can't
: 1182 6 4  dance
- 1190
: 1197 2 2 Let
: 1201 2 4  the
: 1205 2 6  mu
: 1209 1 4 sic
: 1211 2 4  move
: 1215 2 4  your
: 1219 4 4  feet
- 1225
: 1259 2 4 It's
: 1263 2 6  the
: 1267 6 7  rhy
: 1275 6 6 thm
- 1283
: 1291 2 6 Of
: 1295 2 4  the
: 1299 6 2  is
: 1307 6 4 land
- 1315
: 1321 2 2 And
: 1325 2 4  that
: 1329 2 6  su
: 1333 1 4 gar
: 1335 2 4  cane
: 1339 2 4  so
: 1343 4 4  sweet
- 1349
* 1385 2 4 If
: 1389 2 6  you
: 1393 6 7  want
: 1401 6 6  to
- 1409
: 1415 2 6 Do
: 1419 2 4  the
: 1423 6 2  con
: 1431 4 4 ga
- 1437
: 1441 3 2 You've
: 1447 2 2  got
: 1451 2 4  to
: 1455 2 6  lis
: 1459 1 4 ten
: 1461 2 4  to
: 1465 2 4  the
: 1469 4 4  beat
- 1475
: 1590 1 4 Come
: 1592 2 4  on
: 1598 1 2  shake
: 1600 1 -1  your
: 1602 1 4  bo
: 1604 1 4 dy,
: 1606 1 2  ba
: 1608 1 -1 by
: 1610 1 4  do
: 1612 2 6  the
: 1616 2 7  con
: 1620 2 4 ga
- 1623
: 1623 2 2 ...know
: 1627 1 2  you
: 1629 1 0  can't
: 1631 1 -3  con
: 1633 1 2 trol
: 1635 1 2  your
: 1637 2 0 self
: 1641 1 2  a
: 1643 2 4 ny
* 1647 2 6  lon
: 1651 2 2 ger
- 1654
: 1654 2 4 Feel
: 1658 1 4  the
: 1660 1 2  rhy
: 1662 1 -1 thm
: 1664 1 4  of
: 1666 1 4  the
: 1668 1 2  mu
: 1670 1 -1 sic
: 1672 1 4  get
: 1674 2 6 ting
: 1678 2 7  stron
: 1682 2 4 ger
- 1685
: 1686 2 2 Don't
: 1690 1 2  you
* 1692 1 -1  fight
: 1694 1 -3  it
: 1696 1 2  'til
* 1698 1 2  you
: 1700 1 -1  tried
: 1702 1 -3  it
* 1704 1 2  do
: 1706 2 4  the
: 1710 2 6  con
: 1714 2 2 ga,
: 1718 2 4  beat
- 1722
: 1852 2 4 Feel
: 1856 2 6  the
* 1860 6 7  fi
: 1868 6 6 re
- 1876
: 1884 2 6 Of
: 1888 2 4  de
: 1892 6 2 si
: 1900 8 4 re
- 1910
: 1915 2 2 As
: 1919 2 4  you
: 1923 2 6  dance
: 1927 1 4  the
: 1929 2 6  night
: 1933 2 4  a
: 1937 4 4 way
- 1943
: 1978 2 4 'Cause
: 1982 2 6  to
: 1986 6 7  night
: 1994 6 6  we're
- 2002
: 2008 2 6 Gon
* 2012 2 4 na
: 2016 6 2  par
: 2024 6 4 ty
- 2032
: 2039 2 2 'Til
: 2043 2 4  we
: 2047 2 6  see
: 2051 1 6  the
: 2053 2 4  break
: 2057 2 4  of
: 2061 4 4  day
- 2067
: 2102 2 4 Bet
: 2106 2 6 ter
: 2110 5 7  get
: 2118 10 6  your
- 2130
: 2132 2 6 ...self
: 2136 2 4  to
* 2140 6 2 ge
: 2148 6 4 ther
- 2156
: 2163 2 2 And
: 2167 2 4  hold
: 2171 2 6  on
: 2175 1 4  to
: 2177 2 4  what
: 2181 2 4  you've
: 2185 4 4  got
- 2191
: 2226 2 4 Once
: 2230 2 6  the
: 2234 6 7  mu
: 2242 5 6 sic
- 2249
* 2257 2 6 Hits
: 2261 2 4  your
: 2265 6 2  sys
: 2273 4 4 tem
- 2279
: 2289 2 2 There's
: 2293 2 4  no
: 2297 2 6  way
: 2301 1 4  you're
: 2303 2 4  gon
: 2307 2 4 na
: 2311 2 4  stop
- 2315
* 2840 2 4 Come
: 2844 1 4  on
: 2846 1 2  shake
: 2848 1 -1  your
: 2850 1 4  bo
: 2852 1 4 dy,
: 2854 1 2  ba
: 2856 1 -1 by
: 2858 1 4  do
: 2860 2 6  the
: 2864 2 7  con
: 2868 2 4 ga
- 2871
: 2871 2 2 ...know
: 2875 1 2  you
: 2877 1 0  can't
: 2879 1 -3  con
: 2881 1 2 trol
: 2883 1 2  your
: 2885 2 -1 self
: 2889 1 2  a
: 2891 2 4 ny
: 2895 2 6  lon
: 2899 2 2 ger
- 2902
: 2903 2 4 Feel
: 2907 1 4  the
* 2909 1 2  rhy
* 2911 1 -1 thm
: 2913 1 4  of
: 2915 1 4  the
: 2917 1 2  mu
: 2919 1 -1 sic
: 2921 1 4  get
: 2923 2 6 ting
: 2927 2 7  stron
: 2931 2 4 ger
- 2934
: 2934 2 2 Don't
: 2938 1 2  you
: 2940 1 -1  fight
: 2942 1 -3  it
: 2944 1 2  'til
: 2946 1 2  you
: 2948 1 -1  tried
: 2950 1 -3  it
: 2952 1 2  do
: 2954 2 4  the
: 2958 2 6  con
: 2962 2 2 ga,
* 2966 2 4  beat
- 2970
: 3462 1 4 Come
: 3464 2 4  on
: 3470 1 2  shake
: 3472 1 -1  your
: 3474 1 4  bo
: 3476 1 4 dy,
: 3478 1 2  ba
: 3480 1 -1 by
: 3482 1 4  do
: 3484 2 6  the
: 3488 2 7  con
: 3492 2 4 ga
- 3495
: 3496 2 2 ...know
: 3500 1 2  you
: 3502 1 -1  can't
: 3504 1 -3  con
: 3506 1 2 trol
: 3508 1 2  your
: 3510 2 -1 self
: 3514 1 2  a
: 3516 2 4 ny
: 3520 2 6  lon
: 3524 2 2 ger
- 3527
: 3527 2 4 Feel
: 3531 1 4  the
: 3533 1 2  rhy
: 3535 1 -1 thm
: 3537 1 4  of
: 3539 1 4  the
* 3541 1 2  mu
: 3543 1 -1 sic
: 3545 1 4  get
: 3547 2 6 ting
: 3551 2 7  stron
: 3555 2 4 ger
- 3558
: 3558 2 2 Don't
: 3562 1 2  you
: 3564 1 -1  fight
: 3566 1 -3  it
: 3568 1 2  'til
: 3570 1 2  you
: 3572 1 -1  tried
: 3574 1 -3  it
: 3576 1 2  do
: 3578 2 4  the
: 3582 2 6  con
: 3586 2 2 ga
- 3589
: 3589 2 4 Come
: 3593 1 4  on
: 3595 1 2  shake
: 3597 1 -1  your
: 3599 1 4  bo
: 3601 1 4 dy,
: 3603 1 2  ba
: 3605 1 -1 by
: 3607 1 4  do
: 3609 2 6  the
* 3613 2 7  con
* 3617 2 4 ga
- 3620
: 3621 2 2 ...know
: 3625 1 2  you
: 3627 1 -1  can't
: 3629 1 -3  con
: 3631 1 2 trol
: 3633 1 2  your
: 3635 2 -1 self
: 3639 1 2  a
: 3641 2 4 ny
: 3645 2 6  lon
: 3649 2 2 ger
- 3652
: 3652 2 4 Feel
: 3656 1 4  the
: 3658 1 2  rhy
: 3660 1 -1 thm
: 3662 1 4  of
: 3664 1 4  the
: 3666 1 2  mu
: 3668 1 -1 sic
* 3670 1 4  get
: 3672 2 6 ting
: 3676 2 7  stron
: 3680 2 4 ger
- 3683
: 3683 2 2 Don't
: 3687 1 2  you
: 3689 1 -1  fight
: 3691 1 -3  it
: 3693 1 2  'til
: 3695 1 2  you
: 3697 1 -1  tried
: 3699 1 -3  it
: 3701 1 2  do
: 3703 2 4  the
: 3707 2 6  con
: 3711 2 2 ga,
: 3715 2 4  beat
- 3719
: 3964 2 4 Come
: 3968 1 4  on
* 3970 1 2  shake
: 3972 1 -1  your
: 3974 1 4  bo
: 3976 1 4 dy,
: 3978 1 2  ba
: 3980 1 -1 by
: 3982 1 4  do
: 3984 2 6  the
: 3988 2 7  con
: 3992 2 4 ga
E