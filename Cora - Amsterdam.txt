#TITLE:Amsterdam
#ARTIST:Cora
#MP3:Cora - Amsterdam.mp3
#VIDEO:Cora - Amsterdam.mp4
#COVER:Cora - Amsterdam.jpg
#BPM:119,9
#GAP:8150
#VIDEOGAP:-0,2
#ENCODING:UTF8
#EDITION:Schlager
: 0 3 0 Heut 
: 6 1 0 sag 
: 8 2 0 ich 
: 12 2 0 es 
: 16 2 -1 wa
: 18 1 -3 aar 
: 20 3 -1 ein
: 24 3 0 mal 
- 30
: 32 3 4 Mär
: 38 2 0 chen 
: 40 4 0 voll
: 44 1 0 er 
: 48 2 -1 Angst 
: 50 1 -3 u
: 52 3 -1 uund 
: 56 3 0 Qual 
- 60
: 64 3 4 El
: 70 2 0 fen 
: 72 3 0 Prinz 
: 76 1 0 u
: 80 2 -1 uund 
: 82 1 -3 gu
: 84 3 -1 uute 
: 88 3 0 Feen 
- 94
: 96 3 4 Wa
: 102 2 0 ren 
: 104 2 0 für 
: 108 1 0 uns 
: 112 2 -1 nicht 
: 114 1 -3 vor
: 116 3 -1 ge
: 120 3 0 seh'n 
- 126
: 128 4 2 Halt 
: 134 1 2 mich 
: 136 2 2 hast 
: 140 2 2 Du 
: 144 2 2 o
: 146 1 4 ooft 
: 148 3 5 ge
: 152 3 4 sagt 
- 158
: 160 2 7 Wie 
: 166 1 4 hab 
: 168 2 4 ich 
: 172 2 4 Dich 
: 174 1 2 da
: 176 3 2 aaann 
: 180 2 2 ge
: 182 1 4 ee
: 184 3 4 fragt 
- 190
: 192 2 0 Lie
: 194 2 0 be 
: 200 1 0 hat 
: 204 1 0 to
: 206 3 2 tal 
: 212 2 2 ver
: 214 4 4 sagt 
- 220
: 228 2 0 In 
: 232 2 0 Am
: 236 2 4 ster
: 238 14 2 dam 
- 254
: 256 3 0 Komm 
: 262 1 0 wir 
: 264 2 0 fah
: 266 2 0 ren 
: 268 2 0 nach 
: 272 2 -1 Am
: 274 1 -3 ste
: 276 3 -1 eer
: 280 3 0 dam 
- 284
: 284 2 0 Ich 
: 288 2 4 weiß 
: 290 1 0 daß 
: 292 1 0 uns 
: 294 2 0 nichts 
: 296 2 0 pass
: 300 2 0 i
: 304 2 -1 e
: 306 1 -3 re
: 308 3 -1 een 
: 312 3 0 kann 
- 318
: 320 2 4 Du 
: 322 1 0 und 
: 326 2 0 i
: 328 3 0 iich 
: 332 1 0 wir 
: 336 2 -1 ham's 
: 338 1 -3 doch 
: 340 3 -1 im 
: 344 3 0 Griff 
- 348
: 348 2 0 Da
: 350 1 0 bei 
: 352 2 4 sa
: 354 1 0 ßen 
: 356 2 0 wir 
: 360 2 0 längst 
- 363
: 364 1 0 Auf 
: 366 2 0 dem 
: 368 2 -1 sin
: 370 1 -3 ken
: 372 3 -1 den 
: 376 3 0 Schiff 
- 382
: 384 4 2 Bleib 
: 390 1 2 doch 
: 392 2 2 hab 
: 396 2 2 ich 
: 400 2 2 noch 
: 402 1 4 ge
: 404 3 5 ee
: 408 3 4 sagt 
- 414
: 416 2 7 Wie 
: 422 1 4 hast 
: 424 2 4 Du 
: 428 2 4 mich 
: 430 1 2 da
: 432 3 2 aann 
: 436 2 2 ge
: 438 1 4 ee
: 440 3 4 fragt 
- 446
: 448 2 0 Lie
: 450 2 0 be 
: 456 1 0 hat 
: 460 1 0 to
: 462 3 2 tal 
: 468 2 2 ver
: 470 4 4 sagt 
- 478
: 484 2 0 In 
: 488 2 0 Am
: 492 2 4 ster
: 494 14 2 dam 
- 510
: 512 4 0 Traum 
: 520 4 0 von 
: 528 2 -1 Am
: 532 1 0 ster
: 534 6 2 dam 
- 542
: 544 4 4 Der 
: 552 4 5 die 
: 560 2 4 Hoff
: 564 1 2 nung 
: 566 6 0 nahm 
- 572
: 574 2 2 Al
: 576 3 4 lein 
: 580 3 7 in 
: 584 4 5 ei
: 588 3 4 ner 
: 592 2 4 frem
: 594 3 2 den 
: 598 4 2 Stadt 
- 604
: 606 2 0 Al
: 608 3 0 lein 
: 612 3 -1 in 
: 616 3 -1 Am
: 620 2 0 ster
: 622 14 2 dam 
- 638
: 640 4 0 Re
: 648 4 0 gen
: 656 4 -1 bo
: 660 1 0 gen
: 662 6 2 gold 
- 670
: 672 7 4 Ha
: 680 6 5 ben 
: 688 3 4 wir 
: 692 1 2 ge
: 694 5 0 wollt 
- 699
: 700 2 0 Ro
: 702 1 2 te 
: 704 3 4 Ro
: 708 3 7 sen 
: 712 4 5 soll'n 
: 716 3 4 vom 
: 720 2 4 Him
: 722 3 2 mel 
: 726 4 2 fal
: 730 6 0 len 
- 738
: 740 3 -1 Und 
: 744 3 -1 nie 
: 748 2 0 ver
: 750 14 2 blüh'n 
- 766
: 768 3 0 Komm 
: 774 1 0 wir 
: 776 2 0 fah
: 778 2 0 ren 
: 780 2 0 nach 
: 784 2 -1 Am
: 786 1 -3 ste
: 788 3 -1 eer
: 792 3 0 dam 
- 796
: 796 1 0 Es 
: 798 1 0 war 
: 800 2 4 klar 
- 802
: 802 1 0 Daß 
: 806 2 0 ich 
: 808 2 0 Dich 
: 812 1 0 nicht 
: 814 2 0 ha
: 816 2 -1 aal
: 818 1 -3 te
: 820 3 -1 een 
: 824 3 0 kann 
- 830
: 832 2 4 Le
: 834 1 0 ben 
: 838 2 0 hat 
: 840 3 0 sich 
: 844 1 0 den 
: 848 2 -1 Star
: 850 1 -3 ken 
: 852 3 -1 ge
: 856 3 0 wählt 
- 860
: 860 4 0 Ver
: 864 2 4 lo
: 866 1 0 ren 
: 870 1 0 wenn 
: 872 2 0 man 
- 875
: 876 1 0 Zu 
: 878 2 0 den 
: 880 2 -1 Schwä
: 882 1 -3 che
: 884 3 -1 ren 
: 888 3 0 zählt 
- 900
: 1024 4 0 Traum 
: 1032 4 0 von 
: 1040 2 -1 Am
: 1044 1 0 ster
: 1046 6 2 dam 
- 1054
: 1056 4 4 Der 
: 1064 4 5 die 
: 1072 2 4 Hoff
: 1076 1 2 nung 
: 1078 6 0 nahm 
- 1085
: 1086 2 2 Al
: 1088 3 4 lein 
: 1092 3 7 in 
: 1096 4 5 ei
: 1100 3 4 ner 
: 1104 2 4 frem
: 1106 3 2 den 
: 1110 4 2 Stadt 
- 1116
: 1118 2 0 Al
: 1120 3 0 lein 
: 1124 3 -1 in 
: 1128 3 -1 Am
: 1132 2 0 ster
: 1134 14 2 dam 
- 1150
: 1152 4 0 Re
: 1160 4 0 gen
: 1168 4 -1 bo
: 1172 1 0 gen
: 1174 6 2 gold 
- 1182
: 1184 7 4 Ha
: 1192 6 5 ben 
: 1200 3 4 wir 
: 1204 1 2 ge
: 1206 5 0 wollt 
- 1211
: 1212 2 0 Ro
: 1214 1 2 te 
: 1216 3 4 Ro
: 1220 3 7 sen 
: 1224 4 5 soll'n 
: 1228 3 4 vom 
: 1232 2 4 Him
: 1234 3 2 mel 
: 1238 4 2 fal
: 1242 6 0 len 
- 1250
: 1252 3 -1 Und 
: 1256 3 -1 nie 
: 1260 2 0 ver
: 1262 14 2 blüh'n 
- 1280
: 1408 3 3 Komm 
: 1414 1 3 wir 
: 1416 2 3 fah
: 1418 2 3 ren 
: 1420 2 3 nach 
: 1424 2 2 Am
: 1426 1 0 ste
: 1428 3 2 eer
: 1432 3 3 dam 
- 1436
: 1436 1 3 Es 
: 1438 1 3 war 
: 1440 2 7 klar 
- 1442
: 1442 1 3 Daß 
: 1446 2 3 ich 
: 1448 2 3 Dich 
: 1452 1 3 nicht 
: 1454 2 3 ha
: 1456 2 2 aal
: 1458 1 0 te
: 1460 3 2 een 
: 1464 3 3 kann 
- 1470
: 1472 2 7 Le
: 1474 1 3 ben 
: 1478 2 3 hat 
: 1480 3 3 sich 
: 1484 1 3 den 
: 1488 2 2 Star
: 1490 1 0 ken 
: 1492 3 2 ge
: 1496 3 3 wählt 
- 1499
: 1500 4 3 Ver
: 1504 2 7 lo
: 1506 1 3 ren 
: 1510 1 3 wenn 
: 1512 2 3 man 
- 1515
: 1516 1 3 Zu 
: 1518 2 3 den 
: 1520 2 2 Schwä
: 1522 1 0 che
: 1524 3 2 ren 
: 1528 3 3 zählt 
- 1534
: 1536 4 5 Halt 
: 1542 1 5 mich 
: 1544 2 5 hast 
: 1548 2 5 Du 
: 1552 2 5 o
: 1554 1 7 ooft 
: 1556 3 8 ge
: 1560 3 7 sagt 
- 1566
: 1568 2 10 Wie 
: 1574 1 7 hab 
: 1576 2 7 ich 
: 1580 2 7 Dich 
: 1582 1 5 da
: 1584 3 5 aann 
: 1588 2 5 ge
: 1590 1 7 ee
: 1592 3 7 fragt 
- 1598
: 1600 2 3 Lie
: 1602 2 3 be 
: 1608 1 3 hat 
: 1612 1 3 to
: 1614 3 5 tal 
: 1620 2 5 ver
: 1622 10 7 sagt 
- 1634
: 1636 2 3 In 
: 1640 2 3 Am
: 1644 2 7 ster
: 1646 14 5 dam 
- 1662
: 1664 4 3 Traum 
: 1672 4 3 von 
: 1680 2 2 Am
: 1684 1 3 ster
: 1686 6 5 dam 
- 1694
: 1696 4 7 Der 
: 1704 4 8 die 
: 1712 2 7 Hoff
: 1716 1 5 nung 
: 1718 6 3 nahm 
- 1724
: 1726 2 5 Al
: 1728 3 7 lein 
: 1732 3 10 in 
: 1736 4 8 ei
: 1740 3 7 ner 
: 1744 2 7 frem
: 1746 3 5 den 
: 1750 4 5 Stadt 
- 1756
: 1758 2 3 Al
: 1760 3 3 lein 
: 1764 3 2 in 
: 1768 3 2 Am
: 1772 2 3 ster
: 1774 14 5 dam 
- 1790
: 1792 4 3 Re
: 1800 4 3 gen
: 1808 4 2 bo
: 1812 1 3 gen
: 1814 6 5 gold 
- 1822
: 1824 7 7 Ha
: 1832 6 8 ben 
: 1840 3 7 wir 
: 1844 1 5 ge
: 1846 5 3 wollt 
- 1851
: 1852 2 3 Ro
: 1854 1 5 te 
: 1856 3 7 Ro
: 1860 3 10 sen 
: 1864 4 8 soll'n 
: 1868 3 7 vom 
: 1872 2 7 Him
: 1874 3 5 mel 
: 1878 4 5 fal
: 1882 6 3 len 
- 1890
: 1892 3 2 Und 
: 1896 3 2 nie 
: 1900 2 3 ver
: 1902 14 5 blüh'n 
E