#TITLE:Ohne Dich
#ARTIST:Münchener Freiheit
#MP3:Münchener Freiheit - Ohne Dich.mp3
#COVER:Münchener Freiheit - Ohne Dich [CO].jpg
#BACKGROUND:Münchener Freiheit - Ohne Dich [BG].jpg
#EDITION:Singstar - '80s
#LANGUAGE:German
#BPM:180,22
#GAP:21473,75
: 0 1 51 Ich
: 1 3 58  will
: 5 2 58  mich
: 7 3 58  nicht
: 11 2 58  ver
: 13 3 58 än
: 17 2 51 dern
- 21 22
: 31 2 51 Um
: 33 3 58  dir
: 37 2 58  zu
: 39 2 58  im
: 43 2 58 po
: 45 3 58 nie
: 48 3 55 r'n
- 53 54
: 64 1 51 Will
: 65 3 58  nicht
: 69 2 58  den
: 71 3 58  gan
: 75 2 58 zen
* 77 4 58  A
: 82 2 51 bend
- 86 87
: 95 2 51 Prob
: 97 3 58 le
: 101 2 58 me
: 103 2 58  dis
: 107 2 58 ku
: 109 3 58 tie
: 112 3 55 r'n
- 117
: 125 2 58 A
: 127 2 58 ber
: 129 2 58  ei
: 131 2 58 nes
: 133 2 58  geb'
: 135 2 58  ich
: 137 4 60  zu
: 141 4 56 ~
- 147 148
: 173 2 63 Das,
: 177 2 63  was
: 179 3 63  ich
: 184 3 63  will,
: 189 2 63  bist
* 194 16 63  du
- 212 235
: 255 2 58 Ich
: 257 3 58  will
: 261 2 58  nichts
: 263 2 58  ga
: 265 3 58 ran
: 269 3 58 tie
: 273 2 55 ren
- 277 278
: 287 2 51 Das
: 289 2 58  ich
: 291 3 58  nicht
: 295 3 58  hal
: 299 2 58 ten
: 301 3 58  kann
- 306 307
: 319 2 51 Will
: 321 2 58  mit
: 323 3 58  dir
: 327 3 58  was
: 331 2 58  er
: 333 3 58 le
: 337 2 55 ben
- 341
: 349 2 51 Bes
: 351 2 51 ser
* 353 3 60  gleich
: 357 2 58  als
: 361 2 58  ir
: 363 2 55 gend
: 365 3 58 wann
- 370 371
: 381 2 58 Und
: 383 2 58  ich
: 385 2 58  ge
: 387 2 58 be
: 389 2 58  of
: 391 2 58 fen
* 393 4 60  zu
: 397 6 56 ~
- 405 406
: 429 2 63 Das,
: 433 2 63  was
: 435 2 63  ich
: 440 3 63  will,
: 445 3 63  bist
: 449 11 63  du
- 462 497
: 517 2 63 Oh
: 519 2 63 ne
: 521 2 63  dich
- 524
: 525 2 63 Schlaf'
: 527 2 63  ich
: 531 3 60  heut'
: 535 3 62  Nacht
: 539 2 63  nicht
: 541 4 65  ein
- 547
: 549 2 62 Oh
: 551 2 62 ne
: 553 2 62  dich
- 556
: 557 2 62 Fahr'
: 559 3 62  ich
: 563 3 58  heut'
: 567 3 60  Nacht
: 571 3 62  nicht
: 575 3 63  heim
- 579
: 581 2 63 Oh
: 583 2 63 ne
: 585 2 63  dich
- 588
: 589 2 63 Komm'
: 591 3 63  ich
: 595 3 60  heut'
: 599 3 60  nicht
: 603 2 62  zur
: 605 3 63  Ruh'
- 610
: 613 2 63 Das,
: 617 2 63  was
* 621 2 67  ich
: 625 5 65  will,
: 633 2 63  bist
: 637 5 63  du
- 643
: 645 2 63 Oh
: 647 2 63 ne
: 649 2 63  dich
- 652
: 653 2 63 Schlaf'
: 655 2 63  ich
: 659 3 60  heut'
: 663 3 62  Nacht
: 667 2 63  nicht
: 669 4 65  ein
- 675
: 677 2 62 Oh
: 679 2 62 ne
: 681 2 62  dich
- 684
: 685 2 62 Fahr'
: 687 3 62  ich
: 691 3 58  heut'
: 695 3 60  Nacht
: 699 3 62  nicht
: 703 3 63  heim
- 707
: 709 2 63 Oh
: 711 2 63 ne
: 713 2 63  dich
- 716
: 717 2 63 Komm'
: 719 3 63  ich
: 723 3 60  heut'
: 727 3 60  nicht
: 731 2 62  zur
: 733 3 63  Ruh'
- 738
: 741 2 63 Das,
: 745 2 63  was
: 749 2 67  ich
: 753 5 65  will,
: 761 2 63  bist
: 765 12 63  du
- 779 875
: 895 2 51 Ich
: 897 3 58  will
: 901 2 58  nicht
: 903 3 58  al
: 907 2 58 les
: 909 3 58  sa
: 913 2 55 gen
- 917 918
: 927 2 51 Und
: 929 3 58  nicht
: 933 2 58  so
: 935 2 58  viel
: 939 2 58  er
: 941 3 58 klä
: 944 3 55 r'n
- 949 950
: 959 2 51 Und
: 961 3 60  nicht
: 965 2 58  mit
: 967 3 58  so
: 971 2 58  viel
: 973 4 58  Wor
: 978 2 51 ten
- 982 983
: 991 2 51 Den
: 993 2 60  A
: 995 2 58 u
: 997 2 58 gen
: 999 2 58 blick
: 1003 2 58  zer
: 1005 4 58 stö
: 1009 2 55 r'n
- 1013
: 1021 2 58 A
: 1023 2 58 ber
: 1025 2 58  ei
: 1027 2 58 nes
: 1029 2 58  geb'
: 1031 2 58  ich
: 1033 4 60  zu
: 1037 4 56 ~
- 1043 1044
* 1069 2 63 Das,
: 1073 2 63  was
: 1075 3 63  ich
: 1080 3 63  will,
: 1085 2 63  bist
: 1090 19 63  du
: 1109 2 70 ~
- 1113 1131
: 1151 1 58 Und
: 1152 1 58  ich
: 1153 2 67  will
: 1155 3 65  auch
: 1159 3 65  nichts
: 1163 2 63  er
: 1165 4 63 zähl'n
- 1171 1172
: 1181 2 63 Was
: 1183 2 63  dich
: 1185 4 67  eh'
: 1189 2 65  nicht
: 1191 3 65  in
: 1195 1 63 te
: 1196 1 63 res
: 1197 3 63 siert
- 1202 1203
: 1215 2 63 Will
: 1217 2 67  mit
: 1219 3 65  dir
: 1223 3 65  was
: 1227 2 63  er
: 1229 3 63 le
: 1234 2 58 ben
- 1238
: 1245 2 63 Das
: 1247 2 63  uns
* 1250 3 67  bei
: 1254 3 65 de
: 1257 3 65  fas
: 1261 2 63 zi
: 1263 3 65 niert
: 1266 2 60 ~
- 1270
: 1277 2 63 Und
: 1279 2 63  ich
: 1281 2 63  ge
: 1283 2 63 be
: 1285 2 63  of
: 1287 2 63 fen
: 1289 19 65  zu
: 1313 6 60 ~
- 1321
: 1327 2 68 Das,
: 1329 2 68  was
: 1331 3 68  ich
: 1335 2 68  will
: 1337 3 67 ~
: 1341 3 63  bist
: 1345 22 63  du
- 1369 1393
: 1413 2 63 Oh
: 1415 2 63 ne
: 1417 2 63  dich
- 1420
: 1421 2 63 Schlaf'
: 1423 2 63  ich
: 1427 3 60  heut'
: 1431 3 62  Nacht
: 1435 2 63  nicht
: 1437 4 65  ein
- 1443
: 1445 2 62 Oh
: 1447 2 62 ne
: 1449 2 62  dich
- 1452
: 1453 2 62 Fahr'
: 1455 3 62  ich
: 1459 3 58  heut'
: 1463 3 60  Nacht
: 1467 3 62  nicht
: 1471 3 63  heim
- 1475
: 1477 2 63 Oh
: 1479 2 63 ne
: 1481 2 63  dich
- 1484
: 1485 2 63 Komm'
: 1487 3 63  ich
: 1491 3 60  heut'
: 1495 3 60  nicht
: 1499 2 62  zur
: 1501 3 63  Ruh'
- 1506
: 1509 2 63 Das,
: 1513 2 63  was
* 1517 2 67  ich
: 1521 5 65  will,
: 1529 2 63  bist
: 1533 5 63  du
- 1539
: 1541 2 63 Oh
: 1543 2 63 ne
: 1545 2 63  dich
- 1548
: 1549 2 63 Schlaf'
: 1551 2 63  ich
: 1555 3 60  heut'
: 1559 3 62  Nacht
: 1563 2 63  nicht
: 1565 4 65  ein
- 1571
: 1573 2 62 Oh
: 1575 2 62 ne
: 1577 2 62  dich
- 1580
: 1581 2 62 Fahr'
: 1583 3 62  ich
: 1587 3 58  heut'
: 1591 3 60  Nacht
: 1595 3 62  nicht
: 1599 3 63  heim
- 1603
: 1605 2 63 Oh
: 1607 2 63 ne
: 1609 2 63  dich
- 1612
: 1613 2 63 Komm'
: 1615 3 63  ich
: 1619 3 60  heut'
: 1623 3 60  nicht
: 1627 2 62  zur
: 1629 3 63  Ruh'
- 1634
: 1637 2 63 Das,
: 1641 2 63  was
: 1645 2 67  ich
: 1649 5 65  will,
: 1657 2 63  bist
: 1661 12 63  du
- 1675 1905
: 1925 2 63 Oh
: 1927 2 63 ne
: 1929 2 63  dich
- 1932
: 1933 2 63 Schlaf'
: 1935 2 63  ich
: 1939 3 60  heut'
: 1943 3 62  Nacht
: 1947 2 63  nicht
* 1949 4 65  ein
- 1955
: 1957 2 62 Oh
: 1959 2 62 ne
: 1961 2 62  dich
- 1964
: 1965 2 62 Fahr'
: 1967 3 62  ich
: 1971 3 58  heut'
: 1975 3 60  Nacht
: 1979 3 62  nicht
: 1983 3 63  heim
- 1987
: 1989 2 63 Oh
: 1991 2 63 ne
: 1993 2 63  dich
- 1996
: 1997 2 63 Komm'
: 1999 3 63  ich
: 2003 3 60  heut'
: 2007 3 60  nicht
: 2011 2 62  zur
: 2013 3 63  Ruh'
- 2018
: 2021 2 63 Das,
: 2025 2 63  was
: 2029 2 67  ich
: 2033 5 65  will,
: 2041 2 63  bist
: 2045 5 63  du
- 2051
: 2053 2 63 Oh
: 2055 2 63 ne
: 2057 2 63  dich
- 2060
: 2061 2 63 Schlaf'
: 2063 2 63  ich
: 2067 3 60  heut'
: 2071 3 62  Nacht
: 2075 2 63  nicht
: 2077 4 65  ein
- 2083
: 2085 2 62 Oh
: 2087 2 62 ne
: 2089 2 62  dich
- 2092
: 2093 2 62 Fahr'
: 2095 3 62  ich
: 2099 3 58  heut'
: 2103 3 60  Nacht
: 2107 3 62  nicht
* 2111 3 63  heim
- 2115
: 2117 2 63 Oh
: 2119 2 63 ne
: 2121 2 63  dich
- 2124
: 2125 2 63 Komm'
: 2127 3 63  ich
: 2131 3 60  heut'
: 2135 3 60  nicht
: 2139 2 62  zur
: 2141 3 63  Ruh'
- 2146
: 2149 2 63 Das,
: 2153 2 63  was
: 2157 2 67  ich
: 2161 5 65  will,
: 2169 2 63  bist
: 2173 3 63  du
- 2178
: 2181 2 63 Oh
: 2183 2 63 ne
: 2185 2 63  dich
- 2188
: 2189 2 63 Schlaf'
: 2191 2 63  ich
: 2195 3 60  heut'
: 2199 3 62  Nacht
: 2203 2 63  nicht
: 2205 4 65  ein
- 2211
: 2213 2 62 Oh
: 2215 2 62 ne
: 2217 2 62  dich
- 2220
: 2221 2 62 Fahr'
: 2223 3 62  ich
: 2227 3 58  heut'
: 2231 3 60  Nacht
: 2235 3 62  nicht
: 2239 3 63  heim
- 2243
: 2245 2 63 Oh
: 2247 2 63 ne
: 2249 2 63  dich
- 2252
: 2253 2 63 Komm'
: 2255 3 63  ich
: 2259 3 60  heut'
: 2263 3 60  nicht
: 2267 2 62  zur
: 2269 3 63  Ruh'
- 2274
: 2277 2 63 Das,
: 2281 2 63  was
: 2285 2 67  ich
: 2289 5 65  will,
: 2297 2 63  bist
: 2301 5 63  du
- 2307
: 2309 2 63 Oh
: 2311 2 63 ne
: 2313 2 63  dich
- 2316
: 2317 2 63 Schlaf'
: 2319 2 63  ich
: 2323 3 60  heut'
: 2327 3 62  Nacht
: 2331 2 63  nicht
: 2333 4 65  ein
- 2339
: 2341 2 62 Oh
: 2343 2 62 ne
: 2345 2 62  dich
- 2348
: 2349 2 62 Fahr'
: 2351 3 62  ich
: 2355 3 58  heut'
: 2359 3 60  Nacht
: 2363 3 62  nicht
: 2367 3 63  heim
- 2371
: 2373 2 63 Oh
: 2375 2 63 ne
: 2377 2 63  dich
- 2380
: 2381 2 63 Komm'
: 2383 3 63  ich
: 2387 3 60  heut'
: 2391 3 60  nicht
: 2395 2 62  zur
: 2397 3 63  Ruh'
- 2402
: 2405 2 63 Das,
: 2409 2 63  was
: 2413 2 67  ich
: 2417 5 65  will,
: 2425 2 63  bist
: 2429 6 63  du
- 2436
: 2437 2 63 Oh
: 2439 2 63 ne
: 2441 2 63  dich
- 2444
: 2445 2 63 Schlaf'
: 2447 2 63  ich
: 2451 3 60  heut'
: 2455 3 62  Nacht
: 2459 2 63  nicht
: 2461 4 65  ein
- 2467
: 2469 2 62 Oh
: 2471 2 62 ne
: 2473 2 62  dich
- 2476
: 2477 2 62 Fahr'
: 2479 3 62  ich
: 2483 3 58  heut'
: 2487 3 60  Nacht
: 2491 3 62  nicht
: 2495 3 63  heim
- 2499
: 2501 2 63 Oh
: 2503 2 63 ne
: 2505 2 63  dich
- 2508
: 2509 2 63 Komm'
: 2511 3 63  ich
: 2515 3 60  heut'
: 2519 3 60  nicht
: 2523 2 62  zur
: 2525 3 63  Ruh'
E
