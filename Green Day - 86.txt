#TITLE:86
#ARTIST:Green Day
#EDITION:UltraStar Green Day
#MP3:Green Day - 86.mp3
#COVER:Green Day - 86 [CO].jpg
#BACKGROUND:Green Day - 86 [BG].jpg
#BPM:190
#GAP:15684
: 11 2 8 What
: 14 2 8  brings
: 17 2 6  you
: 21 3 4  ar
: 26 4 6 ound? 
- 32
: 51 2 15 Did
: 53 3 15  you
: 57 3 16  lose
: 62 2 15  some
: 66 3 13 thing 
- 70
: 71 2 11  the
: 74 3 16  last
: 78 3 15  time
: 82 3 13  you
: 86 3 11  were
: 90 5 13  her
: 96 6 11 e? 
- 104
: 139 2 -1 You'll
: 142 2 8  ne
: 144 2 8 ver
: 147 4 9  find
: 152 2 8  it
: 154 1 6 ~
: 157 4 6  now
- 163
: 183 3 15 It's
: 187 4 16  bu
: 192 2 15 rried
: 195 4 13  deep
: 200 3 11  with
: 204 3 16  your
: 208 2 15  i
: 211 4 13 dent
: 216 3 11 i
: 221 5 13 ty
: 228 4 11 ~
- 234
: 261 2 11 so
: 263 3 11  stand
: 266 2 15  a
: 269 3 16 side
: 273 3 15  and
: 277 3 13  let
: 281 5 11  the
: 288 5 16  next
: 294 6 13  one
: 301 6 13  pass
- 309
: 327 2 11 Don't
: 329 2 11  let
: 331 2 15  the
: 335 4 16  do
: 339 3 15 or
: 343 3 13  kick
: 347 6 11  you
: 354 5 16  in
: 360 6 13  the
: 367 8 13  ass
- 377
: 397 4 16 There's
: 403 2 15  no
: 406 2 13  re
: 409 7 15 turn
: 417 2 13  from
: 419 2 13  eight
: 422 2 11 y
: 426 5 13 six
- 433
: 462 4 16 There's
: 468 2 15  no
: 471 2 13  re
: 474 7 15 turn
: 482 2 13  from
: 484 2 13  eight
: 487 2 11 y
: 491 5 13 six 
- 498
: 527 4 16 There's
: 533 2 15  no
: 536 2 13  re
: 539 7 15 turn
: 547 2 13  from
: 550 2 13  eight
: 552 2 11 y
: 556 6 13 six 
- 564
: 591 4 16 There's
: 597 2 15  no
: 600 2 13  re
: 604 5 15 turn
: 612 2 13  from
: 614 2 13  eight
: 616 2 11 y
: 620 5 13 six 
- 627
: 630 4 13 Don't
: 635 3 9  e
: 639 4 11 ven
: 644 7 11  try
- 653
: 660 2 8 Ex
: 664 2 8 it
: 667 3 9  out
: 671 2 8  the
: 673 1 6 ~
: 676 5 6  back
- 683
: 703 3 15 And
: 707 3 16  ne
: 711 3 15 ver
: 715 3 13  show
: 719 3 11  your
: 723 3 16  head
: 727 3 15  a
: 731 3 13 round
: 735 3 11  a
: 739 6 13 gain
: 746 4 11 ~
- 752
: 790 3 8 Pur
: 794 2 8 chase
: 796 3 9  your
: 801 2 8  tick
: 803 1 6 ~
: 806 5 6 et 
- 813
: 833 3 15 And
: 837 3 16  quick
: 841 3 15 ly
: 845 3 13  take
: 850 2 11  the
: 853 3 16  last
: 857 3 15  train
: 861 3 13  out
: 865 3 11  of
: 870 6 13  to
: 877 5 11 wn
- 884
: 909 2 11 so
: 912 2 11  stand
: 914 2 15  a
: 917 4 16 side
: 922 3 15  and
: 926 3 13  let
: 930 4 11  the
: 936 4 16  next
: 942 5 13  one
: 948 7 13  pass
- 957
: 975 2 11 Don't
: 977 2 11  let
: 980 2 15  the
: 983 4 16  do
: 987 3 15 or
: 991 4 13  kick
: 996 4 11  you
: 1002 5 16  in
: 1008 6 13  the
: 1016 6 13  ass
- 1024
: 1044 4 16 There's
: 1050 2 15  no
: 1053 2 13  re
: 1056 7 15 turn
: 1064 2 13  from
: 1066 2 13  eight
: 1069 2 11 y
: 1073 5 13 six 
- 1080
: 1109 4 16 There's
: 1115 2 15  no
: 1118 2 13  re
: 1121 6 15 turn
: 1128 2 13  from
: 1131 2 13  eight
: 1133 2 11 y
: 1137 6 13 six 
- 1145
: 1172 4 16 There's
: 1178 2 15  no
: 1181 2 13  re
: 1185 6 15 turn
: 1192 2 13  from
: 1195 2 13  eight
: 1197 2 11 y
: 1201 6 13 six 
- 1209
: 1236 4 16 There's
: 1242 2 15  no
: 1245 2 13  re
: 1249 6 15 turn
: 1256 2 13  from
: 1258 2 13  eight
: 1261 2 11 y
: 1265 6 13 six
- 1272
: 1274 5 13 Don't
: 1280 3 9  e
: 1284 4 11 ven
: 1289 10 11  try
- 1301
: 1634 4 16 There's
: 1640 2 15  no
: 1643 2 13  re
: 1646 7 15 turn
: 1654 2 13  from
: 1656 2 13  eight
: 1659 2 11 y
: 1662 6 13 six 
- 1670
: 1697 4 16 There's
: 1703 2 15  no
: 1706 2 13  re
: 1709 7 15 turn
: 1717 2 13  from
: 1720 2 13  eight
: 1722 2 11 y
: 1725 6 13 six 
- 1733
: 1761 4 16 There's
: 1767 2 15  no
: 1770 2 13  re
: 1773 7 15 turn
: 1781 2 13  from
: 1783 2 13  eight
: 1785 2 11 y
: 1789 6 13 six 
- 1797
: 1823 4 16 There's
: 1829 3 15  no
: 1832 2 13  re
: 1836 6 15 turn
: 1843 2 13  from
: 1846 2 13  eight
: 1848 2 11 y
: 1852 5 13 six 
- 1861
: 1861 5 13 Don't
: 1867 3 9  e
: 1871 4 11 ven
: 1877 10 11  try
E
