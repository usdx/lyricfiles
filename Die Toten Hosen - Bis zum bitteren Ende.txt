#TITLE:Bis zum bitteren Ende
#ARTIST:Die Toten Hosen
#MP3:Die Toten Hosen - Bis zum bitteren Ende.mp3
#COVER:Die Toten Hosen - Bis zum bitteren Ende [CO].jpg
#BACKGROUND:Die Toten Hosen - Bis zum bitteren Ende [BG].jpg
#BPM:400
#GAP:1150
#ENCODING:UTF8
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
: 0 9 4 Und
: 10 10 4  die
: 22 9 4  Jah
: 33 7 4 re
: 43 6 4  zie
: 50 4 4 hen
: 56 9 2  ins
: 66 8 0  Land
- 75
: 77 4 5 und
: 82 4 5  wir
: 88 9 5  trin
: 99 7 5 ken
: 109 5 5  im
: 115 4 7 mer
: 120 7 5  noch
: 131 5 4  oh
: 137 4 2 ne
: 142 8 2  Ver
: 152 10 2 stand.
- 164
: 171 5 4 Denn
: 177 8 4  eins,
: 191 5 4  das
: 197 4 4  wis
: 202 5 4 sen
: 213 5 4  wir
: 219 6 4  ganz
: 229 7 2  ge
: 240 8 0 nau:
- 249
: 251 4 5 Oh
: 256 4 5 ne
: 261 9 5  Alk,
: 272 7 5  da
: 283 5 5  wä
: 289 4 7 re
: 294 9 5  der
: 305 5 4  All
: 311 4 2 tag
: 316 7 2  zu
: 326 10 2  grau.
- 338
: 348 12 4 Korn,
: 370 12 4  Bier,
: 392 10 4  Schnaps
: 404 8 2  und
: 414 8 0  Wein
- 423
: 425 4 5 und
: 430 4 5  wir
: 435 9 5  hö
: 446 7 5 ren
: 457 4 5  un
: 462 5 7 se
: 468 6 5 re
: 479 9 4  Le
: 489 9 2 ber
: 500 8 2  schreien.
- 510
: 518 4 4 Und
: 523 8 4  wenn
: 533 8 4  ein
: 544 11 4 mal
: 560 5 4  der
: 567 7 4  Ab
: 577 7 2 schied
: 588 7 0  naht,
- 597
: 599 5 5 sa
: 605 5 5 gen
: 611 8 5  al
: 621 6 5 le:
: 631 6 5  Das
: 638 4 7  hab
: 643 4 5  ich
: 648 4 5  schon
: 653 4 4  im
: 658 6 2 mer
: 665 7 2  ge
: 674 13 2 ahnt.
- 689
: 1398 7 4 Und
: 1408 9 4  die
: 1419 8 4  Jah
: 1429 7 4 re
: 1439 6 4  zie
: 1446 4 4 hen
: 1452 9 2  ins
: 1462 8 0  Land
- 1472
: 1475 4 5 und
: 1480 4 5  wir
: 1486 9 5  trin
: 1497 7 5 ken
: 1507 5 5  im
: 1513 4 7 mer
: 1518 7 5  noch
: 1529 5 4  oh
: 1535 4 2 ne
: 1540 8 2  Ver
: 1550 10 2 stand.
- 1562
: 1567 5 4 Denn
: 1573 9 4  eins,
: 1584 6 4  das
: 1594 5 4  wis
: 1600 5 4 sen
: 1607 5 4  wir
: 1616 7 4  ganz
: 1627 7 2  ge
: 1637 8 0 nau:
- 1647
: 1649 4 5 Oh
: 1654 4 5 ne
: 1659 9 5  Alk,
: 1670 7 5  da
: 1681 5 5  wä
: 1687 4 7 re
: 1692 9 5  der
: 1703 5 4  All
: 1709 4 2 tag
: 1714 7 2  zu
: 1724 10 2  grau.
- 1736
: 1746 12 4 Korn,
: 1768 12 4  Bier,
: 1790 10 4  Schnaps
: 1802 8 2  und
: 1812 8 0  Wein
- 1821
: 1823 4 5 und
: 1828 4 5  wir
: 1833 9 5  hö
: 1844 7 5 ren
: 1855 4 5  un
: 1860 5 7 se
: 1866 6 5 re
: 1877 9 4  Le
: 1887 9 2 ber
: 1898 8 2  schreien.
- 1908
: 1915 4 4 Und
: 1920 8 4  wenn
: 1930 8 4  ein
: 1941 11 4 mal
: 1956 4 4  der
: 1962 7 4  Ab
: 1973 8 2 schied
: 1984 8 0  naht,
- 1994
: 1996 5 5 sa
: 2002 5 5 gen
: 2008 8 5  al
: 2018 6 5 le:
: 2028 6 5  Das
: 2035 4 7  hab
: 2040 4 5  ich
: 2045 4 5  schon
: 2050 4 4  im
: 2055 6 2 mer
: 2062 7 2  ge
: 2071 13 2 ahnt.
- 2086
: 2096 7 4 Und
: 2106 8 4  die
: 2116 9 4  Jah
: 2127 7 4 re
: 2137 6 4  zie
: 2144 4 4 hen
: 2150 9 2  ins
: 2160 8 0  Land
- 2170
: 2172 4 5 und
: 2177 4 5  wir
: 2183 9 5  trin
: 2194 7 5 ken
: 2204 5 5  im
: 2210 4 7 mer
: 2215 7 5  noch
: 2226 5 4  oh
: 2232 4 2 ne
: 2237 8 2  Ver
: 2247 10 2 stand.
- 2259
: 2263 5 4 Denn
: 2269 9 4  eins,
: 2280 6 4  das
: 2290 5 4  wis
: 2296 5 4 sen
: 2303 5 4  wir
: 2312 6 4  ganz
: 2322 7 2  ge
: 2333 8 0 nau:
- 2343
: 2345 4 5 Oh
: 2350 4 5 ne
: 2355 9 5  Alk,
: 2366 7 5  da
: 2377 5 5  wä
: 2383 4 7 re
: 2388 9 5  der
: 2399 5 4  All
: 2405 4 2 tag
: 2410 7 2  zu
: 2420 10 2  grau.
- 2432
: 2442 12 4 Korn,
: 2464 12 4  Bier,
: 2486 10 4  Schnaps
: 2498 8 2  und
: 2508 8 0  Wein
- 2518
: 2521 4 5 und
: 2526 4 5  wir
: 2531 9 5  hö
: 2542 7 5 ren
: 2553 4 5  un
: 2558 5 7 se
: 2564 6 5 re
: 2575 9 4  Le
: 2585 9 2 ber
: 2596 8 2  schreien.
- 2606
: 2613 4 4 Und
: 2618 8 4  wenn
: 2628 8 4  ein
: 2639 11 4 mal
: 2654 4 4  der
: 2660 7 4  Ab
: 2671 8 2 schied
: 2682 8 0  naht,
- 2692
: 2694 5 5 sa
: 2700 5 5 gen
: 2706 8 5  al
: 2716 6 5 le,
: 2726 6 5  das
: 2733 4 7  hab
: 2738 4 5  ich
: 2743 4 5  schon
: 2748 4 4  im
: 2753 6 2 mer
: 2760 7 2  ge
: 2769 12 2 ahnt.
- 2783
: 2792 7 4 Und
: 2802 8 4  die
: 2812 9 4  Jah
: 2823 7 4 re
: 2833 6 4  zie
: 2840 4 4 hen
: 2846 9 2  ins
: 2856 8 0  Land
- 2866
: 2868 4 5 und
: 2873 4 5  wir
: 2879 9 5  trin
: 2890 7 5 ken
: 2900 5 5  im
: 2906 4 7 mer
: 2911 7 5  noch
: 2922 5 4  oh
: 2928 4 2 ne
: 2933 8 2  Ver
: 2943 10 2 stand.
- 2955
: 2960 5 4 Denn
: 2966 9 4  eins,
: 2977 6 4  das
: 2987 5 4  wis
: 2993 5 4 sen
: 3000 5 4  wir
: 3009 6 4  ganz
: 3021 5 2  ge
: 3030 8 0 nau:
- 3040
: 3042 4 5 Oh
: 3047 4 5 ne
: 3052 9 5  Alk,
: 3063 7 5  da
: 3074 5 5  wä
: 3080 4 7 re
: 3085 9 5  der
: 3096 5 4  All
: 3102 4 2 tag
: 3107 7 2  zu
: 3117 10 2  grau.
- 3129
: 3139 12 4 Korn,
: 3161 12 4  Bier,
: 3183 10 4  Schnaps
: 3195 8 2  und
: 3205 8 0  Wein
- 3214
: 3216 4 5 und
: 3221 4 5  wir
: 3226 9 5  hö
: 3237 7 5 ren
: 3248 4 5  un
: 3253 5 7 se
: 3259 6 5 re
: 3270 9 4  Le
: 3280 9 2 ber
: 3291 8 2  schreien.
- 3301
: 3308 4 4 Und
: 3313 8 4  wenn
: 3323 8 4  ein
: 3334 11 4 mal
: 3349 4 4  der
: 3355 7 4  Ab
: 3366 8 2 schied
: 3377 8 0  naht,
- 3386
: 3388 5 5 sa
: 3394 5 5 gen
: 3400 8 5  al
: 3410 6 5 le:
: 3420 6 5  Das
: 3427 4 7  hab
: 3432 4 5  ich
: 3437 4 5  schon
: 3442 4 4  im
: 3447 6 2 mer
: 3454 7 2  ge
: 3463 13 2 ahnt.
- 3478
: 3508 5 4 Im
: 3514 4 2 mer
: 3518 8 4  ge
: 3527 15 0 ahnt,
: 3550 5 4  im
: 3556 6 2 mer
: 3563 5 4  ge
* 3571 51 0 a
* 3623 63 -1 ~
* 3687 47 0 ~hnt
E