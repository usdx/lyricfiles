#TITLE:Auferstehung
#ARTIST:Eisregen - Pest 4 (4)
#MP3:Eisregen - Pest 4 (4) Auferstehung.mp3
#COVER:Eisregen - Pest 4 (4) Auferstehung [CO].jpg
#BACKGROUND:Eisregen - Pest 4 (4) Auferstehung [BG].jpg
#BPM:300
#GAP:15350
#ENCODING:UTF8
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
: 0 2 0 Wie
: 4 2 0 viel
: 9 4 0  Zeit
: 16 3 0  ist
: 20 2 0  ver
: 24 3 0 gan
: 28 3 0 gen?
- 32
: 34 3 0 Ich
: 39 5 0  weiß
: 46 3 0  es
: 51 5 0  nicht,
- 58
: 60 3 0 es
: 64 3 0  be
: 69 4 0 deu
: 76 4 0 tet
: 82 5 0  mir
: 89 3 0  nichts
: 97 9 0  mehr.
- 108
: 122 2 0 Ich
: 126 3 0  bin
: 131 5 0  tot
- 138
: 141 3 0 und
: 145 3 0  den
: 149 5 0 noch
: 160 5 0  wand
: 167 4 0 le
: 173 8 0  ich.
- 183
: 246 3 0 Mei
: 250 2 0 ne
: 254 2 0  Zü
: 258 2 0 ge
: 262 3 0  von
: 266 2 0  der
: 270 5 0  Pest
: 278 3 0  ge
: 283 5 0 zeich
: 290 6 0 net.
- 298
: 308 3 0 Beu
: 312 3 0 len
: 317 3 0  ü
: 322 2 0 ber
: 326 3 0 all
: 330 2 0  an
: 334 5 0  mei
: 341 3 0 nem
: 346 7 0  Leib.
- 355
: 370 3 0 Mei
: 374 3 0 ne
: 378 3 0  Glie
: 383 3 0 der
: 389 2 0  zer
: 393 5 0 stört
- 399
: 400 2 0  von
: 405 3 0  den
: 410 3 0  Bis
: 414 4 0 sen
: 421 2 0  der
: 425 3 0  Rat
: 429 6 0 ten.
- 436
: 438 3 0 Au
: 443 3 0 gen
: 447 3 0  zer
: 452 7 0 fetzt
: 462 3 0  und
: 466 2 0  den
: 470 3 0 noch
: 474 2 0  se
: 478 2 0 he
: 482 10 0  ich.
- 494
: 753 3 0 Ich
: 757 3 0  ent
: 761 3 0 stei
: 766 4 0 ge
: 774 3 0  der
: 779 3 0  Gru
: 784 4 0 be,
- 790
: 794 4 0 wüh
: 799 3 0 le
: 803 3 0  mich
: 807 3 0  durch
: 812 5 0  all
: 819 3 0  die
: 824 4 0  Lei
: 832 4 0 chen.
- 838
: 885 3 0 Et
: 889 2 0 was
: 893 5 0  noch
: 918 4 0  gibt
: 926 4 0  es
: 934 2 0  zu
: 939 9 0  tun.
- 950
: 1013 4 0 Dann
: 1020 5 0  keh
: 1028 3 0 re
: 1033 5 0  ich
: 1043 5 0  zu
: 1051 5 0 rück,
: 1059 4 0  von
: 1067 5 0  wo
: 1075 4 0  ich
: 1080 7 0  kam.
- 1088
: 1090 5 0 Ich
: 1099 5 0  fin
: 1106 3 0 de
: 1111 4 0  die,
: 1118 3 0  die
: 1123 4 0  ich
: 1129 3 0  su
: 1134 4 0 che,
- 1140
: 1143 2 0 ver
: 1147 2 0 sam
: 1152 6 0 melt
: 1169 4 0  im
: 1176 3 0  Rat
: 1181 4 0 haus
: 1188 2 0  bei
: 1192 6 0  Nacht.
- 1200
: 1262 3 0 Er
: 1268 4 0 starr
: 1274 4 0 te
: 1281 2 0  Ge
: 1285 3 0 sich
: 1289 5 0 ter,
- 1295
: 1297 3 0  als
: 1301 5 0  ih
: 1309 5 0 re
: 1317 4 0  Run
: 1324 3 0 de
: 1328 3 0  ich
: 1333 3 0  stö
: 1338 4 0 re.
- 1344
: 1349 3 0 Man
: 1354 4 0 che
: 1361 3 0  ver
: 1365 3 0 su
: 1369 3 0 chen,
: 1373 3 0  zu
: 1377 2 0  ent
: 1381 3 0 kom
: 1386 4 0 men,
- 1392
: 1397 4 0 man
: 1402 7 0 che
: 1420 5 0  zu
: 1428 5 0  Eis
: 1436 2 0  er
: 1441 10 0 starrt.
- 1453
: 1515 3 0 Doch
: 1519 3 0  es
: 1524 6 0  gibt
: 1536 3 0  für
: 1541 5 0  nie
: 1548 5 0 mand
: 1556 5 0  ei
: 1565 3 0 ne
: 1572 5 0  Chan
: 1580 10 0 ce,
- 1592
: 1602 3 0 ge
: 1607 4 0 nau
: 1613 3 0 so
: 1618 5 0 we
: 1625 7 0 nig,
: 1644 3 0  wie
: 1648 3 0  sie
: 1653 8 0  mir
: 1684 5 0  ei
: 1692 3 0 ne
: 1697 6 0  ga
: 1704 7 0 ben.
- 1713
: 1772 5 0 Dann
: 1780 4 0  ist
: 1787 3 0  ge
: 1792 6 0 tan,
: 1801 3 0  was
: 1805 3 0  ge
: 1810 4 0 tan
: 1817 3 0  wer
: 1821 3 0 den
: 1826 5 0  mus
: 1833 7 0 ste,
- 1842
: 1855 5 0 fri
: 1863 5 0 sches
: 1872 11 0  Blut
: 1887 5 0  ü
: 1894 3 0 ber
: 1899 8 0 all
: 1925 4 0  auf
: 1931 4 0  mei
: 1937 3 0 nem
: 1943 3 0  Ge
: 1947 7 0 sicht.
- 1956
: 2016 2 0 Doch
: 2020 3 0  es
: 2025 6 0  ist
: 2039 5 0  nicht
: 2048 5 0  das
: 2056 5 0  mei
: 2064 3 0 ni
: 2068 7 0 ge,
- 2077
: 2086 6 0 son
: 2094 5 0 dern
: 2102 6 0  das
: 2117 5 0  mei
: 2125 5 0 ner
: 2132 5 0  Pei
: 2140 3 0 ni
: 2145 10 0 ger
- 2157
: 2507 2 0 Und
: 2511 2 0  ich
: 2515 2 0  keh
: 2520 2 0 re
: 2526 2 0  zu
: 2529 4 0 rück
: 2536 3 0  in
: 2540 2 0  das
: 2544 5 0  Reich
: 2551 3 0  der
: 2556 4 0  To
: 2563 4 0 ten,
- 2569
: 2575 3 0 bet
: 2579 2 0 te
: 2583 5 0  mein
: 2590 7 0  Haupt
: 2601 3 0  auf
: 2605 4 0  fau
: 2610 3 0 len
: 2614 4 0 des
: 2621 74 0  Fleisch.
- 2697
: 3567 4 0 Bald
: 3573 4 0  wer
: 3578 2 0 de
: 3583 4 0  ich
: 3591 4 0  eins
: 3599 7 0  sein
- 3608
: 3616 3 0  mit
: 3620 3 0  der
: 3625 5 0  Ern
: 3632 3 0 te
: 3637 3 0  der
: 3641 7 0  Pest.
E