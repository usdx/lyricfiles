#TITLE:Halo & Walking On Sunshine
#ARTIST:Glee Cast
#MP3:Glee Cast - Halo & Walking On Sunshine.mp3
#VIDEO:Glee Cast - Halo & Walking On Sunshine.mp4
#COVER:Glee Cast - Halo & Walking On Sunshine[CO].jpg
#BPM:217,88
#GAP:9310
#ENCODING:UTF8
#PREVIEWSTART:9,310
#LANGUAGE:English
#GENRE:Musical
: 0 28 1 Oh
- 30
: 42 1 -3 Re
: 44 1 1 mem
: 46 1 1 ber
: 48 3 1  those
: 54 1 1  wal
: 56 1 -1 ls
: 58 1 -1  I
: 60 12 2  built
- 74
: 78 1 -6 Well,
: 80 1 -1  ba
: 82 1 -1 by
: 84 3 -1  they're
: 90 1 1  tum
: 92 1 1 b
: 94 1 -3 ling
: 96 4 -3  do
: 102 6 -6 wn
- 110
: 114 1 -6 They
: 116 1 -3  di
: 118 1 -3 dn't
: 120 1 -3  e
: 122 1 -3 ven
: 126 1 -3  put
: 128 1 -3  up
: 130 1 -3  a
: 132 1 -1  fi
: 134 10 -3 ght
- 146
: 150 1 -6 They
: 152 1 -3  di
: 154 1 -3 dn't
: 156 1 -3  e
: 158 1 -3 ven
: 160 3 -3  make
: 166 1 -3  a
: 168 10 -3  sound
- 180
: 186 1 -3 It's
: 188 1 4  like
: 190 1 4  I've
: 192 1 4  been
: 194 1 4  a
: 196 2 6 wa
: 200 6 6 ke
: 208 6 4 ned
- 216
: 220 1 1 E
: 222 1 2 very
: 224 1 4  rule
: 226 1 4  I
: 228 1 4  had
: 230 2 4  you
: 234 2 9  brea
: 238 4 9 ki
: 244 6 6 n'
- 252
: 256 1 1 It's
: 258 1 2  the
: 260 2 4  risk
: 264 1 4  that
: 266 1 1  I'm
: 268 3 -1  ta
: 274 10 -3 kin'
- 286
: 290 1 1 I
: 292 1 2 ~
: 294 1 4  ain't
: 296 1 4  ne
: 298 1 4 ver
: 300 1 4  gon
: 302 1 4 na
: 304 2 4  shut
: 310 1 8  you
: 312 4 9  o
: 317 1 11 ~
* 319 2 13 ut
- 323
: 332 1 9 E
: 334 1 9 very
: 336 1 9 where
: 338 1 9  I'm
: 340 3 9  loo
: 346 1 9 king
: 348 1 9  no
: 350 8 11 w
- 360
: 364 1 9 I'm
: 366 1 9  sur
: 368 1 11 roun
: 370 1 11 ded
: 372 2 11  by
: 376 4 13  your
: 382 1 9  em
: 384 3 9 bra
: 388 1 8 ~
: 390 6 6 ce
- 398
: 405 1 9 Ba
: 407 1 9 by
: 409 1 9  I
: 411 1 9  can
: 413 3 9  feel
: 418 1 9  your
: 420 4 9  ha
: 425 1 4 lo
: 427 1 2 ~
: 429 4 1 ~
- 435
: 437 2 6 And
: 441 2 9  don't
: 445 1 6  it
: 447 2 9  feel...
- 450
: 451 1 9 I
: 453 1 9  can
: 455 1 9  feel
: 457 1 11  your
: 459 2 13  ha
: 463 2 11 lo
: 467 2 13  ha
: 471 4 11 lo
: 477 2 13  ha
: 481 2 11 lo
- 484
: 485 1 9 I
: 487 1 9  can
: 489 1 9  see
: 491 1 11  your
: 493 2 13  ha
: 497 4 11 lo
: 503 2 13  ha
: 507 4 11 lo
: 513 2 14  ha
: 517 3 13 lo
- 521
: 522 1 9 I
: 524 1 9  can
: 526 1 9  feel
: 528 1 11  your
: 530 2 13  ha
: 534 4 11 lo
: 539 2 13  ha...
: 545 1 16  ha
: 547 1 11 ~
: 549 1 13 ~
: 551 1 8 ~
: 553 1 11 ~
: 555 1 6 ~
- 557
: 557 1 9 I
: 559 1 9  can
: 561 1 9  see
: 563 1 11  your
: 565 1 13  ha
: 567 1 11 ~
: 569 4 11 lo
: 575 2 13  ha...
- 578
: 579 2 6 And
: 583 1 9  don't
: 585 2 6  it
: 589 4 9  feel...
- 595
: 597 1 -3 I
: 599 2 1  used
: 603 2 4  to
: 607 2 4  think
: 611 6 4  may
: 619 2 4 be
: 623 2 1  you
: 627 6 4  loved
: 635 3 6  me
- 640
: 643 2 1 now
: 647 2 -1  ba
: 651 1 -3 by
: 653 2 -6  I'm
: 659 6 -1  su
: 667 10 -3 re
- 679
: 723 2 6 And
: 727 2 9  I
: 731 2 6  just
: 735 2 4  can't
: 739 4 6  wait
- 745
: 747 2 4 till
: 751 2 1  the
: 755 4 4  day
: 763 2 -1  when
: 767 2 1  you
: 771 6 -1  knock
: 779 2 -3  on
: 783 2 -6  my
: 787 6 -1  do
: 795 8 -3 or
- 805
: 819 2 9 Oh
: 823 2 4  now
: 827 2 4  now!
- 831
: 835 2 6 I'm
: 839 2 9  wal
: 843 1 6 king
: 845 2 2  on
: 851 6 6  sun
: 859 8 4 shine,
: 875 6 11  wo
: 883 4 9 oah
: 889 2 6 ~!
- 893
: 899 2 6 I'm
: 903 2 9  wal
: 907 1 6 king
: 909 2 2  on
: 915 6 6  sun
: 923 8 4 shine,
: 939 6 11  wo
: 947 4 9 oah
: 953 2 6 ~!
- 957
: 964 2 6 And
: 968 2 9  don't
: 971 1 6  it
: 974 4 9  feel
: 980 2 13  good!
- 983
: 983 1 9 I
: 985 1 9  can
: 987 1 9  feel
: 989 1 11  your
: 991 2 13  ha
: 995 2 11 lo
: 999 2 13  ha
: 1003 2 11 lo
: 1007 2 13  ha
: 1011 2 11 ~
: 1015 14 11 lo
: 1031 10 9 ~
- 1043
: 1047 1 9 I
: 1049 1 9  can
: 1051 1 9  see
: 1053 1 11  your
: 1055 2 13  ha
: 1059 2 11 lo
: 1063 2 13  ha
: 1067 4 11 lo
- 1073
: 1111 1 9 I
: 1113 1 9  can
: 1115 1 9  feel
: 1117 1 11  your
: 1119 2 13  ha
: 1123 2 11 lo
: 1127 2 13  ha
: 1131 2 11 lo
: 1135 2 13  ha
: 1139 2 11 ~
: 1143 10 11 lo
: 1155 1 13 ~
: 1157 12 14 ~
- 1171
: 1175 1 9 I
: 1177 1 9  can
: 1179 1 9  see
: 1181 1 11  your
: 1183 2 13  ha
: 1187 2 11 lo
* 1191 2 13  ha
: 1195 4 11 lo
- 1201
: 1207 1 9 I
: 1209 1 9  can
: 1211 1 9  see
: 1213 1 11  your
: 1215 2 13  ha
: 1219 2 11 lo
: 1223 2 13  ha
: 1227 2 11 lo
- 1230
: 1231 1 9 I
: 1233 1 9  can
: 1235 1 9  see
: 1237 1 11  your
: 1239 2 13  ha
: 1243 4 11 lo
- 1249
: 1253 2 9 HA
: 1257 25 21 LO!
E