#TITLE:Schlaflied
#ARTIST:Die Ärzte
#MP3:Die Ärzte - Schlaflied.mp3
#COVER:Die Ärzte - Schlaflied [CO].jpg
#BACKGROUND:Die Ärzte - Schlaflied [BG].jpg
#BPM:210
#GAP:930
#ENCODING:UTF8
#EDITION:[SC]-Songs
: 1 11 0 Schlaf,
* 14 9 12  mein
: 25 8 11  Kind
: 36 7 7 chen,
: 48 9 9  schla
: 60 8 4 fe
: 71 8 7  ein
- 81
: 91 2 7 Die
: 95 8 9  Nacht,
: 106 7 4  sie
: 117 8 7  schaut
: 128 5 4  zum
: 139 10 2  Fens
* 151 9 11 ter
: 164 7 9  rein
- 173
: 182 3 9 Der
: 187 7 0  run
* 198 8 12 de
: 210 10 11  Mond,
: 228 2 7  er
: 233 6 9  hat
: 244 8 4  dich
: 255 8 7  ger
: 267 6 4 ne
- 275
: 279 9 9 Und
: 290 7 4  es
: 302 7 7  leuch
: 313 7 4 ten
: 325 9 2  dir
* 336 7 11  die
: 347 9 9  Ster
: 358 3 7 ne
- 363
: 369 11 0 Schlaf,
* 382 9 12  mein
: 393 9 11  Klei
: 405 6 7 nes,
: 417 10 9  träu
: 429 8 4 me
: 440 12 7  süß
- 454
: 463 10 9 Bald
: 474 10 4  bist
: 486 10 7  du
: 498 5 4  im
: 509 7 2  Pa
* 520 8 11 ra
: 532 8 9 di
: 542 5 7 es
- 549
: 555 7 0 Denn
* 566 9 12  gleich
: 578 7 11  öff
: 589 6 7 net
: 601 7 9  sich
: 612 9 4  die
: 623 9 7  Tür
- 634
: 647 9 9 Und
: 659 7 4  ein
: 670 8 7  Mons
: 682 7 4 ter
: 693 8 2  kommt
* 704 10 11  zu
: 716 9 9  dir
- 727
: 735 3 9 Mit
: 739 6 0  sei
: 746 4 0 nen
* 751 9 12  elf
: 761 8 11  Au
: 770 10 7 gen
: 783 8 9  schaut
: 792 5 9  es
: 799 4 4  dich
: 806 7 7  an
- 815
: 827 2 7 Und
: 831 8 9  schleicht
: 842 8 4  sich
: 854 8 7  an
: 866 6 4  dein
: 878 8 2  Bett
* 889 8 11 chen
: 899 11 9  ran
- 912
: 918 3 9 Du
: 923 8 0  liegst
: 934 10 12  still
: 946 7 11  da,
: 965 2 7  be
: 969 8 9 wegst
: 981 7 4  dich
: 992 8 7  nicht
- 1002
: 1011 2 4 Das
: 1015 5 9  Mons
: 1022 6 4 ter
: 1032 3 4  zer
: 1037 9 7 kratzt
: 1050 5 4  dir
: 1061 8 2  dein
* 1071 9 11  Ge
: 1083 9 9 sicht
- 1094
: 1107 9 0 Sei
* 1119 6 12 ne
: 1129 2 11  Fin
: 1133 7 7 ger
: 1146 4 7  sind
: 1152 8 9  lang
: 1170 5 4  und
: 1176 9 7  dünn
- 1187
: 1199 10 9 Wehr
: 1210 7 4  dich
: 1221 9 7  nicht
: 1234 6 4  s'hat
: 1245 9 2  kei
* 1257 9 11 nen
: 1268 9 9  Sinn
- 1279
: 1291 7 0 Und
* 1303 7 12  es
: 1313 5 11  kich
: 1318 9 7 ert
: 1337 3 7  wi
: 1342 5 9 e
: 1349 8 4  ver
: 1360 6 7 rückt
- 1368
: 1383 8 9 Als
: 1394 7 4  es
: 1405 8 7  dei
: 1416 6 4 nen
: 1427 9 2  Hals
* 1441 7 11  zu
: 1452 7 9  drückt.
- 1461
: 1470 2 9 Du
: 1474 9 12  schreist,
: 1491 2 12  doch
: 1495 2 9  du
: 1499 7 11  bist
: 1517 2 7  al
: 1522 8 9 lein
: 1538 3 4  zu
: 1544 9 7  Haus'
- 1555
: 1562 3 7 Das
: 1567 10 9  Mons
: 1579 5 4 ter
: 1590 7 7  sticht
: 1601 5 4  dir
: 1608 2 4  die
: 1613 6 2  Au
* 1624 6 11 gen
: 1636 11 9  aus
- 1649
: 1654 3 9 Dann
: 1659 10 0  bist
: 1670 5 12  du
: 1681 8 7  still
: 1701 3 7  und
: 1705 7 9  das
: 1717 5 4  ist
: 1728 8 7  gut
- 1738
: 1747 2 9 Es
: 1752 5 9  beißt
: 1759 2 9  dir
: 1763 5 4  in
: 1770 3 4  den
: 1775 12 7  Hals
: 1792 3 4  und
: 1797 6 2  trinkt
* 1808 8 11  dein
: 1819 9 9  Blut
- 1829
: 1831 5 7 Oh
* 1838 3 7 ne
: 1842 11 0  Blut
: 1866 4 7  bist
: 1871 5 7  du
: 1889 10 9  bleich
: 1901 6 4  wie
: 1912 9 7  Krei
: 1923 4 4 de
- 1928
: 1930 3 9 Dann
: 1935 6 9  frisst
: 1946 4 4  es
: 1958 6 7  dei
: 1970 5 4 ne
: 1982 7 2  Ein
* 1994 8 11 ge
: 2004 9 9 wei
: 2015 3 7 de
- 2020
: 2022 4 7 Dein
: 2028 5 12  klei
: 2036 4 11 nes
: 2049 5 11  Bett
: 2055 5 9 chen
: 2068 4 7  vom
: 2074 9 9  Blut
: 2090 5 4  ganz
: 2097 10 7  rot
- 2109
: 2114 3 7 Die
: 2119 4 9  Son
: 2124 5 9 ne
: 2131 7 4  geht
: 2142 8 7  auf
: 2161 4 4  und
: 2166 7 2  du
* 2177 7 11  bist
: 2188 12 9  tot
- 2202
: 2233 10 0 Schlaf,
* 2246 8 12  mein
: 2257 8 11  Kind
: 2268 5 7 chen,
: 2279 10 9  schlaf
: 2291 7 4  jetzt
: 2303 6 7  ein
- 2311
: 2322 3 7 Am
: 2327 7 9  Him
: 2337 7 4 mel
: 2348 10 7  steh'n
: 2360 5 4  die
: 2371 9 2  Ster
* 2384 7 11 ne
: 2394 9 9 lein
- 2405
: 2417 10 0 Schlaf,
* 2429 8 12  mein
: 2441 9 11  Klei
: 2452 4 7 nes,
: 2463 8 9  schla
: 2475 5 4 fe
: 2485 5 7  schnell
- 2492
: 2505 4 7 Dein
: 2511 7 9  Bett
: 2521 7 4 chen
: 2533 8 7  ist
: 2544 6 4  ein
: 2556 8 2  Ka
* 2568 8 11 rus
: 2579 8 9 sell
- 2589
: 2601 10 0 Schlaf,
* 2614 8 12  mein
: 2625 7 11  Kind
: 2636 4 7 chen,
: 2647 9 9  schlaf
: 2659 6 4  jetzt
: 2671 7 7  ein
- 2680
: 2689 3 7 Sonst
: 2694 10 9  kann
: 2706 8 4  das
: 2717 8 7  Mons
: 2728 7 4 ter
: 2740 8 2  nicht
* 2751 8 11  hi
: 2763 9 7 nein
E