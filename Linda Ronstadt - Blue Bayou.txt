#TITLE:Blue Bayou
#ARTIST:Linda Ronstadt
#MP3:Linda Ronstadt - Blue Bayou.mp3
#COVER:Linda Ronstadt - Blue Bayou [CO].jpg
#BACKGROUND:Linda Ronstadt - Blue Bayou [BG].jpg
#BPM:95
#GAP:5000
#ENCODING:UTF8
: 0 2 67 I
: 2 4 67  feel
: 6 3 69  so
: 9 5 71  bad,
: 14 3 72  got
: 17 1 71  a
: 18 8 71  worried
: 26 7 69  mind;
- 35
: 37 3 67 I'm
: 40 3 69  so
: 43 2 71  lone
: 45 5 72 some
: 50 4 71  all
: 54 1 69  of
: 55 1 71  the
: 56 7 69  time
- 65
: 69 4 67 Since
: 73 1 69  I
: 74 3 71  left
: 77 5 72  my
: 82 3 71  ba
: 85 2 71 by
: 87 8 69  behind
- 95
: 95 4 67 On
: 99 2 72  Blue
: 101 3 72  Ba
: 104 2 69 yo
: 106 2 67 u
- 120
: 133 4 67 Sav
: 137 2 69 ing
: 139 2 71  nic
: 141 5 72 kels,
: 146 4 69  sav
: 150 2 71 ing
: 152 9 69  dimes;
- 163
: 166 3 67 Work
: 169 2 69 ing
: 171 5 71  till
: 176 1 72  the
: 177 4 71  sun
: 181 3 71  don't
: 184 7 69  shine
- 193
: 197 3 67 Look
: 200 2 69 ing
: 202 3 71  for
: 205 4 72 ward
: 209 1 71  to
: 210 3 69  happ
: 213 3 71 ier
: 216 7 69  times
- 223
: 223 1 71 O
: 224 2 69 n
: 226 3 72  Blue
: 229 3 72  Ba
: 232 2 69 yo
: 234 3 67 u
- 245
: 252 2 84 I'm
: 254 3 84  go
: 257 1 84 ing
: 258 3 84  back
: 261 3 83  some
: 264 6 83  day,
- 271
: 273 4 84 Come
: 277 3 83  what
: 280 4 83  may
: 284 5 81  to
: 289 4 83  Blue
: 293 3 81  Ba
: 296 12 79 you
- 312
: 317 3 81 Where
: 320 2 83  the
: 322 3 84  folks
: 325 3 83  are
: 328 4 83  fine,
- 333
: 333 4 83 And the
: 337 7 84  world
: 344 1 83  is
: 345 5 83  mine
: 350 3 81  on
: 353 4 83  Blue
: 357 4 81  Ba
: 361 11 79 you
- 375
: 381 3 83 Where
: 384 2 84  those
: 386 2 86  fish
: 388 3 84 ing
: 391 5 84  boats
- 397
: 397 4 84 With their
: 401 7 86  sails
: 408 1 86  a
: 409 6 86 float,
: 415 3 86 If
: 418 6 86  I
: 424 2 84  could
: 426 3 81  on
: 429 3 77 ly
: 432 9 74  see
- 443
: 446 2 79 That
: 448 1 79  fa
: 449 3 81 mili
: 452 1 79 ar
: 453 3 79  sun
: 456 6 79 rise
- 462
: 462 3 79 Through
: 465 3 81  slee
: 468 4 83 py
: 472 5 81  eye
: 477 1 79 s,
: 478 3 76  How
: 481 5 74  happy
: 486 2 74  I'd
: 488 6 74  be
- 505
: 517 2 67 Gon
: 519 1 67 na
: 520 3 69  see
: 523 6 72  my
: 529 3 71  ba
: 532 2 69 by
: 534 1 71  a
: 535 7 69 gain
- 545
: 549 4 67 Gonna
: 553 3 69  be
: 556 4 71  with
: 560 4 72  some
: 564 1 71  of
: 565 2 71  my
: 567 10 69  friends
- 579
: 581 3 67 May
: 584 2 67 be
: 586 2 69  I'll
: 588 5 72  feel
: 593 3 71  bet
: 596 2 69 ter
: 598 1 71  a
: 599 6 69 gain
- 605
: 605 4 67 On
: 609 3 72  Blue
: 612 3 72  Ba
: 615 3 69 yo
: 618 3 67 u
- 635
: 644 3 67 Sav
: 647 2 69 ing
: 649 2 71  nic
: 651 6 72 kels,
: 657 4 69  sav
: 661 2 71 ing
: 663 11 69  dimes
- 674
: 676 3 67 Work
: 679 2 69 ing
: 681 6 71  till
: 687 1 72  the
: 688 3 71  sun
: 691 3 71  don't
: 694 8 69  shine
- 705
: 708 3 67 Look
: 711 2 67 ing
: 713 6 69  forward
: 719 2 71  to
: 721 2 72  happ
: 723 4 71 ier
: 727 6 69  times
: 733 3 67  On
: 736 3 72  Blue
: 739 4 69  Ba
: 743 5 67 you
- 755
: 762 2 84 I'm
: 764 3 84  go
: 767 1 84 ing
: 768 2 84  back
: 770 4 83  some
: 774 6 83 day,
- 782
: 784 3 84 Come
: 787 2 83  what
: 789 5 83  may
: 794 4 81  to
: 798 4 83  Blue
: 802 4 81  Ba
: 806 12 79 you
- 822
: 827 3 81 Where
: 830 1 83  the
: 831 3 84  folks
: 834 3 83  are
: 837 6 83  fine,
- 844
: 844 3 83 And the
: 847 7 84  world
: 854 1 83  is
: 855 5 83  mine
: 860 2 81  On
: 862 4 83  Blue
: 866 4 81  Ba
: 870 11 79 you
- 885
: 890 3 83 Where
: 893 2 84  those
: 895 2 86  fish
: 897 3 84 ing
: 900 5 84  boats
- 906
: 906 3 84 With their
: 909 8 86  sails
: 917 1 86  a
: 918 6 86 float,
: 924 2 86  If
: 926 6 86  I
: 932 2 84  could
: 934 4 81  on
: 938 3 77 ly
: 941 8 74  see
- 951
: 954 2 79 That
: 956 2 79  fa
: 958 2 81 mil
: 960 1 79 iar
: 961 3 79  sun
: 964 6 79 rise
- 971
: 972 1 79 Through
: 973 3 81  slee
: 976 4 83 py
: 980 5 81  eye
: 985 1 79 s,
: 986 3 76  How
: 989 5 74  happy
: 994 2 74  I'd
: 996 5 74  be
- 1099
: 1145 2 83 Oh
: 1147 2 84  that
: 1149 4 86  boy
: 1153 2 84  of
: 1155 9 84  mine
: 1164 4 84  by
: 1168 2 84  my
: 1170 8 84  side,
- 1178
: 1178 2 84 The
: 1180 7 86  sil
: 1187 2 84 ver
: 1189 4 81  moon
: 1193 2 77  and
: 1195 2 74  the
: 1197 6 74  evening
: 1203 6 74  time
- 1209
: 1209 3 79 Oh,
: 1213 3 81  some
: 1216 4 79  sweet
: 1220 2 79  da
: 1222 2 79 y,
- 1225
: 1226 2 79 Gon
: 1228 2 81 na
: 1230 2 83  take
: 1232 3 81  a
: 1235 7 79 way
: 1242 3 76  this
: 1245 5 74  hurtin'
: 1250 1 74  in
: 1251 8 74 side
- 1263
: 1271 3 79 Well
: 1274 3 79  I'd
: 1277 2 81  ne
: 1279 2 79 ver
: 1281 2 81  be
: 1283 6 79  blue,
- 1290
: 1291 2 79 My
: 1293 3 81  dreams
: 1296 3 83  come
: 1299 2 83  tr
: 1301 13 81 ue
- 1317
: 1320 5 79 On
: 1325 15 81  Blue
: 1340 17 83  Ba
: 1357 51 88 you
E