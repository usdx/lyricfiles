#TITLE:Winnie The Pooh
#ARTIST:Winnie The Pooh
#MP3:Winnie The Pooh - Winnie The Pooh.mp3
#COVER:Winnie The Pooh - Winnie The Pooh [CO].jpg
#BACKGROUND:Winnie The Pooh - Winnie The Pooh [BG].jpg
#BPM:340
#GAP:2029,41
#ENCODING:UTF8
#LANGUAGE:English
#EDITION:SingStar Singalong with Disney
: 0 9 70 Deep
: 10 7 69  In
: 19 9 67  The
: 30 18 64  Hun
: 50 10 62 dred
: 60 21 60  A
: 81 10 62 cre
: 91 10 60  Wood
- 103
: 110 11 69 Where
: 123 7 70  Chris
: 133 7 69 to
: 142 9 67 pher
: 152 19 64  Ro
: 171 9 67 bin
* 182 35 69  plays
- 219
: 235 8 60 You'll
: 245 7 70  find
: 255 7 69  the
: 264 7 67  en
: 274 19 64 chan
: 296 7 67 ted
- 304
* 305 20 72 Neigh
: 325 11 69 bor
: 336 14 65 hood
- 352
: 365 7 60 Of
: 379 7 62  Chris
: 390 7 64 to
: 400 8 65 pher's
: 411 9 64  child
: 420 12 62 ~
: 432 8 60 hood
: 442 28 65  days
- 472
: 494 16 69 A
: 510 5 70  don
: 517 2 69 key
: 521 8 67  named
* 535 10 64  Ee
: 547 11 62 yore
- 559
: 561 9 60 Is
: 573 4 62  his
: 581 10 60  friend
- 593
: 607 3 69 And
: 612 6 70  Kan
: 620 5 69 ga
: 631 2 67  and
: 636 11 64  lit
: 649 13 67 tle
: 662 25 69  Roo
- 689
: 699 10 72 There's
: 712 7 70  Rab
: 719 2 69 bit
- 723
: 734 3 67 And
: 738 6 64  Pig
: 746 4 67 let
- 752
* 762 9 72 And
: 775 8 69  there's
: 788 10 65  Owl
- 800
: 808 4 60 But
: 813 5 62  most
: 820 3 64  of
: 825 8 65  all
: 837 6 64  Win
: 845 3 62 nie
: 849 9 60  the
: 862 23 65  Pooh
- 887
: 909 5 64 Win
* 916 2 62 nie
: 920 4 64  the
: 929 5 62  Pooh
- 936
: 948 7 64 Win
: 955 2 62 nie
: 958 5 64  the
: 968 5 62  Pooh
- 975
: 990 3 60 Chub
: 996 2 60 by
* 1000 5 70  lit
: 1007 3 70 tle
: 1010 4 69  cub
: 1017 3 69 by
- 1021
: 1021 5 67 All
: 1030 5 65  stuffed
: 1042 4 64  with
: 1052 4 62  fluff
: 1062 9 63  he's
- 1072
: 1072 6 64 Win
: 1079 2 62 nie
: 1083 6 64  the
* 1092 3 62  Pooh
- 1097
: 1112 7 64 Win
: 1119 2 62 nie
: 1122 3 64  the
: 1132 6 62  Pooh
- 1140
: 1152 5 60 Wil
: 1160 2 60 ly
: 1163 5 70  nil
: 1170 3 70 ly
: 1173 5 69  sil
: 1179 3 69 ly
: 1183 5 67  old
: 1191 22 65  bear
- 1215
* 1229 6 64 Win
: 1235 2 62 nie
: 1238 3 64  the
: 1245 4 62  Pooh
- 1251
: 1262 5 64 Win
: 1267 2 62 nie
: 1270 4 64  the
: 1277 5 62  Pooh
- 1284
: 1295 3 60 Chub
: 1300 2 60 by
: 1303 4 70  lit
: 1309 3 70 tle
: 1312 3 69  cub
: 1317 2 69 by
- 1320
: 1320 4 67 All
: 1328 4 65  stuffed
: 1337 3 64  with
: 1345 3 62  fluff
: 1353 7 63  he's
- 1361
: 1362 3 64 Win
: 1367 2 62 nie
* 1370 3 64  the
: 1378 4 62  Pooh
- 1384
: 1395 6 64 Win
: 1401 2 62 nie
: 1404 4 64  the
: 1412 5 62  Pooh
- 1419
: 1429 3 60 Wil
: 1434 1 60 ly
: 1438 4 70  nil
: 1443 3 70 ly
: 1446 3 69  sil
: 1452 3 69 ly
* 1456 6 67  old
: 1465 29 65  bear
E