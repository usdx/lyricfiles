#TITLE:Coma White
#ARTIST:Marilyn Manson
#EDITION:[SC]-Songs
#LANGUAGE:English
#MP3:Marilyn Manson - Coma White.mp3
#COVER:Marilyn Manson - Coma White [CO].jpg
#BACKGROUND:Marilyn Manson - Coma White [BG].jpg
#BPM:160
#GAP:25200
: 0 3 11 There's
: 4 3 12  some
: 8 5 12 thing
: 21 7 11  cold
: 29 2 9  and
: 33 10 12  blank
- 45
: 65 3 11 Be
: 69 3 12 hind
: 73 3 12  her
: 81 8 11  smi
: 89 6 9 le
- 97
: 127 3 11 She's
: 132 4 12  stan
: 137 2 12 ding
: 141 3 12  on
: 145 3 12  an
: 150 6 11  o
: 157 3 9 ver
: 161 11 12 pass
- 174
: 190 2 11 In
: 193 2 11  a
: 197 3 12  mi
: 201 3 11 ra
: 205 2 12 cle
: 209 8 11  mi
: 217 11 9 le
- 230
: 241 3 9 Cause
* 245 9 16  you
: 257 7 9  were
: 269 2 9  from
: 273 2 9  a
* 277 4 16  per
* 281 5 14 fect
: 289 10 14  world
- 301
: 305 3 9 A
: 309 10 16  world
: 321 7 9  that
: 341 3 16  threw
: 345 3 14  me
: 349 4 12  a
: 354 7 11 way
: 365 3 14  to
: 369 5 11 da
: 374 10 9 ~y
- 386
: 396 4 9 To
: 401 23 12 day,
: 428 4 9  to
: 433 7 12 da
: 441 7 14 ~
: 449 9 9 y
- 460
: 465 2 9 To
: 469 3 16  ru
: 473 3 14 ~n
: 477 4 12  a
: 481 9 14 way
- 492
: 506 2 21 A
: 510 2 28  pill
: 513 3 26  to
: 517 3 28  make
: 521 5 28  you
: 529 5 28  numb
- 536
: 538 2 21 A
: 542 2 28  pill
: 545 3 26  to
: 549 3 28  make
: 553 5 28  you
: 561 5 28  dumb
- 567
: 569 2 21 A
: 573 2 28  pill
: 577 2 26  to
: 581 2 28  make
: 585 4 28  you
: 598 3 28  a
: 601 4 26 ny
: 609 7 24 bo
: 617 6 26 dy
: 626 5 21  else
- 632
: 634 2 21 But
: 638 2 28  all
: 642 2 26  the
: 646 3 28  drugs
: 650 4 28  in
: 657 5 28  this
: 665 8 24  world
- 675
: 685 5 19 Won't
: 692 4 28  save
: 697 4 26  her
: 705 5 24  from
: 713 5 26  her
: 721 11 21 self
- 734
: 898 3 11 Her
: 902 3 12  mouth
: 906 3 12  was
: 914 3 12  an
: 918 5 11  emp
: 925 2 9 ty
: 929 8 12  cut
- 939
: 957 3 11 She
: 961 3 11  was
: 965 3 12  wai
: 969 2 12 ting
: 973 2 12  to
: 977 9 11  fa
: 986 8 9 ~ll
- 996
: 1020 6 11 Just
: 1029 4 12  blee
: 1033 3 12 ding
: 1037 3 12  like
: 1041 3 12  a
: 1045 7 11  po
: 1053 3 9 la
: 1057 15 12 roid
- 1074
: 1090 2 11 That
: 1094 3 12  lost
: 1098 3 12  all
: 1102 3 12  her
: 1106 8 11  do
: 1114 18 9 lls
- 1134
: 1137 3 9 'Cause
* 1142 8 16  you
: 1153 7 9  were
: 1165 3 9  from
: 1169 3 9  a
* 1174 4 16  per
* 1178 4 14 fect
: 1185 10 14  world
- 1197
: 1202 3 9 A
: 1206 11 16  world
: 1218 9 9  that
: 1237 3 16  threw
: 1241 4 14  me
: 1246 3 12  a
: 1250 8 11 way
: 1262 3 14  to
: 1266 3 11 da
: 1270 12 9 ~y
- 1284
: 1293 4 9 To
: 1298 23 12 day,
: 1325 4 9  to
: 1330 7 12 da
: 1338 4 14 ~
: 1342 3 12 ~
: 1346 9 9 y
- 1357
: 1361 3 9 To
: 1365 4 16  run
: 1370 6 14  a
: 1378 8 14 way
- 1388
: 1402 2 21 A
: 1406 2 28  pill
: 1410 2 26  to
: 1414 2 28  make
: 1418 4 28  you
: 1426 5 28  numb
- 1432
: 1434 2 21 A
: 1438 2 28  pill
: 1442 2 26  to
: 1446 2 28  make
: 1450 4 28  you
: 1458 5 28  dumb
- 1464
: 1466 2 21 A
: 1470 2 28  pill
: 1474 2 26  to
: 1478 2 28  make
: 1482 4 28  you
: 1495 3 28  a
: 1498 5 26 ny
: 1506 7 24 bo
: 1514 6 26 dy
: 1522 6 21  else
- 1529
: 1530 2 21 But
: 1534 2 28  all
: 1538 2 26  the
: 1542 3 28  drugs
: 1546 4 28  in
: 1554 5 28  this
: 1562 8 24  world
- 1572
: 1582 4 19 Won't
: 1590 3 28  save
: 1594 3 26  her
: 1602 4 24  from
: 1610 6 26  her
: 1618 10 21 self
- 1630
: 1659 2 21 A
: 1663 2 28  pill
: 1667 2 26  to
: 1670 2 28  make
: 1674 6 28  you
: 1682 5 28  numb
- 1689
: 1691 2 21 A
: 1694 3 28  pill
: 1698 3 26  to
: 1702 3 28  make
: 1706 5 28  you
: 1714 5 28  dumb
- 1721
: 1723 2 21 A
: 1726 3 28  pill
: 1730 3 26  to
: 1734 3 28  make
: 1738 5 28  you
: 1751 3 28  a
: 1754 5 26 ny
: 1762 7 24 bo
: 1770 6 26 dy
: 1778 5 21  else
- 1784
: 1786 2 21 But
: 1790 3 28  all
: 1794 2 26  the
: 1798 3 28  drugs
: 1802 5 28  in
: 1810 5 28  this
: 1818 8 24  world
- 1828
: 1838 5 19 Won't
: 1846 3 28  save
: 1850 4 26  her
: 1858 4 24  from
: 1866 6 26  her
: 1875 11 21 self
- 1888
* 2167 9 12 You
: 2179 5 9  were
: 2190 2 9  from
: 2194 2 9  a
* 2198 3 14  per
* 2202 4 14 fect
: 2210 8 12  world
- 2220
: 2226 2 9 A
: 2230 8 14  world
: 2242 7 9  that
: 2263 3 14  threw
: 2267 3 14  me
: 2271 3 14  a
: 2274 6 12 way
: 2285 3 14  to
: 2289 6 14 da
: 2296 8 9 ~y
- 2306
: 2555 3 21 A
: 2559 3 28  pill
: 2563 3 26  to
: 2567 3 28  make
: 2571 6 28  you
: 2579 5 28  numb
- 2585
: 2587 3 21 A
: 2591 3 28  pill
: 2595 3 26  to
: 2599 3 28  make
: 2603 5 28  you
: 2611 5 28  dumb
- 2618
: 2620 2 21 A
: 2624 2 28  pill
: 2627 3 26  to
: 2631 3 28  make
: 2635 5 28  you
: 2648 3 28  a
: 2651 5 26 ny
: 2659 7 24 bo
: 2667 6 26 dy
: 2675 5 21  else
- 2681
: 2683 2 21 But
: 2687 2 28  all
: 2691 2 26  the
: 2695 3 28  drugs
: 2699 5 28  in
: 2707 5 28  this
: 2715 8 24  world
- 2725
: 2735 5 19 Won't
: 2742 4 28  save
: 2747 4 26  her
: 2755 4 24  from
: 2763 4 26  her
: 2772 11 21 self
- 2785
: 2811 3 21 A
: 2815 3 28  pill
: 2819 3 26  to
: 2823 3 28  make
: 2827 5 28  you
: 2835 5 28  numb
- 2841
: 2843 3 21 A
: 2847 3 28  pill
: 2851 3 26  to
: 2855 3 28  make
: 2859 6 28  you
: 2867 5 28  dumb
- 2874
: 2876 2 21 A
: 2880 2 28  pill
: 2883 3 26  to
: 2887 3 28  make
: 2891 5 28  you
: 2904 3 28  a
: 2907 5 26 ny
: 2915 7 24 bo
: 2923 6 26 dy
: 2931 6 21  else
- 2938
: 2940 2 21 But
: 2944 2 28  all
: 2948 2 26  the
: 2952 3 28  drugs
: 2956 4 28  in
: 2963 6 28  this
: 2971 9 24  world
- 2982
: 2991 5 19 Won't
: 2998 3 28  save
: 3003 3 26  her
: 3011 3 24  from
: 3019 5 26  her
: 3027 14 21 self
E
