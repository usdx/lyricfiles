#TITLE:Believe Me (Russland 2004)
#ARTIST:Yulia Savicheva
#LANGUAGE:English
#EDITION:Ultrastar Eurovision Edition Vol.3
#GENRE:Pop
#MP3:Yulia Savicheva - Believe Me (Russland 2004).mp3
#COVER:Yulia Savicheva - Believe Me (Russland 2004) [CO].jpg
#BACKGROUND:Yulia Savicheva - Believe Me (Russland 2004) [BG].jpg
#VIDEO:Yulia Savicheva - Believe Me (Russland 2004) [VD#0].mpg
#BPM:120,05
#GAP:400
: 0 2 12 Be
: 2 2 22 lieve 
: 4 1 20 me, 
: 6 1 12 I 
: 8 2 22 just 
: 12 2 20 don't 
: 16 10 12 care, 
- 28
: 32 1 12 If 
: 34 1 22 you 
: 36 1 20 look 
: 38 1 12 a
: 40 2 22 way 
: 44 2 20 or 
: 48 8 13 stare, 
- 58
: 64 1 13 If 
: 66 1 22 you 
: 68 1 20 choose 
: 70 1 13 to 
: 72 2 22 go 
: 76 1 20 or 
: 80 6 13 stay, 
- 88
: 100 1 13 Don't 
: 102 2 15 be
: 104 2 13 lie
: 106 1 12 ve 
: 108 2 12 me, 
: 112 2 11 I 
: 116 6 12 pray, 
- 124
: 292 1 19 In 
: 294 1 19 my 
: 296 2 20 head, 
: 300 1 15 e
: 302 1 15 very
: 304 2 17 day, 
: 308 2 15 e
: 310 2 12 very
: 312 4 13 whe
: 316 2 15 re, 
- 320
: 324 3 15 Oh, 
: 328 2 15 no
: 330 1 13 - 
: 331 2 10 - 
: 333 1 13 -, 
- 336
: 356 1 17 You 
: 358 1 17 and 
* 360 2 19 I
: 362 1 17 - 
: 364 2 17 ca
: 366 1 15 n 
: 368 2 15 ta
: 370 1 13 lk 
: 372 2 13 and 
- 378
: 378 2 13 I 
: 382 1 12 sound 
: 384 4 11 col
: 388 2 12 der, 
- 392
: 420 1 19 In 
: 422 1 19 my 
: 424 2 20 heart, 
- 427
: 428 1 15 Though 
: 430 1 15 I 
: 432 2 17 sa
: 434 1 15 y 
: 436 1 15 I 
: 438 1 12 don't 
: 440 4 13 ca
: 444 2 15 re, 
- 448
: 484 1 17 I 
: 486 1 17 just 
: 488 2 19 cry
: 490 1 17 - 
: 492 2 17 a
: 494 1 15 nd 
: 496 2 15 cry
: 498 1 13 - 
: 500 2 13 on 
- 506
: 506 3 19 Your 
: 510 1 19 big 
* 512 4 19 shoul
: 516 3 22 der, 
- 521
: 528 2 12 Be
: 530 2 22 lieve 
: 532 1 20 me, 
: 534 1 12 I 
: 536 2 22 just 
: 540 2 20 don't 
: 544 8 12 care, 
- 554
: 560 1 12 If 
: 562 1 22 you 
: 564 1 20 look 
: 566 1 12 a
: 568 2 22 way 
: 572 2 20 or 
: 576 6 13 stare, 
- 584
: 592 1 13 If 
: 594 1 22 you 
: 596 1 20 choose 
: 598 1 13 to 
: 600 2 22 go 
: 604 1 20 or 
: 608 6 13 stay, 
- 616
: 628 1 13 Don't 
: 630 2 15 be
: 632 2 13 lie
: 634 1 12 ve 
: 636 2 12 me, 
: 640 2 11 I 
: 644 4 12 pray, 
- 650
: 656 2 12 Be
: 658 2 22 lieve 
: 660 1 20 me, 
: 662 1 12 I 
* 664 2 22 just 
: 668 2 20 don't 
: 672 6 12 care, 
- 680
: 688 1 12 If 
: 690 1 22 you 
: 692 1 20 look 
: 694 1 12 a
: 696 2 22 way 
: 700 2 20 or 
: 704 4 13 sta
: 708 2 15 re, 
- 712
: 720 1 13 If 
: 722 1 22 you 
: 724 1 20 choose 
: 726 1 13 to 
: 728 2 22 go 
: 732 1 20 or 
: 736 6 13 stay, 
- 744
: 756 1 13 Don't 
: 758 2 15 be
: 760 2 13 lie
: 762 1 12 ve 
: 764 2 12 me, 
: 768 2 11 I 
: 772 4 12 pray, 
- 778
: 804 1 19 Far 
: 806 2 19 a
: 808 3 20 way, 
: 812 1 15 like 
: 814 1 15 the 
: 816 2 17 man 
: 820 1 15 in 
: 822 1 12 the 
: 824 4 13 mo
: 828 2 15 on, 
- 832
: 836 3 15 My 
* 840 2 15 de
: 842 1 13 - 
: 844 2 10 a
: 846 1 13 r, 
- 849
: 868 1 17 You 
: 870 1 17 just 
: 872 2 19 wa
: 874 1 17 ve 
: 876 2 17 t
: 878 1 15 o 
: 880 2 15 m
: 882 1 13 e, 
- 884
: 884 3 13 You're 
: 890 4 13 al
: 894 1 12 ways 
: 896 4 11 ly
: 900 2 12 ing, 
- 904
: 932 2 19 E
: 934 1 19 very 
: 936 2 20 face, 
- 939
: 940 1 15 E
: 942 1 15 very 
: 944 3 17 voice, 
: 948 2 15 e
: 950 1 12 very 
: 952 4 13 tu
: 956 2 15 ne, 
- 960
: 996 1 17 Brings 
: 998 1 17 you 
* 1000 2 19 back 
: 1004 2 17 t
: 1006 1 15 o 
: 1008 2 15 m
: 1010 1 13 e 
: 1012 2 13 and 
- 1018
: 1018 3 19 I 
: 1022 1 19 start 
: 1024 4 19 cry
: 1028 3 22 ing, 
- 1033
: 1040 2 12 Be
: 1042 2 22 lieve 
: 1044 1 20 me, 
: 1046 1 12 I 
: 1048 2 22 just 
: 1052 2 20 don't 
: 1056 8 12 care, 
- 1066
: 1072 1 12 If 
: 1074 1 22 you 
: 1076 1 20 look 
: 1078 1 12 a
: 1080 2 22 way 
: 1084 2 20 or 
: 1088 6 13 stare, 
- 1096
: 1104 1 13 If 
: 1106 1 22 you 
: 1108 1 20 choose 
: 1110 1 13 to 
: 1112 2 22 go 
: 1116 1 20 or 
: 1120 6 13 stay, 
- 1128
: 1140 1 13 Don't 
: 1142 2 15 be
: 1144 2 13 lie
: 1146 1 12 ve 
: 1148 2 12 me, 
: 1152 2 11 I 
: 1156 4 12 pray, 
- 1162
: 1168 2 12 Be
: 1170 2 22 lieve 
: 1172 1 20 me, 
: 1174 1 12 I 
* 1176 2 22 just 
: 1180 2 20 don't 
: 1184 6 12 care, 
- 1192
: 1200 1 12 If 
: 1202 1 22 you 
: 1204 1 20 look 
: 1206 1 12 a
: 1208 2 22 way 
: 1212 2 20 or 
: 1216 4 13 sta
: 1220 2 15 re, 
- 1224
: 1232 1 13 If 
: 1234 1 22 you 
: 1236 1 20 choose 
: 1238 1 13 to 
: 1240 2 22 go 
: 1244 1 20 or 
: 1248 6 13 stay, 
- 1256
: 1268 1 13 Don't 
: 1270 2 15 be
: 1272 2 13 lie
: 1274 1 12 ve 
: 1276 2 12 me, 
: 1280 2 11 I 
: 1284 4 12 pray, 
- 1290
: 1296 2 12 Be
: 1298 1 22 lieve 
: 1300 1 20 me, 
: 1302 1 12 I 
: 1304 3 22 just 
: 1308 2 20 don't 
: 1312 6 12 care
E
