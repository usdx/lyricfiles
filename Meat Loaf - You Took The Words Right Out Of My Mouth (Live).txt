#TITLE:You Took The Words Right Out Of My Mouth (Live)
#ARTIST:Meat Loaf
#MP3:Meat Loaf - You Took The Words Right Out Of My Mouth (Live).mp3
#COVER:Meat Loaf - You Took The Words Right Out Of My Mouth (Live) [CO].jpg
#BACKGROUND:Meat Loaf - You Took The Words Right Out Of My Mouth (Live) [BG].jpg
#BPM:252
#GAP:86500
#ENCODING:UTF8
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
#START:49
: 0 2 7 It
: 4 3 7  was
: 9 2 7  a
: 13 3 11  hot
: 21 2 11  sum
: 24 3 11 mer
: 29 4 11  night
: 34 3 9 ~,
- 39
: 44 1 9 the
: 45 4 9  beach
: 50 6 7 ~
: 58 2 7  was
: 62 4 7  burn
: 68 1 4 ing,
- 71
: 78 5 11 fog
: 86 3 14  crawl
: 91 2 14 ing
: 95 3 14  o
: 99 2 14 ver
: 103 2 14  the
: 107 9 16  sand.
- 118
: 136 3 7 When
: 140 2 7  I
: 144 2 11  lis
: 148 2 11 ten
: 152 2 11  to
: 156 3 11  your
: 161 3 11  heart,
- 165
: 165 2 9 I
: 168 2 9  hear
: 172 2 9  the
: 176 3 9  whole
: 181 6 7  world
: 189 6 9  turn
: 197 2 4 ing,
- 201
: 208 3 9  see
: 213 2 9  the
: 218 3 9  shoot
: 222 2 9 ing
: 226 5 9  stars,
- 233
: 238 1 9 fal
: 240 1 9 ling
: 242 6 12  through
: 250 6 11  your
: 259 4 7  trem
: 266 6 9 bling
* 274 16 7  hands.
- 292
: 326 6 7 You
: 334 2 7  were
: 338 3 11  lick
: 342 2 11 ing
: 346 3 11  your
: 351 4 11  lips
- 357
: 366 1 9 and
: 368 1 9  your
: 370 3 9  lip
: 375 4 7 stick's
: 383 5 7  shin
: 390 3 4 ing,
- 395
: 403 3 11  dy
: 407 3 14 ing
: 412 2 14  just
: 416 3 14  to
: 421 2 14  ask
: 425 2 14  for
: 429 2 14  a
: 433 7 16  taste.
- 442
: 457 2 16 Oh,
: 461 3 16  we
: 466 2 16  were
: 470 3 14  ly
: 474 3 14 ing
: 478 3 11  to
: 482 6 14 ge
: 489 2 11 ther
- 493
: 498 1 14 in
: 500 2 14  a
: 503 5 14  sil
: 511 2 14 ver
: 515 6 14  lin
: 523 2 7 ing.
- 526
: 527 3 4 By
: 531 2 7  the
: 535 6 9  light
: 543 1 9  of
: 545 1 9  the
: 548 4 9  moon
- 554
: 559 3 9 know
: 563 2 9  there's
: 567 3 12  not
: 571 2 12  a
: 575 3 11 no
: 579 2 11 ther
: 583 3 7  mo
: 587 3 9 ment,
- 592
* 596 6 12 not
: 603 2 12  a
: 607 3 11 no
: 611 2 11 ther
: 615 3 7  mo
: 620 3 9 ment,
- 625
: 628 5 16 not
: 635 3 16  a
: 639 3 14 no
: 643 2 14 ther
: 647 3 14  mo
: 651 3 14 ment
: 655 6 14  to
: 663 4 14  waste
: 668 8 11 ~.
- 678
: 713 3 7 Oh,
: 718 2 7  well
: 722 2 7  you
* 726 3 16  hold
: 730 2 16  me
: 734 5 16  so
: 742 3 14  close
: 746 3 11 ~,
- 751
: 758 2 12  knees
: 761 2 11 ~
: 765 4 12  grow
: 771 2 11  weak
: 774 2 7 ~.
- 778
: 788 7 9 Soul
: 797 2 9  is
: 801 6 11  fly
: 809 2 9 ing
: 813 2 7  high
: 816 2 4  a
: 821 5 7 bove
: 828 2 7  the
: 832 7 7  ground.
- 841
: 851 4 16 Try
: 856 2 16 ing
: 859 2 16  to
: 864 5 14  speak,
- 871
: 875 2 11  but
: 879 2 11  no
: 883 3 12  mat
: 887 2 12 ter
: 891 3 12  what
: 895 3 12  I
: 900 5 11  do,
- 907
: 914 3 11  just
: 922 4 11  can't
: 930 6 11  seem
: 941 2 11  to
: 946 3 11  make
: 950 2 11  a
: 953 2 11 ny
* 958 11 9  sound.
- 971
: 1033 3 7 Then
: 1037 2 7  you
: 1041 3 16  took
: 1045 3 16  the
: 1050 4 16  words
- 1055
: 1055 3 16 right
: 1059 3 16  out
: 1063 2 18  of
: 1067 2 19  my
: 1071 7 14  mouth.
- 1080
: 1095 6 14 Oh,
: 1103 3 14  it
: 1107 2 12  must
: 1111 4 12  have
: 1116 2 12  been
- 1119
: 1120 2 12 while
: 1123 3 12  you
: 1128 5 12  were
: 1137 4 14  kiss
: 1145 4 11 ing
: 1152 4 7  me.
- 1158
: 1168 2 7 You
: 1171 3 16  took
: 1175 2 16  the
: 1180 3 16  words
- 1184
: 1185 3 16 right
: 1189 3 16  out
: 1193 2 18  of
: 1197 2 19  my
: 1201 7 14  mouth.
- 1210
: 1224 6 11 Oh,
: 1232 2 11  I
: 1236 6 12  swear
: 1244 2 11  it's
: 1250 1 9  true
: 1252 4 7 ~,
- 1258
: 1264 1 7 I
: 1266 1 9  was
: 1268 2 11  just
: 1272 3 9  a
: 1276 3 11 bout
: 1280 2 9  to
: 1284 3 11  say
: 1289 3 9 ~,
- 1294
: 1304 3 11 I
: 1308 2 11  love
: 1311 3 7 ~
: 1317 2 7  you
: 1320 5 9 ~.
- 1327
: 1355 2 7 Then
: 1359 2 7  you
: 1363 2 16  took
: 1367 2 16  the
: 1371 3 16  words
- 1375
: 1375 3 16 right
: 1379 2 16  out
: 1383 2 18  of
: 1387 2 19  my
: 1391 8 14  mouth.
- 1401
: 1415 6 14 Oh,
: 1423 2 14  it
: 1427 2 12  must
: 1431 2 12 've
: 1434 2 12  been
- 1437
: 1439 3 12 while
: 1443 2 12  you
: 1446 5 12  were
* 1454 4 14  kiss
: 1461 3 11 ing
: 1465 2 9 ~
: 1469 5 7  me.
- 1476
: 1484 2 7 You
: 1488 3 16  took
: 1492 2 16  the
: 1496 3 16  words
- 1500
: 1500 3 16  right
: 1504 3 16  out
: 1508 2 18  of
: 1512 2 19  my
* 1516 6 19  mouth
: 1523 3 14 ~.
- 1528
: 1539 5 11 Oh,
: 1546 2 11  I
: 1550 6 12  swear
: 1558 3 11  it's
: 1563 2 9  true
: 1566 3 7 ~,
- 1571
: 1578 1 7 I
: 1580 1 9  was
: 1582 2 11  just
: 1586 2 9  a
: 1590 3 11 bout
: 1594 2 9  to
: 1598 4 11  say
: 1603 3 9 ~,
- 1608
: 1618 3 11 I
: 1622 2 11  love
: 1625 3 7 ~
: 1630 10 9  you.
- 1642
: 1796 3 7 Now
: 1800 2 7  my
: 1804 7 11  bo
: 1812 1 11 dy
: 1814 2 11  is
: 1817 5 11  shak
: 1824 2 7 ing
- 1827
: 1828 3 9 like
: 1833 2 9  a
: 1837 4 9  wave
: 1842 5 7 ~
: 1848 2 7  on
: 1851 2 7  the
: 1854 4 9  wa
: 1859 2 4 ter.
- 1863
: 1869 2 11 Guess
: 1873 3 14  that
: 1878 2 14  I'm
: 1881 3 14  be
: 1886 3 14 gin
: 1890 2 14 ning
: 1894 2 14  to
: 1898 3 16  grin
: 1902 3 14 ~.
- 1907
: 1922 6 16 Oh
: 1930 2 14  we're
: 1934 6 14  fi
: 1942 2 14 nally
: 1945 2 14  a
: 1948 11 16 lone.
- 1960
: 1962 1 9 We
: 1964 2 9  can
: 1967 3 9  do
: 1971 3 7 ~
: 1975 1 7  what
: 1977 1 7  we
: 1979 8 9  want.
- 1989
: 1998 5 9 Night
: 2004 6 9  is
: 2012 6 9  young.
- 2020
: 2028 5 12 No
: 2035 2 12  one's
: 2039 3 11  gon
: 2043 2 11 na
: 2047 3 7  know
: 2051 3 9  where
: 2055 2 7  you,
- 2058
* 2058 7 12 no
: 2067 2 12  one's
: 2071 3 11  gon
: 2075 2 11 na
: 2079 3 7  know
: 2083 3 9  where
: 2087 2 7  you,
- 2090
: 2090 6 16 no
: 2098 2 16  one's
: 2102 3 14  gon
: 2106 2 14 na
: 2110 3 14  know
: 2114 3 14  where
: 2118 5 14  you've
* 2126 11 14  been.
- 2139
: 2176 3 7 Oh,
: 2180 3 7  you're
: 2184 3 7  now
: 2188 2 11  lick
: 2191 3 11 ing
: 2196 3 11  your
: 2200 4 11  lips
- 2206
: 2209 5 9 and
: 2216 2 9  your
: 2219 2 9  lip
: 2223 5 7 stick's
: 2231 7 7  shi
: 2239 3 4 ning.
- 2244
: 2247 2 7 Was
: 2251 3 11  dy
: 2255 3 11 ing
: 2259 2 14  just
: 2262 2 14  to
: 2266 3 14  ask
: 2270 3 14  for
: 2275 2 14  a
: 2278 8 16  taste.
- 2288
: 2302 5 16 Oh,
: 2308 1 14  when
: 2310 2 14  were
: 2313 3 14  ly
: 2317 3 14 ing
: 2321 4 14  to
: 2326 5 14 ge
: 2332 2 11 ther
- 2336
: 2340 1 14 in
: 2342 1 14  a
: 2345 5 14  sil
: 2352 2 14 ver
: 2356 6 14  lin
: 2364 2 11 ing,
- 2367
: 2368 3 7 by
: 2372 2 7  the
: 2376 3 7  light
: 2380 3 9  of
: 2384 3 9  the
: 2389 5 9  moon,
- 2396
: 2400 3 9 know
: 2404 2 9  there's
: 2407 3 12  not
: 2411 3 12  an
: 2415 3 11 oth
: 2419 2 11 er
: 2423 3 7  mo
: 2427 3 9 ment,
- 2432
: 2435 6 12 not
: 2442 3 12  an
: 2446 3 11 oth
: 2450 2 11 er
: 2454 3 7  mo
: 2458 3 9 ment,
- 2463
* 2465 7 16 not
: 2473 3 16  an
: 2477 3 14 oth
: 2481 2 14 er
: 2485 3 14  mo
: 2489 2 14 ment
: 2493 6 14  to
: 2501 9 14  waste.
- 2512
: 2551 2 7 And
: 2555 2 7  then
: 2558 2 7  you
: 2562 2 16  took
: 2566 3 16  the
: 2570 3 16  words
- 2574
: 2576 3 16 right
: 2580 2 16  out
: 2583 2 18  of
: 2587 2 19  my
: 2591 7 14  mouth.
- 2600
: 2616 6 14 Oh,
: 2623 2 14  it
: 2628 2 12  must
: 2632 3 12 've
: 2636 2 12  been
: 2640 3 12  while
: 2644 3 12  you
: 2649 3 12  were
: 2659 3 14  kiss
: 2664 6 11 ing
: 2672 8 7  me.
- 2682
: 2687 2 7 You
: 2691 3 16  took
: 2695 3 16  the
: 2699 4 16  words
- 2704
: 2704 3 16 right
: 2708 3 16  out
: 2712 2 18  of
: 2716 2 19  my
: 2720 8 14  mouth.
- 2730
: 2745 6 11 Oh,
: 2752 1 11  I
* 2756 6 12  swear
: 2764 2 11  it's
: 2769 2 9  true
: 2772 4 7 ~,
- 2778
: 2784 1 7 I
: 2785 1 9  was
: 2787 2 11  just
: 2792 2 9  a
: 2795 2 11 bout
: 2800 1 9  to
: 2804 3 11  say
: 2809 3 9 ~,
- 2814
: 2823 5 16 I
: 2832 2 9  love
: 2834 2 7 ~
: 2838 2 7  you.
- 2842
: 2872 2 7 And
: 2875 1 7  then
: 2878 2 7  you
: 2882 2 16  took
: 2886 2 16  the
: 2890 3 16  words
- 2894
: 2895 3 16 right
: 2899 2 16  out
: 2903 2 18  of
: 2906 2 19  my
: 2911 6 14  mouth.
- 2919
: 2935 5 14 Oh,
: 2942 2 14  it
: 2945 3 12  must
: 2950 2 12 've
: 2953 3 12  been
: 2957 3 12  while
- 2961
: 2962 2 12 you
: 2966 4 12  were
: 2973 4 14  kiss
: 2980 3 11 ing
: 2984 2 9 ~
: 2988 5 7  me.
- 2995
: 3003 2 7 You
: 3007 3 16  took
: 3011 3 16  the
: 3015 3 16  words
- 3019
: 3020 2 16 right
: 3023 3 16  out
: 3027 2 18  of
: 3031 2 19  my
: 3035 5 19  mouth
: 3041 2 14 ~.
- 3045
: 3058 5 11 Oh,
: 3065 2 11  I
: 3069 6 12  swear
: 3077 3 11  it's
: 3083 2 9  true
: 3086 3 7 ~,
- 3091
: 3096 1 7 I
: 3098 1 9  was
: 3100 2 11  just
: 3104 3 9  a
: 3108 3 11 bout
: 3112 2 9  to
: 3116 4 11  say
: 3121 3 9 ~,
- 3126
: 3144 3 11  I
: 3148 2 11  love
: 3151 4 7 ~
: 3157 2 7  you
: 3160 3 9 ~.
- 3165
: 3186 1 9 And
: 3188 2 7  then
: 3191 2 7  you
: 3195 2 7  took
: 3199 2 7  the
: 3203 3 7  words
- 3207
: 3209 2 4 right
: 3212 3 7  out
: 3216 2 7  of
: 3219 2 7  my
: 3224 5 7  mouth.
- 3231
: 3250 4 7 Oh,
: 3256 2 7  you
: 3259 2 7  took
: 3264 2 7  the
: 3268 3 7  words
- 3272
: 3273 2 4 right
: 3276 2 7  out
: 3279 2 7  of
: 3283 2 7  my
: 3288 4 7  mouth.
- 3294
* 3313 5 16 Oh,
: 3320 3 14  you
: 3324 2 14  took
: 3328 2 14  the
: 3332 4 14  words
- 3337
: 3338 2 11 right
: 3341 3 14  out
: 3345 2 14  of
: 3349 2 14  my
: 3354 5 14  mouth.
- 3361
: 3377 6 16 Oh,
: 3385 2 14  you
: 3389 2 14  took
: 3392 2 14  the
: 3397 4 14  words
- 3402
: 3402 2 11 right
: 3405 3 14  out
: 3409 2 14  of
: 3413 2 14  my
: 3418 5 14  mouth.
- 3425
: 3442 6 16 Oh,
: 3450 2 14  you
: 3453 2 14  took
: 3457 2 14  the
: 3461 4 14  words.
- 3467
: 3481 1 14 Oh,
: 3483 1 14  you
: 3485 2 14  took
: 3489 2 14  the
* 3493 6 16  words.
- 3501
: 3513 1 16 Oh,
: 3515 1 14  you
: 3517 5 14  took.
- 3524
: 3530 5 19  Oh,
: 3546 5 21  oh,
: 3563 5 23  oh
: 3570 5 21 ~
: 3577 5 19 ~,
: 3585 5 23  yeah,
: 3594 4 21  yeah,
: 3601 4 19  yeah!
- 3607
: 3619 3 19 Oh,
: 3626 4 17  yeah!
- 3632
: 4344 1 2 Then
: 4345 2 2  you
: 4348 3 7  took
: 4353 2 7  the
: 4357 3 7  words
- 4361
: 4362 2 4 right
: 4365 3 7  out
: 4369 2 7  of
: 4373 2 7  my
: 4377 6 7  mouth.
- 4385
: 4401 6 16 Oh,
: 4410 2 14  you
: 4413 2 14  took
: 4417 2 14  the
: 4421 3 14  words
- 4425
: 4427 2 11 right
: 4430 3 14  out
: 4434 2 14  of
: 4438 5 14  my...
- 4445
* 5492 6 23 Oh,
: 5500 2 16  you
: 5504 2 19  took
: 5508 2 19  the
: 5512 3 19  words
- 5516
: 5517 2 16 right
: 5520 2 19  out
: 5523 2 19  of
: 5527 2 19  my
: 5532 6 19  mouth.
- 5540
: 5555 5 23 Oh,
: 5562 2 19  you
: 5566 2 19  took
: 5569 2 19  the
: 5574 3 19  words
- 5578
: 5579 2 16  right
: 5582 3 19  out
: 5586 2 19  of
: 5590 2 19  my
: 5594 5 19  mouth.
- 5601
: 5679 5 19 Oh,
: 5686 2 19  you
: 5690 2 19  took
: 5693 2 19  the
: 5698 3 19  words
- 5702
: 5702 2 16 right
: 5705 3 19  out
: 5709 2 19  of
: 5713 2 19  my
: 5718 4 19  mouth.
- 5724
: 5740 6 16 Oh,
: 5748 2 16  you
: 5752 2 16  took
: 5756 2 16  the
: 5760 3 16  words
- 5764
: 5764 3 16 right
: 5768 3 16  out
: 5772 2 18  of
: 5776 2 19  my
: 5780 6 14  mouth.
- 5788
* 5803 6 14 Oh,
: 5811 2 14  it
: 5815 2 12  must
: 5819 2 12 've
: 5823 3 12  been
: 5828 3 12  while
: 5832 3 12  you
: 5836 5 12  were
: 5843 5 14  ki
: 5851 3 11 ssing
: 5855 2 9 ~
: 5859 4 7  me.
- 5865
: 5874 2 7 You
: 5878 2 16  took
: 5882 2 16  the
: 5886 3 16  words
- 5890
: 5891 3 16 right
: 5895 3 16  out
: 5899 2 18  of
: 5903 2 19  my
: 5907 5 14  mouth.
- 5914
: 5931 6 14 Oh,
: 5938 2 14  it
: 5942 2 12  must
: 5946 3 12 've
: 5950 3 12  been
: 5955 3 12  while
: 5959 2 12  you
: 5962 5 12  were
* 5970 5 14  ki
: 5978 3 11 ssing
: 5982 2 9 ~
: 5986 3 7  me.
- 5991
: 6002 2 7 You
: 6006 2 16  took
: 6010 2 16  the
: 6014 3 16  words
- 6018
: 6018 3 16  right
: 6022 3 16  out
: 6026 2 18  of
: 6030 3 19  my
: 6034 6 14  mouth.
- 6042
: 6057 8 26 Oh,
: 6066 2 26  it
: 6071 2 24  must
: 6075 3 24 've
: 6079 3 24  been
: 6083 3 24  while
: 6088 2 24  you
: 6091 5 24  were
: 6099 5 26  ki
: 6107 2 23 ssing
: 6110 3 21 ~
: 6115 3 19  me.
- 6120
: 6131 2 7 You
: 6135 2 16  took
: 6139 2 16  the
: 6143 3 16  words
- 6147
: 6148 3 16 right
: 6152 3 16  out
: 6156 2 18  of
: 6160 2 19  my
: 6164 5 14  mouth.
- 6171
: 6188 6 14 Oh,
: 6195 2 14  it
: 6199 2 12  must
: 6203 3 12 've
: 6207 3 12  been
: 6212 3 12  while
: 6216 3 12  you
: 6220 4 12  were
: 6227 5 14  ki
: 6235 3 11 ssing
: 6239 2 9 ~
: 6243 3 7  me.
- 6248
: 6259 2 7 You
: 6263 2 16  took
: 6267 2 16  the
: 6271 3 16  words
- 6275
: 6276 2 16  right
: 6279 3 16  out
: 6283 2 18  of
: 6287 3 19  my
: 6291 6 14  mouth.
- 6299
: 6316 6 26 Oh,
: 6323 2 26  it
: 6328 2 24  must
: 6332 3 24 've
: 6336 3 24  been
: 6340 3 24  while
: 6344 3 24  you
: 6348 5 24  were
: 6356 4 26  ki
: 6363 2 23 ssing
: 6366 3 21 ~
: 6371 3 19  me.
- 6376
: 6386 2 7 You
: 6390 2 16  took
: 6394 2 16  the
: 6398 3 16  words
- 6402
: 6403 3 16 right
: 6407 3 16  out
: 6411 2 18  of
: 6415 2 19  my
: 6419 5 14  mouth.
- 6426
: 6443 6 14 Oh,
: 6450 2 14  it
: 6454 2 12  must
: 6458 2 12 've
: 6461 3 12  been
: 6465 3 12  while
: 6469 2 12  you
: 6473 4 12  were
: 6481 5 14  ki
: 6489 3 11 ssing
: 6493 2 9 ~
: 6497 3 7  me.
- 6502
: 6512 2 7 You
: 6516 2 16  took
: 6520 2 16  the
: 6524 3 16  words
- 6528
: 6528 3 16  right
: 6532 3 16  out
: 6536 2 18  of
: 6540 3 19  my
: 6544 6 14  mouth.
- 6552
: 6567 6 26 Oh,
: 6575 2 26  it
: 6580 2 24  must
: 6583 3 24 've
: 6587 3 24  been
: 6591 3 24  while
: 6595 2 24  you
: 6599 4 24  were
: 6606 5 26  ki
: 6614 2 23 ssing
: 6617 3 21 ~
: 6622 3 19  me.
- 6627
: 6638 2 7 You
: 6642 2 16  took
: 6646 2 16  the
: 6650 3 16  words
- 6654
: 6655 3 16 right
: 6659 3 16  out
: 6663 2 18  of
: 6667 2 19  my
: 6671 5 14  mouth.
- 6678
: 6695 6 14 Oh,
: 6702 2 14  it
: 6706 2 12  must
: 6710 3 12 've
: 6714 3 12  been
: 6718 3 12  while
: 6722 3 12  you
: 6726 4 12  were
: 6733 5 14  ki
: 6741 3 11 ssing
: 6745 2 9 ~
: 6749 3 7  me.
- 6754
: 6765 2 7 You
: 6769 2 16  took
: 6773 2 16  the
: 6777 3 16  words
- 6781
: 6781 3 16  right
: 6785 3 16  out
: 6789 2 18  of
: 6793 3 19  my
: 6797 6 14  mouth.
- 6805
: 6820 6 26 Oh,
: 6828 2 26  it
: 6833 2 24  must
: 6836 3 24 've
: 6840 3 24  been
: 6844 3 24  while
: 6848 3 24  you
: 6852 4 24  were
: 6860 4 26  ki
: 6867 2 23 ssing
: 6870 3 21 ~
: 6875 3 19  me.
- 6880
: 6891 2 7 You
: 6895 2 16  took
: 6899 2 16  the
: 6903 3 16  words
- 6907
: 6908 3 16 right
: 6912 3 16  out
: 6916 2 18  of
: 6919 2 19  my
: 6924 5 14  mouth.
- 6931
: 6947 6 14 Oh,
: 6954 2 14  it
: 6958 2 12  must
: 6962 2 12 've
: 6965 3 12  been
: 6969 3 12  while
: 6973 2 12  you
: 6977 4 12  were
: 6985 5 14  ki
: 6993 3 11 ssing
: 6997 2 9 ~
: 7001 3 7  me.
- 7006
: 7015 2 7 You
: 7019 2 16  took
: 7023 2 16  the
: 7027 3 16  words
- 7031
: 7031 3 16  right
: 7035 3 16  out
: 7039 2 18  of
: 7043 3 19  my
: 7047 6 14  mouth.
- 7055
: 7070 6 26 Oh,
: 7078 2 26  it
: 7083 2 24  must
: 7086 3 24 've
: 7090 3 24  been
: 7094 3 24  while
: 7098 2 24  you
: 7102 4 24  were
: 7109 5 26  ki
: 7117 2 23 ssing
: 7120 3 21 ~
: 7125 3 19  me.
- 7130
: 7140 2 7 You
: 7144 2 16  took
: 7148 2 16  the
: 7152 3 16  words
- 7156
: 7157 3 16 right
: 7161 3 16  out
: 7165 2 18  of
: 7168 2 19  my
: 7172 5 14  mouth.
- 7179
* 7195 6 14 Oh,
: 7202 2 14  it
: 7206 2 12  must
: 7210 3 12 've
: 7214 3 12  been
: 7218 3 12  while
: 7222 3 12  you
: 7226 4 12  were
: 7233 5 14  ki
: 7241 3 11 ssing
: 7245 2 9 ~
: 7249 3 7  me.
- 7254
: 7264 2 7 You
: 7268 2 16  took
: 7272 2 16  the
: 7276 3 16  words
- 7280
: 7280 3 16  right
: 7284 3 16  out
: 7288 2 18  of
: 7292 3 19  my
: 7296 6 14  mouth.
- 7304
: 7318 6 23 Oh,
: 7326 2 21  it
: 7331 2 21  must
: 7334 3 19 've
: 7338 3 19  been
: 7342 3 19  while
: 7346 3 19  you
: 7350 4 19  were
* 7358 4 19  ki
: 7365 2 19 ssing
: 7368 3 19 ~
: 7373 3 19  me.
- 7378
: 7388 2 7 You
: 7392 2 16  took
: 7396 2 16  the
: 7400 3 16  words
- 7404
: 7404 3 16  right
: 7408 3 16  out
: 7412 2 18  of
: 7416 3 19  my
: 7420 6 14  mouth.
- 7428
: 7442 7 23 Oh,
: 7450 3 21  it
: 7454 2 21  must
: 7458 3 19 've
: 7462 3 19  been
: 7466 3 19  while
: 7470 3 19  you
: 7474 4 19  were
* 7482 4 23  ki
* 7488 5 23 ssing
* 7496 4 23  me.
E