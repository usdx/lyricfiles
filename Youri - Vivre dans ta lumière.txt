#TITLE:Vivre dans ta lumičre
#ARTIST:Youri
#MP3:Youri   Vivre dans ta lumičre.mp3
#COVER:Youri   Vivre dans ta lumičre.jpg
#VIDEO:Youri - Vivre dans ta lumičre.mp4
#BPM:205.96
#GAP:18090
: 0 5 5 Vivre
: 7 5 5  dans
: 13 5 3  ta
: 21 5 2  lu
: 28 5 4 mičre
- 46
F 62 1 0 Princesse
F 64 1 0  tant
F 67 1 0  de
F 70 1 0  choses
F 72 1 0  nous
F 75 1 0  se
F 77 1 0 parent
- 80
F 82 1 0 Ma
F 85 1 0  vie
F 87 2 0  c'est
F 90 1 0  le
F 92 1 0  ha
F 95 1 0 sard
- 97
F 99 1 0 J'ai
F 102 1 0  ta
F 104 1 0 toué
F 107 1 0  sur
F 109 1 0  mes
F 112 1 0  bras
- 113
F 114 1 0 Ta
F 117 1 0 gué
F 119 1 0  dans
F 122 1 0  ma
F 124 1 0  me
F 127 1 0 moire
- 128
F 129 1 0 Le
F 135 1 0  des
F 137 1 0 sin
F 140 1 0  de
F 142 1 0  nous
F 145 1 0  deux
- 146
F 147 1 0 Le
F 150 1 0  duo
F 152 1 0  de
F 154 1 0  tes
F 157 1 0  yeux
- 171
F 187 1 0 Posé
F 190 1 0  sur
F 192 1 0  ta
F 195 1 0  peau
F 198 1 0  noire
- 202
F 204 1 0 Fra
F 206 1 0 gile
F 209 1 0  comme
F 211 1 0  du
F 214 1 0  bu
F 216 1 0 vard
- 217
F 219 1 0 Mouillé
F 222 1 0  par
F 224 1 0  tes
F 227 1 0  larmes
- 231
F 233 1 0 Qui
F 236 1 0  tom
F 238 1 0 bent
F 241 1 0  ssur
F 243 1 0  mon
F 245 1 0  cur
- 249
F 251 1 0 Comme
F 253 1 0  une
F 257 1 0  rose
F 260 1 0  sacrée
- 271
F 283 1 0 Comme
F 286 1 0  une
F 290 1 0  rose
F 295 1 0  sacrée
- 306
: 320 5 -4 Vivre
- 338
: 361 8 -4 clandestin
: 370 3 -4  moi
: 374 3 -5  je
: 378 3 -6  veux
: 382 8 -7  vivre
- 403
: 425 2 -4 Dans
: 428 2 -4  ton
: 431 2 -3  re
: 434 3 -3 gard
: 438 3 -3  sous
: 442 3 -3  ta
* 446 4 -1  lu
* 451 4 0 miere
- 468
: 488 2 -4 Redeve
: 491 2 -1 nir
: 494 3 -1  pour
: 501 3 -2  toi
: 507 3 -5  cet
: 513 3 -5  ange
: 521 3 -7  fra
: 527 3 0 gile
- 533
: 535 7 0 J'ai
: 543 7 0  peur
: 551 7 0  mais
: 559 6 0  je
: 566 7 4  veux
: 575 7 5  vivre
- 595
: 622 8 -4 Clandestin
: 631 3 -4  moi
: 635 3 -5  je
: 639 3 -6  veux
: 643 8 -7  vivre
- 664
: 680 2 -4 Dans
: 683 2 -4  ton
: 686 2 -3  re
: 689 3 -3 gard
: 693 3 -3  sous
: 697 3 -3  ta
: 701 4 -1  lu
: 706 4 0 miere
- 723
: 743 2 -4 Redeve
: 746 2 -1 nir
: 749 3 -1  pour
: 756 3 -2  toi
: 762 3 -5  cet
: 768 3 -5  ange
: 776 3 -7  fra
: 782 3 0 gile
- 788
: 790 7 0 J'ai
: 798 7 0  peur
: 806 7 0  mais
: 814 6 0  je
: 821 7 4  veux
: 830 3 5  vivre
- 833
F 834 1 0 Ange
F 837 1 0  noir
F 840 1 0  je
F 842 1 0  le
F 844 1 0  sais
- 847
F 849 1 0 Tu
F 851 1 0  es
F 853 1 0  mon
F 856 1 0  ange
F 859 1 0  noir
- 860
F 862 1 0 L'his
F 864 1 0 toire
F 867 1 0  de
F 869 1 0  mes
F 872 1 0  silences
- 873
F 875 1 0 La
F 877 1 0  couleur
F 880 1 0  de
F 882 1 0  l'ab
F 884 1 0 sence
- 888
F 890 1 0 Le
F 893 1 0  fris
F 897 1 0 son
F 900 1 0  in
F 902 1 0 ter
F 905 1 0 dit
F 908 1 0  des
F 910 1 0  sou
F 912 1 0 pir
F 915 1 0  e
F 918 1 0 ro
F 919 1 0 ti
F 922 1 0 ques
- 923
F 924 1 0 J'veux
F 927 1 0  vivre
F 929 1 0  dans
F 932 1 0  ta
F 935 1 0  lu
F 938 1 0 miere
- 943
F 945 1 0 Dans
F 947 1 0  ta
F 950 1 0  lu
F 952 1 0 miere
- 953
F 955 1 0 Il
F 958 1 0  faut
F 960 1 0  croire
F 962 1 0  a
F 965 1 0  de
F 968 1 0 main
- 969
F 970 1 0 tou
F 973 1 0 jours
F 975 1 0  main
F 978 1 0  dans
F 980 1 0  la
F 983 1 0  main
- 984
F 985 1 0 Sur
F 988 1 0  la
F 991 1 0  route
F 993 1 0  de
F 996 1 0  mes
F 998 1 0  doutes
- 1001
F 1003 1 0 Je
F 1006 1 0  cher
F 1008 1 0 che
F 1010 1 0  ta
F 1013 1 0  lu
F 1016 1 0 miere
- 1017
F 1018 1 0 Je
F 1021 1 0  cher
F 1023 1 0 che
F 1026 1 0  ta
F 1028 1 0  lu
F 1031 1 0 miere
- 1042
F 1057 1 0 Ta
F 1060 1 0  lu
F 1063 1 0 miere
- 1074
: 1089 5 -4 Vivre
- 1107
: 1130 8 -4 clandestin
: 1139 3 -4  moi
: 1143 3 -5  je
: 1147 3 -6  veux
: 1151 8 -7  vivre
- 1172
: 1194 2 -4 Dans
: 1197 2 -4  ton
: 1200 2 -3  re
: 1203 3 -3 gard
: 1207 3 -3  sous
: 1211 3 -3  ta
: 1215 4 -1  lu
: 1220 4 0 miere
- 1237
: 1257 2 -4 Redeve
: 1260 2 -1 nir
: 1263 3 -1  pour
: 1270 3 -2  toi
: 1276 3 -5  cet
: 1282 3 -5  ange
: 1290 3 -7  fra
: 1296 3 0 gile
- 1302
: 1304 7 0 J'ai
: 1312 7 0  peur
: 1320 7 0  mais
: 1328 6 0  je
: 1335 7 4  veux
: 1344 7 5  vivre
- 1364
* 1389 8 -4 Clandestin
: 1398 3 -4  moi
: 1402 3 -5  je
: 1406 3 -6  veux
: 1410 8 -7  vivre
- 1431
: 1449 2 -4 Dans
: 1452 2 -4  ton
: 1455 2 -3  re
: 1458 3 -3 gard
: 1462 3 -3  sous
: 1466 3 -3  ta
: 1470 4 -1  lu
: 1475 4 0 miere
- 1492
: 1512 2 -4 Redeve
: 1515 2 -1 nir
: 1518 3 -1  pour
: 1525 3 -2  toi
: 1531 3 -5  cet
: 1537 3 -5  ange
: 1545 3 -7  fra
: 1551 3 0 gile
- 1557
: 1559 7 0 J'ai
: 1567 7 0  peur
: 1575 7 0  mais
: 1583 6 0  je
: 1590 7 4  veux
: 1599 3 5  vivre
- 1602
F 1603 1 0 Clandestin
F 1606 1 0  rebelle
- 1607
F 1609 1 0 Mon
F 1613 1 0  dieu
F 1616 1 0  que
F 1619 1 0  tu
F 1621 1 0  es
F 1624 1 0  belle
- 1628
F 1630 1 0 Comme
F 1633 1 0  une
F 1636 1 0  a
F 1638 1 0 quarelle,
F 1649 1 0  Ba
F 1653 1 0 by
- 1660
F 1662 1 0 Des
F 1665 1 0 sine
F 1668 1 0  mes
F 1671 1 0  reves
- 1682
F 1691 1 0 Des
F 1694 1 0 sine
F 1696 1 0  mes
F 1699 1 0  reves
- 1713
F 1730 1 0 Viens
F 1736 1 0  viens
F 1746 1 0  vivre
F 1755 1 0  dans
F 1758 1 0  ma
F 1760 1 0  lu
F 1762 1 0 miere
- 1763
F 1765 1 0 Viens
F 1771 1 0  viens
F 1779 1 0  vivre
F 1782 1 0  dans
F 1785 1 0  ma
F 1787 1 0  lu
F 1790 1 0 miere
- 1804
F 1825 1 0 Vivre
F 1828 1 0  dans
F 1830 1 0  ma
F 1832 1 0  lu
F 1834 1 0 mie
F 1836 1 0 re
- 1847
* 1857 5 -4 Vivre
- 1875
: 1898 8 -4 clandestin
: 1907 3 -4  moi
: 1911 3 -5  je
: 1915 3 -6  veux
: 1919 8 -7  vivre
- 1940
: 1962 2 -4 Dans
: 1965 2 -4  ton
: 1968 2 -3  re
: 1971 3 -3 gard
: 1975 3 -3  sous
: 1979 3 -3  ta
: 1983 4 -1  lu
: 1988 4 0 miere
- 2005
: 2025 2 -4 Redeve
: 2028 2 -1 nir
: 2031 3 -1  pour
: 2038 3 -2  toi
: 2044 3 -5  cet
: 2050 3 -5  ange
: 2058 3 -4  fra
: 2064 3 3 gile
- 2070
: 2072 7 0 J'ai
: 2080 7 0  peur
: 2088 7 0  mais
: 2096 6 0  je
: 2103 7 4  veux
: 2112 7 5  vivre
- 2132
: 2154 8 -4 Clandestin
: 2163 3 -4  moi
: 2167 3 -5  je
: 2171 3 -6  veux
: 2175 8 -7  vivre
- 2196
: 2217 2 -4 Dans
: 2220 2 -4  ton
: 2223 2 -3  re
: 2226 3 -3 gard
: 2230 3 -3  sous
: 2234 3 -3  ta
: 2238 4 -1  lu
: 2243 4 0 miere
- 2260
: 2280 2 -4 Redeve
: 2283 2 -1 nir
: 2286 3 -1  pour
: 2293 3 -2  toi
: 2299 3 -5  cet
: 2305 3 -5  ange
: 2313 3 -7  fra
: 2319 3 0 gile
- 2325
: 2327 7 0 J'ai
: 2335 7 0  peur
: 2343 7 0  mais
: 2351 6 0  je
: 2358 7 4  veux
: 2367 3 5  vivre
- 2383
: 2409 8 -4 Clandestin
: 2418 3 -4  moi
: 2422 3 -5  je
: 2426 3 -6  veux
: 2430 8 -7  vivre
- 2451
: 2473 2 -4 Dans
: 2476 2 -4  ton
: 2479 2 -3  re
* 2482 3 -3 gard
* 2486 3 -3  sous
* 2490 3 -3  ta
* 2494 4 -1  lu
* 2499 4 0 miere
- 2516
: 2536 2 -4 Redeve
: 2539 2 -1 nir
: 2542 3 -1  pour
: 2549 3 -2  toi
F 2555 3 -5  cet
F 2561 3 -5  ange
F 2569 3 -4  fra
F 2575 3 3 gile
- 2581
F 2583 7 0 J'ai
F 2591 7 0  peur
F 2599 7 0  mais
F 2607 6 0  je
F 2614 7 4  veux
F 2623 3 5  vivre
- 2632
: 2635 1 0 
E
