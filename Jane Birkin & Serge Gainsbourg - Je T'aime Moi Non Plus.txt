#TITLE:Je T'aime Moi Non Plus
#ARTIST:Jane Birkin & Serge Gainsbourg
#LANGUAGE:Français
#EDITION:[SC]-Songs
#MP3:Jane Birkin & Serge Gainsbourg - Je T'aime Moi Non Plus.mp3
#COVER:Jane Birkin & Serge Gainsbourg - Je T'aime Moi Non Plus [CO].jpg
#BACKGROUND:Jane Birkin & Serge Gainsbourg - Je T'aime Moi Non Plus [BG].jpg
#BPM:167,28
#GAP:11170
F 0 4 0 Je
F 4 4 0  t'aime
- 10
F 20 4 0 Je
F 24 4 0  t'aime
- 30
F 38 4 0 Oh
F 42 2 0  oui,
F 44 2 0  je
F 46 4 0  t'aime
- 52
F 70 4 0 Moi
F 74 2 0  non
F 76 4 0  plus
- 82
F 126 4 0 Oh
F 130 4 0  mon
F 134 2 0  a
F 136 4 0 mour
- 142
F 166 2 0 Comme
F 168 2 0  la
F 170 4 0  va
F 174 4 0 gue
F 196 4 0  ir
F 200 4 0 ré
F 204 2 0 so
F 206 8 0 lue
- 216
: 250 6 0 Je
: 257 5 2  vais,
- 264
: 269 3 5 je
: 273 3 4  vais
: 276 2 2  et
: 278 10 0  je
: 290 7 0  viens
- 299
: 339 2 0 En
: 341 2 -1 tre
: 343 9 -3  tes
: 354 7 -3  reins
- 363
: 394 8 -5 Je
: 403 3 4  vais
: 406 2 2  et
: 408 10 0  je
: 420 5 0  viens
- 427
: 435 3 0 En
: 438 2 -1 tre
: 441 9 -3  tes
: 452 6 -3  reins
- 460
: 470 2 -5 Et
: 472 5 -3  je
: 486 2 -3  me
* 489 10 0  re
: 501 5 2 tiens
- 508
F 518 4 0 Je
F 522 8 0  t'aime
- 532
F 536 4 0 Je
F 540 6 0  t'aime
- 548
F 554 4 0 Oh
F 558 2 0  oui,
F 560 2 0  je
F 562 6 0  t'aime
- 570
F 588 2 0 Moi
F 590 2 0  non
F 592 4 0  plus
- 598
F 648 4 0 Oh
F 652 4 0  mon
F 656 1 0  a
F 657 7 0 mour
- 666
F 680 4 0 Tu
F 684 2 0  es
F 686 2 0  la
F 688 6 0  vague
- 696
F 712 10 0 Moi
F 726 2 0  l'î
F 728 2 0 le
F 732 4 0  nue
- 738
: 770 6 12 Tu
: 778 6 14  vas,
: 790 3 17  tu
: 794 2 16  vas
: 796 2 14  et
: 799 8 12  tu
: 809 7 12  viens
- 818
: 859 2 12 En
: 861 2 11 tre
* 864 8 9  mes
: 874 6 9  reins
- 882
: 914 6 7 Tu
: 921 3 16  vas
: 924 2 14  et
: 927 8 12  tu
: 938 4 12  viens
- 944
: 954 2 12 En
: 957 2 11 tre
: 960 9 9  mes
: 970 6 9  reins
- 978
: 987 3 7 Et
* 991 11 9  je
: 1004 2 9  te
: 1007 4 12  re
: 1019 7 14 joins
- 1028
F 1036 4 0 Je
F 1040 8 0  t'aime
- 1050
F 1058 4 0 Je
F 1062 8 0  t'aime
- 1072
F 1080 2 0 Oh
F 1082 2 0  oui,
F 1084 2 0  je
F 1086 7 0  t'aime
- 1095
F 1106 2 0 Moi
F 1108 2 0  non
F 1110 8 0  plus
- 1120
F 1168 6 0 Oh
F 1174 2 0  mon
F 1176 2 0  a
F 1178 8 0 mour
- 1188
F 1207 3 0 Com
F 1210 2 0 me
F 1212 2 0  la
F 1214 6 0  vague
F 1234 6 0  ir
F 1240 4 0 ré
F 1244 2 0 so
F 1246 10 0 lue
- 1258
: 1289 4 0 Je
: 1294 5 2  vais,
: 1309 3 5  je
: 1313 3 4  vais
: 1316 2 2  et
: 1318 10 0  je
: 1330 8 0  viens
- 1340
: 1378 3 0 En
: 1381 2 -1 tre
: 1383 8 -3  tes
: 1394 5 -3  reins
- 1401
: 1435 7 -5 Je
: 1443 2 4  vais
: 1445 3 2  et
: 1448 8 0  je
: 1459 7 0  viens
- 1468
: 1475 2 0 En
: 1477 2 -1 tre
* 1479 8 -3  tes
: 1491 6 -3  reins
- 1499
: 1507 3 -5 Et
: 1510 4 -3  je
: 1524 2 -3  me
: 1527 8 0  re
: 1539 9 2 tiens
- 1550
: 1805 6 12 Tu
: 1813 6 14  vas,
: 1825 3 17  tu
: 1829 3 16  vas
: 1832 2 14  et
: 1835 7 12  tu
: 1845 6 12  viens
- 1853
: 1894 2 12 En
: 1896 2 11 tre
: 1899 8 9  mes
: 1909 7 9  reins
- 1918
: 1950 4 7 Tu
: 1957 2 16  vas
: 1960 2 14  et
* 1962 8 12  tu
: 1972 6 12  viens
- 1980
: 1988 2 12 En
: 1991 2 11 tre
: 1994 9 9  mes
: 2004 4 9  reins
- 2010
: 2021 2 7 Et
: 2024 7 9  je
: 2035 2 9  te
: 2038 4 12  re
: 2053 7 14 joins
- 2062
F 2068 4 0 Je
F 2072 8 0  t'aime
- 2082
F 2090 4 0 Je
F 2094 8 0  t'aime
- 2104
F 2106 2 0 Oh
F 2108 3 0  oui,
F 2111 3 0  je
F 2114 5 0  t'aime
- 2121
F 2138 1 0 Moi
F 2139 2 0  non
F 2141 4 0  plus
- 2147
F 2202 3 0 Oh
F 2205 3 0  mon
F 2208 3 0  a
F 2211 7 0 mour
- 2220
F 2232 3 0 l´a
F 2235 3 0 mour
F 2238 3 0  phy
F 2241 6 0 sique
F 2263 5 0  est
F 2268 3 0  sans
F 2271 2 0  is
F 2273 4 0 sue
- 2279
: 2316 3 0 Je
: 2320 5 2  vais,
: 2336 2 5  je
: 2339 3 4  vais
: 2342 2 2  et
: 2344 9 0  je
: 2356 8 0  viens
- 2366
: 2403 2 0 En
: 2405 2 -1 tre
: 2408 7 -3  tes
: 2418 5 -3  reins
- 2425
: 2459 6 -5 Je
: 2466 3 4  vais
: 2469 2 2  et
: 2471 9 0  je
: 2482 7 0  viens
- 2491
: 2498 2 0 Je
: 2501 2 -1  me
: 2503 4 -3  re
: 2514 5 -3 tiens
- 2521
F 2531 6 12 Non!
F 2548 4 14  Mainte
F 2552 5 17 nant
F 2564 8 16  viens....
E
