#TITLE:Proudest Monkey
#ARTIST:Dave Matthews Band
#MP3:Dave Matthews Band - Proudest Monkey.mp3
#VIDEO:Dave Matthews Band - Proudest Monkey.mkv
#COVER:Dave Matthews Band - Proudest Monkey [CO].jpg
#BACKGROUND:Dave Matthews Band - Proudest Monkey [BG].jpg
#BPM:240
#GAP:22851
#VIDEOGAP:0
#MedleyStartBeat:2246
#MedleyEndBeat:2913
#ENCODING:UTF8
#PREVIEWSTART:163,226
#LANGUAGE:English
#GENRE:ez
#CREATOR:skizzo tinyurl.com/skizzosBay
: 0 72 12 Swi
: 72 5 10 ng
: 79 6 10  in
: 86 8 10  this
: 95 15 8  tree
- 112
: 155 8 12 Mmmmh
: 164 3 12  O
: 167 3 10 h
: 171 3 12  i
: 174 3 10 ~
: 178 3 12  a
: 181 4 10 m
: 186 8 12  bounce
: 195 8 10  around
: 204 4 8  s
: 208 4 10 o
: 213 3 8  we
: 216 12 10 ll
- 229
: 229 3 12 Bra
: 232 4 10 nch
: 237 3 10  to
: 241 8 10  branch,
- 251
: 257 2 8 Li
: 259 5 10 mb
: 265 3 10  to
: 269 4 10  limb
: 274 4 8  you
: 279 15 8  see
- 296
: 343 4 15 A
: 347 4 13 ll
: 352 6 13  in
: 359 5 13  a
: 365 12 15  day's
: 378 4 12  dre
: 382 4 10 ~
: 386 24 8 am
- 412
: 414 4 12 I
: 419 4 10  am
: 424 10 12  stuck
- 436
: 453 7 10 Like
: 461 4 12  the
: 466 3 12  oth
: 469 2 10 er
: 472 7 10  mon
: 479 10 12 keys
: 490 30 10  here
- 522
: 540 4 12 I
: 544 3 10 ~
: 548 4 12  a
: 552 3 10 m
: 556 4 12  a
: 560 5 10 ~
: 566 4 12  h
: 570 4 10 um
: 574 4 12 bl
: 578 4 10 e
: 583 7 12  mon
: 590 32 8 key
- 624
: 638 6 -2 Sit
: 644 4 10 ting
: 649 3 10  up
: 653 3 10  in
: 657 2 10  her
: 659 10 8 e
: 670 20 8  again
- 692
: 727 4 8 But
: 732 6 15  then
: 739 11 13  came
: 751 3 15  the
: 755 54 12  day
- 810
: 812 3 8 I
: 815 4 12 ~
: 820 5 12  climb
: 825 5 10 ed
: 831 2 10  ou
: 833 4 8 t
: 838 5 8  of
: 844 7 10  these
: 852 7 8  safe
: 860 20 8  limbs
- 882
: 916 4 12 Ve
: 920 4 10 n
: 924 4 12 tur
: 928 4 10 ed
: 933 3 10  a
: 936 5 12 w
: 941 2 10 a
: 943 13 8 y
- 958
: 980 4 12 Wal
: 984 5 10 king
: 990 5 12  ta
: 995 2 10 l
: 997 5 8 l,
: 1003 6 10  head
: 1010 8 10  high
: 1019 6 10  up
: 1026 4 12  a
: 1030 5 10 nd
: 1036 6 10  sing
: 1042 10 8 ing
- 1054
: 1098 3 8 I
: 1102 6 12  went
: 1109 5 12  t
: 1114 4 10 o
: 1119 3 12  the
: 1123 5 12  ci
: 1128 16 8 ty
- 1146
: 1188 4 12 Ca
: 1192 4 10 r
: 1197 10 10  horns,
: 1208 11 10  corners
: 1220 3 10  and
: 1224 2 10  the
: 1227 3 10  grit
: 1230 24 8 ty
- 1256
: 1294 4 12 No
: 1298 4 10 w
: 1303 3 12  i
: 1306 4 10 ~
: 1311 5 12  am
: 1317 4 10  the
: 1322 4 12  pro
: 1326 4 10 u
: 1330 6 12 dest
: 1337 7 12  mon
: 1344 51 8 key
- 1396
: 1396 5 10 you've
: 1402 4 10  ev
: 1406 3 8 er
: 1410 28 8  seen
- 1440
: 1469 4 20 Mon
: 1473 3 8 key
: 1477 3 8  see,
: 1481 4 20  mon
: 1485 4 8 key
: 1490 6 8  do
F 1497 8 10 Ye
F 1505 9 12 ah
- 1516
* 2246 7 15 Then
* 2254 6 13  comes
* 2261 3 15  the
* 2265 13 12  d
* 2278 4 10 a
: 2282 24 8 y
- 2307
* 2307 6 11 Star
* 2313 4 10 ing
* 2318 5 10  at
* 2324 15 10  myself
* 2340 2 8  i
* 2342 3 10 ~
* 2346 8 10  tu
* 2354 4 8 rn
* 2359 4 10  to
* 2364 4 10  quest
* 2368 7 8 ion
* 2376 20 8  me
- 2398
: 2422 4 8 I
: 2427 3 12  wo
: 2430 4 10 n
: 2434 4 12 de
: 2438 4 10 r
: 2443 3 12  d
: 2446 3 10 o
: 2450 3 12  i
: 2453 3 10 ~
: 2457 6 12  want
: 2464 4 10  the
: 2469 2 9  s
: 2471 8 10 imple,
: 2480 2 9  s
: 2482 11 10 imple
: 2494 2 9  li
: 2496 24 10 fe
- 2522
: 2532 4 10 Mmmh
: 2537 4 10 that
: 2542 3 10  i
: 2546 5 10  once
: 2552 1 10  li
: 2553 7 8 ved
: 2561 4 8  in
: 2566 20 8  well
- 2588
: 2624 2 15 O
: 2626 3 13 h
: 2630 7 13  things
: 2638 6 15  were
: 2645 5 12  qui
: 2650 6 10 et
: 2657 28 10  then
- 2687
: 2702 4 8 In
: 2707 2 8  a
: 2710 4 15  wa
: 2714 3 13 y
: 2718 6 13  they
: 2725 7 13  were
: 2733 4 13  th
: 2737 3 8 e
: 2741 4 11  bet
: 2745 4 10 ter
: 2750 26 8  days
- 2778
: 2798 5 8 But
: 2804 3 12  no
: 2807 4 10 w
: 2812 4 12  i
: 2816 3 10 ~
: 2820 4 12  a
: 2824 3 10 m
: 2828 6 12  the
: 2835 4 10  pr
: 2839 3 12 o
: 2842 5 10 u
: 2847 5 12 dest
: 2853 9 12  mon
: 2862 51 8 key
- 2914
: 2915 3 10 you've
: 2919 3 10  ev
: 2922 2 8 er
: 2925 37 8  seen
F 2963 20 10  Ahhhhh
- 2984
: 2986 4 20 Mon
: 2990 2 8 key
: 2993 4 8  see,
: 2998 4 20  mon
: 3002 4 8 key
: 3007 4 8  do
F 3012 9 10 Ye
F 3021 9 12 ah
- 3032
: 3604 38 12 Monkey see, Monkey do
- 3644
: 3651 38 12 Monkey see, Monkey do
- 3691
: 3698 32 12 Monkey see, Monkey do
- 3732
: 3746 45 12 Monkey see, Monkey do
- 3792
: 3792 40 12 Monkey see, Monkey do
- 3834
: 3840 36 12 Monkey see, Monkey do
- 3878
: 3888 33 12 Monkey see, Monkey do
- 3923
: 3934 34 12 Monkey see, Monkey do
- 3970
: 3983 33 12 Monkey see, Monkey do
- 4018
: 4029 46 12 Monkey see, Monkey do, do ,do ,do ,do
- 4076
: 4076 33 12 Monkey see, Monkey do
- 4111
: 4118 44 12 Monkey see, Monkey do oh yeah
- 4164
: 4168 47 12 Monkey see, Monkey do, do ,do ,do ,do
- 4216
: 4216 41 12 Monkey see, Monkey do, do ,do ,do ,do
- 4259
: 4263 46 12 Monkey see, Monkey do, do ,do ,do ,do ,do ,do
- 4310
: 4310 36 12 Monkey see, Monkey do
E