#TITLE:Warum
#ARTIST:Juli
#MP3:Juli - Warum.mp3
#COVER:Juli - Warum [CO].jpg
#BACKGROUND:Juli - Warum [BG].jpg
#BPM:204
#GAP:28920
#ENCODING:UTF8
#EDITION:[SC]-Songs
#CREATOR:Zaddo
: 0 2 16 Du
: 3 2 18  stellst
: 6 2 18  mir
: 8 3 18  tau
: 12 3 16 send
: 16 5 18  Fra
: 22 2 16 gen,
- 25
: 26 4 18 stellst
: 30 3 16  dich
: 34 2 18  mit
: 37 4 18 ten
: 42 2 18  in
: 45 3 16  den
: 49 5 19  Wind
- 56
: 59 3 16 und
: 63 3 16  ich
: 67 4 23  hoff
: 72 4 24  du
: 77 9 19  checkst
- 87
: 89 2 19 dass
: 91 2 19  sie
: 94 4 21  nicht
: 99 4 23  wich
: 104 5 21 tig
: 110 7 23  sind
- 119
: 122 4 16 Komm
: 127 2 16  wir
: 130 3 18  set
: 134 2 18 zen
: 137 3 18  jetzt
: 141 3 16  die
: 145 5 18  Se
: 151 3 16 gel,
- 154
: 154 3 18 neh
: 158 3 16 men
: 162 2 18  al
: 164 3 16 les
: 168 3 18  mit
: 172 3 16  was
: 176 4 19  geht
- 182
: 186 4 16 Nicht
: 191 2 16  mehr
: 194 5 23  um
: 200 5 24 zu
: 206 4 19 dreh´n,
- 212
: 217 1 16 auch
: 219 3 18  wenn
: 223 3 16  der
: 227 6 21  Wind
: 234 4 19  sich
: 239 6 18  dreht
- 247
: 250 3 18 Hey,
: 254 4 16  ich
: 259 3 18  hör
: 263 2 18  dich
: 265 3 18  lei
: 269 2 16 se
: 273 3 18  la
: 277 2 16 chen
- 280
: 282 3 18 und
: 286 3 16  dann
: 290 3 18  merk
: 294 2 18  ich
: 297 3 18  wie´s
: 301 3 16  mich
: 305 5 19  trifft
- 312
: 315 3 16 Ja,
: 319 3 16  ich
: 323 3 18  lie
: 326 2 18 be
: 329 3 18  die
: 333 3 16 se
: 337 3 18  Ta
: 342 2 16 ge,
- 346
: 348 2 18 die
: 351 3 18  man
: 355 2 18  mor
: 357 2 18 gens
: 361 3 18  schon
: 365 3 19  ver
: 369 4 18 gisst
- 375
: 380 2 16 Und
: 383 2 16  ich
: 386 3 18  schau
: 390 2 18  dir
: 393 3 18  in
: 397 3 16  die
: 401 5 18  Au
: 406 3 16 gen,
- 410
: 412 2 18 bin
: 414 4 16  ge
: 418 4 18 blen
: 422 2 18 det
: 425 3 18  von
: 429 2 16  dem
: 432 6 19  Licht,
- 440
: 443 2 16 was
: 446 4 16  jetzt
: 451 5 23  um
: 457 4 24  sich
: 462 7 19  greift,
- 471
: 474 3 16 auch
: 478 4 16  wenn
: 483 4 21  du
: 488 5 19  nicht
: 494 8 18  sprichst
- 504
: 515 3 18 Al
: 518 3 16 les
: 522 4 18  an
: 527 19 16  dir
: 566 4 16  blei
: 570 6 18 ~bt
: 577 5 16  stumm
- 584
: 587 6 19 Wa
: 593 6 18 rum,
: 602 6 19  wa
: 608 7 18 rum,
: 624 2 16  wa
: 626 2 18 rum
: 629 2 18  ist
: 633 2 18  doch
: 636 4 16  e
: 640 7 16 gal,
- 649
: 651 4 19 denn
: 656 9 18  heu
: 666 3 19 te
: 670 7 21  Nacht
: 678 3 18  sind
: 682 4 19  nur
: 687 5 23  wir
: 693 4 19  zwei
: 698 5 18  wich
: 703 6 19 tig
- 711
: 715 6 19 Wa
: 721 7 18 rum,
: 730 6 19  wa
: 736 7 18 rum,
: 752 2 16  wa
: 754 2 18 rum
: 757 3 18  ist
: 761 4 18  doch
: 765 3 16  e
: 769 6 16 gal
- 777
: 785 1 16 Wa
: 786 3 18 rum
: 789 2 18  ist
: 793 4 18  jetzt
: 798 3 16  e
: 801 11 16 gal
- 814
: 833 2 16 Wir
: 835 3 18  schau´n
: 839 2 18  ü
: 841 3 18 ber
: 845 3 16  die
: 849 3 18  Dä
: 853 2 16 cher,
- 856
: 856 1 16 ich
: 858 4 18  schreib
: 863 3 16  dein
: 867 3 18  Na
: 871 1 18 men
: 873 3 18  in
: 877 3 19  die
: 881 6 19  Nacht
- 889
: 891 2 16 Hey,
: 894 3 16  wir
: 898 3 18  brau
: 902 2 18 chen
: 905 2 18  nicht
: 908 3 16  mal
: 912 3 18  Wor
: 917 3 16 te,
- 921
: 923 3 18 denn
: 926 2 16  es
: 928 4 18  reicht
: 933 3 16  schon
: 937 3 18  wenn
: 941 2 19  du
: 944 5 18  lachst
- 951
: 954 4 16 Aus
: 958 2 16  Se
: 961 4 18 kun
: 966 2 18 den
: 969 3 18  wer
: 973 2 16 den
: 976 6 18  Stun
: 982 4 16 den
- 987
: 987 3 18 und
: 991 2 16  ich
: 994 2 18  weiß
: 997 2 18  es
: 1000 3 18  klingt
: 1004 3 16  ver
: 1008 5 19 rückt,
- 1015
: 1018 3 18 doch
: 1022 3 16  wenn's
: 1026 6 23  ganz
: 1033 3 24  hart
: 1037 8 19  kommt,
- 1046
: 1046 4 16  dreh´n
: 1050 4 16  wir
: 1055 3 16  die
: 1059 5 21  Zeit
: 1065 5 19  zu
: 1071 3 18 rück
- 1076
: 1088 2 16 Und
: 1090 3 18  al
: 1093 4 16 les
: 1098 4 18  an
: 1103 18 16  dir
: 1142 4 16  blei
: 1146 6 18 ~bt
: 1153 9 16  stumm
- 1164
: 1227 6 19 Wa
: 1233 6 18 rum,
: 1243 6 19  wa
: 1249 6 18 rum,
: 1264 2 16  wa
: 1266 3 18 rum
: 1270 2 18  ist
: 1273 3 18  doch
: 1277 4 16  e
: 1281 5 16 gal,
- 1288
: 1290 5 19 denn
: 1296 9 18  heu
: 1306 3 19 te
: 1310 6 21  Nacht
: 1319 3 18  sind
: 1323 3 19  nur
: 1327 6 23  wir
: 1334 3 19  zwei
: 1338 4 18  wich
: 1342 3 19 tig
- 1347
: 1355 6 19 Wa
: 1361 6 18 rum,
: 1370 6 19  wa
: 1376 6 18 rum,
: 1392 2 16  wa
: 1394 2 18 rum
: 1397 3 18  ist
: 1401 3 18  doch
: 1404 3 16  e
: 1408 10 16 gal
- 1420
: 1425 2 16 Wa
: 1427 2 18 rum
: 1430 2 18  ist
: 1432 4 18  jetzt
: 1437 3 16  e
: 1440 9 18 gal
- 1451
: 1457 2 16 Wa
: 1459 2 18 rum
: 1462 2 18  ist
: 1465 3 18  doch
: 1469 4 16  e
: 1473 25 19 ga
: 1498 4 19 ~
: 1502 8 19 ~
: 1510 9 18 ~l
- 1521
: 1794 20 18 Bleib
: 1815 6 19  bei
: 1823 6 18  mir
- 1831
: 1838 4 18 Du
: 1843 7 23  siehst
: 1851 3 21  zu
: 1855 4 19  mir
- 1859
: 1859 18 18 Bleib
: 1879 6 19  noch
: 1887 5 19  hier
- 1894
: 1922 18 18 Bleib
: 1942 7 19  bei
: 1951 4 18  mir
- 1957
: 1966 4 18 Du
: 1971 7 23  siehst
: 1978 4 21  zu
: 1983 4 19  mir
- 1988
: 1988 17 18 Bleib
: 2007 7 21  bei
: 2015 4 21  mi
: 2019 28 19 r
- 2049
: 2059 6 19 Wa
: 2065 6 18 rum,
: 2075 6 19  wa
: 2081 6 18 rum,
: 2097 2 16  wa
: 2099 2 18 rum
: 2102 3 18  ist
: 2105 2 18  doch
: 2108 3 16  e
: 2112 5 16 gal,
- 2119
: 2122 6 19 denn
: 2129 8 18  heu
: 2138 4 19 te
: 2143 7 21  Nacht
: 2150 3 18  sind
: 2154 3 19  nur
: 2158 6 23  wir
: 2165 4 19  zwei
: 2170 4 18  wich
: 2174 5 19 tig
- 2181
: 2187 6 19 Wa
: 2193 6 18 rum,
: 2202 6 19  wa
: 2208 6 18 rum,
: 2225 2 16  wa
: 2227 2 18 rum
: 2230 3 18  ist
: 2233 2 18  doch
: 2236 4 16  e
: 2240 10 16 gal
- 2252
: 2257 2 18 Wa
: 2259 2 19 rum
: 2262 2 19  ist
: 2265 3 19  jetzt
: 2269 4 16  e
: 2274 20 16 ga
: 2294 8 19 ~
: 2302 5 16 ~l
- 2309
: 2315 6 19 Wa
: 2321 6 18 rum,
: 2330 6 19  wa
: 2336 6 18 rum,
: 2352 1 19  wa
: 2354 3 21 rum
: 2358 2 21  ist
: 2361 4 21  doch
: 2365 3 19  e
: 2369 5 18 gal,
- 2376
: 2380 4 19 denn
: 2384 9 18  heu
: 2394 3 19 te
: 2399 6 21  Nacht
: 2406 3 18  sind
: 2410 4 19  nur
: 2415 5 23  wir
: 2421 4 19  zwei
: 2426 4 18  wich
: 2430 6 19 tig
- 2438
: 2443 6 19 Wa
: 2449 6 18 rum,
: 2458 6 19  wa
: 2464 6 18 rum,
: 2481 2 16  wa
: 2483 2 18 rum
: 2486 2 18  ist
: 2489 2 18  doch
: 2492 3 16  e
: 2496 10 16 gal
- 2508
: 2513 2 18 Wa
: 2515 2 19 rum
: 2518 2 19  ist
: 2520 4 19  jetzt
: 2525 3 16  e
: 2528 9 18 gal
- 2539
: 2545 2 18 Wa
: 2547 2 19 rum
: 2550 2 19  ist
: 2553 3 19  doch
: 2557 3 16  e
: 2561 26 19 ga
: 2587 5 19 ~
: 2592 7 19 ~
: 2599 18 18 ~l
E