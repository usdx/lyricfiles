#TITLE:Change Despair
#ARTIST:Lagwagon
#MP3:Lagwagon - Change Despair.mp3
#COVER:Lagwagon - Change Despair [CO].jpg
#BACKGROUND:Lagwagon - Change Despair [BG].jpg
#BPM:107,45
#GAP:10000
#ENCODING:UTF8
: 0 6 65  Ev
: 6 2 65 ery
: 8 3 64 thing
: 11 2 60  that
: 13 3 57  I've
: 16 6 62  done
- 23
: 25 3 62  Here
: 28 2 60  in
: 30 3 58 to
: 33 6 60  one
- 40
: 41 3 60  In
: 44 3 58  kee
: 47 2 57 ping
: 49 5 53  with
- 55
: 65 8 65  Some
: 73 3 64  earned
: 76 2 60  con
: 78 3 57 vic
: 81 6 62 tion
- 87
: 88 4 62  Some
: 92 2 60  stag
: 94 3 65 na
: 97 6 60 tion
- 104
: 105 4 60  Some
: 109 3 58  change
: 112 2 57  des
: 114 4 53 pair
- 119
: 129 4 60  Dis
: 133 4 60 ap
: 137 3 60 point
: 140 2 60 ment
: 142 3 62  and
: 145 6 60  odds
- 152
: 153 3 60  in
: 156 3 58  a
: 159 3 57  cos
: 162 3 53 tume
: 166 4 48  you
: 170 4 58  can't
: 174 4 57  take
: 178 6 55  off
- 185
: 186 3 60  Leave
: 189 2 58  it
: 191 3 57  be
: 194 4 53 hind
- 198
: 199 2 53  I
: 201 3 60  know
: 204 2 58  that
: 206 3 57  this
: 209 4 55  time
- 214
: 215 2 53  I
: 217 3 57  get
: 220 2 55  in
: 222 3 52  for
: 225 5 53  free
- 231
: 256 3 65  Our
: 259 2 65  la
: 261 3 65 test
: 264 3 64  ren
: 267 1 65 di
: 269 3 69 tion
: 272 4 62  lame
- 277
: 280 2 60  Re
: 282 2 60 in
: 284 3 58 car
: 287 5 60 nate
- 293
: 295 3 60  Re
: 298 2 58 turn
: 300 3 57  the
: 303 5 53  same
- 309
: 319 3 65  Still
: 322 2 65  the
: 324 3 65  best
: 327 3 64  times
: 330 2 60  are
: 332 3 57  here
: 335 5 62  now
- 341
: 342 3 62  Now
: 345 2 60  that
: 347 3 58  I'm
: 350 6 60  bored,
: 358 3 60  now
: 361 2 58  that
: 363 3 57  I'm
: 366 5 53  numb
- 372
: 381 6 60  Done,
: 387 2 60  I
: 389 3 60  know
: 392 2 62  this
: 394 3 64  is
: 397 6 60  wrong
- 403
: 404 4 60  Safe
: 408 2 58  in
: 410 3 57  that
: 413 4 53  stance
: 417 4 48  but
: 421 4 58  in
: 425 4 57 com
: 429 5 55 plete
- 434
: 435 2 55  I
: 437 3 60  leave
: 440 3 58  it
: 443 2 57  be
: 445 5 53 hind
- 450
: 451 2 53  I
: 453 3 60  know
: 456 2 58  that
: 458 3 57  this
: 461 4 55  time
: 467 2 53  I
: 469 3 57  get
: 472 2 55  in
: 474 2 52  for
: 476 5 53  free
- 482
: 543 1 69  Some
: 544 1 67  of
: 545 2 65  it's
: 547 2 60  ha
: 549 2 67 bi
: 551 5 67 tual
- 557
: 591 2 69  Some
: 593 1 67  of
: 594 2 65  it
: 596 2 60  pre
: 598 2 67 dic
: 600 1 67 ta
: 601 4 65 ble
- 606
: 607 2 62  Some
: 609 2 62 times
: 611 2 62  the
: 613 3 62  change
: 616 2 60  is
: 618 2 62  not
: 620 2 57  e
: 622 4 55 nough
- 627
: 655 3 53  And
: 658 2 57  in
: 660 2 62  the
: 662 3 57  lost
: 666 1 60  em
: 667 2 62 pa
: 669 4 64 thy
- 673
: 674 2 65  me
: 676 2 65 mo
: 678 4 65 ries,
: 683 1 62  bet
: 684 2 64 ter
: 686 9 65  days
E