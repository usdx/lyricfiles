#TITLE:Aozora
#ARTIST:Air - Lia
#MP3:Lia - Aozora.mp3
#VIDEO:Lia - Aozora.avi
#COVER:Lia - Aozora [CO].jpg
#BPM:260.4
#GAP:520
#ENCODING:UTF8
#LANGUAGE:Japanese
#GENRE:Anime
#YEAR:2004
#CREATOR:Subkei
: 0 7 8 A
: 8 7 10 no
: 16 7 10  u
: 24 24 12 mi
- 54
: 56 7 12 Do
: 64 7 13 ko 
: 72 7 12 ma
: 80 7 12 de 
: 88 24 10 mo
- 118
: 120 7 10 A
: 128 7 12 o
: 136 7 10 ka
: 144 6 10 ~
: 152 24 8 tta
- 182
: 184 7 8 To
: 192 7 10 o
: 200 7 8 ku 
: 208 7 10 ma
: 216 24 12 de
- 252
: 256 7 8 A
: 264 7 10 no
: 272 7 10  mi
: 280 24 12 chi
- 310
: 312 7 12 Do
: 320 7 13 ko 
: 328 7 12 ma
: 336 7 12 de 
: 344 24 10 mo
- 373
: 376 7 10 Tsu
: 384 7 12 zu
: 392 7 10 i
: 400 7 10 te
: 408 24 8 ta
- 438
: 440 7 3 Ma
: 448 6 5 ~
: 456 7 3 ssu
: 464 7 3 gu
: 472 24 8 ni
- 508
: 512 7 8 I
: 520 7 12 chi
: 528 15 15 ban
: 544 7 12  ha
: 552 7 8 ya
: 560 12 5 ku
- 574
: 576 15 8 Su
: 592 15 10 na
: 608 7 12 o 
: 616 24 8 ni
- 646
: 648 7 8 Wa
: 656 15 10 ra
: 672 6 12 ~
: 680 24 8 tta
- 710
: 712 7 8 mo
: 720 15 10 no
: 736 6 12 ga
: 744 13 8 chi
- 758
: 760 7 5 I
: 768 7 8 chi
: 776 7 12 ba
: 784 14 15 n
: 800 7 12  su
: 808 7 8 ki 
: 816 12 5 na
- 833
: 836 12 8 A
: 849 14 10 no 
* 864 7 12 hi
* 872 32 8 to
- 909
: 912 23 8 Wa
: 937 21 8 ra
: 961 14 6 tte
: 977 59 8 ru
- 1050
* 1090 3 8 Da
* 1094 3 10 re
* 1098 3 12  yo
* 1102 3 13 ri 
* 1106 23 15 mo
* 1130 15 13  to
* 1146 7 12 o
* 1154 7 10 ku
* 1162 24 8  ni
* 1186 6 8  i
* 1194 4 10 tte
* 1198 8 10 mo
- 1208
: 1210 7 10 Ko
: 1218 7 8 ko
: 1226 7 12  ka
: 1234 23 8 ra
: 1258 3 8  ma
: 1262 8 8 ta
- 1272
: 1274 7 8 Wa
: 1282 7 8 ra
: 1290 5 10 ~
: 1297 24 10 tte
* 1322 3 12  ku
* 1326 3 10 re
* 1330 8 8 ru?
- 1344
: 1346 7 8 Hi
: 1354 7 15 to
: 1362 23 15 mi
: 1386 15 13  wo
: 1402 7 12  to
: 1410 7 10 ji
: 1418 3 8 re
: 1422 12 8 ba
- 1440
: 1442 12 15 Fu
: 1458 7 10 tto
: 1466 7 10  na
: 1474 7 8 tsu
: 1482 7 12  no
: 1490 36 8  hi
- 1528
: 1530 7 10 No
* 1538 7 8  ni
* 1546 7 7 o
: 1554 48 8 i
- 1621
: 1667 7 8 A
: 1675 7 10 no
: 1683 7 10  ka
: 1691 24 12 wa
- 1720
: 1723 7 12 A
: 1731 7 13 so
: 1739 7 12 n
: 1747 7 12 de
: 1755 24 10 ru
- 1785
: 1787 7 10 Fu
: 1795 7 12 ta
: 1803 7 10 ri
: 1811 7 10  ki
: 1819 24 8 ri
- 1849
: 1851 7 8 Do
: 1859 7 10 ro
: 1867 7 8  da
: 1875 7 10 ra
: 1883 24 12 ke
- 1919
: 1923 7 8 A
: 1931 7 10 no
* 1939 7 10  ku
* 1947 24 12 mo
- 1977
: 1979 7 12 O
: 1987 5 13 ~
: 1995 7 12 tte
: 2003 7 12 i
: 2011 24 10 ru
- 2041
: 2043 7 10 To
: 2051 7 12 do
: 2059 7 10 i
: 2067 7 10 ta
: 2075 24 8 ra
- 2105
* 2107 7 3 Shi
* 2115 7 5 a
* 2123 7 3 wa
* 2131 7 3 se
: 2139 24 8  to
- 2176
: 2180 7 8 I
: 2188 7 12 chi
: 2196 15 15 ban
: 2212 7 12  ha
: 2220 7 8 ya
: 2228 12 5 ku
- 2242
: 2244 15 8 Ko
: 2260 15 10 no
: 2276 7 12  sa
: 2284 24 8 ka
- 2314
: 2316 7 8 No
: 2324 15 10 bo
: 2340 5 12 ~
: 2348 24 8 tta
- 2378
: 2380 7 8 Mo
: 2388 15 10 no
: 2404 7 12 ga
: 2412 8 8 chi
- 2426
: 2428 7 5 I
: 2436 7 8 chi
: 2444 7 12 ba
: 2452 14 15 n
: 2468 7 12  su
: 2476 7 8 ki
: 2484 12 5  na
- 2498
: 2500 15 8 A
: 2516 15 10 no
: 2532 7 12  ba
: 2540 32 8 sho
- 2578
: 2580 23 8 Me
: 2604 23 8 za
: 2628 15 6 shi
: 2644 64 8 te
- 2725
* 2756 3 8 Ta
* 2760 3 10 ku
* 2764 3 10 sa
* 2768 3 12 n
* 2772 22 15  no
* 2796 15 13  o
* 2812 7 12 mo
* 2820 7 10 i
* 2828 3 8 de
* 2832 27 8  ga 
* 2860 3 10 a
* 2864 8 10 ru
- 2874
: 2876 7 10 Ho
: 2884 7 8 ka
: 2892 7 12  ni 
: 2900 18 8 wa
- 2922
: 2924 3 8 Na
: 2928 11 8 ni
: 2940 7 8  mo
: 2948 7 8  i
: 2956 7 10 ra
: 2964 16 10 na
: 2980 7 10 i
: 2988 3 12  gu
: 2992 3 10 ra
: 2996 8 8 i
- 3010
* 3012 7 8 Hi
* 3020 7 15 to
* 3028 23 15 mi
: 3052 15 13  wo
: 3068 7 12  to
: 3076 7 10 ji
: 3084 3 8 re
: 3088 12 8 ba
- 3106
: 3108 15 15 Su
: 3124 7 10 gu
: 3132 7 10  a
: 3140 7 8 no
* 3148 7 12  u
* 3156 36 8 mi
- 3194
: 3196 7 10 No 
: 3204 7 8 ni
: 3212 7 7 o
: 3220 44 8 i
- 3272
: 3275 15 8 Ma
: 3291 25 8 ta
* 3319 3 10  na
* 3323 3 8 tsu
* 3327 15 6  ga
: 3343 15 6  ku
: 3359 36 8 ru
- 3404
: 3407 15 4 Gi
: 3423 15 6 n
* 3439 7 10 i
* 3447 3 11 ro
* 3451 11 6  ni
* 3463 11 1  hi
* 3475 11 6 ka
: 3487 44 8 ru
- 3540
: 3543 7 8 Mi
: 3551 23 8 na
: 3575 3 10 mo
: 3579 3 8  ni
: 3583 15 6  u
: 3599 15 6 tsu
: 3615 44 8 su
- 3667
: 3671 3 8 Fu
: 3675 3 10 ta
: 3679 15 11 ri 
* 3695 7 10 bu
* 3703 3 11 n
* 3707 18 13  no
: 3727 15 6  ka
: 3743 64 8 ge
- 3825
: 3855 3 8 Da
: 3859 3 10 re
* 3863 3 12  yo
* 3867 3 13 ri 
* 3871 23 15 mo
: 3895 15 13  to
: 3911 7 12 o
: 3919 7 10 ku
: 3927 24 8  ni
: 3951 6 8  i
: 3959 3 10 tte
: 3963 8 10 mo
- 3973
: 3975 7 10 Ko
: 3983 7 8 ko
: 3991 7 12  ka
: 3999 22 8 ra
: 4023 3 8  ma
: 4027 10 8 ta
: 4039 7 8  wa
: 4047 7 8 ra
: 4055 6 10 ~
: 4063 23 10 tte
* 4087 3 12  ku
* 4091 3 10 re
* 4095 8 8 ru?
- 4109
: 4111 7 8 Hi
: 4119 7 15 to
: 4127 23 15 mi
: 4151 15 13  wo
: 4167 7 12  to
: 4175 7 10 ji
: 4183 3 8 re
: 4187 16 8 ba
- 4205
: 4207 14 15 Fu
: 4223 7 10 tto
: 4231 7 10  a
: 4239 7 8 no
: 4247 7 12  hi
: 4255 36 8  no
* 4295 7 10  a
* 4303 7 8 o
* 4311 7 7 zo
* 4319 70 8 ra
E