#TITLE:Viva las Vegas
#ARTIST:Elvis Presley
#EDITION:[SC]-Songs
#MP3:Elvis Presley - Viva Las Vegas.mp3
#COVER:Elvis Presley - Viva Las Vegas [CO].jpg
#BACKGROUND:Elvis Presley - Viva Las Vegas [BG].jpg
#BPM:290
#GAP:7630
: 0 7 2 Bright
: 8 7 2  light
: 16 3 4  ci
: 20 3 7 ty
: 24 3 9  gon
: 28 2 7 na
: 31 4 11  set
: 35 7 11  my
: 42 11 7  soul,
- 54
: 54 4 9 gon
: 58 4 7 na
: 62 4 16  set
: 66 7 14  my
: 73 12 11  soul
: 85 7 7  on
: 92 6 9  fi
: 98 5 11 re.
- 105
: 120 3 2 Got
: 124 3 2 ta
: 128 7 2  whole
: 136 3 2  lot
: 140 3 2 ta
: 144 3 4  mon
: 148 3 7 ey
: 152 4 9  that's
: 156 3 7  re
: 160 3 11 ad
: 164 3 11 y
: 168 3 11  to
: 172 11 7  burn,
- 184
: 184 3 9 so
: 187 3 7 ~
: 191 4 16  get
: 196 7 14  those
: 204 11 11  stakes
: 216 7 7  up
: 224 4 9  high
: 228 4 11 er.
- 234
: 244 7 2 There's
: 252 3 2  a
: 256 3 4  thou
: 260 3 4 sand
: 264 3 4  pret
: 268 3 -1 ty
: 272 3 2  wom
: 275 10 4 en
- 286
: 288 3 7 wait
: 291 5 7 ing
: 296 3 7  out
: 300 15 4  there.
- 317
: 320 4 4 They'
: 324 3 -1 re
: 328 6 4  all
: 335 4 4  liv
: 339 7 4 ing
: 348 3 4  the
: 352 4 7  de
: 356 3 7 vil
: 360 4 7  may
: 365 14 4  care,
- 380
: 380 3 -1 and
: 384 7 4  I'm
: 392 3 4  just
: 396 3 -1  a
: 400 3 4  dev
: 404 3 4 il
: 408 7 4  with
: 416 7 7  love
: 424 3 7  to
: 428 15 4  spare,
- 445
: 448 3 16 Vi
: 452 19 16 va
: 472 7 16  Las
: 480 3 14  Veg
: 484 3 11 as,
: 512 3 16  Vi
: 516 19 16 va
: 536 7 16  Las
: 544 3 14  Ve
: 548 3 11 gas
- 553
: 576 7 14 How
: 584 7 14  I
: 592 7 16  wish
: 600 4 21  th
: 604 3 19 at
: 608 3 23  there
: 612 7 23  were
: 620 11 19  more
- 632
: 632 3 21 than
: 636 4 19  the
: 640 4 27  twe
: 644 3 28 nty
: 648 4 26  four
: 653 7 23  hou
: 660 3 19 rs
: 664 3 21  in
: 668 3 19  the
: 672 4 22  d
: 676 3 23 ay.
- 681
: 700 3 14 But
: 704 3 14  ev
: 708 3 14 en
: 712 7 14  if
: 720 6 16  there
: 727 5 19  were
: 734 5 21  fo
: 739 7 23 rty
: 747 3 21  mo
: 750 9 19 re,
- 761
: 764 2 19 I
: 767 3 28  woul
: 770 3 28 dn't
: 774 5 26  sleep
: 779 3 26  a
: 782 5 23  min
: 787 4 21 ute
: 792 7 19  aw
: 799 10 23 ay.
- 811
: 820 7 7 Oh,
: 828 3 6  there's
: 832 7 4  black
: 840 3 4 jack
: 844 3 -1  and
: 848 3 4  pok
: 852 3 4 er
- 856
: 856 3 4 and
: 860 3 4  the
: 864 3 7  roulet
: 868 7 7 te
: 876 15 4  wheel.
- 892
: 892 3 11 A
: 896 3 16  for
: 900 3 16 tune
: 904 3 16  won
: 908 3 16  and
: 912 7 16  lost
: 920 5 16  on
: 927 5 19  ev
: 933 6 19 ery
: 940 15 16  deal
- 957
: 960 3 4 All
: 964 3 4  you
: 968 3 4  need's
: 972 3 -1  a
: 976 3 4  strong
: 980 3 4  heart
- 984
: 984 3 4 and
: 988 3 4  a
: 992 7 7  nerve
: 1000 3 7  of
: 1004 15 4  steel.
- 1021
: 1024 3 28 Vi
: 1028 19 28 va
: 1048 7 28  Las
: 1056 3 26  Ve
: 1060 3 23 gas,
: 1088 3 28  Vi
: 1092 19 28 va
: 1112 7 28  Las
: 1120 3 26  Ve
: 1124 3 23 gas
- 1129
: 1152 5 19 Vi
: 1158 5 19 va
: 1164 3 16  Las
: 1168 3 19  Ve
: 1172 3 21 gas
: 1176 3 19  with
: 1180 3 16  your
: 1188 2 19  ne
: 1190 5 16 ons
: 1196 7 19  fla
: 1203 4 21 shing,
- 1208
: 1208 3 19 and
: 1211 3 16  your
: 1214 3 19  one
: 1217 4 19  armed
: 1222 4 19  ban
: 1226 4 16 dits
: 1231 4 19  cra
: 1235 3 21 shing
- 1240
: 1248 2 26 all
: 1250 5 28  those
: 1256 7 26  hopes
: 1264 4 23  do
: 1268 3 19 wn
: 1272 3 19  the
: 1276 19 23  drain.
- 1297
: 1312 3 19 Vi
: 1315 6 19 va
: 1321 3 16  Las
: 1324 5 19  Ve
: 1329 3 21 gas
: 1332 4 19  turn
: 1336 4 16 ing
- 1340
: 1340 7 19 day
: 1347 4 19  in
: 1351 2 16 to
: 1354 5 19  night
: 1359 5 21  time,
- 1365
: 1365 3 19 turn
: 1368 5 16 ing
: 1373 7 19  night
: 1380 3 19  in
: 1383 3 16 to
: 1386 7 19  day
: 1393 4 21 time.
- 1398
: 1398 3 19 If
: 1401 3 19  you
: 1404 7 21  see
: 1411 3 21  it
: 1417 12 21  once,
- 1431
: 1435 3 21 you'll
: 1438 4 28  ne
: 1442 3 28 ver
: 1445 4 28  be
: 1449 3 26  the
: 1453 5 29  sa
: 1458 4 28 me
: 1463 4 26  a
: 1467 31 26 gain
- 1500
: 1524 3 2 I'm
: 1528 3 2  gon
: 1532 3 2 na
: 1536 3 2  keep
: 1540 7 2  on
: 1548 3 2  a
: 1552 3 4  run
: 1556 3 7 ning
- 1560
: 1560 3 9 gon
: 1564 3 7 na
: 1568 3 11  have
: 1572 7 11  me
: 1580 3 11  some
: 1584 7 7  fun,
- 1592
: 1592 3 7 if
: 1596 3 7  it
: 1600 2 14  co
: 1602 2 16 sts
: 1605 6 14  me
: 1612 3 14  my
: 1616 3 11  ve
: 1620 3 7 ry
: 1624 4 9  la
: 1628 3 7 st
: 1632 4 10  di
: 1636 3 11 me.
- 1641
: 1653 6 2 If
: 1660 3 2  I
: 1664 7 2  wind
: 1672 3 2  up
: 1676 7 4  broke
: 1684 7 7  well
: 1692 4 7  I'll
: 1697 4 11  al
: 1702 6 11 ways
: 1709 3 11  re
: 1713 4 7 mem
: 1718 4 4 ber
- 1641
: 1720 3 7 that
: 1724 3 7  I
: 1733 6 16  had
: 1740 3 14  a
: 1744 6 11  swing
: 1750 5 7 ing
: 1757 11 11  time.
- 1770
: 1798 4 4 I'm
: 1802 5 4  gon
: 1807 3 -1 na
: 1810 5 4  give
: 1815 4 4  it
: 1820 3 4  ev
: 1823 3 4 ery
: 1826 5 7 thing
: 1832 6 7  IÂ´ve
: 1839 12 4  got
- 1853
: 1856 2 14 L
: 1858 5 16 a
: 1863 2 16 dy
: 1865 6 16  luck
: 1872 7 16  please
: 1880 4 16  let
: 1885 4 16  the
: 1890 8 19  dice
: 1899 4 19  stay
: 1903 12 16  hot.
- 1917
: 1921 3 4 Let
: 1925 3 4  me
: 1929 6 4  shoot
: 1936 1 4  a
: 1937 5 4  sev
: 1942 3 4 en
: 1945 7 4  with
: 1952 3 4  e
: 1955 4 7 v
: 1959 6 7 ery
: 1965 9 4  shot.
- 1976
: 1984 6 28 Vi
: 1990 20 28 va
: 2012 5 28  Las
: 2018 4 26  Ve
: 2022 7 23 gas
- 2031
: 2049 5 16 Vi
: 2054 20 16 va
: 2078 3 16  Las
: 2082 5 14  Ve
: 2087 8 11 gas
- 2097
: 2112 6 28 Vi
: 2118 18 28 va
: 2138 8 28  Las
: 2146 3 26  Ve
: 2149 11 23 gas
- 2162
: 2177 3 16 Vi
: 2180 27 16 va,
: 2209 3 18  Vi
: 2212 19 18 va,
: 2233 8 14  Las
: 2241 5 18  Ve
: 2246 105 19 gas
E
