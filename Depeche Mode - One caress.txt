#TITLE:One caress
#ARTIST:Depeche Mode
#EDITION:UltraStar Depeche Mode
#MP3:Depeche Mode - One caress.mp3
#COVER:Depeche Mode - One caress [CO].jpg
#BACKGROUND:Depeche Mode - One caress [BG].jpg
#VIDEO:Depeche Mode - One caress.avi
#BPM:101
#GAP:15630
: 0 2 9 Well
: 2 2 9  I'm
: 4 2 7  down
: 6 2 9  on
: 8 2 9  my
: 10 2 7  knees
: 12 2 9  a
: 14 9 9 gain
- 24
: 24 2 4 And
: 26 2 4  I
: 28 2 7  pray
: 30 2 9  to
: 32 2 9  the
: 34 2 12  on
: 36 2 7 ly
: 38 5 9  one
- 44
: 44 2 4 Who
: 46 2 7  has
: 48 2 9  the
: 50 5 9  strength
- 56
: 56 2 4 To
: 58 2 7  bear
: 60 2 9  the
: 62 9 9  pain
- 72
: 72 2 4 To
: 74 2 4  for
: 76 2 7 give
: 78 2 9  all
: 80 2 9  the
: 82 2 12  things
: 84 1 11  that
: 85 3 7  I've
: 88 5 9  done
- 94
: 94 5 12 Oh
: 100 5 9  Gi
: 106 5 4 rl
- 113
: 118 3 7 Lead
: 122 2 7  me
: 124 2 7  in
: 126 2 9 to
: 128 2 10  your
: 130 1 10  da
: 131 3 9 rk
: 134 1 7 ne
: 135 4 9 ss
- 140
: 140 2 9 When
: 142 2 17  this
: 144 7 12  world
: 152 2 9  is
: 154 2 9  try
: 156 2 9 ing
: 158 2 9  it's
: 160 3 10  har
: 164 5 9 dest
- 170
: 170 2 5 To
: 172 3 10  leave
: 176 2 9  me
: 178 2 7  un
: 180 3 9 im
: 184 15 2 pressed
- 200
: 200 2 5 Just
: 202 2 7  one
: 204 2 9  ca
: 206 17 9 ress
- 224
: 224 2 5 From
: 226 2 7  you
: 228 2 9  and
: 230 5 5  I'm
: 236 43 12  blessed
- 281
: 336 2 9 When
: 338 2 9  you
: 340 2 7  think
: 342 2 9  you've
: 344 2 9  tried
: 346 2 7  ev
: 348 2 9 ery
: 350 7 9  road
- 359
: 366 2 4 Ev
: 368 2 7 ery
: 370 2 12  a
: 372 2 7 ve
: 374 5 9 nue
- 380
: 380 2 4 Take
: 382 2 7  one
: 384 2 9  more
: 386 5 9  look
- 392
: 392 2 4 Atwhat
: 394 2 7  you
: 396 2 9  found
: 398 7 9  old
- 407
: 410 2 4 And
: 412 2 7  in
: 414 2 9  it
: 416 2 9  you'll
: 418 2 12  find
: 420 1 11  some
: 421 3 7 thing
: 424 5 9  new
- 430
: 430 5 12 Oh
: 436 5 9  Gi
: 442 5 4 rl
- 449
: 454 3 7 Lead
: 458 2 7  me
: 460 2 7  in
: 462 2 9 to
: 464 2 10  your
: 466 1 10  da
: 467 3 9 rk
: 470 1 7 ne
: 471 4 9 ss
- 476
: 476 2 9 When
: 478 2 17  this
: 480 7 12  world
: 488 2 9  is
: 490 2 9  try
: 492 2 9 ing
: 494 2 9  it's
: 496 3 10  har
: 500 5 9 dest
- 506
: 506 2 5 To
: 508 3 10  leave
: 512 2 9  me
: 514 2 7  un
: 516 3 9 im
: 520 15 2 pressed
- 536
: 536 2 5 Just
: 538 2 7  one
: 540 2 9  ca
: 542 17 9 ress
- 560
: 560 2 5 From
: 562 2 7  you
: 564 2 9  and
: 566 5 5  I'm
: 572 43 12  blessed
- 617
: 626 2 5 I'm
: 628 3 7  shy
: 632 2 9 ing
: 634 2 7  from
: 636 2 9  the
: 638 7 2  light
- 647
: 650 2 2 I
: 652 3 5  al
: 656 2 2 ways
: 658 2 0  loved
: 660 2 2  the
: 662 7 2  night
- 671
: 674 2 2 And
: 676 3 5  now
: 680 2 2  you
: 682 2 5  of
: 684 2 7 fer
: 686 7 2  me
: 694 5 0  e
: 700 5 2 ter
: 706 5 5 nal
: 712 11 7  dark
: 724 11 9 ness
- 737
: 771 2 9 I
: 773 2 7  have
: 775 2 9  to
: 777 2 9  be
: 779 2 7 lieve
: 781 2 9  that
: 783 7 9  sin
- 792
: 795 2 4 Can
: 797 3 7  make
: 801 2 9  a
: 803 2 12  bet
: 805 2 7 ter
: 807 5 9  man
- 813
: 815 2 4 It
: 817 2 7 's
: 819 2 9  the
: 821 5 9  mood
: 827 2 4  that
: 829 2 7  I
: 831 2 9  am
: 833 7 9  in
- 842
: 845 2 4 That
: 847 2 7  left
: 849 2 9  us
: 851 2 9  back
: 853 2 12  where
: 855 1 11  we
: 856 3 7  be
: 859 5 9 gan
- 865
: 865 5 12 Oh
: 871 5 9  Gi
: 877 5 4 rl
- 884
: 889 3 7 Lead
: 893 2 7  me
: 895 2 7  in
: 897 2 9 to
: 899 2 10  your
: 901 1 10  da
: 902 3 9 rk
: 905 1 7 ne
: 906 4 9 ss
- 911
: 911 2 9 When
: 913 2 17  this
: 915 7 12  world
: 923 2 9  is
: 925 2 9  try
: 927 2 9 ing
: 929 2 9  it's
: 931 3 10  har
: 935 5 9 dest
- 941
: 942 2 5 To
: 944 3 10  leave
: 948 2 9  me
: 950 2 7  un
: 952 3 9 im
: 956 15 2 pressed
- 972
: 973 2 5 Just
: 975 2 7  one
: 977 2 9  ca
: 979 17 9 ress
- 997
: 998 2 5 From
: 1000 2 7  you
: 1002 2 9  and
: 1004 5 5  I'm
: 1010 43 12  blessed
- 1055
: 1062 5 12 Oh
: 1068 5 9  Gi
: 1074 5 4 rl
- 1081
: 1086 3 7 Lead
: 1090 2 7  me
: 1092 2 7  in
: 1094 2 9 to
: 1096 2 10  your
: 1098 1 10  da
: 1099 3 9 rk
: 1102 1 7 ne
: 1103 4 9 ss
- 1108
: 1108 2 9 When
: 1110 2 17  this
: 1112 7 12  world
: 1120 2 9  is
: 1122 2 9  try
: 1124 2 9 ing
: 1126 2 9  it's
: 1128 3 10  har
: 1132 5 9 dest
- 1138
: 1138 2 5 To
: 1140 3 10  leave
: 1144 2 9  me
: 1146 2 7  un
: 1148 3 9 im
: 1152 15 2 pressed
- 1168
: 1169 2 5 Just
: 1171 2 7  one
: 1173 2 9  ca
: 1175 17 9 ress
- 1193
: 1193 2 5 From
: 1195 2 7  you
: 1197 2 9  and
: 1199 5 5  I'm
: 1205 43 12  blessed
E
