#TITLE:Ich will Spaß
#ARTIST:Markus
#MP3:Markus - Ich will Spaß.mp3
#COVER:Markus - Ich will Spaß [CO].jpg
#BACKGROUND:Markus - Ich will Spaß [BG].jpg
#BPM:300
#GAP:13153
#ENCODING:UTF8
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
: 0 2 17 Mein
: 4 3 17  Ma
: 8 3 17 se
: 12 5 17 ra
: 20 2 17 ti
- 23
: 24 2 17 fährt
: 28 3 15  zwei
: 33 2 17 hun
: 36 3 17 dert
: 41 5 17 zehn.
- 48
* 57 3 20 Schwupp,
: 64 2 17  die
: 68 3 17  Po
: 72 3 17 li
: 77 4 17 zei
: 85 3 17  hat´s
: 92 2 15  nicht
: 97 3 17  ge
: 104 4 17 seh´n,
- 110
: 112 2 17 das
: 116 2 15  macht
: 121 6 13  Spaß.
- 129
: 160 2 18 Ich
: 165 2 18  geb
: 169 4 18  Gas,
: 177 2 17  ich
: 181 2 15  geb
: 186 5 13  Gas.
- 193
: 257 2 17 Will
: 261 2 17  nicht
: 266 5 17  spar´n,
- 273
: 275 2 17 will
: 278 2 17  nicht
: 281 2 17  ver
: 285 3 15 nünf
: 290 3 17 tig
* 298 5 17  sein,
- 305
: 322 3 17 tank
: 326 2 17  nur
: 329 2 17  das
: 334 3 17  gu
: 338 6 17 te
: 350 3 15  Su
: 354 4 17 per
: 362 4 17  rein,
- 368
: 371 2 17 ich
: 374 3 15  mach
: 379 5 13  Spaß.
- 386
: 418 2 18 Ich
: 422 2 18  geb
: 427 5 18  Gas,
: 435 2 17  ich
: 439 2 15  geb
: 444 5 13  Gas.
- 451
: 482 2 17 Ich
: 487 2 18  will
: 491 5 20  Spaß,
: 499 2 17  ich
: 503 2 18  will
: 508 7 20  Spaß.
- 517
: 547 2 17 Ich
: 550 2 18  will
: 556 4 20  Spaß,
: 563 2 17  ich
: 567 2 18  will
: 572 7 20  Spaß.
- 581
: 611 2 17 Ich
: 615 2 18  geb
: 619 5 20  Gas,
: 628 2 17  ich
: 631 2 18  geb
: 636 7 20  Gas.
- 645
: 675 2 17 Ich
: 679 2 18  will
: 684 5 20  Spaß,
: 692 2 17  ich
: 696 2 18  will
* 701 5 20  Spaß.
- 708
: 772 2 17 Ich
: 776 3 17  schubs
: 781 2 17  die
: 785 4 17  En
: 792 3 17 ten
: 800 3 15  aus
: 805 2 17  dem
: 808 2 18  Ver
: 812 4 17 kehr,
- 818
: 837 2 17 ich
: 841 3 17  jag´
: 846 2 17  die
: 850 4 17  O
: 858 4 17 pels
: 866 2 15  vor
: 871 3 17  mir
: 878 3 17  her,
- 883
: 885 2 17 ich
: 889 3 15  mach
: 895 5 13  Spaß.
- 902
: 933 2 18 Ich
: 938 2 18  mach
: 943 5 18  Spaß,
: 950 2 17  ich
: 954 2 15  mach
: 959 5 13  Spaß.
- 966
: 1030 2 17 Und
: 1034 2 17  kost´
: 1038 3 17  Ben
: 1043 4 17 zin
: 1051 3 17  auch
: 1058 3 15  drei
: 1063 4 17  Mark,
: 1071 4 17  zehn,
- 1077
* 1088 4 20 scheiß
: 1095 2 17 e
: 1099 5 17 gal,
- 1106
: 1119 2 17 es
: 1123 2 15  wird
: 1127 4 17  schon
: 1135 4 17  geh´n,
- 1141
: 1143 2 17 ich
: 1147 2 15  will
: 1151 5 13  fahr´n.
- 1158
: 1191 2 18 Ich
: 1195 2 18  will
: 1200 4 18  fahr´n,
: 1207 2 17  ich
: 1212 2 15  will
: 1217 6 13  fahr´n.
- 1225
: 1256 2 17 Ich
: 1259 2 18  will
: 1264 5 20  Spaß,
: 1272 2 17 ich
: 1276 2 18  will
* 1281 6 20  Spaß.
- 1289
: 1319 2 17 Ich
: 1323 2 18  will
: 1328 4 20  Spaß,
: 1336 2 17  ich
: 1340 2 18  will
: 1345 5 20  Spaß.
- 1352
: 1383 2 17 Ich
: 1387 2 18  geb
: 1392 5 20  Gas,
: 1400 2 17  ich
: 1404 2 18  geb
: 1409 4 20  Gas.
- 1415
: 1448 2 17 Ich
: 1452 2 18  will
: 1457 4 20  Spaß,
: 1465 2 17  ich
: 1469 2 18  will
: 1473 6 20  Spaß.
- 1481
: 1936 2 17 Deutsch
: 1940 2 17 land,
: 1944 2 15  Deutsch
* 1948 4 17 land,
- 1954
: 1960 2 15 spürst
: 1964 4 17  du
: 1972 4 17  mich?
- 1978
: 1996 2 17 Heut
: 2000 3 17  nacht
: 2008 2 17  komm
: 2012 4 17  ich
: 2024 3 15  ü
: 2028 4 17 ber
: 2036 4 17  dich.
- 2042
: 2044 3 17 Das
: 2049 2 15  macht
: 2054 5 13  Spaß.
- 2061
: 2092 2 18 Das
: 2097 2 18  macht
: 2101 5 18  Spaß,
: 2108 2 17  das
: 2113 2 15  macht
: 2118 6 13  Spaß.
- 2126
: 2188 2 13 Der
: 2192 3 17  Tank
: 2196 3 17 wart
: 2201 3 17  ist
: 2209 4 17  dein
: 2217 2 15  bes
: 2221 3 17 ter
: 2229 5 17  Freund.
- 2236
* 2245 4 20 Hui,
: 2253 3 17  wenn
: 2257 2 17  ich
: 2262 4 17  komm,
- 2268
: 2277 3 17 wie
: 2282 2 15  der
: 2286 3 17  sich
: 2294 4 17  freut.
- 2300
: 2302 2 17 Er
: 2306 3 15  braucht
: 2312 5 13  Spaß!
- 2319
: 2350 2 18 Er
: 2354 2 18  hat
: 2359 4 18  Spaß,
: 2365 2 17  er
: 2369 3 15  hat
: 2375 5 13  Spaß.
- 2382
: 2414 2 17 Wir
: 2418 2 18  woll´n
: 2423 5 20  Spaß,
: 2431 2 17  wo
: 2435 2 18 llen
: 2439 6 20  Spaß.
- 2447
: 2478 2 17 Wir
: 2482 2 18  woll´n
: 2487 4 20  Spaß,
: 2495 2 17  wo
: 2498 2 18 llen
* 2504 8 20  Spaß.
- 2514
: 2542 3 17 Wir
: 2546 2 18  geb´n
: 2551 5 20  Gas,
: 2559 3 17  ge
: 2563 2 18 ben
: 2567 8 20  Gas.
- 2577
: 2606 2 20 Wir
: 2610 2 20  woll´n
: 2615 5 20  Spaß,
: 2623 2 17  wo
: 2627 2 18 llen
: 2632 9 20  Spaß.
- 2643
: 2670 2 17 Ich
: 2674 2 18  will
: 2680 4 20  Spaß,
: 2688 2 17  ich
: 2692 2 18  will
* 2696 8 20  Spaß.
- 2706
: 2735 2 20 Ich
: 2740 2 20  brauch
: 2744 6 20  Spaß,
: 2752 2 17  ich
: 2756 2 18  brauch
: 2761 9 20  Spaß.
- 2772
: 2800 2 20 Ich
: 2804 2 20  will
: 2808 5 20  fahr´n,
: 2816 2 17  ich
: 2820 2 18  will
: 2824 8 20  fahr´n.
- 2834
: 2864 2 20 Ich
: 2868 2 20  will
: 2872 6 20  fahr´n,
: 2880 2 17  ich
: 2884 2 18  will
* 2889 7 20  fahr´n.
- 2898
: 2928 2 20 Ich
: 2933 2 20  mach
: 2937 5 20  Spaß,
: 2945 2 17  ich
: 2949 2 18  brauch
: 2954 7 20  Spaß.
E