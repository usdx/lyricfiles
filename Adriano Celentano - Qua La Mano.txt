#TITLE:Qua La Mano
#ARTIST:Adriano Celentano
#MP3:Adriano Celentano - Qua La Mano.mp3
#VIDEO:Adriano Celentano - Qua La Mano.mp4
#COVER:Adriano Celentano - Qua La Mano.JPG
#BPM:224.81
#GAP:19058
#ENCODING:UTF8
: 0 2 0 Che
: 3 2 0  guaio
: 6 2 0  ho
: 9 2 0  combi
: 13 7 0 nato
: 30 9 -2  certo
- 49
: 61 2 -2 Per
: 64 4 -2  star
: 69 3 -1  fermo
: 73 2 -1  non
: 76 3 -1  son
: 80 7 -4  nato
- 101
: 127 2 0 e
: 130 3 0  tu
: 134 2 0  che
: 137 2 0  mhai
: 141 6 0  crea
: 148 4 -3 to
: 166 3 -3  lo
: 170 3 -3  so,
- 183
: 194 3 -3 Sarei
: 198 4 -3  nero
: 203 2 -3  e
: 207 7 -3  avvilito
: 232 4 5  e
: 237 3 5  poi
: 243 3 5  piu
: 248 3 5  ce
: 253 3 5  lei
- 259
: 261 1 0 Coro:
: 267 1 0  muoviti
: 274 2 0  un
: 279 1 0  po
- 289
: 292 10 3 forse
: 303 4 3  lo
: 308 8 0  faro
- 326
: 333 8 -7 cheforte
: 342 8 -7  sei
- 355
: 357 6 -1 quando
- 363
: 365 3 3 Qui
: 369 2 5  ce
* 374 5 1  lei
- 386
: 389 2 0 muoviti
: 403 2 0  un
: 408 1 0  po
- 419
: 423 2 0 si
: 426 2 3  lo
: 430 3 -8  fa
: 435 2 0 ro
- 447
: 451 2 0 mi
: 460 2 0  sembriun
: 471 1 0  re
- 486
: 519 7 3 Che
: 527 6 1  guaio
: 536 5 0  e
- 570
: 704 2 -2 A
: 708 3 0  volte
: 713 3 0  lho
: 717 3 0  ten
: 721 2 0 ta
: 724 5 0 to
: 735 8 -2  credi
- 753
: 769 2 -2 ma
: 773 4 -1  senza
: 779 5 -1  risulta
: 789 5 -1 to
- 808
: 831 2 -2 E
: 836 2 0  il
* 839 2 0  giorno
: 843 2 0  che
: 848 2 0  son
: 851 2 0  nato
: 871 4 -1  son
: 877 8 -2  certo
- 892
: 895 4 -1 Che
: 900 3 -1  sara
: 904 11 -3  stato
- 925
: 940 5 4 mentre
: 949 5 4  tu
: 959 6 2  ascoltavi
: 972 4 0  un
: 977 2 0  rock
: 980 11 0  scatenato
- 1005
: 1024 3 -3 Il
: 1029 4 0  rock
: 1034 2 -2  del
: 1038 4 -2  creato
- 1052
: 1062 4 6 a
: 1067 3 6  volte
: 1071 3 6  lho
: 1081 3 5  ten
: 1085 2 5 ta
: 1088 2 5 to
- 1095
: 1097 2 0 muovitiun
: 1107 2 0  po
- 1119
: 1128 3 2 Forse
: 1133 2 2  lo
: 1140 3 0  faro
- 1152
: 1156 2 0 che
: 1164 2 0  forte
: 1172 2 0  sei
- 1184
: 1192 3 0 quando
: 1197 4 2  qui
: 1204 3 2  ce
: 1209 4 0  lei
- 1219
: 1221 29 0 muovitiun
: 1253 1 0  po
- 1255
: 1257 4 0 Si
* 1263 2 2  lo
* 1267 4 0  faro
- 1281
: 1285 1 0 mi
: 1294 2 0  sembri
: 1304 2 0  un
: 1308 1 0  re
- 1323
: 1354 6 3 che
: 1362 6 2  guaio
: 1374 5 0  e
- 1408
: 1544 4 0 se
: 1549 4 0  mi
: 1554 4 0  hai
: 1559 6 0  capito
- 1579
: 1609 1 0 Ho
: 1621 30 0  perdonato
- 1661
: 1680 2 0 qua
: 1686 1 0  la
: 1692 2 0  mano
- 1708
: 1733 2 0 io
: 1738 1 0  parlero
: 1745 2 0  per
: 1750 1 0  te
- 1780
: 2215 2 0 non
: 2218 3 0  credo
: 2223 3 0  sia
: 2228 3 0  peccato,
- 2241
: 2248 15 0 Certo,
- 2274
: 2278 2 -2 sono
: 2282 2 -2  troppo
: 2288 2 -2  scombinato
- 2304
: 2340 2 0 ma
: 2344 2 0  tu
: 2349 1 0  che
: 2355 2 0  mhai
: 2359 1 0  creato
- 2369
: 2373 14 0 sicuro,
- 2397
: 2406 7 0 Lavevi
: 2414 8 0  calcolato
- 2436
: 2468 6 0 che
* 2475 4 0  guaio
: 2480 2 0  ho
: 2483 7 0  combinato
- 2496
: 2498 3 0 Lo
: 2502 6 0  so
: 2517 3 0  per
: 2521 3 0  star
: 2525 5 0  fermo
: 2531 3 0  non
: 2535 4 0  son
: 2540 7 0  nato
- 2557
: 2561 2 0 non
: 2567 2 0  son
: 2576 2 0  nato
: 2586 1 0  oh
: 2589 1 0  oh
: 2593 1 0  oh.
- 2600
: 2602 2 0 Che
: 2610 2 0  forte
: 2616 2 0  sei
- 2626
: 2629 5 0 quando
: 2635 5 2  qui
: 2641 5 5  ce
: 2648 5 8  lei
- 2659
: 2661 9 0 muoviti
: 2673 2 0  un
: 2678 2 0  po
- 2690
: 2697 2 0 forse
: 2703 2 2  lo
: 2711 2 4  faro
- 2725
: 2729 2 0 Che
: 2738 1 0  forte
: 2746 2 0  sei
- 2760
: 2764 2 3 se
: 2770 2 3  qui
: 2776 2 0  ce
: 2782 2 -2  lei
- 2791
: 2794 2 0 mi
: 2803 2 0  sembri
: 2810 2 0  un
: 2814 2 0  re
- 2830
: 2859 4 3 che
: 2867 5 0  guaio
: 2875 6 0  e.
E