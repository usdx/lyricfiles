#TITLE:Dead Inside
#ARTIST:Muse
#LANGUAGE:English
#GENRE:Rock
#YEAR:2015
#CREATOR:gun88
#MP3:Muse - Dead Inside.mp3
#COVER:Muse - Dead Inside [CO].png
#VIDEO:Muse - Dead Inside.mp4
#MEDLEYSTARTBEAT:1188
#MEDLEYENDBEAT:1620
#BPM:200
#GAP:767
: 0 3 3 Dead
* 4 3 2  in
: 8 4 2 side
- 38
F 128 3 -12 Dead
F 132 3 -12  in
F 136 4 -12 side
- 150
: 164 3 0 Re
: 168 14 7 vere
* 188 3 7  a
: 192 3 5  mil
: 196 3 3 lion
: 200 18 7  prayers
- 228
: 232 3 0 And
: 236 6 7  draw
: 244 6 5  me
: 252 6 3  in
: 260 3 2 to
: 264 3 2  your
: 268 7 3  ho
: 276 3 0 li
: 280 10 0 ness
- 300
: 312 3 -5 But
: 316 3 -5  there's
: 320 3 -5  noth
: 324 3 -5 ing
: 328 3 3  there
: 332 8 2 ~
- 346
: 348 3 -2 Light
: 352 3 -2  on
: 356 3 -2 ly
* 360 3 3  shines
: 364 6 2 ~
- 374
: 376 3 2 From
: 380 6 3  those
: 388 3 0  who
: 392 6 0  share
- 408
: 420 3 0 Un
: 424 18 7 leash
* 444 3 7  a
: 448 3 5  mil
: 452 3 3 lion
: 456 18 7  drones
- 481
: 484 3 -2 And
: 488 3 -2  con
: 492 4 7 fine
: 500 4 5  me
: 508 6 3  then
: 516 4 2  e
: 524 6 3 rase
: 532 2 0  me
: 536 10 0  babe
- 556
: 568 3 -5 Do
: 572 3 -5  you
: 576 3 -5  have
: 580 3 -5  no
: 584 3 3  soul
: 588 8 2 ~?
- 602
: 604 3 -2 It's
: 608 3 -2  like
: 612 3 -2  I
* 616 3 3  died
: 620 6 2 ~
- 633
: 636 7 3 Long
: 644 3 0  a
: 648 6 0 go
- 664
: 680 3 0 Your
: 684 7 7  lips
* 692 6 5  feel
: 700 8 3  warm
: 710 12 2  to
: 724 3 3  the
: 728 9 3  touch
- 738
: 740 3 0 You
* 744 3 0  can
: 748 6 7  bring
: 756 6 5  me
: 764 7 3  back
: 772 3 2  to
: 776 10 3  life
- 796
: 804 2 0 On
: 808 3 0  the
: 812 7 7  out
: 820 7 5 side
: 828 7 3  you're
: 836 7 2  a
: 844 15 5 blaze
: 860 3 3  and
: 864 2 2  a
: 867 8 0 live
- 884
: 888 3 0 But
: 892 3 0  you're
* 896 3 3  dead
: 900 3 2  in
: 904 4 2 side
- 934
: 1188 3 0 You're
: 1192 16 7  free
* 1214 1 5  to
: 1216 3 5  touch
: 1220 3 3  the
: 1224 18 7  sky
- 1252
: 1256 3 0 Whilst
* 1260 4 7  I
: 1268 4 5  am
: 1276 6 3  crushed
: 1284 4 2  and
: 1292 7 3  pul
: 1300 3 0 ver
: 1304 10 0 ised
- 1324
: 1332 3 -5 Be
: 1336 3 -5 cause
: 1340 3 -5  you
: 1344 3 -5  need
: 1348 3 -5  con
* 1352 3 3 trol
: 1356 8 2 ~
- 1370
: 1372 3 -2 Now
: 1376 3 -2  I'm
: 1380 3 -2  the
: 1384 3 3  one
: 1388 6 2 ~
- 1398
: 1400 3 2 Who's
: 1404 7 3  let
* 1412 3 0 ting
: 1416 6 0  go
- 1432
: 1444 3 0 You
: 1448 18 7  like
* 1470 1 5  to
: 1472 3 5  give
: 1476 3 3  an
: 1480 18 7  inch
- 1508
: 1512 3 0 Whilst
: 1516 4 7  I
: 1524 4 5  am
: 1532 6 3  giv
: 1540 2 2 ing
: 1544 3 2  in
: 1548 3 3 fin
* 1552 3 0 i
: 1556 6 0 ty
- 1572
: 1588 3 -5 But
: 1592 1 -5  now
: 1594 1 -5  I've
: 1596 3 -5  got
: 1600 3 -5  noth
: 1604 3 -5 ing
* 1608 3 3  left
: 1612 8 2 ~
- 1626
: 1628 3 -2 You
: 1632 3 -2  have
: 1636 3 -2  no
: 1640 3 3  cares
: 1644 6 2 ~
- 1654
: 1656 3 2 And
: 1660 7 3  I'm
* 1668 3 0  be
: 1672 6 0 reft
- 1688
: 1704 3 0 Your
: 1708 7 7  skin
: 1716 6 5  feels
: 1724 10 3  warm
: 1736 10 2  to
: 1748 3 3  ca
: 1752 8 3 ress
- 1762
: 1764 3 0 I
* 1768 3 0  see
: 1772 6 7  mag
: 1780 6 5 ic
: 1788 4 3  in
: 1794 4 2  your
: 1800 10 3  eyes
- 1820
: 1828 3 0 On
* 1832 3 0  the
: 1836 7 7  out
: 1844 7 5 side
: 1852 7 3  you're
: 1860 7 2  a
: 1868 15 5 blaze
: 1884 3 3  and
: 1888 3 2  a
: 1892 8 0 live
- 1909
: 1912 3 0 But
: 1916 3 0  you're
: 1920 3 3  dead
: 1924 3 2  in
: 1928 4 2 side
- 1958
F 2048 3 -12 Dead
F 2052 3 -12  in
F 2056 6 -12 side
- 2088
: 2316 7 7 Feel
: 2324 3 5  me
: 2328 8 3  now
- 2342
: 2344 11 7 Hold
* 2356 3 5  me
: 2360 8 3  please
- 2374
: 2376 5 -2 I
: 2382 7 7  need
: 2390 3 5  you
: 2394 3 3  to
* 2398 5 7  see
: 2404 3 5  who
: 2408 3 3  I
: 2412 3 5 ~
: 2416 14 5  am
- 2434
: 2436 3 7 O
: 2440 3 7 pen
: 2444 5 7  up
* 2450 1 5  to
: 2452 6 3  me
- 2465
: 2468 4 7 Stop
: 2474 6 7  hid
* 2482 2 3 ing
: 2485 2 5  from
: 2488 8 3  me
- 2498
: 2500 4 -2 It's
: 2508 5 7  hurt
: 2515 2 5 ing
: 2518 6 3  me
- 2530
: 2532 3 -2 On
: 2536 3 -2 ly
* 2540 6 8  you
: 2552 3 7  can
: 2556 3 5  stop
: 2560 3 3  the
: 2564 24 7  pain
- 2594
: 2596 4 10 Don't
: 2604 14 10  leave
: 2620 3 8  me
: 2624 7 8  out
: 2632 7 7 ~
* 2640 1 7  in
* 2642 1 8  the
: 2644 5 7  cold
* 2650 3 5 ~
: 2654 7 3 ~
- 2662
: 2664 3 -2 Don't
: 2668 6 7  leave
: 2676 6 5  me
: 2684 7 3  out
: 2692 4 2  to
: 2700 12 3  die
- 2719
: 2722 1 3 I
: 2724 5 7  gave
* 2730 1 5  you
: 2732 3 5  eve
: 2736 3 3 ry
: 2740 4 3 thing
- 2746
: 2748 3 3 I
: 2752 7 7  can't
: 2760 3 5  give
: 2764 3 3  you
: 2768 1 3  an
* 2770 1 5 y
: 2772 6 7 more
- 2787
: 2790 1 -2 Now
* 2792 3 -2  I've
: 2796 3 -2  be
: 2800 5 8 come
: 2806 4 7 ~
: 2812 4 5  just
: 2820 4 7  like
: 2828 3 7  you
: 2832 3 5 ~
: 2836 8 3 ~
- 2853
: 2856 3 0 My
: 2860 7 7  lips
: 2868 7 5  feel
: 2876 9 3  warm
: 2886 12 2  to
* 2900 1 3  the
: 2902 8 3  touch
- 2914
: 2916 3 -5 But
: 2920 3 -5  my
: 2924 7 7  words
: 2932 7 5  seem
: 2940 3 3  so
: 2944 3 2  a
: 2948 8 3 live
- 2969
: 2984 3 0 My
: 2988 7 7  skin
: 2996 7 5  is
: 3004 8 3  warm
: 3016 11 2  to
* 3028 2 3  ca
: 3032 6 3 ress
- 3042
: 3044 3 -5 I'll
: 3048 3 -5  con
: 3052 7 7 trol
: 3060 7 5  and
: 3068 4 3  hyp
: 3073 4 2 no
: 3078 10 3 tize
- 3097
: 3100 3 7 You've
: 3104 4 7  taught
: 3110 4 7  me
: 3115 4 7  to
: 3120 8 7  lie
* 3132 1 5  with
: 3134 5 3 out
* 3140 3 2  a
: 3144 10 3  trace
- 3164
: 3172 3 0 And
* 3176 3 0  to
: 3180 6 7  kill
: 3188 6 5  with
: 3196 5 3  no
: 3204 3 2  re
: 3208 10 3 morse
- 3228
: 3236 3 7 On
: 3240 4 7  the
: 3245 5 7  out
: 3252 8 7 side
- 3266
: 3268 3 2 I'm
* 3272 3 3  the
: 3276 15 5  great
: 3292 2 3 ~
: 3295 4 2 est
: 3300 12 0  guy
- 3318
: 3320 3 0 Now
: 3324 3 0  I'm
: 3328 3 3  dead
: 3332 3 2  in
: 3336 6 2 side
- 3368
F 3456 3 -12 Dead
F 3460 3 -12  in
F 3464 4 -12 side
E
