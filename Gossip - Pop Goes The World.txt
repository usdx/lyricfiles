#TITLE:Pop goes the World
#ARTIST:Gossip
#MP3:Gossip - Pop Goes The World.mp3
#VIDEO:Gossip - Pop Goes The World.mp4
#COVER:Gossip - Pop Goes The World[CO].jpg
#BPM:256,02
#GAP:22733
#VIDEOGAP:1
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Rock
: 0 1 17 Find
: 2 1 17  your
: 4 2 17  self
: 7 1 17  in
: 9 1 17  a
: 11 3 17  si
: 16 1 20 tu
: 18 4 24 a
: 24 4 15 tion
- 30
: 32 1 17 Can
: 34 1 17  not
: 36 3 17  talk
: 40 1 17  your
: 42 3 17  way
: 46 3 20  out
: 50 6 24  of
- 60
: 64 1 17 Sti
: 66 1 17 mu
: 68 3 17 late
: 72 1 17  the
: 74 3 17  con
: 78 3 20 ver
: 82 4 24 sa
: 88 4 15 tion
- 94
: 96 3 17 How
: 100 3 17  do
: 104 1 17  you
: 106 3 17  rise
: 110 2 20  a
: 113 6 24 bove
- 119
: 120 3 20 You
: 124 6 22  try
: 132 3 24  to
: 136 3 22 ~
: 140 3 22  tell
: 144 3 20  so
: 148 3 19 me
: 152 3 19 o
: 156 3 17 ne
- 159
: 160 3 17 Bu
: 164 3 15 t
: 168 6 15  you
: 176 3 15  can't
: 180 3 15  de
: 184 3 17 scri
: 188 3 12 be
: 192 10 12  it
- 216
: 256 3 17 We'll
: 260 3 17  start
: 264 3 17  a
: 268 1 17  de
: 270 3 20 mon
: 274 4 24 stra
: 280 4 15 tion
- 286
: 288 3 17 Or
: 292 3 17  we'll
: 296 3 17  cre
: 300 1 17 ate
: 302 3 20  a
: 306 4 24  scene
- 316
: 320 3 17 Make
: 324 3 17  noise
: 328 1 17  from
: 330 3 17  our
: 334 3 20  frus
: 338 4 24 tra
: 344 4 15 tion
- 350
: 352 3 17 News
: 356 3 17  pa
: 360 1 17 pers,
: 362 3 17  ma
: 366 3 20 ga
: 370 4 24 zines
- 375
: 376 3 20 We'll
: 380 6 22  turn
: 388 6 24  them
: 396 3 22  on
: 400 2 20  their
: 404 2 19 ~
: 408 6 19  heads
- 415
: 416 3 17 You
: 420 3 15 ~
: 424 6 15  can't
: 432 6 15  de
: 440 3 17 ny
: 444 3 12 ~
: 448 10 12  it
- 468
: 500 2 12 For
: 504 14 17  once
- 520
: 528 3 17 We'll
: 532 3 24  do
: 536 6 22  what
: 544 6 20  come
: 552 6 19  na
: 560 1 15 tu
: 562 1 15 ra
: 564 3 12 l
: 568 6 12 ly
- 584
: 592 3 12 We'll
: 596 3 24  ap
: 600 6 22 pro
: 608 3 20 ach
: 612 3 20  it
: 616 10 19  ca
: 628 3 27 sual
: 632 1 24 ly
: 634 1 22 ~
: 636 8 20 ~
- 652
: 660 3 24 With
: 664 6 22  no
: 672 3 20 ~
: 676 3 20  a
: 680 10 19 po
: 692 3 15 lo
: 696 4 17 gy
- 712
: 732 3 20 O
: 736 3 22 ~
: 740 3 22 ~
: 744 3 24 ~
: 748 3 22 ~
: 752 3 20 h
- 755
: 756 2 12 For
: 760 12 17  once
- 776
: 784 3 17 We
: 788 3 24  will
: 792 6 22  ha
: 800 3 20 ve
: 804 3 20  the
: 808 6 19  fi
: 816 6 15 nal
: 824 6 12  say
- 840
: 852 3 24 Good
: 856 6 22 by
: 864 3 20 e
: 868 3 20  to
: 872 10 19  yes
: 884 3 27 ter
: 888 1 24 day
: 890 1 22 ~
: 892 8 20 ~
- 904
: 912 3 12 'Cause
: 916 3 24  we
: 920 2 22  kno
: 924 2 20 w
: 928 6 20  we're
: 936 6 19  hear
: 944 6 15  to
: 952 6 17  stay
- 972
: 988 3 20 O
: 992 3 22 ~
: 996 3 22 ~
: 1000 3 24 ~
: 1004 3 22 ~
: 1008 3 20 ~
: 1012 4 20 h
- 1018
: 1020 6 24 Pop,
: 1028 6 24  pop,
: 1036 6 22  pop
: 1044 3 22  goes
: 1048 1 20  the
: 1050 12 24  world
- 1072
: 1084 18 24 Ne
: 1104 6 22 w
: 1112 3 22  sen
: 1116 3 22 sa
: 1120 6 20 ti
: 1128 4 22 on
- 1140
: 1148 6 24 Pop,
: 1156 6 24  pop,
: 1164 6 22  pop
: 1172 3 22  goes
: 1176 1 20  the
: 1178 1 24  wor
: 1180 1 22 ~
: 1182 1 17 ~
: 1184 6 17 ~
: 1192 4 15 ld
- 1204
: 1212 18 27 Ne
: 1232 6 22 w
: 1240 3 22  cre
: 1244 3 22 a
: 1248 8 20 tion
- 1268
: 1280 3 17 Give
: 1284 3 17  e
: 1288 3 17 very
: 1292 1 17  ge
: 1294 3 20 ne
: 1298 4 24 ra
: 1304 4 15 tion
- 1310
: 1312 3 17 A
: 1316 3 17  diffe
: 1320 3 17 rent
: 1324 1 17  set
: 1326 3 20  of
: 1330 8 24  rules
- 1340
: 1344 3 17 We'll
: 1348 3 17  start
: 1352 3 17  with
: 1356 1 17  T.
: 1358 3 20 V.
: 1362 4 24  sta
: 1368 4 15 tions
- 1374
: 1376 3 17 The
: 1380 3 17  ra
: 1384 1 17 di
: 1386 3 17 os
: 1390 3 20  and
: 1394 4 24  schools
- 1399
: 1400 3 20 Just
: 1404 6 22  try
: 1412 6 24  to
: 1420 3 22  have
: 1424 3 20  so
: 1428 3 19 me
: 1432 6 19  fun
- 1439
: 1440 6 17 And
: 1448 6 15  don't
: 1456 6 15  get
: 1464 3 17  cau
: 1468 1 17 ~
: 1470 1 15 ~
: 1472 4 12 ght
- 1500
: 1536 3 17 We'll
: 1540 3 17  cap
: 1544 3 17 ture
: 1548 3 17  their
: 1552 1 20  at
: 1554 4 24 ten
: 1560 4 15 tion
- 1566
: 1568 3 17 We'll
: 1572 3 17  make
: 1576 3 17  them
: 1580 3 17  quite
: 1584 1 20  a
: 1586 6 24  ware
- 1596
: 1600 1 17 Of
: 1602 3 17  all
: 1606 3 17  of
: 1610 3 17  our
: 1614 3 20  in
: 1618 4 24 ten
: 1624 4 15 tions
- 1630
: 1632 3 17 We'll
: 1636 3 17  make
: 1640 3 17  'em
: 1644 1 17  stop
: 1646 3 20  and
: 1650 4 24  stare
- 1655
: 1656 3 20 They'll
: 1660 6 22  take
: 1668 6 24  a
: 1676 3 22  se
: 1680 6 20 cond
: 1688 6 19  look
- 1695
: 1696 6 17 On
: 1704 6 15  se
: 1712 6 15 cond
: 1720 3 17  thou
: 1724 1 17 ~
: 1726 1 15 ~
: 1728 6 12 ght
- 1756
: 1780 2 12 For
: 1784 14 17  once
- 1804
: 1808 3 17 We'll
: 1812 3 24  do
: 1816 6 22  what
: 1824 6 20  come
: 1832 6 19  na
: 1840 1 15 tu
: 1842 1 15 ra
: 1844 3 12 l
: 1848 6 12 ly
- 1860
: 1872 3 17 We'll
: 1876 3 24  ap
: 1880 6 22 pro
: 1888 3 20 ach
: 1892 3 20  it
: 1896 10 19  ca
: 1908 3 27 sual
: 1912 1 24 ly
: 1914 1 22 ~
: 1916 8 20 ~
- 1932
: 1940 3 24 With
: 1944 6 22  no
: 1952 3 20 ~
: 1956 3 20  a
: 1960 10 19 po
: 1972 3 15 lo
: 1976 4 17 gy
- 1992
: 2012 3 20 O
: 2016 3 22 ~
: 2020 3 22 ~
: 2024 3 24 ~
: 2028 3 22 ~
: 2032 3 20 h
- 2035
: 2036 2 12 For
: 2040 12 17  once
- 2056
: 2064 3 17 We
: 2068 3 24  can
: 2072 3 22  ha
: 2076 3 20 ve
: 2080 6 20  the
: 2088 3 19  fi
: 2092 3 15 ~
: 2096 6 15 nal
: 2104 8 12  say
- 2120
: 2132 3 24 Good
: 2136 6 22 by
: 2144 3 20 e
: 2148 3 20  to
: 2152 10 19  yes
: 2164 3 27 ter
: 2168 1 24 day
: 2170 1 22 ~
: 2172 8 20 ~
- 2188
: 2192 3 12 'Cause
: 2196 3 24  we
: 2200 3 22  kno
: 2204 3 20 w
: 2208 6 20  we're
: 2216 10 19  hear
: 2228 3 15  to
: 2232 3 17  sta
: 2236 1 17 ~
: 2238 1 15 ~
: 2240 6 12 y
- 2260
: 2268 3 20 O
: 2272 3 22 ~
: 2276 3 22 ~
: 2280 3 24 ~
: 2284 3 22 ~
: 2288 3 20 ~
: 2292 4 20 h
- 2298
: 2300 6 24 Pop,
: 2308 6 24  pop,
: 2316 6 22  pop
: 2324 3 22  goes
: 2328 1 20  the
: 2330 8 24  world
- 2352
: 2364 22 24 Ne
: 2388 3 22 w
: 2392 3 22  sal
: 2396 3 22 va
: 2400 6 20 ti
: 2408 2 17 on
- 2420
: 2428 6 24 Pop,
: 2436 6 24  pop,
: 2444 6 22  pop
: 2452 3 22  goes
: 2456 1 20  the
: 2458 1 24  wor
: 2460 1 17 ~
: 2462 6 17 ~
: 2470 4 15 ld
- 2480
: 2492 18 27 Ne
: 2512 6 22 w
: 2520 3 22  trans
: 2524 3 22 la
: 2528 8 20 tion
- 2548
: 2556 6 24 Pop,
: 2564 6 24  pop,
: 2572 6 22  pop
: 2580 3 22  goes
: 2584 1 20  the
: 2586 8 24  world
- 2604
: 2620 22 24 Ne
: 2644 3 22 w
: 2648 3 22  e
: 2652 3 22 la
: 2656 8 20 tion
- 2672
: 2684 6 24 Pop,
: 2692 6 24  pop,
: 2700 6 22  pop
: 2708 3 22  goes
: 2712 1 20  the
: 2714 1 24  wor
: 2716 1 20 ~
: 2718 8 17 ~
: 2728 4 15 ld
- 2740
: 2748 26 27 Ne
: 2776 20 29 ~
: 2798 6 22 w
: 2808 3 22  sen
: 2812 3 22 sa
: 2816 4 20 ti
: 2822 4 22 on
E