#TITLE:Geile Zeit
#ARTIST:Juli
#LANGUAGE:Deutsch
#EDITION:SingStar Deutsch Rock-Pop
#GENRE:Pop
#YEAR:2004
#MP3:Juli - Geile Zeit.mp3
#COVER:Juli - Geile Zeit [CO].jpg
#BACKGROUND:Juli - Geile Zeit [BG].jpg
#VIDEO:Juli - Geile Zeit [VD#0].avi
#VIDEOGAP:0
#BPM:216
#GAP:9513,89
: 0 1 60 Hast
: 2 1 62  du
: 4 1 60  geg
: 6 4 62 laubt
- 11
: 13 3 60 Hast
: 17 1 62  du
: 19 3 60  ge
: 24 3 64 hofft
- 28
: 30 2 59 Dass
: 34 2 59  al
: 38 2 59 les
: 42 3 59  bes
: 46 3 60 ser
: 50 4 60  wird?
- 56
: 64 1 60 Hast
: 66 1 62  du
: 68 1 60  ge
: 70 4 62 weint
- 76
: 78 2 60 Hast
: 82 2 62  du
: 84 1 60  ge
: 88 5 64 fleht
- 94
: 94 2 60 Weil
: 97 3 60  al
: 102 2 60 les
: 106 3 64  an
* 111 4 65 ders
: 118 2 62  ist?
: 120 4 64 ~
- 125
: 126 3 62 Wo
: 129 1 62  ist
: 133 1 62  die
: 135 4 62  Zeit
- 141
: 145 1 62 Wo
: 146 1 62  ist
: 150 2 60  das
: 155 2 62  Meer?
: 157 3 64 ~
- 162
: 167 1 60 Sie
: 168 4 62  fehlt
: 179 1 64  sie
: 181 7 62  fehlt
: 190 4 64  hier
- 196
: 198 1 62 Du
: 199 3 62  fragst
: 207 3 64  mich
- 212 213
: 223 2 60 Wo
: 226 2 59  sie
: 230 2 60  ge
: 233 4 59 blie
: 238 2 60 ben
: 241 5 60  ist
- 248 301
: 321 1 60 Die
: 323 2 62  Näch
: 326 2 60 te
: 328 2 62  kom
: 330 2 62 men
- 333
: 334 3 60 Die
: 338 3 62  Ta
: 342 2 60 ge
: 345 4 64  gehen
- 350
: 352 1 59 Es
: 354 2 59  dreht
: 358 2 59  und
: 362 3 59  wen
: 365 3 60 det
: 370 5 60  sich
- 377
: 384 1 60 Hast
: 386 1 62  du
: 388 1 60  die
: 390 5 62  Scher
: 395 1 62 ben
- 397
: 397 4 62 Nicht
: 402 2 60  ge
: 406 5 64 sehen
- 412
: 413 3 64 Auf
: 418 2 64  de
: 420 2 64 nen
: 422 3 60  du
: 426 4 64  wei
: 432 4 65 ter
: 439 4 64 gehst?
- 444
: 446 1 62 Wo
: 448 2 62  ist
: 452 2 60  das
: 456 2 62  Licht
- 460
: 466 1 62 Wo
: 467 2 62  ist
: 470 3 60  dein
: 478 4 64  Stern?
- 483
: 483 3 60 Er
: 488 4 62  fehlt
: 498 2 60  er
: 503 4 62  fehlt
: 510 4 64  hier
: 516 1 62  du
: 517 5 62  fragst
: 526 4 64  mich
- 532 533
: 544 2 59 Wo
: 546 2 59  er
: 549 3 59  ge
: 553 3 59 blie
: 557 4 60 ben
: 561 5 60  ist
- 568
: 574 3 57 Wird
: 578 2 57  al
: 581 2 57 les
: 584 4 59  an
: 589 6 60 ders?
- 597 598
: 606 2 57 Wird
: 610 1 57  al
: 612 1 57 les
: 616 4 59  an
: 622 5 60 ders?
- 629 630
: 638 2 57 Wird
: 642 1 57  al
: 644 1 57 les
: 648 5 69  an
: 654 37 67 ders?
- 693 694
: 714 6 71 Ja
: 720 3 72  ich
: 726 5 69  weiß
- 732
: 733 2 64 Es
: 738 3 64  war
: 741 1 64  'ne
: 746 5 71  gei
: 751 3 72 le
: 757 6 67  Zeit
- 765
: 767 2 64 Uns
: 769 3 64  war
: 773 3 64  kein
: 777 6 71  Weg
: 783 5 72  zu
: 789 5 67  weit
- 796 797
: 813 8 60 Du
: 821 6 62  fehlst
: 830 6 64  hier
- 838
: 842 5 71 Ja
: 847 3 72  ich
: 854 4 69  weiß
- 859
: 861 2 64 Es
: 866 2 64  war
: 870 2 64  'ne
: 873 5 71  gei
: 879 5 72 le
: 885 4 67  Zeit
- 891
: 898 3 62 Hey
: 902 2 64  es
: 906 3 71  tut
: 911 4 72  mir
: 917 7 67  Leid
- 926
: 934 1 65 Es
: 935 2 67 ~
: 937 3 65 ist
: 943 4 64  vor
: 950 6 60 bei
- 958
: 965 2 60 Es
: 969 2 65  ist
: 976 2 64  vor
: 978 3 65 ~
* 982 34 64 bei
- 1018 1019
: 1030 2 60 Es
: 1033 2 64  ist
: 1037 3 64  vor
: 1040 2 65 ~
: 1045 2 64 bei
: 1047 2 65 ~
: 1049 17 64 ~
- 1068 1099
: 1119 1 60 Du
: 1120 3 62  willst
: 1125 1 60  hier
: 1127 3 62  weg
- 1132
: 1135 1 60 Du
: 1137 3 62  willst
: 1141 1 60  hier
* 1143 5 64  raus
- 1149
: 1151 1 59 Du
: 1153 2 59  willst
: 1157 2 59  die
: 1161 3 59  Zeit
: 1166 3 60  zu
: 1170 3 60 rück
- 1175
: 1183 1 60 Du
: 1185 2 62  at
: 1189 2 60 mest
: 1191 4 62  ein
- 1196
: 1198 2 60 Du
: 1200 3 62  at
: 1204 2 60 mest
: 1208 4 64  aus
- 1213
: 1214 3 60 Doch
: 1218 2 60  nichts
: 1222 2 60  ve
: 1226 4 64 rän
: 1232 5 65 dert
: 1239 4 64  sich
- 1245
: 1247 1 62 Wo
: 1248 2 62  ist
: 1251 2 60  die
: 1254 5 62  Nacht
- 1261
: 1265 1 62 Wo
: 1266 2 62  ist
: 1269 2 60  der
: 1271 2 62  Weg?
: 1273 5 64 ~
- 1280
: 1282 3 60 Wie
: 1286 10 62  weit
: 1297 3 60  wie
: 1301 6 62  weit
: 1310 3 64  noch?
- 1315
: 1319 5 62 Fragst
: 1326 5 64  mich
- 1333 1334
: 1343 1 59 Wo
: 1346 2 59  wir
: 1349 2 59  ge
: 1354 3 59 we
: 1358 3 60 sen
: 1362 5 60  sind
- 1369
: 1374 2 57 Wird
: 1378 1 57  al
: 1380 3 57 les
: 1384 4 59  an
: 1389 6 60 ders?
- 1397
: 1405 3 57 Wird
: 1409 1 57  al
: 1411 2 57 les
: 1415 5 59  an
: 1421 5 60 ders?
- 1428 1429
: 1437 3 57 Wird
: 1442 1 57  al
: 1445 2 57 les
: 1449 5 69  an
: 1454 24 67 ders?
- 1480
: 1482 6 71 Ja
* 1488 3 72  ich
: 1494 5 69  weiß
- 1500
: 1501 2 64 Es
: 1506 3 64  war
: 1509 1 64  'ne
: 1514 5 71  gei
: 1519 3 72 le
: 1525 6 67  Zeit
- 1533
: 1535 2 64 Uns
: 1537 3 64  war
: 1541 3 64  kein
: 1545 6 71  Weg
: 1551 5 72  zu
: 1558 4 67  weit
- 1564 1565
: 1581 8 60 Du
: 1589 6 62  fehlst
: 1598 6 64  hier
- 1606
: 1610 5 71 Ja
: 1615 3 72  ich
: 1622 4 69  weiß
- 1628
: 1630 2 64 Es
: 1633 2 64  war
: 1637 2 64  'ne
: 1641 5 71  gei
: 1647 5 72 le
: 1653 4 67  Zeit
- 1659
: 1666 3 62 Hey
: 1670 2 64  es
: 1674 3 71  tut
: 1679 4 72  mir
: 1685 7 67  Leid
- 1694
: 1702 1 65 Es
: 1703 2 67 ~
: 1705 3 65 ist
: 1711 4 64  vor
* 1718 6 60 bei
- 1726 1727
: 1738 6 71 Ja
: 1744 3 72  ich
: 1750 5 69  weiß
- 1756
: 1757 2 64 Es
: 1762 3 64  war
: 1765 1 64  'ne
: 1770 5 71  gei
: 1775 3 72 le
: 1781 6 67  Zeit
- 1789
: 1794 3 62 Hey
: 1797 4 64  es
: 1801 4 69  tut
: 1807 4 69  mir
: 1813 5 69  Leid
: 1818 5 67 ~
- 1825
: 1829 4 67 Es
: 1833 4 65  ist
: 1839 5 64  vor
: 1846 4 60 bei
- 1852 1871
: 1891 2 60 Die
: 1894 7 67  Lich
: 1901 5 65 ter
: 1906 3 64  sind
: 1910 4 64  aus
- 1915
: 1917 2 60 Es
: 1921 2 60  ist
: 1925 8 67  schwer
: 1933 5 65  zu
: 1938 2 64  ver
: 1942 7 64 stehen
- 1951
: 1957 4 60 Du
: 1962 3 64  siehst
: 1966 4 64  hil
: 1970 3 62 flos
: 1975 5 64  zu
- 1981
: 1982 4 64 Wie
: 1986 3 62  die
* 1991 6 67  Zei
: 1998 4 65 ger
: 2002 2 64  sich
: 2006 6 64  drehen
- 2014
: 2018 4 60 Du
: 2022 6 67  siehst
: 2029 4 65  dei
: 2033 3 64 nen
: 2038 7 64  Stern
- 2046
: 2046 4 64 Ihn
: 2050 3 62  kann
: 2054 6 64  nichts
- 2061
: 2062 3 64 Mehr
: 2066 3 62  zer
: 2071 5 64 stören
- 2078
: 2085 1 60 Denn
: 2087 1 60  du
: 2089 3 60  weißt
: 2093 3 60  dass
: 2098 3 60  es
* 2102 7 62  geil
: 2110 2 62  war
: 2112 4 64 ~
- 2118
: 2126 2 60 Dass
: 2130 2 60  es
: 2134 4 62  geil
: 2141 4 62  war
: 2145 9 64 ~
- 2156 2157
: 2182 1 60 Denn
: 2184 1 60  du
: 2186 2 60  weißt
: 2189 3 60  dass
: 2194 3 60  es
: 2198 5 62  geil
: 2206 2 62  war
: 2208 5 64 ~
- 2215 2230
: 2250 7 71 Ja
: 2257 3 72  ich
* 2263 5 69  weiß
- 2269
: 2270 2 64 Es
: 2275 3 64  war
: 2278 1 64  'ne
: 2283 5 71  gei
: 2288 3 72 le
: 2294 6 67  Zeit
- 2302
: 2304 2 64 Uns
: 2306 3 64  war
: 2310 3 64  kein
: 2314 6 71  Weg
: 2320 5 72  zu
: 2326 5 67  weit
- 2333 2334
: 2350 8 60 Du
: 2358 6 62  fehlst
: 2366 6 64  hier
- 2374
: 2379 5 71 Ja
: 2384 3 72  ich
: 2391 4 69  weiß
- 2397
: 2399 2 64 Es
: 2402 2 64  war
: 2406 2 64  'ne
: 2410 5 71  gei
: 2416 5 72 le
: 2422 4 67  Zeit
- 2428
: 2435 3 62 Hey
: 2439 2 64  es
: 2443 3 71  tut
: 2448 4 72  mir
* 2454 4 67  Leid
- 2460 2461
: 2471 1 65 Es
: 2472 2 67 ~
: 2474 3 65 ist
: 2480 4 64  vor
: 2487 3 60 bei
- 2492 2493
: 2506 6 71 Ja
: 2512 3 72  ich
: 2518 5 69  weiß
- 2524
: 2525 2 64 Es
: 2530 3 64  war
: 2533 1 64  'ne
: 2538 5 71  gei
: 2543 3 72 le
: 2549 6 67  Zeit
- 2557
: 2561 4 62 Hey
: 2565 3 64  es
: 2570 4 69  tut
: 2575 4 69  mir
: 2581 5 69  Leid
: 2586 5 67 ~
- 2593
: 2598 3 67 Es
: 2601 3 65  ist
: 2606 4 64  vor
: 2613 5 64 bei
: 2618 11 62 ~
- 2631 2675
: 2695 4 69 Es
* 2699 2 76  ist
: 2703 6 65  vor
: 2711 21 64 bei
- 2734 2735
: 2758 2 60 Es
: 2762 2 64  ist
: 2767 5 65  vor
: 2775 18 64 bei
- 2795 2796
: 2823 2 64 Es
: 2826 3 67  ist
: 2830 6 67  vor
: 2839 31 64 bei
- 2872 2873
: 2887 2 60 Es
: 2890 3 64  ist
: 2896 4 65  vor
: 2903 5 65 bei
: 2908 27 64 ~
E
