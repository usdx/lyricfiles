#TITLE:Satisfaction
#ARTIST:Benny Benassi
#MP3:Benny Benassi - Satisfaction.mp3
#VIDEO:Benny Benassi - Satisfaction.mp4
#COVER:Benny Benassi - Satisfaction.jpg
#BPM:260,33
#GAP:378,73
#ENCODING:UTF8
F 0 3 9 Push
: 7 3 8  me
- 16
: 19 3 11 And
: 24 3 11  then
: 28 1 10  just
: 32 2 9  touch
: 40 4 9  me
- 50
: 53 2 10 Till
: 56 2 10  I
: 60 3 10  can
: 65 2 8  get
: 72 6 8  my
- 85
: 88 1 9 sa
: 90 2 9 tis
: 96 2 7 fac
: 101 2 7 tion
- 113
: 128 3 10 Push
* 136 4 3  me
- 146
: 148 2 18 And
: 152 2 21  then
: 156 2 21  just
: 160 3 21  hurt
: 168 4 15  me
- 178
: 180 3 21 Till
: 185 2 16  I
: 188 3 15  can
: 193 3 17  get
: 199 5 16  my
- 213
: 216 2 21 sa
: 219 2 19 tis
: 224 4 22 fac
: 232 3 14 tion,
- 245
: 249 1 21 sa
: 251 2 19 tis
: 256 4 22 fac
: 265 3 14 tion
- 302
* 801 4 9 Push
* 808 4 8  me
- 818
: 820 2 9 And
: 825 2 9  then
: 829 2 10  just
: 833 3 9  touch
: 841 4 9  me
- 851
: 853 3 10 Till
: 857 3 10  I
: 861 3 10  can
: 865 3 8  get
: 874 4 8  my
- 886
: 889 2 9 sa
: 892 2 9 tis
: 897 3 7 fac
: 902 3 7 tion
- 915
: 930 3 10 Push
: 938 4 2  me
- 947
: 949 2 18 And
: 953 2 21  then
: 957 2 22  just
: 961 4 21  hurt
: 969 4 15  me
- 979
: 981 3 21 Till
: 986 2 16  I
: 989 3 15  can
: 993 3 19  get
: 1001 4 16  my
- 1014
: 1017 2 21 sa
: 1020 2 19 tis
: 1026 3 22 fac
: 1033 3 14 tion
- 1070
: 1312 4 9 Push
: 1319 4 8  me
- 1329
: 1332 2 11 And
: 1335 3 11  then
: 1340 2 10  just
: 1344 4 9  touch
: 1351 4 9  me
- 1361
: 1364 3 10 Till
: 1368 3 10  I
: 1372 3 10  can
: 1376 3 8  get
: 1384 4 8  my
- 1396
: 1399 2 9 sa
: 1402 2 9 tis
: 1407 4 7 fac
: 1413 3 7 tion
- 1426
: 1437 3 10 Push
: 1445 4 2  me
- 1455
: 1457 2 18 And
: 1461 2 21  then
: 1465 2 21  just
: 1469 3 21  hurt
: 1477 4 15  me
- 1487
: 1489 3 21 Till
: 1494 2 16  I
: 1497 3 15  can
: 1501 3 17  get
: 1508 4 16  my
- 1521
: 1524 2 21 sa
: 1528 1 19 tis
: 1533 3 22 fac
: 1540 3 14 tion
- 1553
: 1557 2 21 Sa
: 1560 2 19 tis
: 1566 3 22 fac
: 1572 3 14 tion,
- 1585
: 1589 2 21 sa
: 1592 2 19 tis
: 1598 3 22 fac
: 1604 3 14 tion,
- 1617
* 1621 2 21 sa
* 1624 2 19 tis
* 1630 3 22 fac
* 1636 3 14 tion
- 1648
: 1652 2 21 Sa
: 1655 1 19 tis
: 1661 3 22 fac
: 1668 2 14 tion,
- 1681
: 1685 2 21 sa
: 1688 1 19 tis
: 1694 3 22 fac
: 1700 3 14 tion
- 1737
: 2121 3 10 Push
: 2128 4 2  me
- 2138
: 2140 2 18 And
: 2144 3 21  then
: 2149 2 23  just
: 2153 4 21  hurt
: 2160 4 15  me
- 2170
: 2173 2 21 Till
: 2178 2 16  I
: 2181 3 15  can
: 2185 3 17  get
: 2192 4 16  my
- 2205
: 2209 2 21 sa
: 2212 2 19 tis
: 2217 3 22 fac
: 2224 3 14 tion
- 2237
: 2247 3 9 Push
: 2254 4 8  me
- 2264
: 2267 2 11 And
: 2270 2 11  then
: 2275 2 10  just
: 2279 4 9  touch
: 2286 4 9  me
- 2297
: 2300 3 10 Till
: 2305 2 10  I
: 2309 3 10  can
: 2313 3 8  get
: 2321 4 8  my
- 2333
* 2336 2 9 sa
* 2339 2 9 tis
* 2345 2 7 fac
* 2350 3 7 tion
E