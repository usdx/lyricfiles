#TITLE:LA - LE - LU
#ARTIST:Disney Junior
#MP3:Disney - LaLeLu.mp3
#VIDEO:Disney - LaLeLu.mp4
#COVER:Disney - LaLeLu [CO].jpg
#BPM:270,27
#GAP:9186,53
#MedleyStartBeat:96
#MedleyEndBeat:633
#ENCODING:CP1252
#PREVIEWSTART:14,515
#LANGUAGE:German
#GENRE:Volkslied
#EDITION:Fepo eigene
#YEAR:2011
#CREATOR:Fepo
: 0 6 1 Psst,
: 26 2 1  al
: 29 2 1 le
: 32 3 1  lei
: 36 2 1 se
: 40 6 1  sein
- 48
* 96 24 -4 La
* 132 8 1  le
* 144 10 5  lu,
- 156
: 168 4 3 Nur
: 174 3 1  der
: 180 3 0  Mann
: 186 3 -2  im
: 192 10 -4  Mond
: 216 12 3  schaut
: 240 7 6  zu,
- 249
: 264 4 5 Wenn
: 270 3 3  die
: 275 5 0  klei
: 282 3 -2 nen
: 288 18 -4  Ba
: 324 8 8 bys
: 336 8 8  schla
: 348 6 6 fen,
- 356
: 383 16 5 Drum
: 408 8 3  schlaf
: 420 7 1  auch
: 432 6 3  du.
- 440
* 481 31 -4 La
* 516 8 1  le
* 528 8 5  lu,
- 538
: 552 5 3 Vor
: 559 3 1  dem
: 564 3 0  Bet
: 569 3 -2 tchen
: 576 16 -4  steh'n
: 612 8 3  zwei
: 624 9 6  Schuh'
- 635
: 649 4 5 Und
: 656 3 3  die
: 661 3 0  sind
: 666 3 -2  ge
: 672 20 -4 nau
: 696 20 8  so
: 720 11 8  mü
: 734 7 6 de,
- 743
: 768 16 5 Geh'n
: 792 7 -4  jetzt
: 804 8 3  zur
: 816 11 1  Ruh'.
- 829
: 864 12 10 Da
: 888 7 3  kommt
: 898 5 5  auch
: 906 5 6  der
: 914 6 8  Sand
: 924 9 8 mann,
- 935
: 960 12 6 Leis'
: 984 6 0  tritt
: 994 4 1  er
: 1002 4 3  ins
: 1009 9 5  Haus,
- 1020
: 1056 10 6 Sucht
: 1080 5 0  aus
: 1090 5 1  sei
: 1098 3 3 nen
: 1104 6 5  Träu
: 1113 5 5 men
- 1120
: 1152 8 5 Dir
: 1166 6 3  den
: 1177 8 1  schöns
: 1188 6 -2 ten
: 1196 16 3  aus.
- 1214
* 1248 31 -4 La
* 1283 8 1  le
* 1295 8 5  lu,
- 1305
: 1319 4 3 Nur
: 1325 3 1  der
: 1331 3 0  Mann
: 1337 3 -2  im
: 1343 10 -4  Mond
: 1367 12 3  schaut
: 1391 7 6  zu,
- 1400
: 1416 4 5 Wenn
: 1422 3 3  die
: 1427 5 0  klei
: 1434 3 -2 nen
: 1440 18 -4  Ba
: 1466 16 8 bys
: 1488 8 8  schla
: 1500 6 6 fen,
- 1508
: 1535 16 5 Drum
: 1560 8 3  schlaf
: 1572 7 1  auch
: 1584 6 3  du.
- 1592
* 1634 31 -4 La
* 1669 8 1  le
* 1681 8 5  lu,
- 1691
: 1704 5 3 Vor
: 1711 3 1  dem
: 1716 3 0  Bet
: 1721 3 -2 tchen
: 1728 16 -4  steh'n
: 1752 16 3  zwei
: 1775 9 6  Schuh'
- 1786
: 1800 4 5 Und
: 1807 3 3  die
: 1812 3 0  sind
: 1817 3 -2  ge
: 1823 20 -4 nau
: 1847 18 8  so
: 1871 11 8  mü
: 1885 7 6 de,
- 1894
: 1921 16 5 Geh'n
: 1945 7 -4  jetzt
: 1957 8 3  zur
: 1969 11 1  Ruh'.
- 1982
: 2015 17 10 Sind
: 2042 5 3  al
: 2050 5 5 le
: 2058 3 6  die
: 2065 7 8  Ster
: 2074 6 8 ne
- 2082
: 2114 13 6 Am
: 2137 6 0  Him
: 2146 5 1 mel
: 2155 4 3  er
: 2161 10 5 wacht
- 2173
: 2208 16 6 Dann
: 2232 6 0  sing
: 2243 5 1  ich
: 2252 3 3  es
: 2257 2 5  ge
: 2260 2 3 ~r
: 2264 7 5 ne
- 2273
: 2304 8 5 Dies
: 2317 5 3  Lied
: 2326 7 1  Dir
: 2337 6 -2  zur
: 2347 10 3  Nacht
- 2359
* 2400 31 -4 La
* 2435 8 1  le
* 2447 8 5  lu,
- 2457
: 2472 4 3 Nur
: 2478 3 1  der
: 2484 3 0  Mann
: 2490 3 -2  im
: 2496 10 -4  Mond
: 2520 12 3  schaut
: 2544 7 6  zu,
- 2553
: 2567 4 5 Wenn
: 2573 3 3  die
: 2578 5 0  klei
: 2585 3 -2 nen
: 2591 18 -4  Ba
: 2615 15 8 bys
: 2638 9 8  schla
: 2651 6 6 fen,
- 2659
: 2687 16 5 Drum
: 2712 8 3  schlaf
: 2724 7 1  auch
: 2736 6 3  du.
- 2744
* 2784 31 -4 La
* 2819 8 1  le
* 2831 8 5  lu,
- 2841
: 2856 5 3 Vor
: 2863 3 1  dem
: 2868 3 0  Bet
: 2873 3 -2 tchen
: 2880 16 -4  steh'n
: 2904 16 3  zwei
: 2927 9 6  Schuh'
- 2938
: 2953 4 5 Und
: 2960 3 3  die
: 2965 3 0  sind
: 2970 3 -2  ge
: 2976 20 -4 nau
: 3000 20 8  so
: 3024 11 8  mü
: 3038 7 6 de,
- 3047
: 3073 16 5 Geh'n
: 3096 7 -4  jetzt
: 3108 8 3  zur
: 3120 11 1  Ruh'.
- 3133
: 3169 16 5 Geh'n
: 3194 7 -4  jetzt
: 3208 11 3  zur
: 3224 11 1  Ruh'.
E