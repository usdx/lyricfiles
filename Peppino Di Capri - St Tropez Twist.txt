#TITLE:St Tropez Twist
#ARTIST:Peppino Di Capri
#EDITION:Singstar - Italian Party
#LANGUAGE:Italian
#COVER:Peppino Di Capri - St Tropez Twist [CO].jpg
#BACKGROUND:Peppino Di Capri - St Tropez Twist [BG].jpg
#MP3:Peppino Di Capri - St Tropez Twist.mp3
#BPM:331,80
#GAP:5018,08
: 0 2 55 A
: 4 2 57  St
: 8 3 60  Tro
: 14 4 60 pez
- 20 21
: 44 3 55 La
: 48 3 60  lu
: 51 2 60 na
: 56 2 58  si
: 60 4 57  de
: 68 2 55 sta
* 72 4 57  con
: 80 6 55  te
- 88 112
: 132 3 55 E
: 136 3 57  bal
: 140 2 60 la~ il
: 146 3 60  twist
- 151 152
: 172 2 55 Con
: 177 2 60 tan
: 181 2 60 do
: 184 2 58  le
: 189 3 57  stel
: 197 2 55 le
: 200 5 57  nel
: 208 9 55  ciel
- 219 253
: 273 3 61 Ma
: 277 2 62  la
: 281 2 62  stel
: 285 2 62 la~ an
: 289 3 61 cor
: 293 2 62  piů
: 297 2 62  bel
: 301 2 62 la
- 304
: 305 3 59 Non
: 309 2 60  č~ in
* 313 2 60  cie
: 317 2 60 lo~ č
: 321 3 59  qui
: 325 2 60  vi
: 329 2 60 ci
: 333 2 60 na~ a
: 337 3 60  me
- 342 343
: 357 2 55 A
: 361 2 58  St
: 365 2 60  Tro
: 369 2 60 pez
- 373 374
: 400 3 61 Ma
: 404 2 62  la
: 408 2 62  stel
: 412 2 62 la~ an
: 416 3 61 cor
: 420 2 62  piů
* 424 2 62  bel
: 428 2 62 la
- 431
: 432 3 59 Non
: 436 2 60  č~ in
: 440 2 60  cie
: 444 2 60 lo~ č
: 448 3 59  qui
: 452 2 60  vi
: 456 2 60 ci
: 460 2 60 na~ a
: 464 3 60  me
- 469 470
: 484 2 55 A
: 488 2 58  St
: 492 2 60  Tro
: 496 2 60 pez
- 500 501
: 526 4 65 Twist,
: 534 4 65  twist
- 540
: 547 4 60 Tut
: 555 3 62 to~ il
: 560 3 65  mon
: 567 4 65 do
- 573 574
: 591 4 64 Twist,
: 599 4 64  twist
- 605
: 611 7 60 Sta~ im
: 620 2 62 paz
: 624 5 64 zen
: 631 5 64 do
- 638 639
: 654 5 65 So
: 662 4 65 gna
- 668
: 675 5 60 Vuol
: 683 3 62  tor
: 688 5 65 na
: 694 4 65 re
- 700 701
: 718 2 61 U
: 722 2 62 na
: 726 2 62  lun
: 730 2 62 ga
: 734 2 61  not
: 738 2 62 te~ an
: 742 2 62 co
: 746 2 62 ra
- 749
: 750 3 64 Mai
: 755 2 64  piů
: 757 2 62 ~
: 761 3 60  scor
: 766 3 59 da
: 771 2 57 re~ a
- 774
: 775 2 57 St
: 779 2 59  Tro
: 783 3 60 pez
- 788 789
: 812 3 55 La
: 816 2 60  gen
: 820 2 60 te
: 825 3 58  si
: 829 6 57  chie
: 836 2 55 de
: 840 5 57  per
: 848 8 55 ché
- 858 880
: 900 3 55 Tu
: 904 3 57  bal
: 908 2 60 li~ il
: 912 3 60  twist
- 917 918
: 940 2 55 Por
: 944 2 60 tan
: 948 2 60 do~ un
: 952 2 58  ve
* 956 5 57 sti
: 964 2 55 to~ in
: 968 6 57  la
: 976 6 55 mé
- 984 1018
: 1038 3 61 Vuoi
: 1042 2 62  sem
: 1046 2 62 bra
: 1050 2 62 re~ an
: 1055 3 61 cor
: 1059 2 62  piů
: 1063 2 62  bel
: 1067 2 62 la
- 1070
: 1071 3 59 Ma
: 1075 2 60  la
: 1079 2 60  mo
: 1083 2 60 da~ č
: 1087 3 59  sem
: 1091 2 60 pre
: 1095 2 60  quel
: 1099 2 60 la
: 1103 4 60  se
- 1109 1110
: 1122 3 55 Tu
* 1126 3 57  bal
: 1130 2 60 li~ il
: 1135 3 60  twist
- 1140 1141
: 1166 3 61 Vuoi
: 1170 2 62  sem
: 1174 2 62 bra
: 1178 2 62 re~ an
: 1183 3 61 cor
: 1187 2 62  piů
: 1191 2 62  bel
: 1195 2 62 la
- 1198
: 1199 3 59 Ma
: 1203 2 60  la
: 1207 2 60  mo
: 1211 2 60 da~ č
: 1215 3 59  sem
: 1219 2 60 pre
: 1223 2 60  quel
: 1227 2 60 la
* 1231 4 60  se
- 1237 1238
: 1250 3 55 Tu
: 1254 3 57  bal
: 1258 2 60 li~ il
: 1263 3 60  twist
- 1268
F 1275 8 67 Twist
- 1285 1792
: 1812 4 65 Twist,
: 1820 4 65  twist
- 1826
: 1832 4 60 Tut
: 1840 3 62 to~ il
: 1845 3 65  mon
: 1852 4 65 do
- 1858 1859
* 1876 4 64 Twist,
: 1884 4 64  twist
- 1890
: 1897 7 60 Sta~ im
: 1906 2 62 paz
: 1910 5 64 zen
: 1917 5 64 do
- 1924 1925
: 1940 5 65 So
: 1948 4 65 gna
- 1954
: 1961 5 60 Vuol
: 1969 3 62  tor
: 1974 5 65 na
: 1980 4 65 re
- 1986 1987
: 2004 2 61 U
: 2008 2 62 na
: 2012 2 62  lun
: 2016 2 62 ga
: 2020 2 61  not
: 2024 2 62 te~ an
: 2028 2 62 co
: 2032 2 62 ra
- 2035
: 2036 3 64 Mai
: 2042 2 64  piů
: 2044 2 62 ~
: 2048 3 60  scor
: 2053 3 59 da
: 2058 2 57 re~ a
- 2061
: 2062 2 57 St
: 2066 2 59  Tro
: 2071 3 60 pez
- 2076 2077
: 2100 3 55 La
: 2104 2 60  gen
: 2108 2 60 te
: 2113 3 58  si
: 2117 6 57  chie
: 2124 2 55 de
: 2128 5 57  per
* 2136 8 55 ché
- 2146 2168
: 2188 3 55 Tu
: 2192 3 57  bal
: 2196 2 60 li~ il
: 2200 3 60  twist
- 2205 2206
: 2228 2 55 Por
: 2232 2 60 tan
: 2236 2 60 do~ un
: 2240 2 58  ve
: 2244 5 57 sti
: 2252 2 55 to~ in
: 2256 6 57  la
: 2264 6 55 mé
- 2272 2308
* 2328 3 61 Vuoi
: 2332 2 62  sem
: 2336 2 62 bra
: 2340 2 62 re~ an
: 2345 3 61 cor
: 2349 2 62  piů
: 2353 2 62  bel
: 2357 2 62 la
- 2360
: 2361 3 59 Ma
: 2365 2 60  la
: 2369 2 60  mo
: 2373 2 60 da~ č
: 2377 3 59  sem
: 2381 2 60 pre
: 2385 2 60  quel
: 2389 2 60 la
: 2393 4 60  se
- 2399 2400
: 2412 3 55 Tu
: 2416 3 57  bal
: 2420 2 60 li~ il
: 2425 3 60  twist
- 2430 2439
: 2459 4 61 Bal
: 2464 7 62 la
: 2472 4 62 no
: 2478 4 61  can
: 2484 5 62 ta
: 2490 3 62 no
- 2494
: 2496 3 61 Sen
: 2501 4 62 ti
: 2507 4 62  che
: 2513 4 61  bri
: 2519 4 62 vi
: 2525 3 59 do
- 2529
: 2530 3 59 Que
: 2536 4 60 sta~ č
: 2542 4 60  la
: 2548 5 59  vi
: 2555 3 60 ta
- 2559
: 2559 3 60 Fan
* 2564 4 59 ta
: 2570 3 60 sti
: 2575 3 60 ca
: 2580 3 55  di
- 2584
: 2586 3 57 St
: 2592 3 60  Tro
: 2596 4 60 pez

