#TITLE:The Zephyr Song
#ARTIST:Red Hot Chili Peppers
#MP3:Red Hot Chili Peppers - The Zephyr Song.mp3
#VIDEO:Red Hot Chili Peppers - The Zephyr Song.mp4
#COVER:Red Hot Chili Peppers - The Zephyr Song.jpg
#BPM:312.03
#GAP:17492
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Rock
#YEAR:2002
#CREATOR:mustangfred and PatTwo
: 0 1 0 Can
: 2 1 0  I
: 6 2 0  get
: 10 2 0  your
: 15 3 -1  hand
: 21 3 0  to
: 26 5 -1  write
: 32 7 0  on
- 40
: 42 1 0 Just
: 44 1 0  a
: 46 4 0  piece
: 52 3 0  of
: 57 3 -1  leg
: 63 2 0  to
: 68 4 -1  bite
: 74 4 0  on?
- 83
: 85 1 0 What
: 87 1 0  a
: 89 2 0  night
: 95 1 0  to
: 101 3 -1  fly
: 106 2 0  my
: 111 4 -1  kite
: 117 2 0  on
- 121
: 123 2 0 Do
: 127 4 0  you
: 133 3 -1  wan
: 138 1 0 na
: 143 2 -1  flash
: 149 2 0  your
: 153 4 -1  light
: 158 5 0  on?
- 168
: 170 2 4 Take
: 173 1 4  a
: 176 2 4  look
: 181 1 4  it's
: 186 3 4  on
: 191 1 4  dis
* 198 3 4 play
* 203 15 2 ~
: 224 7 -1  for
: 234 10 0  you
- 252
: 255 1 4 Com
: 257 1 4 ing
: 259 4 4  down,
: 265 3 4  no
: 270 2 4  not
: 276 3 4  to
: 282 4 4 day
: 288 18 2 ~
- 316
: 340 1 0 Did
: 343 1 0  you
: 346 1 0  meet
: 350 2 0  your
: 355 3 -1  for
: 361 2 0 tune
: 367 4 -1  tell
: 372 8 0 er
- 381
: 383 1 0 Get
: 385 1 0  it
: 387 2 0  off
: 393 1 0  with
: 398 2 -1  no
: 403 1 0  pro
: 408 5 -1 pel
: 414 4 0 ler
- 424
: 426 1 0 Do
: 428 1 0  it
: 430 2 0  up
: 436 1 0  it’s
: 440 4 -1  on
: 446 1 0  with
: 451 4 -1  Stel
: 457 2 0 la
- 460
: 462 4 -1 What
: 468 2 0  a
: 472 4 -1  way
: 478 1 0  to
: 483 2 -1  fi
: 486 1 0 nal
: 488 1 0 ly
: 494 4 -1  smell
: 500 3 0  her
- 509
: 511 1 4 Pick
: 513 1 4 in’
: 515 2 4  up
: 521 1 4  but
: 526 1 4  not
: 531 1 4  too
* 537 5 4  strong
* 543 14 2 ~
: 563 9 -1  for
: 574 8 0  you
- 591
: 595 1 4 Take
: 597 1 4  a
: 601 2 4  piece
: 606 2 4  and
: 611 4 4  pass
: 616 2 3  it
: 619 2 4 ~
: 622 4 4  on
: 627 9 2 ~
- 677
: 728 5 2 Fly
: 735 7 1  a
: 744 9 2 way
* 755 2 2  on
* 758 8 4 ~
: 771 5 2  my
: 782 3 1  zeph
: 788 10 2 yr
- 809
: 813 2 2 I
: 819 14 1  feel
: 835 1 2  it
: 840 2 2  more
: 844 10 4 ~
: 857 2 2  than
: 862 8 1  ev
: 873 3 -1 er
: 877 7 -3 ~
- 893
: 897 5 2 And
: 904 13 1  in
: 919 1 2  this
: 926 3 2  per
: 930 6 4 ~
: 941 4 2 fect
* 950 6 1  weath
* 958 12 2 er
- 980
: 984 2 2 We’ll
: 989 14 1  find
: 1006 1 2  a
: 1010 3 2  place
: 1014 7 4 ~
: 1026 2 2  to
: 1031 7 1 geth
: 1041 5 -1 er
: 1047 7 -3 ~
- 1056
* 1058 37 -3 Fly
* 1096 3 -5 ~
* 1101 24 -5  on
- 1135
: 1142 37 -8 my
: 1182 2 -13 ~
: 1186 8 -12  wind
- 1204
: 1234 1 0 Re
: 1236 1 0 bel
: 1239 4 0  and
: 1244 3 0  a
: 1249 4 -1  lib
: 1255 3 0 er
: 1260 4 -1 a
: 1265 7 0 tor
- 1273
: 1275 3 0 Find
: 1279 1 0  a
: 1281 4 0  way
: 1287 2 0  to
: 1292 4 -1  be
: 1297 1 0  a
: 1302 5 -1  skat
: 1308 4 0 er
- 1317
: 1319 2 0 Rev
: 1322 1 0  it
: 1324 1 0  up
: 1329 3 0  to
: 1334 3 -1  lev
: 1340 1 0 i
: 1345 4 -1 tate
: 1350 1 0  her
- 1353
: 1355 1 0 Su
: 1360 2 0 per
: 1367 3 0  friend
: 1371 4 0 ly
: 1377 3 -1  a
: 1383 3 0 vi
: 1388 3 -1 a
: 1392 4 0 tor
- 1402
: 1404 2 4 Take
: 1407 1 4  a
: 1410 1 4  look
: 1414 2 4  it's
: 1419 4 4  on
: 1425 1 4  dis
: 1430 4 4 play
: 1436 16 2 ~
: 1457 8 -1  for
: 1467 11 0  you
- 1485
: 1488 1 4 Com
: 1490 1 4 ing
: 1493 3 4  down,
: 1498 2 4  no
: 1503 2 4  not
: 1509 2 4  to
: 1514 5 4 day
: 1521 30 0 ~
- 1571
: 1621 5 2 Fly
: 1628 8 1  a
* 1638 8 2 way
: 1648 3 2  on
: 1652 8 4 ~
: 1664 6 2  my
: 1674 4 1  zeph
: 1681 10 2 yr
- 1703
: 1707 2 2 I
: 1713 13 1  feel
: 1728 2 2  it
: 1734 13 4  more
: 1750 2 2  than
: 1755 8 1  ev
: 1766 3 -1 er
: 1771 7 -3 ~
- 1788
: 1792 4 2 And
: 1798 13 1  in
: 1814 1 2  this
: 1819 7 4  per
: 1827 3 2 ~
: 1835 2 2 fect
: 1844 5 1  weath
: 1851 10 2 er
- 1872
: 1876 3 2 We’ll
: 1883 13 1  find
: 1899 1 2  a
: 1904 2 2  place
: 1908 6 4 ~
: 1920 3 2  to
: 1925 8 1 geth
: 1936 4 -1 er
: 1942 7 -3 ~
- 1958
: 1962 3 -3 In
: 1967 1 -3  the
: 1970 6 -3  wa
: 1978 4 -3 ter
: 1984 3 -3  where
: 1989 1 -3  I
: 1994 2 -3  cen
: 1999 3 -3 ter
: 2005 6 -3  my
: 2013 1 -3  e
: 2015 2 -3 mo
: 2026 4 -10 tion
- 2040
: 2047 3 -3 All
: 2052 4 -3  the
: 2058 7 -3  world
: 2070 1 -3  can
: 2074 11 -1  pass
: 2091 2 1  me
: 2096 9 -3  by
: 2106 4 -5 ~
- 2120
: 2133 5 2 Fly
: 2140 6 1  a
: 2148 10 2 way
: 2160 2 2  on
* 2164 7 4 ~
: 2175 6 2  my
: 2186 2 1  zeph
: 2192 10 2 yr
- 2214
: 2218 1 2 We’ll
: 2223 10 2  find
: 2235 1 2  a
: 2244 2 2  place
: 2247 6 4 ~
: 2259 2 2  to
: 2265 7 6 geth
: 2275 3 4 er
: 2280 14 2 ~
- 2335
: 2637 2 4 Whoa
: 2642 3 4  whoa
: 2648 2 4  whoa
: 2653 3 4  whoa
: 2658 3 4  whoa
: 2664 5 4  whoa
: 2671 15 2 ~
- 2690
: 2692 8 -1 Do
: 2703 6 0  you
: 2710 3 -1 ~?
- 2720
: 2723 2 4 Yeah
: 2728 2 4  yeah
: 2733 2 4  yeah
: 2738 3 4  yeah
: 2743 3 4  yeah
: 2750 3 4  yeah
: 2754 12 2 ~
- 2786
: 2808 2 4 Whoa
: 2813 2 4  whoa
: 2818 3 4  whoa
: 2823 3 4  whoa
: 2829 3 4  whoa
: 2835 3 4  whoa
: 2840 16 2 ~
- 2858
: 2860 7 -1 Won't
: 2872 11 0  you?
- 2889
: 2892 3 4 Yeah
: 2898 3 4  yeah
: 2903 2 4  yeah
: 2908 3 4  yeah
: 2913 3 4  yeah
: 2919 2 4  yeah
: 2923 16 2 ~
- 2959
: 2983 5 2 Fly
: 2990 8 1  a
: 2999 10 2 way
: 3011 11 4  on
: 3025 7 2  my
: 3037 2 1  zeph
: 3043 9 2 yr
- 3064
: 3068 1 2 I
: 3074 13 1  feel
: 3089 1 2  it
: 3095 13 4  more
: 3111 1 2  than
: 3116 8 1  ev
: 3128 3 -1 er
: 3132 5 -3 ~
- 3149
: 3153 3 2 And
: 3158 11 1  in
: 3174 1 2  this
: 3180 2 2  per
: 3183 7 4 ~
: 3196 2 2 fect
: 3205 4 1  weath
: 3212 10 2 er
- 3232
: 3239 1 2 We’ll
: 3244 12 2  find
: 3258 2 2  a
: 3265 2 2  place
: 3268 8 4 ~
: 3281 3 2  to
: 3287 8 1 geth
: 3298 3 -1 er
: 3302 9 -3 ~
- 3319
: 3322 4 -3 In
: 3328 1 -3  the
: 3331 7 -3  wa
: 3339 2 -3 ter
: 3343 5 -3  where
: 3350 1 -3  I
: 3355 4 -3  cen
: 3361 2 -3 ter
: 3366 3 -3  my
: 3371 3 -3  e
: 3376 2 -3 mo
: 3387 2 -10 tion
- 3399
: 3408 4 -3 All
: 3414 2 -3  the
: 3419 7 -3  world
: 3430 1 -3  can
: 3436 11 -1  pass
: 3451 4 1  me
: 3457 11 -3  by
: 3469 11 -5 ~
- 3489
: 3493 5 2 Fly
: 3500 8 1  a
: 3510 8 2 way
: 3520 2 2  on
: 3524 8 4 ~
: 3535 6 2  my
: 3546 3 1  zeph
: 3553 11 2 yr
- 3575
: 3579 2 2 We’re
: 3584 14 1  gon
: 3600 3 2 na
: 3605 11 4  live
: 3620 4 2  for
: 3626 8 6 ev
: 3638 3 4 er
: 3643 158 2 ~
- 3842
: 3961 4 2 For
: 3967 8 6 ev
: 3978 4 4 er
: 3984 149 2 ~
E