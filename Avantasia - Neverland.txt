#ARTIST:Avantasia

#TITLE:Neverland

#MP3:Avantasia - Part II - 06 - Neverland.mp3

#COVER:avantasia_mnever.jpg
#BACKGROUND:Avantasia.jpg
#BPM:202,34

#GAP:33800

: 0 4 -1 Caught 
: 4 4 1 in 
: 8 2 3 Ne
: 10 4 1 ver
: 14 4 -1 land, 
: 20 4 -1 no 
: 24 2 3 pur
: 26 4 3 pose 
: 30 4 3 to 
: 34 4 4 be 
: 38 16 3 seen
- 56
: 64 4 -1 There's 
: 68 4 1 no 
: 72 4 3 point 
: 76 4 1 of 
: 80 4 -1 des
: 84 4 -2 ti
: 88 16 3 na
: 104 14 1 tion
- 118
: 120 2 1 Ne
: 122 4 1 ver
: 126 4 1 en
: 130 4 3 ding 
: 134 4 4 waste
: 138 6 3 lands
- 144
: 144 4 1 such 
: 148 4 3 a 
: 152 2 5 wick
: 154 4 5 ed 
: 158 4 5 o
: 162 4 6 dys
: 166 12 5 sey
- 184
: 184 6 1 Don't 
: 190 6 1 know 
: 196 4 3 what 
: 200 6 7 we 
: 206 6 6 have 
: 212 4 4 done 
* 216 16 6 wrong
- 248
: 256 3 0 Lead 
: 259 3 2 us 
: 262 4 4 back 
: 266 4 2 to 
: 270 5 0 Rome 
: 275 4 0 to 
: 279 2 7 fol
: 281 5 7 low 
: 286 4 7 you 
: 290 4 8 a
: 294 6 10 gain 
: 300 10 8 ~
- 319
: 319 3 11 Bring 
: 322 4 11 the 
: 327 3 11 po
: 331 3 7 wer 
: 335 3 6 get 
: 339 5 7 sal
: 344 8 9 va 
: 352 4 7 ~ 
: 356 4 5 ~ 
: 360 12 4 tion
- 375
: 375 3 6 I 
: 378 3 6 know 
: 381 5 6 less 
: 386 4 7 than 
: 390 4 6 all 
: 394 4 4 but 
: 398 6 2 more
- 404
: 404 4 3 than 
: 408 2 5 ma
: 410 5 5 ny 
: 415 3 5 who 
: 418 4 7 know 
: 422 14 5 less
- 437
: 441 5 7 I 
: 446 6 9 know 
: 452 4 7 it's 
: 456 6 5 kee
: 462 5 7 ping 
: 467 4 5 us 
* 471 23 12 strong
- 506
: 516 5 7 And 
: 521 6 9 while 
: 527 5 7 they 
: 532 4 5 are 
: 536 10 7 pul
: 546 5 9 ling 
: 551 13 7 strings
- 577
: 584 6 7 While 
: 590 5 5 they 
: 595 5 3 are 
: 600 6 5 in 
: 606 5 3 com
: 611 10 1 mend
- 633
: 643 5 3 They're 
: 648 6 1 hang
: 654 4 3 ing 
: 658 4 5 on 
: 662 14 7 strings 
: 676 4 8 and 
: 680 9 7 fate
- 689
: 692 4 8 They 
: 696 4 6 do 
: 708 4 8 com
: 712 4 6 mend 
: 724 4 8 in
: 728 4 9 to 
: 732 2 9 a
: 734 4 9 no
: 738 6 10 ther 
: 744 8 11 hand
- 764
: 768 4 16 Caught 
: 772 4 18 in 
: 776 2 19 Ne
: 778 5 18 ver
: 783 9 16 land
- 796
: 800 4 18 Heat 
: 804 4 19 and 
: 808 2 21 fi
: 810 4 21 re 
: 814 4 19 snow 
: 818 5 18 and 
: 823 6 16 ice
- 830
: 832 4 14 They 
: 836 4 16 call 
: 840 2 17 ne
: 842 4 16 ther
: 846 8 14 world
- 864
: 864 4 14 What 
: 868 4 15 we 
: 872 4 16 call 
: 876 4 17 a 
: 880 4 18 pa
: 884 2 19 ra
: 886 8 20 dise
- 890
: 896 4 16 Caught 
: 900 4 18 in 
: 904 2 19 Ne
: 906 5 18 ver
: 911 9 16 land
- 924
: 928 4 18 And 
: 932 4 19 their 
: 936 2 21 spi
: 938 4 21 rits 
: 942 4 19 can
: 946 5 18 not 
: 951 6 16 rise
- 957
: 960 4 14 From 
: 964 4 16 the 
: 968 2 17 ne
: 970 4 16 ther
: 974 8 14 world
- 990
: 992 4 14 They 
: 996 4 15 can't 
: 1000 4 16 see 
: 1004 4 17 the 
: 1008 4 18 pa
: 1012 2 19 ra
: 1014 6 20 dise
- 1074
: 1344 4 1 And 
: 1348 4 3 the 
: 1352 4 5 Ro
: 1356 2 3 man 
: 1358 5 1 whore
- 1364
: 1364 2 6 The 
: 1366 4 8 ma
: 1370 5 8 sters 
: 1375 3 8 and 
: 1378 6 11 the 
: 1384 2 13 sla 
: 1386 2 11 ~ 
: 1388 6 9 ves
- 1402
* 1408 4 13 Ra
: 1412 3 9 ging 
: 1415 4 8 on 
: 1419 4 7 with
: 1423 4 9 out 
: 1427 5 10 they 
: 1432 16 11 don't 
: 1448 14 13 know
- 1464
* 1464 8 15 Good 
* 1472 8 13 in
* 1480 8 10 ten
* 1488 8 7 tions 
: 1496 6 11 on 
: 1502 5 9 their 
: 1507 5 8 minds 
: 1512 10 7 ~
- 1522
: 1528 5 7 Can't 
: 1533 6 9 ask 
: 1539 4 7 for 
: 1543 6 5 why 
: 1549 5 7 they 
: 1554 4 5 bow 
* 1558 23 12 out
- 1580
: 1608 7 11 Lead 
: 1615 5 9 us 
: 1620 5 7 to 
: 1625 15 9 E
: 1640 14 3 den
- 1656
: 1672 6 7 Judge 
: 1678 6 5 those 
: 1684 4 3 who 
: 1688 6 5 bite 
: 1694 6 7 off 
: 1700 16 8 more
- 1716
: 1736 5 12 Then 
: 1741 5 12 they 
: 1746 4 12 can 
* 1750 14 14 chew 
: 1764 4 12 to 
: 1768 10 8 serve
- 1778
: 1780 4 8 With
: 1784 8 6 out 
: 1796 4 8 they 
: 1800 8 6 ask
- 1808
: 1812 4 8 Or 
: 1816 4 9 call 
: 1820 2 9 in 
: 1822 4 9 doubt 
: 1826 6 10 the 
: 1832 10 11 task
- 1842
: 1856 4 16 Caught 
: 1860 4 18 in 
: 1864 2 19 Ne
: 1866 5 18 ver
: 1871 9 16 land
- 1879
: 1888 4 18 Heat 
: 1892 4 19 and 
: 1896 2 21 fi
: 1898 4 21 re 
: 1902 4 19 snow 
: 1906 5 18 and 
: 1911 6 16 ice
- 1916
: 1920 4 14 They 
: 1924 4 16 call 
: 1928 2 17 ne
: 1930 4 16 ther
: 1934 8 14 world
- 1950
: 1952 4 14 What 
: 1956 4 15 we 
: 1960 4 16 call 
: 1964 4 17 a 
: 1968 4 18 pa
: 1972 2 19 ra
: 1974 8 20 dise
- 1982
: 1984 4 16 Caught 
: 1988 4 18 in 
: 1992 2 19 Ne
: 1994 5 18 ver
: 1999 9 16 land
- 2007
: 2016 4 18 And 
: 2020 4 19 their 
: 2024 2 21 spi
: 2026 4 21 rits 
: 2030 4 19 can
: 2034 5 18 not 
: 2039 6 16 rise
- 2045
: 2048 4 14 From 
: 2052 4 16 the 
: 2056 2 17 ne
: 2058 4 16 ther
: 2062 8 14 world
- 2070
: 2080 4 14 They 
: 2084 4 15 can't 
: 2088 4 16 see 
: 2092 4 17 the 
: 2096 4 18 pa
: 2100 2 19 ra
: 2102 6 20 dise
- 2110
: 2622 6 4 Caught 
: 2628 4 2 in 
: 2632 3 4 Ne
: 2635 3 2 ver
: 2638 10 0 land
- 2654
: 2656 4 3 In 
: 2660 4 5 the 
: 2664 4 6 place 
: 2668 4 5 of 
: 2672 3 6 ma
: 2675 3 5 ny 
: 2678 8 3 eyes
- 2685
: 2689 3 5 Make 
: 2692 4 3 it 
: 2696 3 5 be 
: 2699 4 3 what 
: 2703 9 1 they
- 2716
* 2720 5 10 Are 
: 2725 2 5 al
: 2727 4 5 lowed 
: 2731 4 3 to 
: 2735 2 4 re
: 2737 2 5 a
: 2739 7 6 lize
- 2748
: 2752 4 19 Caught 
: 2756 4 21 in 
: 2760 2 22 Ne
: 2762 5 21 ver
: 2767 9 19 land
- 2776
: 2785 4 21 Heat 
: 2789 4 22 and 
: 2793 2 24 fi
: 2795 4 24 re 
: 2799 4 22 snow 
: 2803 5 21 and 
: 2808 6 19 ice
- 2814
: 2816 4 17 They 
: 2820 4 19 call 
: 2824 2 20 ne
: 2826 4 19 ther
: 2830 8 17 world
- 2838
: 2848 4 17 What 
: 2852 4 18 we 
: 2856 4 19 call 
: 2860 4 20 a 
: 2864 4 21 pa
: 2868 2 22 ra
: 2870 8 23 dise
- 2878
: 2880 4 19 Caught 
: 2884 4 21 in 
: 2888 2 23 Ne
: 2890 5 22 ver
: 2895 9 20 land
- 2904
: 2912 4 21 And 
: 2916 4 22 their 
: 2920 2 24 spi
: 2922 4 24 rits 
: 2926 4 22 can
: 2930 5 21 not 
: 2935 6 19 rise
- 2941
: 2944 4 17 From 
: 2948 4 19 the 
: 2952 2 20 ne
: 2954 4 19 ther
: 2958 8 17 world
- 2966
: 2976 4 17 They 
: 2980 4 18 can't 
: 2984 4 19 see 
: 2988 4 20 the 
: 2992 4 21 pa
: 2996 2 22 ra
: 2998 6 23 dise
- 3004
: 3008 4 19 Caught 
: 3012 4 21 in 
: 3016 2 22 Ne
: 3018 4 21 ver
: 3022 9 19 land
- 3032
: 3040 4 21 Heat 
: 3044 4 22 and 
: 3048 2 24 fi
: 3050 4 24 re 
: 3054 4 22 snow 
: 3058 5 21 and 
: 3063 6 19 ice
- 3070
: 3072 4 17 They 
: 3076 4 19 call 
: 3080 2 20 ne
: 3082 4 19 ther
: 3086 8 17 world
- 3094
: 3104 4 17 What 
: 3108 4 18 we 
: 3112 4 19 call 
: 3116 4 20 a 
: 3120 4 21 pa
: 3124 2 22 ra
: 3126 8 23 dise
- 3134
: 3136 4 19 Caught 
: 3140 4 21 in 
: 3144 2 22 Ne
: 3146 5 21 ver
: 3151 9 19 land
- 3160
: 3168 4 21 And 
: 3172 4 22 their 
: 3176 2 24 spi
: 3178 4 24 rits 
: 3182 4 22 can
: 3186 5 21 not 
: 3191 6 19 rise
- 3197
: 3200 4 17 From 
: 3204 4 19 the 
: 3208 2 20 ne
: 3210 4 19 ther
: 3214 8 17 world
- 3222
: 3232 4 17 They 
: 3236 4 18 can't 
: 3240 4 19 see 
: 3244 4 20 the 
: 3248 4 21 pa
: 3252 2 22 ra
: 3254 6 23 dise
- 3260
: 3264 4 19 Caught 
: 3268 4 21 in 
: 3272 2 22 Ne
: 3274 5 21 ver
: 3279 9 19 land
- 3288
: 3297 4 21 Heat 
: 3301 4 22 and 
: 3305 2 24 fi
: 3307 4 24 re 
: 3311 4 22 snow 
: 3315 5 21 and 
: 3320 6 19 ice

