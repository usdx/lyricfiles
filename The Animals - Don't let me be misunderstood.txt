#TITLE:Don't let me be misunderstood
#ARTIST:The Animals
#EDITION:UltraStar 60s
#MP3:The Animals - Don't let me be misunderstood.mp3
#COVER:The Animals - Don't let me be misunderstood [CO].jpg
#VIDEO:The Animals - Don't let me be misunderstood.avi
#VIDEOGAP:-0,19
#BPM:110
#GAP:9300
: 0 6 59 Baby,
: 10 1 59  do
: 11 1 61  you
: 12 2 62  und
: 14 1 61 er
: 15 5 61 stand
: 20 2 59  me
: 22 5 57  now,
- 31
: 34 3 54 Some
: 37 3 55 times
: 40 2 55  I
: 42 2 55  feel
: 44 1 55  a
: 45 1 54  litt
: 46 1 54 le
: 47 4 54  mad.
- 57
: 63 2 54 But,
: 65 1 58  don't
: 66 2 59  you
: 68 2 59  know
: 70 2 59  that
: 72 1 59  no
: 73 1 59  one
- 74
: 74 1 59 a
: 75 3 59 live
: 78 2 59  can
: 80 7 64  al
: 87 2 64 ways
: 89 1 64  be
: 90 1 61  an
: 91 2 59  ang
: 93 3 57 el.
- 97
: 100 2 55 When
: 102 2 55  things
: 104 2 55  go
: 106 3 55  wrong
: 109 2 55  I
: 111 2 55  feel
: 113 2 55  real
: 115 3 54  bad.
- 120
: 128 4 66 I'm
: 132 2 66  just
: 134 2 66  a
: 136 5 66  soul
: 141 2 66  whose
: 143 2 66  in
: 145 2 66 tent
: 147 2 64 ions
: 149 2 62  are
: 151 5 66  good,
- 158
: 162 2 66 Oh
: 164 3 66  Lord,
: 168 2 64  please
: 170 2 64  don't
: 172 2 64  let
: 174 2 66  me
: 176 2 64  be
- 178
: 178 2 62 mis
: 180 2 59 und
: 182 2 57 er
: 184 4 59 stood.
- 210
: 248 3 59 Bab
: 251 3 59 y,
: 258 3 59  some
: 261 3 59 times
: 264 2 59  I'm
: 266 3 61  so
: 269 3 61  care
: 272 4 61 free,
- 279
: 283 1 55 With
: 284 1 55  a
: 285 5 55  joy
: 290 2 55  that's
: 292 2 55  hard
: 294 1 54  to
: 295 4 54  hide.
- 302
: 310 2 54 And
: 312 2 59  some
: 314 2 59 times
: 316 1 59  it
: 317 3 59  seems
: 320 2 59  that,
- 323
: 325 2 64 all
: 327 1 64  I
: 328 2 64  have
: 330 1 64  to
: 331 2 64  do
: 333 2 64  is
: 335 3 61  worr
: 338 2 59 y
- 342
: 346 2 55 And
: 348 1 55  then
: 349 2 55  you're
: 351 4 59  bound
: 355 1 55  to
: 356 2 55  see
: 358 1 55  my
: 359 1 57  oth
: 360 1 54 er
: 361 4 54  side.
- 370
: 376 4 66 I'm
: 380 2 66  just
: 382 2 66  a
: 384 4 66  soul
: 388 2 66  whose
: 390 2 66  in
: 392 2 66 tent
: 394 2 64 ions
: 396 2 62  are
: 398 5 66  good,
- 405
: 409 2 66 Oh
: 411 3 66  Lord,
: 415 3 64  please
: 418 2 64  don't
: 420 1 64  let
: 421 2 66  me
: 423 2 64  be
- 426
: 426 3 62  mis
: 429 1 59 und
: 430 2 57 er
: 432 3 59 stood.
- 439
: 449 2 59 If
: 451 2 59  I
: 453 2 59  seem
: 455 3 61  edg
: 458 3 61 y,
- 460
: 461 2 57 I
: 463 3 59  want
: 466 1 59  you
: 467 1 59  to
: 468 4 61  know,
- 476
: 480 2 59 That
: 482 1 59  I
: 483 1 59  nev
: 484 2 59 er
: 486 2 61  mean
: 488 2 61  to
: 490 2 62  take
: 492 2 64  it
: 494 2 66  out
: 496 2 66  on
: 498 5 66  you.
- 505
: 511 2 59 Life
: 513 2 59  has
: 515 2 59  its
: 517 2 61  prob
: 519 3 61 lems,
- 522
: 523 2 57 And
: 525 2 59  I
: 527 2 59  get
: 529 1 59  my
: 530 5 61  share,
- 540
: 540 2 61 And
: 542 2 59  that's
: 544 2 59  one
: 546 2 59  thing
: 548 3 59  I
: 551 3 61  nev
: 554 2 62 er
: 556 2 64  mean
: 558 1 66  to
: 559 3 66  do,
- 561
: 564 2 54 'Cause
: 566 1 54  I
: 567 3 54  love
: 570 1 59  you,
- 568
: 572 5 59 Oh,
: 581 2 61  Oh,
: 583 3 62  oh,
: 586 3 64  oh, bab
: 589 2 64 y
: 591 1 64  don't
: 592 1 64  you
: 593 2 64  know
: 595 1 61  I'm
: 596 3 59  hu
: 599 4 57 man.
- 607
: 607 1 57 I
: 608 2 55  have
: 610 5 55  thoughts
: 616 2 55  like
: 618 1 55  an
: 619 1 57 y
: 620 1 57  oth
: 621 1 54 er
: 622 4 54  one.
- 629
: 635 2 59 Some
: 637 3 59 times
: 640 1 59  I
: 641 2 59  find
: 643 2 59  my
: 645 3 59 self,
: 651 6 64  Lord,
: 657 2 64  re
: 659 4 61 grett
: 663 3 59 ing,
- 666
: 669 2 55 Some
: 671 4 55  foolish
: 675 3 55  thing
: 678 1 55  some
: 679 2 55  little
: 681 2 57  simp
: 683 2 57 le
: 685 1 54  thing
: 686 2 54 I've
: 688 6 54  done.
- 695
: 699 3 66 I'm
: 702 3 66  just
: 705 1 66  a
: 706 4 66  soul
: 710 2 66  whose
: 712 2 66  in
: 714 2 66 tent
: 716 2 64 ions
: 718 2 62  are
: 720 10 66  good,
- 729
: 731 2 66 Oh
: 733 3 66  Lord,
: 737 3 64  please
: 740 2 64  don't
: 742 1 64  let
: 743 2 66  me
: 745 2 64  be
- 747
: 747 2 62 mis
: 749 2 59 und
: 751 2 57 er
: 753 4 59 stood.
- 773
: 785 4 66 I'm
: 789 2 66  just
: 791 1 66  a
: 792 5 66  soul
: 797 2 66  whose
: 799 2 66  in
: 801 2 66 tent
: 803 2 64 ions
: 805 2 62  are
: 807 9 66  good,
- 816
: 817 3 66 Oh
: 820 3 66  Lord,
: 824 2 64  please
: 826 2 64  don't
: 828 2 64  let
: 830 2 66  me
: 832 2 64  be
- 833
: 833 3 62 mis
: 836 2 59 und
: 838 2 57 er
: 840 3 59 stood.
- 852
: 872 4 66 I'm
: 876 2 66  just
: 878 1 66  a
: 879 4 66  soul
: 883 2 66  whose
: 885 2 66  in
: 887 2 66 tent
: 889 2 64 ions
: 891 2 62  are
: 893 10 66  good,
- 903
: 904 2 66 Oh
: 906 4 66  Lord,
: 911 2 64  please
: 913 2 64  don't
: 915 2 64  let
: 917 2 66  me
: 919 2 64  be
- 921
: 921 2 62 mis
: 923 2 59 und
: 925 1 57 er
: 926 5 59 stood.
E
