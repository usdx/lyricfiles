#TITLE:Looking for Freedom
#ARTIST:David Hasselhoff
#MP3:David Hasselhoff - Looking for Freedom.mp3
#VIDEO:David Hasselhoff - Looking for Freedom [VD#-4].avi
#COVER:David Hasselhoff - Looking for Freedom [CO].jpg
#BACKGROUND:David Hasselhoff - Looking for Freedom [BG].jpg
#BPM:249,68
#GAP:4250
#VIDEOGAP:-4
#ENCODING:UTF8
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
: 0 2 -5 One
: 4 3 7  mor
: 8 2 7 ning
: 12 2 7  in
: 16 6 7  june
- 23
: 25 3 7 Some
: 30 3 7  twen
: 34 2 7 ty
: 38 6 4  years
: 47 2 2  a
: 51 5 0 go
- 58
: 72 4 -3 I
: 78 3 0  was
: 83 4 0  born
: 88 3 0  a
: 93 3 2  rich
: 99 6 0  man's
: 109 5 0  son
- 116
: 140 2 -5 I
: 145 2 7  had
: 148 3 7  e
: 152 2 7 very
: 156 5 7 thing
- 163
: 170 2 7 That
: 175 1 4  mo
: 177 2 2 ney
: 181 2 0  could
: 185 6 0  buy
- 193
: 206 2 -5 But
: 210 3 -3  free
: 215 4 0 dom
- 221
: 232 4 4 I
: 237 3 2 ~
: 243 5 0  ha
: 249 2 -3 ~d
: 254 9 0  none
- 265
* 294 3 7 I've
: 299 2 7  been
: 304 2 4  loo
: 308 2 4 kin'
: 312 3 2  fo
: 316 2 0 ~r
: 321 9 0  free
: 333 4 0 dom
- 339
: 362 3 0 I've
: 366 2 0  been
: 370 3 0  loo
: 374 2 0 kin'
: 378 3 2  so
: 382 2 0 ~
: 386 12 4  long
- 400
: 427 2 7 I've
: 431 2 7  been
: 435 2 4  loo
: 439 2 4 kin'
: 443 3 2  fo
: 447 3 0 ~r
: 452 9 0  free
: 464 3 0 dom
- 469
: 492 2 4 Still
: 495 2 4  the
: 499 5 4  search
: 507 3 2  goe
: 511 2 0 ~s
: 516 9 0  on
- 527
: 556 3 7 I've
: 560 2 7  been
: 564 3 4  loo
: 568 2 4 kin'
: 572 3 2  fo
: 576 2 0 ~r
: 580 9 0  free
: 592 5 0 dom
- 599
: 620 3 0 Since
: 624 2 0  I
: 628 3 0  left
: 632 3 0  my
: 636 3 2  ho
: 640 2 0 ~me
: 645 1 4  to
: 647 1 2 ~
: 649 8 4 ~wn
- 659
: 685 3 7 I've
: 689 2 7  been
: 693 3 4  loo
: 697 2 4 kin'
: 701 3 2  fo
: 705 2 0 ~r
: 710 8 0  free
: 721 4 0 dom
- 727
: 749 3 4 Still
: 753 2 4  it
: 757 6 4  can't
: 765 3 2  be
: 769 2 0 ~
* 773 11 0  found
- 786
: 833 3 -5 I
: 837 3 -3  hea
: 841 2 0 ded
: 845 3 0  down
: 849 2 -3  the
: 853 5 0  track
- 860
: 865 2 0 My
: 869 3 4  bag
: 873 2 4 gage
: 877 2 2  on
: 881 2 4  my
: 885 3 2  ba
: 889 3 0 ~ck
- 894
: 898 2 -5 I
: 902 2 -3  left
: 906 2 0  the
: 910 3 0  ci
: 914 2 0 ty
: 918 4 2  far
: 926 2 0  be
: 930 7 0 hind
- 939
: 965 3 -3 Wal
: 969 2 0 kin'
: 973 3 0  down
: 977 2 -3  that
: 982 6 0  road
- 990
: 994 5 4 With
: 1002 2 4  my
: 1006 3 2  hea
: 1010 2 4 vy
: 1014 3 2  loa
: 1018 3 0 ~d
- 1023
: 1030 2 -3 Tryin'
: 1034 2 0  to
: 1038 2 0  find
: 1042 2 0  some
: 1047 5 2  peace
: 1055 2 0  of
: 1059 7 0  mind
- 1068
: 1102 3 7 Fa
: 1106 2 7 ther
: 1110 4 7  said
: 1118 2 7  "You'll
: 1122 2 7  be
: 1126 3 4  so
: 1130 2 2 ~
: 1134 2 0 rry
: 1138 4 0  son
- 1144
: 1159 2 -3 If
: 1162 3 0  you
: 1166 3 0  leave
: 1170 2 -3  your
: 1174 5 0  home
: 1182 2 0  this
: 1186 3 4  wa
* 1190 4 7 ~y
- 1196
: 1218 2 -5 And
: 1222 3 -3  when
: 1226 3 0  you
: 1230 3 0  re
: 1234 2 -3 a
: 1238 7 0 lize
- 1247
: 1250 2 0 The
: 1254 3 4  free
: 1258 2 4 dom
: 1262 3 2  mo
: 1266 2 4 ney
: 1270 3 2  bu
: 1274 4 0 ~ys
- 1280
: 1286 2 -3 You'll
: 1290 3 0  come
: 1294 3 0  run
: 1298 2 -3 ning
: 1302 19 0  home
- 1323
: 1334 3 4 So
: 1338 1 2 ~
: 1340 1 0 ~
: 1342 3 0 ~me
: 1350 15 0  day!"
- 1367
* 1391 2 7 I've
: 1395 2 7  been
: 1399 2 4  loo
: 1403 2 4 kin'
: 1407 3 2  fo
: 1411 2 0 ~r
: 1416 9 0  free
: 1428 3 0 dom
- 1433
: 1456 2 0 I've
: 1460 2 0  been
: 1464 2 0  loo
: 1468 2 0 kin'
: 1472 3 2  so
: 1476 2 0 ~
: 1480 11 4  long
- 1493
: 1520 2 7 I've
: 1524 2 7  been
: 1528 3 4  loo
: 1532 2 4 kin'
: 1536 3 2  fo
: 1540 2 0 ~r
: 1544 9 0  free
: 1556 4 0 dom
- 1562
: 1584 2 4 Still
: 1588 2 4  the
: 1592 5 4  search
: 1600 3 2  goe
: 1604 2 0 ~s
: 1609 9 0  on
- 1620
: 1649 2 7 I've
: 1653 2 7  been
: 1657 3 4  loo
: 1661 2 4 kin'
: 1665 3 2  fo
: 1669 2 0 ~r
: 1673 9 0  free
: 1685 4 0 dom
- 1691
: 1713 3 0 Since
: 1717 2 0  I
: 1721 2 0  left
: 1725 2 0  my
: 1729 3 2  ho
: 1733 2 0 ~me
: 1737 1 4  to
: 1739 1 2 ~
: 1741 8 4 ~wn
- 1751
: 1777 3 7 I've
: 1781 2 7  been
: 1785 3 4  loo
: 1789 2 4 kin'
: 1793 3 2  fo
: 1797 2 0 ~r
: 1801 8 0  free
: 1813 4 0 dom
- 1819
: 1841 3 4 Still
: 1845 2 4  it
: 1849 5 4  can't
: 1857 3 2  be
: 1861 2 0 ~
: 1865 10 0  found
- 1877
: 2155 2 -5 I
: 2159 2 -3  paid
: 2163 2 0  a
: 2167 3 0  lot
: 2171 2 -3 ta
: 2175 6 0  dues,
- 2183
: 2187 2 0 Had
: 2191 3 4  plen
: 2195 2 4 ty
: 2199 2 4  to
: 2203 3 2  lo
: 2207 5 0 ~se
- 2214
: 2223 3 -3 Tra
: 2227 2 0 vel
: 2231 2 0 ling
: 2235 2 -3  a
: 2239 5 0 cross
: 2247 2 2  the
: 2251 8 0  land
- 2261
: 2287 2 -3 Worked
: 2291 3 0  on
: 2295 2 -3  a
: 2299 5 0  farm,
- 2306
: 2311 2 0 Got
: 2315 2 0  some
: 2319 2 2  mus
: 2323 2 4 cle
: 2327 2 2  in
: 2331 2 4  my
: 2335 3 2  a
: 2339 2 0 ~rm
- 2343
: 2347 2 -3 But
: 2351 2 -3  still
: 2355 3 0  I'm
: 2359 3 0  not
: 2363 2 -3  a
: 2367 3 2  se
: 2371 2 0 ~lf-
: 2375 2 0 ma
: 2378 2 -3 ~de
: 2383 8 0  man
- 2393
: 2410 2 -5 I'll
: 2414 3 7  be
: 2418 2 7  on
: 2422 3 7  the
: 2426 3 7  run
- 2431
: 2434 2 7 For
: 2438 3 7  ma
: 2442 2 7 ny
: 2446 3 4  yea
: 2450 2 2 ~rs
: 2454 2 0  to
: 2458 3 0  come
- 2463
: 2478 2 -3 I'll
: 2482 2 0  be
: 2486 2 0  sear
: 2490 2 -3 ching
: 2494 4 0  door
: 2501 2 -3  to
: 2505 2 4  doo
: 2508 1 2 ~
: 2510 2 4 ~
* 2513 2 7 ~r
- 2517
: 2537 2 -5 And
: 2541 3 -3  gi
: 2545 2 0 ven
: 2549 2 0  some
: 2553 5 0  time,
- 2560
: 2569 2 0 Some
: 2573 3 4  day
: 2577 2 4  I'm
: 2581 2 2  gon
: 2584 2 4 na
: 2589 2 2  fi
: 2592 3 0 ~nd
- 2597
: 2600 2 -3 The
: 2604 3 2  free
: 2608 2 0 ~
: 2612 17 0 dom
- 2631
: 2644 2 5  I've
: 2648 2 5  been
: 2652 6 5  sear
* 2660 5 7 chin
: 2667 3 4  fo
: 2671 1 2 ~
: 2673 16 0 ~r
- 2691
: 2707 2 7 I've
: 2711 2 7  been
: 2715 3 4  loo
: 2719 2 4 kin'
: 2723 3 2  fo
: 2727 2 0 ~r
: 2732 8 0  free
: 2744 4 0 dom
- 2750
: 2773 2 0 I've
: 2776 2 0  been
: 2780 3 0  loo
: 2784 2 0 kin'
: 2788 3 2  fo
: 2792 2 0 ~r
: 2796 2 4  lo
: 2799 1 2 ~
: 2801 7 4 ~ng
- 2810
: 2836 2 7 I've
: 2840 2 7  been
: 2844 3 4  loo
: 2848 2 4 kin'
: 2852 3 2  fo
: 2856 2 0 ~r
: 2860 9 0  free
: 2872 4 0 dom
- 2878
: 2900 2 4 Still
: 2904 2 4  the
: 2908 5 4  search
: 2916 3 2  goe
: 2920 2 0 ~s
: 2924 8 0  on
- 2934
: 2964 2 7 I've
: 2968 2 7  been
: 2972 3 4  loo
: 2976 2 4 kin'
: 2980 3 2  fo
: 2984 2 0 ~r
: 2988 9 0  free
: 3001 4 0 dom
- 3007
: 3027 3 0 Since
: 3031 2 0  I
: 3035 2 0  left
: 3039 3 0  my
: 3044 3 2  ho
: 3048 2 0 ~me
: 3052 1 4  to
: 3054 1 2 ~
: 3056 8 4 ~wn
- 3066
: 3091 2 7 I've
: 3095 2 7  been
: 3099 3 4  loo
: 3103 2 4 kin'
: 3107 3 2  fo
: 3111 2 0 ~r
: 3116 8 0  free
: 3127 4 0 dom
- 3133
: 3155 3 4 Still
: 3159 2 4  it
: 3163 5 4  can't
: 3171 3 2  be
: 3175 2 0 ~
: 3179 9 0  found
- 3190
: 3218 3 7 I've
: 3222 2 7  been
: 3226 3 7  loo
: 3230 2 7 kin'
: 3234 3 9  fo
: 3238 2 7 ~r
* 3242 10 7  free
: 3255 2 4 do
: 3258 2 0 ~m
- 3262
: 3282 2 0 I've
: 3286 2 0  been
: 3290 3 0  loo
: 3294 2 0 kin'
: 3298 3 2  so
: 3302 2 0 ~
: 3306 1 4  lo
: 3308 1 2 ~
: 3310 5 4 ~ng
- 3317
: 3346 3 7 I've
: 3350 2 7  been
: 3354 3 4  loo
: 3358 2 4 kin'
: 3362 3 2  fo
: 3366 2 0 ~r
: 3370 9 0  free
: 3383 4 0 dom
- 3389
: 3409 3 4 Still
: 3413 2 4  the
: 3417 5 4  search
: 3425 3 2  goe
: 3429 2 0 ~s
: 3433 9 0  on
- 3444
: 3471 3 7 I've
: 3475 2 7  been
: 3479 3 4  loo
: 3483 2 4 kin'
: 3487 3 2  fo
: 3491 2 0 ~r
: 3496 8 0  free
: 3508 4 0 dom
- 3514
: 3535 3 0 Since
: 3539 2 0  I
: 3543 2 0  left
: 3547 2 0  my
: 3551 3 2  ho
: 3555 2 0 ~me
: 3559 1 4  to
: 3561 1 2 ~
: 3563 8 4 ~wn
- 3573
: 3598 3 7 I've
: 3602 2 7  been
: 3606 3 4  loo
: 3610 2 4 kin'
: 3614 3 2  fo
: 3618 2 0 ~r
: 3623 8 0  free
: 3635 4 0 dom
- 3641
: 3662 3 4 Still
: 3666 2 4  it
: 3670 5 4  can't
: 3678 3 2  be
: 3682 2 0 ~
: 3686 8 0  found
E