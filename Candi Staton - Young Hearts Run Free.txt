#TITLE:Young Hearts Run Free
#ARTIST:Candi Staton
#MP3:Candi Staton - Young Hearts Run Free.mp3
#VIDEO:Candi Staton - Young Hearts Run Free [VD#0].avi
#COVER:Candi Staton - Young Hearts Run Free [CO].jpg
#BPM:231
#GAP:9285,71
#VIDEOGAP:0
#ENCODING:UTF8
#LANGUAGE:Englisch
#GENRE:Pop
#EDITION:SingStar Anthems
#YEAR:1976
: 0 3 60 What's
: 4 1 60  the
: 6 2 60  sense
: 10 2 60  in
: 14 5 62  shar
: 19 3 62 ing
- 24
: 28 3 62 This
: 33 3 64  one
: 36 2 64  and
: 38 2 64  on
: 40 3 62 ~
: 43 4 60 ly
: 47 6 62  life
- 55
: 65 4 60 End
: 69 2 60 ing
: 71 2 60  up
: 79 3 62  just
: 85 2 62  an
: 87 4 62 oth
: 91 3 60 er
- 95
: 95 4 64 Lost
: 101 4 62  and
: 105 4 62  lone
: 109 4 60 ly
: 113 5 62  wife
- 120
: 128 4 65 You
: 133 4 65  count
: 137 2 65  up
: 141 4 65  the
: 145 2 67  years
: 147 2 65 ~
: 149 1 62 ~
- 152
: 157 3 65 And
* 161 5 69  they
: 167 4 70  will
: 172 3 69  be
: 178 2 69  filled
: 180 1 67 ~
: 181 3 65 ~
- 185
: 185 3 65 With
: 192 9 69  tears
: 202 2 65 ~
: 204 2 62 ~
: 206 7 65 ~
- 215
: 259 4 60 Love
: 264 3 60  on
: 267 3 60 ly
: 271 3 62  breaks
: 277 2 62  up
: 279 2 60 ~
- 283
: 286 1 60 To
: 290 5 64  start
: 295 4 64  o
: 299 2 62 ver
: 301 2 60  a
: 304 2 60 gain
: 306 7 62 ~
- 315
: 323 6 60 You'll
: 330 2 60  get
: 334 2 60  the
: 336 4 62  bab
: 340 3 62 ies
: 343 2 60 ~
- 347
: 350 1 60 But
: 353 4 64  you
: 357 3 64  won't
: 363 2 64  have
: 365 2 62 ~
: 368 2 60 your
: 370 2 62 ~
: 373 2 62 man
: 375 2 60 ~
- 379
: 387 5 65 While
: 393 3 65  he
: 397 2 65  is
: 400 4 67  bu
: 406 4 67 sy
: 410 4 67  lov
: 414 3 65 ing
- 418
* 419 5 69 Ev
: 426 4 70 er
: 430 3 69 y
: 434 2 67  wo
: 436 2 65 ~
: 438 3 65 man
- 442
: 444 3 65 That
: 447 2 65  he
: 450 5 69  can
: 455 1 67 ~
: 456 8 69 ~
- 466
: 479 3 65 Uh
: 483 4 65  huh
- 489
: 515 3 65 Say
: 518 4 65  I'm
: 522 4 67  gon
: 526 3 69 na
: 529 3 69  leave
: 532 2 67 ~
: 534 2 65 ~
- 538
: 542 1 72 A
: 545 4 72  hun
: 549 3 72 dred
: 553 3 72  times
: 559 1 69  a
: 561 6 69  day
- 569
: 575 2 62 It's
: 579 3 65  eas
: 583 1 65 ier
: 585 5 67  said
: 591 3 69  than
: 594 2 67  done
: 596 4 65 ~
- 602
: 607 2 60 When
: 609 1 62  you
: 611 2 65  just
: 615 3 67  can't
: 619 5 69  break
: 624 3 72  a
* 627 5 74 way
- 634
: 664 6 72 Ooh
: 670 4 70 ~
: 676 4 69 young
: 682 10 67  hearts
- 694
: 708 4 69 Run
: 714 9 67  free
- 725
: 738 1 60 And
: 740 5 69  nev
: 746 3 67 er
: 751 4 69  be
: 756 3 67  hung
: 759 2 65 ~
: 761 3 65 up
- 766
: 773 5 65 Hung
: 778 3 64  up
: 783 3 65  like
: 789 2 64  my
: 791 3 62 ~
: 795 6 62 man
: 801 5 62  and
: 806 2 62  me
: 808 1 60 ~
: 809 2 62 ~
: 811 8 60 ~
- 821
: 859 2 60 My
: 861 3 62 ~
: 865 4 62 man
: 869 3 65  and
: 873 4 65  me
: 877 2 62 ~
: 879 2 60 ~
: 881 3 62 ~
- 886
: 922 5 72 Ooh
: 927 6 69 ~
: 933 5 69 young
* 939 10 67  hearts
- 951
: 963 2 62 To
: 965 4 69  your
: 971 3 67 self
: 976 4 69  be
: 981 3 67  true
: 984 4 65 ~
- 990
: 997 2 72 Don't
: 999 3 69 ~
: 1003 4 69 be
: 1008 3 69  no
: 1013 2 67  fool
: 1015 4 65 ~
: 1020 4 65 when
- 1026
: 1029 3 65 Love
: 1032 2 64 ~
: 1035 5 64 real
: 1040 3 65 ly
- 1044
: 1046 2 65 Don't
: 1048 2 62 ~
: 1050 3 60 ~
: 1054 6 62 love
: 1061 4 62  you
: 1066 2 62 ~
: 1068 11 60 ~
- 1081
: 1122 4 62 Don't
: 1128 5 65  love
: 1134 4 65  you
- 1140
: 1194 3 60 It's
: 1199 3 60  high
: 1204 2 60  time
: 1206 3 62 ~
: 1209 3 62 now
- 1214
: 1217 2 60 Just
: 1222 3 67  one
: 1227 2 64  crack
: 1231 2 62  at
: 1234 2 60  life
: 1236 6 62 ~
- 1244
: 1254 6 60 Who
: 1260 3 60  wants
: 1264 2 60  to
: 1267 2 60  live
: 1269 3 62 ~
: 1272 2 62 it
- 1276
: 1281 2 60 In
: 1286 5 64  trou
: 1291 2 64 ble
: 1293 2 62 ~
: 1296 2 60 and
: 1300 4 62  strife
: 1304 2 60 ~
- 1308
: 1312 4 62 My
: 1317 5 65  mind
: 1323 3 65  must
: 1328 3 65  be
: 1333 4 67  free
- 1339
: 1345 3 65 To
: 1349 6 72  learn
: 1355 2 72  all
: 1357 3 69 ~
: 1360 3 69 I
: 1365 2 67  can
: 1367 2 65 ~
: 1369 3 62 a
: 1373 5 65 bout
: 1381 3 69  me
: 1384 1 68 ~
: 1385 4 69 ~
- 1391
: 1408 2 65 Mmm
: 1412 5 65  hmm
- 1419
: 1445 5 60 I'm
: 1451 4 60  gon
: 1455 3 60 na
: 1458 4 62  love
: 1463 3 62  me
- 1468
: 1472 2 60 For
: 1474 2 60  the
: 1477 3 64  rest
: 1483 3 64  of
: 1487 2 62  my
: 1489 2 60 ~
: 1492 7 62 days
- 1501
: 1506 2 58 En
: 1509 6 60 cou
: 1515 3 60 rage
: 1520 3 60  the
: 1523 5 62  ba
: 1528 3 62 bies
- 1533
: 1539 5 64 Ev
: 1544 2 64 ery
: 1547 4 64  time
: 1552 2 62  they
: 1557 2 60  say
: 1559 4 62 ~
- 1565
: 1574 3 65 Self
: 1579 3 65 pre
: 1584 2 65 ser
: 1587 3 67 va
: 1592 4 67 tion
- 1597
: 1599 2 65 Is
: 1602 3 65  what's
* 1609 7 69  real
: 1616 2 70 ly
: 1619 4 69  go
: 1623 3 67 ing
: 1627 3 67  on
: 1630 5 65 ~
- 1636
: 1636 2 65 To
: 1639 4 69 day
: 1643 2 67 ~
: 1645 6 69 ~
- 1653
: 1700 2 72 Say
: 1703 2 72  I'm
: 1706 4 72  gon
: 1710 2 70 na
: 1712 5 72  turn
: 1718 4 69  loose
- 1724
: 1732 4 72 Thou
: 1737 2 69 sand
: 1740 3 69  times
: 1743 3 67 ~
: 1747 2 65 a
: 1749 4 65  day
- 1755
: 1760 1 60 But
: 1763 5 65  how
* 1770 2 67  can
: 1772 3 69  I
: 1777 4 69  turn
: 1782 2 67  loose
: 1784 3 65 ~
- 1789
: 1791 2 60 When
: 1793 1 62  I
: 1795 2 65  just
: 1799 3 67  can't
: 1803 4 69  break
: 1808 3 72  a
: 1811 2 72 way
: 1813 2 74 ~
- 1817
: 1847 5 72 Oh
: 1854 4 69 ~
: 1859 4 69 young
: 1865 11 67  hearts
- 1878
: 1891 4 69 Run
: 1897 10 67  free
- 1909
: 1919 3 62 They'll
: 1923 5 69  nev
: 1929 3 67 er
: 1933 4 69  be
: 1939 3 67  hung
: 1942 3 65 ~
: 1945 3 65 up
- 1950
* 1955 5 65 Hung
: 1960 3 64  up
: 1965 3 65  like
: 1971 2 64  my
: 1973 3 62 ~
: 1977 6 62 man
: 1983 5 62  and
: 1988 3 62  me
: 1991 2 60 ~
: 1993 1 62 ~
: 1994 13 60 ~
- 2009
: 2051 4 62 You
: 2055 4 65  and
: 2059 6 65  me
- 2067
* 2104 5 72 Ooh
: 2109 6 69 ~
: 2115 5 69 young
: 2121 11 67  hearts
- 2134
: 2143 2 62 To
: 2147 4 69  your
: 2153 3 67 self
: 2158 4 69  be
: 2163 3 67  true
: 2166 4 65 ~
- 2172
: 2179 5 69 Don't
: 2185 3 67  be
: 2189 4 69  no
: 2195 2 69  fool
: 2197 2 68 ~
: 2201 5 65 when
- 2208
: 2211 5 65 Love
: 2217 5 64  real
: 2222 3 65 ly
: 2227 5 64  don't
: 2239 6 62  love
: 2246 4 62  you
: 2250 10 60 ~
- 2262
: 2303 1 60 Don't
: 2304 4 62 ~
* 2308 6 65 love
: 2315 5 65  you
- 2322
: 2496 4 69 Young
: 2502 11 67  hearts
- 2515
: 2528 5 69 Run
: 2534 4 69  free
: 2538 2 67 ~
: 2540 2 65 ~
- 2544
: 2556 2 62 They'll
: 2560 5 69  nev
: 2565 4 67 er
: 2570 3 69  be
: 2576 2 67  hung
: 2578 3 65 ~
: 2582 3 65 up
- 2587
: 2592 5 65 Hung
: 2597 3 64  up
: 2603 2 65  like
: 2607 3 64  my
: 2610 3 62 ~
: 2613 5 62 man
: 2619 3 65  and
: 2623 6 65  me
- 2631
: 2682 2 60 My
: 2684 3 62 ~
: 2688 2 60 man
: 2690 3 62 ~
: 2693 3 65 and
: 2699 5 62  me
: 2704 5 60 ~
- 2711
: 2726 4 60 Oh
: 2730 4 62 ~
: 2734 4 65 ~
: 2738 4 67 ~
: 2742 4 69 ~
: 2746 3 67 ~
- 2750
: 2750 4 69 Young
: 2755 11 67  hearts
- 2768
: 2778 2 62 To
: 2781 4 72  your
: 2787 4 69 self
: 2792 2 67  be
* 2797 6 67  true
- 2805
: 2813 5 69 Don't
: 2819 3 67  be
: 2824 3 69  no
: 2829 2 69  fool
: 2831 3 67 ~
: 2836 5 65 when
- 2843
: 2845 5 65 Love
: 2851 4 64  real
: 2856 3 65 ly
: 2861 4 64  don't
: 2869 5 62  love
: 2876 3 62  you
: 2879 2 60 ~
: 2881 1 62 ~
: 2882 10 60 ~
- 2894
: 2935 2 60 It
: 2938 4 62  don't
: 2943 4 65  love
: 2948 4 65  you
- 2954
: 2992 5 72 Ooh
: 2997 5 69 ~
: 3002 5 69 young
: 3008 13 67  hearts
- 3023
: 3034 5 69 Run
: 3041 10 67  free
- 3053
: 3062 2 62 They'll
: 3066 5 69  nev
: 3072 4 67 er
: 3077 3 69  be
: 3083 2 67  hung
: 3085 3 65 ~
: 3089 3 65 up
- 3094
: 3099 5 65 Hung
: 3104 3 64  up
: 3109 2 65  like
: 3114 3 64  my
: 3117 3 62 ~
: 3120 5 62 man
: 3126 3 62  and
: 3130 6 62  me
: 3136 1 60 ~
: 3137 4 62 ~
: 3141 12 60 ~
- 3155
: 3189 2 60 My
: 3191 3 62 ~
: 3195 7 65 man
: 3203 3 62  and
: 3207 4 62  me
E