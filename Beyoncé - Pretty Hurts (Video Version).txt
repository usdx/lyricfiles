#TITLE:Pretty Hurts (Video Version)
#ARTIST:Beyoncé
#MP3:Beyoncé - Pretty Hurts.mp3
#VIDEO:Beyoncé - Pretty Hurts.mp4
#BPM:260,06
#GAP:81425
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Pop
#YEAR:2014
#AUTHOR:Coppo
: 0 6 11 Pret
: 7 9 10 ty
: 20 7 6  hurts
: 28 4 11 ~
: 33 9 10 ~
- 53
: 57 2 -1 We
: 61 2 -1  shine
: 64 2 -1  a
: 68 5 -1  light
: 75 3 -1  on
: 79 2 -1  what
: 83 5 6 e
: 89 2 8 ver's
: 94 9 3  worst
: 104 3 1 ~
: 108 7 3 ~
- 127
: 131 3 -1 Per
: 137 5 6 fec
: 145 5 -1 tion
: 152 3 -1  is
: 156 2 -1  a
: 160 5 6  di
: 168 5 -1 sease
: 176 2 -1  of
: 179 3 -1  a
: 184 5 6  na
: 192 11 -1 tion
- 220
: 256 7 11 Pret
: 264 9 10 ty
: 276 7 6  hurts
: 284 5 11 ~
: 290 7 10 ~
- 303
: 305 2 -1 We
: 309 2 -1  shine
: 312 2 -1  a
: 316 5 -1  light
: 323 3 -1  on
: 327 2 -1  what
: 331 5 6 e
: 337 2 8 ver's
: 341 9 3  worst
: 351 3 1 ~
: 355 9 3 ~
- 371
: 374 2 -1 Try
: 377 2 -1 na
: 381 5 6  fix
: 388 3 -1  some
: 392 2 -1 thing
: 395 3 -1  that
: 399 2 -1  you
: 403 5 6  can't
: 411 3 -1  fix
- 416
: 418 2 -1 What
: 421 2 -1  you
: 425 5 6  can't
: 433 9 -1  see
- 447
: 449 2 -2 It's
: 453 2 -1  the
: 457 3 3  soul
: 461 2 3  that
: 465 3 1  needs
: 469 2 -1  a
: 473 3 -2  sur
: 477 2 -1 ge
: 481 31 -1 ry
- 546
: 724 2 -1 Uh
: 728 5 4 ~
: 736 5 3 ~
- 775
: 852 2 -1 Uh
: 856 5 4 ~
: 864 5 3 ~
- 879
: 888 3 -1 Ma
: 892 2 -1 ma
: 896 7 -1  said
- 909
: 912 3 -1 'You're
: 916 2 -1  a
: 920 3 -1  pret
: 924 2 1 ty
: 928 7 1  girl
: 936 7 -4 ~
- 946
: 948 2 -4 What's
: 952 2 -1  in
: 956 3 -1  your
: 960 7 -1  head
: 968 3 -4 ~
- 977
: 980 2 -4 It
: 984 3 -1  does
: 988 2 1 n't
: 992 3 4  mat
: 996 11 3 ter
: 1008 3 1 ~
- 1014
: 1016 2 -1 Brush
: 1020 2 -1  your
: 1024 11 -1  hair
- 1044
: 1048 2 -1 Fix
: 1052 2 1  your
: 1056 7 1  teeth
: 1064 11 -4 ~
- 1078
: 1080 2 -1 What
: 1084 3 -1  you
: 1088 11 -1  wear
- 1105
: 1108 2 -4 Is
: 1112 3 -1  all
: 1116 2 1  that
: 1120 3 4  mat
: 1124 11 3 ters
: 1136 3 1 ~'
- 1142
: 1144 5 6 Just
* 1152 5 -1  an
* 1160 15 6 o
* 1176 3 6 ther
* 1180 2 8 ~
: 1184 19 3  stage
- 1206
: 1208 3 -1 Pa
: 1214 3 1 geant
: 1220 2 -1  the
: 1224 3 4  pain
: 1228 9 6 ~
: 1240 3 4  a
: 1244 2 3 ~
: 1248 19 3 way
- 1270
: 1272 3 1 This
: 1280 3 -1  time
: 1288 5 6  I'm
: 1296 3 6  gon
: 1300 2 6 na
: 1304 5 6  take
: 1312 2 8  the
: 1316 5 3  crown
- 1322
: 1324 3 -1 With
: 1328 3 -1 out
: 1336 7 -1  fal
: 1344 2 -1 ling
: 1348 9 6  down,
: 1360 13 4  down,
: 1376 19 3  down
- 1398
* 1400 7 11 Pret
* 1408 13 10 ty
* 1424 7 6  hurts
: 1432 7 11 ~
: 1440 9 10 ~
- 1450
: 1452 2 -1 We
: 1456 3 -1  shine
: 1460 2 -1  the
: 1464 7 -1  light
: 1472 2 -1  on
: 1476 3 -1  what
: 1480 7 6 e
: 1488 2 8 ver's
: 1492 11 3  worst
: 1504 3 1 ~
: 1508 9 3 ~
- 1518
: 1520 5 -1 Per
: 1528 5 6 fec
: 1536 5 -1 tion
: 1544 3 -1  is
: 1548 2 -1  a
: 1552 5 6  di
: 1560 5 -1 sease
: 1568 3 -1  of
: 1572 2 -1  a
: 1576 5 6  na
: 1584 11 -1 tion
- 1598
: 1600 3 -1 Pret
: 1604 2 -1 ty
: 1608 5 6  hurts,
: 1616 3 11  pret
: 1620 2 11 ty
: 1624 9 10  hurts
- 1643
: 1656 7 11 Pret
: 1664 13 10 ty
* 1680 7 6  hurts
* 1688 7 11 ~
* 1696 9 10 ~
- 1706
: 1708 2 -1 We
: 1712 3 -1  shine
: 1716 2 -1  the
: 1720 7 -1  light
: 1728 2 -1  of
: 1732 3 -1  what
: 1736 7 6 e
: 1744 2 8 ver's
: 1748 11 3  worst
: 1760 3 1 ~
: 1764 5 3 ~
- 1770
: 1772 2 -1 We
: 1776 3 -1  try
: 1780 2 -1  to
: 1784 5 6  fix
: 1792 3 -1  some
: 1796 2 -1 thing
: 1800 3 -1  but
: 1804 2 -1  you
: 1808 5 6  can't
: 1816 5 -1  fix
- 1822
: 1824 3 -1 What
: 1828 2 -1  you
: 1832 5 6  can't
: 1840 11 -1  see
- 1854
: 1856 2 -2 It's
: 1860 2 -1  the
: 1864 3 3  soul
: 1868 2 3  that
: 1872 3 1  needs
: 1876 2 -1  a
: 1880 3 -2  sur
: 1884 2 -1 ge
: 1888 19 -1 ry
- 1917
: 1940 2 -1 Uh
: 1944 5 4 ~
: 1952 5 3 ~
- 1967
: 1976 3 -1 Blond
: 1980 2 -1 er
: 1984 11 -1  hair
- 2004
: 2008 1 -1 Flat
: 2010 3 1 ~
: 2016 7 1  chest
: 2024 7 -4 ~
- 2037
: 2040 3 -1 T
: 2044 2 -1 V
: 2048 9 -1  says
: 2058 3 -4 ~
- 2066
: 2068 2 -4 'Big
: 2072 3 -1 ger
: 2076 2 1  is
: 2080 3 4  bet
: 2084 9 3 ter
: 2094 3 1 ~'
- 2102
: 2104 5 -1 South
: 2112 9 -1  beach
- 2132
: 2136 3 -1 Su
: 2140 2 1 gar
: 2144 7 1  free
: 2152 11 -4 ~
- 2166
: 2168 5 -1 Vogue
: 2176 11 -1  says
- 2193
: 2196 3 -6 'Thin
: 2200 3 -1 ner
: 2204 2 1  is
* 2208 3 4  bet
* 2212 5 3 ter
* 2218 1 1 ~
* 2220 3 -1 ~'
- 2229
: 2232 5 6 Just
: 2240 5 -1  an
: 2248 15 6 o
: 2264 3 6 ther
: 2268 2 8 ~
: 2272 19 3  stage
- 2294
: 2296 3 -1 Pa
: 2302 3 1 geant
: 2308 2 -1  the
: 2312 3 4  pain
: 2316 9 6 ~
: 2328 3 4  a
: 2332 2 3 ~
: 2336 19 3 way
- 2358
: 2360 3 1 This
: 2368 3 -1  time
: 2376 5 6  I'm
: 2384 3 6  gon
: 2388 2 6 na
: 2392 5 6  take
: 2400 2 8  the
: 2404 5 3  crown
- 2410
: 2412 3 -1 With
: 2416 3 -1 out
: 2424 7 -1  fal
: 2432 2 -1 ling
: 2436 9 6  down,
: 2448 13 4  down,
: 2464 19 3  down
- 2517
: 3466 7 11 Pret
: 3474 13 10 ty
: 3490 7 6  hurts
: 3498 7 11 ~
: 3506 9 10 ~
- 3516
: 3518 2 -1 We
: 3522 3 -1  shine
: 3526 2 -1  the
: 3530 7 -1  light
: 3538 2 -1  on
: 3542 3 -1  what
: 3546 7 6 e
: 3554 2 8 ver's
: 3558 11 3  worst
: 3570 3 1 ~
: 3574 9 3 ~
- 3584
: 3586 5 -1 Per
: 3594 5 6 fec
: 3602 5 -1 tion
: 3610 3 -1  is
: 3614 2 -1  a
: 3618 5 6  di
: 3626 5 -1 sease
: 3634 3 -1  of
: 3638 2 -1  a
: 3642 5 6  na
: 3650 11 -1 tion
- 3664
: 3666 3 -1 Pret
: 3670 2 -1 ty
: 3674 5 6  hurts,
: 3682 3 11  pret
: 3686 2 11 ty
: 3690 9 10  hurts
- 3709
: 3722 7 11 Pret
: 3730 13 10 ty
: 3746 7 6  hurts
: 3754 7 11 ~
: 3762 9 10 ~
- 3772
: 3774 2 -1 We
: 3778 3 -1  shine
: 3782 2 -1  the
: 3786 7 -1  light
: 3794 2 -1  of
* 3798 3 -1  what
* 3802 7 6 e
* 3810 2 8 ver's
* 3814 11 3  worst
* 3826 3 1 ~
* 3830 5 3 ~
- 3836
: 3838 2 -1 We
: 3842 3 -1  try
: 3846 2 -1  to
: 3850 5 6  fix
: 3858 3 -1  some
: 3862 2 -1 thing
: 3866 3 -1  but
: 3870 2 -1  you
: 3874 5 6  can't
: 3882 5 -1  fix
- 3888
: 3890 3 -1 What
: 3894 2 -1  you
: 3898 5 6  can't
: 3906 11 -1  see
- 3920
: 3922 2 -2 It's
: 3926 2 -1  the
: 3930 3 3  soul
: 3934 2 3  that
: 3938 3 1  needs
: 3942 2 -1  a
: 3946 3 -2  sur
: 3950 2 -1 ge
: 3954 9 -1 ry
- 3964
: 3966 2 4 Ain't
: 3970 3 4  got
: 3974 2 4  no
: 3978 3 4  doc
: 3982 2 4 tor
: 3986 3 4  or
: 3990 1 4  pill
- 3992
: 3994 3 3 That
: 3998 2 3  can
: 4002 3 3  take
: 4006 2 3  the
: 4010 3 3  pain
: 4014 2 3  a
: 4018 3 3 way
: 4022 3 1 ~
- 4028
: 4030 2 -1 The
: 4034 3 -1  pain's
: 4038 2 1  in
: 4042 3 3 side
: 4046 2 1  and
: 4050 3 -1  no
: 4054 2 3 bo
: 4058 3 1 dy
: 4062 2 1  frees
: 4066 1 1  you
- 4068
: 4070 2 1 From
: 4074 3 1  your
: 4078 2 -1  bo
: 4082 3 -1 dy
: 4086 3 -4 ~
- 4092
: 4094 2 -6 It's
: 4098 2 -6  the
: 4102 3 -1  soul,
: 4106 2 -6  it's
: 4110 2 -6  the
: 4114 11 -1  soul
: 4126 2 -6  that
: 4130 2 -6  need
: 4134 7 -1  sur
: 4142 2 -2 ge
: 4146 5 -2 ry
: 4152 19 1 ~
- 4172
: 4174 2 -6 It's
: 4178 2 -6  my
: 4182 7 -1  soul
: 4190 2 -2  that
: 4194 2 -2  needs
: 4198 7 -2  sur
: 4206 2 1 ge
: 4210 5 1 ry
: 4216 1 -1 ~
: 4218 5 -4 ~
- 4224
: 4226 3 4 Plas
: 4230 2 4 tic
: 4234 3 4  smiles
: 4238 2 4  and
: 4242 3 4  de
: 4246 1 4 nial
- 4248
: 4250 2 3 Can
: 4254 3 3  on
: 4258 2 3 ly
: 4262 3 3  take
: 4266 2 3  you
: 4270 2 3  so
: 4274 3 3  far
: 4278 3 1 ~
- 4287
: 4290 3 -1 Then
: 4294 2 1  you
: 4298 2 3  break
: 4302 3 1  when
: 4306 2 -1  the
: 4310 2 3  fake
: 4314 3 1  fa
: 4318 1 1 cade
- 4320
: 4322 3 1 Leaves
: 4326 2 1  you
: 4330 3 -1  in
: 4334 2 -1  the
: 4338 3 1  dark
: 4342 1 -1 ~
: 4344 1 -4 ~
- 4348
: 4350 2 -6 You
: 4354 2 -6  left
: 4358 2 -4  with
: 4362 3 -1  shat
: 4366 2 1 tered
: 4370 3 1  mir
: 4374 5 -1 rors
: 4380 1 -4 ~
: 4382 1 -6 ~
- 4384
: 4386 3 -1 And
: 4390 2 -1  the
: 4394 5 -1  shards
- 4400
: 4402 3 -1 Of
: 4406 2 -1  a
: 4410 3 -1  beau
: 4414 2 1 ti
: 4418 5 1 ful
: 4426 27 1  past
: 4454 3 4 ~
: 4458 9 6 ~
: 4468 7 8 ~
: 4476 1 10 ~
: 4478 3 8 ~
: 4482 23 6 ~
- 4515
: 4526 5 13 Oh
: 4532 1 11 ~
: 4534 1 8 ~
: 4536 1 6 ~
: 4538 2 8 ~
: 4542 7 8 ~
: 4550 1 6 ~
: 4552 1 3 ~
- 4563
: 4586 3 6 Pret
: 4590 2 8 ty
: 4594 3 11  hurts
: 4598 3 13 ~
: 4602 11 15 ~
: 4614 1 13 ~
: 4616 1 11 ~
- 4634
: 4674 7 16 Pret
: 4682 7 13 ~
: 4690 5 11 ty
: 4698 19 11  hurts
: 4718 3 13 ~
- 4731
: 4738 7 16 Pret
: 4746 5 15 ~
: 4752 1 13 ~
: 4754 1 11 ~
: 4756 3 11 ty
: 4762 11 11  hurts
: 4774 1 8 ~
: 4776 1 6 ~
- 4787
: 4802 5 11 Oh
: 4810 3 8 ~
- 4822
: 4826 5 11 Oh,
: 4834 5 11  oh
: 4842 3 8 ~
- 4855
* 4866 7 16 Pret
* 4874 3 15 ~
* 4878 1 13 ~
* 4880 1 11 ~
* 4882 5 11 ty
* 4890 7 16  hurts
* 4898 3 15 ~
* 4902 1 13 ~
* 4904 1 11 ~
* 4906 3 13 ~
* 4910 9 11 ~
- 4936
: 4968 3 -1 When
: 4973 5 3  you're
: 4979 2 6  a
: 4983 1 6 lone
: 4985 4 8 ~
: 4990 1 8  all
: 4992 2 10 ~
: 4995 2 8 ~
: 4999 1 8  by
: 5001 2 6  your
: 5005 12 6 self
- 5023
: 5026 2 6 And
: 5030 2 8  you're
: 5034 9 11  ly
: 5045 2 10 ing
: 5048 2 8 ~
: 5051 1 8  in
: 5053 2 6 ~
: 5056 1 6  your
: 5058 2 3 ~
: 5062 15 3  bed
- 5087
: 5095 2 -1 Re
: 5100 3 3 flec
: 5105 3 6 tions
: 5110 7 8  stares
: 5120 2 8  right
: 5123 2 10 ~
: 5127 3 8  in
: 5132 2 6 to
: 5135 8 6  you
: 5144 1 3 ~
- 5154
: 5158 2 6 Are
: 5161 2 8  you
: 5164 5 11  hap
: 5170 2 10 py
: 5174 2 8  with
: 5178 3 6  your
: 5184 13 3 self?
- 5207
: 5222 3 -2 You
: 5229 3 3  stripped
: 5234 2 6  a
: 5237 1 6 way
: 5239 2 8 ~
: 5243 3 10  the
: 5249 4 8  mas
: 5254 2 3 ~
: 5258 2 3 que
: 5262 9 6 rade
: 5272 1 3 ~
- 5282
: 5285 2 6 The
: 5288 2 8  il
: 5292 5 11 lu
: 5298 2 10 sion
: 5301 3 8  has
: 5306 3 6  been
: 5312 9 3  shed
: 5322 1 1 ~
- 5333
: 5350 3 6 Are
: 5355 3 8  you
* 5360 5 11  hap
* 5366 2 10 py
: 5370 2 8  with
: 5374 3 10  your
: 5380 25 11 self?
- 5411
: 5414 3 6 Are
: 5418 2 8  you
: 5422 7 13  hap
: 5430 3 15 py
: 5434 2 13 ~
: 5438 2 13  with
* 5442 2 11  your
* 5446 9 8 self
* 5456 1 8 ~
* 5458 1 6 ~
* 5460 1 3 ~
* 5462 15 6 ~?
- 5511
: 5565 3 6 Yes
: 5569 3 6 ~
: 5573 3 8 ~
: 5577 29 3 ~
- 5618
: 5622 2 -1 Uh
: 5626 5 4 ~
: 5634 5 3 ~.
E