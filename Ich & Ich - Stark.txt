#TITLE:Stark
#ARTIST:Ich & Ich
#MP3:Ich & Ich - Stark.mp3
#VIDEO:Ich & Ich - Stark [VD#1,6].avi
#COVER:Ich & Ich - Stark [CO].jpg
#BPM:279,95
#GAP:13640
#VIDEOGAP:1,6
#ENCODING:UTF8
: 0 2 1 Ich
: 4 2 3  bin
: 8 2 1  seit
: 12 3 10  Wo
: 16 2 8 chen
: 20 2 8  un
: 24 3 6 ter
: 28 6 6 wegs
- 36
: 48 2 3 und
: 52 3 6  trin
: 56 2 3 ke
: 60 2 6  zu
: 64 2 3  viel
: 68 3 6  Bier
: 72 2 6  und
: 76 6 10  Wein.
- 84
: 132 3 3 Mei
: 136 2 1 ne
: 140 3 10  Woh
: 144 2 8 nung
: 148 2 8  ist
: 152 2 6  ver
: 156 3 6 ö
: 160 2 3 det,
- 163
: 164 3 6 mei
: 168 2 3 nen
: 172 3 6  Spie
: 176 2 3 gel
: 180 3 6  schlag
: 184 2 3  ich
: 189 3 6  kurz
: 194 3 3  und
: 201 6 1  klein.
- 209
: 256 2 1 Ich
: 260 2 1  bin
: 263 2 1  nicht
* 268 5 10  der
: 276 2 8  der
: 280 2 6  ich
: 284 5 6  sein
: 292 4 3  will
- 298
: 304 2 3 und
: 308 2 6  will
: 312 2 3  nicht
: 316 4 6  sein
: 324 2 6  wer
: 328 2 3  ich
: 332 6 8  bin.
- 340
: 392 2 3 Mein
: 396 3 10  Le
: 400 2 8 ben
: 404 2 8  ist
: 408 2 6  das
: 413 3 6  Cha
: 418 5 6 os,
- 425
: 432 2 3 schau
: 436 2 6  mal
: 440 2 6  ge
: 444 3 6 nau
: 449 4 6 er
: 456 4 6  hin.
- 462
: 516 2 1 Ich
: 520 2 1  bin
: 524 3 10  tier
: 528 2 8 isch
: 532 2 8  ei
: 536 2 6 fer
: 540 3 6 süch
: 546 3 3 tig
- 551
: 560 2 3 und
: 564 2 6  un
: 568 2 3 ge
: 572 3 6 recht
: 578 2 6  zu
* 584 8 10  Frauen.
- 594
: 648 2 1 Und
: 652 2 10  wenn
: 655 2 8  es
: 660 4 8  ernst
: 668 5 6  wird,
- 675
: 688 2 3 bin
: 692 2 6  ich
: 696 2 3  noch
: 700 3 6  im
: 704 2 3 mer
: 708 2 6  ab
: 712 2 3 ge
: 716 7 1 haun.
- 725
: 776 2 1 Ich
: 780 3 10  fra
: 784 2 8 ge
: 788 3 8  gra
: 792 2 6 de
: 796 5 6  dich,
- 803
: 820 2 6 macht
: 824 2 3  das
: 828 2 6  al
: 832 2 3 les
: 836 3 6  ei
: 840 2 3 nen
* 844 7 8  Sinn?
- 853
: 904 2 6 Mein
: 908 3 10  Le
: 912 2 8 ben
: 916 2 8  ist
: 920 2 6  ein
: 924 4 6  Cha
: 930 3 6 os,
- 935
: 944 2 3 schau
: 948 2 6  mal
: 952 2 6  ge
: 956 3 6 nau
: 962 4 6 er
: 968 5 6  hin.
- 975
: 1032 2 6 Und
* 1037 8 18  Du
: 1048 5 13  glaubst
: 1056 3 11  ich
: 1062 3 10  bin
: 1068 3 8  sta
: 1073 3 6 rk
- 1078
: 1080 2 6 und
: 1084 2 6  ich
: 1089 4 6  kenn
: 1094 3 1  den
: 1100 8 8  Weg.
- 1110
: 1165 7 18 Du
: 1176 6 13  bil
: 1184 3 11 dest
: 1191 3 10  dir
: 1196 3 8  ei
: 1200 2 6 ~n
- 1203
: 1204 2 3 ich
: 1208 5 6  weiß
: 1216 2 6  wie
: 1220 2 6  al
: 1224 2 1 les
: 1228 9 8  geht.
- 1239
: 1288 2 5 Du
: 1292 3 6  denkst
: 1296 2 5  ich
: 1300 2 6  hab
: 1308 2 6  al
: 1312 2 6 les
: 1316 2 6  im
: 1320 7 10  Griff
- 1329
: 1360 2 3 und
: 1364 2 6  kon
: 1368 2 5 trol
: 1372 3 6 lier
: 1376 2 6  was
: 1380 2 5  ge
: 1384 6 6 schieht.
- 1392
: 1412 3 6 A
: 1416 2 6 ber
* 1420 5 18  ich
: 1428 5 13  steh
: 1436 4 11  nur
: 1442 4 10  hier
: 1448 5 8  o
: 1456 4 6 ben
- 1460
: 1460 5 6 und
: 1468 3 6  sing
: 1474 4 6  mein
: 1480 12 6  Lied.
- 1494
: 1540 2 3 Ich
: 1544 2 1  bin
: 1548 3 10  dau
: 1552 2 8 ernd
: 1556 2 8  auf
: 1560 2 6  der
: 1564 5 6  Su
: 1570 4 6 che
- 1576
: 1584 2 3 und
: 1588 2 6  weiß
: 1592 2 3  nicht
: 1596 3 6  mehr
: 1601 4 6  wo
: 1608 7 10 nach.
- 1617
: 1668 2 1 Ich
: 1672 2 1  zieh
: 1676 3 10  näch
: 1680 2 8 te
: 1684 3 8 lang
: 1688 2 6  durch
: 1693 7 6  Bars,
- 1702
: 1716 2 6 im
: 1720 2 3 mer
: 1724 2 6  der
: 1728 2 3  der
: 1732 2 3  am
: 1736 2 6  lau
: 1740 2 3 tes
: 1744 2 3 ten
: 1748 3 1  lacht.
- 1753
: 1804 3 10 Nie
: 1808 2 8 mand
: 1812 2 8  sieht
: 1816 2 6  mir
: 1820 3 6  an
- 1825
: 1828 2 6 wie
: 1832 2 3  ver
: 1836 5 6 wirrt
: 1844 4 3  ich
: 1852 3 6  wirk
: 1858 4 3 lich
* 1868 7 8  bin.
- 1877
: 1928 2 3 Ist
: 1932 3 10  al
: 1936 2 8 les
: 1940 2 8  nur
: 1944 2 6  Fa
: 1948 4 6 sa
: 1954 2 3 de,
- 1958
: 1968 2 3 schau
: 1972 2 6  mal
: 1976 2 6  ge
: 1980 3 6 nau
: 1986 3 6 er
: 1992 5 6  hin.
- 1999
: 2056 2 6 Und
: 2060 8 18  Du
: 2072 5 13  glaubst
: 2080 3 11  ich
: 2086 3 10  bin
: 2092 3 8  sta
: 2096 5 6 rk
- 2102
: 2104 2 6 und
: 2108 2 6  ich
: 2113 3 6  kenn
: 2118 3 1  den
: 2124 12 8  Weg.
- 2138
* 2188 8 18 Du
: 2200 6 13  bil
: 2208 3 11 dest
: 2215 3 10  dir
: 2220 3 8  ei
: 2224 2 6 ~n
- 2227
: 2228 2 3 ich
: 2232 4 6  weiß
: 2240 2 6  wie
: 2244 2 6  al
: 2248 2 1 les
: 2252 11 8  geht.
- 2265
: 2312 2 1 Du
: 2316 2 6  denkst
: 2320 2 3  ich
: 2324 4 6  hab
: 2332 2 6  al
: 2336 2 6 les
: 2340 2 6  im
: 2344 9 10  Griff.
- 2355
: 2384 2 3 Und
: 2388 2 6  kon
: 2392 2 5 trol
: 2396 3 6 lier
: 2400 2 6  was
: 2404 2 6  ge
: 2408 9 6 schieht.
- 2419
: 2436 3 6 A
: 2440 2 6 ber
* 2444 5 18  ich
: 2452 5 13  steh
: 2460 4 11  nur
: 2466 4 10  hier
: 2472 6 8  o
: 2480 4 6 ben
- 2484
: 2484 4 6  und
: 2492 3 6  sing
: 2498 3 6  mein
: 2504 11 6  Lied.
- 2517
: 2572 5 18 Ich
: 2580 5 13  steh
: 2588 3 11  nur
: 2594 4 10  hier
: 2600 6 8  o
: 2608 3 6 ben
- 2612
: 2612 5 6  und
: 2620 3 6  sing
: 2626 4 6  mein
: 2632 12 6  Lied.
- 2646
: 2692 2 3 Stell
: 2696 2 1  dich
: 2700 2 10  mit
: 2704 3 8  mir
: 2708 2 8  in
: 2712 2 6  die
: 2716 3 6  Son
: 2720 2 3 ne
- 2723
: 2724 2 6 o
: 2728 2 3 der
: 2732 2 6  geh
: 2736 2 3  mit
: 2740 2 6  mir
: 2744 2 3  ein
: 2748 3 6  klei
: 2753 3 6 nes
: 2761 6 10  Stück.
- 2769
: 2824 2 1 Ich
: 2828 3 10  zeig
: 2832 2 8  dir
: 2836 2 8  mei
: 2840 2 6 ne
: 2844 4 6  Wahr
: 2849 4 6 heit,
- 2855
: 2864 2 3 für
: 2868 3 6  ei
: 2872 2 3 nen
: 2876 3 6  Au
: 2881 4 3 gen
: 2888 7 1 blick.
- 2897
: 2952 2 1 Ich
* 2956 3 10  fra
: 2960 2 8 ge
: 2964 2 8  mich
: 2968 2 6  ge
: 2972 2 6 nau
: 2976 2 3  wie
: 2980 5 6  du,
- 2987
: 2995 2 6 wo
: 2999 2 3  ist
: 3004 3 6  hier
: 3009 4 3  der
: 3017 8 8  Sinn?
- 3027
: 3080 2 6 Mein
: 3084 3 10  Le
: 3088 2 8 ben
: 3092 2 8  ist
: 3097 3 6  ein
: 3103 5 6  Cha
: 3110 2 6 os.
- 3114
: 3120 2 3 Schau
: 3124 2 6  mal
: 3128 2 6  ge
: 3132 3 6 nau
: 3137 4 6 er
: 3144 5 6  hin.
- 3151
: 3208 2 6 Und
* 3212 8 18  Du
: 3224 5 13  glaubst
: 3232 3 11  ich
: 3238 3 10  bin
: 3244 3 8  sta
: 3248 5 6 rk
- 3254
: 3256 2 6 und
: 3260 2 6  ich
: 3265 4 6  kenn
: 3271 2 1  den
: 3276 9 8  Weg.
- 3287
: 3340 8 18 Du
: 3352 5 13  bil
: 3360 3 11 dest
: 3367 3 10  dir
: 3372 3 8  ei
: 3376 2 6 ~n
- 3379
: 3380 2 3 ich
: 3384 4 6  weiß
: 3392 2 6  wie
: 3396 2 6  al
: 3400 2 1 les
: 3404 9 8  geht.
- 3415
: 3456 5 18 Oh
: 3462 2 18  Du
: 3468 3 18  denkst
: 3474 2 17  ich
: 3478 3 15  hab
: 3483 4 15  al
: 3489 3 13 les
: 3495 3 13  im
: 3500 8 13  Griff
- 3510
: 3536 2 18 und
: 3540 2 18  kon
: 3544 2 18 trol
: 3548 3 18 lier
: 3552 3 17  was
: 3558 2 15  ge
: 3564 4 15 schi
: 3570 5 13 eht.
- 3577
: 3588 3 6 A
: 3592 2 6 ber
* 3596 4 18  ich
: 3604 6 13  steh
: 3612 3 11  nur
: 3617 3 10  hier
: 3624 6 8  o
: 3632 3 6 ben
- 3636
: 3636 5 6  und
: 3644 2 6  sing
: 3649 5 6  mein
: 3656 11 6  Lied.
- 3669
: 3720 8 18 Ich
: 3732 5 13  steh
: 3740 3 11  nur
: 3745 3 10  hier
: 3752 6 8  o
: 3760 3 6 ben
- 3764
: 3764 4 6  und
: 3772 2 6  sing
: 3777 4 6  mein
: 3784 10 6  Lied
E