#TITLE:Je l'aime à mourir
#ARTIST:Shakira
#MP3:Shakira - Je l'aime à mourir.mp3
#VIDEO:Shakira - Je l'aime à mourir.mp4
#COVER:Shakira - Je l'aime à mourir.png
#BPM:237.68
#GAP:13205
#ENCODING:UTF8
#LANGUAGE:French
#GENRE:Pop
#YEAR:2012
#CREATOR:mustangfred and Volodia
: 0 1 0 Y
: 3 1 4  yo
: 5 2 4  que has
: 9 1 4 ta a
: 12 4 4 yer
: 19 2 2  so
: 22 1 2 lo
: 26 1 2  fui
: 28 2 4  un
: 31 2 0  hol
: 34 1 0 ga
: 38 5 0 zán
- 48
: 50 1 2 y
: 54 2 4  soy
: 57 2 4  el
* 60 3 4  guar
* 64 3 4 dián
: 70 1 2  de
: 73 1 2  sus
: 76 4 2  sue
: 82 1 0 ños
: 85 1 0  de a
: 88 2 0 mor
: 91 1 2 ~
- 93
: 95 1 0 La
: 98 2 2  quie
: 101 2 0 ro a
* 104 3 2  mo
* 108 2 2 rir
* 111 5 4 ~
- 131
: 153 1 2 Pue
: 155 1 5 den
: 158 1 5  des
: 161 1 5 tro
: 165 2 5 zar
: 168 2 7 ~
: 172 1 4  to
: 174 1 4 do a
: 177 4 4 que
: 183 1 2 llo
: 187 2 2  que
: 190 3 2  ven
- 195
* 197 2 2 por
* 200 4 4 que
: 206 4 5  e
: 211 2 5 lla
: 214 1 5  de un
: 217 5 5  so
: 224 1 4 plo
: 227 1 4  lo
: 229 5 4  vuel
: 236 2 2 ve a
: 240 2 2  cre
: 243 2 2 ar
- 248
: 250 1 0 co
: 253 1 2 mo
: 256 1 0  si
* 259 3 2  na
* 263 1 2 da
: 265 1 4 ~
- 273
: 276 2 0 co
: 279 1 2 mo
: 282 1 0  si
: 285 3 2  na
: 289 1 2 da
: 291 1 4 ~
- 301
: 304 1 0 La
: 307 2 2  quie
: 310 2 0 ro a
: 314 2 7  mo
: 317 9 4 rir
- 341
: 360 1 0 E
: 362 1 2 lla
: 366 2 4  pa
: 369 1 4 ra
: 372 1 4  las
: 376 4 4  ho
: 382 1 2 ras
: 385 1 2  de
: 389 4 2  ca
: 394 1 4 ~
: 396 1 0 da
: 399 1 0  re
: 402 3 0 loj
- 411
: 413 1 0 y
: 415 2 2  me a
: 419 2 4 yu
: 422 1 4 da a
: 425 1 4  pin
: 428 2 4 tar
: 431 1 7 ~
: 435 2 2  trans
: 440 1 2 pa
: 442 3 2 ren
: 448 2 0 te el
: 451 2 0  do
: 454 4 0 lor
- 459
: 461 1 0 con
: 465 1 2  su
* 468 1 0  son
* 471 1 2 ri
* 475 3 2 sa
* 479 6 4 ~
- 500
: 520 1 0 Y
: 522 2 4  le
: 525 2 5 van
: 529 1 5 ta u
: 531 2 5 na
: 535 3 5  to
: 539 1 9 ~
: 542 1 4 rre
: 545 2 4  des
: 548 1 4 de el
: 552 2 4  cie
: 555 1 2 lo has
: 560 1 2 ta a
: 562 5 2 quí
- 568
: 570 1 2 y
: 572 1 4  me
: 574 3 4  co
: 578 2 5 ~
: 582 2 5 se u
: 585 1 5 nas
: 588 3 5  a
: 593 1 9 ~
: 595 1 4 las
: 598 2 4  y
: 601 2 4  me a
: 605 1 4 yu
: 607 2 2 da a
: 611 2 2  su
: 614 4 2 bir
- 619
: 621 1 0 a
: 624 2 2  to
: 627 1 0 da
: 630 2 2  pri
: 634 2 2 sa
: 637 5 4 ~
- 646
: 648 1 0 a
: 650 3 2 ~
: 655 1 2  to
: 657 1 0 da
: 660 1 2  pri
: 664 1 2 sa
: 666 4 4 ~
- 673
: 675 1 0 La
: 679 1 2  quie
: 681 2 0 ro a
: 687 2 7  mo
: 690 8 4 rir
- 708
: 728 1 4 Co
: 731 2 4 no
: 734 2 5 ~
: 738 2 4 ce
: 741 10 -1  bien
- 760
* 763 6 4 ca
* 770 1 4 da
: 773 2 2  gue
: 776 1 0 ~
: 779 8 0 rra
- 795
: 798 1 0 ca
: 800 2 2 ~
: 803 2 2 da he
: 807 1 2 ri
: 810 2 -5 ~
: 813 2 -5 da,
: 818 4 0  ca
: 823 1 2 da
: 827 7 4  ser
- 837
: 839 1 4 Co
: 841 1 4 no
: 843 2 5 ~
: 847 1 4 ce
: 850 2 4  bien
: 853 1 2 ~
: 855 9 -1 ~
- 870
: 872 1 4 ca
: 874 2 5 ~
: 878 1 4 ~
: 880 2 4 da
: 883 3 2  gue
: 887 1 0 ~
: 890 6 0 rra
- 903
: 906 1 0 de
: 908 2 2  la
: 911 26 0  vi
: 949 3 -5 da y
: 958 2 0  del
: 961 2 0  a
: 965 3 0 mor,
* 971 2 0  tam
* 975 4 -5 bién
- 988
: 991 2 -1 Eh
: 994 3 -3  eh
: 998 2 -1  eh
: 1001 2 2  eh
: 1004 1 0  eh
: 1006 1 2 ~
: 1008 2 0 ~
: 1011 3 -1  eh
: 1015 11 0  eh
- 1036
: 1044 2 2 Eh
: 1047 2 0  eh
: 1051 2 2  eh
: 1055 2 5  eh
: 1058 1 4 ~
: 1060 1 5  eh
: 1062 2 4  eh
: 1065 2 2  eh
* 1068 17 4  eh
- 1094
: 1098 2 2 Eh
: 1101 3 0  eh
: 1105 2 2  eh
: 1108 3 5  eh
: 1112 1 4 ~
: 1114 2 5  eh
: 1117 1 4 ~
: 1119 1 2  eh
: 1121 1 4  eh
: 1123 2 2  eh
: 1126 28 0  eh
- 1185
: 1286 1 0 Moi
: 1289 1 0  je
: 1291 1 4 ~
: 1293 1 4  n'é
: 1296 1 4 tais
: 1299 5 4  rien
: 1306 1 2  et
: 1309 2 2  voi
: 1313 4 2 là
: 1320 1 0  qu'au
: 1322 2 0 jour
: 1327 3 0 d'hui
- 1337
: 1340 1 0 Je
: 1344 1 4  suis
: 1347 2 4  le
: 1350 1 4  gar
: 1353 5 4 dien
: 1360 1 2  du
: 1364 1 2  som
: 1367 4 2 meil
: 1373 1 0  de
: 1377 1 0  ses
: 1380 2 0  nuits
: 1383 2 2 ~
- 1386
: 1388 1 0 Je
: 1391 1 2  l'aime
: 1394 1 0  à
: 1397 2 2  mou
* 1400 2 2 rir
* 1403 6 4 ~
- 1424
: 1445 1 0 Vous
: 1448 2 4  pou
: 1451 1 5 vez
: 1454 1 5  dé
: 1457 7 5 truire
: 1467 1 4  tout
: 1471 1 4  ce
: 1475 4 4  qu'il
: 1482 1 2  vous
: 1485 1 2  plai
: 1487 4 2 ra
- 1497
: 1500 2 4 Elle
: 1504 1 5  n'a
: 1507 2 5  qu'à
: 1510 2 5  ouv
: 1513 6 5 rir
: 1523 2 4  l'es
: 1528 3 4 pace
: 1534 1 2  de
: 1537 1 2  ses
: 1541 4 2  bras
- 1546
: 1548 1 0 Pour
: 1551 1 2  tout
: 1554 1 0  re
: 1557 1 2 cons
: 1561 2 2 truire
: 1564 5 4 ~
- 1573
: 1575 1 0 Pour
: 1578 1 2  tout
: 1581 1 0  re
: 1584 2 2 cons
: 1588 2 2 truire
: 1591 2 4 ~
- 1599
: 1602 2 0 Je
: 1605 2 2  l'aime
: 1608 1 0  à
: 1611 3 7  mou
: 1615 7 4 rir
- 1637
: 1660 1 0 Elle
: 1662 2 2  a
: 1666 2 4  gom
: 1670 1 4 mé
: 1673 1 4  les
: 1676 3 4  chif
: 1680 1 2 fres
: 1683 1 2  des
: 1686 2 2  hor
: 1690 1 2 loges
: 1692 1 4 ~
- 1695
: 1697 1 0 du
: 1700 2 0  quar
: 1703 4 0 tier
- 1713
: 1715 1 0 Elle
: 1717 1 2  a
: 1721 2 4  fait
: 1724 1 4  de
: 1727 2 4  ma
: 1731 4 4  vie
: 1738 1 2  des
* 1741 1 2  co
* 1744 5 2 cottes
: 1752 1 0  en
: 1755 1 0  pa
: 1758 4 0 pier
- 1763
: 1765 1 0 Des
: 1768 2 2  é
: 1772 1 0 clats
: 1775 1 0  de
: 1777 3 2  rires
: 1781 7 4 ~
- 1803
: 1823 1 0 Elle
: 1825 2 4  a
: 1829 2 5  bâ
: 1832 1 5 ti
: 1835 1 5  des
: 1840 2 5  ponts
: 1843 1 9 ~
: 1846 2 4  en
: 1849 2 4 tre
: 1853 2 4  nous
: 1856 1 5 ~
: 1859 2 2  et
: 1862 1 2  le
: 1867 3 2  ciel
- 1875
: 1877 1 2 Et
: 1880 2 4  nous
: 1883 1 5  les
: 1887 2 5  tra
: 1890 1 5 ver
: 1894 3 5 sons
: 1898 1 9 ~
- 1901
: 1903 1 4 à
: 1907 2 4  cha
: 1910 1 5 ~
: 1913 1 2 que
: 1916 1 2  fois
: 1920 4 2  qu’elle
- 1925
: 1927 1 0 Ne
: 1930 1 2  veut
: 1933 2 0  pas
: 1937 1 2  dor
: 1940 2 2 mir
: 1943 4 4 ~
- 1953
: 1955 1 0 Ne
: 1958 1 2  veut
: 1961 1 0  pas
: 1964 1 2  dor
: 1967 2 2 mir
: 1970 3 4 ~
- 1979
: 1981 2 0 Je
: 1985 2 2  l'aime
: 1988 1 0  à
: 1991 2 7  mou
: 1995 8 4 rir
- 2018
: 2036 2 4 Elle
: 2039 1 5  a
: 2042 1 4  dû
: 2046 9 -1  faire
: 2060 1 4  tou
: 2062 3 5 ~
: 2067 1 4 tes
: 2069 2 4  les
: 2073 2 2  guer
: 2076 2 0 ~
: 2080 3 0 res
- 2093
: 2105 2 -5 Pour
: 2108 2 -1  ê
: 2111 1 0 tre
: 2115 1 2  si
: 2119 8 -5  forte
: 2130 1 0  au
: 2132 1 2 jour
: 2136 1 2 d’hui
: 2138 7 4 ~
- 2146
: 2148 2 4 Elle
: 2151 1 5  a
: 2154 1 4  dû
: 2157 2 4  faire
: 2161 5 -1 ~
: 2172 1 4  tou
: 2174 2 5 ~
: 2178 1 4 tes
: 2181 1 4  les
: 2185 2 2  guer
: 2188 2 0 ~
: 2191 4 0 res
- 2205
: 2213 2 -5 De
: 2216 1 2  la
: 2220 24 0  vie
- 2254
: 2263 1 -5 Et
: 2266 2 0 ~
: 2269 3 0  l’a
: 2274 5 0 mour,
: 2281 2 0  aus
: 2285 3 -5 si
- 2298
: 2310 2 -1 Eh
: 2313 3 -3  eh
: 2317 1 -1  eh
: 2320 2 2  eh
: 2323 1 0  eh
: 2325 2 0  eh
: 2328 1 0  eh
: 2330 2 -1 ~
: 2334 9 0  eh
- 2353
: 2364 2 2 Eh
: 2367 3 0  eh
: 2371 3 2  eh
: 2375 2 5  eh
: 2378 1 4 ~
: 2380 1 5  eh
: 2382 1 4 ~
: 2384 3 2  eh
: 2388 17 4  eh
- 2416
: 2420 2 2 Eh
: 2423 2 0  eh
: 2426 2 2  eh
: 2429 3 5  eh
: 2433 1 4  eh
: 2436 1 5  eh
: 2438 1 4 ~
: 2440 2 2  eh
: 2443 1 4  eh
: 2445 1 2 ~
: 2447 15 0  eh
- 2472
: 2492 2 4 Elle
: 2495 1 5  a
: 2498 1 4  dû
: 2502 8 -1  faire
: 2517 1 4  tou
: 2519 2 5 ~
: 2523 2 4 tes
: 2526 2 4  les
* 2530 2 2  guer
* 2533 2 0 ~
* 2537 9 0 res
- 2556
: 2564 2 -5 Pour
: 2567 1 -1  ê
: 2570 1 0 tre
: 2574 1 2  si
: 2577 7 -5  forte
: 2588 1 0  au
: 2591 1 2 jour
* 2595 1 2 d’hui
* 2597 6 4 ~
- 2604
: 2606 2 4 Elle
: 2609 2 5  a
: 2612 1 4  dû
: 2616 2 4  faire
: 2620 6 -1 ~
: 2632 5 5  tou
: 2638 1 4 tes
: 2640 2 4  les
: 2644 2 2  guer
: 2647 2 0 ~
: 2650 14 0 res
- 2672
: 2675 2 0 De
: 2678 1 2  la
: 2681 1 0  vie
: 2683 33 2 ~
- 2720
: 2722 1 -5 Et
: 2725 2 0 ~
: 2728 1 0  l’a
: 2731 5 0 mour,
: 2738 1 0  aus
: 2742 1 -5 si
- 2747
: 2749 3 4 Elle
: 2753 1 5  a
: 2756 1 4  dû
: 2760 6 -1  faire
: 2775 1 4  tou
: 2777 3 5 ~
: 2782 1 4 tes
: 2784 2 4  les
: 2789 1 2  guer
: 2791 2 0 ~
: 2795 11 0 res
- 2816
: 2823 1 -5 Pour
: 2826 1 -1  ê
: 2829 1 0 tre
: 2833 1 2  si
: 2836 7 -5  forte
: 2848 2 0  au
: 2851 1 2 jour
: 2855 1 2 d’hui
: 2857 6 4 ~
- 2864
: 2866 2 4 Elle
: 2869 2 5  a
: 2873 1 4  dû
: 2877 1 4  faire
: 2880 7 -1 ~
: 2893 1 4  tou
: 2895 2 5 ~
: 2899 1 4 tes
: 2902 2 4  les
: 2906 2 2  guer
: 2909 2 0 ~
: 2913 9 0 res
- 2932
: 2936 2 0 De
: 2939 2 2  la
: 2943 1 0  vie
: 2945 31 2 ~
- 2985
: 2988 1 -5 Et
: 2990 2 -2 ~
: 2993 3 0 ~
: 2998 1 0  l’a
: 3002 8 0 mour,
: 3012 2 0  aus
: 3017 2 0 si
- 3029
: 3042 3 -1 Eh
: 3046 2 -3  eh
: 3049 1 -1  eh
: 3052 2 2  eh
: 3055 1 0  eh
: 3057 2 0  eh
: 3060 2 0  eh
: 3063 1 -1 ~
: 3067 10 0  eh
- 3087
: 3097 2 2 Eh
: 3100 3 0  eh
: 3104 3 2  eh
: 3108 2 5  eh
: 3111 1 4 ~
: 3113 1 5  eh
: 3115 1 4 ~
: 3117 3 2  eh
: 3121 11 4  eh
- 3142
: 3154 2 2 Eh
: 3157 2 0  eh
: 3161 3 2  eh
: 3165 3 5  eh
: 3169 1 4  eh
: 3172 6 4  eh
: 3179 3 2  eh
: 3183 1 4  eh
: 3185 2 2 ~
: 3188 2 0  eh
: 3191 3 2 ~
: 3195 1 0 ~
: 3197 2 2 ~
: 3200 40 0 ~
E