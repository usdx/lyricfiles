#TITLE:Versteh sie einfach nicht
#ARTIST:Maeckes
#LANGUAGE:German
#GENRE:Rap
#CREATOR:Kowi-Homie
#MP3:Maeckes- Versteh sie einfach nicht [HD-VIDEO].mp3
#COVER:maeckes-null.jpg
#BACKGROUND:Genetikk - Tiefschlaf [BG].jpg
#VIDEO:Maeckes- Versteh sie einfach nicht [HD-VIDEO].mp4
#VIDEOGAP:0.00
#BPM:203.87
#GAP:66061
: 0 1 0 Ich
: 5 1 0  versteh
: 9 1 0  diese
: 13 3 0  Menschen
: 18 3 0  nicht,
: 26 1 0  wie
: 28 1 0  sie
: 31 5 0  herumrennen
- 37
: 39 2 0 Mit
: 42 3 0  abgebrochenen
: 47 11 0  Antennen,
- 61
: 64 1 0 In
: 67 2 0  den
: 70 3 0  Kellern
: 75 1 0  ihrer
: 77 5 0  Selbst,
: 84 3 0  sich
: 88 1 0  nicht
: 90 8 0  anerkennend,
- 100
: 102 1 0 Aber
: 105 1 0  sich
: 108 4 0  erkennend
: 113 2 0  an
: 118 6 0  den
: 125 3 0  Tränen
: 130 3 0  und
: 134 2 0  dem
: 138 4 0  verklemmten
: 143 2 0  Lächeln
- 145
: 146 3 0 Ich
: 150 2 0  versteh
: 154 1 0  diese
: 156 3 0  Menschen
: 161 4 0  nicht,
: 166 5 0  sie
: 174 6 0  erwarben
: 182 2 0  ihr
: 186 5 0  Leben
: 193 1 0  auf
: 195 7 0  Kredit,
- 203
: 204 1 0 Doch
: 207 1 0  sie
: 210 5 0  bezahlen
: 216 1 0  nicht
: 219 2 0  ab,
- 222
: 223 2 0 Sie
: 227 2 0  zahlen
: 230 1 0  es
: 232 5 0  zurück,
: 239 1 0  Stück
: 242 1 0  für
: 244 4 0  Stück
: 250 2 0  suchen
: 253 3 0  sie
: 257 2 0  wahres
: 261 4 0  Glück,
- 266
: 267 1 0 Und
: 270 1 0  wenn
: 272 2 0  sie
: 276 3 0  es
: 281 2 0  haben
: 284 1 0  schmeißen
: 287 1 0  sie
: 289 1 0  es
: 292 1 0  weg.
- 293
: 294 5 0 Ich
: 300 1 0  versteh
: 303 1 0  diese
: 305 1 0  Menschen
: 308 4 0  nicht,
: 321 1 0  sie
: 324 3 0  geben
: 331 2 0  Liebe,
: 336 1 0  um
: 339 4 0  Liebe
: 345 1 0  zu
: 348 5 0  kriegen,
- 353
: 354 1 0 Führen
: 357 2 0  Kriege
: 361 2 0  um
: 364 4 0  Kriege
: 370 1 0  zu
: 372 5 0  führen,
- 378
: 380 2 0 Reden,
: 383 2 0  damit
: 386 5 0  sie
: 393 1 0  sich
: 395 2 0  beim
: 398 2 0  Reden
: 402 7 0  zuhören
: 417 4 0  können,
: 425 2 0  sie
: 428 5 0  tun
: 435 2 0  alles
: 438 2 0  nur
: 441 2 0  für
: 444 2 0  sich
- 447
: 448 2 0 Ich
: 451 4 0  versteh
: 457 1 0  diese
: 459 2 0  Menschen
: 463 4 0  nicht,
: 471 1 0  sie
: 474 8 0  funktionieren
: 484 1 0  weder
: 486 3 0  in
: 491 3 0  Massen
: 496 3 0  noch
: 500 3 0  allein,
- 503
: 504 1 0 Weder
: 507 2 0  getrennt
: 511 2 0  noch
: 514 4 0  vereint,
- 518
: 519 1 0 Sie
: 522 4 0  funktionieren
: 527 5 0  nicht,
: 536 3 0  und
: 541 2 0  wenn,
: 544 3 0  nur
: 549 3 0  um
: 554 2 0  sich
: 557 8 0  fortzupflanzen,
- 567
: 569 2 0 Um
: 573 2 0  sich
: 576 1 0  zu
: 578 2 0  morden
: 581 1 0  und
: 584 2 0  zu
: 587 2 0  hassen
- 592
: 596 3 0 Ich
: 600 1 0  versteh
: 603 1 0  diese
: 606 2 0  Menschen
: 611 4 0  nicht,
: 618 2 0  mit
: 622 2 0  ihren
: 626 6 0  verschiedenen
: 634 7 0  Fahnen
- 641
: 642 2 0 Kämpfen
: 645 2 0  sie
: 649 2 0  mit
: 652 7 0  kriegerischem
: 659 6 0  Elan
: 666 2 0  für
: 669 4 0  Frieden
: 675 2 0  auf
: 678 5 0  Erden,
- 684
: 685 2 0 Traun
: 688 2 0  sich
: 692 3 0  nicht
: 696 7 0  aufzustehen,
: 704 1 0  doch
: 706 1 0  liegen
: 709 3 0  im
: 714 1 0  Sterben,
- 715
: 716 3 0 Sie
: 722 2 0  träumen
: 727 3 0  lieber
: 732 2 0  von
: 735 2 0  ihrer
: 738 3 0  Auferstehung
- 742
: 744 2 0 Ich
: 747 1 0  versteh
: 750 2 0  diese
: 753 5 0  Menschen
: 760 3 0  nicht,
: 769 2 0  sie
: 772 2 0  trauern
: 776 3 0  wegen
: 780 2 0  ihrer
: 784 6 0  Beerdigung,
- 791
: 793 3 0 Im
: 799 4 0  Voraus
: 805 2 0  -
: 808 1 0  ihr
: 811 2 0  Leben
: 814 5 0  lang
- 819
: 820 2 0 Eine
: 823 3 0  lebenslange
: 827 16 0  Todesstrafe
: 845 1 0  für
: 847 1 0  jeden,
- 849
: 850 3 0 Abzusitzen
: 855 8 0  auf
: 865 9 0  Bewährung
: 875 1 0  für
: 878 2 0  des
: 881 3 0  Menschen
: 886 7 0  Bestehen
- 893
: 894 1 0 Ich
: 897 1 0  versteh
: 900 2 0  diese
: 903 6 0  Menschen
: 910 1 0  nicht
- 1063
: 1203 1 0 Ich
: 1206 1 0  versteh
: 1208 1 0  diese
: 1211 1 0  Menschen
: 1213 4 0  nicht,
- 1218
: 1220 2 0 Seh
: 1223 1 0  die
: 1226 2 0  Idee
: 1230 2 0  hinter
: 1233 1 0  den
: 1236 4 0  Menschen
: 1241 1 0  nicht
- 1243
: 1244 5 0 Doch
: 1250 1 0  kann
: 1253 2 0  auch
: 1256 3 0  nicht
: 1261 6 0  wegschauen,
- 1267
: 1268 5 0 Denn
: 1274 1 0  einer
: 1277 2 0  dieser
: 1281 10 0  gottverdammten
: 1292 5 0  Menschen
: 1299 3 0  bin
: 1303 4 0  ich
- 1308
: 1310 3 0 Somit
: 1313 1 0  wird
: 1316 2 0  die
: 1320 4 0  Suche
: 1325 2 0  nach
: 1328 5 0  Erkenntnis
: 1334 2 0  zur
: 1337 6 0  Pflicht
- 1344
: 1345 1 0 Ich
: 1347 1 0  versteh
: 1349 2 0  diese
: 1352 3 0  Menschen
: 1356 6 0  nicht,
- 1365
: 1369 1 0 Seh
: 1372 1 0  die
: 1375 3 0  Idee
: 1380 1 0  hinter
: 1382 1 0  den
: 1384 5 0  Menschen
: 1390 6 0  nicht
- 1397
: 1398 2 0 Doch
: 1401 2 0  kann
: 1404 3 0  auch
: 1409 2 0  nicht
: 1412 6 0  wegschauen,
- 1419
: 1420 1 0 Denn
: 1422 1 0  einer
: 1425 3 0  dieser
: 1430 11 0  gottverdammten
: 1442 5 0  Menschen
: 1448 1 0  bin
: 1451 6 0  ich
- 1458
: 1459 2 0 Somit
: 1463 1 0  wird
: 1465 2 0  die
: 1469 4 0  Suche
: 1474 2 0  nach
: 1477 5 0  Erkenntnis
: 1484 1 0  zur
: 1486 5 0  Pflicht
- 1492
: 1493 2 0 Ich
: 1496 1 0  versteh
: 1499 3 0  diese
: 1503 2 0  Menschen
: 1507 7 0  nicht,
: 1518 1 0  sie
: 1521 2 0  geben
: 1525 4 0  für
: 1531 1 0  Führer
: 1534 2 0  ihre
: 1538 5 0  Stimme
: 1544 3 0  ab,
- 1548
: 1549 2 0 Und
: 1552 2 0  schweigen
: 1555 1 0  ein
: 1557 5 0  Leben
: 1564 3 0  lang,
- 1568
: 1569 2 0 Ich
: 1571 1 0  versteh
: 1574 3 0  diese
: 1578 3 0  Menschen
: 1582 6 0  nicht,
: 1592 1 0  sie
: 1595 3 0  sehnen
: 1599 1 0  sich
: 1601 2 0  nach
: 1605 13 0  Ansehen,
- 1618
: 1619 3 0 Doch
: 1623 2 0  verlieren
: 1628 4 0  was
: 1633 3 0  anderes
: 1637 1 0  aus
: 1640 2 0  den
: 1643 5 0  Augen:
: 1650 5 0  Tränen,
- 1657
: 1659 2 0 Jeder
: 1662 1 0  von
: 1664 1 0  denen
: 1667 2 0  hält
: 1670 3 0  sich
: 1674 1 0  für
: 1676 1 0  den
: 1679 8 0  Auserwählten,
- 1688
: 1689 2 0 Aber
: 1693 4 0  schweigt
: 1699 2 0  um
: 1702 1 0  nicht
: 1705 8 0  aufzufallen
- 1714
: 1715 2 0 Ich
: 1718 2 0  versteh
: 1722 5 0  diese
: 1729 5 0  Menschen
: 1736 3 0  nicht,
: 1743 1 0  diese
: 1745 1 0  Leute
: 1748 8 0  stehen
: 1757 2 0  unterm
: 1759 5 0  Joch
: 1766 2 0  von
: 1769 2 0  König
: 1772 2 0  Schönheit,
- 1776
: 1778 2 0 Doch
: 1781 1 0  sie
: 1786 1 0  stürzen
: 1789 1 0  ihn
: 1792 1 0  nicht
: 1795 1 0  wegen
: 1798 4 0  Prinz-ipientreue,
- 1807
: 1813 1 0 Diese
: 1815 3 0  Leute
: 1821 5 0  sanieren
: 1827 14 0  Regierungsgebäude
: 1843 1 0  bevor
: 1845 2 0  sie
: 1848 8 0  Krankenhäuser
: 1858 4 0  bauen,
- 1864
: 1867 2 0 Ich
: 1870 1 0  versteh
: 1873 4 0  diese
: 1878 1 0  Menschen
: 1881 3 0  nicht,
: 1885 7 0  wie
: 1893 3 0  sie
: 1897 2 0  Tag
: 1901 5 0  ein
: 1906 1 0  Tag
: 1909 2 0  aus,
- 1911
: 1912 5 0 Arbeit
: 1918 2 0  und
: 1921 5 0  Einsamkeit
: 1927 15 0  aushalten,
: 1945 1 0  Tag
: 1947 3 0  aus
: 1952 3 0  Tag
: 1957 4 0  ein,
- 1961
: 1962 2 0 Jede
: 1965 4 0  Gelegenheit
: 1970 2 0  nutzen
: 1973 5 0  mal
: 1979 21 0  auszuteilen,statt
: 2001 2 0  Leid
: 2005 2 0  auch
: 2008 4 0  mal
: 2014 2 0  zu
: 2016 2 0  teilen
- 2018
: 2019 2 0 Ich
: 2023 3 0  versteh
: 2027 1 0  diese
: 2029 2 0  Menschen
: 2032 6 0  nicht,
: 2039 1 0  sie
: 2042 2 0  brauchen
: 2044 2 0  soviel
: 2048 23 0  Aufmerksamkeit,
: 2072 2 0  doch
: 2075 2 0  können
: 2078 5 0  nur
: 2084 2 0  wenig
: 2087 5 0  schenken,
- 2092
: 2093 1 0 Ich
: 2096 1 0  versteh
: 2098 1 0  diese
: 2101 4 0  Menschen
: 2107 4 0  nicht,
: 2114 2 0  sie
: 2118 1 0  geben
: 2120 2 0  selten
: 2123 4 0  was
: 2128 1 0  auf
: 2132 13 0  Ehrlichkeit,
- 2145
: 2146 2 0 Doch
: 2149 3 0  gegen
: 2153 2 0  Aufpreis
: 2156 2 0  gern
: 2160 14 0  Aufmerksamkeit.
- 2193
: 2189 1 0 Ich
: 2192 1 0  versteh
: 2195 1 0  diese
: 2198 1 0  Menschen
: 2200 7 0  nicht,
- 2208
: 2209 2 0 Aber
: 2213 7 0  einer
: 2221 2 0  dieser
: 2225 7 0  Menschen
: 2233 2 0  bin
: 2236 6 0  ich
- 2243
: 2244 2 0 Ich
: 2247 2 0  versteh
: 2250 2 0  diese
: 2254 6 0  Menschen
: 2261 6 0  nicht,
- 2268
: 2269 2 0 Seh
: 2272 2 0  die
: 2275 4 0  Idee
: 2281 1 0  hinter
: 2283 1 0  den
: 2285 5 0  Menschen
: 2292 5 0  nicht
- 2297
: 2298 2 0 Doch
: 2301 2 0  kann
: 2304 2 0  auch
: 2308 2 0  nicht
: 2312 8 0  wegschauen,
- 2320
: 2321 2 0 Denn
: 2324 3 0  einer
: 2328 2 0  dieser
: 2331 10 0  gottverdammten
: 2342 5 0  Menschen
: 2349 2 0  bin
: 2352 6 0  ich
- 2359
: 2360 2 0 Somit
: 2363 2 0  wird
: 2366 2 0  die
: 2369 4 0  Suche
: 2375 2 0  nach
: 2378 5 0  Erkenntnis
: 2384 2 0  zur
: 2387 7 0  Pflicht
E
