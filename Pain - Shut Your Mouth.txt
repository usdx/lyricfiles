#TITLE:Shut Your Mouth
#ARTIST:Pain
#EDITION:[SC]-Songs
#LANGUAGE:Englisch
#MP3:Pain - Shut Your Mouth.mp3
#COVER:Pain - Shut Your Mouth [CO].jpg
#BACKGROUND:Pain - Shut Your Mouth [BG].jpg
#BPM:300
#GAP:72110
: 0 5 -1  The
: 6 5 -1  on
: 12 5 -1 ly
: 21 8 -1  thing
: 32 7 -1  I
: 42 6 -1  e
: 51 4 -1 ver
: 60 9 -3  wan
: 71 4 -1 ted,
- 77
: 81 4 -1  the
: 86 4 -1  on
: 91 6 -1 ly
: 101 8 -1  thing
: 112 7 -1  I
: 122 5 2  e
: 131 6 2 ver
: 141 8 2  nee
: 151 5 1 ded
- 158
: 160 4 9  is
: 166 7 11  my
: 181 10 14  own
: 196 7 6  way.
: 211 3 4  I
: 216 4 6  got
: 221 3 4 ta
: 226 4 6  have
: 231 7 7  it
: 242 4 4  a
: 247 5 2 ~ll.
- 254 292
: 312 3 11  I
: 317 3 9  don't
: 322 6 11  want
: 332 3 11  your
: 336 4 9  o
: 342 7 11 pin
: 351 4 11 ion,
- 356
: 356 4 11  I
: 361 3 9  don't
: 366 4 11  need
: 371 3 11  your
: 376 4 9  i
: 381 7 11 deas!
- 390
: 396 4 11  Stay
: 402 2 9  the
: 406 2 11  fuck
: 409 1 11  out
: 411 4 11  of
: 417 3 9  my
: 422 5 11  face,
- 429
: 435 5 11  stay
: 442 2 9  a
: 446 4 11 way
: 452 6 11  from
: 461 4 11  me
: 466 3 9 ~!
- 471 472
: 486 9 6  I´m
: 502 7 14  my
: 517 8 6  own
: 531 9 6  god,
: 542 3 4  I
: 547 3 6  do
: 551 3 6  as
: 556 3 4  I
: 561 8 4  ple
: 571 9 2 ~ase.
- 582 632
: 652 6 4  Just
: 662 6 4  wipe
: 671 7 4  your
: 681 6 4  own
: 690 7 4  ass
- 699
: 701 3 4  and
* 706 3 7  shut
* 711 2 7  your
* 715 5 7  mo
* 722 6 6 ~uth!
- 730 731
: 745 17 4 Who-
: 766 16 2 ho
- 784
: 790 9 6  I
: 802 8 6  had
: 813 6 6  e
: 822 6 6 nough
: 832 3 2  and
: 836 3 2  you're
: 841 9 2  go
: 852 5 2 ing
: 861 8 2  do
: 871 12 1 ~wn.
- 885 886
: 906 16 4 Who-
: 926 12 2 ho!
- 940 941
: 951 7 6  What
: 961 8 6  comes
: 972 6 6  a
: 982 7 6 round
: 992 2 2  you
: 996 6 2  know
: 1006 5 2  goes
: 1015 3 2  a
: 1020 8 2 ro
: 1030 4 1 ~und.
- 1036 1037
: 1052 5 -1  My
: 1061 8 -1  mind
: 1071 6 -1  is
: 1081 7 -1  play
: 1091 5 -1 ing
: 1100 6 -1  tricks
: 1111 3 -1  on
: 1116 6 -1  me,
- 1124
: 1132 3 -1  I´m
: 1136 4 -1  not
: 1141 3 -1  as
: 1145 4 -1  sta
: 1151 3 -1 ble
: 1157 3 -1  as
: 1162 3 -1  I
: 1166 3 2  used
: 1171 3 2  to
: 1176 4 2  be
: 1181 5 1 ~.
- 1188 1189
: 1206 8 11  Pushed
: 1222 6 14  and
: 1236 6 6  shoved,
- 1244
: 1250 5 6  you
: 1256 3 6  know
: 1260 4 6  you're
: 1266 4 6  go
: 1271 3 6 ing
: 1276 3 4  too
: 1282 4 4  fa
: 1287 6 2 ~r.
- 1295 1331
: 1351 3 6  I
: 1355 4 6  will
: 1360 4 4  not
: 1366 7 6  break
: 1381 6 6  my
: 1396 7 6  back
: 1411 3 6  for
: 1416 4 6  you
: 1422 3 4  no
: 1426 5 6  more.
- 1432
: 1432 3 6  I´m
: 1436 4 6  gon
: 1441 3 4 na
: 1447 6 6  go
: 1461 6 6  my
: 1476 7 6  way,
- 1485
: 1490 4 6  I´m
: 1495 4 6  gon
: 1500 3 4 na
: 1505 4 6  take
: 1511 3 6  con
: 1516 4 6 tro
: 1521 5 4 ~l.
- 1528
: 1536 8 11  Time
: 1551 6 14  to
: 1566 10 6  wake
: 1582 5 6  up
- 1589 1590
: 1600 4 4  and
: 1606 4 6  dig
: 1611 3 4  my
: 1616 4 6 self
: 1622 7 6  out
: 1631 3 6  of
: 1636 3 4  this
: 1642 6 4  he
: 1650 10 2 ~ll.
- 1662 1671
: 1691 7 4  Just
: 1701 7 4  wipe
: 1711 7 4  your
: 1721 8 4  own
: 1732 5 4  ass
- 1739
: 1741 3 4  and
* 1746 3 7  shut
* 1751 3 7  your
* 1755 4 7  mo
* 1761 6 6 ~uth!
- 1769 1770
: 1785 17 4 Who-
: 1805 17 2 ho!
- 1824 1825
: 1834 5 6  I
: 1841 7 6  had
: 1852 6 6  e
: 1861 6 6 nough
: 1871 3 2  and
: 1875 4 2  you're
: 1881 8 2  goi
: 1891 6 2 ng
: 1900 9 2  do
: 1911 10 1 ~wn.
- 1923 1924
: 1945 18 4 Who-
: 1966 15 2 ho!
- 1983
: 1990 8 6  What
: 2000 8 6  comes
: 2012 6 6  a
: 2021 7 6 round
: 2030 4 6  you
: 2036 5 6  know
: 2046 6 6  goes
: 2054 5 9  a
: 2061 9 6 rou
: 2072 14 4 ~nd
- 2088 2205
* 2225 4 7  Shut
* 2231 3 7  your
* 2236 4 7  mo
* 2241 8 6 ~uth!
- 2251 2252
: 2265 18 4 Who-
: 2286 13 2 ho!
- 2301 2302
: 2315 4 6  I
: 2322 7 6  had
: 2332 6 6  e
: 2341 7 6 nough
: 2351 4 2  and
: 2356 3 2  you're
: 2361 8 2  go
: 2371 6 2 ing
: 2380 9 2  do
: 2391 10 1 ~wn.
- 2403 2452
: 2472 6 6  What
: 2481 8 6  comes
: 2492 5 6  a
: 2501 7 6 round
: 2511 4 6  you
: 2517 4 6  know
: 2527 5 6  goes
: 2534 4 9  a
: 2541 17 6 round.
- 2560 2561
: 2585 18 4 Who-
: 2606 11 2 ho!
- 2619
* 2625 3 7  Shut
* 2630 4 7  your
* 2636 4 7  mo
* 2641 8 6 ~uth!
- 2651 2652
: 2666 17 4 Who-
: 2686 10 2 ho!
- 2698
* 2706 3 7  Shut
* 2711 3 7  your
* 2716 4 7  mo
* 2722 6 6 ~uth!
- 2730 2731
: 2746 18 4 Who-
: 2766 11 2 ho!
- 2779
* 2785 4 7  Shut
* 2791 3 7  your
* 2796 5 7  mo
* 2802 7 6 ~uth!
- 2811 2874
F 2894 22 0 What did they Say?
F 2916 30 0  Did they believe you?
- 2948 2988
F 3008 23 0 Well, it´s always like that.
F 3031 34 0  It doesn´t matter.
- 3067 3068
F 3087 36 0 You drank out of the bottle.
- 3125 3240
F 3260 37 0 In the backyard.

