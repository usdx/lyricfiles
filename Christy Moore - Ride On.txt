#TITLE:Ride On
#ARTIST:Christy Moore
#MP3:Christy Moore - Ride On.mp3
#COVER:Christy Moore - Ride On [CO].jpg
#BACKGROUND:Christy Moore - Ride On [BG].jpg
#BPM:113
#GAP:35000
#ENCODING:UTF8
: 0 6 71 True
: 6 2 71  you
: 8 6 71  ride
: 14 2 69  the
: 16 3 71  fin
: 19 4 73 est
: 23 6 74  horse
- 40
: 41 2 71 I
: 43 1 71  ĂÂ´ve
: 44 1 71  ev
: 45 1 73 er
: 46 6 74  seen
- 55
: 64 4 73 Stand
: 68 4 73 ing
: 72 3 73  six
: 75 5 73 teen
: 80 3 74  one
: 83 2 73  or
: 85 5 73  two
- 92
: 95 3 71 with
: 98 7 73  eyes
: 105 4 74  wild
: 109 2 73  and
: 111 7 71  green
- 120
: 135 2 71 You
: 137 6 71  ride
: 143 2 69  the
: 145 3 71  horse
: 148 3 73  so
: 151 5 74  well
- 158
: 161 5 71 hands
: 166 3 71  light
: 169 4 71  to
: 173 2 73  the
: 175 6 74  touch
- 183
: 195 4 73 I
: 200 2 73  could
: 202 6 73  ne
: 208 2 73 ver
: 210 2 73  go
: 212 1 74  with
: 213 2 73  you
- 215
: 215 3 73 no
: 218 2 73  mat
: 220 2 73 ter
: 222 2 71  how
: 224 1 73  I
: 225 3 74  wan
: 228 2 73 ted
: 230 3 71  to
- 235
: 257 16 71 Ride
: 273 6 74  on
- 281
: 297 8 74 see
: 305 7 71  you
- 314
: 322 7 73 I
: 329 2 73  could
: 331 5 73  ne
: 336 2 73 ver
: 338 2 74  go
: 340 2 73  with
: 342 2 73  you
- 344
: 344 2 71  no
: 346 2 73  mat
: 348 2 71 ter
: 350 3 73  how
: 353 2 74  I
: 355 2 73  wan
: 357 2 71 ted
: 359 4 71  to
- 365
: 386 15 71 Ride
: 401 7 74  on
- 410
: 426 8 74 see
: 434 6 71  you
- 442
: 451 7 73 I
: 458 2 73  could
: 460 6 73  ne
: 466 2 73 ver
: 468 2 74  go
: 470 2 73  with
: 472 2 73  you
- 474
: 474 2 71 no
: 476 2 73  mat
: 478 2 71 ter
: 480 2 73  how
: 482 2 74  I
: 484 2 73  wan
: 486 2 71 ted
: 488 4 71  to
- 494
: 516 6 71 When
: 522 3 71  you
: 525 6 71  ride
: 531 2 69  in
: 533 3 71 to
: 536 3 73  the
: 539 4 74  night
- 545
: 548 2 71 with
: 550 6 71 out
: 556 1 71  a
: 557 4 73  trace
: 561 9 74  behind
- 572
: 586 2 73 Run
: 588 2 73  your
: 590 5 73  claw
: 595 2 73  a
: 597 3 74 long
: 600 2 73  my
: 602 3 73  gut
- 607
: 622 2 71 one
: 624 4 69  last
: 628 4 71  time
- 634
: 645 1 71 I
: 646 6 71  turn
: 652 2 69  to
: 654 6 71  face
: 660 2 69  the
: 662 3 71  emp
: 665 2 73 ty
: 667 5 74  space
- 674
: 682 3 71 where
: 685 1 71  you
: 686 4 71  used
: 690 2 73  to
: 692 5 74  lie
- 699
: 709 2 73 And
: 711 2 73  look
: 713 1 73  for
: 714 2 73  the
: 716 4 73  spark
: 725 1 73  that
: 726 3 73  lights
: 729 2 74  the
: 731 3 73  night
- 736
: 738 4 73 through
: 742 1 73  a
: 743 1 74  te
: 744 3 73 ar
: 747 2 71 drop
: 749 3 71  in
: 753 4 69  my
: 757 5 71  eye
- 764
: 782 7 71 Ride
: 789 6 74  on
- 797
: 814 6 74 see
: 820 6 71  you
- 828
: 840 6 73 I
: 846 2 73  could
: 848 5 73  ne
: 853 2 73 ver
: 855 3 74  go
: 858 2 73  with
: 860 2 73  you
- 862
: 862 2 73 no
: 864 4 71  matter
: 868 1 73  how
: 870 2 74  I
: 872 2 73  wan
: 874 2 71 ted
: 876 4 71  to
- 882
: 904 15 71 Ride
: 919 5 74  on
- 926
: 944 9 74 see
: 953 4 71  you
- 959
: 969 6 73 I
: 975 2 73  could
: 977 6 73  ne
: 983 2 73 ver
: 985 2 74  go
: 987 2 73  with
: 989 2 73  you
- 991
: 991 2 73 no
: 993 4 71  matter
: 997 2 73  how
: 999 2 74  I
: 1001 2 73  wan
: 1003 2 71 ted
: 1005 2 71  to
E