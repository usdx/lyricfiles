#TITLE:Himbeereis Zum Frühstück
#ARTIST:Hoffmann & Hoffmann
#LANGUAGE:Deutsch
#MP3:Hoffmann & Hoffmann - Himbeereis Zum Frühstück.mp3
#COVER:Hoffmann & Hoffmann - Himbeereis Zum Frühstück [CO].jpg
#VIDEO:Hoffmann & Hoffmann - Himbeereis Zum Frühstück.mpg
#VIDEOGAP:-9
#BPM:130
#GAP:15800
: 0 4 9 Du 
: 14 1 0 ich 
: 16 3 14 seh 
: 20 2 14 dich 
: 22 2 12 noch 
: 26 2 9 wie 
: 30 5 7 heut'
- 35
: 46 1 0 du 
: 48 3 14 trugst 
: 52 2 14 ein 
: 54 2 12 Hoch
: 58 3 9 zeits
: 64 4 5 kleid
- 68
: 78 1 2 Und 
: 80 3 14 bald 
: 84 1 14 schon 
: 86 3 12 soll
: 90 1 9 test 
: 92 3 9 du 
: 96 7 10 sei
: 104 7 9 ne 
: 112 7 7 Frau 
: 120 4 5 sein
: 124 3 7 
- 127
: 128 4 9 Ich 
: 142 1 0 wollt 
: 144 2 14 dich 
: 146 2 14 aus 
: 150 2 12 Spaß 
: 154 2 9 ent
: 158 5 7 führn' 
- 163
: 172 2 0 und 
: 174 1 0 ich 
: 176 2 14 brach
: 180 2 14 te 
: 182 2 12 Dich 
: 186 2 9 zu 
: 192 5 7 mir
- 197
: 206 1 7 Es 
: 208 3 14 war 
: 212 1 14 ein 
: 214 5 7 Spiel, 
: 220 1 10 doch 
: 222 1 12 dann 
: 224 7 10 bliebst 
: 232 8 9 du 
: 240 5 7 hier 
- 245
: 248 2 9 Him
: 250 1 12 beer
: 252 2 12 eis 
: 254 2 9 zum 
: 256 8 14 Früh
: 264 3 12 stück, 
- 267
: 280 2 14 Rock 
: 282 1 12 'n 
: 284 2 9 Roll 
: 286 2 7 im 
: 288 7 9 Fahr
: 296 4 9 stuhl,
- 300
: 312 2 9 du 
: 314 1 12 und 
: 316 2 12 ich, 
: 318 2 9 wir 
: 320 8 14 war
: 328 4 14 en 
: 344 2 12 hoff
: 346 2 14 nungs
: 348 2 12 los 
: 350 2 9 ve
: 352 3 7 rrückt 
- 355
: 376 2 9 Him
: 378 1 12 beer
: 380 2 12 eis 
: 382 2 9 zum 
: 384 8 14 Früh
: 392 3 12 stück, 
- 395
: 406 4 14 träu
: 410 2 12 mend 
: 412 2 9 durch 
: 414 2 7 den 
: 416 7 9 Som
: 424 3 9 mer, 
- 427
: 440 2 14 Mit 
: 442 1 14 der 
: 444 2 14 Berg 
: 446 2 9 und 
: 448 7 14 Tal
: 456 4 14 bahn 
: 460 6 12 
: 472 2 12 fuh
: 474 2 12 ren 
: 476 2 12 wir 
: 478 1 12 ins 
: 480 2 9 Glück 
: 482 3 5 
- 485
: 512 4 9 Du 
: 526 1 0 die 
: 528 3 14 Zeit 
: 532 1 14 ver
: 534 2 12 ging 
: 538 2 9 im 
: 542 6 7 Flug,
- 548
: 558 1 0 ich 
: 560 2 14 hat
: 562 2 14 te 
: 566 2 12 nie 
: 570 2 9 ge
: 576 1 7 nug
: 577 1 5 
: 578 2 2 
: 590 1 2 
- 591
: 592 2 14 von 
: 594 2 14 dir 
: 598 2 12 und 
: 602 2 9 deinen 
: 604 2 12 verr
: 608 7 10 ück
: 616 7 9 ten 
: 624 7 7 Träu
: 632 4 5 men
- 636
: 636 3 7 Auch 
: 640 4 9 wenn 
: 654 1 0 ich 
: 656 2 14 dich 
: 658 2 14 nicht 
: 662 2 12 hal
: 666 2 9 ten 
: 670 5 7 kann, 
- 675
: 684 2 0 zieh 
: 686 1 0 das 
: 688 2 14 Kleid 
: 692 2 14 noch 
: 694 2 12 ein
: 698 2 9 mal 
: 704 5 7 an; 
- 709
: 718 1 7 dein 
: 720 3 14 Hoch
: 724 1 14 zeits
: 726 5 7 kleid, 
: 732 1 10 denn 
: 734 1 12 so 
: 736 7 10 fing 
: 744 8 9 es 
: 752 2 7 an. 
- 754
: 760 2 9 Him
: 762 1 12 beer
: 764 2 12 eis 
: 766 2 9 zum 
: 768 8 14 Früh
: 776 3 12 stück, 
- 779
: 792 2 14 Rock 
: 794 1 12 'n 
: 796 2 9 Roll 
: 798 2 7 im 
: 800 7 9 Fahr
: 808 4 9 stuhl,
- 812
: 824 2 9 du 
: 826 1 12 und 
: 828 2 12 ich, 
: 830 2 9 wir 
: 832 8 14 war
: 840 4 14 en 
: 856 2 12 hoff
: 858 2 14 nungs
: 860 2 12 los 
: 862 2 9 ve
: 864 3 7 rrückt 
- 867
: 888 2 9 Him
: 890 1 12 beer
: 892 2 12 eis 
: 894 2 9 zum 
: 896 8 14 Früh
: 904 3 12 stück, 
- 907
: 918 4 14 träu
: 922 2 12 mend 
: 924 2 9 durch 
: 926 2 7 den 
: 928 7 9 Som
: 936 3 9 mer, 
- 939
: 952 2 9 Mit 
: 954 1 12 der 
: 956 2 12 Berg 
: 958 2 9 und 
: 960 7 14 Tal
: 968 4 14 bahn 
: 972 6 12 
: 984 2 12 fuh
: 986 2 12 ren 
: 988 2 12 wir 
: 990 1 12 ins 
: 992 5 5 Glück 
- 997
: 1240 2 9 Him
: 1242 1 12 beer
: 1244 2 12 eis 
: 1246 2 9 zum 
: 1248 8 14 Früh
: 1256 3 12 stück, 
- 1259
: 1272 2 14 Rock 
: 1274 1 12 'n 
: 1276 2 9 Roll 
: 1278 2 7 im 
: 1280 7 9 Fahr
: 1288 4 9 stuhl,
- 1292
: 1304 2 9 du 
: 1306 1 12 und 
: 1308 2 12 ich, 
: 1310 2 9 wir 
: 1312 8 14 war
: 1320 4 14 en 
: 1336 2 12 hoff
: 1338 2 14 nungs
: 1340 2 12 los 
: 1342 2 9 ve
: 1344 3 7 rrückt 
- 1347
: 1368 2 9 Him
: 1370 1 12 beer
: 1372 2 12 eis 
: 1374 2 9 zum 
: 1376 8 14 Früh
: 1384 3 12 stück, 
- 1387
: 1398 4 14 träu
: 1402 2 12 mend 
: 1404 2 9 durch 
: 1406 2 7 den 
: 1408 7 9 Som
: 1416 3 9 mer, 
- 1419
: 1432 2 9 Mit 
: 1434 1 12 der 
: 1436 2 12 Berg 
: 1438 2 9 und 
: 1440 7 14 Tal
: 1448 4 14 bahn 
: 1452 7 12 
: 1464 2 12 fuh
: 1466 2 12 ren 
: 1468 2 12 wir 
: 1470 1 12 ins 
: 1472 2 9 Glück
- 1474
: 1474 3 5  
: 1560 2 14 Him
: 1562 2 14 beer
: 1564 2 14 eis 
: 1566 2 14 zum 
: 1568 8 16 Früh
: 1576 3 14 stück, 
- 1579
: 1624 2 14 Rock 
: 1626 1 12 'n 
: 1628 2 9 Roll 
: 1630 2 7 im 
: 1632 7 9 Fahr
: 1640 4 9 stuhl 
- 1644
: 1688 2 14 Him
: 1690 2 14 beer
: 1692 2 14 eis 
: 1694 2 14 zum 
: 1696 8 16 Früh
: 1704 3 14 stück, 
E
