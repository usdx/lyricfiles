#TITLE:Down On The Corner
#ARTIST:CCR
#MP3:CCR - Down On The Corner.mp3
#COVER:CCR - Down On The Corner [CO].jpg
#BACKGROUND:CCR - Down On The Corner [BG].jpg
#BPM:108
#GAP:23000
: 1 2 72 Ear
: 3 3 72 ly
: 6 1 72  in
: 7 1 76  the
: 8 2 79  eve
: 10 3 79 nin
- 15
: 16 3 79 Just
: 19 1 79  a
: 20 1 79 bout
: 21 2 79  sup
: 23 1 76 per
: 24 5 72  time
- 31
: 33 3 72 Ov
: 36 2 72 er
: 38 1 72  by
: 40 1 76  the
: 41 2 79  court
: 43 4 79 house
- 47
: 47 2 79 they're
: 49 3 79  start
: 52 2 79 ing
: 54 1 79  to
: 55 2 79  un
: 57 1 79 wi
: 58 3 79 nd
- 63
: 65 3 77 Four
: 68 2 77  kids
: 70 2 77  on
: 72 1 81  the
: 73 5 81  corner
- 80
: 82 3 81 trying
: 85 1 79  to
: 86 2 79  bring
: 88 1 79  you
: 89 3 81  up
- 94
: 98 1 72 Wil
: 99 2 72 ly
: 101 2 72  picks
: 103 2 76  a
: 105 2 79  tune
: 107 3 79  out
- 110
: 110 3 79 and
: 113 1 79  he
: 114 3 76  blows
: 117 1 79  it
: 118 2 79  on
: 120 1 79  the
: 121 3 76  harp
- 126
: 130 2 77 Do
: 132 2 77 wn
: 134 2 77  on
: 136 1 76  the
: 137 6 76  corner,
: 145 4 79  out
: 149 3 79  in
: 152 1 79  the
: 153 4 76  street
- 157
: 157 2 76 Wil
: 159 1 76 ly
: 160 1 76  and
: 161 1 76  the
: 162 2 77  Poor
: 164 2 77  boy
: 166 1 77 s
: 167 2 76  are
: 169 3 76  playin
- 174
: 174 2 76 Bring
: 176 1 74  a
: 177 2 74  nick
: 179 3 74 el
: 182 7 76  tap your feet
- 192
: 194 2 72 Roos
: 196 2 72 ter
: 198 2 72  hits
: 200 1 76  the
: 201 2 79  wash
: 203 4 79 board
- 208
: 209 2 79 peo
: 211 1 79 ple
: 212 2 79  just
: 214 2 76  got
: 216 6 72  to smile
- 224
: 225 3 72 Blink
: 228 1 72 y
: 229 3 72  thumps
: 232 1 76  the
: 233 1 79  gut
: 234 3 79  bass
- 237
: 237 4 79 and
: 241 3 79  so
: 244 2 79 los
: 246 2 79  for
: 248 1 79  a
: 249 2 79 whi
: 251 2 79 le
- 255
: 258 2 77 Poor
: 260 2 77 boy
: 262 3 77  twangs
: 265 1 81  the
: 266 7 81  rhythm out
- 274
: 274 2 81 on
: 276 1 79  his
: 277 2 79  kal
: 279 2 79 ama
: 281 4 81 zoo
- 287
: 289 2 72 Wil
: 291 2 72 ly
: 293 3 72  goes
: 296 1 76  in
: 297 2 79  to
: 299 1 79  a
: 300 5 79  dance
- 305
: 305 1 79 and
: 306 1 76  dou
: 307 2 79 bles
: 309 3 79  on
: 312 1 79  ka
: 313 2 76 zoo
- 318
: 322 2 77 Do
: 324 3 77 wn
: 327 1 77  on
: 328 1 76  the
: 329 6 76  corner,
: 337 4 79  out
: 341 3 79  in
: 344 1 79  the
: 345 4 76  street
- 349
: 349 2 76 Wil
: 351 1 76 ly
: 352 1 76  and
: 353 1 76  the
: 354 2 77  Poor
: 356 5 77  boys
: 361 1 77  are
: 362 3 76  playin
- 365
: 365 3 76 Bring
: 368 1 76  a
: 369 2 74  nick
: 371 3 74 el
: 374 2 74  tap'
: 376 4 76   your feet
- 421
: 513 3 77 Do
: 516 2 77 wn
: 518 2 77  on
: 520 1 76  the
: 521 6 76  corner,
: 529 4 79  out
: 533 3 79  in
: 536 1 79  the
: 537 4 76  street
- 542
: 542 1 76 Wil
: 543 1 76 ly
: 544 1 76  and
: 545 1 76  the
: 546 2 77  Poor
: 548 3 77  boy
: 551 1 77 s
: 552 1 76  are
: 553 4 76  playin
- 558
: 558 2 76 Bring
: 560 1 74  a
: 561 2 74  nick
: 563 3 74 el
: 566 6 76  tap' your feet
- 600
: 640 3 72 You
: 643 1 72  don't
: 644 2 72  need
: 646 2 76  a
: 648 1 79  pen
: 649 3 79 ny
- 654
: 656 3 79 just
: 659 1 79  to
: 660 3 79  hang
: 663 1 79  a
: 664 4 76 round
- 669
: 671 1 72 But
: 672 2 72  if
: 674 2 72  you've
: 676 2 76  got
: 678 1 79  a
: 679 2 79  nick
: 681 2 79 el
- 684
: 684 3 79 won't
: 687 2 79  you
: 689 1 79  lay
: 690 2 79  your
: 692 2 79  mon
: 694 1 79 ey
: 695 5 76  down?
- 702
: 704 2 77 Ov
: 706 2 77 er
: 708 2 77  on
: 710 1 81  the
: 711 3 81  cor
: 714 2 81 ner
- 718
: 720 3 79 there's
: 723 1 79  a
: 724 2 79  hap
: 726 1 81 py
: 727 5 76  noise
- 734
: 737 2 72 People
: 739 3 72  come
: 742 2 76  from
: 744 2 79  all
: 746 1 79  a
: 747 4 79 round
- 751
: 751 1 79 to
: 752 2 76  watch
: 754 1 79  the
: 755 3 79  mag
: 758 1 79 ic
: 759 4 76  boy
- 765
: 768 3 77 Do
: 771 2 77 wn
: 773 1 77  on
: 774 1 76  the
: 775 6 76  corner,
: 783 5 79  out
: 788 2 79  in
: 790 1 79  the
: 791 4 76  street
- 796
: 796 1 76 Wil
: 797 1 76 ly
: 798 1 76  and
: 799 1 76  the
: 800 2 77  Poor
: 802 4 77  boy
: 806 1 77 s
: 807 1 76  are
: 808 4 76  playin
- 812
: 812 2 76 Bring
: 814 1 74  a
: 815 2 74  nick
: 817 3 74 el
: 820 7 76  tap' your feet
- 830
: 832 2 77 Do
: 834 2 77 wn
: 836 2 77  on
: 838 1 76  the
: 839 6 76  corner,
: 847 5 79  out
: 852 2 79  in
: 854 1 79  the
: 855 4 76  street
- 860
: 860 1 76 Wil
: 861 1 76 ly
: 862 1 76  and
: 863 1 76  the
: 864 2 77  Poor
: 866 3 77  boy
: 869 1 77 s
: 870 1 76  are
: 871 4 76  playin
- 875
: 875 3 76 Bring
: 878 1 74  a
: 879 2 74  nick
: 881 3 74 el
: 884 7 76  tap' your feet
E
