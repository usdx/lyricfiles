#TITLE:Prayer in C (Robin Schulz Remix)
#ARTIST:Lilly Wood & The Prick and Robin Schulz
#MP3:Lilly Wood & The Prick and Robin Schulz - Prayer in C (Robin Schulz Remix).mp3
#VIDEO:Lilly Wood & The Prick and Robin Schulz - Prayer in C (Robin Schulz Remix).mp4
#COVER:Lilly Wood & The Prick and Robin Schulz - Prayer in C (Robin Schulz Remix).jpg
#BPM:245.95
#GAP:23887
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Dance
#YEAR:2014
#AUTHOR:mustangfred and asb
: 0 1 -1 Yeah
: 2 3 0 ~
* 11 1 -1  you
* 13 1 0 ~
: 16 2 4  nev
: 19 1 2 er
: 24 3 2  said
: 28 3 0  a
: 32 1 0  word
- 34
: 36 1 0 You
: 40 3 0  did
: 44 1 0 n't
: 48 2 4  send
: 52 2 2  me
: 55 3 0  no
* 60 3 2  let
* 64 4 2 ter
- 74
: 76 2 0 Don't
: 79 1 2  think
: 81 1 4 ~
: 84 2 2  I
: 88 1 0  could
: 91 2 2  for
: 96 2 2 give
: 100 5 2  you?
- 115
: 128 9 0 See
: 139 3 0  our
* 144 3 4  world
: 148 1 2  is
: 152 2 2  slow
: 155 2 0 ly
: 160 2 2  dy
: 164 1 0 ing
- 166
: 168 2 0 I'm
: 172 2 0  not
: 176 2 4  wast
: 180 2 2 ing
: 184 3 2  no
: 188 1 0  more
: 192 1 0  time
: 194 1 2 ~
- 201
: 204 1 0 Don't
: 207 3 4  think
: 212 1 2  I
: 216 2 0  could
: 220 3 2  be
: 224 3 2 lieve
: 229 2 2  you
: 232 2 4 ~
: 236 2 0 ~?
- 270
: 512 1 -1 Yeah
: 514 4 0 ~
: 523 1 -1  you
: 525 1 0 ~
: 527 1 2  nev
: 529 1 4 ~
* 531 1 2 er
: 536 2 2  said
: 539 3 0  a
: 544 1 0  word
- 546
: 548 1 0 You
: 552 2 0  did
: 555 2 0 n't
: 560 2 4  send
: 564 2 2  me
: 568 2 0  no
: 572 2 2  let
: 575 4 2 ter
- 585
: 587 2 0 Don't
: 591 1 2  think
: 593 1 4 ~
: 596 2 2  I
: 600 1 0  could
: 604 2 2  for
: 607 3 2 give
: 612 3 2  you?
: 616 2 4 ~
- 628
: 639 8 0 See
: 651 3 0  our
* 655 1 2  world
* 657 2 4 ~
: 660 1 2  is
: 664 2 2  slow
: 667 2 0 ly
: 671 3 2  dy
: 676 1 0 ing
- 678
: 680 2 0 I'm
: 683 2 0  not
* 688 1 4  wast
* 691 3 2 ing
: 696 2 2  no
: 699 2 0  more
: 703 4 0  time
- 713
: 715 2 0 Don't
: 719 1 2  think
: 721 1 4 ~
: 724 1 2  I
: 728 2 0  could
: 732 2 2  be
: 735 3 2 lieve
* 740 2 2  you
* 743 2 4 ~
* 748 3 0 ~?
- 761
: 768 1 4 Yeah
: 770 1 2 ~
: 779 2 0  our
* 783 2 4  hands
: 787 2 2  will
: 791 2 2  get
: 796 2 0  more
: 799 2 2  wrin
: 803 2 0 kled
- 806
: 808 2 0 And
: 812 2 0  my
: 816 2 4  hair
: 819 2 2 ~
: 823 3 2  will
: 827 1 0  be
: 831 4 0  grey
- 841
: 844 2 0 Don't
: 847 1 2  think
: 849 1 4 ~
: 852 2 2  I
: 856 1 0  could
: 859 2 2  for
: 864 2 2 give
* 868 2 2  you
* 871 4 0 ~?
- 884
: 888 1 4 And
: 890 1 2 ~
* 896 1 4  see
* 898 2 2 ~
* 901 2 4 ~
: 907 1 0  the
: 911 1 2  chil
: 913 1 4 ~
: 915 2 2 dren
: 919 3 2  are
: 927 3 2  starv
: 931 3 0 ing
- 935
: 937 1 0 And
: 940 2 0  their
: 943 3 4  hous
: 948 2 2 es
: 952 2 2  were
: 955 1 0  de
: 959 6 0 stroyed
- 970
: 972 1 0 Don't
: 975 1 2  think
: 977 1 4 ~
: 980 1 2  they
: 983 2 0  could
: 987 2 2  for
: 991 3 2 give
: 996 5 2  you
: 1004 3 0 ~?
- 1017
: 1024 1 4 Hey
: 1026 1 2 ~
: 1035 2 0  when
: 1040 2 4  seas
: 1044 1 2  will
: 1048 3 2  cov
: 1052 1 0 er
: 1056 4 0  lands
- 1062
: 1064 2 0 And
: 1068 2 0  when
: 1071 3 4  man
: 1075 2 2  will
: 1079 3 2  be
: 1083 3 0  no
: 1087 4 0  more
- 1097
: 1099 2 0 Don't
: 1104 2 4  think
: 1108 1 2  you
: 1112 1 0  can
: 1115 2 2  for
: 1119 3 2 give
: 1124 2 2  you
: 1127 7 0 ~?
- 1141
* 1144 1 4 Yeah
* 1146 2 2 ~
: 1152 1 4  when
: 1154 4 2 ~
: 1163 2 0  there'll
: 1167 1 4  just
: 1169 2 2 ~
: 1175 3 2  be
: 1183 3 2  si
: 1187 1 0 lence
- 1190
: 1192 2 0 And
: 1196 2 0  when
: 1199 1 2  life
: 1201 1 4 ~
: 1205 1 2  will
: 1208 2 2  be
: 1216 3 2  o
: 1220 4 0 ver
- 1226
: 1228 2 0 Don't
: 1231 1 2  think
: 1233 1 4 ~
: 1236 2 2  you
: 1240 1 0  will
: 1244 1 2  for
: 1247 3 2 give
: 1252 4 2  you
: 1260 3 0 ~?
- 1295
: 1408 1 -1 Yeah
: 1410 2 0 ~
: 1419 2 0  you
: 1423 3 4  nev
: 1427 1 2 er
: 1431 3 2  said
: 1435 3 0  a
: 1439 2 0  word
- 1442
: 1444 1 0 You
: 1447 3 0  did
: 1451 1 0 n't
: 1456 2 4  send
: 1460 1 2  me
: 1463 3 0  no
: 1468 2 2  let
: 1471 3 2 ter
- 1480
: 1483 2 0 Don't
: 1487 2 4  think
: 1492 1 2  I
: 1495 1 0  could
: 1499 2 2  for
: 1503 3 2 give
: 1508 3 2  you?
- 1521
: 1536 4 0 See
: 1546 3 0  our
* 1552 2 4  world
: 1555 1 2  is
: 1560 2 2  slow
: 1563 1 0 ly
: 1568 2 2  dy
: 1571 2 0 ing
- 1574
: 1576 2 0 I'm
: 1579 2 0  not
: 1583 2 4  wast
: 1588 1 2 ing
: 1591 2 2  no
: 1596 1 0  more
: 1600 1 0  time
: 1602 1 2 ~
- 1609
: 1611 1 0 Don't
: 1615 2 4  think
: 1619 2 2  I
: 1624 1 0  could
: 1628 2 2  be
: 1632 2 2 lieve
: 1636 6 2  you
: 1644 2 0 ~?
- 1678
: 1920 1 -1 Yeah
: 1922 4 0 ~
: 1931 2 0  you
: 1935 3 4  nev
: 1939 1 2 er
: 1943 3 2  said
: 1947 3 0  a
: 1951 2 0  word
- 1954
: 1956 1 0 You
: 1960 2 0  did
: 1963 2 0 n't
: 1967 1 2  send
: 1969 1 4 ~
: 1972 2 2  me
: 1975 3 0  no
: 1979 3 2  let
: 1983 3 2 ter
- 1992
: 1995 2 0 Don't
: 1999 1 2  think
: 2001 1 4 ~
: 2003 2 2  I
: 2007 1 0  could
: 2011 2 2  for
: 2015 3 2 give
: 2020 3 2  you?
: 2024 1 4 ~
- 2035
: 2048 6 0 See
: 2059 3 0  our
: 2063 4 4  world
: 2068 1 2  is
: 2072 2 2  slow
: 2075 1 0 ly
* 2079 3 2  dy
* 2084 1 0 ing
- 2086
: 2088 2 0 I'm
: 2091 2 0  not
: 2095 2 4  wast
: 2099 2 2 ing
: 2104 2 2  no
: 2107 2 0  more
: 2111 4 0  time
- 2121
: 2123 2 0 Don't
: 2127 2 4  think
: 2131 2 2  I
: 2135 2 0  could
: 2139 2 2  be
: 2143 3 2 lieve
: 2148 2 2  you
: 2151 2 4 ~
: 2155 4 0 ~?
- 2169
: 2176 1 4 Yeah
: 2178 1 2 ~
: 2187 2 0  our
: 2191 2 4  hands
: 2195 1 2  will
: 2199 2 2  get
: 2203 2 0  more
* 2207 1 2  wrin
* 2211 2 0 kled
- 2214
: 2216 2 0 And
: 2219 2 0  my
: 2223 3 4  hair
: 2227 2 2 ~
: 2232 2 2  will
: 2235 1 0  be
: 2239 4 0  grey
- 2249
: 2251 1 0 Don't
: 2255 2 4  think
: 2259 2 2  I
: 2263 1 0  could
: 2267 2 2  for
: 2271 2 2 give
: 2275 2 2  you
: 2279 2 0 ~?
- 2292
: 2296 1 4 And
: 2298 1 2 ~
: 2303 2 4  see
: 2306 5 2 ~
: 2315 1 0  the
: 2319 1 2  chil
: 2321 1 4 ~
: 2323 2 2 dren
: 2326 4 2  are
: 2335 3 2  starv
: 2339 1 0 ing
- 2342
: 2344 2 0 And
: 2347 2 0  their
: 2351 2 4  hous
: 2356 1 2 es
: 2359 3 2  were
: 2363 1 0  des
: 2367 4 0 troyed
- 2377
: 2379 1 0 Don't
: 2383 2 4  think
: 2387 2 2  they
: 2391 1 0  could
: 2395 1 2  for
: 2399 3 2 give
: 2403 5 2  you
: 2412 3 0 ~?
- 2427
: 2431 2 4 Hey
: 2434 1 2 ~
: 2443 2 0  when
: 2448 2 4  seas
: 2451 1 2  will
: 2455 3 2  cov
: 2459 1 0 er
: 2463 5 0  lands
- 2470
: 2472 2 0 And
: 2475 2 0  when
* 2479 3 4  man
: 2483 2 2  will
: 2487 2 2  be
: 2491 2 0  no
: 2495 2 0  more
: 2498 1 2 ~
- 2505
: 2507 2 0 Don't
: 2511 2 4  think
: 2515 2 2  you
: 2520 1 0  can
: 2523 1 2  for
: 2527 3 2 give
: 2532 2 2  you
: 2535 7 0 ~?
- 2549
: 2552 1 4 Yeah
: 2554 2 2 ~
* 2559 1 4  when
* 2561 4 2 ~
: 2571 2 0  there'll
: 2575 1 4  just
: 2577 2 2 ~
: 2583 4 2  be
: 2591 3 2  si
: 2595 1 0 lence
- 2597
: 2599 2 0 And
: 2603 2 0  when
: 2607 1 2  life
: 2609 1 4 ~
: 2612 1 2  will
: 2615 3 2  be
: 2623 2 2  o
: 2628 3 0 ver
- 2633
: 2635 2 0 Don't
: 2639 2 4  think
: 2643 2 2  you
: 2647 2 0  will
: 2651 1 2  for
: 2655 3 2 give
: 2660 3 2  you
: 2664 2 4 ~
: 2668 6 0 ~?
E