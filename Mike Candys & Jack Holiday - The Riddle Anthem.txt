#TITLE:The Riddle Anthem
#ARTIST:Mike Candys & Jack Holiday
#MP3:Mike Candys & Jack Holiday - The Riddle Anthem.mp3
#VIDEO:Mike Candys & Jack Holiday - The Riddle Anthem.mp4
#COVER:Mike Candys & Jack Holiday - The Riddle Anthem.jpg
#BPM:254,25
#GAP:73560
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Dance & Electronic
#EDITION:Songs von aac
#AUTHOR:Nicolai
: 0 2 6 Near
: 4 2 8  a
: 8 6 9  tree
: 16 2 9  by
: 20 2 11  a
: 24 2 9  ri
: 28 2 8 ver
- 30
: 32 2 6 There's
: 36 2 4  a
: 40 6 4  hole
: 48 2 6  in
: 52 2 8  the
: 56 6 8  ground
- 62
: 64 3 6 Where
: 68 2 8  an
* 72 6 9  old
: 80 3 9  man
: 84 2 11  of
: 88 3 9  A
: 92 2 8 ran
- 94
: 96 3 6 Goes
: 100 2 4  a
: 104 6 6 round
: 112 3 6  and
: 116 2 4  a
: 120 6 4 round
- 126
: 128 3 6 And
: 132 3 8  his
: 136 6 9  mind
: 144 3 9  is
: 148 2 11  a
: 152 3 9  bea
: 156 3 8 con
- 159
: 161 2 6 In
: 164 2 4  the
* 168 7 4  veil
: 176 3 6  of
: 180 2 8  the
: 184 6 8  night
- 190
: 192 2 6 For
: 196 2 8  a
: 200 5 9  strange
: 208 3 9  kind
: 212 3 11  of
: 216 2 9  fa
: 220 3 8 shion
- 223
: 224 3 6 There's
: 228 2 4  a
: 232 6 6  wrong
: 240 3 6  and
: 244 2 4  a
* 248 6 4  right
- 255
: 257 2 6 Near
: 261 2 8  a
: 265 6 9  tree
: 273 2 9  by
: 277 2 11  a
: 281 2 9  ri
: 285 2 8 ver
- 287
: 289 2 6 There's
: 293 2 4  a
: 297 6 4  hole
: 305 2 6  in
: 309 2 8  the
: 313 6 8  ground
- 319
: 321 3 6 Where
: 325 2 8  an
* 329 6 9  old
: 337 3 9  man
: 341 2 11  of
: 345 3 9  A
: 349 2 8 ran
- 351
: 353 3 6 Goes
: 357 2 4  a
* 361 6 6 round
: 369 3 6  and
: 373 2 4  a
: 377 6 4 round
- 383
: 385 3 6 And
: 389 3 8  his
: 393 6 9  mind
: 401 3 9  is
: 405 2 11  a
: 409 3 9  bea
: 413 3 8 con
- 416
: 418 2 6 In
: 421 2 4  the
* 425 7 4  veil
: 433 3 6  of
: 437 2 8  the
: 441 6 8  night
- 447
: 449 2 6 For
: 453 2 8  a
: 457 5 9  strange
: 465 3 9  kind
: 469 3 11  of
: 473 2 9  fa
: 477 3 8 shion
- 480
: 481 3 6 There's
: 485 2 4  a
: 489 6 6  wrong
: 497 3 6  and
: 501 2 4  a
* 505 6 4  right
- 544
: 1089 2 6 Near
: 1093 2 8  a
: 1097 6 9  tree
: 1105 2 9  by
: 1109 2 11  a
: 1113 2 9  ri
: 1117 2 8 ver
- 1119
: 1121 2 6 There's
: 1125 2 4  a
: 1129 6 4  hole
: 1137 2 6  in
: 1141 2 8  the
: 1145 6 8  ground
- 1151
: 1153 3 6 Where
: 1157 2 8  an
* 1161 6 9  old
: 1169 3 9  man
: 1173 2 11  of
: 1177 3 9  A
: 1181 2 8 ran
- 1183
: 1185 3 6 Goes
: 1189 2 4  a
* 1193 6 6 round
: 1201 3 6  and
: 1205 2 4  a
: 1209 6 4 round
- 1215
: 1217 3 6 And
: 1221 3 8  his
: 1225 6 9  mind
: 1233 3 9  is
: 1237 2 11  a
: 1241 3 9  bea
: 1245 3 8 con
- 1248
: 1250 2 6 In
: 1253 2 4  the
* 1257 7 4  veil
: 1265 3 6  of
: 1269 2 8  the
: 1273 6 8  night
- 1279
: 1281 2 6 For
: 1285 2 8  a
: 1289 5 9  strange
: 1297 3 9  kind
: 1301 3 11  of
: 1305 2 9  fa
: 1309 3 8 shion
- 1312
: 1313 3 6 There's
: 1317 2 4  a
: 1321 6 6  wrong
: 1329 3 6  and
: 1333 2 4  a
: 1337 6 4  right
- 1343
: 1345 2 6 Near
: 1349 2 8  a
: 1353 6 9  tree
: 1361 2 9  by
: 1365 2 11  a
: 1369 2 9  ri
: 1373 2 8 ver
- 1375
: 1377 2 6 There's
: 1381 2 4  a
: 1385 6 4  hole
: 1393 2 6  in
: 1397 2 8  the
* 1401 6 8  ground
- 1407
: 1409 3 6 Where
: 1413 2 8  an
: 1417 6 9  old
: 1425 3 9  man
: 1429 2 11  of
: 1433 3 9  A
: 1437 2 8 ran
- 1439
: 1441 3 6 Goes
: 1445 2 4  a
* 1449 6 6 round
: 1457 3 6  and
: 1461 2 4  a
: 1465 6 4 round
- 1471
: 1473 3 6 And
: 1477 3 8  his
: 1481 6 9  mind
: 1489 3 9  is
: 1493 2 11  a
: 1497 3 9  bea
: 1501 3 8 con
- 1504
: 1506 2 6 In
: 1509 2 4  the
: 1513 7 4  veil
: 1521 3 6  of
* 1525 2 8  the
* 1529 6 8  night
- 1535
: 1537 2 6 For
: 1541 2 8  a
: 1545 5 9  strange
: 1553 3 9  kind
: 1557 3 11  of
: 1561 2 9  fa
: 1565 3 8 shion
- 1568
: 1569 3 6 There's
: 1573 2 4  a
: 1577 6 6  wrong
: 1585 3 6  and
: 1589 2 4  a
: 1593 6 4  right
- 1599
: 1601 3 4 But
* 1605 3 16  he'll
* 1609 4 14  ne
* 1614 2 13 ver
* 1617 3 11  Ne
* 1621 3 9 ver
* 1625 5 6  fight
: 1634 2 4  o
: 1637 4 6 ver
: 1642 19 6  you
E