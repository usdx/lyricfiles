#TITLE:No fear
#ARTIST:The Rasmus
#MP3:The Rasmus - No fear.mp3
#COVER:The Rasmus - No fear [CO].jpg
#BACKGROUND:The Rasmus - No fear [BG].jpg
#BPM:117
#GAP:38000
#ENCODING:UTF8
: 0 8 32 Girl
- 7
: 24 2 32 You
: 26 2 32  live
: 28 1 32 d
: 29 4 32  your
: 33 5 32  life
- 37
: 38 3 28 like
: 41 1 28  a
: 42 3 28  sleep
: 45 2 28 ing
: 48 5 28  swan
- 52
: 63 3 35 Your
: 66 4 35  time
: 70 3 35  has
: 73 4 35  come
- 76
: 92 2 35 To
: 94 3 30  go
: 97 2 30  deep
: 99 4 30 er
- 102
: 127 8 30 Girl
- 134
: 151 3 30 Your
: 154 4 32  final
: 158 8 32  journey
- 165
: 168 2 32 has
: 170 3 32  just
: 173 7 32  begun
- 182
: 190 5 28 But
: 195 5 28  destiny
- 199
: 216 5 28 chose
: 221 2 28  the
: 223 3 28  reap
: 226 5 35 er
- 230
: 249 5 35 No
: 254 8 35  fear
- 261
: 275 4 35 Desti
: 279 7 35 nation
: 286 5 30  Dark
: 291 5 30 ness
- 295
: 314 3 30 No
: 317 10 30  fear
- 326
: 338 5 28 Desti
: 343 5 35 nation
: 350 4 32  Dark
: 354 6 35 ness
- 359
: 378 2 35 No
: 380 5 32  fear
- 384
: 416 8 32 Girl
- 423
: 438 3 32 Rain
: 441 4 32  falls
: 445 3 32  down
- 447
: 450 3 32 from
: 453 1 32  the
: 454 5 32  northern
: 459 10 39  skies
- 468
: 475 4 39 Like
: 479 4 39  poison
: 483 2 39 ed
: 485 7 39  knives
- 491
: 503 4 39 with
: 507 3 39  no
: 510 6 39  mercy
- 515
: 541 8 40 Girl
- 548
: 566 2 40 Close
: 568 3 40  your
: 571 5 40  eyes
- 575
: 578 2 40 for
: 580 2 40  the
: 582 3 40  one
: 585 3 40  last
: 588 12 35  time
- 599
: 605 2 35 sleep
: 607 2 35 less
: 609 10 35  nights
- 618
: 625 3 35 From
: 628 4 35  here
: 632 2 35  to
: 634 9 35  eternity
- 642
: 661 3 37 No
: 664 7 37  fear
- 670
: 688 4 37 Desti
: 692 4 37 nation
: 696 5 37  Dark
: 701 6 37 ness
- 706
: 726 3 37 No
: 729 7 37  fear
- 735
: 751 4 39 Desti
: 755 4 39 nation
: 759 5 39  Dark
: 764 7 39 ness
- 770
: 792 6 39 No
: 798 11 39  fear
- 808
: 1062 11 39 Ooh
: 1073 5 39  No
: 1078 11 32  fear
- 1088
: 1099 4 32 Desti
: 1103 5 32 nation
: 1108 4 32  Dark
: 1112 7 32 ness
- 1118
: 1136 3 32 No
: 1139 11 32  fear
- 1149
: 1162 4 32 Desti
: 1166 4 32 nation
: 1170 5 32  Dark
: 1175 7 32 ness
- 1177
: 1188 11 32 Ooh
: 1199 4 32  No
: 1203 8 32  fear
- 1210
: 1224 5 32 Desti
: 1229 5 32 nation
: 1234 5 32  Dark
: 1239 7 32 ness
- 1245
: 1264 4 32 No
: 1268 7 32  fear
- 1274
: 1288 4 32 Desti
: 1292 5 32 nation
: 1297 6 39  Dark
: 1303 12 39 ness
- 1315
: 1327 3 39 No
: 1330 6 39  fear
E