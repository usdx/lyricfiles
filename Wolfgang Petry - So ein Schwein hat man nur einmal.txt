#TITLE:So ein Schwein hat man nur einmal
#ARTIST:Wolfgang Petry
#MP3:Wolfgang Petry - So ein Schwein hat man nur einmal.mp3
#VIDEO:Wolfgang Petry - So ein Schwein hat man nur einmal.mp4
#COVER:Wolfgang Petry - So ein Schwein hat man nur einmal [CO].jpg
#BPM:132
#GAP:13920
#ENCODING:UTF8
#LANGUAGE:German
#GENRE:Schlager
#YEAR:1998
: 0 2 -9 Ich 
: 2 2 -9 kann 
: 4 2 -9 seit 
* 6 4 0 Ta
* 10 4 0 gen 
: 14 3 0 nicht 
: 18 1 0 mehr 
: 20 4 0 rau
: 24 2 -2 chen
- 28
: 36 2 -4 fühl 
: 38 3 -2 mich 
* 42 3 -2 wie'n 
: 46 3 -2 al
: 50 2 -4 ter 
: 52 3 -7 Mann
- 57
: 68 2 -4 Ich 
: 70 4 0 lie
: 74 2 0 ge 
* 78 3 0 hier 
: 82 3 0 und 
: 86 2 0 kann 
: 88 3 -2 nicht 
: 92 2 -2 mehr
- 96
: 101 1 1 und 
: 102 3 1 du 
: 106 3 0 bist 
* 110 3 -2 schuld 
: 114 2 -4 dar
: 116 2 -2 an
- 120
: 132 2 -4 Bis 
: 134 4 1 hier 
: 138 3 1 her 
* 142 3 3 und 
: 146 2 5 nicht 
: 148 3 5 wei
: 152 2 3 ter
- 156
: 158 2 0 Jetzt 
: 162 2 -2 ist 
: 166 3 -4 Schluss 
: 170 2 -4 ich 
* 174 4 8 brau
* 178 2 7 che 
: 180 2 5 dich
- 184
: 196 2 -4 Ich 
: 198 3 1 glaub 
: 202 3 1 viel
: 206 3 3 leicht 
: 210 1 5 an 
* 212 4 5 Wun
* 216 2 3 der
- 220
: 222 2 0 doch 
* 226 3 -2 be
* 230 1 -4 scheu
* 232 3 -4 ert 
: 238 2 0 bin 
: 240 3 -4 ich 
: 244 3 3 nicht
- 249
: 254 4 -4 So 
: 258 4 -2 ein 
* 262 3 0 Schwein 
: 266 3 0 hat 
: 270 4 0 man 
: 274 2 1 nur 
: 276 4 0 ein
: 280 6 -4 mal
- 286
: 286 4 0 Wa
: 290 3 3 rum 
* 294 3 8 lässt 
: 298 3 7 du 
: 302 3 7 mich 
: 306 2 5 im 
* 308 3 3 Stich
- 313
: 318 3 3 Wir 
: 322 3 0 sind 
: 326 3 -4 bei
: 330 3 -4 de 
: 334 2 -5 kei
: 338 2 -4 ne 
* 340 4 0 En
* 344 2 -4 gel
- 348
: 350 2 -4 doch 
* 354 2 -4 ver
* 358 4 5 ges
* 362 3 3 sen 
: 366 3 1 kann 
: 370 2 0 ich 
: 372 3 -2 nicht
- 377
: 382 3 -4 So 
: 386 3 -2 ein 
* 390 3 0 Schwein 
: 394 2 0 hat 
: 398 2 0 man 
: 402 2 1 nur 
: 404 4 0 ein
: 408 4 -4 mal
- 413
: 414 3 0 Geht 
: 418 3 3 mir 
* 422 3 8 voll 
: 426 3 7 am 
: 430 3 7 Arsch 
: 434 2 8 vor
: 436 3 5 bei
- 441
: 446 3 -4 Mei
: 450 3 -4 ne 
* 454 3 1 Ner
* 458 3 1 ven 
: 462 3 -4 sind 
: 466 2 -2 am 
: 468 4 0 En
: 472 3 -4 de
- 476
: 478 3 -4 Mensch 
: 482 3 5 das 
* 486 3 5 pack 
: 490 2 3 ich 
: 494 4 3 nicht 
: 498 2 5 al
: 500 3 3 lein
- 505
: 564 2 -9 Du 
: 566 4 0 glaubst 
: 570 4 0 nicht 
: 574 3 0 an 
: 578 1 0 die 
* 580 4 0 Lie
* 584 2 -2 be
- 588
: 596 2 -4 Da 
: 598 3 -2 fehlt 
: 602 3 -2 dir 
* 606 3 -2 echt 
: 610 2 -4 der 
: 612 3 -7 Draht
- 617
: 628 2 -4 Wa
: 630 3 0 rum 
: 634 3 0 hast 
: 638 2 0 du 
: 642 3 0 mich 
* 646 3 0 klar 
: 650 2 -2 ge
: 652 2 -2 macht
- 656
: 660 2 -4 jetzt 
* 662 3 1 hab'n 
: 666 2 0 wir 
: 670 3 -2 den 
: 674 2 -4 Sa
: 676 2 -2 lat
- 680
: 692 2 -4 Bis 
: 694 4 1 hier 
: 698 2 1 her 
* 702 3 3 und 
: 706 2 5 nicht 
: 708 3 5 wei
: 712 3 3 ter
- 716
: 718 3 0 Jetzt 
: 722 3 -2 ist 
: 726 3 -4 Schluss 
: 730 2 -4 ich 
* 734 4 8 brau
* 738 2 7 che 
: 740 3 5 dich
- 745
: 756 2 -4 Ich 
: 758 3 1 glaub 
: 762 3 1 viel
: 766 3 3 leicht 
: 770 1 5 an 
* 772 4 5 Wun
* 776 2 3 der
- 780
: 782 3 0 doch 
* 786 3 -2 be
* 790 1 -4 scheu
* 792 5 -4 ert 
: 798 2 0 bin 
: 800 4 -4 ich 
: 804 2 3 nicht
- 808
: 814 4 -4 So 
: 818 4 -2 ein 
* 822 3 0 Schwein 
: 826 3 0 hat 
: 830 4 0 man 
: 834 2 1 nur 
: 836 4 0 ein
: 840 6 -4 mal
- 846
: 846 4 0 Wa
: 850 3 3 rum 
* 854 3 8 lässt 
: 858 3 7 du 
: 862 3 7 mich 
: 866 2 5 im 
: 868 3 3 Stich
- 873
: 878 3 3 Wir 
: 882 3 0 sind 
: 886 3 -4 bei
: 890 3 -4 de 
: 894 3 -5 kei
: 898 2 -4 ne 
* 900 4 0 En
* 904 4 -4 gel
- 909
: 910 2 -4 doch 
* 914 2 -4 ver
* 918 4 5 ges
* 922 3 3 sen 
: 926 3 1 kann 
: 930 1 0 ich 
: 932 3 -2 nicht
- 937
: 942 3 -4 So 
: 946 3 -2 ein 
* 950 3 0 Schwein 
: 954 2 0 hat 
: 958 3 0 man 
: 962 2 1 nur 
: 964 4 0 ein
: 968 4 -4 mal
- 973
: 974 3 0 Geht 
: 978 3 3 mir 
* 982 3 8 voll 
: 986 2 7 am 
: 990 3 7 Arsch 
: 994 2 8 vor
: 996 3 5 bei
- 1001
: 1006 3 -4 Mei
: 1010 3 -4 ne 
* 1014 3 1 Ner
* 1018 3 1 ven 
: 1022 3 -4 sind 
: 1026 2 -2 am 
: 1028 3 0 En
: 1032 4 -4 de
- 1037
: 1038 3 -4 Mensch 
: 1042 3 5 das 
* 1046 3 5 pack 
: 1050 3 3 ich 
: 1054 3 3 nicht 
: 1058 2 5 al
: 1060 3 3 lein
- 1065
: 1198 3 -4 So 
: 1202 3 -2 ein 
* 1206 3 0 Schwein 
: 1210 3 0 hat 
: 1214 4 0 man 
: 1218 2 1 nur 
: 1220 4 0 ein
: 1224 6 -4 mal
- 1230
: 1230 4 0 Wa
: 1234 3 3 rum 
* 1238 3 8 lässt 
: 1242 3 7 du 
: 1246 3 7 mich 
: 1250 2 5 im 
* 1252 3 3 Stich
- 1257
: 1262 3 3 Wir 
: 1266 3 0 sind 
: 1270 3 -4 bei
: 1274 3 -4 de 
: 1278 3 -5 kei
: 1282 2 -4 ne 
* 1284 4 0 En
* 1288 2 -4 gel
- 1292
: 1294 3 -4 doch 
* 1298 3 -4 ver
* 1302 4 5 ges
* 1306 3 3 sen 
: 1310 4 1 kann 
: 1314 2 0 ich 
: 1316 3 -2 nicht
- 1321
: 1326 4 -4 So 
: 1330 3 -2 ein 
* 1334 3 0 Schwein 
: 1338 3 0 hat 
: 1342 3 0 man 
: 1346 2 1 nur 
: 1348 4 0 ein
: 1352 4 -4 mal
- 1357
: 1358 3 0 Geht 
: 1362 3 3 mir 
* 1366 3 8 voll 
: 1370 3 7 am 
: 1374 3 7 Arsch 
: 1378 2 8 vor
: 1380 3 5 bei
- 1385
: 1390 3 -4 Mei
: 1394 3 -4 ne 
* 1398 3 1 Ner
* 1402 3 1 ven 
: 1406 3 -4 sind 
: 1410 2 -2 am 
: 1412 3 0 En
: 1416 3 -4 de
- 1420
: 1422 3 -4 Mensch 
: 1426 3 5 das 
* 1430 3 5 pack 
: 1434 3 3 ich 
: 1438 3 3 nicht 
: 1442 2 5 al
: 1444 4 3 lein
- 1450
: 1454 3 -4 So 
: 1458 3 -2 ein 
* 1462 3 0 Schwein 
: 1466 3 0 hat 
: 1470 4 0 man 
: 1474 2 1 nur 
: 1476 4 0 ein
: 1480 6 -4 mal
- 1486
: 1486 4 0 Wa
: 1490 3 3 rum 
* 1494 3 8 lässt 
: 1498 3 7 du 
: 1502 3 7 mich 
: 1506 2 5 im 
* 1508 3 3 Stich
- 1513
: 1518 3 3 Wir 
: 1522 3 0 sind 
: 1526 3 -4 bei
: 1530 3 -4 de 
: 1534 3 -5 kei
: 1538 2 -4 ne 
* 1540 4 0 En
* 1544 4 -4 gel
- 1549
: 1550 3 -4 doch 
* 1554 3 -4 ver
* 1558 4 5 ges
* 1562 3 3 sen 
: 1566 3 1 kann 
: 1570 2 0 ich 
: 1572 3 -2 nicht
- 1577
: 1582 3 -4 So 
: 1586 3 -2 ein 
* 1590 3 0 Schwein 
: 1594 3 0 hat 
: 1598 2 0 man 
: 1602 2 1 nur 
: 1604 4 0 ein
: 1608 4 -4 mal
- 1613
: 1614 3 0 Geht 
: 1618 3 3 mir 
* 1622 3 8 voll 
: 1626 3 7 am 
: 1630 3 7 Arsch 
: 1634 2 8 vor
: 1636 3 5 bei
- 1641
: 1646 3 -4 Mei
: 1650 3 -4 ne 
* 1654 3 1 Ner
* 1658 3 1 ven 
: 1662 3 -4 sind 
: 1666 2 -2 am 
: 1668 3 0 En
: 1672 3 -4 de
- 1676
: 1678 3 -4 Mensch 
: 1682 3 5 das 
* 1686 3 5 pack 
: 1690 3 3 ich 
: 1694 3 3 nicht 
: 1698 2 5 al
: 1700 2 3 lein. 
E