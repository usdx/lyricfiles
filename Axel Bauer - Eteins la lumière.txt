#TITLE:Eteins la lumičre
#ARTIST:Axel Bauer
#MP3:Axel Bauer - Eteins la lumière.mp3
#VIDEO:Axel Bauer - Eteins la lumière.avi
#COVER:Axel Bauer - Eteins la lumière.jpg
#BPM:291.89
#GAP:20206
#ENCODING:UTF8
#LANGUAGE:French
#GENRE:Rock
#YEAR:1992
#CREATOR:mustangfred and thoun
: 0 1 16 A
: 4 3 21  for
: 11 2 21 ce
: 15 5 23  de
: 24 3 23  se
: 31 8 24  voir,
: 41 2 24  on
: 44 1 24  ne
: 48 2 23  se
: 52 3 21  vo
: 56 4 23 yait
: 63 12 24  plus
- 94
: 128 1 16 A
: 132 6 21  tant
: 140 1 21  vou
: 143 7 23 loir
: 152 3 23  y
: 160 11 24  croire,
: 173 2 24  on
: 176 1 23  n'y
: 180 3 21  cro
* 184 4 23 yait
: 192 11 21  plus
- 222
: 249 10 9 Et
: 261 2 9  ne
: 265 4 9  me
: 271 5 11  lais
: 281 3 11 se
: 288 12 12  pas
- 310
: 325 1 12 Si
: 329 5 9  tu
: 336 4 11  n'es
: 344 4 11  pas
: 353 3 12  sűr
: 358 14 11 ~
- 384
: 388 9 9 Loin
: 400 7 11  loin
: 409 3 11  de
: 417 9 12  toi
: 430 1 12  tu
: 433 2 11  me
: 438 1 9  dis
: 441 4 11  tout
* 449 2 9  bas
* 452 1 12 ~
* 454 5 9 ~
- 478
: 510 1 9 Cette
: 518 1 9  pe
: 522 9 9 tite
: 533 3 11  a
: 538 4 11 ven
: 546 3 12 ture
* 550 1 14 ~
: 553 10 12 ~
- 573
: 589 3 9 Va
: 597 3 9  tour
: 602 2 11 ner
: 605 4 11  en
: 613 1 12  dé
: 617 4 11 con
: 625 5 11 fi
: 634 11 12 ture
- 655
: 668 4 12 E
: 677 11 11 claire
: 690 10 12  moi
- 706
: 709 45 12 Eh,
* 756 2 12  é
* 761 3 12 teins
: 766 4 12  la
: 772 11 11  lu
: 785 13 12 mičre
- 809
: 813 1 12 Mon
: 817 4 12 tre
: 824 3 12  moi
: 830 3 12  ton
: 837 1 11  co
: 842 3 11 té
: 849 4 11  som
: 854 2 9 ~
: 857 12 9 bre
- 875
: 878 1 11 Re
: 881 7 12 gar
: 890 2 12 de
: 894 4 12  les
: 902 10 11  om
: 914 4 12 bres
: 920 3 9 ~
: 928 4 9  qui
: 934 3 9  errent
: 938 11 11 ~
- 959
: 966 2 12 Cherche
: 971 3 12  un
: 978 5 11  peu
: 985 2 9 ~
: 989 2 9  de
: 993 3 11  lu
: 998 2 12 ~
: 1002 31 12 mičre
- 1043
: 1051 1 16 Tout
: 1054 3 16  s'é
* 1061 40 16 claire
- 1139
: 1318 1 9 Main
: 1322 2 9 te
: 1326 4 9 nant
: 1334 3 11  que
: 1338 3 11  le
: 1345 11 12  ciel
: 1358 4 11  n'a
: 1366 3 9  plus
: 1371 3 11  de
* 1377 3 12  mur
* 1381 2 14 ~
: 1384 4 12 ~
- 1407
: 1445 1 9 Lais
: 1451 2 9 sons
: 1457 4 11  nous
: 1464 5 11  glis
: 1474 5 12 ser
: 1481 3 11 ~
: 1486 4 11  dans
: 1492 2 9  l'ou
: 1497 4 11 ver
: 1505 2 9 ture
: 1508 1 11 ~
: 1510 4 9 ~
- 1533
: 1568 1 9 Le
: 1573 4 9  coeur
: 1579 1 9  est
: 1585 5 11  si
: 1592 4 11  lé
: 1600 15 12 ger
: 1617 4 11 ~
- 1631
: 1635 5 9 Lŕ
: 1641 3 9  oů
: 1648 3 12  je
: 1656 4 11  t'em
: 1665 4 12 mčne
* 1671 11 11 ~
- 1692
: 1700 3 9 D'au
: 1707 1 9 tres
: 1712 3 11  sont
: 1719 6 11  al
: 1727 12 12 lés
: 1741 2 11  dans
: 1748 1 9  ces
: 1752 4 11  do
* 1760 3 9 mai
* 1764 1 12 ~
: 1766 6 9 nes
- 1791
: 1836 1 12 E
: 1840 6 12 teins
: 1848 2 12  la
: 1852 5 11  lu
: 1859 17 12 mičre
- 1886
: 1899 1 12 Net
: 1903 5 12 toie
: 1911 1 12  ce
: 1916 4 12  qui
: 1923 1 14  n'est
: 1928 3 11  pas
: 1934 4 12  toi
: 1939 1 14 ~
: 1941 6 12 ~
- 1959
: 1963 8 12 Souf
: 1975 2 12 fle
: 1979 4 12  la
: 1987 9 11  pous
: 2001 7 12 sičre
: 2014 2 9  sur
: 2019 2 9  toi
: 2023 19 11 ~
: 2044 14 12 ~
- 2061
: 2063 3 12 E
: 2071 3 12 teins
: 2076 4 12  la
: 2082 11 11  lu
: 2095 15 12 mičre
- 2119
: 2123 1 12 Mon
: 2127 5 12 tre
: 2135 1 12  moi
: 2139 3 12  ton
: 2147 1 10  co
: 2151 3 10 té
: 2159 3 10  som
: 2164 2 9 ~
: 2168 10 9 bre
- 2184
: 2187 1 12 Re
: 2191 7 12 gar
: 2200 2 12 de
: 2204 5 12  les
: 2212 10 11  om
: 2224 6 12 bres
: 2232 2 9 ~
: 2237 4 9  qui
: 2243 3 9  errent
: 2248 15 11 ~
- 2272
: 2276 2 11 Cherche
: 2281 3 12  un
: 2288 4 11  peu
: 2294 4 9 ~
: 2300 2 9  de
* 2304 2 11  lu
* 2308 2 12 ~
* 2312 24 12 mičre
- 2346
: 2360 1 16 Tout
: 2364 4 16  s'é
: 2372 64 16 claire
: 2438 7 14 ~
- 2483
: 3141 1 16 A
: 3145 4 21  for
: 3153 1 21 ce
: 3157 2 23  de
: 3165 4 23  se
: 3173 8 24  voir,
: 3183 1 24  on
: 3185 1 24  ne
: 3189 1 23  se
: 3193 3 21  vo
: 3197 4 23 yait
: 3205 3 24  plus
: 3210 5 24 ~
- 3234
: 3269 1 4 A
: 3273 4 9  tant
: 3280 2 9  vou
: 3284 7 11 loir
: 3293 3 11  y
: 3302 10 12  croire,
: 3314 1 12  on
: 3316 1 11  n'y
: 3321 3 9  cro
: 3326 3 11 yait
: 3333 2 9  plus
: 3336 2 12 ~
: 3340 4 9 ~
- 3363
: 3392 2 9 Cette
: 3399 2 9  pe
: 3405 8 9 tite
: 3416 2 11  a
: 3420 4 11 ven
* 3428 1 12 ture
* 3430 1 14 ~
: 3432 14 12 ~
- 3456
: 3472 3 9 Va
: 3480 3 9  tour
: 3485 2 11 ner
: 3488 4 11  en
: 3496 1 12  dé
: 3501 2 11 con
: 3508 3 11 fi
: 3515 11 12 ture
- 3536
: 3560 27 12 Eh
: 3589 33 9 ~
: 3623 5 11 ~
- 3633
: 3635 3 12 E
: 3642 4 12 teins
: 3648 3 12  la
: 3653 11 11  lu
: 3666 12 12 mičre
- 3690
: 3694 2 12 Mon
: 3699 4 12 tre
: 3706 2 12  moi
: 3712 2 12  ton
: 3719 1 11  co
: 3723 3 11 té
: 3731 2 11  som
: 3735 2 9 ~
: 3739 7 9 bre
- 3755
: 3759 1 12 Re
: 3762 6 12 gar
: 3771 2 12 de
: 3775 3 12  les
: 3783 10 11  om
* 3795 4 12 bres
* 3801 3 9 ~
: 3808 5 9  qui
: 3816 1 9  errent
: 3819 18 11 ~
- 3844
: 3847 2 12 Cherche
: 3852 4 12  un
: 3859 4 11  peu
: 3865 3 9 ~
: 3870 3 9  de
: 3875 5 12  lu
: 3883 37 12 mičre
- 3929
: 3932 1 16 Tout
: 3936 3 16  s'é
: 3943 2 14 claire
: 3947 43 16 ~
: 3992 20 16 ~
- 4031
: 4064 2 16 S'é
: 4071 2 15 claire
: 4075 52 16 ~
: 4129 3 15 ~
: 4134 8 14 ~
E