#TITLE:Please Don't Leave Me
#ARTIST:Pink
#MP3:Pink - Please Don't Leave Me.mp3
#VIDEO:Pink - Please Don't Leave Me [VD#0].avi
#COVER:Pink - Please Don't Leave Me [CO].jpg
#BPM:276
#GAP:490
#ENCODING:UTF8
#LANGUAGE:English
#YEAR:2009
: 0 2 13 Da
: 4 2 15  da
: 8 2 17  da
: 12 6 15  da
- 28
: 32 2 12 Da
: 36 2 13  da
: 40 2 15  da
: 44 6 13  da
- 60
: 80 5 18 Da
: 88 2 17  da
: 92 4 13  da
: 100 4 10  da
: 108 7 8  da
- 151
: 208 5 18 Da
: 216 2 17  da
: 220 4 13  da
: 228 4 10  da
: 236 5 8  da
- 246
: 248 2 13 I
: 252 2 13  don't
: 256 2 17  know
: 260 2 20  if
: 264 2 20  I
: 268 2 20  can
: 272 5 20  yell
: 280 2 20  an
: 284 2 20 y
: 288 3 20  lou
: 292 2 17 ~
: 296 6 17 der
- 312
: 316 2 13 How
: 320 3 17  man
: 324 2 20 y
: 328 2 20  times
: 332 2 17  have
: 336 4 20  I
- 342
: 344 2 20 kicked
: 348 2 20  you
: 352 3 22  out
: 356 2 17 ta
* 360 6 17  here?
- 376
: 396 2 17 Or
* 400 4 18  said
: 407 3 17  some
: 412 5 15 thing
: 420 4 13  in
: 428 4 15 sult
: 436 3 15 ing
: 440 2 17 ~?
- 460
: 504 2 13 I
: 508 2 13  can
: 512 2 17  be
: 517 4 20  so
: 524 4 20  mean
: 529 3 17 ~,
- 534
: 536 2 20 when
: 540 2 20  I
: 544 3 20  wan
: 548 2 17 na
: 552 4 17  be.
- 565
: 568 2 13 I
: 572 2 13  am
: 576 2 17  ca
: 580 6 20 pa
: 588 5 20 ble
: 596 2 17  of
: 600 3 20  real
: 604 2 20 ly
: 608 2 20  an
: 612 2 17 y
: 616 4 17 thing.
- 630
: 648 2 13 I
: 652 2 13  can
: 657 4 18  cut
: 664 3 17  you
: 669 4 15  in
: 676 5 13 to
: 684 5 15  pie
: 692 3 15 ces
: 696 7 17 ~,
- 721
: 740 2 13 but
: 744 2 13  my
: 748 3 13  heart
: 752 4 15 ~
: 758 7 17  is
- 783
: 813 5 15 bro
: 821 7 17 ken.
- 846
* 880 32 13 Please
: 914 10 15 ~
: 932 4 6  don't
: 940 4 6  leave
: 948 11 5  me
- 977
: 1008 32 13 Please
: 1042 8 15 ~
: 1060 5 6  don't
: 1068 5 6  leave
: 1076 12 5  me
- 1106
: 1128 5 13 I
: 1136 3 18  al
: 1141 4 17 ways
: 1148 5 15  say
: 1156 5 13  how
: 1164 5 18  I
: 1172 5 17  don't
: 1180 5 15  need
: 1187 3 13  you
- 1191
: 1193 2 13 But
: 1196 2 13  it's
* 1200 4 18  al
: 1206 4 17 ways
: 1212 5 15  gon
: 1220 2 13 na
: 1223 2 12 ~
- 1226
: 1228 5 12 come
: 1236 5 13  right
: 1244 5 15  back
: 1252 3 13  to
: 1256 2 8 ~
: 1260 7 8  this
- 1277
: 1288 15 15 Please,
: 1316 4 6  don't
: 1324 5 6  leave
: 1332 12 5  me
- 1362
: 1408 2 17 How
: 1412 2 20  did
: 1416 2 20  I
: 1420 2 20  be
: 1424 5 20 come
: 1432 2 20  so
: 1436 2 20  ob
: 1440 4 20 nox
: 1448 6 17 ious?
- 1464
: 1472 2 17 What
: 1476 2 20  is
: 1480 2 20  it
: 1484 2 17  with
: 1487 2 20  you,
- 1490
: 1492 2 20 that
: 1496 2 20  makes
: 1500 2 20  me
: 1504 2 20  act
: 1508 2 17  like
* 1512 7 17  this?
- 1529
: 1548 2 17 I've
: 1552 3 18  ne
: 1557 4 17 ver
: 1564 4 15  been
: 1572 4 13  this
: 1580 5 15  nas
: 1588 9 17 ty.
- 1615
: 1656 2 13 Can't
: 1660 2 13  you
: 1664 3 17  tell
: 1668 2 20  that
: 1672 2 20  this
: 1676 2 17  is
: 1680 5 20  all
- 1686
: 1688 2 20 just
: 1692 2 20  a
: 1696 3 20  con
: 1700 2 17 ~
: 1704 5 17 test?
- 1714
: 1716 2 13 The
: 1720 2 13  one
: 1724 2 13  that
: 1728 2 17  wins
: 1732 2 20  will
: 1736 3 20  be
: 1740 2 17  the
: 1744 1 20  one,
- 1746
: 1748 2 17 that
: 1752 2 20  hits
: 1756 2 17  the
: 1760 3 22  hard
* 1765 9 22 est
: 1776 4 20 ~
- 1790
: 1804 2 17 But
: 1808 4 18  ba
: 1814 4 17 by,
: 1820 5 15  I
: 1828 5 13  don't
: 1836 5 15  mean
: 1844 3 15  it
: 1848 6 17 ~
- 1872
: 1896 2 13 I
: 1900 4 15  mean
* 1908 10 17  it,
- 1936
: 1960 2 13 I
: 1964 5 15  pro
: 1972 3 15 mise
: 1976 3 17 ~
- 1997
: 2032 31 13 Please
* 2065 11 15 ~
: 2084 5 6  don't
: 2092 5 6  leave
: 2100 7 5  me
- 2117
* 2140 12 13 Oh,
: 2160 31 13  please
: 2193 10 15 ~
: 2212 5 6  don't
: 2220 5 6  leave
: 2228 10 5  me
- 2256
: 2284 2 13 I
: 2288 3 18  al
: 2293 3 17 ways
: 2300 6 15  say
: 2308 4 13  how
: 2316 5 18  I
: 2324 5 17  don't
: 2332 5 15  need
: 2339 2 13  you
- 2343
: 2345 2 13 But
: 2348 2 13  it's
: 2352 4 18  al
: 2358 4 17 ways
: 2364 5 15  gon
: 2371 3 13 na
: 2375 2 12 ~
- 2378
: 2380 5 12 come
: 2388 5 13  right
: 2396 5 15  back
: 2404 3 13  to
: 2408 2 8 ~
: 2412 5 8  this
- 2427
: 2440 3 13 Please
: 2444 8 15 ~,
- 2464
: 2468 3 18 don't
: 2472 2 17 ~
: 2476 3 18  leave
: 2480 2 17 ~
* 2484 10 17  me
: 2496 3 13 ~
- 2517
: 2536 2 13 I
: 2540 2 13  for
* 2544 6 22 got
: 2552 2 22  to
: 2556 5 20  say
: 2564 2 20  out
: 2568 10 15  loud,
- 2588
: 2600 5 17 how
: 2608 3 18  beau
: 2612 5 17 ti
: 2620 5 17 ful
: 2628 2 13  you
: 2632 3 18  real
: 2636 6 17 ly
: 2644 7 17  are
: 2656 2 15  to
: 2660 3 13  me.
- 2666
: 2668 2 13 I
* 2672 5 22  can
: 2680 2 20 not
: 2684 5 20  be
: 2692 2 20  with
: 2696 10 15 out.
- 2716
: 2728 2 15 You're
: 2732 2 17  my
: 2736 3 18  per
: 2741 4 17 fect
: 2748 5 17  lit
: 2755 2 13 tle
: 2760 3 18  punch
: 2766 4 17 ing
: 2772 6 17  bag.
- 2785
: 2788 2 13 And
: 2792 2 13  I
: 2796 5 15  need
* 2804 9 17  you,
- 2831
: 2856 2 13 I'm
: 2862 5 15  sor
: 2871 6 17 ry.
- 2887
: 2896 4 18 Da
: 2904 2 17  da
: 2908 5 13  da
: 2916 4 10  da
: 2924 5 8  da
- 2940
: 2944 2 13 Da
: 2948 2 15  da
: 2952 2 17  da
: 2956 6 15  da
- 2972
: 2976 2 12 Da
: 2980 2 13  da
* 2984 2 15  da
: 2988 5 13  da
- 3003
: 3024 5 18 Da
: 3032 2 17  da
: 3036 5 13  da
: 3044 4 10  da
: 3052 2 8  da
- 3055
: 3057 8 13 Please,
: 3089 7 15  please
: 3108 5 6  don't
: 3116 5 6  leave
: 3124 7 5  me
- 3141
: 3164 5 13 Ba
: 3172 6 15 by,
- 3182
: 3184 29 13 please
: 3216 11 15 ~
: 3236 5 6  don't
: 3244 5 6  leave
: 3252 7 5  me
- 3277
: 3312 17 20 Please
: 3332 3 25 ~
: 3336 3 24 ~
: 3340 15 20 ~
: 3364 5 20  don't
: 3372 5 22  leave
* 3380 13 22  me
- 3411
: 3432 5 13 I
: 3440 3 18  al
: 3445 4 17 ways
: 3452 5 15  say
: 3460 4 13  how
: 3468 5 18  I
: 3476 5 17  don't
: 3484 5 15  need
: 3490 3 13  you
- 3494
: 3496 2 13 But
: 3500 2 13  it's
: 3504 4 18  al
: 3510 3 17 ways
: 3516 5 15  gon
: 3523 3 13 na
: 3527 2 12 ~
- 3530
: 3532 5 12 come
: 3540 5 13  right
: 3548 5 15  back
: 3556 3 13  to
: 3560 2 8 ~
: 3563 3 8  this
- 3567
: 3569 29 13 Please
* 3601 10 15 ~
: 3620 3 15  don't
: 3624 2 13 ~
: 3628 3 15  leave
: 3632 2 13 ~
: 3636 10 13  me
- 3664
: 3696 30 13 Please
: 3729 10 15 ~
: 3748 3 15  don't
: 3752 2 13 ~
: 3756 3 15  leave
: 3760 2 13 ~
: 3764 14 13  me
- 3796
: 3816 5 13 I
: 3824 3 18  al
: 3829 3 17 ways
: 3836 6 15  say
: 3844 4 13  how
: 3852 5 18  I
: 3860 5 17  don't
: 3868 5 15  need
: 3875 2 13  you
- 3878
: 3880 2 13 But
: 3884 2 13  it's
* 3888 4 18  al
: 3894 4 17 ways
: 3900 5 15  gon
: 3907 3 13 na
: 3911 2 12 ~
- 3914
: 3916 5 12 come
: 3924 5 13  right
: 3932 5 15  back
: 3940 3 13  to
: 3944 2 8 ~
: 3948 4 8  this
- 3961
: 3964 8 15 Please,
: 3979 11 15  please
: 4004 5 6  don't
: 4012 5 6  leave
: 4020 8 5  me
- 4038
: 4060 3 1 Ba
: 4064 2 3 ~
: 4068 6 5 by,
- 4084
: 4088 13 1 please,
: 4114 9 3  please
: 4139 5 -6  don't
: 4147 2 -6  leave
: 4150 3 -7 ~
: 4155 9 -7  me
E