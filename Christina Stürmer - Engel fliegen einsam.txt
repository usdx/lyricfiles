#TITLE:Engel fliegen einsam
#ARTIST:Christina Stürmer
#MP3:Christina Stürmer - Engel fliegen einsam.mp3
#VIDEO:Christina Stürmer - Engel fliegen einsam [VD#0].mp4
#COVER:Christina Stürmer - Engel fliegen einsam [CO].jpg
#BPM:97,92
#GAP:6430
#ENCODING:UTF8
#LANGUAGE:Deutsch
: 0 2 11 Weißt 
: 2 1 11 du 
: 4 2 13 wie 
: 6 2 13 die 
: 8 2 13 Dich
: 10 1 11 ter 
: 12 3 11 schrei
: 15 3 9 ben
- 20
: 20 1 11 Hast 
: 21 1 11 du 
: 23 1 13 je 
: 24 2 13 ei
: 26 3 13 nen 
: 29 1 11 ge
: 30 4 13 sehn
- 36
: 40 1 13 Dich
: 41 1 13 ter 
: 43 3 13 schrei
: 47 1 11 ben 
: 49 2 11 ein
* 51 4 13 sam
- 58
: 70 1 4 Und 
: 71 2 13 weißt 
: 74 1 13 du 
: 75 2 13 wie 
: 77 2 13 die 
: 79 3 13 Ma
: 82 1 11 ler 
: 84 2 11 ma
: 87 3 9 len
- 91
: 91 1 9 Hast 
: 92 1 9 du 
: 94 1 13 je 
* 95 2 16 ei
* 97 2 16 nen 
: 99 2 13 ge
: 101 4 9 sehn
- 108
: 111 1 13 Ma
: 112 1 13 ler 
: 114 3 13 ma
: 117 1 11 len 
: 118 2 11 ein
: 122 4 13 sam
- 129
: 141 1 13 Und 
: 142 2 21 weißt 
: 145 1 20 du 
: 146 2 20 wie 
: 148 2 18 die 
: 150 2 18 En
: 152 2 16 gel 
: 155 3 13 flie
: 158 3 9 gen
- 162
: 163 1 9 Hast 
: 164 1 9 du 
: 165 1 13 je 
: 166 1 16 ei
: 169 2 16 nen 
: 171 2 13 ge
: 173 4 9 sehen
- 179
: 183 1 18 En
: 184 1 16 gel 
: 186 3 16 flie
: 189 1 14 gen 
: 191 2 14 ein
* 194 4 13 sam
- 202
: 213 1 9 Und 
: 214 2 16 weißt 
: 216 1 16 du 
: 217 3 16 wie 
: 220 1 14 ich 
: 222 1 13 mich 
: 224 2 13 jetzt 
: 226 3 11 füh
: 229 2 9 le
- 232
: 234 1 9 Hast 
: 235 1 9 du 
: 237 1 13 je 
: 238 1 16 dar
: 240 2 16 an 
: 242 1 13 ge
: 244 4 9 dacht
- 250
: 254 1 14 Du 
: 255 2 13 und 
: 257 3 13 ich 
: 261 1 11 ge
: 262 3 11 mein
: 265 6 11 sam
- 276
: 290 1 13 En
: 291 2 13 gel 
* 293 3 13 flie
* 297 1 11 gen 
: 298 3 11 ein
: 301 4 13 sam
- 307
: 325 1 13 Du 
: 326 1 13 und 
: 328 3 13 ich 
: 332 1 11 ge
: 333 3 11 mein
: 336 5 9 sam
- 343
: 361 1 13 En
: 362 2 13 gel 
: 364 3 13 flie
: 368 1 11 gen 
: 369 3 11 ein
: 372 4 13 sam
- 378
: 397 1 13 Nie
: 399 2 13 mals 
: 401 2 13 mehr 
: 403 1 11 al
: 404 3 11 lein 
: 407 5 9 sein 
- 414
: 428 1 11 weißt 
: 429 1 11 du 
: 431 2 13 wie 
: 433 2 13 die 
: 435 2 13 Träu
: 437 2 11 me  
: 439 3 11 schla
: 442 4 9 fen?
- 447
: 447 1 11 Hast 
: 448 1 11 du 
: 450 1 13 je 
: 451 2 13 ei
: 453 3 13 nen 
: 456 1 11 ge
: 457 4 13 sehn
- 463
: 468 1 13 Träu
: 469 1 13 me 
: 471 3 13 schla
: 474 2 11 fen 
: 477 2 11 ein
: 479 4 13 sam
- 486
: 498 1 4 Und 
: 499 2 13 weißt 
: 502 1 13 Du 
: 503 2 13 wie 
: 505 2 13 die 
: 507 3 13 Feen 
: 510 1 11 ver
: 512 2 11 zau
: 515 3 9 bern
- 519
: 519 1 9 Hast 
: 520 1 9 du 
: 522 1 13 je 
: 523 2 16 ei
: 525 2 16 ne 
: 527 2 13 ge
: 529 4 9 sehn
- 536
: 539 1 13 Feen 
: 541 1 13 ver
: 542 3 13 zau
: 546 1 11 bern  
: 548 2 11 ein
: 551 4 13 sam
- 558
: 569 1 9 Und 
: 570 2 21 weißt 
: 572 1 20 du 
: 573 2 20 wie 
: 575 2 18 die 
: 577 2 18 En
: 579 2 16 gel 
: 582 3 13 flie
: 585 3 9 gen
- 589
: 590 1 9 Hast 
: 591 1 9 du 
: 592 1 13 je 
: 594 1 16 ei
: 596 2 16 nen 
* 598 2 13 ge
: 600 4 9 sehen
- 606
: 610 1 18 En
: 612 1 16 gel 
: 614 3 16 flie
: 617 1 14 gen 
: 619 2 14 ein
: 622 4 13 sam
- 630
: 641 1 9 Ich 
: 642 2 16 weißt 
: 644 1 16 es 
* 645 3 16 geht 
: 648 1 14 Dir 
: 650 1 13 ganz 
: 652 2 13 ge
: 654 3 11 nau 
: 657 2 9 so
- 660
: 662 1 9 Was 
: 663 1 9 hast 
: 664 1 13 du 
: 665 1 16 mit 
: 667 2 16 mir 
: 669 1 13 ge
: 671 4 9 macht
- 677
: 681 1 14 Du 
: 682 3 13 und 
: 685 2 13 ich 
: 688 1 11 ge
: 689 3 11 mein
* 692 9 11 sam
- 703
: 717 1 13 En
: 718 2 13 gel 
: 720 3 13 flie
: 724 1 11 gen 
: 725 3 11 ein
: 728 4 13 sam
- 734
: 753 1 13 Du 
: 754 1 13 und 
: 756 3 13 ich 
: 760 1 11 ge
: 761 3 11 mein
: 764 5 9 sam
- 771
: 789 1 13 En
: 790 2 13 gel 
: 792 3 13 flie
: 795 1 11 gen 
: 796 3 11 ein
: 799 4 13 sam
- 805
: 824 1 13 Nie
: 826 2 13 mals 
: 828 2 13 mehr 
: 830 1 11 al
: 831 3 11 lein 
: 834 5 9 sein 
- 841
: 852 3 7 Dann 
: 856 1 7 bin 
: 857 1 5 ich 
: 861 3 9 auf
: 864 2 11 ge
* 866 4 11 wacht
: 870 6 9 - 
- 878
: 889 2 7 und 
: 891 2 7 ha
: 893 1 5 be 
: 895 4 9 mich 
: 900 1 11 ge
: 901 3 9 fragt
: 904 7 9 - 
- 913
: 924 2 6 bist 
: 926 2 6 du 
: 928 2 4 auch 
: 931 4 6 so 
: 935 2 9 al
: 937 4 11 lei
: 941 7 9 ~n 
- 950
: 959 2 5 und 
: 961 3 7 muss 
: 964 2 9 das 
: 966 3 9 wirk
: 970 2 11 lich 
: 973 2 9 sei
: 975 8 9 ~n
- 985
: 1002 1 13 En
: 1003 2 13 gel 
: 1005 3 13 flie
: 1009 1 11 gen 
: 1010 3 11 ein
* 1013 4 13 sam
- 1019
: 1038 1 13 Du 
: 1039 1 13 und 
: 1041 3 13 ich 
: 1044 1 11 ge
: 1045 3 11 mein
: 1048 5 9 sam
- 1055
: 1073 1 13 En
: 1074 2 13 gel 
: 1077 3 13 flie
: 1080 1 11 gen 
: 1081 3 11 ein
: 1084 4 13 sam
- 1090
: 1109 1 13 Nie
: 1111 2 13 mals 
: 1113 2 13 mehr 
: 1115 1 11 al
: 1116 3 11 lein 
: 1119 5 9 sein 
- 1126
: 1145 1 13 En
: 1146 2 13 gel 
: 1148 3 13 flie
: 1151 1 11 gen 
: 1152 3 11 ein
: 1155 4 13 sam
- 1161
: 1180 1 13 Nie
: 1182 2 13 mals 
: 1184 2 13 mehr 
: 1186 1 11 al
: 1187 3 11 lein 
* 1191 12 9 sein 
E