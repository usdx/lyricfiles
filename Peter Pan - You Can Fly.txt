#TITLE:You Can Fly
#ARTIST:Peter Pan
#MP3:Peter Pan - You Can Fly.mp3
#COVER:Peter Pan - You Can Fly [CO].jpg
#BACKGROUND:Peter Pan - You Can Fly [BG].jpg
#BPM:240
#GAP:5625
#ENCODING:UTF8
#LANGUAGE:English
#EDITION:SingStar Singalong with Disney
: 0 2 57 Think
: 5 2 57  of
: 9 2 58  a
: 12 2 60  won
: 15 3 58 der
: 19 2 57 ful
: 22 15 60  thought
- 39
* 44 5 62 A
: 50 3 60 ny
: 55 2 58  mer
: 58 2 57 ry
: 61 2 55  lit
: 64 2 53 tle
: 67 12 57  thought
- 81
: 88 4 55 Think
: 93 3 58  of
: 98 4 52  Christ
: 102 2 55 mas
- 105
: 107 3 53 Think
: 111 2 57  of
: 114 6 48  snow
- 121
: 122 3 55 Think
: 126 3 58  of
: 130 3 52  sleigh
: 134 3 55  bells
- 138
: 138 2 53 Off
: 142 2 57  you
* 144 4 60  go
- 149
: 149 3 58 Like
: 153 3 58  rein
: 156 3 62 deer
: 159 3 55  in
: 163 3 58  the
: 166 29 62  sky
- 196
: 198 2 64 You
: 201 2 64  can
: 204 4 65  fly
- 209
* 210 2 60 You
: 213 3 60  can
: 217 5 58  fly
- 223
: 223 2 64 You
: 226 2 64  can
: 229 28 65  fly
- 259
: 270 2 65 Think
: 273 3 65  of
: 277 2 67  the
: 280 3 69  hap
: 284 3 67 pi
: 287 4 65 est
* 291 20 72  things
- 312
: 313 5 74 It's
: 321 2 72  the
: 324 2 70  same
: 327 2 69  as
: 330 2 67  hav
: 333 2 65 ing
: 336 13 64  wings
- 351
: 357 3 55 Take
: 361 3 58  the
: 366 3 52  path
: 370 3 55  that
- 374
: 374 3 53 Moon
: 379 2 57 beams
: 383 5 48  make
- 390
: 393 2 55 If
: 397 3 58  the
: 401 3 52  moon
: 405 3 55  is
: 409 3 53  still
: 412 1 57  a
: 414 4 60 wake
- 419
: 421 2 57 You'll
: 424 3 58  see
: 428 2 62  him
: 432 2 55  wink
: 435 3 58  his
: 439 15 62  eye
: 455 6 55 ~
* 461 6 58 ~
: 467 6 62 ~
- 474
: 475 2 67 You
: 479 2 67  can
: 483 5 65  fly
- 489
: 489 2 60 You
: 492 2 60  can
: 496 6 58  fly
- 503
: 503 1 67 You
: 506 2 67  can
: 509 15 65  fly
- 526
* 557 2 64 Up
: 561 2 57  you
: 565 2 57  go
: 569 1 55  with
: 571 2 54  a
: 574 4 52  heigh
: 578 2 59  and
: 583 2 59  ho
- 586
: 587 2 56 To
: 589 2 54  the
: 591 4 52  stars
: 595 3 61  be
: 600 3 61 yond
: 604 4 61  the
: 608 8 62  blue
- 618
: 624 3 61 There's
: 628 3 62  a
: 632 2 64  Ne
: 635 2 64 ver
: 639 5 64  Land
- 645
: 646 3 64 Wait
: 651 4 64 ing
: 657 4 64  for
* 662 21 66  you
- 684
: 686 5 66 Where
: 693 3 64  all
: 697 3 62  your
: 701 2 61  hap
: 705 2 62 py
- 708
: 709 6 59 Dreams
: 717 6 61  come
: 726 17 57  true
- 745
: 756 4 57 E
: 762 3 57 very
: 767 10 60  dream
: 778 4 58  that
: 782 6 57  you
: 788 10 55  dream
- 799
: 800 6 53 Will
: 807 6 52  come
: 814 28 53  true
: 843 12 51 ~
- 857
: 874 4 49 When
: 879 4 54  there's
: 884 4 56  a
* 888 5 58  smile
: 893 5 56  in
: 898 5 54  your
: 903 21 61  heart
- 926
: 935 5 63 There's
: 941 6 61  no
: 948 3 59  bet
: 952 4 58 ter
: 957 5 56  time
: 962 6 54  to
: 968 32 58  start
- 1002
: 1016 5 56 Think
: 1021 3 59  of
: 1026 4 53  all
: 1030 4 56  the
- 1035
: 1035 4 54 Joy
: 1039 2 58  you'll
: 1043 5 49  find
- 1050
: 1052 5 56 When
: 1057 3 59  you
: 1061 3 53  leave
- 1065
: 1065 3 56 The
: 1068 3 54  world
: 1072 1 58  be
: 1074 5 61 hind
- 1080
: 1080 2 58 And
: 1084 2 59  bid
: 1087 2 63  your
: 1090 3 56  cares
: 1093 3 59  good
: 1097 7 63 bye
: 1104 7 66 ~
- 1113
: 1128 2 65 You
: 1132 2 65  can
: 1137 4 66  fly
- 1142
: 1142 2 61 You
: 1146 3 61  can
: 1149 5 59  fly
- 1155
: 1155 1 65 You
: 1158 2 65  can
* 1161 5 66  fly
- 1167
: 1167 1 61 You
: 1170 2 61  can
: 1173 5 59  fly
- 1179
: 1180 1 65 You
: 1182 2 65  can
: 1185 32 66  fly
- 1219
: 1464 2 64 When
: 1468 3 69  there's
: 1472 3 71  a
: 1476 3 73  smile
: 1481 3 71  in
: 1485 3 69  your
: 1489 16 76  heart
- 1507
: 1514 5 78 There's
: 1521 5 76  no
: 1527 3 74  bet
: 1532 2 73 ter
: 1536 4 71  time
: 1541 6 69  to
* 1549 38 68  start
- 1589
: 1610 2 71 Think
: 1615 2 74  of
: 1619 3 68  all
: 1624 2 71  the
- 1627
: 1628 4 69 Joy
: 1632 3 73  you'll
: 1636 4 64  find
- 1642
: 1645 3 71 When
: 1650 2 74  you
: 1654 2 68  leave
: 1658 2 71  the
: 1662 2 69  world
: 1666 2 73  be
: 1668 3 76 hind
- 1672
: 1674 2 73 And
: 1678 2 74  bid
: 1681 2 78  your
: 1685 3 71  cares
: 1689 4 74  good
: 1693 7 78 bye
: 1701 12 81 ~
- 1715
: 1729 3 68 You
: 1733 3 68  can
: 1737 5 69  fly
- 1743
: 1744 2 64 You
: 1747 2 64  can
: 1750 5 62  fly
- 1756
: 1757 1 56 You
: 1760 2 56  can
: 1764 5 57  fly
- 1770
: 1770 1 57 You
: 1773 1 57  can
: 1776 6 56  fly
- 1783
: 1783 2 56 You
: 1786 2 56  can
: 1789 13 57  fly
: 1802 12 56 ~
: 1815 7 57 ~
E