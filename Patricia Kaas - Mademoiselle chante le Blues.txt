#TITLE:Mademoiselle chante le Blues
#ARTIST:Patricia Kaas
#MP3:Patricia Kaas - Mademoiselle chante le Blues.mp3
#VIDEO:Patricia Kaas - Mademoiselle chante le Blues.avi
#COVER:Patricia Kaas - Mademoiselle chante le Blues.jpg
#BPM:315,14
#GAP:19317
#ENCODING:UTF8
#LANGUAGE:French
#GENRE:Pop
#YEAR:1987
: 0 3 2 Y'en
: 4 4 5  a
: 12 3 2  qu'é
: 16 5 5 lčvent
: 24 2 2  des
: 28 7 5  gosses
- 38
: 40 4 5 Au
: 48 3 2  fond
: 53 4 5  d'un
: 61 3 5  H
: 67 5 5 L
: 73 3 2 M
- 86
: 96 4 4 Y'en
: 101 4 5  a
: 109 3 2  qui
: 113 5 5  roulent
: 120 3 2  leur
: 125 6 5  bosse
- 135
: 137 4 5 Du
: 144 2 2  Bré
: 150 5 5 sil
: 157 3 5  en
: 162 3 5  U
: 169 3 2 kraîne
- 182
: 192 4 5 Y'en
: 197 4 4  a
: 204 2 2  qui
: 208 6 4  font
: 216 2 2  la
: 221 5 4  noce
- 231
: 233 4 4 Du
: 240 2 2  cô
: 246 3 4 té
: 253 3 4  d'An
: 258 6 4 gou
: 265 5 4 lčme
- 279
: 282 2 -3 Et
: 285 2 -1  y'en
: 288 2 2  a
: 293 7 5  męme
: 302 8 2 ~
- 320
: 337 2 2 Qui
: 341 4 5  mi
: 348 2 2 litent
: 352 5 5  dans
: 360 3 2  la
: 365 4 5  rue
- 373
: 375 6 5 A
: 383 1 2 vec
: 388 4 5  tracts
: 397 3 5  et
: 401 6 5  bande
: 408 4 2 roles
- 422
: 432 3 4 Y'en
: 436 5 5  a
: 444 4 2  qui
: 449 3 5  n'en
: 456 2 2  peuvent
: 461 5 5  plus
- 470
: 472 4 5 De
: 479 5 2  jouer
: 485 3 5  les
: 493 2 5  sex
: 497 4 5  sym
: 504 3 2 bols
- 517
: 528 3 5 Y'en
: 532 5 4  a
: 540 3 4  qui
: 544 6 4  vendent
: 552 3 2  l'a
: 556 8 4 mour
- 567
: 569 4 4 Au
: 577 3 2  fond
: 582 4 4  de
: 588 3 4  leur
: 593 4 4  ba
: 600 4 4 gnole
- 607
: 609 2 -3 Mad'
: 613 1 -1 moi
: 616 2 1 selle
: 620 2 2  chante
: 624 2 4  le
: 628 11 5  blues
: 641 19 2 ~
- 681
: 707 2 2 So
: 710 1 2 yez
: 713 1 2  pas
: 716 1 2  trop
: 720 4 2  ja
: 725 7 9 louses
: 734 9 2 ~
- 749
: 752 3 2 Mad'
: 757 2 2 moi
: 760 3 2 selle
: 764 2 2  bois
: 768 2 2  du
: 772 7 5  rouge
: 780 16 4 ~
- 806
: 816 2 -3 Mad'
* 820 9 9 moi
: 832 8 8 selle
: 844 10 7  chante
: 856 6 5  le
: 865 9 7  blues
: 876 4 5 ~
: 881 11 2 ~
- 934
: 1057 2 2 Y'en
: 1060 4 5  a
: 1068 2 2  huit
: 1073 4 5  heures
: 1080 2 2  par
: 1085 6 5  jour
- 1094
: 1096 4 5 Qui
: 1103 2 2  tapent
: 1108 7 5  sur
: 1116 3 5  des
: 1120 4 5  ma
: 1128 4 5 chines
: 1133 6 2 ~
- 1148
: 1152 3 4 Y'en
: 1157 4 5  a
: 1164 2 2  qui
: 1169 6 5  font
: 1176 1 2  la
: 1181 5 5  cour
* 1192 6 5  mas
: 1201 2 2 cu
: 1204 4 5 line
: 1213 2 5  fé
: 1217 6 5 mi
: 1224 3 2 nine
- 1237
: 1248 3 5 Y'en
: 1252 4 4  a
: 1260 3 4  qui
: 1264 5 4  lčchent
: 1272 2 4  les
: 1276 5 4  bottes
- 1286
: 1288 4 4 Comme
: 1296 3 2  on
: 1300 6 4  lčche
: 1309 3 4  les
: 1313 3 4  vi
: 1320 5 4 tri
: 1328 1 4 nes
- 1335
: 1337 2 -2 Et
: 1340 2 -2  y'en
: 1343 2 2  a
: 1349 8 5  męme
: 1359 10 2 ~
- 1379
: 1393 1 2 Qui
: 1397 5 5  font
: 1404 2 2  du
: 1409 6 5  ci
: 1417 2 2 né
: 1421 8 5 ma
: 1432 6 5  qu'on
: 1440 2 2  ap
: 1444 6 5 pelle
: 1452 3 5  Ma
: 1457 6 5 ry
: 1465 7 2 lin
- 1474
* 1476 9 7 Mais
* 1486 3 9 ~
- 1495
: 1497 3 7 Ma
: 1501 2 5 ry
: 1505 2 7 lin
: 1509 2 9  Du
: 1513 8 2 bois
- 1527
: 1530 3 2 S'ra
: 1536 2 2  ja
* 1541 4 9 mais
: 1547 4 9  Nor
: 1552 3 9 ma
: 1560 9 2  Jean
- 1580
: 1584 1 2 Faut
: 1588 4 4  pas
: 1595 3 4  croire
: 1600 2 4  que
: 1603 1 4  le
: 1607 5 4  ta
: 1613 2 4 lent
- 1621
: 1624 4 4 C'est
: 1631 2 2  tout
: 1636 4 4  c'qu'on
: 1644 2 4  s'i
: 1648 4 4 ma
: 1656 4 4 gine
- 1663
: 1665 2 -3 Mad'
: 1668 2 -1 moi
: 1672 2 1 selle
: 1676 2 2  chante
: 1680 2 4  le
: 1684 12 5  blues
: 1698 18 2 ~
- 1737
: 1763 1 2 So
: 1765 1 2 yez
: 1768 1 2  pas
: 1772 1 2  trop
: 1775 4 2  ja
: 1781 6 9 louses
: 1789 15 2 ~
- 1806
: 1808 3 2 Mad'
: 1813 2 2 moi
: 1816 2 2 selle
: 1820 2 2  bois
: 1823 3 2  du
: 1829 8 5  rouge
: 1838 12 4 ~
- 1860
: 1872 2 -3 Mad'
* 1877 9 9 moi
: 1889 8 8 selle
: 1901 6 7  chante
: 1914 4 5  le
: 1921 10 7  blues
: 1932 4 5 ~
: 1938 6 2 ~
- 1953
: 1957 3 2 Elle
: 1961 5 2  a
: 1969 2 2  du
: 1973 8 5  gos
: 1984 20 5 pel
- 2013
: 2016 2 2 Dans
: 2020 6 5  la
: 2029 9 2  voix
- 2048
: 2056 2 9 Et
: 2059 2 7  elle
: 2062 2 5  y
: 2067 49 9  croit
: 2117 6 7 ~
- 2165
: 2546 2 8 Y'en
: 2549 4 10  a
: 2556 2 8  qui
* 2561 4 10  s'font
: 2567 2 10  bonne
: 2572 4 10  soeur
- 2582
* 2585 4 10 A
* 2591 2 8 vo
* 2596 4 10 cate
: 2604 3 10  phar
: 2608 3 8 ma
: 2615 11 3 cienne
- 2635
* 2638 5 10 Y'en
: 2644 6 8  a
: 2653 2 6  qui
: 2656 5 8  ont
: 2665 2 6  tout
: 2668 4 8  dit
- 2678
: 2681 4 8 Quand
: 2687 2 6  elles
: 2692 6 8  ont
: 2700 2 8  dit
: 2704 4 10  je
: 2712 14 3 t'aime
- 2733
: 2736 3 6 Y'en
: 2740 5 5  a
: 2748 2 3  qui
: 2752 5 5  sont
: 2759 2 3  vieilles
: 2764 8 5  filles
- 2775
: 2777 4 5 Du
: 2784 1 3  cô
: 2788 4 5 té
: 2796 3 5  d'An
: 2800 6 5 gou
: 2808 8 5 lčme
- 2822
: 2825 2 -2 Et
: 2828 2 0  y'en
: 2831 3 3  a
: 2837 8 6  męme
: 2846 14 3 ~
- 2870
: 2880 2 3 Qui
: 2884 4 6  joue
: 2892 3 3  femme
: 2896 5 6  li
* 2904 4 3 bé
* 2909 3 6 rée
- 2918
: 2921 3 6 P'tit
: 2927 3 3  joint
: 2933 4 6  et
: 2940 2 3  gar
: 2946 5 6 dé
: 2953 9 3 nal
- 2973
: 2977 1 6 Qui
: 2980 6 6  mé
: 2988 3 3 lange
: 2994 5 6  vie
: 3000 3 3  en
: 3005 6 6  rose
: 3012 4 6  et
: 3018 2 3  i
* 3024 10 8 mage
: 3036 2 8  d'E
: 3040 5 8 pi
: 3048 10 3 nal
- 3069
: 3073 2 6 Qui
: 3076 3 5  veulent
: 3083 2 3  se
: 3088 4 5  faire
: 3095 2 3  du
: 3099 5 5  bien
- 3106
: 3108 3 5 Sans
: 3112 6 5  ja
: 3121 4 5 mais
: 3132 4 5  s'faire
: 3137 1 5  du
: 3144 6 5  mal
- 3151
: 3153 2 -2 Mad'
: 3157 2 0 moi
: 3160 1 2 selle
: 3164 2 3  chante
: 3168 1 5  le
: 3171 7 6  blues
: 3183 19 3 ~
- 3223
: 3249 3 3 So
: 3253 1 3 yez
: 3256 1 3  pas
: 3260 1 3  trop
: 3263 3 3  ja
* 3269 7 10 louses
: 3277 14 3 ~
- 3293
: 3295 3 3 Mad'
: 3300 2 3 moi
: 3304 2 3 selle
: 3308 2 3  bois
: 3311 3 3  du
: 3316 10 6 rouge
: 3327 13 5 ~
- 3350
* 3359 1 -2 Mad'
* 3364 9 10 moi
* 3377 8 9 selle
: 3389 6 8  chante
: 3400 5 6  le
: 3408 11 8  blues
: 3420 4 6 ~
: 3425 5 3 ~
- 3441
: 3445 3 3 Elle
: 3449 5 3  a
: 3456 2 3  du
* 3461 9 6  gos
: 3473 20 6 pel
- 3501
: 3504 3 3 Dans
: 3508 7 6  la
: 3519 10 3  voix
- 3540
: 3544 3 3 Et
: 3548 2 8  elle
: 3551 1 6  y
: 3555 48 10  croit
- 3609
* 3612 5 10 Mad'
: 3621 4 10 moi
: 3629 4 10 selle
: 3636 5 8  chante
: 3646 2 6  le
: 3651 7 8  blues
: 3659 3 6 ~
: 3663 6 3 ~
: 3670 12 3 ~
- 3692
* 3720 5 10 Oh
* 3731 4 8 ~
: 3742 3 10  le
: 3748 6 10  blues
: 3755 13 3 ~
: 3769 15 6 ~
- 3794
: 3804 4 10 Mad'
: 3815 2 10 moi
: 3820 4 10 selle
: 3827 2 10  chante
- 3836
: 3839 2 8 Le
: 3843 4 10  blues
: 3854 4 3 ~
: 3865 13 6 ~
E