#TITLE:Sang Real
#ARTIST:Dredg
#MP3:Dredg - Sang Real.mp3
#COVER:Dredg - Sang Real [CO].jpg
#BACKGROUND:Dredg - Sang Real [BG].jpg
#BPM:120
#GAP:29400
: 0 2 67 I'm
: 2 3 69  the
: 7 1 70  ad
: 8 2 69 dict
: 10 2 67  on
: 12 1 69  the
: 13 3 70  cor
: 16 2 69 ner
- 18
: 19 3 67 I'm
: 22 3 69  the
: 27 1 70  law
: 28 1 69 yer
: 29 3 67  in
: 32 1 69  the
: 33 3 70  tow
: 36 3 69 er
- 38
: 39 2 67 I'm
: 41 3 69  the
: 46 2 72  bo
: 48 1 72 dy
: 49 2 72  with
: 51 1 70  the
: 52 3 72  coro
: 55 2 70 na
- 57
: 58 2 70 No,
: 60 2 70  the
: 63 1 69  lea
: 64 1 69 der
: 65 3 69  with
: 69 1 69  all
: 70 2 69  the
: 72 2 69  po
: 74 2 67 wer
- 76
: 77 3 67 You're
: 80 2 69  the
: 83 5 70  pillow
: 88 1 69  with the
: 89 3 70  cool
: 92 6 69  side
- 100
: 101 1 69 The
: 102 2 70  sand
: 104 3 69  du
: 107 2 67 r
: 109 1 69 ing
: 110 2 70  high
: 112 5 69  tide
- 119
: 120 1 72 The
: 121 2 72  co
: 124 4 72 ck
: 128 2 70 tail
: 130 2 72  pool
: 132 6 70 side
- 138
: 139 1 70 The
: 140 1 69  wa
: 142 1 69 ter
: 143 1 69  when
: 145 4 69  clouds
: 149 1 69  col
: 150 5 67 lide
- 155
: 156 1 74 From
: 157 3 74  the
: 160 8 74  in
: 169 5 70 si
: 174 2 72 de
: 176 8 72  out,
: 189 5 77  we
: 194 2 79  were
: 196 20 74  formed
- 230
: 231 1 70 And
: 232 2 74  from
: 234 1 74  the
: 236 10 74  in
: 246 5 70 si
: 251 3 72 de
: 254 8 72  out,
: 267 4 77  we
: 271 3 79  will
: 274 15 74  fall
- 303
: 304 5 79 Soon
: 309 5 81  this
: 314 10 79  wall
: 324 9 77  will
: 333 11 72  come
: 344 4 77  to
: 348 3 79  an
: 351 13 79  end
- 400
: 464 3 67 I'm
: 467 3 69  a
: 472 1 70  lo
: 473 1 69 cal
: 474 2 67  but
: 476 1 69  for
: 477 3 70 ei
: 480 3 69 gner
- 483
: 484 2 67 Still
: 486 2 69  the
: 491 1 70  ad
: 492 2 69 dict,
: 494 1 67  yet
: 495 2 69  I'm
: 497 3 70  so
: 500 2 69 ber
- 502
: 503 3 67 Still
: 506 3 69  the
: 510 2 72  bo
: 512 1 72 dy
: 513 2 72  with
: 515 1 70  the
: 517 3 72  cor
: 520 2 70 oner
- 521
: 522 2 70  Ma
: 524 2 70 ny
: 526 4 69  friends,
: 530 2 69  yet
: 532 2 69  still
: 535 1 69  a
: 536 2 69  lo
: 539 3 67 ner
- 541
: 542 2 67 You're
: 544 3 69  the
: 549 1 70  pil
: 550 2 69 low
: 553 1 67  with
: 554 1 69  the
: 555 2 70  cool
: 558 6 69  side
- 563
: 564 1 69 The
: 566 2 70  sand
: 568 2 69  du
: 571 2 67 r
: 573 1 69 ing
: 575 2 70  high
: 577 6 69  tide
- 583
: 584 1 72 The
: 585 3 72  co
: 588 4 72 ck
: 592 1 70 tail
: 594 2 72  pool
: 596 5 70 side
- 602
: 603 2 70 The
: 605 1 69  wa
: 606 1 69 ter
: 607 2 69  when
: 609 4 69  clouds
: 614 1 69  col
: 615 4 67 lide
- 619
: 620 2 74 From
: 622 2 74  the
: 624 9 74  in
: 634 5 70 si
: 639 2 72 de
: 641 11 72  out,
: 653 4 77  we
: 658 2 79  were
: 661 14 74  formed
- 693
: 694 3 74 And
: 697 2 74  from
: 699 2 74  the
: 701 7 74  in
: 711 4 70 si
: 716 2 72 de
: 718 12 72  out,
: 731 4 77  we
: 735 3 79  will
: 738 15 74  fall
- 768
: 769 5 79 Soon
: 774 5 81  this
: 779 8 79  all
: 788 8 77  will
: 797 11 72  come
: 808 5 77  to
: 813 2 79  an
: 815 15 74  end
- 846
: 847 4 79 Soon
: 851 6 81  this
: 857 9 79  all
: 866 8 77  will
: 875 8 72  come
: 885 5 77  to
: 890 3 79  an
: 893 14 79  end
- 1020
: 1044 2 74 From
: 1046 2 74  the
: 1048 11 74  in
: 1060 4 70 si
: 1064 3 72 de
: 1067 9 72  out,
: 1079 5 77  we
: 1084 3 79  were
: 1087 19 74  formed
- 1119
: 1120 2 74 And
: 1122 2 74  from
: 1124 2 74  the
: 1126 11 74  in
: 1137 4 70 si
: 1141 3 72 de
: 1144 9 72  out,
: 1156 4 77  we
: 1161 2 79  will
: 1163 17 74  fall
- 1194
: 1195 5 79 Soon
: 1200 5 81  this
: 1205 9 79  all
: 1214 8 77  will
: 1224 9 72  come
: 1234 5 77  to
: 1239 2 79  an
: 1241 16 74  end
- 1271
: 1272 5 79 Soon
: 1277 5 81  this
: 1282 9 79  all
: 1292 10 77  will
: 1302 9 72  come
: 1311 5 77  to
: 1316 4 79  an
: 1320 23 79  end
E
