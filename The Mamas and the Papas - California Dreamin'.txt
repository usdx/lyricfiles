#TITLE:California Dreamin'
#ARTIST:The Mamas and the Papas
#MP3:The Mamas and the Papas - California Dreamin'.mp3
#COVER:The Mamas and the Papas - California Dreamin' [CO].jpg
#BACKGROUND:The Mamas and the Papas - California Dreamin' [BG].jpg
#BPM:224
#GAP:7900
#ENCODING:UTF8
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
: 0 3 13 All
: 4 2 15  the
: 8 3 13  leaves
: 13 2 11  are
: 17 13 13  brown
- 32
: 57 2 13 and
: 61 2 15  the
* 65 6 13  sky
: 73 2 11  is
: 78 7 8  grey.
- 87
: 129 2 16 I've
: 133 4 16  been
: 140 1 16  for
: 142 1 15  a
: 145 7 16  walk
- 154
: 185 3 16 on
: 189 2 13  a
: 193 3 16  win
: 197 4 16 ter's
: 205 8 15  day.
- 215
: 256 2 8 I'd
: 260 2 8  be
: 264 3 11  safe
: 268 2 8  and
* 272 3 16  wa
: 276 7 13 ~rm,
- 285
: 312 2 13 if
: 316 2 15  I
: 320 3 13  was
: 324 2 11  in
: 328 3 11  L.
: 332 6 8 A.
- 340
: 384 3 20 Ca
: 388 2 18 li
: 392 3 16 for
: 396 2 15 nia
: 400 3 16  drea
: 405 10 13 min'
- 417
: 444 2 16 on
: 448 3 18  such
: 452 2 16  a
: 456 3 18  win
: 460 2 16 ter's
* 464 22 20  day.
- 488
: 511 3 13 Stopped
: 515 2 15  in
: 519 1 13 to
: 521 2 11  a
: 525 5 13  church,
- 532
: 563 2 11 I
: 567 3 13  passed
: 571 2 11  a
: 575 2 13 lo
: 578 4 11 ~ng
: 587 2 6  the
: 591 5 8  way.
- 598
: 627 3 20 Well,
* 633 4 20  I
: 639 3 18  got
: 643 3 16  down
: 647 2 13  on
: 651 3 11  my
: 658 10 16  knees
- 670
: 690 3 13 and
: 694 2 16  I
: 698 3 13  pre
: 702 2 16 te
: 705 4 13 ~nd
: 711 2 13  to
: 715 6 15  pray.
- 723
: 754 2 20 You
: 758 3 20  know
: 762 2 20  the
: 766 3 20  prea
: 770 2 18 cher
: 774 3 16  lights
: 778 2 15  your
: 782 3 16  coa
: 786 6 13 ~ls.
- 794
: 818 3 13 He
: 822 2 13  kno
: 825 3 11 ~ws,
: 830 4 11  I'm
: 836 3 13  gon
: 840 3 11 na
: 847 6 8  stay.
- 855
: 893 3 20 Ca
: 897 2 18 li
: 901 3 16 for
: 906 2 15 nia
: 910 3 16  drea
: 914 16 13 min'
- 932
: 953 2 16 on
: 957 2 18  such
: 961 2 16  a
: 965 3 18  win
: 969 2 16 ter's
* 973 31 20  day.
- 1006
: 1540 5 16 All
: 1548 4 16  the
: 1556 6 15  leaves
: 1564 3 15  are
: 1569 13 13  bro
: 1585 12 15 ~wn
- 1599
: 1612 2 15 and
: 1616 2 15  the
: 1620 6 15  sky
: 1628 2 15  is
: 1633 13 15  gre
: 1648 13 16 ~y.
- 1663
: 1668 4 20 I've
: 1675 5 20  been
: 1683 3 18  for
: 1688 4 18  a
: 1695 16 20  wa
* 1715 9 21 ~lk
- 1726
: 1739 3 18 on
: 1743 2 18  a
: 1747 3 18  win
: 1752 4 18 ter's
: 1759 19 15  day.
- 1780
: 1795 3 16 If
: 1802 5 16  I
: 1810 3 15  di
: 1814 4 15 dn't
: 1822 4 13  tell
: 1830 9 13  he
: 1842 7 15 ~r,
- 1851
: 1866 2 15 I
: 1870 2 15  could
: 1874 4 15  leave
: 1882 2 15  to
: 1886 20 15 day.
- 1908
: 1922 6 16 Ca
: 1930 4 16 li
: 1938 5 15 for
: 1946 2 15 nia
* 1950 6 13  drea
: 1958 6 13 min'
- 1965
: 1965 2 16 on
: 1969 2 18  such
: 1973 2 16  a
: 1977 3 15  win
: 1981 2 13 ter's,
- 1985
: 1993 3 13 Ca
: 1997 2 13 li
: 2001 5 15 for
: 2009 2 15 nia
: 2013 6 16  drea
: 2021 6 16 min'
- 2028
: 2029 2 16 on
: 2033 2 18  such
: 2037 2 16  a
: 2041 3 15  win
: 2045 2 15 ter's,
- 2049
: 2056 3 13 Ca
: 2060 2 13 li
: 2064 5 15 for
: 2072 2 15 nia
: 2076 6 16  drea
: 2084 5 16 min'
- 2090
: 2091 2 16 on
: 2095 2 18  such
: 2099 2 16  a
: 2103 2 15  win
: 2107 2 13 ter's
* 2111 55 20  day.
E