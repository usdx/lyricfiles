#TITLE:Bare Necessities
#ARTIST:The Jungle Book
#LANGUAGE:English
#EDITION:SingStar Singalong with Disney
#MP3:The Jungle Book - Bare Necessities.mp3
#COVER:The Jungle Book - Bare Necessities [CO].jpg
#BACKGROUND:The Jungle Book - Bare Necessities [BG].jpg
#BPM:350
#GAP:11357,14
: 0 2 55 Look
: 8 4 57  for
: 14 4 60  the
: 27 12 64  bare
: 41 4 62  ne
: 47 3 64 ces
: 53 2 62 si
: 57 7 60 ties
- 65
: 66 3 60 The
: 72 3 62  sim
: 78 4 60 ple
* 85 4 62  bare
: 92 4 60  ne
: 98 4 62 ces
: 104 3 60 si
: 108 8 57 ties
- 118
: 122 3 55 For
: 126 3 60 get
: 130 4 55  a
: 135 5 60 bout
: 143 3 64  your
: 150 4 69  wor
: 156 4 67 ries
: 163 4 65  and
: 169 5 64  your
: 176 6 62  strife
- 184 185
: 208 4 67 I
: 215 4 69  mean
: 221 5 67  the
: 235 10 69  bare
: 249 4 67  ne
: 254 3 69 ces
: 260 2 67 si
: 264 8 64 ties
- 273
: 274 4 60 Are
: 281 3 62  Mo
: 287 4 60 ther
: 293 3 62  Na
: 300 3 60 ture's
: 306 5 67  re
: 313 3 60 ci
: 317 8 60 pes
- 326
: 326 4 62 That
: 333 3 64  bring
: 339 4 67  the
: 346 4 64  bare
: 352 3 67  ne
: 359 1 64 ces
: 366 3 60 si
: 370 8 57 ties
: 378 5 55  of
: 385 3 60  life
- 390 391
: 418 4 60 Wher
: 422 4 59 e
: 428 3 59 ver
: 432 6 57  I
: 439 10 55  wan
* 449 9 62 der
- 460 461
F 471 12 60 Wherever
F 483 10 60  I
F 494 7 60  roam
- 503 504
F 521 6 55 I
F 527 6 55  couldn't
F 533 7 55  be
F 549 14 55  fonder
- 565 566
: 574 7 62 Of
: 581 7 60  my
: 588 6 62  big
: 595 9 64  home
- 606 607
: 627 4 64 The
: 634 2 65  bees
: 640 5 67  are
: 647 4 69  buzz
: 653 4 69 in'
: 660 4 69  in
: 666 5 65  the
: 672 7 64  tree
- 681
: 683 3 62 To
: 686 5 64  make
: 691 4 65  some
: 698 5 67  ho
: 705 5 67 ney
: 710 5 67  just
: 717 6 64  for
: 725 8 62  me
- 735
: 743 4 60 When
* 750 3 64  you
: 757 4 57  look
: 763 2 57  un
: 768 1 57 der
: 770 5 57  the
: 777 4 64  rocks
: 783 5 57  and
: 791 4 57  plants
- 796
: 798 5 57 And
: 805 3 65  take
: 809 2 65  a
: 812 6 65  glance
: 824 2 64  at
: 829 2 64  the
: 831 4 62  fan
: 838 4 60 cy
: 842 6 59  ants
- 850
F 852 4 55 Then
F 859 9 55  maybe
F 875 2 55  try
F 879 3 55  a
F 885 6 55  few
- 893 1077
: 1097 4 64 The
: 1105 4 65  bare
: 1112 4 67  ne
: 1118 4 69 ces
: 1123 2 69 si
* 1126 9 69 ties
: 1138 5 65  of
: 1144 4 64  life
- 1150
: 1154 4 62 Will
: 1158 6 60  come
: 1164 4 59  to
: 1168 7 60  you
- 1177 1178
: 1202 4 60 They'll
: 1209 5 57  come
: 1216 3 59  to
: 1220 7 60  you
- 1229 1230
: 1255 4 55 Look
: 1262 4 57  for
: 1268 4 60  the
: 1281 8 64  bare
: 1294 4 62  ne
: 1301 4 64 ces
: 1307 3 62 si
: 1311 9 60 ties
- 1321
: 1321 4 60 The
: 1328 4 62  sim
: 1334 4 60 ple
: 1340 4 62  bare
: 1347 4 60  ne
: 1353 4 62 ces
: 1359 3 60 si
: 1363 7 57 ties
- 1372
: 1376 4 55 For
: 1380 3 60 get
: 1384 3 55  a
: 1389 8 60 bout
: 1399 5 64  your
* 1405 5 69  wor
: 1411 5 67 ries
: 1418 5 65  and
: 1424 6 64  your
: 1431 7 62  strife
- 1440 1441
: 1462 5 67 I
: 1469 5 69  mean
: 1477 5 67  the
: 1495 7 69  bare
: 1503 4 67  ne
: 1509 5 69 ces
: 1515 3 67 si
: 1519 9 64 ties
- 1529
: 1529 5 60 That's
: 1536 5 62  why
: 1542 6 60  a
: 1549 6 62  bear
: 1555 6 60  can
: 1562 5 67  rest
: 1568 3 60  at
: 1573 9 60  ease
- 1583
: 1583 6 62 With
: 1590 5 64  just
: 1596 4 67  the
: 1602 5 64  bare
: 1611 4 67  ne
: 1615 4 64 ces
: 1621 4 60 si
: 1625 9 57 ties
: 1634 6 55  of
* 1640 4 60  life
- 1646 1647
F 1672 3 55 Now
F 1678 2 55  when
F 1680 5 55  you
F 1687 2 55  pick
F 1691 2 55  a
F 1693 9 55  paw
F 1702 6 55  paw
- 1710 1711
F 1725 2 59 Or
F 1729 2 59  a
F 1733 13 59  prickly
F 1746 5 59  pear
- 1753 1754
F 1779 4 55 And
F 1784 4 55  you
F 1791 2 55  prick
F 1795 3 55  a
F 1799 9 55  raw
F 1808 7 55  paw
- 1817 1818
F 1826 2 60 Well
F 1830 4 60  next
F 1836 7 60  time
F 1848 14 60  beware
- 1864 1865
F 1883 5 69 Don't
F 1889 5 69  pick
F 1895 4 69  the
F 1902 8 69  prickly
F 1915 4 69  pear
F 1921 3 69  with
F 1924 2 69  the
F 1927 4 69  paw
- 1932
F 1934 4 60 When
F 1940 5 60  you
F 1946 2 60  pick
F 1948 2 60  a
F 1953 4 60  pear
F 1958 2 60  try
F 1961 2 60  to
F 1964 5 60  use
F 1970 4 60  the
F 1976 5 60  claw
- 1983 1984
F 1997 4 60 But
F 2003 4 60  you
F 2009 3 60  don't
F 2015 7 60  need
F 2027 3 57  to
F 2030 5 57  use
F 2037 1 57  the
F 2041 7 57  claw
- 2049
F 2051 4 65 When
F 2058 3 65  you
F 2064 2 65  pick
F 2068 2 65  a
F 2071 4 65  pear
F 2077 2 65  of
F 2081 2 65  the
F 2084 4 65  big
F 2091 4 65  paw
F 2098 5 65  paw
- 2105 2106
F 2116 2 60 Have
F 2120 3 60  I
F 2126 5 60  given
F 2134 2 60  you
F 2137 2 60  a
F 2141 5 60  clue?
- 2148 2332
: 2352 4 64 The
: 2358 5 65  bare
: 2366 4 67  ne
: 2373 2 69 ces
: 2377 2 69 si
: 2380 10 69 ties
: 2393 5 65  of
: 2399 4 64  life
- 2404
: 2405 5 62 Will
* 2412 6 60  come
: 2419 4 59  to
: 2423 9 60  you
- 2434 2435
: 2455 4 60 They'll
: 2462 4 57  come
: 2469 4 59  to
: 2473 7 60  you
- 2482 2491
: 2511 4 55 Look
: 2518 6 57  for
: 2525 4 60  the
: 2537 9 64  bare
: 2551 3 62  ne
* 2558 3 64 ces
: 2564 2 62 si
: 2568 7 60 ties
- 2576
: 2577 4 60 The
: 2584 3 62  sim
: 2589 5 60 ple
: 2595 5 62  bare
: 2602 4 60  ne
: 2608 3 62 ces
: 2614 2 60 si
: 2618 7 57 ties
- 2627
: 2630 3 55 For
: 2633 5 60 get
: 2640 3 55  a
: 2643 9 60 bout
: 2653 7 64  your
: 2660 7 69  wor
: 2667 6 67 ries
: 2673 6 65  and
: 2680 6 64  your
: 2687 5 62  strife
- 2694
F 2699 7 55 Yeah
F 2711 5 55  man
- 2717
: 2719 3 67 I
: 2726 4 69  mean
: 2733 4 67  the
: 2745 12 69  bare
: 2759 3 67  ne
: 2765 3 69 ces
: 2771 2 67 si
: 2775 7 64 ties
- 2783
: 2784 4 60 That's
: 2790 4 62  why
: 2797 5 60  a
: 2804 6 62  bear
: 2811 5 60  can
* 2817 5 67  rest
: 2824 3 60  at
: 2828 9 60  ease
- 2838
: 2838 5 62 With
: 2844 5 64  just
: 2851 4 67  the
: 2856 6 64  bare
: 2863 5 67  ne
: 2870 3 64 ces
: 2876 2 60 si
: 2880 9 57 ties
: 2889 6 55  of
: 2895 6 60  life
- 2903 2904
F 2922 4 55 Yeah
- 2928 2929
: 2941 3 64 With
* 2947 4 64  just
: 2957 2 67  the
: 2959 9 64  bare
: 2971 2 67  ne
: 2976 6 64 ces
: 2982 5 60 si
: 2987 5 57 ties
: 2994 7 55  of
: 3001 12 60  life
- 3014
F 3015 9 55 Yeah
F 3025 10 55  man
E
