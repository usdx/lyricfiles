#TITLE:Hotel California
#ARTIST:Eagles
#EDITION:[SC]-Songs
#LANGUAGE:Englisch
#MP3:Eagles - Hotel California.mp3
#COVER:Eagles - Hotel California [CO].jpg
#BACKGROUND:Eagles - Hotel California [BG].jpg
#BPM:146
#GAP:53500
: 0 1 6 On
: 2 1 6  a
: 4 2 6  dark
: 8 1 4  de
: 10 1 4 sert
: 12 3 4  high
: 16 2 6 way,
- 20
: 32 1 4 cool
: 34 1 6 ~
: 36 1 6  wind
: 38 1 4  in
: 41 2 4  my
: 44 3 4  hair,
- 49
: 63 1 4 warm
: 65 2 6 ~
: 68 2 6  smell
: 71 1 4  of
: 73 1 4  co
: 75 3 4 li
: 80 3 6 tas
- 85
: 94 2 6 ri
: 97 1 6 sing
: 99 1 6  up
: 102 2 4  through
: 105 1 4  the
: 107 2 4  air
: 110 1 2 ~
: 112 1 -1 ~ .
- 115
: 127 1 6 Up
: 129 1 6  a
: 131 2 6 head
: 135 1 4  in
: 137 1 2  the
: 139 2 2  dis
: 143 3 6 tance
- 148
: 156 2 2 I
: 159 1 6  saw
: 161 1 6  a
: 163 1 6  shi
: 165 2 4 mme
: 168 2 2 ring
: 171 2 2  light.
- 175
: 188 1 2 My
: 190 1 4  head
: 192 1 4  grew
: 194 1 4  hea
: 196 1 4 vy
: 198 1 4  and
: 200 1 2  my
: 202 2 4  sight
: 206 2 2  grew
: 209 2 6  dim,
- 213
: 220 1 2 I
: 222 1 6  had
: 224 1 6  to
: 226 2 6  stop
: 230 1 4  for
: 232 2 4  the
: 236 2 6  night.
- 240
: 254 1 6 There
: 256 2 6  she
: 260 1 7  stood
: 262 1 6  in
: 264 1 6  the
: 266 2 7  door
: 270 2 6 way,
- 274
: 283 1 2 I
: 285 1 6  heard
: 287 1 6  the
: 289 2 6  mis
: 292 2 4 sion
: 296 3 4  bell.
- 301
: 319 1 -3 And
: 321 1 4  I
: 323 1 4  was
: 325 1 4  thin
: 327 1 2 king
: 329 1 4  to
: 331 2 4  my
: 335 2 6 self:
- 338
: 338 1 6 "This
: 340 1 6  could
: 342 2 6  be
: 345 2 6  hea
: 348 2 6 ven
: 351 1 6  or
: 353 1 6  this
: 356 1 4  could
: 358 2 4  be
: 362 2 4  he
: 365 1 2 ~
: 367 2 -1 ~ll."
- 371
: 381 1 6 Then
: 383 1 6  she
: 385 2 6  lit
: 388 1 4  up
: 390 1 2  a
: 393 2 2  can
* 397 3 6 dle
- 402
: 412 1 -3 and
: 414 2 2  she
: 417 3 6  showed
: 421 1 4  me
: 423 1 2  the
: 425 2 2  way
: 428 1 -3 ~ .
- 431
: 444 1 4 There
: 446 1 4  were
: 448 1 4  voi
: 450 1 4 ces
: 452 1 4  down
: 454 1 2  the
: 457 2 4  cor
: 460 1 2 ri
: 462 3 6 dor,
- 467
: 473 1 6 I
: 475 1 6  thought
: 478 1 4  I
: 480 2 6  heard
: 483 1 9  them
: 485 1 10 ~
: 488 1 6  say
: 490 3 4 ~ :
- 495
: 508 1 7 "Wel
: 510 2 7 come
: 514 1 7  to
: 516 1 7  the
: 518 3 7  Ho
: 522 3 7 tel
: 528 2 9  Ca
: 531 2 7 li
: 534 2 7 for
: 538 2 6 nia,
- 542
: 563 1 6 such
: 565 1 6  a
: 567 1 6  lo
: 569 1 4 ~ve
: 571 1 4 ly
: 573 3 4  place,
- 578
: 594 1 4 such
: 596 1 4  a
: 598 1 4  lo
: 600 2 2 ~ve
: 603 1 2 ly
: 605 3 2  face.
- 610
: 630 1 7 Plen
: 632 1 7 ty
: 634 2 7  of
: 637 2 7  room
: 640 2 7  at
: 644 1 7  the
: 646 3 7  Ho
: 650 2 7 tel
* 654 3 9  Ca
: 658 2 7 li
: 661 3 7 for
: 665 2 6 nia,
- 669
: 690 1 4 a
: 692 1 6 ny
: 694 3 6  time
: 698 1 4  of
: 700 3 4  year,
- 705
: 722 1 7 you
: 724 1 7  can
: 726 2 7  find
: 730 1 6  it
: 732 3 6  here."
- 737
: 759 1 6 Her
: 761 1 6  mind
: 763 1 6  is
: 766 1 7  tif
: 768 1 6 fa
: 770 2 6 ny-
: 773 2 7 twis
: 777 3 6 ted.
- 782
: 791 1 6 She
: 793 1 6  got
: 795 1 6  the
: 797 2 6  Mer
: 800 2 4 ce
: 803 2 4 des
: 807 1 4  Be
: 809 2 6 ~nz.
- 813
: 823 1 4 She
: 825 1 4  got
: 827 1 4  a
: 829 1 4  lot
: 831 1 4  of
: 833 1 6  pre
: 835 2 4 tty,
: 839 1 6  pre
: 841 1 4 tty
* 844 3 6  boys,
- 849
: 858 1 2 that
: 860 1 6  she
: 862 2 4  calls
: 867 1 4  fri
: 869 1 2 ~
: 871 2 -1 ~ends.
- 875
: 888 1 6 How
: 890 1 6  they
: 892 2 6  dance
: 896 1 4  in
: 898 1 2  the
: 900 2 2  court
: 904 1 4 yard
: 906 2 6 ~ ,
- 910
* 916 8 6 sweet
: 926 2 4  sum
: 929 1 2 mer
: 932 3 2  sweat.
- 937
: 951 2 4 Some
: 955 2 4  dance
: 959 1 4  to
: 961 1 2  re
: 963 3 6 mem
: 967 3 4 ber.
- 972
: 983 2 6 Some
: 987 2 6  dance
: 991 1 4  to
: 993 3 4  for
: 997 3 6 get.
- 1002
: 1015 1 6 So
: 1017 2 6  I
: 1021 1 6  called
: 1023 1 6  up
: 1025 1 6  the
: 1027 2 7  cap
: 1031 1 4 ta
: 1033 2 6 ~in:
- 1037
: 1045 1 4 "Please
: 1047 3 6 ~ ,
: 1052 2 6  bring
: 1055 1 4  me
: 1057 2 4  my
: 1060 1 4  wine
: 1062 2 6 ~ !"
- 1066
F 1068 1 -8 He
F 1071 2 -6  said:
- 1075
: 1077 1 4 "We
: 1079 1 4  ha
: 1081 1 4 ven't
: 1084 2 4  had
: 1087 2 4  that
: 1092 1 4  spi
: 1094 2 4 rit
: 1097 3 6  here,
- 1101
: 1103 2 -1 since
* 1109 2 6  nine
* 1113 2 6 teen
: 1117 2 4  six
: 1120 1 4 ty-
: 1122 2 4 ni
: 1125 1 2 ~
: 1127 2 -1 ~ne."
- 1131
: 1142 2 6 And
: 1146 2 6  still
: 1150 2 6  those
: 1154 1 6  voi
: 1156 1 4 ces
: 1158 1 2  are
: 1160 2 2  cal
: 1163 1 4 ling
- 1165
: 1166 2 6 from
* 1170 7 6  far
: 1179 1 4 ~
: 1181 1 2  a
: 1183 10 2 way
: 1195 1 -1 ~
: 1196 1 -3 ~ .
- 1199
: 1205 1 4 Wake
: 1207 1 4  you
: 1209 2 4  up
: 1213 1 4  in
: 1215 1 2  the
: 1217 1 4  mi
: 1219 1 4 ddle
: 1221 1 4  of
: 1223 1 2  the
: 1225 1 4  night
: 1227 2 6 ~ ,
- 1231
: 1237 1 6 just
: 1239 1 4  to
: 1241 2 6  hear
: 1245 1 9  them
: 1247 1 10 ~
: 1249 5 6  say
: 1256 1 4 ~
: 1258 1 4 ~
: 1260 2 6 ~ :
- 1264
: 1269 1 7 "Wel
: 1271 2 7 come
: 1275 1 7  to
: 1277 1 7  the
: 1279 2 7  Ho
: 1283 3 7 tel
* 1289 3 9  Ca
: 1293 1 7 li
: 1295 2 7 for
: 1299 2 6 nia,
- 1303
: 1324 1 6 such
: 1326 1 6  a
: 1328 1 6  lo
: 1330 1 4 ~ve
: 1332 1 4 ly
: 1335 3 4  place,
- 1340
: 1356 1 4 such
: 1358 1 4  a
: 1360 1 4  lo
: 1362 1 2 ~ve
: 1364 1 2 ly
: 1367 3 -1  face.
- 1372
: 1390 1 2 They
: 1392 1 7  li
: 1394 1 7 vin'
: 1396 1 7  it
: 1398 2 7  up
: 1401 2 7  at
: 1405 1 7  the
: 1407 2 7  Ho
: 1411 2 7 tel
* 1415 3 9  Ca
: 1419 1 7 li
: 1421 3 7 for
: 1426 2 6 nia.
- 1430
: 1451 1 4 What
: 1453 1 6  a
: 1455 2 6  nice
: 1459 1 4  sur
: 1461 4 4 prise,
- 1467
: 1483 1 7 bring
: 1485 1 7  your
: 1487 2 7  a
: 1491 1 6 li
: 1493 12 6 bis!"
- 1507
: 1523 1 6 Mir
: 1525 2 6 rors
: 1529 1 7  on
: 1531 2 6  the
: 1535 2 7  cei
: 1539 3 6 ling,
- 1544
: 1552 1 4 the
: 1554 2 6  pink
: 1558 1 6  cham
: 1560 2 6 paign
: 1563 2 4  on
: 1566 3 6  ice
- 1570
F 1572 1 -8 and
F 1575 1 -6  she
F 1577 2 -6  said:
- 1581
: 1586 1 6 "We
: 1588 1 4  are
: 1590 2 4  all
: 1594 2 4  just
: 1598 2 6  pri
: 1601 1 4 so
: 1603 2 4 ners
: 1606 3 6  here
- 1611
: 1617 1 6 of
: 1619 2 6  our
: 1622 1 6  own
: 1624 1 4 ~
: 1627 1 2  de
: 1629 3 2 vice."
- 1634
: 1648 1 2 And
: 1650 1 6  in
: 1652 1 6  the
: 1655 2 6  ma
: 1658 2 4 ster's
: 1662 2 2  cham
: 1665 3 6 bers
- 1670
: 1678 2 -3 they
: 1682 2 6  ga
: 1685 2 4 thered
: 1688 2 4  for
: 1691 1 2  the
: 1694 2 2  feast.
- 1698
: 1710 1 -3 They
: 1713 2 4  stab
: 1716 2 4  it
: 1719 1 4  with
: 1721 2 2  their
: 1726 1 4  stee
: 1728 2 2 ly
: 1731 3 6  knives,
- 1735
: 1736 1 6 but
: 1738 2 2  they
: 1741 2 6  just
: 1745 2 6  can't
* 1751 2 6  kill
: 1754 1 4  the
: 1756 3 6  beast.
- 1761
: 1776 2 6 Last
: 1780 1 6  thing
: 1782 2 7  I
: 1786 1 6  re
: 1788 2 7 mem
: 1792 1 5 ber
: 1794 1 6 ~ :
- 1797
: 1800 1 2 I
: 1802 2 2  was
: 1808 1 6  ru
: 1810 2 6 nning
: 1813 2 6  for
: 1816 1 4  the
: 1818 2 4  door
: 1821 2 6 ~ .
- 1825
: 1840 1 4 I
: 1842 1 4  had
: 1844 1 4  to
: 1847 1 4  find
: 1849 1 4  the
: 1851 2 4  pa
: 1854 2 4 ssage
: 1857 3 6  back
- 1861
: 1863 1 6 to
: 1865 1 6  the
: 1867 2 6  place,
: 1871 2 6  I
: 1875 1 6  was
: 1877 1 4 ~
: 1879 1 2  be
* 1881 3 4 fore
: 1885 1 2 ~
: 1887 2 -1 ~ .
- 1891
: 1902 1 2 "Re
: 1904 3 6 lax!"
: 1910 1 4  said
: 1912 1 2  the
: 1914 2 2  night
: 1918 3 6  man.
- 1923
: 1926 1 2 "We
: 1928 3 -1  are
: 1934 1 -1  pro
: 1936 3 2 grammed
: 1940 1 2  to
: 1944 1 -1  re
: 1946 1 2 cei
: 1948 1 -1 ~
: 1950 1 -3 ve.
- 1953
: 1966 1 4 You
: 1968 1 4  can
: 1970 1 4  check
: 1972 1 4  out
: 1974 1 4  a
: 1976 2 2 ny
: 1979 3 4  time
: 1983 1 2  you
: 1986 3 6  like,
- 1991
: 1995 1 6 but
: 1997 2 6  you
: 2000 2 6  can
* 2003 2 10  ne
: 2006 1 6 ver
* 2009 1 4  leave
* 2011 4 6 ~ ."
E
