#TITLE:Murder The Government
#ARTIST:NoFX
#MP3:NoFX - Murder The Government.mp3
#COVER:NoFX - Murder The Government [CO].jpg
#BACKGROUND:NoFX - Murder The Government [BG].jpg
#BPM:260
#GAP:1510
#ENCODING:UTF8
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
: 1 3 11 I
: 5 3 11  wan
: 9 2 11 na
: 13 6 11  see
: 27 3 11  the
: 31 6 11  con
: 38 4 11 sti
: 43 5 11 tu
: 50 9 10 tion
: 62 9 11  burn
- 73
: 77 4 11 Wan
: 82 2 11 na
: 86 8 11  watch
: 95 2 11  the
: 98 7 11  white
: 106 10 11  house
: 119 3 11  o
: 123 3 8 ver
: 129 13 8 turn
- 143
: 145 4 11 Wan
: 150 2 11 na
: 154 6 11  wit
: 162 7 11 ness
: 170 7 11  some
: 179 7 11  blue
: 188 6 11  blood
: 196 6 11  bleed
: 206 13 10  red
- 221
: 256 1 15 Wan
: 258 1 15 na
: 260 3 15  tar
: 264 2 15  and
: 267 4 16  lynch
: 272 1 15  the
: 274 4 13  K
: 279 3 11 K
: 283 3 11 K
- 286
: 286 1 15 Wan
: 288 1 15 na
: 290 3 15  pull
: 294 2 15  and
: 297 4 16  shoot
: 302 2 15  the
: 305 1 13  N
: 307 5 11 R
: 313 8 11 A
- 323
: 327 5 16 Yeah
: 334 5 15  yeah
: 341 14 13  yeah...
- 357
: 375 3 15 Mur
: 379 4 15 der
: 384 2 13  the
: 387 1 15  gov
: 389 2 18 ern
: 392 8 15 ment,
- 402
: 405 3 15  Mur
: 409 4 15 der
: 414 2 13  the
: 417 1 15  gov
: 419 2 13 ern
: 422 8 11 ment
- 432
: 436 3 16 Mur
: 440 4 16 der
: 445 1 16  the
: 447 1 16  gov
: 449 2 16 ern
: 452 8 16 ment,
- 461
: 462 10 13  then
: 479 1 18  do
: 480 1 18  it
: 481 1 18  a
: 483 4 18 gain
: 489 4 15  yeah
- 494
: 496 3 15 Mur
: 500 4 15 der
: 505 2 13  the
: 508 1 15  gov
: 510 2 18 ern
: 514 8 15 ment,
- 524
: 528 3 15  Mur
: 532 4 15 der
: 537 1 13  the
: 539 1 15  gov
: 541 2 13 ern
: 544 8 11 ment
- 554
: 557 3 15 Mur
: 561 4 15 der
: 566 2 15  the
: 569 1 15  gov
: 571 2 15 ern
: 574 5 15 ment,
: 580 2 15  and
: 583 12 13  then
- 597
: 606 3 15 Mur
: 610 3 15 der
: 614 2 15  the
: 618 4 13  gov
: 625 8 15 ern
: 641 8 15 me
: 651 5 13 ~
: 657 1 13 ~
* 659 14 11 ~nt
E