#TITLE:Si tu savais
#ARTIST:Shy'm
#MP3:Shy'm - Si tu savais.mp3
#VIDEO:Shy'm - Si tu savais.mp4
#COVER:Shy'm - Si tu savais.jpg
#BPM:432
#GAP:22980
#ENCODING:UTF8
: 0 8 14 Oh
: 12 4 14  ton
: 20 4 14  re
: 28 8 14 gard
: 40 8 12  sur
: 52 8 12  moi
- 62
: 64 4 10 qui
: 72 4 10  se
: 80 8 10  pose
: 92 8 9  et
: 104 8 10  m'en
: 116 8 7 traîne -
- 126
: 128 8 14 Sur
: 140 4 14  ton
: 148 2 14  doux
: 152 4 14  vi
: 160 4 12 sage
: 168 4 12  je
: 176 4 12  de
: 184 4 12 vi
: 192 4 10 ne
- 198
: 200 4 10 les
: 208 4 10  mots,
: 216 12 9  les
: 232 8 10  po
: 244 8 7 emes -
- 254
: 256 2 10 Et
: 260 2 10  pas
: 264 4 10  be
: 272 2 10 soin
: 276 2 10  de
: 280 4 12  par
: 288 4 14 ler,
- 294
: 296 8 5 tout
: 308 4 5  est
: 316 4 15  é
: 324 8 14 crit
: 336 12 12  dans
: 352 4 14  nos
: 360 20 10  yeux -
- 382
: 384 4 10 Et
: 392 4 10  ca
: 400 4 10  me
: 408 2 12  fait
: 412 4 10  re
: 420 4 5 ver
- 436
: 440 4 10 ne
: 448 4 9  serait_ce
: 456 4 5  que
: 464 2 5  de
: 468 8 5  dire
: 480 4 5  nous
: 488 20 7  deux -
- 510
: 512 4 17 Me
: 520 6 17  vois
: 528 4 17 tu
: 536 4 17  comme
: 544 4 12  je
* 552 4 12  te
* 560 12 14  vois? -
- 574
: 576 4 12 Me
: 584 8 12  sens
: 592 4 12 tu
: 600 4 12  comme
: 608 4 10  je
: 616 8 14  te
: 628 24 14  sens? -
- 654
: 656 4 17 Fau
: 664 4 10 drait
: 672 4 12  sur
: 680 4 12 tout
: 688 4 14  rien
: 696 8 12  gâ
: 708 16 10 cher -
- 726
: 728 10 17 Ba
: 740 2 17 by
: 744 2 17  si
: 752 6 19  tu
: 764 6 17  sa
: 772 10 10 vais
: 788 2 10  comme
: 792 6 17  tu
: 804 2 17  me
: 808 2 19  fais
: 812 2 17  du
: 820 10 10  bien -
- 837
: 840 2 10 Quand
: 848 2 10  on
: 856 2 17  a
: 864 2 17  quelques
: 876 2 17  mi
: 880 2 19 nutes
: 888 6 17  vo
: 900 10 10 lées
- 917
: 920 2 22 au
: 928 2 21  quo
: 936 2 19 ti
: 944 2 17 dien
: 948 2 19  _
: 952 2 17  _ -
- 958
: 960 2 17 Je
: 968 2 14  sais
: 976 2 14  la
: 984 2 17  chance
: 992 2 14  qui
: 1000 2 17  nous
: 1008 2 19  est
: 1020 2 17  don
: 1028 2 10 née
- 1037
: 1040 2 10 de
: 1048 10 17  voir
: 1068 2 19  si
* 1076 2 17  loin
* 1080 2 19  _
* 1084 14 17  _ -
- 1105
: 1108 2 10 De
: 1116 2 22  par
: 1124 6 21 ta
: 1136 2 19 ger
: 1144 2 17  la
: 1148 2 19  route,
- 1157
: 1160 2 14 ton
: 1168 2 14  bo
: 1176 2 14 nheur
: 1184 2 15  est
: 1196 2 17  le
: 1204 2 14  mien
: 1212 18 12  _ -
- 1237
: 1240 6 14 Oh
* 1252 6 12  oh
* 1264 10 10  oh,
* 1308 2 14  oh
* 1316 6 17  oh
* 1328 10 12  oh -
- 1366
: 1408 6 14 Oh
: 1416 6 14  cette
: 1424 6 14  i
: 1432 10 14 mage
: 1444 10 12  de
: 1456 14 12  toi
: 1472 6 10  qui
: 1480 6 10  me
: 1488 6 10  suit
: 1496 10 9  ou
: 1508 10 10  je
: 1520 14 7  vais
- 1534
: 1536 6 14 Comme
: 1544 6 14  u
: 1552 6 14 ne
: 1560 6 14  gra
: 1568 6 12 vu
: 1576 6 12 re
: 1584 6 12  mar
: 1592 6 10 quée
- 1598
: 1600 6 10 pour
: 1608 6 10  ne
: 1616 6 10  pas
: 1624 10 9  ou
: 1636 10 10 bli
: 1648 10 7 er
- 1658
: 1660 2 10 Le
: 1664 6 10  par
: 1672 6 10 fum
: 1680 2 10  des
: 1684 10 12  mé
: 1696 14 14 moires
- 1710
: 1712 6 5 Quand
: 1720 6 15  tu
: 1728 6 14  es
: 1736 6 12  loin
: 1744 10 12  de
: 1756 10 14  mes
: 1768 22 10  yeux
- 1790
: 1792 6 10 L'en
: 1800 2 10 vie
: 1804 2 10  de
: 1808 2 10  te
: 1812 10 12  re
: 1824 14 10 voir
- 1838
: 1840 6 5 De
: 1848 6 10  re
: 1856 6 9 trou
: 1864 6 5 ver
: 1872 2 5  la
: 1876 10 5  vie
: 1888 6 7  a
: 1896 22 7  deux
- 1918
: 1920 2 17 Me
: 1928 6 17  vois
: 1936 2 17 tu
: 1944 2 17  comme
: 1952 2 12  je
: 1960 2 12  te
: 1968 10 14  vois?
- 1982
: 1984 2 12 Me
: 1992 6 12  sens
: 2000 2 12 tu
: 2008 2 12  comme
: 2016 2 10  je
: 2024 6 14  te
: 2036 22 14  sens?
- 2062
: 2064 6 17 Fau
: 2072 2 10 drait
: 2080 6 12  sur
: 2088 2 12 tout
: 2096 2 14  rien
: 2104 6 12  gâ
: 2112 18 10 cher
- 2134
: 2136 10 17 Ba
: 2148 2 17 by
: 2152 2 17  si
: 2160 6 19  tu
: 2172 6 17  sa
: 2180 10 10 vais
: 2196 2 10  comme
: 2200 6 17  tu
: 2212 2 17  me
: 2216 2 19  fais
: 2220 2 17  du
: 2228 10 10  bien
- 2248
: 2252 2 10 Quand
: 2256 4 10  on
: 2264 4 17  a
: 2276 2 17  quelques
: 2280 4 17  mi
: 2288 4 19 nutes
: 2296 6 17  vo
: 2308 10 10 lées
- 2325
: 2328 4 22 au
: 2336 4 21  quo
: 2344 4 19 ti
: 2352 2 17 dien
* 2356 2 19  _
* 2360 4 17  _
- 2366
: 2368 4 17 Je
: 2376 4 14  sais
: 2384 4 14  la
: 2392 4 17  chance
: 2400 4 14  qui
: 2408 4 17  nous
: 2416 4 19  est
: 2424 6 17  don
: 2436 4 10 née
- 2446
: 2448 4 10 de
: 2456 10 17  voir
: 2476 2 19  si
: 2480 2 17  loin
: 2484 2 19  _
: 2488 14 17  _
- 2509
: 2512 4 10 De
: 2520 6 22  par
: 2532 6 21 ta
: 2544 4 19 ger
: 2552 2 17  la
: 2556 4 19  route,
- 2566
: 2568 4 14 ton
: 2576 4 14  bo
: 2584 4 14 nheur
: 2592 4 15  est
: 2600 4 17  le
: 2612 2 14  mien
: 2616 22 12  _
- 2645
: 2648 4 14 Oh
: 2660 4 12  oh
: 2672 12 10  oh,
* 2712 4 14  oh
* 2724 4 17  oh
* 2736 10 12  oh
- 2756
: 2776 8 14 Oh
: 2788 8 12  oh
: 2800 14 10  oh,
: 2840 8 14  oh
: 2852 8 17  oh
: 2864 12 12  oh
- 2886
: 2904 8 14 Oh
: 2916 8 12  oh
: 2928 8 10  oh,
* 2968 8 14  oh
* 2980 8 17  oh
* 2992 14 12  oh
- 3016
: 3032 8 14 Oh
: 3044 8 12  oh
: 3056 10 10  oh,
: 3096 8 14  oh
: 3108 8 17  oh
: 3120 14 12  oh
- 3144
: 3160 10 17 Ba
: 3172 2 17 by
: 3176 2 17  si
: 3184 6 19  tu
: 3196 6 17  sa
: 3204 10 10 vais
: 3220 2 10  comme
: 3224 6 17  tu
: 3236 2 17  me
: 3240 2 19  fais
: 3244 2 17  du
: 3252 10 10  bien
- 3269
: 3272 6 10 Quand
: 3280 6 10  on
: 3288 6 17  a
: 3296 6 17  quelques
: 3304 6 17  mi
: 3312 6 19 nutes
: 3320 8 17  vo
: 3332 12 10 lées
- 3350
: 3352 6 22 au
: 3360 6 21  quo
: 3368 6 19 ti
: 3376 2 17 dien
: 3380 2 19  _
: 3384 6 17  _
- 3390
: 3392 6 17 Je
: 3400 6 14  sais
: 3408 6 14  la
: 3416 6 17  chance
- 3422
: 3424 6 14 qui
: 3432 6 17  nous
: 3440 6 19  est
: 3448 6 17  don
: 3456 8 10 née
: 3472 6 10  de
: 3480 8 17  voir
: 3496 6 19  si
: 3508 2 17  loin
: 3512 2 19  _
: 3516 12 17  _
- 3534
: 3536 6 10 De
: 3548 6 22  par
: 3556 8 21 ta
: 3568 2 19 ger
: 3572 2 17  la
: 3576 8 19  route,
- 3590
: 3592 6 14 ton
: 3600 6 14  bo
: 3608 6 14 nheur
: 3616 6 15  est
: 3624 6 17  le
: 3636 2 14  mien
: 3640 24 12  _
- 3670
: 3672 10 17 Ba
: 3684 2 17 by
: 3688 2 17  si
: 3696 6 19  tu
: 3708 6 17  sa
: 3716 10 10 vais
: 3732 2 10  comme
: 3736 6 17  tu
: 3748 2 17  me
: 3752 2 19  fais
: 3756 2 17  du
: 3764 10 10  bien
- 3781
: 3784 6 10 Quand
: 3792 6 10  on
: 3800 6 17  a
: 3808 6 17  quelques
: 3816 6 17  mi
: 3824 6 19 nutes
: 3832 10 17  vo
: 3844 18 10 lées
- 3862
: 3864 6 22 au
: 3872 6 21  quo
: 3880 6 19 ti
: 3888 2 17 dien
: 3892 2 19  _
: 3896 6 17  _
- 3902
: 3904 6 17 Je
: 3912 6 14  sais
: 3920 6 14  la
: 3928 6 17  chance
: 3936 6 14  qui
: 3944 6 17  nous
: 3952 6 19  est
: 3960 6 17  don
: 3968 14 10 née
- 3982
: 3984 6 10 de
: 3992 14 17  voir
: 4008 6 19  si
: 4016 2 17  loin
: 4020 2 19  _
: 4024 22 17  _
- 4046
: 4048 6 10 De
: 4056 10 22  par
: 4068 10 21 ta
: 4080 2 19 ger
: 4084 2 17  la
: 4088 14 19  route,
- 4102
: 4104 6 14 ton
: 4112 6 14  bo
: 4120 6 14 nheur
: 4128 6 15  est
: 4136 6 17  le
: 4144 6 14  mien
: 4152 12 12  _
E