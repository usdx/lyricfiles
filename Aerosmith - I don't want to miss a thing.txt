#TITLE:I don't want to miss a thing
#ARTIST:Aerosmith
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
#MP3:Aerosmith - I don't want to miss a thing.mp3
#COVER:Aerosmith - I don't want to miss a thing [CO].jpg
#BACKGROUND:Aerosmith - I don't want to miss a thing [BG].jpg
#VIDEO:Aerosmith - I don't want to miss a thing [VD#0].mpg
#VIDEOGAP:0
#BPM:241,8
#GAP:30870
: 2 4 9 I
: 8 2 9  could
: 13 4 6  stay
: 21 2 9  a
: 25 7 11 wake,
- 34
: 46 3 6 just
: 53 3 9  to
: 60 7 18  hear
: 71 4 14  you
: 79 5 16  brea
: 90 8 14 thing,
- 100
: 129 3 13 watch
: 134 3 14  you
: 140 5 11  smile
: 150 4 9  while
: 157 3 9  you
: 162 3 6  are
: 170 6 9  slee
: 179 3 9 ping
- 184
: 194 2 14 while
: 199 3 14  you're
: 205 5 11  far
: 215 2 9  a
: 219 6 9 way
: 228 4 6  and
: 234 6 9  drea
: 243 6 11 ming.
- 251
: 258 3 9 I
: 263 3 9  could
: 269 4 6  spend
: 276 2 9  my
: 281 6 11  life
- 289
: 301 3 6 in
: 308 3 9  this
* 317 9 18  swe
* 327 4 14 et
: 338 2 14  su
: 342 5 16 rren
: 352 6 14 der.
- 360
: 386 4 13 I
: 391 4 14  could
: 397 4 11  stay
: 405 4 9  lost
: 414 2 9  in
: 419 3 9  this
: 426 4 9  mo
: 433 6 11 ment
- 441
: 458 2 6  for
: 462 3 9 e
: 467 3 7 ~
: 473 9 11 ver.
- 484
: 509 2 11 E
: 513 2 13 very
: 517 2 14  mo
: 521 2 16 ment
: 526 6 16  spent
: 538 3 9  with
: 548 6 18  you
: 555 2 16 ~
: 557 3 14 ~
- 562
: 564 2 14 is
: 568 2 11  a
: 572 2 14  mo
: 576 2 16 ment
: 580 4 18  I
: 588 10 18  trea
* 604 40 16 sure.
- 646
: 669 2 18 Don't
: 673 2 18  wa
: 677 2 18 nna
: 681 6 18  clo
: 688 3 16 se
: 693 2 16  my
: 696 16 16  eyes,
- 714
: 728 2 16 I
: 732 2 16  don't
: 736 2 16  wa
: 740 2 16 nna
: 745 7 21  fall
: 756 2 19  a
: 760 4 18 sleep,
- 766
: 768 4 16 cause
: 774 4 14  I'd
: 780 3 16  miss
: 786 2 11  you
: 791 3 14  ba
: 795 2 14 by
- 799
: 804 2 9 and
: 808 2 9  I
: 812 6 18  don't
: 820 2 16  wa
: 824 2 14 nna
: 828 2 14  miss
: 832 2 9  a
: 836 4 16  thing
: 842 4 14 ~.
- 848
: 856 2 18 Cause
: 860 2 18  e
: 864 2 18 ven
: 868 2 18  when
: 872 2 18  I
: 876 6 18  dream
: 884 2 16  of
: 888 16 16  you,
- 906
: 920 2 16 the
: 924 2 16  swee
: 928 2 16 test
: 932 2 16  dream
: 936 2 16  would
* 941 6 21  ne
: 949 2 19 ver
: 954 4 18  do.
- 960
: 964 2 16 I'd
: 970 3 14  still
: 976 2 16  miss
: 981 2 11  you
: 985 3 14  ba
: 989 2 14 by
- 993
: 996 2 9 and
: 1000 2 9  I
: 1004 6 18  don't
: 1012 2 16  wa
: 1016 2 14 nna
: 1020 2 14  miss
: 1024 2 9  a
: 1028 6 16  thing
: 1036 4 14 ~.
- 1042
: 1155 2 9 Ly
: 1159 2 9 ing
: 1164 6 6  close
: 1173 3 9  to
: 1178 4 11  you,
- 1184
: 1196 4 6 fee
: 1202 4 9 ling
: 1210 6 18  your
: 1222 6 14  heart
: 1232 8 16  bea
: 1244 6 14 ting
- 1252
: 1280 4 13 and
: 1286 4 14  I'm
: 1292 4 11  won
: 1300 2 9 dering
: 1307 4 9  what
: 1315 4 6  you're
: 1321 4 9  drea
: 1327 4 9 ming,
- 1333
: 1348 2 14 won
: 1352 2 14 dering
: 1356 4 11  if
: 1362 2 9  it's
: 1370 4 9  me
: 1376 4 9  you're
: 1384 4 18  see
: 1390 6 16 ing.
- 1398
: 1411 2 16 Then
: 1414 2 14  I
: 1421 3 16  kiss
: 1426 2 14  your
: 1432 6 14  eyes
- 1440
: 1448 2 14 and
: 1452 4 16  thank
: 1460 6 14  God
: 1468 6 19  we're
* 1478 6 21  to
: 1490 12 18 ge
: 1506 2 16 ther
: 1510 4 14 ~.
- 1516
: 1532 2 11 I
: 1536 2 13  just
: 1540 2 14  wa
: 1544 2 16 nna
: 1548 8 16  stay
: 1562 4 21  with
: 1570 6 18  you
- 1578
: 1588 2 14 in
: 1592 2 11  this
: 1596 2 14  mo
: 1600 2 16 ment
: 1604 2 18  for
* 1608 10 18 e
: 1624 4 16 ver,
- 1630
: 1639 2 14 fo
: 1643 4 18 re
: 1649 2 16 ver
: 1653 4 14  and
: 1659 4 18  e
: 1665 2 16 ver
: 1669 6 14 ~.
- 1677
: 1686 2 16 I
: 1690 2 18  don't
: 1694 2 18  wa
: 1698 2 18 nna
: 1704 8 18  close
: 1716 2 16  my
: 1720 12 16  eyes,
- 1734
: 1750 2 16 I
: 1754 2 16  don't
: 1758 2 16  wa
: 1762 2 16 nna
: 1768 6 21  fall
: 1778 2 19  a
: 1782 6 18 sleep,
- 1790
: 1792 2 16 cause
: 1796 4 14  I'd
: 1802 4 16  miss
: 1808 4 11  you
: 1814 2 14  ba
: 1818 2 14 by
- 1822
: 1826 2 9 and
: 1830 4 9  I
: 1836 5 18  don't
: 1843 2 16  wa
: 1847 2 14 nna
: 1851 2 14  miss
: 1854 2 9  a
: 1858 6 16  thing
: 1866 4 14 ~.
- 1872
: 1879 2 18 Cause
: 1883 2 18  e
: 1886 2 18 ven
: 1890 2 18  when
: 1894 4 18  I
: 1900 6 18  dream
: 1908 2 16  of
: 1912 16 16  you,
- 1930
: 1942 2 16 the
: 1946 2 16  swee
: 1950 2 16 test
: 1954 2 16  dream
: 1958 2 16  would
: 1962 6 21  ne
: 1970 2 19 ver
: 1976 4 18  do.
- 1982
: 1984 4 16 I'd
: 1990 4 14  still
: 1996 2 16  miss
: 2002 2 11  you
: 2006 2 14  ba
: 2010 2 14 by
- 2014
: 2018 2 9 and
: 2022 2 9  I
: 2026 6 18  don't
: 2034 2 16  wa
: 2038 2 14 nna
: 2042 2 14  miss
: 2046 2 9  a
: 2050 6 16  thing
: 2058 4 14 ~.
- 2064
: 2074 2 14 I
: 2078 2 14  don't
: 2082 2 14  wa
: 2086 2 14 nna
: 2090 4 16  miss
: 2098 4 16  one
* 2106 14 16  smile
: 2124 2 14 ~
: 2126 4 12 ~.
- 2132
: 2138 2 16 I
: 2142 2 16  don't
: 2146 2 16  wa
: 2150 2 14 nna
: 2154 4 14  miss
: 2162 6 16  one
: 2173 6 11  kiss.
- 2181
: 2202 4 14 I
: 2208 2 14  just
: 2212 2 14  wa
: 2216 2 12 nna
: 2220 4 14  be
: 2226 2 12  with
: 2232 4 14  you,
- 2237
: 2238 4 16 right
: 2246 8 17  here
: 2258 2 16  with
: 2264 6 14  you,
- 2272
: 2282 4 12  just
: 2290 4 12  like
: 2298 12 12  this.
- 2312
: 2330 2 17 I
: 2334 2 17  just
: 2338 2 17  wa
: 2342 2 17 nna
: 2346 6 17  ho
: 2353 3 16 ld
: 2361 3 16  you
: 2367 5 16  clo
: 2374 2 14 ~
: 2378 4 12 se.
- 2384
: 2390 2 16 I
: 2394 2 16  feel
: 2398 2 19  your
: 2402 2 19  heart
: 2406 2 16  so
: 2412 4 19  close
: 2420 2 16  to
: 2424 6 21  mine
: 2432 12 19 ~.
- 2446
: 2462 4 14 And
: 2468 2 16  just
: 2474 4 17  stay
: 2480 4 17  here
: 2488 6 19  in
: 2498 2 17  this
: 2504 4 16  mo
: 2510 4 14 ment
- 2516
: 2522 4 12 for
: 2528 2 14  all
: 2532 2 16  the
: 2538 6 16  rest
: 2546 2 21  of
* 2552 34 21  time.
- 2588
: 2602 4 21 Yeah,
: 2610 4 21  yeah,
: 2618 4 21  yeah,
: 2626 4 21  yeah,
* 2635 30 26  yeah!
- 2667
: 2682 2 18 Don't
: 2686 2 18  wa
: 2690 2 18 nna
: 2695 5 18  clo
: 2701 2 16 se
: 2706 2 16  my
: 2710 14 16  eyes,
- 2726
: 2746 2 16  don't
: 2750 2 16  wa
: 2754 2 16 nna
: 2758 6 21  fa
: 2765 2 19 ll
: 2770 2 19  a
: 2774 4 18 sleep,
- 2780
: 2784 2 16 cause
: 2788 4 14  I'd
: 2794 4 16  miss
: 2800 4 11  you
: 2806 2 14  ba
: 2810 2 14 by
- 2814
: 2818 2 9 and
: 2822 2 9  I
: 2826 6 18  don't
: 2834 2 16  wa
: 2838 2 14 nna
: 2842 2 14  miss
: 2846 2 9  a
: 2850 4 16  thing
: 2857 3 14 ~.
- 2862
: 2870 2 18 Cause
: 2874 2 18  e
: 2878 2 18 ven
: 2882 2 18  when
: 2886 2 18  I
: 2890 6 18  dream
: 2898 2 16  of
: 2902 16 16  you,
- 2920
: 2934 2 16 the
: 2938 2 16  swee
: 2942 2 16 test
: 2946 2 16  dream
: 2950 4 16  would
: 2956 6 21  ne
: 2964 2 19 ver
: 2968 4 18  do.
- 2974
: 2976 4 16 I'd
: 2982 4 14  still
: 2988 4 16  miss
: 2994 4 11  you
: 3000 2 14  ba
: 3004 2 14 by
- 3008
: 3010 2 9 and
: 3014 2 9  I
: 3018 6 18  don't
: 3026 2 16  wa
: 3030 2 14 nna
: 3034 2 14  miss
: 3038 2 9  a
: 3042 4 16  thing
: 3048 4 14 ~.
- 3054
: 3060 4 18 I
: 3066 2 21  don't
: 3070 2 21  wa
: 3074 2 18 nna
: 3078 8 21  close
: 3090 4 18  my
* 3096 4 23  eyes
: 3102 4 21 ~,
- 3108
: 3126 2 18 I
: 3130 2 21  don't
: 3134 2 21  wa
: 3138 2 18 nna
: 3142 8 21  fall
: 3154 2 19  a
: 3158 4 18 sleep,
- 3164
: 3168 2 16 cause
: 3172 4 14  I'd
: 3179 5 16  miss
: 3186 2 11  you
: 3190 2 14  ba
: 3194 2 14 by
- 3198
: 3202 2 9 and
: 3206 2 9  I
: 3210 4 18  don't
: 3218 2 16  wa
: 3222 2 14 nna
: 3226 2 14  miss
: 3230 2 9  a
: 3234 2 16  thing
: 3238 4 14 ~.
- 3244
: 3254 2 14 Cause
: 3258 2 18  e
: 3262 2 18 ven
: 3266 2 19  when
: 3270 2 21  I
* 3274 8 26  dream
: 3288 4 26  of
: 3294 6 26  you,
- 3302
: 3318 2 26 the
: 3324 2 26  swee
: 3328 2 24 test
: 3334 2 21  dream
: 3338 2 19  would
: 3342 2 19  ne
: 3344 2 21 ver
: 3348 4 18  do.
- 3354
: 3360 4 14 I'd
: 3366 2 14  still
: 3371 3 18  miss
: 3376 3 11  you
: 3381 2 14  ba
: 3385 2 14 by
- 3389
: 3394 2 14 and
: 3398 2 14  I
: 3402 6 21  don't
: 3410 2 19  wa
: 3414 2 19 nna
: 3418 2 19  miss
: 3422 2 19  a
: 3426 6 18  thi
* 3433 10 26 --ng
- 3445
: 3450 4 26 Don't
: 3458 2 19  wa
: 3462 2 18 nna
: 3466 4 19  close
: 3472 4 18  my
: 3478 8 21  eyes,
- 3488
: 3502 2 26 I
: 3506 4 21  don't
: 3512 2 26  wa
: 3516 2 21 nna
* 3522 26 26  fall
: 3554 2 21  a
: 3560 6 21 sleep,
: 3572 6 19  yeah.
- 3580
: 3588 4 14 I
: 3594 4 18  don't
: 3600 2 14  wa
: 3604 2 11 nna
: 3610 2 14  miss
: 3614 2 14  a
: 3619 6 14  thing.
E
