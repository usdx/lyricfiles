#TITLE:Die Flut (Witt)
#ARTIST:Witt ft. Heppner
#MP3:Witt feat. Heppner - Die Flut.mp3
#VIDEO:Witt feat. Heppner - Die Flut.mp4
#COVER:Witt feat. Heppner - Die Flut[CO].jpg
#BPM:220
#GAP:15560
#MedleyStartBeat:1873
#MedleyEndBeat:2143
#ENCODING:UTF8
#PREVIEWSTART:142,005
#LANGUAGE:German
#GENRE:Pop
#EDITION:UltraStar '90s
#YEAR:1998
: 0 3 4 Wenn
: 4 3 4  ich
: 8 4 4  in 
: 14 6 4 mir
: 21 6 3  kei
: 29 3 3 ne
: 33 7 6  Ru
: 41 3 3 he
: 45 6 3  fühl'
- 53
: 60 3 4 Bit
: 64 3 4 ter
: 68 5 4 keit
: 75 7 4  mein 
: 83 7 3 dunk
: 91 3 4 les 
: 95 9 6 Herz
: 106 6 3  um
: 112 7 4 spült.
- 120
: 122 3 4 Ich
: 126 3 4  nur
: 130 6 8  war
: 137 7 4 te
: 151 4 3  auf
: 156 3 3  den
: 160 5 6  näch
: 166 4 6 sten
: 171 8 3  Tag,
- 181
: 186 4 1 der
: 191 5 8  mir
: 197 3 11  er
: 201 32 9 wacht.
- 235
: 244 3 4 Wenn
: 248 3 6  die
: 252 4 8  Fin
: 258 4 4 ster
: 263 8 6 nis
: 274 3 8  den
: 278 6 8  kla
: 285 2 6 ren
: 288 2 8  Blick
: 291 4 4  ver
: 296 6 6 hüllt,
- 303
: 304 5 8 Kein
: 312 7 8  Sinn
: 320 6 4  mehr
: 334 3 3  ei
: 338 3 4 ne
: 342 6 6  Sehn
: 350 6 8 sucht
: 358 8 6  stillt,
- 368
: 374 4 6 ruf'
: 380 6 4  ich
: 388 6 3  mir
: 396 4 3  her
: 401 9 4 bei
: 415 4 6  den
: 420 4 8  ei
: 426 3 6 nen
: 431 11 6  Traum,
- 444
: 446 3 8 der
: 450 15 9  sich
: 476 4 8  nie
: 481 8 4 mals
: 492 3 4  er
: 496 21 4 füllt.
- 519
: 772 3 16 Wann
: 776 3 18  kommt
: 780 3 20  die
: 784 8 20  Flut
- 794
: 802 6 18 Ü
: 809 2 16 ber
: 813 12 16  mich?
- 827
: 832 3 16 Wann
: 836 3 18  kommt
: 840 3 20  die
: 844 8 20  Flut,
- 854
: 858 5 20 die
: 865 4 18  mich
: 871 5 16  be
: 878 10 16 rührt?
- 890
: 894 3 16 Wann
: 898 3 18  kommt
: 902 3 20  die
: 906 10 20  Flut,
- 918
: 920 4 20 die
: 925 5 18  mich
: 931 4 16  mit
: 936 6 18  fort
: 944 10 16 nimmt
- 956
: 960 3 15 in
: 964 3 16  ein
: 968 5 18  an
: 974 6 18 dres
: 986 6 18  gro
: 993 2 20 ßes
: 997 5 20  Le
: 1004 16 18 ben
- 1020
: 1020 3 20 ir
: 1024 3 16 gend
: 1028 18 16 wo.
- 1048
: 1101 3 4 All'
: 1105 2 4  die
: 1108 6 4  Zeit
: 1119 3 4  so
: 1123 6 3  schnell
: 1130 4 6  vor
: 1135 7 8 ü
: 1143 3 6 ber
: 1147 5 4 zieht
- 1154
: 1162 3 6 Je
: 1166 2 4 de
: 1169 6 4  Spur
: 1177 6 4  von
: 1185 6 3  mir
: 1192 5 3  wie
: 1199 7 3  Staub
: 1208 4 3  zer
: 1214 8 4 fliegt
- 1223
: 1223 3 4 End
: 1227 3 4 los
: 1231 6 4  weit
: 1239 4 4  ge
: 1246 4 8 trie
: 1252 16 8 ben
- 1270
: 1272 3 8 Von
: 1276 6 6  un
: 1284 6 6 sicht
: 1292 7 6 ba
: 1300 3 4 rer
: 1304 24 9  Hand
- 1330
: 1346 3 4 Gibt
: 1350 3 4  es
: 1354 4 4  dort
: 1365 3 4  am
: 1369 5 6  kal
: 1375 4 4 ten
: 1380 6 3  Fir
: 1387 3 4 ma
: 1392 6 3 ment
- 1400
: 1403 3 4 Nicht
: 1407 3 4  auch
: 1411 6 4  den
: 1418 5 4  Stern
: 1425 4 4  der
: 1430 5 8  nur
: 1436 3 8  für
: 1440 6 8  mich
: 1448 2 9  ver
: 1452 6 8 brennt
- 1460
: 1472 3 4 Ein
: 1476 6 4  dump
: 1483 6 4 fes
: 1490 6 8  Leuch
: 1497 6 8 ten
- 1505
: 1510 3 8 Wie
: 1514 6 8  ein
: 1521 4 11  Feu
: 1526 6 9 er
: 1538 5 8  in
: 1544 3 6  der
: 1549 24 4  Nacht
- 1575
: 1579 3 6 Das
: 1583 4 8  nie 
: 1590 4 4 ver
: 1595 34 4 geht
- 1631
: 1873 3 16 Wann
: 1877 3 18  kommt
: 1881 2 20  die
: 1885 6 20  Flut
- 1893
: 1905 7 18 Ü
: 1913 2 16 ber
: 1917 9 16  mich?
- 1928
: 1934 3 16 Wann
: 1938 3 18  kommt
: 1942 2 20  die
: 1946 6 20  Flut,
- 1954
: 1962 3 20 die
: 1966 5 18  mich
: 1974 5 16  be
: 1980 10 16 rührt?
- 1992
: 1996 3 16 Wann
: 2000 3 18  kommt
: 2004 2 20  die
: 2008 10 20  Flut,
- 2020
: 2022 3 20 die
: 2026 6 18  mich
: 2033 3 16  mit
: 2037 7 18  fort
: 2046 10 16 nimmt
- 2058
: 2062 3 15 in
: 2066 3 16  ein
: 2070 6 18  an
: 2077 8 18 dres
: 2087 6 18  gro
: 2094 3 20 ßes
: 2099 6 20  Le
: 2107 10 18 ben
- 2118
: 2120 3 16 Wann
: 2124 3 18  kommt
: 2128 2 20  die
: 2131 12 20  Flut
- 2145
: 2151 6 18 Ü
: 2158 3 16 ber
: 2163 9 16  mich?
- 2174
: 2180 3 16 Wann
: 2184 3 18  kommt
: 2188 2 20  die
: 2192 8 20  Flut,
- 2202
: 2206 3 20 die
: 2210 5 18  mich
: 2217 6 16  be
: 2225 10 16 rührt?
- 2237
: 2240 3 16 Wann
: 2244 3 18  kommt
: 2248 2 20  die
: 2252 10 20  Flut,
- 2264
: 2268 3 20 die
: 2272 7 18  mich
: 2280 3 16  mit
: 2284 6 18  fort
: 2292 10 16 nimmt
- 2304
: 2306 3 15 in
: 2310 3 16  ein
: 2314 6 18  an
: 2322 6 18 dres
: 2332 6 18  gro
: 2339 4 20 ßes
: 2344 6 20  Le
: 2352 10 18 ben
- 2364
: 2368 3 20 ir
: 2372 3 16 gend
: 2376 36 16 wo
- 2414
: 2612 3 18 Doch
: 2616 3 18  es
: 2620 3 18  wird
: 2624 3 23  kei
: 2628 5 23 ne
: 2635 7 23  an
: 2644 3 25 dre
: 2650 6 25  ge
: 2658 12 23 ben
- 2670
: 2670 3 16 Wann
: 2674 3 18  kommt
: 2678 2 20  die
: 2682 6 20  Flut
- 2690
: 2702 6 18 Ü
: 2709 3 16 ber
: 2713 10 16  mich?
- 2725
: 2730 3 16 Wann
: 2734 3 18  kommt
: 2738 2 20  die
: 2742 8 20  Flut,
- 2752
: 2757 3 20 die
: 2761 6 18  mich
: 2769 5 16  be
: 2776 10 16 rührt?
- 2788
: 2794 3 16 Wann
: 2798 3 18  kommt
: 2802 2 20  die
: 2806 10 20  Flut,
- 2818
: 2820 3 20 die
: 2824 6 18  mich
: 2831 4 16  mit
: 2836 7 18  fort
: 2844 8 16 nimmt
- 2854
: 2858 3 15 in
: 2862 3 16  ein
: 2866 7 18  an
: 2874 6 18 dres
: 2883 6 18  gro
: 2890 4 20 ßes
: 2895 6 20  Le
: 2903 10 18 ben
- 2914
: 2914 3 16 Wann
: 2918 3 18  kommt
: 2922 2 20  die
: 2926 12 23  Flut
- 2940
: 2976 3 16 Wann
: 2980 2 18  kommt
: 2984 2 20  die
: 2988 16 23  Flut,
- 3006
: 3038 3 16 Wann
: 3042 2 18  kommt
: 3046 2 20  die
: 3050 12 23  Flut,
- 3064
: 3103 3 15 in
: 3107 3 17  ein
: 3111 6 18  an
: 3118 7 18 dres
: 3128 6 18  gro
: 3136 3 20 ßes
: 3141 6 20  Le
: 3149 10 18 ben
- 3159
: 3159 3 16 Wann
: 3163 2 18  kommt
: 3167 2 20  die
: 3171 12 23  Flut
E