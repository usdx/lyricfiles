#TITLE:Porzellan
#ARTIST:Farin Urlaub
#MP3:Farin Urlaub - Porzellan.mp3
#VIDEO:Farin Urlaub - Porzellan [VD#0].flv
#COVER:Farin Urlaub - Porzellan [CO].jpg
#BPM:297,5
#GAP:17330
#VIDEOGAP:0
#ENCODING:UTF8
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
: 0 3 -4  Das
: 4 4 5  Glück
: 11 3 5  ist
: 15 3 3  im
: 19 3 1 mer
: 23 4 5  da
- 29
: 80 4 -4  wo
: 87 3 -2  du
: 91 4 0  nicht
: 99 4 -2  bist.
- 105
: 130 3 -4  Du
: 134 3 5  willst
: 138 3 3  im
: 142 3 1 mer
: 146 6 5  das
- 154
: 202 4 -4  was
: 210 3 -2  du
: 214 5 0  nicht
: 221 5 -2  kriegst
- 228
: 251 3 -2  und
: 255 4 5  du
: 263 3 5  be
: 267 5 5 klagst
: 275 6 1  dich
- 283
: 313 2 1  es
: 317 3 5  ist
: 321 4 5  nicht
: 328 3 5  fair
: 332 4 3 ~.
- 338
: 377 3 5  Schön
: 381 3 3  ist
: 385 3 1  nur
: 389 5 5  das
- 396
: 446 5 -4  was
: 454 4 -2  du
: 461 3 0  ver
: 465 6 -2 passt.
- 473
: 496 2 -4  Du
: 499 4 5  brauchst
: 504 3 3  ir
: 508 3 1 gend
: 512 5 5 was
- 519
: 568 5 -4  was
: 576 3 -2  du
: 580 5 0  nicht
: 588 6 -2  hast.
- 596
: 610 3 -4  Du
: 614 4 -2  bist
: 621 5 5  nie
: 629 3 5  zu
: 633 5 5 frie
: 640 4 1 den
- 646
: 671 2 -4  du
: 675 5 -2  willst
: 683 3 5  im
: 687 4 5 mer
: 694 3 5  mehr
: 698 5 3 ~.
- 705
: 732 3 5  Du
: 736 4 5  wärst
: 744 5 5  gern
: 751 3 8  wie
: 755 6 5  sie
- 763
: 792 3 5  du
: 797 4 5  wärst
: 805 5 5  gern
: 812 3 8  wie
: 816 7 5  er.
- 825
: 854 3 5  Du
: 858 4 5  wärst
: 865 3 5  gern
: 869 3 3  je
: 873 3 1 mand
: 877 6 5  an
: 885 5 1 ders
- 892
: 908 5 5  Haupt
: 916 5 5 sa
: 923 3 3 che
: 927 6 5  ir
: 935 3 3 gend
: 939 4 5 wer.
- 945
: 980 5 5  Glück
: 988 5 5  gibt
: 995 3 6  es
: 1000 5 5  ü
: 1007 6 1 ber
: 1015 5 1 all
- 1022
: 1056 3 5  viel
: 1060 5 5 leicht
: 1068 5 3  auch
* 1076 7 6  hier.
- 1085
: 1102 3 -4  Es
: 1106 5 5  liegt
: 1114 5 3  an
: 1122 7 5  dir.
- 1131
: 1350 3 5  Du
: 1354 4 5  siehst
: 1361 3 5  die
: 1366 5 3  An
: 1374 6 5 dern
- 1382
: 1418 3 -4  und
: 1422 5 -4  dich
: 1430 5 -2  packt
: 1437 3 0  die
: 1441 6 -2  Wut:
- 1449
: 1468 3 -4  War
: 1472 3 -2 um
: 1476 3 5  geht
: 1480 3 3  es
: 1484 3 1  dir
: 1488 6 5  schlecht
- 1496
: 1545 4 -4  und
: 1552 3 -2  de
: 1556 5 0 nen
: 1564 6 -2  gut?
- 1572
: 1594 3 -4  Du
: 1598 4 5  fühlst
: 1605 3 5  dich
: 1609 5 5  ein
: 1617 7 1 sam
- 1626
: 1655 3 -4  du
: 1659 5 5  fühlst
: 1666 5 5  dich
: 1674 5 3  leer.
- 1681
: 1709 2 -4  Du
: 1712 2 -4  gehst
: 1716 2 -4  an
: 1720 2 5  so
: 1724 3 3  vie
: 1728 2 1 len
: 1732 3 5  Din
: 1738 6 1 gen
- 1746
: 1789 4 -4  acht
: 1796 6 -2 los
: 1804 3 0  vor
: 1808 5 -2 bei.
- 1815
: 1831 3 -4  Für
: 1835 2 5  im
: 1838 3 5 mer
: 1842 3 5  Skla
: 1846 3 3 ve
: 1850 3 1  der
: 1854 8 5  Angst
- 1864
: 1911 4 -4  nie
: 1918 4 -2  wirk
: 1923 4 0 lich
: 1930 6 -2  frei.
- 1938
: 1956 5 5  Mach
: 1964 5 5  dir
: 1972 3 6  das
: 1976 5 5  Le
: 1983 7 1 ben
- 1992
: 2032 5 5  doch
: 2040 4 5  nicht
: 2045 3 3  so
* 2052 9 6  schwer.
- 2063
: 2074 3 5  Du
: 2078 5 5  wärst
: 2086 5 5  gern
: 2094 3 3  wie
: 2098 6 5  sie
- 2106
: 2136 3 5  du
: 2140 4 5  wärst
: 2148 5 5  gern
: 2155 3 8  wie
: 2159 6 5  er.
- 2167
: 2197 3 5  Du
: 2201 3 5  wärst
: 2205 3 5  so
: 2209 3 5  gern
: 2213 3 3  je
: 2217 3 1 mand
: 2221 5 5  an
: 2228 7 1 ders
- 2237
: 2250 6 5  Haupt
: 2258 5 5 sa
: 2265 3 3 che
: 2269 6 5  ir
: 2277 3 3 gend
: 2281 5 5 wer.
- 2288
: 2322 6 5  Glück
: 2329 5 5  gibt
: 2337 3 6  es
: 2342 6 5  ü
: 2350 6 1 ber
: 2358 6 1 all
- 2366
: 2399 3 5  be
: 2403 5 5 stimmt
: 2411 5 3  auch
: 2418 8 6  hier.
- 2428
: 2444 3 3  Es
: 2448 5 5  liegt
: 2456 5 3  an
: 2464 8 5  dir.
- 2474
: 2807 3 -4  Viel
: 2811 3 5 leicht
: 2818 3 5  wirst
: 2822 3 3  du's
: 2826 3 1  be
: 2830 6 5 grei
: 2838 4 1 fen
- 2844
: 2895 3 -2  ir
: 2900 5 0 gend
: 2907 4 -2 wann
- 2913
: 2937 2 -4  und
: 2940 3 5  wenn's
: 2944 3 3  so
: 2948 3 1  weit
: 2952 6 5  ist
- 2960
: 2993 3 -4  bit
: 2997 3 -4 te
: 3001 5 5  denk
: 3008 4 3  dar
: 3013 5 1 an:
- 3020
: 3056 4 5  Glück
: 3063 5 5  ist
: 3071 3 6  zer
: 3075 6 5 brech
: 3083 7 1 lich
- 3092
: 3116 3 -4  fass
: 3120 3 -4  es
: 3124 3 5  vor
: 3128 3 3 sich
: 3132 3 1 tig
: 3136 5 3  an
- 3143
: 3178 4 -4  wie
: 3186 6 5  Por
: 3193 4 8 zel
: 3198 7 5 lan.
- 3207
: 3540 3 5  Du
: 3544 5 5  wärst
: 3552 4 5  gern
: 3560 3 3  wie
: 3564 5 5  sie
- 3571
: 3575 3 3  (wie
: 3579 5 5  sie
: 3590 3 3  wie
: 3594 5 5  sie)
- 3600
: 3601 4 5  du
: 3606 4 5  wärst
: 3613 5 5  gern
: 3619 4 8  wie
* 3624 7 10  er.
- 3633
: 3662 3 5  Du
: 3666 3 5  wärst
: 3670 3 5  so
: 3674 3 5  gern
: 3678 3 3  je
: 3682 3 1 mand
: 3686 6 5  an
: 3693 4 1 ders
- 3699
: 3716 5 5  Haupt
: 3723 6 5 sa
: 3731 3 3 che
: 3735 5 5  ir
: 3742 3 3 gend
: 3746 6 5 wer.
- 3754
: 3787 6 5  Glück
: 3795 5 5  gibt
: 3803 3 6  es
: 3807 6 5  ü
: 3815 6 1 ber
: 3823 5 1 all
- 3830
: 3864 4 5  be
: 3869 5 5 stimmt
: 3876 6 3  auch
* 3884 8 6  hier...
E