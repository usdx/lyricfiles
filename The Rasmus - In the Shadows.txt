#TITLE:In the Shadows
#ARTIST:The Rasmus
#MP3:The Rasmus - In the Shadows.mp3
#COVER:The Rasmus - In the Shadows [CO].jpg
#BACKGROUND:The Rasmus - In the Shadows [BG].jpg
#BPM:212,04
#GAP:12800
#ENCODING:UTF8
: 0 4 69 Oh
: 4 2 66  oh
: 8 4 69  oh
: 12 13 66  oh,
- 27
: 32 4 69 oh
: 36 2 66  oh
: 40 4 69  oh
: 44 21 66  oh
- 67
: 128 4 69 Oh
: 132 2 66  oh
: 136 4 69  oh
: 140 13 66  oh,
- 155
: 160 4 69 oh
: 164 2 66  oh
: 168 4 69  oh
: 172 5 66  oh
: 177 4 64 -
: 181 10 61 -
- 193
: 260 8 54 No
: 268 18 62  sleep,
- 288
: 313 3 57 no
: 316 4 56  sleep
: 320 4 57  un
: 324 4 56 til
: 328 4 54  I'm
: 332 6 50  done
- 339
: 341 3 50 with
: 344 8 59  find
: 352 4 57 ing
: 356 4 56  the
: 360 8 56  ans
: 368 12 57 wer
- 382
: 390 6 54 Won't
: 396 25 62  stop,
: 441 3 57  won't
: 444 4 56  stop
: 448 4 57  be
: 452 4 56 fore
: 456 4 54  I
: 460 8 50  find
- 468
: 468 4 50 the
: 472 8 59  cure
: 480 4 57  for
: 484 4 56  this
: 488 8 56  can
: 496 13 57 cer
- 511
: 524 6 64 Some
: 530 27 66 times
: 568 4 61  I
: 572 4 61  feel
: 576 4 61  like
: 580 4 61  go
: 584 4 61 ing
: 588 8 62  down
- 596
: 596 4 62 I'm
: 600 8 62  so
: 608 4 61  dis
: 612 4 59 con
: 616 8 59 nec
: 624 13 61 ted
- 639
: 652 6 64 Some
: 658 14 66 how
: 672 2 64 -
: 674 8 61 -
: 697 4 61  I
: 701 4 61  know
: 705 4 61  that
: 709 4 61  I
: 713 4 61  am
- 718
: 718 6 62 haun
: 724 8 61 ted
: 732 4 59  to
: 736 4 57  be
: 740 4 56 -
: 744 8 56  wan
: 752 13 54 ted
- 767
: 769 3 69 I've
: 772 4 66  been
: 776 4 69  wat
: 780 18 66 ching
- 799
: 800 4 69 I've
: 804 4 66  been
: 808 4 69  wait
: 812 6 66 ing
: 818 2 64 -
: 820 12 61 -
- 832
: 832 4 59 In
: 836 4 61  the
: 840 5 61  sha
: 845 16 62 dows
- 862
: 864 4 54 For
: 868 4 57  my
: 872 8 59  time
: 880 2 57 -
: 882 10 54 -
- 894
: 896 4 69 I've
: 900 4 66  been
: 904 4 69  sear
: 908 16 66 ching
- 926
: 928 4 69 I've
: 932 4 66  been
: 936 4 69  liv
: 940 6 66 ing
: 946 2 64 -
: 948 10 61 -
- 959
: 960 4 59 For
: 964 4 61  to
: 968 4 61 mo
: 972 15 62 rrows
- 989
: 992 8 64 All
: 1000 4 62  my
: 1004 6 62  life
: 1010 14 61 -
- 1024
: 1024 4 69 Oh
: 1028 4 66  oh
: 1033 4 69  oh
: 1037 12 66  oh,
- 1051
: 1056 4 69 oh
: 1060 5 66  oh
: 1065 4 69  oh
: 1069 18 66  oh
- 1088
: 1089 4 59 In
: 1093 3 61  the
: 1096 5 61  sha
: 1101 20 62 dows
- 1123
: 1152 4 69 Oh
: 1156 4 66  oh
: 1160 4 69  oh
: 1164 14 66  oh,
- 1180
: 1184 4 69 oh
: 1188 4 66  oh
: 1193 3 69  oh
: 1196 6 66  oh
: 1202 3 64 -
: 1205 11 61 -
- 1217
: 1217 3 59 In
: 1220 4 61  the
: 1224 4 61  sha
: 1228 21 62 dows
- 1251
: 1284 8 54 They
: 1292 24 62  say
: 1337 4 57  that
: 1341 4 56  I
: 1345 4 57  must
: 1349 4 56  learn
- 1353
: 1353 4 54 to
: 1357 7 50  kill
: 1364 4 50  be
: 1368 8 59 fore
: 1376 4 57  I
: 1380 4 56  can
: 1384 8 56  feel
: 1392 14 57  safe
- 1408
: 1413 3 54 But
: 1416 28 62  I,
: 1465 4 57  I'd
: 1469 4 56  ra
: 1473 4 57 ther
: 1477 4 56  kill
: 1481 4 54  my
: 1485 7 50 self
- 1493
: 1493 4 50 than
: 1497 7 59  turn
: 1504 4 57  in
: 1508 4 56  to
: 1512 8 56  their
: 1520 17 54  slave
- 1539
: 1549 5 64 Some
: 1554 25 66 times
: 1593 4 61  I
: 1597 4 61  feel
: 1601 4 61  that
: 1605 4 61  I
: 1609 4 61  should
: 1613 8 62  go
- 1621
: 1621 3 62 and
: 1624 8 62  play
: 1632 4 61  with
: 1636 5 59  the
: 1641 7 59  thun
: 1648 11 61 der
- 1661
: 1677 5 64 Some
: 1682 26 66 how
: 1721 4 61  I
: 1725 4 61  just
: 1729 4 61  don't
: 1733 4 61  wan
: 1737 4 61 na
: 1741 7 62  stay
- 1749
: 1749 7 61 and
: 1756 4 59  wait
: 1760 4 57  for
: 1764 4 56  a
: 1768 8 56  won
: 1776 11 54 der
- 1789
: 1792 4 69 I've
: 1796 4 66  been
: 1800 4 69  wat
: 1804 19 66 ching
- 1824
: 1824 4 69 I've
: 1828 4 66  been
: 1832 4 69  wait
: 1836 6 66 ing
: 1842 3 64 -
: 1845 8 61 -
- 1854
: 1856 4 59 In
: 1860 4 61  the
: 1864 5 61  sha
: 1869 17 62 dows
- 1887
: 1888 4 54 For
: 1892 4 57  my
: 1896 8 59  time
: 1904 2 57 -
: 1906 11 54 -
- 1919
: 1921 4 69 I've
: 1925 3 66  been
: 1928 4 69  sear
: 1932 18 66 ching
- 1951
: 1952 4 69 I've
: 1956 4 66  been
: 1960 4 69  liv
: 1964 6 66 ing
: 1970 3 64 -
: 1973 8 61 -
- 1982
: 1984 4 59 For
: 1988 4 61  to
: 1992 5 61 mor
: 1997 13 62 rows
- 2012
: 2017 7 64 All
: 2024 4 62  my
: 2028 6 62  life
: 2034 13 61 -
: 2047 3 60 -
: 2050 9 59 -
- 2060
: 2060 6 61 Late
: 2066 7 62 ly
: 2077 5 61  I've
: 2082 7 62  been
: 2092 6 61  walk
: 2098 7 62 ing,
- 2107
: 2109 5 61 walk
: 2114 6 62 ing
: 2120 4 59  in
: 2124 6 59  cir
: 2130 7 61 cles,
- 2138
: 2140 6 59 watch
: 2146 7 61 ing,
: 2156 6 59  wait
: 2162 6 61 ing
: 2168 4 57  for
: 2172 6 59  some
: 2178 7 61 thing
- 2187
: 2189 5 61 Feel
: 2194 7 62  me,
: 2205 5 61  touch
: 2210 7 62  me,
: 2221 5 61  heal
: 2226 7 62  me,
- 2234
: 2236 6 61 come
: 2242 6 62  take
: 2248 4 59  me
: 2252 6 59  hi
: 2258 32 61 gher
- 2292
: 2321 3 69 I've
: 2324 4 66  been
: 2328 4 69  wat
: 2332 17 66 ching
- 2350
: 2352 4 69 I've
: 2356 4 66  been
: 2360 4 69  wait
: 2364 6 66 ing
: 2370 3 64 -
: 2373 9 61 -
- 2383
: 2384 4 59 In
: 2388 4 61  the
: 2392 4 61  sha
: 2396 18 62 dows
- 2415
: 2416 4 54 For
: 2420 4 57  my
: 2424 8 59  time
: 2432 2 57 -
: 2434 11 54 -
- 2447
: 2449 3 69 I've
: 2452 4 66  been
: 2456 4 69  sear
: 2460 17 66 ching
- 2478
: 2480 4 69 I've
: 2484 4 66  been
: 2488 4 69  liv
: 2492 6 66 ing
: 2498 2 64 -
: 2500 9 61 -
- 2510
: 2512 5 59 For
: 2517 3 61  to
: 2520 4 61 mor
: 2524 14 62 rows
- 2540
: 2544 9 64 All
: 2553 4 62  my
: 2557 5 62  life
: 2562 14 61 -
- 2576
: 2576 4 69 I've
: 2580 4 66  been
: 2584 4 69  wat
: 2588 32 66 ching
- 2622
: 2640 4 69 I've
: 2644 4 66  been
: 2648 4 69  wait
: 2652 32 66 ing
- 2686
: 2704 4 69 I've
: 2708 4 66  been
: 2712 4 69  sear
: 2716 32 66 ching
- 2750
: 2769 4 69 I've
: 2773 4 66  been
: 2777 4 69  liv
: 2781 17 66 ing
- 2799
: 2800 5 64 For
: 2805 3 62  to
: 2808 5 62 mor
: 2813 16 61 rows
- 2830
: 2832 4 69 Oh
: 2836 3 66  oh
: 2840 4 69  oh
: 2844 12 66  oh,
- 2858
: 2865 3 69 oh
: 2868 3 66  oh
: 2872 4 69  oh
: 2876 17 66  oh
- 2895
: 2897 4 59 In
: 2901 3 61  the
: 2904 4 61  sha
: 2908 20 62 dows
- 2930
: 2961 4 69 Oh
: 2965 3 66  oh
: 2969 4 69  oh
: 2973 12 66  oh,
- 2987
: 2993 3 69 oh
: 2996 3 66  oh
: 3000 4 69  oh
: 3004 6 66  oh
: 3010 2 64 -
: 3012 10 61 -
- 3023
: 3025 3 59 In
: 3028 4 61  the
: 3032 4 61  sha
: 3036 20 62 dows
- 3058
: 3088 4 69 I've
: 3092 4 66  been
: 3096 4 69  wait
: 3100 38 66 ing!
E