#TITLE:Night Crawler
#ARTIST:Judas Priest
#MP3:Judas Priest - Night Crawler.mp3
#COVER:Judas Priest - Night Crawler [CO].jpg
#BACKGROUND:Judas Priest - Night Crawler [BG].jpg
#BPM:194,2
#GAP:33800
#ENCODING:UTF8
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
: 0 8 18 How
: 8 7 19 ling
: 16 6 18  winds
: 24 5 16  keep
: 32 7 18  screa
: 40 6 11 ming
: 48 9 14  round
- 59
: 64 7 13 And
: 72 7 14  the
: 80 7 16  rain
: 88 7 13  comes
: 96 2 14  pou
: 98 2 13 ring
: 100 8 11  down
- 110
: 128 8 18 Doors
: 137 6 19  are
: 145 6 18  locked
: 152 7 16  and
: 160 5 18  bol
: 168 6 11 ted
: 176 9 14  now
- 187
: 192 7 13 As
: 200 6 14  the
: 208 7 16  thing
: 216 7 13  crawls
: 224 2 14  in
: 226 2 13 to
: 228 9 11  town
- 239
: 259 5 18 Straight
: 265 2 19  out
: 268 2 16  of
: 271 7 18  hell
- 280
: 292 4 13 One
: 297 2 14  of
: 300 2 11  a
: 303 7 13  kind
- 312
: 325 3 18 Stal
: 328 3 18 king
: 332 3 18  his
: 336 6 19  vic
: 343 4 18 tim
- 349
: 352 3 13 Don't
: 356 3 13  look
: 360 3 13  be
: 364 3 14 hind
: 368 3 13  you
: 372 2 11  Ni
: 374 2 13 ~
: 376 6 14 ght
: 384 3 13 craw
: 388 4 11 ler
- 394
: 412 2 14 Be
: 414 5 16 ware
: 420 2 14  the
: 422 3 13  beast
: 426 3 11  in
* 430 5 6  black
- 437
: 440 2 13 Ni
: 442 5 14 ght
: 448 4 13 craw
: 452 4 11 ler
- 458
: 476 3 14 You
: 480 3 16  know
: 484 3 13  he's
: 488 2 14  co
: 490 3 13 ming
: 494 6 13  back
- 502
: 504 3 13 Ni
: 507 3 14 ght
: 511 5 13 craw
: 516 76 11 ler
- 594
: 640 7 18 Sanc
: 648 8 19 tua
: 656 7 18 ry
: 664 7 16  is
: 672 5 18  be
: 680 6 11 ing
: 688 6 14  sought
- 696
: 704 8 13 Whis
: 712 6 14 pered
: 720 7 16  prayers
: 728 6 13  a
: 735 3 14  last
: 738 2 13  re
: 740 4 11 sort
- 746
: 769 7 18 Ho
: 777 6 19 ming
: 784 7 18  in
: 792 6 16  it's
: 800 6 18  cry
: 808 8 11  dis
: 816 7 14 torts
- 825
: 832 8 13 Ter
: 840 4 14 ror
* 848 5 16  struck
: 856 6 13  they
: 864 2 14  know
: 866 3 13  they're
: 869 5 11  caught
- 876
: 900 4 18 Straight
: 905 2 19  out
: 908 2 16  of
: 911 8 18  hell
- 921
: 932 4 13 One
: 937 2 14  of
: 940 1 11  a
: 942 9 13  kind
- 953
: 964 4 18 Stal
: 968 3 18 king
: 972 3 18  his
: 976 8 19  vic
: 984 4 18 tim
- 990
: 992 3 13 Don't
: 996 3 13  look
: 1000 3 13  be
: 1004 3 14 hind
: 1008 3 13  you
: 1012 2 11  Ni
: 1014 2 13 ~
: 1016 6 14 ght
: 1024 3 13 craw
: 1028 3 11 ler
- 1033
: 1052 2 14 Be
: 1054 5 16 ware
: 1060 2 14  the
: 1062 4 13  beast
: 1067 3 11  in
* 1071 5 6  black
- 1078
: 1080 2 13 Ni
: 1082 5 14 ght
: 1088 4 13 craw
: 1092 4 11 ler
- 1098
: 1116 3 14 You
: 1120 3 16  know
: 1124 3 13  he's
: 1128 2 14  co
: 1130 3 13 ming
: 1135 5 6  back
- 1142
: 1144 3 13 Ni
: 1147 3 14 ght
: 1152 4 13 cra
: 1156 4 11 ~w
: 1160 80 11 ler
- 1242
: 1576 6 18 As
: 1584 6 25  night
: 1592 6 26  is
: 1600 12 25  fa
: 1612 4 23 ~
: 1616 12 23 lling
- 1630
: 1656 6 26 The
: 1664 6 28  end
: 1672 5 26  is
: 1680 6 25  draw
: 1687 7 26 ing
* 1696 7 26  near
- 1705
: 1720 7 26 They'll
: 1728 4 28  he
: 1732 8 26 ar
- 1742
: 1752 7 26 Their
: 1760 7 28  last
: 1768 8 26  rites
: 1777 7 25  e
: 1784 7 26 cho
: 1792 7 25  on
: 1800 7 25  the
: 1808 70 25  wind
- 1880
: 2270 4 4 Hu
: 2274 4 4 ddled
: 2279 3 4  in
: 2282 3 2  the
: 2286 3 4  ce
: 2289 8 -1 llar
- 2299
: 2304 4 4 Fear
: 2309 3 -1  caught
: 2313 3 -1  in
: 2316 4 -1  their
: 2321 10 2  eyes
- 2333
: 2337 3 4 Da
: 2340 4 -1 ring
: 2345 4 4  not
: 2349 2 2  to
: 2352 3 4  move
: 2355 3 2  or
: 2358 7 4  breathe
- 2366
: 2368 3 -1 As
: 2372 2 -1  the
: 2375 3 -1  crea
: 2378 3 -1 ture
: 2382 10 -1  cries
- 2394
: 2401 3 4 Fin
: 2404 2 2 ger
: 2407 5 4  nails
: 2413 3 2  start
: 2417 4 4  scrat
: 2421 7 -1 ching
- 2430
: 2433 3 4 On
: 2436 2 4  the
: 2439 3 4  out
: 2443 3 2 side
: 2448 9 4  wall
- 2459
: 2464 5 4 Claw
: 2469 3 2 ing
: 2473 3 2  at
: 2477 2 2  the
: 2479 3 4  win
: 2482 8 2 dows
- 2492
: 2496 3 -1 'Come
: 2500 2 4  to
: 2502 5 2  me'
: 2508 3 4  it
: 2512 10 -1  calls
- 2524
: 2530 3 -1 At
: 2533 3 -1 mos
: 2536 5 4 phere's
: 2541 2 2  e
: 2543 2 2 lec
: 2545 5 2 tric
- 2552
: 2554 2 4 As
: 2556 2 2  it
: 2559 5 4  now
: 2565 3 2  des
: 2568 2 2 cend
: 2572 2 3  the
: 2575 10 4  stairs
- 2587
: 2594 3 2 Hi
: 2597 3 4 ding
: 2601 2 2  in
: 2603 1 -1  the
: 2605 4 -1  dark
: 2609 4 -1 ness
- 2615
: 2618 2 4 Is
: 2620 3 4  so
: 2624 3 4  fu
: 2627 4 2 tile
: 2632 3 4  from
: 2635 2 4  its
: 2638 8 4  glare
- 2648
: 2656 5 4 Death
: 2662 5 4  comes
: 2668 2 4  in
: 2670 2 2  an
: 2672 6 -1  instant
- 2680
: 2687 3 4 As
: 2691 3 4  they
: 2695 4 4  hoped
: 2699 2 2  it
: 2702 7 4  would
- 2711
: 2717 8 4 Souls
: 2726 2 4  as
: 2728 2 3 cend
: 2731 2 4  to
: 2734 2 3  hea
: 2736 4 2 ven
- 2741
: 2743 3 2 While
: 2746 2 4  it
: 2749 4 4  feasts
: 2754 2 4  on
: 2757 5 2  flesh
: 2763 4 4  and
: 2768 6 -1  bloo
: 2774 3 4 ~
: 2777 3 16 d
- 2782
: 2917 4 18 Straight
: 2922 2 19  out
: 2925 2 16  of
: 2928 8 18  hell
- 2938
: 2949 4 13 One
: 2954 2 14  of
: 2957 1 11  a
: 2959 9 13  kind
- 2970
: 2981 4 18 Stal
: 2985 3 19 king
: 2989 3 18  his
: 2993 6 19  vic
: 2999 5 18 tim
- 3006
: 3009 3 13 Don't
: 3013 3 13  look
: 3017 3 13  be
: 3021 3 14 hind
: 3025 3 13  you
: 3029 2 11  Ni
: 3031 2 13 ~
: 3033 6 14 ght
: 3041 3 13 craw
: 3045 4 11 ler
- 3051
: 3069 2 14 Be
: 3071 5 16 ware
: 3077 1 14  the
: 3079 3 13  beast
: 3083 3 11  in
: 3088 5 6  black
- 3095
: 3097 2 13 Ni
: 3099 5 14 ght
: 3106 3 13 craw
: 3109 4 11 ler
- 3115
: 3133 3 14 You
: 3137 3 16  know
: 3141 2 13  he's
: 3145 2 13  co
: 3147 3 13 ming
: 3151 5 6  back
- 3158
: 3161 3 14 Ni
: 3164 3 13 ght
: 3169 3 13 craw
: 3173 5 11 ler
- 3180
: 3197 3 14 Be
: 3201 3 16 ware
: 3205 2 13  the
: 3207 4 13  beast
: 3212 2 11  in
: 3216 5 6  black
- 3223
: 3225 3 14 Ni
: 3228 4 13 ght
: 3233 4 13 craw
: 3237 6 11 ler
- 3245
: 3262 3 14 You
: 3266 3 16  know
: 3270 3 13  he's
: 3274 2 13  co
: 3276 3 14 ming
: 3280 5 6  back
- 3287
: 3289 3 13 Ni
: 3292 4 14 ght
: 3297 4 13 craw
: 3301 101 11 ler
- 3404
: 3478 2 13 Be
: 3480 4 14 ware
: 3485 2 11  the
: 3488 6 14  beast
- 3496
: 3546 2 13 Ni
: 3549 2 14 ght
: 3554 3 13 craw
: 3558 11 11 ler
- 3571
: 3605 2 13 Be
: 3607 5 14 ware
: 3614 1 11  the
: 3616 7 14  beast
E