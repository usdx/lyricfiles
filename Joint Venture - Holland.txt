#TITLE:Holland
#ARTIST:Joint Venture
#MP3:Joint Venture - Holland.mp3
#COVER:Joint Venture - Holland [CO].jpg
#BACKGROUND:Joint Venture - Holland [BG].jpg
#BPM:297,5
#GAP:13800
#ENCODING:UTF8
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
: 0 2 2 Nicht
: 5 4 7  weit
: 12 2 7  von
: 15 2 7  uns
: 19 2 7  im
: 24 3 9  Wes
: 30 2 7 ten
- 34
: 48 2 4 da
: 51 3 2  liegt
: 56 4 2  ein
: 64 3 4  klei
: 68 2 5 nes
: 74 5 4  Land,
- 81
: 94 2 4 das
: 97 2 2  ich
: 102 2 0  im
: 105 5 0 mer
: 112 2 2  wenn
: 115 2 4  ich
: 122 8 5  da
: 132 3 4  war
- 137
: 139 3 2 ziem
: 144 3 0 lich
: 150 3 -1  ü
: 154 4 0 ber
: 161 4 2 zeu
: 167 2 -7 gend
: 172 6 4  fand.
- 180
: 195 2 -5 Ein
: 199 4 7  Land
: 206 2 7  mit
: 209 4 7  al
: 215 2 7 ten
: 219 6 9  Wind
: 228 4 11 mühl'n
- 233
: 235 5 12 und
: 243 2 9  mit
: 249 3 7  net
: 253 3 9 ten
: 261 3 7  klei
: 266 3 5 nen
: 274 3 5  Städ
: 278 3 4 ten,
- 283
: 293 2 -8 mit
: 298 3 4  Ba
: 303 2 4 mi,
: 308 4 4  Vla,
: 316 3 4  Fri
: 320 3 9 kan
: 325 4 7 deln
- 331
: 339 2 0 und
: 344 2 0  frit
: 347 4 -1 tier
: 353 2 0 ten
: 359 4 2  Fleisch
: 366 5 4 kro
: 373 2 2 ket
: 377 2 0 ten.
- 381
: 467 2 5 In
: 470 4 7  E
: 475 5 7 dam
: 482 3 7  gibts
: 488 3 7  den
* 494 5 9  Kä
: 501 5 7 se
- 508
: 511 3 5 und
: 515 2 4  in
: 519 3 2  Ams
: 526 2 2 ter
: 529 4 4 dam
: 535 2 5  die
: 540 3 5  Grach
: 547 4 4 ten.
- 553
: 563 2 2 Ich
: 569 2 0  steh'
: 572 3 0  auf
: 580 5 2  Frau'n
: 586 2 4  in
: 589 5 5  Holz
: 599 3 4 schuh'n
- 604
: 608 4 0 und
: 616 4 -1  blau-
: 622 4 0 weiß-
: 630 4 2 ro
: 636 2 -5 ten
: 641 3 5  Trach
: 647 3 4 ten.
- 652
: 662 2 -5 Ich
: 668 2 7  kom
: 671 2 7 me
: 674 5 7  gern
: 683 2 7  zum
: 688 5 9  Ba
: 695 3 11 den
- 700
: 703 3 11 Und
: 708 3 9  ich
: 715 4 7  bleib'
: 721 3 9  auch
: 727 4 7  gern
: 734 3 5  zum
: 741 4 5  Zel
: 747 2 4 ten.
- 751
: 761 2 0 Ja,
: 765 2 4  sie
: 769 4 4  sind
: 778 2 4  tol
: 781 2 4 le
: 784 3 9  Nach
: 790 5 7 barn
- 797
: 811 2 0 und
: 815 3 -1  doch
: 825 2 -1  tren
: 828 2 0 nen
: 831 3 2  uns
: 836 4 2  Wel
: 844 3 0 ten.
- 849
: 882 2 0 Ich
: 888 4 12  lie
: 894 3 12 be
: 901 2 12  Su
: 904 3 12 per
: 911 6 11 skunk
- 919
: 925 4 11 und
: 931 3 9  ich
: 937 4 7  lie
: 942 2 9 be
: 947 4 7  Sauce
: 955 2 5  spe
: 960 6 4 cial,
- 968
: 978 3 0 a
: 982 2 0 ber
: 986 4 12  ei
: 991 2 12 ne
: 995 3 12  Sa
: 1000 2 12 che
: 1004 5 11  gibts,
- 1011
: 1015 4 11 da
: 1021 3 11  bin
: 1025 4 9  ich
: 1034 4 7  me
: 1040 4 9 ga-
: 1046 4 7 na
: 1053 3 5 tio
: 1057 6 4 nal.
- 1065
: 1078 2 0 Es
: 1085 4 12  kam
: 1091 2 12  ü
: 1094 2 12 ber
: 1098 2 12  die
: 1102 10 11  Jah
: 1115 3 11 re
- 1119
: 1120 3 11 und
: 1127 3 9  jetzt
: 1133 2 7  sitzt
: 1139 2 9  es
: 1146 3 7  ziem
: 1151 2 5 lich
: 1157 2 4  fest:
- 1161
: 1169 2 0 So
: 1172 2 0 lang's
: 1176 2 0  um
: 1181 3 9  Fuß
: 1188 2 7 ball
: 1192 3 5  geht
- 1197
: 1200 2 4 hass'
: 1204 2 4  ich
: 1208 3 5  Hol
: 1212 4 4 land
: 1220 3 2  wie
: 1226 3 -1  die
: 1232 2 0  Pest!
- 1236
: 1318 3 7 Grad
: 1324 4 7  bei
: 1330 3 4  wich
: 1336 2 4 ti
: 1339 3 4 gen
: 1346 2 11  Tur
* 1350 3 9 nier'n
- 1355
: 1363 3 7 sollt'
: 1369 3 5  man
: 1374 2 4  sich
: 1380 2 2  nicht
: 1386 2 2  da
: 1392 3 4 für
: 1398 2 5  ge
: 1402 4 4 nier'n.
- 1408
: 1417 2 4 Was
: 1423 3 2  kann
: 1430 2 0  schlim
: 1433 2 0 me
: 1437 2 2 res
: 1444 2 4  pas
: 1450 5 5 sier'n
- 1457
: 1461 2 4 als
: 1466 2 2  dass
: 1472 2 0  wir
: 1477 4 -1  ge
: 1483 2 0 gen
: 1490 3 2  die
: 1497 2 -5  ver
: 1501 4 4 lier'n?
- 1507
: 1519 3 7 Bei
: 1523 2 7 de
: 1527 2 7  ham'
: 1532 3 7  wir
: 1537 3 7  un
: 1542 3 7 sern
* 1547 4 9  Ru
* 1552 5 11 di,
- 1559
: 1567 2 11 wir
: 1570 3 9  ham'
: 1576 4 7  ih
: 1582 3 9 ren
: 1588 4 7  nie
: 1594 2 5  be
: 1601 2 4 spuckt.
- 1605
: 1608 3 4 Ein
: 1614 3 4  gu
: 1619 2 2 ter
: 1623 4 0  Deut
: 1630 4 -1 scher
: 1637 2 0  denkt
: 1642 2 0  eu
: 1646 3 0 ro
: 1651 4 9 pä
: 1657 3 7 isch,
- 1662
: 1666 3 5 au
: 1672 3 4 ßer
: 1677 3 5  wenn
: 1681 4 4  er
: 1688 2 2  ki
: 1691 2 0 cken
: 1698 3 0  guckt.
- 1703
: 1740 3 0 Ich
: 1747 4 12  lie
: 1752 3 12 be
: 1758 3 12  Su
: 1762 2 12 per
: 1769 5 11 skunk
- 1776
: 1783 3 11 und
: 1789 3 9  ich
: 1796 3 7  lie
: 1801 2 9 be
: 1806 3 7  Sauce
: 1813 2 5  spe
: 1819 6 4 cial.
- 1827
: 1838 2 -1 A
: 1841 2 0 ber
: 1845 4 12  ei
: 1851 3 12 ne
: 1855 3 12  Sa
: 1860 2 12 che
: 1864 6 11  gibts,
- 1872
: 1875 3 11 da
: 1880 3 11  bin
: 1885 3 9  ich
: 1893 5 7  me
: 1899 4 9 ga-
: 1905 4 7 na
: 1912 3 5 tio
: 1916 5 4 nal.
- 1923
: 1937 2 0 Es
: 1943 3 12  kam
: 1949 2 12  ü
: 1952 2 12 ber
: 1956 3 12  die
: 1961 8 11  Jah
: 1972 3 11 re
- 1976
: 1978 3 11 und
: 1985 3 9  jetzt
: 1992 2 7  sitzt
: 1998 2 9  es
: 2005 3 7  ziem
: 2010 3 5 lich
: 2017 2 4  fest:
- 2021
: 2028 2 -1 So
: 2031 2 0 lang's
: 2036 2 0  um
: 2040 3 9  Fuß
: 2047 2 7 ball
: 2051 5 5  geht
- 2057
: 2059 2 4 hass'
: 2063 2 4  ich
: 2068 2 5  Hol
: 2071 5 4 land
: 2079 4 2  wie
: 2085 4 -1  die
: 2091 2 0  Pest!
- 2095
: 2136 3 12 Ich
: 2141 4 11  weiß,
: 2149 2 11  du
: 2152 2 11  bist
: 2156 4 12  da
: 2164 3 14  schlau
: 2168 6 14 er,
- 2175
: 2177 3 11 du
: 2182 3 11  bist
: 2189 3 12  nicht
: 2195 3 12  so
: 2200 3 9  ein
* 2207 3 9  Pro
* 2211 5 7 let,
- 2218
: 2231 2 12 im
: 2234 2 12 mer
: 2238 3 11  lo
: 2242 3 11 cker,
- 2246
: 2248 3 11 selbst
: 2255 3 12  wenns
: 2262 3 14  Eins-
: 2269 3 14 Null
- 2273
: 2275 3 12 für
: 2281 4 11  die
* 2287 5 12  Nie
* 2294 4 12 der
* 2300 4 9 lan
* 2306 2 5 de
: 2312 5 7  steht:
- 2319
: 2326 3 0 "Wer
: 2331 3 0  sich
: 2336 4 7  auf
: 2344 3 7 regt
: 2350 3 7  we
: 2354 3 7 gen
: 2360 4 9  Fuß
: 2368 5 11 ball
- 2373
: 2373 3 11 ist
: 2378 5 9  ein
: 2385 5 7  ar
: 2392 3 9 mer
: 2397 2 7  I
: 2401 2 5 di
: 2405 8 4 ot!"
- 2415
: 2422 2 4 Fick
: 2429 2 2  dich
: 2435 4 0  selbst,
- 2441
: 2445 2 0 wenn
: 2448 2 0  ich
: 2452 4 0  O
: 2458 5 9 ran
: 2466 4 7 je
: 2473 3 5  se
: 2477 3 4 he,
- 2482
: 2485 3 5 seh'
: 2491 2 4  ich
: 2497 3 2  nun
: 2503 3 -1 mal
: 2510 7 0  rot!
- 2519
: 2552 3 0 Ich
: 2558 3 12  lie
: 2563 3 12 be
: 2569 2 12  Su
: 2573 2 12 per
* 2580 6 11 skunk
- 2588
: 2593 4 11 und
: 2599 4 9  ich
: 2606 3 7  lie
: 2611 2 9 be
: 2616 3 7  Sauce
: 2623 2 5  spe
: 2628 7 4 cial.
- 2637
: 2649 2 0 A
: 2652 2 0 ber
: 2656 4 12  ei
: 2662 2 12 ne
: 2666 3 12  Sa
: 2672 2 12 che
: 2675 4 11  gibt's,
- 2681
: 2686 4 12 da
: 2692 2 11  bin
: 2695 4 9  ich
: 2704 4 7  me
: 2710 4 9 ga-
: 2716 3 7 na
: 2723 3 5 tio
: 2727 6 4 nal.
- 2735
: 2748 2 0 Es
: 2755 3 12  kam
: 2761 2 12  ü
: 2764 2 12 ber
: 2768 3 12  die
* 2773 9 14  Jah
* 2785 4 11 re
- 2790
: 2791 4 11 und
: 2798 3 9  jetzt
: 2804 2 7  sitzt
: 2810 2 9  es
: 2816 3 7  ziem
: 2822 2 5 lich
: 2828 2 4  fest:
- 2832
: 2840 2 0 So
: 2843 3 0 lang's
: 2848 2 0  um
: 2853 3 9  Fuß
: 2859 2 7 ball
: 2863 3 5  geht
- 2868
: 2871 2 4 hass'
: 2874 2 4  ich
: 2879 2 5  Hol
: 2882 5 4 land
: 2890 4 2  wie
: 2896 3 -1  die
: 2902 2 0  Pest!
- 2906
: 2936 2 0 So
: 2939 3 0 lang's
: 2944 2 0  um
: 2949 3 9  Fuß
: 2955 3 7 ball
: 2960 3 5  geht
- 2965
: 2968 2 4 hass'
: 2972 3 4  ich
* 2978 2 5  Hol
* 2982 4 4 land
: 2991 4 2  wie
: 2997 4 -1  die
: 3005 3 0  Pest!
E