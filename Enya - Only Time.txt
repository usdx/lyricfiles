#TITLE:Only Time
#ARTIST:Enya
#MP3:Enya - Only Time.mp3
#VIDEO:Enya - Only Time.mp4
#COVER:Enya - Only Time[CO].jpg
#BPM:213,8
#GAP:11700
#ENCODING:UTF8
#PREVIEWSTART:31,490
#LANGUAGE:English
#GENRE:Pop
#YEAR:2000
: 0 9 3 Who 
: 11 7 3 can 
: 20 9 3 say 
* 31 5 5 where 
: 38 2 2 the 
: 42 3 2 roa
* 46 5 3 ~d 
: 53 13 3 goes
- 68
: 72 5 5 Where 
: 79 3 2 the 
: 84 3 2 day
* 88 4 3 ~ 
: 94 12 3 flows
- 108
: 114 5 5 On
: 121 5 2 ly 
: 128 14 3 time
- 144
: 162 3 -2 And 
: 167 7 3 who 
: 176 6 3 can 
: 184 12 3 say 
* 198 3 5 if 
: 203 3 2 your 
: 208 3 2 love
* 212 4 3 ~ 
: 218 13 3 goes
- 233
: 236 6 5 As 
: 244 5 2 your 
: 250 3 2 heart
* 254 4 3 ~ 
: 260 14 3 chose
- 276
: 278 6 5 On
: 286 6 2 ly 
: 294 12 3 time
- 308
: 702 10 3 Who 
: 714 6 3 can 
: 722 10 3 say 
* 734 4 5 why 
: 740 3 2 your 
: 744 4 2 heart
* 749 3 3 ~ 
: 754 14 3 sights
- 770
: 775 6 5 As 
: 783 4 2 your 
: 789 2 2 love
* 792 4 3 ~ 
: 798 12 3 files
- 812
: 816 5 5 On
: 823 5 2 ly 
: 830 12 3 time
- 844
: 863 4 -2 And 
: 869 7 3 who 
: 878 6 3 can 
: 886 10 3 say 
* 898 5 5 why 
: 905 4 2 your 
: 911 3 2 heart
* 915 4 3 ~ 
: 921 11 3 cries
- 934
: 940 5 5 When 
: 947 3 2 your 
: 952 3 2 love
* 956 4 3 ~ 
: 962 12 3 dies
- 976
: 982 4 5 On
: 988 6 2 ly 
: 996 14 3 time
- 1012
: 1406 9 3 Who 
: 1417 5 3 can 
: 1424 10 3 say 
* 1436 5 5 when 
: 1443 2 2 the 
: 1447 5 2 roa
* 1453 5 3 ~ds 
: 1460 8 3 meet
- 1470
: 1477 5 5 That 
: 1484 3 2 love 
: 1489 4 2 might
* 1494 4 3 ~ 
: 1500 12 3 be
- 1514
: 1519 5 5 In 
: 1526 6 2 your 
: 1534 12 3 heart
- 1548
: 1566 5 -2 And 
: 1573 6 3 who 
: 1581 7 3 can 
: 1590 9 3 say 
* 1601 6 5 when 
: 1609 2 2 the 
: 1613 3 2 day
* 1617 3 3 ~ 
: 1622 10 3 sleeps
- 1634
: 1643 4 5 If 
: 1649 3 2 the 
: 1654 4 2 night
* 1659 5 3 ~ 
: 1666 10 3 keeps
- 1678
: 1686 4 5 All 
: 1692 6 2 your 
: 1700 14 3 heart
- 1716
: 2172 9 3 Who 
: 2183 6 3 can 
: 2191 10 3 say 
* 2203 3 5 if 
: 2208 3 2 your 
: 2213 3 2 love
* 2217 5 3 ~ 
: 2224 12 3 grows
- 2238
: 2243 5 5 As 
: 2250 3 2 your 
: 2255 3 2 heart
* 2259 3 3 ~ 
: 2264 14 3 chose
- 2280
: 2284 5 5 On
: 2291 6 2 ly 
: 2299 15 3 time
- 2316
: 2333 3 -2 And 
: 2338 7 3 who 
: 2347 7 3 can 
: 2356 11 3 say 
* 2369 3 5 where 
: 2374 3 2 the 
: 2379 3 2 ro
* 2383 4 3 ~ad 
: 2389 11 3 goes
- 2402
: 2409 3 5 Where 
: 2414 3 2 the 
: 2419 3 2 day
* 2423 4 3 ~ 
: 2429 11 3 flows
- 2442
: 2449 5 5 On
: 2456 5 2 ly 
: 2463 15 3 time
- 2480
: 2501 10 3 Who 
: 2513 19 3 knows 
: 2534 4 5 on
: 2540 6 2 ly 
: 2548 10 3 time
- 2560
: 2584 10 3 Who 
: 2596 19 3 knows 
: 2617 4 5 on
: 2623 6 2 ly 
: 2631 11 3 time
E