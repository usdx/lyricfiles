#TITLE:Ca fait rire les oiseaux
#ARTIST:La Compagnie Créole
#MP3:La Compagnie Créole - Ca fait rire les oiseaux.mp3
#VIDEO:La Compagnie Créole - Ca fait rire les oiseaux.mp4
#COVER:La Compagnie Créole - Ca fait rire les oiseaux.jpg
#BPM:248.45
#GAP:19542
#ENCODING:UTF8
#LANGUAGE:French
#GENRE:Zouk
#YEAR:1986
#CREATOR:mustangfred
: 0 2 -3 Ça
: 4 2 0  fait
: 8 4 5  rire
: 14 3 5  les
* 20 3 5  oi
* 24 6 5 seaux
- 31
: 33 2 5 Ça
: 36 2 4  fait
* 40 2 2  chan
* 44 1 2 ter
: 46 2 2  les
: 50 1 2  a
: 53 3 2 beilles
- 65
: 69 1 2 Ça
: 73 3 0  chas
: 78 1 0 se
: 80 3 0  les
: 84 2 0  nu
: 89 5 0 ages
: 97 1 -2  et
: 100 2 -3  fait
: 105 2 -5  bril
: 108 2 0 ler
: 111 2 0  le
* 116 3 0  so
* 120 3 0 leil
- 127
: 129 2 -3 Ça
: 133 2 0  fait
: 137 3 5  rire
: 142 3 5  les
: 148 3 5  oi
: 152 4 5 seaux
- 157
: 159 4 5 Et
: 165 2 4  dan
: 169 1 2 ser
: 172 1 2  les
: 174 2 2  é
: 178 1 2 cu
: 180 3 2 reuils
- 190
: 193 2 2 Ça
: 197 1 2  ra
: 201 3 0 jou
: 206 1 0 te
: 208 2 0  des
: 213 2 0  cou
: 216 5 0 leurs
- 222
: 224 1 -2 Aux
: 230 1 -3  cou
: 232 2 -5 leurs
: 236 3 0  de
: 240 2 0  l'arc
: 244 2 0  en
: 248 3 0  ciel
- 255
: 257 1 -3 Ça
: 261 2 0  fait
: 265 4 5  rire
: 271 2 5  les
: 276 3 5  oi
: 280 5 5 seaux
- 295
* 304 7 9 Oh,
* 313 6 7  oh,
* 321 6 9  oh,
: 329 3 5  rire
: 334 3 5  les
: 340 3 5  oi
: 344 5 5 seaux
- 365
: 385 1 -3 Ça
: 389 2 0  fait
: 393 4 5  rire
: 399 2 5  les
: 404 2 5  oi
: 408 6 5 seaux
- 424
: 432 7 9 Oh,
: 441 6 7  oh,
: 449 6 9  oh,
: 457 4 5  rire
: 463 2 5  les
: 468 3 5  oi
: 473 4 5 seaux
- 493
: 516 2 0 Une
: 520 4 0  chan
: 527 3 0 son
: 532 3 0  d'a
: 537 1 2 mour
- 539
: 541 2 0 C'est
: 545 2 -3  comme
: 549 1 -7  un
: 552 1 -5  loo
: 555 1 -5 ping
: 558 2 -5  en
: 561 2 -7  a
: 565 5 -3 vion
- 577
: 580 2 0 Ça
: 584 3 7  fait
: 589 1 7  bat
: 593 1 7 tre
: 596 1 7  le
* 600 2 9  coeur
- 603
: 605 1 7 Des
: 609 2 5  filles
: 612 3 2  et
: 616 3 5  des
: 621 3 5  gar
: 628 5 2 çons
- 642
: 645 1 0 Une
: 649 3 0  chan
: 655 3 0 son
: 660 3 0  d'a
: 664 1 2 mour
- 666
: 668 2 0 C'est
: 672 2 -3  d'l'o
: 677 1 -7 xy
: 680 2 -5 gčne
: 684 1 -5  dans
: 686 2 -5  la
: 689 2 -7  mai
: 693 3 -3 son
- 705
: 709 1 0 Tes
: 713 1 7  pieds
: 717 1 7  touchent
: 721 1 7  plus
* 724 2 7  par
* 728 4 9  terre
- 734
: 736 3 5 T'es
: 740 2 2  en
* 744 3 5  lé
* 749 1 5 vi
* 752 2 5 ta
* 757 4 2 tion
- 763
: 765 2 2 Si
: 769 1 2  y'a
: 772 1 4  d'la
: 777 3 5  pluie
: 782 3 5  dans
: 788 2 5  ta
* 793 7 5  vie
- 801
: 803 1 5 Si
: 805 1 4  le
: 808 3 4  soir
: 813 1 2  te
: 816 1 2  fait
: 820 6 2  peur
- 834
: 837 2 2 La
: 841 1 0  mu
: 845 1 0 sique
: 849 2 0  est
: 853 9 0  lŕ
: 865 4 2  pour
* 873 1 0  ça
* 875 2 -5 ~
* 878 6 -3 ~
- 893
: 896 2 2 Y'a
: 901 2 4  tou
: 905 2 5 jours
: 909 2 5  une
* 913 2 5  mé
* 916 1 5 lo
* 918 7 5 die
- 926
: 928 3 5 Pour
: 932 2 4  des
: 937 4 4  jours
: 944 2 2  meil
: 948 6 2 leurs
- 960
: 963 1 0 Al
: 965 1 0 lez
: 969 2 0  tape
: 975 3 0  dans
: 980 2 0  tes
: 984 3 0  mains
- 993
: 996 2 0 Ça
: 1000 3 0  por
: 1006 3 0 te
: 1012 2 0  bon
: 1016 4 0 heur
- 1026
: 1028 2 0 C'est
: 1032 2 0  ma
: 1037 1 0 gique
: 1040 3 0  un
: 1044 1 0  re
: 1048 3 0 frain
- 1052
: 1054 4 0 qu'on
: 1060 1 0  re
: 1064 3 0 prend
: 1070 2 0  tous
: 1076 2 0  en
: 1080 2 0  coeur
- 1083
: 1085 1 -3 Et
: 1088 2 -3  ça
: 1092 2 0  fait
: 1096 4 5  rire
: 1102 3 5  les
: 1108 2 5  oi
: 1112 4 5 seaux
- 1119
: 1121 1 5 Ça
: 1124 2 4  fait
: 1128 2 2  chan
: 1132 1 2 ter
: 1134 2 2  les
: 1138 1 2  a
: 1140 4 2 beilles
- 1153
: 1157 1 2 Ça
: 1161 3 0  chas
: 1167 1 0 se
: 1169 2 0  les
: 1172 3 0  nu
: 1176 7 0 ages
: 1185 1 -2  et
: 1188 2 -3  fait
: 1193 2 -5  bril
: 1196 1 0 ler
: 1199 2 0  le
: 1204 3 0  so
: 1208 2 0 leil
- 1215
: 1217 1 -3 Ça
: 1221 2 0  fait
: 1225 3 5  rire
: 1230 3 5  les
* 1236 2 5  oi
* 1240 6 5 seaux
: 1248 3 5  et
: 1252 2 4  dan
: 1256 2 2 ser
: 1260 1 2  les
: 1263 1 2  é
: 1266 1 2 cu
: 1268 2 2 reuils
- 1277
: 1280 2 2 Ça
: 1284 2 2  ra
: 1289 3 0 jou
: 1294 1 0 te
: 1296 2 0  des
: 1301 2 0  cou
: 1304 5 0 leurs
- 1310
: 1312 1 -2 Aux
: 1317 2 -3  cou
: 1320 2 -5 leurs
: 1324 3 0  de
: 1328 1 0  l'arc
: 1332 1 0  en
: 1336 2 0  ciel
- 1343
: 1345 1 -3 Ça
: 1348 3 0  fait
: 1353 4 5  rire
: 1359 2 5  les
: 1364 2 5  oi
: 1368 5 5 seaux
- 1383
: 1393 5 9 Oh,
: 1400 7 7  oh,
: 1409 5 9  oh,
: 1417 3 5  rire
: 1422 3 5  les
: 1428 3 5  oi
: 1432 5 5 seaux
- 1453
: 1477 2 0 T'es
: 1480 2 0  re
: 1483 2 0 ve
: 1486 4 0 nu
: 1493 1 0  chez
: 1496 1 2  toi
- 1498
: 1500 3 0 La
: 1505 2 -3  tę
: 1508 2 -7 te
: 1512 2 -5  pleine
: 1516 1 -5  de
: 1518 1 -5  sou
: 1522 1 -7 ve
: 1524 4 -3 nirs
- 1537
: 1541 1 0 Des
* 1544 3 7  soirs
: 1549 1 7  au
: 1553 2 7  clair
: 1556 3 7  de
: 1560 4 9  lune
- 1566
: 1568 3 5 Des
: 1573 2 2  mo
: 1576 3 5 ments
: 1581 1 5  de
: 1585 2 5  plai
: 1589 4 2 sir
- 1601
: 1604 2 0 T'es
: 1608 1 0  re
: 1611 2 0 ve
: 1614 4 0 nu
: 1620 2 0  chez
: 1624 2 2  toi
: 1628 2 0  et
: 1632 2 -3  tu
: 1636 2 -7  veux
: 1640 1 -5  dé
: 1643 1 -5 jŕ
: 1646 1 -5  re
: 1649 2 -7 par
: 1653 3 -3 tir
- 1665
: 1668 1 0 Re
: 1672 2 7 trou
: 1676 2 7 ver
: 1680 3 7  l'a
: 1684 2 7 ven
: 1688 2 9 ture
: 1692 2 9  qui
: 1696 3 5  n'au
: 1700 2 2 rait
: 1704 4 5  pas
: 1710 1 5  dű
: 1713 2 5  fi
: 1716 4 2 nir
- 1723
: 1725 2 2 Si
: 1728 3 2  y'a
: 1732 2 4  du
: 1736 4 5  gris
: 1741 5 5  dans
: 1748 2 5  tes
: 1751 7 5  nuits
- 1763
: 1765 2 5 Des
: 1768 4 4  lar
: 1773 1 4 mes
: 1775 2 2  dans
: 1779 1 2  ton
: 1783 5 2  coeur
- 1794
: 1797 2 2 La
: 1800 2 0  mu
: 1805 1 0 sique
: 1809 2 0  est
: 1812 11 0  lŕ
: 1825 3 2  pour
: 1833 2 0  ça
: 1836 1 -5 ~
: 1838 7 -3 ~
- 1854
: 1857 2 2 Y'a
: 1861 1 4  tou
: 1865 2 5 jours
: 1869 1 5  une
: 1873 2 5  mé
: 1876 1 5 lo
: 1878 7 5 die
- 1887
: 1889 2 5 Pour
: 1892 2 4  des
: 1896 4 4  jours
: 1901 2 2 ~
: 1905 2 2  meil
: 1909 6 2 leurs
- 1921
: 1923 1 0 Al
: 1925 1 2 lez
: 1929 1 0  tape
: 1934 2 0  dans
: 1940 2 0  tes
: 1944 4 0  mains
- 1954
: 1956 2 0 Ça
: 1960 3 0  por
: 1966 3 0 te
: 1972 2 0  bon
: 1976 4 0 heur
- 1986
: 1988 2 0 C'est
: 1993 1 0  ma
: 1997 1 0 gique
: 2001 2 0  un
: 2004 1 0  re
: 2008 3 0 frain
- 2012
: 2014 3 0 Qu'on
: 2020 1 0  re
: 2024 3 0 prend
: 2030 1 0  tous
: 2036 1 0  en
: 2040 2 0  coeur
- 2043
: 2045 1 -3 Et
: 2048 2 -3  ça
: 2052 2 0  fait
: 2056 4 5  rire
: 2062 3 5  les
: 2068 2 5  oi
: 2072 5 5 seaux
- 2079
: 2081 1 5 Ça
: 2084 2 4  fait
: 2088 2 2  chan
: 2092 1 2 ter
: 2094 2 2  les
: 2098 1 2  a
: 2100 4 2 beilles
- 2113
: 2116 2 2 Ça
: 2120 4 0  chas
: 2127 1 0 se
: 2129 2 0  les
: 2133 2 0  nu
: 2137 6 0 ages
: 2145 1 -2  et
: 2149 1 -3  fait
: 2153 1 -5  bril
: 2156 2 0 ler
: 2159 2 0  le
: 2164 3 0  so
: 2168 2 0 leil
- 2175
: 2177 1 -3 Ça
: 2180 2 0  fait
: 2184 4 5  rire
: 2190 3 5  les
: 2196 2 5  oi
: 2200 6 5 seaux
: 2208 3 5  et
: 2212 2 4  dan
: 2216 2 2 ser
: 2220 1 2  les
: 2222 2 2  é
: 2226 1 2 cu
: 2228 2 2 reuils
- 2238
: 2241 2 2 Ça
: 2245 1 2  ra
: 2248 4 0 jou
: 2254 1 0 te
: 2257 1 0  des
: 2261 2 0  cou
: 2264 5 0 leurs
- 2270
: 2272 1 -2 Aux
: 2277 2 -3  cou
: 2280 2 -5 leurs
: 2284 2 0  de
: 2288 2 0  l'arc
: 2292 1 0  en
: 2296 3 0  ciel
- 2303
: 2305 1 -3 Ça
: 2308 3 0  fait
: 2313 3 5  rire
: 2318 3 5  les
: 2324 2 5  oi
: 2328 5 5 seaux
- 2343
: 2352 6 9 Oh,
: 2360 7 7  oh,
: 2369 5 9  oh,
: 2377 4 5  rire
: 2383 2 5  les
: 2388 2 5  oi
: 2392 5 5 seaux
- 2413
: 2432 2 -3 Ça
: 2436 2 0  fait
: 2440 4 5  rire
: 2446 3 5  les
: 2452 2 5  oi
: 2456 5 5 seaux
- 2462
: 2464 2 5 Ça
: 2468 2 4  fait
: 2472 2 2  chan
: 2476 1 2 ter
: 2478 2 2  les
: 2482 1 2  a
: 2484 4 2 beilles
- 2497
: 2500 2 2 Ça
: 2504 4 0  chas
: 2510 1 0 se
: 2512 2 0  les
: 2516 2 0  nu
: 2520 6 0 ages
: 2528 1 -2  et
: 2532 2 -3  fait
: 2536 2 -5  bril
: 2540 2 0 ler
: 2544 1 0  le
: 2548 2 0  so
: 2552 3 0 leil
- 2559
: 2561 1 -3 Ça
: 2564 2 0  fait
: 2568 4 5  rire
: 2574 3 5  les
: 2580 2 5  oi
: 2584 6 5 seaux
: 2592 2 5  et
: 2596 2 4  dan
: 2600 1 2 ser
: 2603 1 2  les
: 2606 2 2  é
: 2610 1 2 cu
: 2612 3 2 reuils
- 2621
: 2624 2 2 Ça
: 2628 2 2  ra
: 2632 4 0 jou
: 2638 1 0 te
: 2640 2 0  des
: 2644 2 0  cou
: 2648 5 0 leurs
- 2654
: 2656 1 -2 Aux
: 2661 2 -3  cou
: 2664 2 -5 leurs
: 2668 2 0  de
: 2672 2 0  l'arc
: 2676 1 0  en
: 2680 2 0  ciel
- 2687
: 2689 1 -3 Ça
: 2692 2 0  fait
: 2697 3 5  rire
: 2702 3 5  les
: 2708 2 5  oi
: 2712 5 5 seaux
- 2727
: 2736 6 9 Oh,
: 2744 7 7  oh,
: 2753 5 9  oh,
: 2761 4 5  rire
: 2767 2 5  les
: 2772 2 5  oi
: 2776 6 5 seaux
- 2798
: 2816 2 -3 Ça
: 2820 2 0  fait
: 2824 4 5  rire
: 2830 3 5  les
: 2836 2 5  oi
: 2840 5 5 seaux
- 2855
: 2864 6 9 Oh,
: 2872 7 7  oh,
: 2881 5 9  oh,
: 2889 4 5  rire
: 2895 2 5  les
: 2900 2 5  oi
: 2904 3 5 seaux
E