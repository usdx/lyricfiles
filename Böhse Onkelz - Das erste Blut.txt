#TITLE:Das erste Blut
#ARTIST:Böhse Onkelz
#MP3:Böhse Onkelz - Das erste Blut.mp3
#COVER:Böhse Onkelz - Das erste Blut[CO].jpg
#BACKGROUND:Böhse Onkelz - Das erste Blut[BG].jpg
#BPM:198,18
#GAP:21600
#ENCODING:UTF8
#LANGUAGE:German
#GENRE:Metal
#EDITION:UltraStar Böhse Onkelz
#CREATOR:Eggman
: 0 3 57 Ich 
: 4 3 64 will 
: 8 3 64 nicht 
: 12 4 64 dei
: 16 3 62 ne 
: 20 4 64 Lie
: 24 6 64 be 
- 31
: 36 4 64 Nein 
: 41 3 64 mein 
: 45 3 64 Schö
: 48 4 66 nes 
: 53 8 61 Kind 
- 62
: 65 3 64 Du 
: 69 4 62 weißt, 
: 74 3 62 wo 
: 78 3 62 von 
: 82 3 62 ich 
: 86 4 62 re
: 90 4 62 de 
- 94
: 94 4 62 Wer
: 98 3 62 de 
: 102 4 64 mei
: 106 3 64 ne 
: 110 4 64 Kö
* 114 3 64 ni
* 117 4 64 gin 
- 121
: 123 4 61 Ich 
: 128 4 61 den
: 132 3 61 ke 
: 136 2 59 im
: 138 3 59 mer 
: 142 4 59 nur 
: 147 3 57 das 
: 151 4 59 ei
: 155 8 59 ne 
- 167
: 179 4 57 Wenn 
: 184 3 64 wir 
: 188 3 64 zu
: 191 4 64 sam
: 195 3 66 men 
: 199 10 61 sind 
- 220
: 268 4 64 Das 
: 273 2 64 er
: 275 3 62 ste 
: 279 13 64 Blut 
- 296
: 301 3 60 Dein 
: 305 3 60 er
: 308 3 58 stes 
: 312 11 60 Blut 
- 323
: 324 4 62 Ich 
: 329 3 62 kann 
: 333 3 62 nicht 
* 337 3 62 län
* 340 3 60 ger 
: 344 4 62 war
: 348 5 64 ten 
- 354
: 355 4 64 Schenk 
: 360 3 64 mir 
: 364 4 64 Dein 
: 369 3 64 ers
: 372 4 66 tes 
: 377 9 61 Blut 
- 386
: 397 4 64 Das 
: 402 2 64 er
: 404 3 62 ste 
: 408 13 64 Blut 
- 423
: 430 3 60 Dein 
: 434 3 60 er
: 437 3 58 stes 
: 441 11 60 Blut 
- 452
: 453 4 62 Ich 
: 458 3 62 kann 
: 462 3 62 nicht 
* 466 3 62 län
* 469 3 60 ger 
: 473 4 62 war
: 477 5 64 ten 
- 483
: 484 4 64 Schenk 
: 489 3 64 mir 
: 493 4 64 Dein 
: 498 3 64 ers
: 501 4 66 tes 
: 506 9 61 Blut 
- 524
: 574 3 64 Nur 
: 578 3 64 ei
: 581 3 62 ne 
: 585 3 64 Nacht 
: 589 3 60 in 
: 593 4 60 mei
: 597 2 58 nen 
* 601 3 60 Ar
* 604 4 56 men
- 609
: 613 2 62 Ei
: 615 1 62 ne 
: 617 3 62 Nacht 
: 621 2 62 wir 
: 625 3 62 zwei 
: 629 2 62 all
: 631 8 64 ein 
- 643
: 645 3 64 Das 
: 649 4 64 En
: 653 3 64 de 
: 657 3 64 Dei
: 660 3 61 ner 
: 664 3 61 Un
: 667 6 57 schuld 
- 674
: 675 4 64 Nichts 
: 680 4 64 wird 
: 685 2 64 wie 
: 688 2 60 frü
: 690 3 62 her 
: 694 7 58 sein 
- 702
: 711 3 64 Tag 
: 715 3 61 und 
* 719 4 61 Nacht 
: 724 3 61 Tag
: 727 4 61 ein, 
: 732 4 61 Tag
: 736 7 61 aus 
- 746
: 754 4 60 Ein 
: 759 4 62 sinn
: 763 4 60 lich
: 767 3 60 es 
: 771 4 58 Ver
: 775 5 56 gnü
: 780 5 58 gen 
- 790
: 843 4 64 Das 
: 848 2 64 er
: 850 3 62 ste 
: 854 13 64 Blut 
- 868
: 874 3 60 Dein 
: 878 3 60 er
: 881 3 58 stes 
: 885 11 60 Blut 
- 896
: 897 4 62 Ich 
: 902 3 62 kann 
: 906 3 62 nicht 
* 910 3 62 län
* 913 3 62 ger 
: 917 4 62 war
: 921 5 64 ten 
- 927
: 928 4 64 Schenk 
: 933 3 64 mir 
: 937 4 64 Dein 
: 942 3 64 ers
: 945 4 66 tes 
: 950 9 61 Blut 
- 960
: 970 4 64 Das 
: 975 2 64 er
: 977 3 62 ste 
: 981 13 64 Blut 
- 995
: 1003 3 60 Dein 
: 1007 3 60 er
: 1010 3 58 stes 
: 1014 11 60 Blut 
- 1025
: 1025 4 62 Ich 
: 1030 3 62 kann 
: 1034 3 62 nicht 
* 1038 3 62 län
* 1041 3 60 ger 
: 1045 4 62 war
: 1049 5 64 ten 
- 1056
: 1057 4 64 Schenk 
: 1062 3 64 mir 
: 1066 4 64 Dein 
: 1071 3 64 ers
: 1074 4 66 tes 
: 1079 9 61 Blut
- 1090
: 1481 4 64 Das 
: 1486 2 64 er
: 1488 3 62 ste 
: 1492 13 64 Blut 
- 1507
: 1515 3 60 Dein 
: 1519 3 60 er
: 1522 3 58 stes 
: 1526 11 60 Blut 
- 1537
: 1538 4 62 Ich 
: 1542 3 62 kann 
: 1546 3 62 nicht 
* 1550 3 62 län
* 1554 3 62 ger 
: 1557 4 62 war
: 1561 5 64 ten 
- 1566
: 1567 4 64 Schenk 
: 1572 3 64 mir 
: 1576 4 64 Dein 
: 1581 3 64 ers
: 1584 4 66 tes 
: 1589 9 61 Blut 
- 1605
: 1609 4 64 Das 
: 1614 2 64 er
: 1616 3 62 ste 
: 1620 13 64 Blut 
- 1638
: 1642 3 60 Dein 
: 1646 3 60 er
: 1649 3 58 stes 
: 1653 11 60 Blut 
- 1664
: 1664 4 62 Ich 
: 1669 3 62 kann 
: 1673 3 62 nicht 
* 1677 3 62 län
* 1680 3 60 ger 
: 1684 4 62 war
: 1688 5 64 ten 
- 1694
: 1696 4 64 Schenk 
: 1701 3 64 mir 
: 1705 4 64 Dein 
: 1710 3 64 ers
: 1713 4 66 tes 
: 1718 9 61 Blut
- 1729
: 1793 3 57 Ich 
: 1797 3 64 will 
: 1801 3 64 nicht 
: 1805 4 64 dei
: 1809 3 62 ne 
: 1813 4 64 Lie
: 1817 6 64 be 
- 1824
: 1827 4 64 Nein 
: 1832 3 64 mein 
: 1836 3 64 Schö
: 1839 4 66 nes 
: 1844 8 61 Kind 
- 1853
: 1856 3 64 Du 
: 1860 4 62 weißt, 
: 1865 3 62 wo 
: 1869 3 62 von 
: 1873 3 62 ich 
: 1877 4 62 re
: 1881 4 62 de 
- 1885
: 1885 4 62 Wer
: 1889 3 62 de 
: 1893 4 64 mei
: 1897 3 64 ne 
* 1901 4 64 Kö
* 1905 3 64 ni
: 1908 4 64 gin 
- 1912
: 1912 4 61 Ich 
: 1917 4 61 den
: 1921 3 61 ke 
: 1925 2 59 im
: 1927 3 59 mer 
: 1931 4 59 nur 
: 1936 3 57 das 
: 1940 4 59 ei
: 1944 8 59 ne 
- 1953
: 1967 4 57 Wenn 
: 1972 3 64 wir 
: 1976 3 64 zu
: 1979 4 64 sam
: 1983 3 66 men 
: 1987 10 61 sind 
- 2000
: 2054 4 64 Das 
: 2059 2 64 er
: 2061 3 62 ste 
: 2065 13 64 Blut 
- 2079
: 2086 3 60 Dein 
: 2090 3 60 er
: 2093 3 58 stes 
: 2097 10 60 Blut 
- 2107
: 2107 4 62 Ich 
: 2112 3 62 kann 
: 2116 3 62 nicht 
* 2120 3 62 län
* 2123 3 62 ger 
: 2127 4 62 war
: 2131 5 64 ten 
- 2137
: 2138 4 64 Schenk 
: 2143 3 64 mir 
: 2147 4 64 Dein 
: 2152 3 64 ers
: 2155 4 66 tes 
: 2160 9 61 Blut 
- 2170
: 2180 4 64 Das 
: 2185 2 64 er
: 2187 3 62 ste 
: 2191 13 64 Blut 
- 2205
: 2212 3 60 Dein 
: 2216 3 60 er
: 2219 3 58 stes 
: 2223 10 60 Blut 
- 2233
: 2233 4 62 Ich 
: 2238 3 62 kann 
: 2242 3 62 nicht 
* 2246 3 62 län
* 2249 3 60 ger 
: 2253 4 62 war
: 2257 5 64 ten 
- 2263
: 2264 4 64 Schenk 
: 2269 4 63 mir 
: 2274 4 64 Dein 
: 2279 3 64 ers
: 2282 4 66 tes 
: 2287 9 61 Blut
- 2297
: 2305 4 64 Das 
: 2310 2 64 er
: 2312 3 62 ste 
: 2316 13 64 Blut 
- 2330
: 2336 3 60 Dein 
: 2340 3 60 er
: 2343 3 58 stes 
: 2347 11 60 Blut 
- 2358
: 2359 4 62 Ich 
: 2364 3 62 kann 
: 2368 3 62 nicht 
: 2372 3 62 län
: 2375 3 60 ger 
: 2379 4 62 war
: 2383 5 64 ten 
- 2389
: 2390 4 64 Schenk 
: 2395 3 64 mir 
: 2399 4 64 Dein 
: 2404 3 64 ers
: 2407 4 66 tes 
: 2412 9 61 Blut
- 2423
: 2431 4 64 Das 
: 2436 2 64 er
: 2438 3 62 ste 
: 2442 13 64 Blut 
- 2456
: 2462 3 60 Dein 
: 2466 3 60 er
: 2469 3 58 stes 
: 2473 11 60 Blut 
- 2484
: 2485 4 62 Ich 
: 2490 3 62 kann 
: 2494 3 62 nicht 
: 2498 3 62 län
: 2501 3 60 ger 
: 2505 4 62 war
: 2509 5 64 ten 
- 2515
: 2516 4 62 Ich 
: 2521 3 62 kann 
: 2525 3 62 nicht 
: 2529 3 62 län
: 2532 3 60 ger 
: 2536 4 62 war
: 2540 5 64 ten
- 2546
: 2547 4 62 Ich 
: 2552 3 62 kann 
: 2556 3 62 nicht 
: 2560 3 62 län
: 2563 3 60 ger 
: 2567 4 62 war
: 2571 5 64 ten
- 2576
: 2576 3 64 Auf 
: 2580 3 64 dein 
* 2584 24 61 Blut
E