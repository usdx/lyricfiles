#TITLE:Dr. Stein
#ARTIST:Helloween
#MP3:Helloween - Dr. Stein.mp3
#COVER:Helloween - Dr. Stein [CO].jpg
#BACKGROUND:Helloween - Dr. Stein [BG].jpg
#BPM:149,03
#GAP:38300
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Metal
#EDITION:Renegade
: 0 2 19 Once 
: 2 4 19 they 
: 6 4 19 killed 
: 10 4 19 his 
: 14 4 18 mon
: 18 4 16 ster
- 22
: 22 4 16 when 
: 26 6 16 he 
: 32 4 15 went 
: 36 4 15 in
: 40 2 16 to 
: 42 2 18 a
: 44 4 16 - 
: 48 4 19 trap
- 53
: 64 2 19 Now 
: 66 4 19 he's 
: 70 4 19 ma
: 74 4 19 kin' 
: 78 4 18 bet
: 82 4 16 ter 
: 86 6 16 ones
- 92
: 96 2 15 on 
: 98 6 15 a 
: 104 2 23 high
: 106 6 15 er 
: 112 4 16 step
- 117
: 122 2 11 O
: 124 2 12 n 
: 126 2 9 a 
: 128 4 11 wa
: 132 4 6 rm 
: 136 2 23 sum
: 138 4 18 mer-
: 142 10 19 day
- 152
: 156 4 19 the 
: 160 4 21 doc
: 164 4 21 tor 
: 168 4 26 went 
: 172 4 21 a
: 176 8 23 way
- 184
: 186 2 23 to 
: 188 2 21 a 
: 190 6 24 place 
: 196 4 24 where 
: 200 4 26 he 
: 204 4 24 could 
: 208 2 23 ma
: 210 6 19 ke 
: 216 4 23 it 
: 220 8 23 re
: 228 16 21 al
- 244
: 252 2 12 his 
: 254 2 9 as
: 256 4 11 sis
: 260 4 11 tant's 
: 264 2 23 hips 
: 266 4 18 were 
: 270 6 19 nice
- 277
: 282 2 19 so 
: 284 2 19 he 
: 286 6 21 cloned 
: 292 4 21 her 
: 296 4 26 once 
: 300 4 21 or 
: 304 8 23 twice
- 312
: 316 4 18 now his 
: 320 4 16 hips 
: 324 4 16 are 
: 328 4 19 ach
: 332 4 19 ing, 
: 336 4 21 wh
: 340 4 18 at 
: 344 4 18 a 
: 348 4 18 de
: 352 8 19 al
- 361
: 384 4 16 Doc
: 388 4 12 tor 
: 392 4 18 Stein 
: 396 4 21 grows 
: 400 4 19 fun
: 404 4 18 ny 
: 408 4 16 crea
: 412 4 14 tures, 
- 416
: 416 4 16 let 
: 420 4 12 them 
: 424 4 18 run 
: 430 2 14 in
: 432 4 23 to 
: 436 4 26 the 
: 440 4 23 night!
- 444
: 448 4 16 They 
: 452 4 12 be
: 456 4 18 come 
: 460 4 21 great 
: 464 4 19 rock 
: 468 4 18 mu
: 472 4 16 si
: 476 4 14 cians
- 480
: 480 8 16 and 
: 488 8 19 their 
: 496 6 21 ti
: 502 2 18 me 
: 504 6 18 i
: 510 2 14 s 
: 512 4 16 right
- 516
: 536 4 21 Time 
: 540 4 21 is 
: 544 2 21 ri
: 546 10 23 ght!
- 558
: 640 2 19 Some
: 642 4 19 times 
: 646 4 19 when 
: 650 4 19 he's 
: 654 4 18 fee
: 658 4 16 ling 
: 662 4 16 bored, 
- 666
: 666 6 16 he's 
: 672 2 14 cal
: 674 6 14 lin 
: 680 2 16 it 
: 682 2 18 a
: 684 2 16 - 
: 686 4 18 da
: 690 2 19 y
- 692
: 704 2 19 he's 
: 706 4 19 got 
: 710 4 19 his 
: 714 4 19 com
: 718 4 18 pu
: 722 4 16 ters 
: 726 2 16 and 
: 728 4 15 they 
- 732
: 736 2 16 do 
: 738 6 14 it 
: 744 2 23 their 
: 746 4 14 own 
: 750 10 16 way
- 760
: 764 4 9 They 
: 768 4 11 mix 
: 772 4 11 some 
: 776 2 23 D-
: 778 4 18 N-
: 782 10 19 A
- 792
: 796 4 19 Some 
: 800 4 21 skin 
: 804 2 21 and 
: 806 2 21 a 
: 808 2 26 cer
: 810 4 21 tain 
: 814 10 23 spray
- 824
: 826 2 23 You 
: 828 5 23 can watch 
: 836 4 24 it 
: 840 4 26 on 
: 844 4 24 a 
: 848 2 23 la
: 850 6 19 ~ 
: 856 4 23 ser-
: 860 8 23 scre
: 868 16 21 en
- 884
: 892 2 12 And 
: 894 2 9 the 
: 896 2 11 fel
: 898 8 11 lows 
: 906 2 23 blue 
: 908 4 18 and 
: 912 8 19 grey
- 920
: 926 2 19 or 
: 928 4 21 some
: 932 4 21 times 
: 936 4 26 pink 
: 940 4 21 and 
: 944 8 23 green
- 952
: 956 4 19 Just 
: 960 4 16 check 
: 964 4 16 it 
: 968 4 19 out 
: 972 4 19 on 
: 976 2 18 Hal
: 978 6 14 lo
: 984 4 18 we
: 988 4 18 e
: 992 0 19 n
- 995
: 1008 4 16 Doc
: 1012 4 12 tor 
: 1016 4 18 Stein 
: 1020 4 21 grows 
: 1024 4 19 fun
: 1028 4 18 ny 
: 1032 4 16 crea
: 1036 4 14 tures
- 1040
: 1040 4 16 let 
: 1044 4 12 them 
: 1048 4 18 run 
: 1054 2 14 in
: 1056 4 23 to 
: 1060 4 26 the 
: 1064 4 23 night!
- 1068
: 1072 4 16 They 
: 1076 4 12 be
: 1080 4 18 come 
: 1084 4 21 great 
: 1088 4 19 po
: 1092 4 18 li
: 1096 4 16 ti
: 1100 4 14 cians
- 1104
: 1104 8 16 and 
: 1112 8 19 their 
: 1120 6 21 time 
: 1126 2 18 i
: 1128 6 18 s 
: 1134 2 14 ri
: 1136 4 16 ght
- 1200
: 1856 4 11 One night 
: 1860 4 6 he 
: 1864 2 23 cloned 
: 1866 4 18 him
: 1870 10 19 self
- 1880
: 1884 4 19 put 
: 1888 4 21 his 
: 1892 4 21 brother 
: 1896 4 26 on 
: 1900 4 21 a 
: 1904 8 23 shelf
- 1912
: 1914 2 23 bu
: 1916 2 21 t 
: 1918 6 24 when 
: 1924 4 24 he 
: 1928 4 26 fell 
: 1932 4 24 as
: 1936 2 23 le
: 1938 6 19 ep 
: 1944 4 23 that 
: 1948 8 23 ni
: 1956 16 21 ght
- 1972
: 1980 2 12 he
: 1982 2 9 ~
: 1984 4 11 crept 
: 1988 4 11 up 
: 1992 2 23 from 
: 1994 4 18 be
: 1998 6 19 hind
- 2004
: 2010 2 19 and
: 2012 2 19 ~ 
: 2014 6 21 thought 
: 2020 4 21 "well, 
: 2024 4 26 ne
: 2028 4 21 ver
: 2032 8 23 mind!"
- 2040
: 2040 4 19 took 
: 2044 4 18 a 
: 2048 4 16 sy
: 2052 4 16 rienge 
: 2056 4 19 and 
: 2060 4 19 blew 
: 2064 4 21 ou
: 2068 4 18 t 
: 2072 4 18 his 
: 2076 4 18 li
: 2080 8 19 -fe
- 2088
: 2112 4 16 Doc
: 2116 4 12 tor 
: 2120 4 18 Stein 
: 2124 4 21 grows 
: 2128 4 19 fun
: 2132 4 18 ny 
: 2136 4 16 crea
: 2140 4 14 tures
- 2144
: 2144 4 16 Let 
: 2148 4 12 them 
: 2152 4 18 run 
: 2158 2 14 in
: 2160 4 23 to 
: 2164 4 26 the 
: 2168 4 23 night
- 2172
: 2176 4 16 They 
: 2180 4 12 be
: 2184 4 18 come 
: 2188 4 21 a 
: 2192 4 19 great 
: 2196 4 18 po
: 2200 4 16 sess
: 2204 4 14 ion
- 2208
: 2208 8 16 and 
: 2216 8 19 their 
: 2224 6 21 time 
: 2230 2 18 i
: 2232 6 18 s 
: 2238 2 14 right
- 2240
: 2240 4 16 Doc
: 2244 4 12 tor 
: 2248 4 18 Stein 
: 2252 4 21 grows 
: 2258 2 19 fun
: 2260 4 18 ny 
: 2264 4 16 crea
: 2268 4 14 tures
- 2272
: 2272 4 16 Let 
: 2276 4 12 them 
: 2280 4 18 run 
: 2286 2 14 in
: 2288 2 21 to 
: 2290 4 26 the 
: 2294 2 26 ni
: 2296 8 23 ght
- 2304
: 2304 4 16 they 
: 2308 4 12 be
: 2312 4 18 come 
: 2316 4 21 a 
: 2320 4 19 great 
: 2324 4 18 o
: 2328 4 16 pre
: 2332 4 14 ssion
- 2336
: 2336 8 16 and 
: 2344 8 19 their 
: 2352 6 21 time 
: 2358 2 18 i
: 2360 6 18 s 
: 2366 2 14 ri
: 2368 4 16 ght
- 2372
: 2392 4 18 time 
: 2396 4 18 is 
: 2400 20 19 right
- 2420
: 2424 4 21 time 
: 2428 4 21 is 
: 2432 20 23 right 
- 2452
: 2456 4 26 time 
: 2460 4 26 is 
: 2464 36 28 right 
E