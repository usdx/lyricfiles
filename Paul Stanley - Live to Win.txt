#TITLE:Live to Win
#ARTIST:Paul Stanley
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
#MP3:Paul Stanley - Live to Win.mp3
#COVER:Paul Stanley - Live to Win [CO].jpg
#BACKGROUND:Paul Stanley - Live to Win [BG].jpg
#VIDEO:Paul Stanley - Live to Win [VD#0].avi
#BPM:297,5
#GAP:16750
: 0 5 7 Frus
: 12 5 5 trat
: 20 4 0 ed
: 33 10 7  de
: 48 5 5 grad
: 56 5 0 ed
- 63
: 70 6 7  down
: 79 4 5  be
: 85 4 7 fore
: 95 4 8  you're
: 104 7 5  done
- 113
: 146 7 7 Re
: 160 6 5 jec
: 170 4 0 tion
: 184 8 7  de
: 197 5 5 pres
: 205 5 0 sion
- 212
: 220 6 7  can't
: 229 3 5  get
: 234 5 7  what
: 244 5 8  you
* 254 11 12  want
- 267
: 272 3 5 You
: 276 3 12  ask
: 281 2 12  me
: 285 4 13  how
: 291 3 8  I
: 297 8 10  make
: 309 8 8  my
: 324 11 12  way
- 337
: 347 2 5 You
: 351 3 12  ask
: 356 3 12  me
: 361 3 13  ev
: 365 2 8 ery
: 370 10 10 where
: 383 10 8  and
: 398 9 5  why
- 409
: 421 3 5 You
: 426 3 12  hang
: 431 3 12  on
: 436 3 13  ev
: 441 2 8 ery
: 446 8 10  word
: 459 6 8  I
: 473 7 12  say
- 482
: 510 3 10 But
: 515 2 8  the
: 520 5 10  truth
: 529 4 10  sounds
: 535 5 10  like
: 544 5 12  a
: 554 8 10  lie
- 564
: 575 6 20 Live
: 586 5 19  to
: 596 8 17  win
- 606
: 619 3 20  'till
: 624 3 19  you
: 630 10 17  die
- 642
: 655 4 20  'till
: 661 3 19  the
: 666 10 17  light
: 680 4 15  dies
: 686 5 17  in
: 694 6 19  your
: 703 6 17  ey
: 711 2 15 ~
: 714 5 12 ~es
- 721
: 726 5 20 Live
: 735 5 19  to
: 745 7 17  win
- 754
: 768 4 20 take
: 774 3 19  it
: 779 12 17  all
- 793
: 806 3 20  just
: 811 3 19  keep
* 816 11 17  fight
: 830 2 15 ing
: 835 4 17  till
: 843 5 19  you
: 852 5 17  fa
: 859 3 15 ~
: 863 4 12 ~ll
- 869
: 896 7 7 Ob
: 909 6 5 ses
: 919 5 0 sive
: 932 8 7  com
: 947 4 5 pul
: 957 5 0 sive
- 964
: 970 5 7 suf
: 979 2 5 fo
: 984 4 7 cate
: 993 5 8  your
: 1003 9 5  mind
- 1014
: 1046 7 7 Con
: 1060 5 5 fu
: 1070 4 0 sion
: 1083 8 7  de
: 1097 5 5 lu
: 1106 4 0 sions
- 1112
: 1120 5 7 kill
: 1128 3 5  your
: 1134 6 7  dreams
: 1144 4 8  in
* 1154 11 12  time
- 1167
: 1171 3 5 You
: 1176 3 12  ask
: 1181 3 12  me
: 1185 3 13  how
: 1190 3 8  I
: 1195 7 10  took
: 1208 5 8  the
: 1222 13 12  pain
- 1237
: 1250 3 12 Crawled
: 1255 3 12  up
: 1260 3 13  from
: 1264 3 8  my
: 1270 7 10  low
: 1283 7 8 est
: 1297 8 5  low
- 1307
: 1325 3 12 Step
: 1330 3 13  by
: 1335 3 12  step
: 1340 2 8  and
: 1345 8 10  day
: 1358 9 8  by
: 1373 13 12  day
- 1388
: 1410 3 17 'Till
: 1415 3 20  there's
: 1420 4 22  one
: 1429 3 22  last
: 1435 4 22  breath
: 1444 4 20  to
: 1454 11 20  go
- 1467
* 1475 6 20 Live
: 1485 5 19  to
: 1494 11 17  win
- 1507
: 1518 3 20 'till
: 1523 3 19  you
: 1529 11 17  die
- 1542
: 1555 3 20 'till
: 1560 3 19  the
: 1566 9 17  light
: 1579 3 15  dies
: 1585 4 17  in
: 1594 5 19  your
: 1603 7 17  ey
: 1611 2 15 ~
: 1614 4 12 ~es
- 1620
: 1626 5 20 Live
: 1636 4 19  to
: 1645 9 17  win
- 1656
: 1669 3 20 take
: 1674 3 19  it
* 1679 11 17  all
- 1692
: 1705 3 20 just
: 1710 3 19  keep
* 1715 11 17  fight
: 1728 3 15 ing
: 1734 4 17  till
: 1743 4 19  you
: 1753 6 17  fa
: 1760 3 15 ~
: 1764 5 12 ~ll
- 1771
: 1793 6 20 Day
: 1804 5 20  by
: 1814 5 20  day
: 1828 5 19  ki
: 1836 3 19 ckin'
: 1842 3 19  all
: 1847 3 17  the
: 1852 5 19  way
- 1859
: 1870 5 17  I'm
: 1880 4 17  not
: 1888 6 17  cav
: 1897 4 19 in'
: 1904 9 15 in
- 1915
: 1935 3 12 Let
: 1940 2 15  an
: 1944 6 17 oth
: 1954 2 17 er
: 1959 4 17  rou
: 1964 2 15 ~nd
: 1969 4 15  be
: 1978 8 15 gin
- 1988
: 2000 5 20 Live
: 2010 4 19  to
: 2020 8 17  win
- 2030
* 2039 13 20 Yeah
- 2054
: 2076 15 20 Live
- 2093
: 2115 13 20 Yeah
- 2130
: 2151 15 20 Win
- 2168
: 2451 5 20 Live
: 2460 4 19  to
* 2470 9 17  win
- 2481
: 2492 3 20 'till
: 2498 3 19  you
: 2503 12 17  die
- 2517
: 2531 3 20 'till
: 2536 3 19  the
: 2541 9 17  light
: 2553 4 15  dies
: 2559 5 17  in
: 2568 4 19  your
: 2577 7 17  ey
: 2585 2 15 ~
: 2588 5 12 ~es
- 2595
: 2602 4 20 Live
: 2611 4 19  to
: 2620 8 17  win
- 2630
: 2644 2 20 take
: 2648 4 19  it
: 2654 9 17  all
- 2665
: 2680 3 20 just
: 2685 3 19  keep
* 2690 10 17  fight
: 2703 3 15 ing
: 2708 4 17  'till
: 2718 4 19  you
: 2728 5 17  fa
: 2734 2 15 ~
: 2737 5 12 ~ll
- 2744
: 2768 6 20 Day
: 2779 5 20  by
: 2787 6 20  day
: 2802 6 19  ki
: 2812 3 19 ckin'
: 2816 3 19  all
: 2822 3 17  the
: 2827 5 19  way
- 2834
: 2843 6 17  I'm
: 2854 4 17  not
: 2863 6 17  cav
: 2873 4 19 in'
: 2878 9 15 in
- 2889
: 2910 3 12 Let
: 2915 3 15  an
: 2919 5 17 oth
: 2928 3 17 er
: 2933 4 17  rou
: 2938 2 15 ~nd
: 2942 5 15  be
: 2951 9 15 gin
- 2962
: 2975 6 20 Live
: 2984 5 19  to
: 2994 9 17  win
- 3005
: 3049 6 20 Live
: 3059 5 22  to
: 3068 12 17  win
- 3082
: 3124 6 24 Live
: 3134 3 22  to
: 3138 2 20 ~
* 3143 9 22  wi
: 3154 11 20 ~n
- 3167
: 3238 12 20 Yeah
- 3252
* 3276 15 20 Live
- 3293
: 3313 14 20 Yeah
- 3329
* 3351 17 20 Win
E
