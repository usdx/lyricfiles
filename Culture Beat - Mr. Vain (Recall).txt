#TITLE:Mr. Vain (Recall)
#ARTIST:Culture Beat
#MP3:Culture Beat - Mr. Vain (Recall).mp3
#VIDEO:Culture Beat - Mr. Vain (Recall).mp4
#COVER:Culture Beat - Mr. Vain (Recall)[CO].jpg
#BPM:279,62
#GAP:6960
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Pop
#YEAR:2003
: 0 2 4 Call
: 4 2 4  him
: 8 2 2  Mis
: 12 2 -3 ter
* 16 2 0  Rai
* 20 2 -3 der,
- 25
: 28 4 4 call
: 36 2 4  him
: 40 2 2  Mis
: 44 2 -3 ter
: 48 5 0  Wrong,
- 56
: 64 2 4 call
: 68 2 4  him
: 72 2 2  Mis
: 76 2 -3 ter
: 80 6 0  Vain.
- 89
: 128 2 4 Call
: 132 2 4  him
: 136 2 2  Mis
: 140 2 -3 ter
: 144 2 0  Rai
: 148 2 -3 der,
- 153
: 156 4 4 call
: 164 2 4  him
: 168 2 2  Mis
: 172 2 -3 ter
: 176 5 0  Wrong,
- 184
: 192 2 4 call
: 196 2 4  him
: 200 4 2  in
* 208 6 0 sane.
- 217
: 244 2 0 He'd
: 248 2 4  say:
: 256 4 4  I
: 264 4 7  know
: 272 1 4  what
: 274 2 7  I
: 278 3 9  want
- 284
: 284 2 7 and
: 288 4 7  I
: 296 2 7  want
: 300 2 2  it
: 304 5 4  now.
- 312
: 320 4 2 I
: 328 4 4  want
: 336 4 -3  you,
- 343
* 348 2 -3 cause
: 352 4 2  I'm
: 360 2 0  Mis
: 364 2 -3 ter
: 368 6 0  Vain.
- 377
: 384 4 4 I
: 392 4 7  know
: 400 1 4  what
: 402 2 4  I
: 406 4 9  want
: 412 2 7  and
: 416 4 7  I
: 424 2 7  want
: 428 2 2  it
: 432 6 4  now,
- 441
: 448 4 2 I
: 456 4 4  want
: 464 4 -3  you,
- 471
: 476 2 -3 cause
: 480 4 2  I'm
: 488 2 0  Mis
: 492 2 -3 ter
: 496 6 0  Vain.
- 505
: 512 2 4 Call
: 516 2 4  him
: 520 2 2  Mis
: 524 2 -3 ter
: 528 2 0  Rai
: 532 2 -3 der,
- 537
: 540 4 4 call
: 548 2 4  him
: 552 2 2  Mis
: 556 2 -3 ter
: 560 5 0  Wrong,
- 568
: 576 2 4 call
: 580 2 4  him
: 584 2 2  Mis
: 588 2 -3 ter
: 592 6 0  Vain.
- 601
: 638 2 4 Call
: 642 2 4  him
: 646 2 2  Mis
: 650 2 -3 ter
: 654 2 0  Rai
: 658 2 -3 der,
- 663
: 668 4 4 call
: 676 2 4  him
: 680 2 2  Mis
: 684 2 -3 ter
: 688 5 0  Wrong,
- 696
: 704 2 4 call
: 708 2 4  him
* 712 5 -3  in
* 720 6 0 sane.
- 729
: 756 2 4 He'd
: 760 2 0  say:
: 768 4 4  I
: 776 4 7  know
: 784 1 4  what
: 786 2 7  I
: 790 3 9  want
- 796
: 796 2 7 and
: 800 4 7  I
: 808 2 7  want
: 812 2 2  it
: 816 6 4  now,
- 825
: 832 4 0 I
: 840 4 4  want
: 848 4 -3  you,
- 855
: 860 2 -3 cause
: 864 4 4  I'm
: 872 2 2  Mis
: 876 2 -3 ter
: 880 6 0  Vain.
- 889
: 894 4 4 I
: 902 4 7  know
: 910 1 4  what
: 912 2 4  I
* 916 4 9  want
: 922 2 7  and
: 926 5 7  I
: 934 2 7  want
: 938 2 2  it
: 942 6 4  now,
- 951
: 958 4 2 I
: 966 4 4  want
* 974 4 -3  you,
- 981
: 986 2 -3 cause
: 990 4 4  I'm
: 998 2 0  Mis
: 1002 2 -3 ter
: 1006 6 0  Vain.
- 1100
: 1150 2 3 Call
: 1154 1 3  me
: 1156 2 3  Rai
: 1160 1 3 der,
: 1162 2 3  call
: 1166 2 3  me
: 1170 2 3  Wrong,
- 1174
: 1174 2 3 call
: 1178 1 3  me
: 1180 4 3  in
: 1186 4 3 sane,
: 1194 1 3  call
: 1196 1 3  me
: 1198 2 3  Mis
: 1202 2 3 ter
: 1206 4 3  Vain.
- 1212
: 1214 2 3 Call
: 1218 2 3  me
: 1222 1 3  what
: 1224 1 3  you
: 1226 3 3  like
- 1231
: 1234 1 3 as
: 1236 2 3  long
: 1240 2 3  as
: 1244 2 3  you
: 1248 4 3  call
: 1254 2 3  me,
: 1260 2 3  time
: 1264 2 3  and
: 1268 2 3  a
: 1272 2 3 gain.
- 1276
: 1280 2 3 Feel
: 1284 1 3  the
: 1286 1 3  pre
: 1288 2 3 sence
: 1292 1 3  of
: 1294 1 3  the
: 1296 2 3  au
: 1300 2 3 ra
- 1304
: 1304 2 3 of
: 1308 2 3  the
: 1312 4 3  man,
: 1324 2 3  none
: 1328 2 3  to
: 1332 2 3  com
: 1336 2 3 pare.
- 1340
: 1342 2 3 Love
: 1346 1 3 less
: 1348 2 3  dy
: 1352 2 3 ing,
- 1356
: 1356 2 3 for
: 1360 1 3  a
: 1362 2 3  chance,
: 1366 2 3  just
: 1370 1 3  to
: 1372 2 3  touch
: 1376 1 3  a
: 1378 3 3  hand
- 1383
: 1384 2 3 or
: 1388 1 3  a
: 1390 1 3  mo
: 1392 2 3 ment
: 1396 1 3  to
: 1398 3 3  share.
- 1403
: 1408 2 3 Can't
: 1412 1 3  de
: 1414 1 3 ny
: 1416 1 3  the
: 1418 2 3  urge
: 1422 1 3  that
: 1424 2 3  makes
: 1428 2 3  them
- 1432
: 1432 2 3 want
: 1436 1 3  to
: 1438 2 3  lose
: 1442 2 3  them
: 1446 2 3 selves
- 1450
: 1452 1 3 to
: 1454 1 3  the
: 1456 2 3  de
: 1460 1 3 bon
: 1462 2 3 air
: 1466 2 3  one.
- 1470
: 1470 2 3 Hold
: 1474 1 3  me
: 1476 2 3  back,
- 1480
: 1480 1 3 the
: 1482 2 3  simp
: 1486 2 3 le
: 1490 2 3  fact
: 1494 1 3  is
: 1496 1 3  that
: 1498 2 3  I'm
: 1502 2 3  all
: 1506 3 3  that
- 1511
: 1514 2 3 and
: 1518 1 3  I'm
: 1520 2 3  al
: 1524 2 3 ways
: 1528 2 3  near
- 1532
: 1532 1 3 one
: 1534 1 3  se
: 1536 2 3 xy
: 1540 2 3  can't
: 1544 1 3  per
: 1546 2 3 plex
: 1550 1 3  me
: 1552 2 3  now.
- 1556
: 1558 1 3 You
: 1560 2 3  know
: 1564 2 3  who'
: 1568 3 3  raw
- 1573
: 1576 1 3 as
: 1578 2 3  if
: 1582 1 3  did
: 1584 2 3 n't
: 1588 2 3  know
: 1592 1 3  be
: 1594 2 3 fore.
- 1598
: 1598 1 3 I
: 1600 2 3  know
: 1604 1 3  what
: 1606 2 3  I
: 1608 2 3  want
: 1612 1 3  and
: 1614 1 3  I
: 1616 1 3  want
: 1618 1 3  it
: 1620 2 3  now.
- 1624
: 1624 1 3 I
: 1626 2 3  want
: 1630 4 3  you,
- 1638
: 1642 2 3 then
: 1646 1 3  I
: 1648 2 3  want
: 1652 1 3  a
: 1654 1 3  lit
: 1656 1 3 tle
: 1658 4 3  more.
- 1870
: 1920 2 4 Call
: 1924 2 4  him
* 1928 2 2  Mis
* 1932 2 -3 ter
: 1936 2 0  Rai
: 1940 2 -3 der,
- 1945
: 1948 4 4 call
: 1956 2 4  him
: 1960 2 2  Mis
: 1964 2 -3 ter
: 1968 5 0  Wrong,
- 1976
: 1984 2 4 call
: 1988 2 4  him
: 1992 2 2  Mis
: 1996 2 -3 ter
: 2000 6 0  Vain.
- 2009
: 2046 2 4 Call
: 2050 2 4  him
: 2054 2 2  Mis
: 2058 2 -3 ter
* 2062 2 0  Rai
* 2066 2 -3 der,
- 2071
: 2076 4 4 call
: 2084 2 4  him
: 2088 2 2  Mis
: 2092 2 -3 ter
: 2096 5 0  Wrong,
- 2104
: 2112 2 4 call
: 2116 2 4  him
: 2120 4 -3  in
: 2128 6 0 sane.
- 2137
: 2164 2 4 He'd
: 2168 2 0  say:
* 2176 4 4  I
: 2184 4 7  know
: 2192 1 4  what
: 2194 2 7  I
: 2198 3 9  want
- 2204
: 2204 2 7 and
: 2208 4 7  I
: 2216 2 7  want
: 2220 2 2  it
: 2224 5 4  now,
- 2232
: 2240 4 2 I
* 2248 4 4  want
: 2256 4 -3  you,
- 2263
: 2268 2 -3 cause
: 2272 4 4  I'm
: 2280 2 0  Mis
: 2284 2 -3 ter
: 2288 6 0  Vain.
- 2297
: 2304 4 4 I
: 2312 4 7  know
: 2320 1 4  what
: 2322 2 4  I
: 2326 4 9  want
: 2332 2 7  and
: 2336 4 7  I
: 2344 2 7  want
: 2348 2 2  it
: 2352 6 4  now,
- 2361
: 2368 4 2 I
: 2376 4 4  want
* 2384 4 -3  you,
- 2391
: 2396 2 -3 cause
: 2400 4 4  I'm
: 2408 2 0  Mis
: 2412 2 -3 ter
: 2416 6 0  Vain.
- 2606
: 2656 2 3 Please,
: 2660 2 3  oh
: 2664 2 3  please,
: 2668 2 3  oh
: 2672 2 3  please,
: 2676 2 3  oh
: 2680 2 3  please,
- 2684
: 2684 2 3 oh
: 2688 2 3  please,
: 2692 2 3  oh,
: 2696 2 3  ba
: 2700 2 3 by
: 2704 2 3  please,
- 2708
: 2708 2 3 you
: 2712 2 3  beg
: 2716 2 3  you
: 2720 2 3  want
: 2724 2 3  to
: 2728 2 3  stay.
- 2732
: 2732 2 3 You
: 2736 2 3  got
: 2740 2 3  to
: 2744 2 3  get
: 2748 2 3  some
: 2752 2 3  caught
: 2756 2 3  up
- 2760
: 2760 1 3 in
: 2762 1 3  the
: 2764 2 3  charm
: 2772 1 3  that
: 2774 1 3  I
: 2776 2 3  laid
: 2780 2 3  on
: 2784 2 3  thick
- 2788
: 2788 1 3 and
: 2790 2 3  now
: 2794 2 3  there's
: 2798 2 3  no
: 2802 2 3 where
: 2806 2 3  to
: 2810 2 3  run.
- 2814
: 2814 2 3 Just
: 2818 1 3  a
: 2820 2 3 no
: 2824 2 3 ther
: 2828 2 3  fish
: 2832 1 3  to
: 2834 2 3  fit
: 2838 1 3  the
: 2840 2 3  worm
- 2844
: 2844 1 3 on
: 2846 1 3  the
: 2848 2 3  hook
: 2852 1 3  of
: 2854 1 3  my
: 2856 2 3  line,
: 2860 3 3  yeah.
- 2865
: 2866 2 3 I
: 2870 1 3  keep
: 2872 2 3  ma
: 2876 2 3 ny
: 2880 2 3  fe
: 2884 1 3 males,
- 2886
: 2886 2 3 lon
: 2890 1 3 ging
: 2892 2 3  for
: 2896 1 3  a
: 2898 2 3  chance
- 2902
: 2902 1 3 to
: 2904 2 3  win
: 2908 2 3  my
: 2912 2 3  heart
: 2916 2 3  with
: 2920 2 3  S
: 2924 2 3 E
: 2928 2 3 X
: 2932 2 3  and
: 2936 2 3  plen
: 2940 2 3 ty.
- 2948
: 2982 2 9 Mis
: 2986 2 4 ter
: 2990 4 9  Wro
: 2996 8 12 ~ng!
- 3007
: 3046 2 9 Mis
: 3050 2 9 ter
: 3054 2 7  Rai
: 3058 8 9 der!
- 3069
: 3110 2 9 Mis
: 3114 2 4 ter
* 3118 2 9  Wro
* 3122 8 12 ~ng!
- 3133
: 3166 14 14 Yea
: 3182 8 9 ~h!
- 3193
: 3200 2 4 Call
: 3204 2 4  him
: 3208 2 2  Mis
: 3212 2 -3 ter
: 3216 2 0  Rai
: 3220 2 -3 der,
- 3225
: 3228 5 4 call
: 3236 2 4  him
: 3240 2 2  Mis
: 3244 2 -3 ter
: 3248 5 0  Wrong,
- 3256
* 3264 2 4 call
* 3268 2 4  him
* 3272 2 2  Mis
* 3276 2 -3 ter
* 3280 6 0  Vain.
- 3289
: 3326 2 4 Call
: 3330 2 4  him
: 3334 2 2  Mis
: 3338 2 -3 ter
: 3342 2 0  Rai
: 3346 2 -3 der,
- 3351
: 3356 4 4 call
: 3364 2 4  him
: 3368 2 2  Mis
: 3372 2 -3 ter
: 3376 5 0  Wrong,
- 3384
: 3392 2 4 call
: 3396 2 4  him
* 3400 5 -3  in
: 3408 6 0 sane.
- 3417
: 3444 2 4 He'd
: 3448 2 0  say:
: 3456 4 4  I
: 3464 4 7  know
: 3472 1 4  what
: 3474 2 7  I
: 3478 3 9  want
- 3484
: 3484 2 7 and
: 3488 4 7  I
: 3496 2 7  want
: 3500 2 2  it
* 3504 5 4  now,
- 3512
: 3520 4 2 I
: 3528 4 4  want
: 3536 4 -3  you,
- 3543
: 3548 2 -3 cause
: 3552 4 2  I'm
: 3560 2 0  Mis
: 3564 2 -3 ter
: 3568 6 0  Vain.
- 3577
: 3584 4 4 I
: 3592 4 7  know
: 3600 1 4  what
: 3602 2 7  I
: 3606 4 9  want
: 3612 2 7  and
: 3616 4 7  I
: 3624 2 7  want
: 3628 2 2  it
: 3632 6 4  now,
- 3641
: 3648 4 2 I
: 3656 4 4  want
: 3664 4 -3  you,
- 3673
: 3676 2 -3 cause
: 3680 4 4  I'm
: 3688 2 0  Mis
: 3692 2 -3 ter
* 3696 6 0  Vain.
E