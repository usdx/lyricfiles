#TITLE:Le Diable Au Corps
#ARTIST:Clara Morgane
#MP3:Clara Morgane - Le Diable Au Corps.mp3
#VIDEO:Clara Morgane - Le Diable Au Corps.mp4
#COVER:Clara Morgane - Le Diable Au Corps.jpg
#BPM:249.55
#GAP:1381
#VIDEOGAP:3
#ENCODING:UTF8
#GENRE:Dance & House
#CREATOR:Leroidukitch/Tyty
: 0 3 2 Le
: 4 3 2  soleil
: 8 2 2  dort
: 11 3 3  en
: 18 4 5 core
- 25
: 27 3 2 J'ai
: 31 3 2  Le
* 39 3 2  Diable
* 43 3 2  Au
* 47 4 4  Corps
- 53
: 55 3 2 La
: 60 2 2  nuit
: 63 3 2  me
: 67 3 2  jette
: 71 2 2  un
: 74 3 2  sort
- 93
: 121 3 2 Il
: 125 3 2  faut
: 132 3 2  me
: 136 3 2  laissé
: 142 4 4  faire
- 152
: 155 3 5 Ne
: 159 3 2  plus
: 166 3 3  t'en
: 173 3 5  faire
- 180
: 182 3 5 L'en
: 186 3 2 fer
: 190 3 2  pou
: 194 3 2 rrait
: 198 3 2  te
: 202 3 2  plaire
- 221
: 253 5 1 J'au
: 261 9 2 rais
: 281 5 5  sa
: 287 5 5  peau,
: 294 5 4  il
: 300 5 4  sait
: 310 5 2  il
: 318 5 2  faut
- 339
* 381 6 6 Je
* 389 5 5  t'au
* 395 5 4 rais
: 407 3 4  ŕ
: 411 3 6  mon
: 415 5 6  pied,
: 423 3 5  ne
: 427 3 4  dis
: 431 3 3  plus
: 435 5 2  un
: 446 5 -3  mot
- 467
: 500 3 2 Viens
: 504 3 3  gout
: 508 2 2 ter
: 511 3 3  ŕ
: 519 4 2  mes
: 524 3 2  lčvres,
: 532 3 2  je
: 536 2 2  sens
: 539 4 2  mon
: 544 3 8 ter
: 551 2 4  la
: 554 3 4  fičvre
- 562
: 564 2 6 J'au
: 567 2 6 rais
: 574 3 2  ta
: 578 3 0  peau
- 597
: 633 2 2 Pour
: 637 2 2 quoi
: 641 4 -1  se
: 647 2 2  pas
: 651 9 2 ser
: 662 2 2  de
: 666 2 2  s'em
: 670 2 2 bras
: 673 5 1 ser,
: 680 5 -1  de
: 687 10 -1  s'em
: 700 11 6 braser
- 727
: 765 3 2 Le
: 769 3 2  soleil
: 773 2 2  dort
: 776 3 2  en
: 783 4 5 core
- 790
: 792 3 2 J'ai
: 796 3 2  Le
* 804 3 2  Diable
* 808 3 2  Au
* 812 4 4  Corps
- 818
: 820 3 4 La
: 825 2 2  nuit
: 828 3 2  me
: 832 3 2  jette
: 836 2 2  un
: 839 3 2  sort
- 858
: 886 3 2 Il
: 890 3 2  faut
: 897 3 2  me
: 901 13 2  laisser
: 915 5 7  faire
- 922
: 924 3 2 Ne
: 928 3 2  plus
: 935 3 4  t'en
: 942 9 7  faire
- 951
: 952 3 5 L'en
: 956 3 2 fer
: 960 3 2  pou
: 964 3 2 rrait
: 968 3 2  te
: 972 6 2  plaire
- 988
: 1010 3 2 Et
: 1014 3 4  Le
: 1018 4 5  soleil
: 1025 3 7  dort
- 1028
: 1030 2 2 Je
: 1033 3 2  veux
: 1041 3 2  ton
: 1045 6 2  corps,
- 1052
: 1054 3 2 Rien
: 1058 3 2  n'est
: 1066 3 2  plus
: 1070 7 2  fort
- 1077
: 1078 3 2 La
: 1082 3 2  nuit
: 1086 3 2  me
: 1090 3 2  jette
: 1094 3 2  un
: 1098 6 2  sort
- 1120
: 1145 3 5 Il
: 1149 3 5  faut
: 1153 6 5  lais
: 1162 4 5 ser
: 1167 6 8  faire
- 1173
: 1175 5 5 Ne
: 1181 7 9  plus
: 1189 5 8  s'en
: 1195 7 7  faire
- 1207
: 1209 3 7 L'en
: 1213 3 7 fer
: 1217 3 7  pou
: 1221 3 7 rrait
: 1225 3 7  nous
: 1229 4 8  plaire
- 1249
: 1277 5 1 J'au
: 1285 9 2 rais
: 1305 5 5  sa
: 1311 5 5  peau,
: 1318 5 4  elle le
: 1324 5 4  sait,
: 1334 5 2  elle le
: 1342 5 2  vaut
- 1363
: 1405 6 6 Je
* 1413 5 5  t'au
* 1419 5 4 rais
: 1431 3 4  ŕ
: 1435 3 6  mes
: 1439 5 6  pieds,
: 1447 3 5  ne
: 1451 3 4  dis
: 1455 3 3  plus
: 1459 5 2  un
: 1470 5 -3  mot
- 1491
: 1524 3 2 Si
: 1528 3 3  le
: 1532 2 2  vice
: 1538 3 3  s'im
: 1543 4 2 misce
: 1551 3 2  en
: 1556 3 2 tre
: 1560 2 2  mes
: 1563 4 2  cuisses
: 1572 3 2  com
: 1579 9 2 plices
- 1588
: 1590 2 6 Qu'on
: 1593 2 6  me
: 1600 3 1  mau
: 1604 3 0 disse
- 1623
: 1659 2 2 Pour
: 1663 2 4 quoi
: 1667 4 3  se
: 1673 2 2  pas
: 1677 9 2 ser
: 1688 2 2  de
: 1692 2 3  s'em
: 1696 2 0 bras
: 1699 5 -1 ser,
: 1706 5 -1  de
: 1713 10 -1  s'em
: 1726 11 6 braser
- 1753
: 1787 3 2 Le
: 1791 3 2  soleil
: 1795 2 2  dort
: 1798 3 2  en
: 1805 4 5 core
- 1812
: 1814 3 2 J'ai
: 1818 3 2  Le
* 1831 3 2  Diable
* 1835 3 2  Au
* 1839 4 4  Corps
- 1847
: 1849 3 4 La
: 1854 2 2  nuit
: 1857 3 2  me
: 1861 3 2  jette
: 1865 2 2  un
: 1868 3 2  sort
- 1887
: 1908 3 2 Il
: 1912 3 2  faut
: 1919 3 2  me
: 1923 13 2  laisser
: 1937 5 7  faire
- 1944
: 1946 3 2 Ne
: 1950 3 2  plus
: 1957 3 4  t'en
: 1964 9 7  faire
- 1973
: 1974 3 5 L'en
: 1978 3 2 fer
: 1982 3 2  pou
: 1986 3 2 rrait
: 1990 3 2  te
: 1994 6 2  plaire
- 2010
: 2032 3 2 Et
: 2036 3 4  Le
: 2040 4 5  soleil
: 2047 3 7  dort
- 2050
: 2052 2 2 Je
: 2055 3 2  veux
: 2063 3 2  ton
: 2067 6 2  corps,
- 2074
: 2076 3 2 Rien
: 2080 3 2  n'est
: 2088 3 2  plus
: 2092 7 2  fort
- 2099
: 2100 3 2 La
: 2104 3 2  nuit
: 2108 3 2  me
: 2112 3 2  jette
: 2116 3 2  un
: 2120 6 2  sort
- 2142
: 2167 3 5 Il
: 2171 3 5  faut
: 2175 6 5  lais
: 2184 4 5 ser
: 2189 6 8  faire
- 2195
: 2197 5 5 Ne
: 2203 7 9  plus
: 2211 5 8  s'en
: 2217 7 7  faire
- 2229
: 2231 3 7 L'en
: 2235 3 7 fer
: 2239 3 7  pou
: 2243 3 7 rrait
: 2247 3 7  nous
: 2251 4 8  plaire
- 2288
: 2555 3 2 Le
: 2559 3 2  soleil
: 2563 2 2  dort
: 2566 3 2  en
: 2573 4 5 core
- 2580
: 2582 3 2 J'ai
: 2586 3 2  Le
: 2594 3 2  Diable
: 2598 3 2  Au
: 2602 4 4  Corps
- 2608
: 2610 3 4 La
: 2615 2 2  nuit
: 2618 3 2  me
: 2622 3 2  jette
: 2626 2 2  un
: 2629 3 2  sort
- 2648
: 2676 3 2 Il
: 2680 3 2  faut
: 2687 3 2  me
: 2691 13 2  laisser
: 2705 5 7  faire
- 2712
: 2714 3 2 Ne
: 2718 3 2  plus
: 2725 3 4  t'en
: 2732 9 7  faire
- 2741
: 2742 3 5 L'en
: 2746 3 2 fer
: 2750 3 2  pou
: 2754 3 2 rrait
: 2758 3 2  te
: 2762 6 2  plaire
- 2784
: 2799 3 2 Et
: 2803 3 4  Le
: 2807 4 5  soleil
: 2814 3 7  dort
- 2817
: 2819 2 2 Je
: 2822 3 2  veux
: 2828 3 2  ton
: 2832 6 2  corps,
: 2844 3 2 Rien
- 2845
: 2848 3 2  n'est
: 2855 3 2  plus
: 2859 7 2  fort
- 2866
: 2867 3 2 La
: 2871 3 2  nuit
: 2875 3 2  me
: 2879 3 2  jette
: 2883 3 2  un
: 2887 6 2  sort
- 2912
: 2935 3 5 Il
: 2939 3 5  faut
: 2943 6 5  lais
: 2952 4 5 ser
: 2957 6 8  faire
- 2963
: 2965 5 5 Ne
: 2971 7 7  plus
: 2979 5 8  s'en
: 2985 7 7  faire
- 2994
: 2996 3 5 L'en
: 3000 3 5 fer
: 3004 3 5  pou
: 3008 3 5 rrait
: 3012 3 5  nous
: 3016 4 5  plaire
- 3036
: 3052 3 2 Et
: 3056 3 4  Le
: 3060 4 5  soleil
: 3067 3 7  dort
- 3070
: 3074 2 2 Je
: 3079 3 2  veux
* 3089 3 2  ton
* 3093 6 2  corps,
- 3098
: 3100 3 2 Rien
: 3104 3 2  n'est
: 3112 3 2  plus
: 3116 7 2  fort
- 3123
: 3124 3 2 La
: 3128 3 2  nuit
: 3132 3 2  me
: 3136 3 2  jette
: 3140 3 2  un
: 3144 6 2  sort
- 3166
: 3185 3 5 Il
: 3189 3 5  faut
: 3193 6 5  lais
: 3202 4 5 ser
: 3207 6 8  faire
- 3218
: 3220 5 5 Ne
: 3226 7 9  plus
: 3234 5 8  s'en
: 3240 7 7  faire
- 3252
: 3254 3 7 L'en
: 3258 3 7 fer
: 3262 3 7  pou
: 3266 3 7 rrait
: 3270 3 7  nous
: 3274 4 8  plaire
E