#TITLE:Fever
#ARTIST:Peggy Lee
#MP3:Peggy Lee - Fever.mp3
#VIDEO:Peggy Lee - Fever.avi
#COVER:Peggy Lee - Fever [CO].jpg
#BPM:274,52
#GAP:15180
#MedleyStartBeat:225
#MedleyEndBeat:425
#ENCODING:UTF8
#PREVIEWSTART:27,474
#LANGUAGE:English
#GENRE:Blues
#YEAR:1958
: 0 3 2 Ne
: 4 2 0 ver
: 8 4 2  know
: 13 2 0  how
: 16 4 2  much
: 21 3 -3  I
: 25 4 0  love
: 31 3 2  you
- 36
: 59 5 2 Ne
: 65 2 0 ver
: 68 5 2  know
: 74 2 0  how
: 77 4 2  much
: 82 2 0 ~
: 85 3 -3  I
: 90 3 -3  care
- 95
: 123 6 2 When
: 130 2 0  you
: 133 3 2  put
: 137 2 0  your
: 140 8 2  arms
: 149 4 -3  a
: 154 5 0 round
* 162 2 2  me
- 166
: 178 2 -3 I
: 181 4 -3  get
: 186 2 -3  a
: 189 4 2  fe
: 194 2 0 ver
: 197 3 2  thats
: 201 3 0  so
: 205 4 2  hard
* 210 3 0 ~
: 214 1 -3  to
: 218 4 -3  bear
- 223
: 225 2 -3 You
: 228 2 -3  give
: 233 2 -3  me
: 236 7 0  fe
: 244 6 -3 ver
- 252
* 290 6 3 When
: 298 2 0  you
: 301 5 2  kiss
: 308 4 0  me
- 314
: 316 5 2 Fe
: 322 1 0 ver
: 325 5 2  when
: 331 2 0  you
: 334 3 2  hold
: 338 3 0 ~
: 342 1 -3  me
: 346 10 -3  tight
- 358
* 381 3 9 Fe
* 385 2 10 ver
- 389
: 411 3 2 In
: 415 1 0  the
: 418 4 2  mor
: 423 2 0 ning
- 427
: 444 6 2 Fe
: 451 1 0 ver
: 453 1 3  all
: 455 2 2 ~
: 461 5 2  through
: 467 2 0 ~
: 470 2 -3  the
: 475 7 -3  night
- 484
: 514 10 2 Sun
: 525 5 0  lights
: 532 2 2  up
: 539 2 -3  the
: 542 4 0  day
: 548 3 2 time
- 553
: 572 5 2 Moon
: 578 2 3 ~
: 581 6 2  lights
: 590 3 0  up
: 598 2 -3  the
: 603 3 -3  night
- 608
* 635 2 8 I
* 638 5 9 ~
* 645 5 3  light
* 651 3 3  up
: 657 3 3  when
: 661 2 2  you
: 666 3 2  call
: 670 6 3 ~
: 678 4 0  my
: 683 3 -3  name
- 688
: 691 6 -3 And
: 699 2 -3  you
: 702 4 2  know
: 707 2 0  I'm
: 710 4 2  gon
: 715 2 0 na
: 718 4 2  treat
: 723 2 0 ~
: 726 4 -3  you
: 732 3 -3  right
- 737
: 740 2 -3 You
: 743 2 -3  give
: 747 2 -3  me
: 750 7 0  fe
: 758 3 -3 ver
- 763
: 804 5 3 When
: 811 2 0  you
: 814 6 2  kiss
: 822 3 0  me
- 827
: 829 6 2 Fe
: 836 1 0 ver
: 838 4 2  when
: 844 2 0  you
: 847 4 2  hold
: 852 2 0 ~
: 855 2 -3  me
: 860 3 -3  tight
- 865
* 893 2 8 Fe
* 896 3 9 ver
- 901
: 924 3 3 In
: 928 2 0  the
: 931 3 2  mor
: 935 2 0 ning
- 939
: 957 6 2 Fe
: 964 2 0 ver
: 967 3 2  all
: 974 5 2  through
: 980 2 0 ~
: 983 2 -3  the
: 987 4 -3  night
- 993
* 1029 3 0 E
: 1036 2 -3 very
: 1039 7 -3 bo
: 1048 4 -5 dy's
: 1061 3 -3  got
: 1068 2 -5  the
: 1071 7 -3  fe
: 1079 2 0 ver
- 1083
: 1092 3 0 That
: 1096 3 -3  is
: 1102 5 -3  some
: 1111 5 -5 thing
: 1117 6 -3  you
: 1124 6 -5  all
: 1133 4 -3  know
- 1139
* 1157 6 0 Fe
* 1164 3 -3 ver
: 1168 4 -3  is
: 1175 2 -5 n't
: 1187 4 -3  such
: 1195 3 -5  a
: 1199 3 -3  new
: 1204 6 0  thing
- 1212
: 1214 4 2 Fe
: 1219 2 0 ver
: 1222 5 2  star
: 1228 2 0 ted
: 1231 4 2  long
: 1236 2 0 ~
: 1240 2 -3  a
: 1244 4 -3 go
- 1250
: 1412 8 4 Ro
: 1421 2 2 me
: 1424 2 4 o
: 1431 2 -1  loved
: 1436 7 2  Ju
: 1444 3 4 liet
- 1449
: 1468 7 4 Ju
: 1476 2 4 li
: 1479 2 4 et
: 1484 2 4  she
: 1490 3 4  felt
* 1494 3 3 ~
: 1499 2 1  the
: 1504 5 -2  same
- 1511
: 1533 6 4 When
: 1540 2 2  he
: 1543 3 4  put
: 1548 2 2  his
: 1551 5 4  arms
: 1559 3 -1  a
: 1563 8 2 round
: 1573 2 4  her
- 1577
: 1588 2 -1 He
: 1591 3 -2  said
: 1596 4 4  "Ju
: 1601 3 2 liet
* 1606 3 4  ba
* 1610 4 2 by
: 1615 2 4  you
: 1619 2 2 're
: 1623 3 -2  my
: 1629 4 -2  flame"
- 1634
: 1636 3 -2 Thou
: 1642 2 -2  gi
: 1645 2 -2 vest
: 1650 3 1  fe
: 1654 2 -2 ver,
- 1658
: 1700 4 4 When
: 1707 2 1  we
: 1710 5 3  kis
: 1716 2 1 seth
- 1720
: 1724 3 3 Fe
: 1728 2 1 ver
: 1732 2 3  with
: 1736 3 1  thy
: 1741 6 3  fla
: 1748 2 1 ~
: 1751 3 -2 ming
: 1756 4 -2  youth
- 1762
: 1787 3 8 Fe
: 1791 3 10 ver
- 1796
: 1819 4 4 I'm
: 1824 2 1  a
: 1827 4 3  fi
: 1832 2 1 re
- 1836
: 1851 3 3 Fe
: 1855 2 1 ver
* 1859 3 3  yeah
: 1863 4 1  I
: 1869 5 3  burn
: 1875 4 1 ~
: 1882 4 -2  for
* 1891 9 -2  sooth'
- 1902
: 2052 3 4 Cap
: 2057 2 2 tain
: 2061 4 4  Smith
: 2067 3 2  and
* 2076 3 4  Po
: 2080 2 -1 ca
: 2084 3 2 hon
: 2088 4 4 tas
- 2094
* 2108 7 4 Had
: 2116 2 2  a
: 2119 3 4  ve
: 2123 2 2 ry
: 2126 4 4  mad
* 2131 2 2  ~
: 2135 3 -1  af
: 2139 4 -1 fair
- 2145
: 2173 5 4 When
: 2179 2 2  her
: 2182 4 4  Dad
: 2187 3 2 dy
: 2193 4 4  tried
: 2198 1 -1  to
: 2202 7 2  kill
: 2210 3 4  him
- 2215
: 2226 2 -1 She
: 2229 4 -1  said
: 2236 5 4  "Dad
: 2242 2 2 dy
: 2245 4 4  Oh
: 2250 2 2  ~
: 2253 4 4  don't
: 2258 2 2  ~
: 2261 2 -1  you
: 2267 5 -1  dare"
- 2273
: 2275 1 -1 He
: 2277 2 -1  gives
: 2281 2 -1  me
: 2284 8 2  fe
: 2293 4 -1 ver
- 2299
: 2337 6 5 With
: 2345 2 2  his
: 2348 4 4  kis
: 2353 3 2 ses
- 2358
: 2361 5 4 Fe
: 2369 2 2 ver
: 2372 5 4  when
: 2378 2 2  he
: 2381 3 4  holds
: 2385 2 2 ~
: 2388 2 -1  me
: 2393 8 -1  tight
- 2403
* 2425 2 9 Fe
* 2428 2 11 ver
- 2432
* 2456 4 5 I'm
: 2461 3 2  his
: 2465 2 4  Mis
: 2468 5 2 ses
- 2475
: 2489 1 -1 Oh
: 2491 3 4  dad
: 2495 2 2 dy
: 2498 4 4  won't
: 2504 2 2  you
: 2507 5 4  treat
: 2513 2 2 ~
: 2516 3 -1  him
: 2521 2 -1  right
- 2525
* 2553 5 4 Now
: 2559 4 2  you've
: 2566 3 4  lis
: 2570 3 2 tened
: 2575 2 4  to
: 2579 4 -1  my
: 2586 6 2  sto
: 2594 2 4 ry
- 2598
: 2617 5 4 Here's
: 2624 2 2  the
: 2628 4 4  point
: 2633 2 2  that
: 2636 3 4  I
: 2640 2 2 ~
: 2643 4 -1  have
: 2649 4 -1  made
- 2655
* 2680 6 4 Chicks
: 2688 2 2  were
: 2691 4 4  born
: 2697 1 2  to
: 2699 8 4  give
: 2708 2 -1  you
: 2712 7 2  fe
: 2720 3 4 ver
- 2725
: 2736 2 -1 Be
: 2739 2 -1  it
: 2744 3 4  Fah
: 2748 3 2 ren
: 2753 3 4 heit
: 2757 2 2  or
: 2762 5 4  Cen
: 2768 3 2 ~
: 2772 2 -1 ti
: 2776 5 -1 grade
- 2783
: 2785 2 -1 They
: 2788 2 -1  give
: 2793 2 -1  you
: 2796 6 2  fe
: 2803 3 -1 ver
- 2808
: 2849 5 5 When
: 2857 1 2  you
: 2859 4 4  kiss
: 2867 5 2  them
- 2873
: 2875 5 4 Fe
: 2881 2 2 ver
: 2884 2 4  if
: 2889 2 2  you
: 2892 4 4  live
: 2897 2 2 ~
: 2901 3 -1  and
: 2905 6 -1  learn
- 2913
: 2937 3 10 Fe
: 2941 2 11 ver
- 2945
: 2968 3 5 Till
: 2972 2 2  you
: 2978 3 4  siz
: 2982 2 2 zle
- 2986
: 3001 7 4 What
: 3009 2 2  a
: 3012 3 4  lo
: 3017 2 2 vely
: 3020 5 4  way
: 3026 1 2 ~
: 3028 2 -1  to
: 3033 5 -1  burn
- 3040
: 3068 4 4 What
: 3073 2 2  a
: 3076 4 4  lo
: 3081 2 2 vely
: 3084 4 4  way
: 3089 2 2  ~
: 3092 2 -1  to
: 3097 8 -1  burn
- 3107
* 3128 3 11 What
: 3132 2 5  a
: 3135 5 4  lo
: 3141 2 2 vely
: 3145 4 4  way
: 3151 5 2 ~
: 3157 1 -1  to
: 3160 9 -1  burn
- 3171
: 3195 5 4 What
: 3201 1 2  a
* 3203 4 5  lo
: 3208 2 2 vely
: 3211 5 4  way
: 3217 2 2  ~
: 3220 2 -1  to
: 3225 12 -1  burn
E