#TITLE:Johnny Walker (Live)
#ARTIST:Marius Müller Westernhagen
#COVER:Marius Müller Westernhagen - Johnny Walker [CO].jpg
#MP3:Marius Mueller Westernhagen - Johnny Walker.mp3
#BACKGROUND:Marius Müller Westernhagen - Johnny Walker [BG].jpg
#BPM:127,64
#GAP:0
: 45 6 64 John
: 51 5 67 ny
: 56 7 67  Wal
: 63 3 64 ker,
- 67
: 71 1 64  jetzt
: 72 3 65  bist
: 75 3 64  Du
: 78 5 60  wie
: 83 2 62 der
: 85 7 64  da.
- 89
: 120 7 64 John
: 127 6 67 ny
: 133 2 67  Wal
: 135 10 64 ker,
- 141
: 145 3 64  ich
: 148 3 65  zahl'
: 151 4 64  dich
: 155 6 60  gleich
: 161 2 62  in
: 163 5 62  bar. 
- 170
: 196 8 64 John
: 204 4 67 ny
: 208 5 67  Wal
: 214 6 64 ker, 
: 222 2 64  Du
: 224 3 65  hast
: 227 3 64  mich
: 230 4 65  nie
: 234 3 69  ent
: 238 7 67 taeuscht.
- 241
: 268 4 62 John
: 272 6 62 ny,
: 281 4 62  Du
: 285 3 64  bist
: 289 3 62  mein
: 293 3 60  bes
: 296 2 62 ter
: 299 6 60  Freund. 
- 306
: 336 6 64 John
: 342 3 67 ny
: 346 6 67  Wal
: 352 6 64 ker,
- 356
: 362 2 65  im
: 364 4 64 mer
: 368 4 60  braun
: 373 3 62  ge
: 376 7 64 brannt.
- 383
: 413 6 64 John
: 420 2 67 ny
: 423 5 67  Wal
: 429 5 64 ker,
- 432
: 435 2 64  mit
: 437 2 64  dem
: 439 3 65  Ruec
: 443 3 64 ken
: 447 4 60  an
: 451 2 62  die
: 454 5 62  Wand. 
-459
: 489 6 64 John
: 496 2 67 ny
: 500 5 67  Wal
: 506 5 64 ker 
- 508
: 512 4 64  komm'
: 516 3 65  giess
: 519 3 64  dich
: 522 4 65  noch
: 526 4 69 mal
: 531 4 67  ein.
- 533
: 559 5 62 John
: 564 5 62 ny,
: 572 5 62  lass
: 577 2 64  Drei
: 579 5 62 zehn
: 584 3 60  gera
: 587 2 60 de
: 589 9 60  sein. 
- 597
: 628 2 71 Ich
: 630 2 71  hab's
: 632 3 71  ver
: 635 4 71 sucht,
- 638
: 641 4 71 ich
: 645 3 72  kom
: 649 3 74 me
: 654 2 72  o
: 656 3 72 hne
: 659 3 69  dich
: 663 3 65  nicht
: 666 5 67  aus. 
- 666
: 702 3 64 Wo
: 705 2 71 zu
: 708 7 71  auch?
: 715 4 64  Du
: 719 1 64  ge
: 720 4 71 faellst
: 724 2 64  mir
: 727 4 69  ja.
- 727
: 760 5 72 Kein
: 765 10 72  Mensch
: 779 5 72  hoert
: 784 4 69  mir
: 788 3 67  so
: 792 3 67  gut
: 795 4 64  zu
: 799 3 64  wie
: 802 6 64  Du
- 808
: 815 4 62 und
: 820 7 62  Johnny
: 831 4 62  Du
: 835 3 64  lachst
: 839 2 62  mich
: 842 3 60  auch
: 845 4 62  nie
: 850 6 60  aus. 
- 855
: 886 5 64 John
: 892 3 67 ny
: 896 6 67  Wal
: 903 5 64 ker,
- 906
: 910 2 64  ich
: 913 2 65  glaub'
: 916 3 64  nicht
: 920 4 60  an
: 925 2 62  den
: 927 7 64  Quatsch.
- 929
: 960 9 64 John
: 970 2 67 ny
: 972 6 67  Wal
: 979 6 64 ker 
- 981
: 985 4 64  Du
: 989 3 65  waerst
: 992 2 64  'ne
: 995 3 60  Teu
: 1000 4 62 fels
: 1004 6 62 fratz. 
- 1010
: 1037 8 64 John
: 1046 2 67 ny
: 1050 6 67  Wal
: 1056 4 64 ker 
- 1059
: 1063 2 64  von
: 1065 2 65  mir
: 1067 4 64  aus
: 1071 4 65  ros
: 1075 3 69 te
: 1079 3 67  nicht,
- 1084
: 1111 3 62 John
: 1115 5 62 ny,
: 1123 4 62  ich
: 1127 2 64  fuehl
: 1130 3 62  mich
: 1133 4 60  koe
: 1138 2 62 nig
: 1140 3 60 lich. 
- 1150
: 1452 2 71 Ich
: 1454 3 71  hab's
: 1457 2 71  ver
: 1460 4 71 sucht,
- 1462
: 1465 4 71 ich
: 1470 2 72  kom
: 1473 3 74 me
: 1477 2 72  o
: 1479 4 72 hne
: 1483 4 69  dich
: 1487 3 65  nicht
: 1491 6 67  aus. 
- 1498
: 1525 5 64 Wo
: 1530 3 71 zu
: 1533 6 71  auch?
: 1543 8 71  Du gefaellst
: 1551 3 72  mir
: 1554 5 69  ja.
- 1560
: 1587 4 72 Kein
: 1591 8 72  Mensch
: 1604 6 72  hoert
: 1611 3 69  mir
: 1614 3 67  so
: 1617 4 67  gut
: 1621 5 64  zu
: 1626 3 60  wie
: 1628 4 64  Du
- 1635
: 1643 3 62 und
: 1646 6 62  Johnny
: 1659 3 62  Du
: 1662 2 64  lachst
: 1664 3 62  mich
: 1668 4 60  auch
: 1672 3 62  nie
: 1676 3 60  aus. 
- 1678
: 1708 10 64 John
: 1719 3 67 ny
: 1723 6 67  Wal
: 1730 8 64 ker 
- 1731
: 1737 3 65  na
: 1741 2 64 na
: 1744 4 60  na
: 1750 2 62  na
: 1753 5 64 na,
- 1835
: 1865 7 64 John
: 1872 4 67 ny
: 1877 5 67  Wal
: 1882 3 64 ker 
- 1808
: 1814 4 65  na
: 1818 3 64 na
: 1822 6 60  na
: 1829 2 62  na
: 1831 4 62 na, 
- 1835
: 1865 7 64 John
: 1872 5 67 ny
: 1878 5 67  Wal
: 1883 3 64 ker 
- 1885
: 1889 4 64  Du
: 1893 2 65  hast
: 1895 4 64  mich
: 1899 3 65  nie
: 1903 3 69  ent
: 1906 7 67 taeuscht.
- 1910
: 1936 5 62 John
: 1941 6 62 ny
: 1950 4 62  Du
: 1954 3 64  bist
: 1957 5 62  mein
: 1962 3 60  bes
: 1965 3 60 ter
: 1968 5 60  Freund.
- 1989
: 1999 4 62 John
: 2004 6 62 ny
: 2013 4 62  Du
: 2017 2 64  bist
: 2019 5 62  mein
: 2024 3 60  bes
: 2027 2 60 ter
: 2030 6 60  Freund. 
- 1961
: 2062 5 62 John
: 2068 6 62 ny
: 2075 4 62  Du
: 2078 4 64  bist
: 2083 4 62  mein
: 2087 4 60  bes
: 2091 3 60 ter
: 2095 11 60  Freund.
E
