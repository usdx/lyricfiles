#TITLE:Wish You Were Here
#ARTIST:Pink Floyd
#MP3:Pink Floyd - Wish You Were Here.mp3
#COVER:Pink Floyd - Wish You Were Here [CO].jpg
#BACKGROUND:Pink Floyd - Wish You Were Here [BG].jpg
#BPM:121,95
#GAP:90000
: 0 4 72 So,
: 4 3 67 -
: 16 1 72  so 
: 18 1 71  you
: 20 1 72  think
: 22 1 71  you
: 24 1 67  can
: 26 4 69  tell
: 30 1 62 -
- 49
: 56 1 74 Heaven
: 57 2 72 -
: 59 1 74  from
: 60 5 76  Hell
: 65 7 74 -
- 79
: 84 3 76 Blue
: 88 2 76  skies
: 90 3 74 -
: 93 2 71  from
: 95 7 74  pain
- 112
: 119 2 71 Can
: 121 2 72  you
: 123 3 72  tell
: 126 1 71  a
: 127 4 67  green
: 131 2 69  field
: 133 2 66 -
: 135 1 64 -
: 136 6 62 -
- 146
: 152 1 74 from
: 153 1 74  a
: 155 7 74  cold
: 162 4 71  steel
: 166 2 72  rail?
: 168 2 71 -
: 170 5 67 -
- 182
: 188 1 71 A
: 189 7 74  smile
: 196 2 72  from
: 198 2 71  a
: 200 6 72  veil?
- 215
: 223 1 72 Do
: 224 2 71  you
: 226 3 72  think
: 229 2 71  you
: 231 2 67  can
: 233 6 67  tell?
- 255
: 255 1 72 And
: 256 2 72  did
: 258 2 71  they
: 260 2 72  get
: 262 2 74  you
: 264 2 74  to
: 266 5 76  trade
: 271 2 74 -
: 273 2 72 -
: 275 5 67 -
- 280
: 289 1 76 your
: 293 7 76  heroes
: 300 2 74  for
: 302 2 71  ghosts?
: 304 2 74 -
: 306 4 69 -
- 310
: 322 2 74 Hot
: 324 6 74  ashes
: 330 2 71  for
: 332 3 74  trees?
: 335 4 76 -
: 339 5 74 -
- 344
: 354 3 76 Hot
: 357 4 76  air
: 361 2 74  for
: 363 2 71  a
: 365 7 74  cool
: 372 2 71  breeze?
: 374 6 74 -
- 379
: 387 4 76 Cold
: 391 4 74  comfort
: 395 2 74  for
: 397 2 71  change
: 399 2 67 -
: 401 3 69 -
: 404 3 71 -
: 407 3 62 -
- 410
: 420 1 62 And
: 421 2 74  did
: 423 6 74  you
: 429 2 71  exchange
: 431 5 72 -
: 436 2 71 -
: 438 7 67 -
- 445
: 448 2 67 a
: 450 2 74  walk
: 452 4 72  on
: 456 4 72  part
: 460 2 71  in
: 462 2 67  the
: 464 6 72  war
- 467
: 474 3 67 for
: 477 1 67  a
: 478 6 72  lead
: 484 7 72  role
: 491 1 71  in
: 492 2 67  a
: 494 11 72  cage
: 505 5 67 -
- 816
: 824 2 72 How
: 826 2 71  i
: 828 4 72  wish,
: 832 2 71 -
: 834 3 67 -
- 836
: 841 2 72 How
: 843 1 71  I
: 844 3 72  wish
: 847 1 71  you
: 848 2 67  where
: 850 4 69  here.
: 854 5 62 -
- 870
: 877 2 72 We're just
: 879 4 74 -
: 883 5 76  two
: 888 4 76  lost
: 892 5 76  souls
- 895
: 896 5 76 swimming
: 901 2 74  in a
: 903 1 76 -
: 904 4 74  fish
: 908 2 76  bowl,
: 910 2 76 -
: 912 2 74 -
: 914 4 71 -
- 915
: 919 5 74 year
: 925 5 74  after
: 930 2 71  year,
: 932 5 74 -
- 947
: 953 1 74 Running
: 954 3 72 -
: 957 3 74  over
: 960 1 71 -
: 961 1 69  the
: 962 4 74  same
: 966 3 74  old
: 969 6 74  ground.
- 977
: 983 2 72 What
: 985 2 74  have
: 987 1 72  we
: 988 4 74  found?
: 992 2 72 -
: 994 3 71 -
: 997 5 67 -
- 998
: 1001 3 74 The
: 1004 5 74  same
: 1009 3 72  old
: 1012 2 71  fears.
: 1014 3 72 -
: 1017 2 71 -
: 1019 4 67 -
- 1027
: 1033 3 72 Wish
: 1036 5 72  you
: 1041 2 71  were
: 1043 2 67  here.
: 1045 3 72 -
: 1048 1 71 -
: 1049 4 67 -
E
