#TITLE:Fallen Leaves
#ARTIST:Billy Talent
#MP3:Billy Talent - Fallen Leaves.mp3
#COVER:41s7tE2RwNL__AA240_.jpg
#BACKGROUND:leaves_s.jpg
#VIDEO:Billy Talent - Fallen Leaves.avi
#VIDEOGAP:0,5
#BPM:125
#GAP:7550
: 0 1 0 In 
: 1 1 2  a 
: 2 2 3  croo
: 4 2 0 ked 
: 6 2 3  lit
: 8 2 5 tle
: 10 4 7  town 
- 14
: 14 2 7 they  
: 16 2 7  were
: 18 2 7  lost 
: 20 2 8  and 
: 22 2 7  ne
: 24 2 5 ver 
: 26 4 3  found 
- 30
: 30 2 3 Fal
: 32 2 3 len 
: 34 3 3 Leaves,
: 38 2 3  Fal
: 40 2 3 len 
: 42 3 3 Leaves, 
: 46 2 3  Fal
: 48 2 3 len 
: 50 4 3  Leaves
- 55
: 56 1 3 on 
: 57 2 5  the 
: 59 5 2  ground 
- 64
: 134 2 2 I 
: 136 2 3  hitched 
: 138 2 0  a 
* 140 8 2  ri--de 
- 149
: 150 2 3 un
: 152 2 7 til 
: 154 2 3  the 
* 156 10 2  coa--st
- 167
: 167 1 2 To 
: 168 3 3  leave 
: 171 2 0  be
* 173 8 2 hi--nd 
- 182
: 182 3 3 all 
: 185 2 7  of 
: 187 2 3  my 
* 189 8 2  gho--sts 
- 198
: 198 3 3 Sear
: 201 2 7 ching 
: 203 2 3  for 
: 205 4 5  some
: 209 5 5 thing 
- 214
: 215 2 5 I 
: 217 3 8  could 
: 220 2 5  n't 
: 222 3 7  find 
: 225 2 5  a-
: 227 3 3 at 
* 230 16 3  ho----
* 246 8 3 --
* 254 6 2 -me 
- 260
: 264 2 2 Can't 
: 266 2 3  get 
: 268 2 0  no 
* 270 8 2  jo--b 
- 279
: 280 2 3 Can you 
: 282 3 7  spare 
: 285 2 3  a 
* 287 8 2  di--me
- 296
: 297 2 2 Just 
: 299 2 3  one 
: 301 2 0  more 
* 303 6 2  hi--t 
- 311
: 312 3 3 and 
: 315 2 7  I'll 
: 317 2 3  be 
* 319 8 2  fi--ne 
- 328
: 329 2 3 I 
: 331 3 7  swear 
: 334 2 3  to 
: 336 4 5  god 
: 340 4 5  this'- 
- 344
: 346 2 5 ll 
: 348 2 8  be 
: 350 2 5  my 
: 352 3 7  one 
: 355 2 5  la-
: 357 3 3 -st 
* 360 14 2  ti----
* 374 3 5 --
* 377 13 7 -me 
- 390
: 391 1 0 In 
: 392 1 2  a 
: 393 1 3  croo
: 394 2 0 ked 
* 396 2 12  lit
: 398 2 5 tle
: 400 4 7  town 
- 405
: 405 2 7 they  
: 407 2 7  were
: 409 2 7  lost 
: 411 2 8  and 
: 413 2 7  ne
: 415 2 5 ver 
: 417 4 3  found 
- 421
: 421 2 3 Fal
: 423 2 3 len 
: 425 3 3 Leaves,
: 429 2 3  Fal
: 431 2 3 len 
: 433 3 3 Leaves, 
: 437 2 3  Fal
: 439 2 3 len 
: 441 4 3  Leaves
- 445
: 446 1 3 on 
: 447 2 5  the 
: 449 5 2  ground 
- 454
: 455 1 0 Run 
: 456 1 2  a
: 457 2 3 way 
: 459 2 0  be
* 461 2 12 fore 
: 463 2 5  you 
: 465 4 7  drown 
- 470
: 470 2 7 or 
: 472 2 7  the 
: 474 2 7  streets 
: 476 2 8  will 
: 478 2 7  beat 
: 480 2 5  you 
: 482 4 3  down 
- 486
: 486 2 3 Fal
: 488 2 3 len 
: 490 3 3 Leaves,
: 494 2 3  Fal
: 496 2 3 len 
: 498 3 3 Leaves, 
: 502 2 3  Fal
: 504 2 3 len 
: 506 4 3  Leaves
- 509
: 510 2 3 on 
: 512 2 5  the 
: 514 9 2  grou----
: 523 4 0 nd 
- 527
: 590 2 2 When 
: 592 2 3  it 
: 594 2 0  gets 
* 596 8 2  da--rk 
- 604
: 605 3 3 In 
: 608 1 7  Pi
: 609 3 3 geon  
* 612 9 2  Pa--rk
- 621
: 622 2 2 Voice 
: 624 2 3  in 
: 626 2 0  my 
* 628 8 2  hea--d 
- 636
: 638 2 3 will 
: 640 2 7  soon 
: 642 2 3  be 
* 644 9 2  fe--d 
- 653
: 656 2 7 By 
: 658 2 3  the 
: 660 4 5  vul
: 664 5 5 tures  
- 669
: 670 2 5 that 
: 672 2 8  cir
: 674 3 5 cle 
: 677 4 7  round 
: 681 2 5  the-
: 683 2 3 -e 
* 685 14 2  dea---
* 699 3 5 --
* 702 12 7 -d 
- 714
: 716 1 0 In 
: 717 1 2  a 
: 718 2 3  croo
: 720 2 0 ked 
* 722 2 12  lit
: 724 2 5 tle
: 726 4 7  town 
- 730
: 730 2 7 they  
: 732 2 7  were
: 734 2 7  lost 
: 736 2 8  and 
: 738 2 7  ne
: 740 2 5 ver 
: 742 4 3  found 
- 746
: 746 2 3 Fal
: 748 2 3 len 
: 750 3 3 Leaves,
: 754 2 3  Fal
: 756 2 3 len 
: 758 3 3 Leaves, 
: 762 2 3  Fal
: 764 2 3 len 
: 766 4 3  Leaves
- 770
: 771 1 3 on 
: 772 2 5  the 
: 774 5 2  ground 
- 779
: 780 2 0 Run 
: 782 1 2  a
: 783 2 3 way 
: 785 1 0  be
* 786 2 12 fore 
: 788 2 5  you 
: 790 4 7  drown 
- 794
: 795 2 7 or 
: 797 1 7  the 
: 798 3 7  streets 
: 801 2 8  will 
: 803 2 7  beat 
: 805 2 5  you 
: 807 4 3  down 
- 811
: 811 2 3 Fal
: 813 2 3 len 
: 815 3 3 Leaves,
: 819 2 3  Fal
: 821 2 3 len 
: 823 3 3 Leaves, 
: 827 2 3  Fal
: 829 2 3 len 
: 831 4 3  Leaves
- 835
: 836 1 3 on 
: 837 2 5  the 
: 839 9 2  grou----
: 848 4 0 nd 
- 852
F 852 128 0 (Solo)
- 980
: 980 2 5 I 
: 982 2 7  ne
: 984 2 8 ver 
: 986 4 7  once 
: 990 4 5  thought 
- 994
: 996 2 3 I'd 
: 998 2 5  e
: 1000 2 7 ver 
: 1002 4 5  be 
: 1006 4 3  caught
- 1010
: 1012 2 5  Sta
: 1014 2 7 ring 
: 1016 2 8  at 
: 1018 4 7  side
: 1022 4 5 walks 
- 1026
: 1029 2 3 Hi
: 1031 2 5 ding 
: 1033 2 7  my 
: 1035 4 5  track 
: 1039 4 3 marks 
- 1043
: 1045 2 5 I  
: 1047 2 7  left 
: 1049 2 8  my 
: 1051 4 7  best 
: 1055 5 5  friends 
- 1060
: 1061 2 3 Or 
: 1063 2 5  did 
: 1065 2 7  they 
: 1067 4 5  just 
: 1071 4 3  leave 
- 1075
: 1075 17 2 me---
: 1092 16 7 --e
- 1108
: 1171 1 0 In 
: 1172 1 2  a 
: 1173 1 3  croo
: 1174 2 0 ked 
: 1176 2 3  lit
: 1178 3 5 tle
: 1181 4 7  town 
- 1185
: 1185 2 7 they  
: 1187 2 7  were
: 1189 2 7  lost 
: 1191 2 8  and 
: 1193 2 7  ne
: 1195 2 5 ver 
: 1197 4 3  found 
- 1201
: 1201 2 3 Fal
: 1203 2 3 len 
: 1205 3 3 Leaves,
: 1209 2 3  Fal
: 1211 2 3 len 
: 1213 3 3 Leaves, 
: 1217 2 3  Fal
: 1219 2 3 len 
: 1221 4 3  Leaves
- 1225
: 1226 2 3 on 
: 1228 1 5  the 
: 1229 5 2  ground 
- 1234
: 1236 1 0 In 
: 1237 1 2  a 
: 1238 2 3  croo
: 1240 2 0 ked 
* 1242 2 12  lit
: 1244 2 5 tle
: 1246 4 7  town 
- 1250
: 1250 2 7 they  
: 1252 2 7  were
: 1254 2 7  lost 
: 1256 2 8  and 
: 1258 2 7  ne
: 1260 2 5 ver 
: 1262 4 3  found 
- 1266
: 1267 2 3 Fal
: 1269 2 3 len 
: 1271 3 3 Leaves,
: 1275 2 3  Fal
: 1277 2 3 len 
: 1279 3 3 Leaves, 
: 1283 2 3  Fal
: 1285 2 3 len 
: 1287 4 3  Leaves
- 1291
: 1291 2 3 on 
: 1293 2 5  the 
: 1295 5 2  ground 
- 1300
: 1301 1 0 Run 
: 1302 1 2  a
: 1303 2 3 way 
: 1305 2 0  be
* 1307 2 12 fore 
: 1309 2 5  you 
: 1311 4 7  drown 
- 1315
: 1315 2 7 or 
: 1317 2 7  the 
: 1319 2 7  streets 
: 1321 2 8  will 
: 1323 2 7  beat 
: 1325 2 5  you 
: 1327 4 3  down 
- 1331
: 1331 2 3 Fal
: 1333 2 3 len 
: 1335 3 3 Leaves,
: 1339 2 3  Fal
: 1341 2 3 len 
: 1343 3 3 Leaves, 
: 1347 2 3  Fal
: 1349 2 3 len 
: 1351 4 3  Leaves
- 1355
: 1356 2 3 on 
: 1358 2 5  the 
: 1360 6 2  ground 
- 1366
: 1367 1 0 Run 
: 1368 1 2  a
: 1369 1 3 way 
: 1370 2 0  be
: 1372 2 12 fore 
: 1374 2 10  you 
* 1376 9 7  drown 
- 1385
: 1397 2 3 Fal
: 1399 2 3 len 
: 1401 3 3 Leaves,
: 1405 2 3  Fal
: 1407 2 3 len 
: 1409 3 3 Leaves, 
: 1413 2 3  Fal
: 1415 2 3 len 
: 1417 4 3  Leaves
- 1421
: 1421 2 3 on 
: 1423 2 5  the 
: 1425 5 2  ground
- 1430
: 1432 1 0 Run 
: 1433 1 2  a
: 1434 1 3 way 
: 1435 2 0  be
: 1437 2 12 fore 
: 1439 2 10  you 
* 1441 9 7  drown 
- 1450
: 1462 2 3 Fal
: 1464 2 3 len 
: 1466 3 3 Leaves,
: 1470 2 3  Fal
: 1472 2 3 len 
: 1474 3 3 Leaves, 
: 1478 2 3  Fal
: 1480 2 3 len 
: 1482 4 3  Leaves
- 1486
: 1486 2 3 on 
: 1488 2 5  the 
: 1490 8 2  grou----
: 1498 13 0 nd 
E
