#TITLE:Nights in white satin
#ARTIST:Moody blues
#MP3:Moody blues - Nights in white satin.mp3
#COVER:Moody blues - Nights in white satin [CO].jpg
#BACKGROUND:Moody blues - Nights in white satin [BG].jpg
#BPM:76
#GAP:11200
#ENCODING:UTF8
: 0 2 71 Nights
: 2 2 69  in
: 4 2 67  white
: 6 1 69  sat
: 8 10 69 in
- 20
: 22 1 71 Nev
: 23 1 71 er
: 24 2 71  reach
: 26 3 69 ing
: 29 1 67  the
: 30 10 69  end
- 42
: 48 2 76 Lett
: 50 2 74 ers
: 52 3 72  I've
: 55 2 74  writt
: 57 8 71 en
- 67
: 71 1 69 Nev
: 72 1 69 er
: 73 2 69  mean
: 75 2 67 ing
: 77 1 69  to
: 78 2 67  send
: 80 8 64 -
- 90
: 96 2 71 Beau
: 98 2 69 ty
: 100 1 67  i'd
: 102 7 69  always
: 109 5 69  missed
- 116
: 118 1 71 With
: 119 2 71  these
: 121 3 71  eyes
: 124 1 69 -
: 125 1 67  be
: 126 8 69 fore
- 136
: 144 2 76 Just
: 146 2 74  what
: 148 1 72  the
: 149 5 74  truth
: 154 8 71  is
- 163
: 165 1 69 I
: 166 2 69  can't
: 168 2 69  say
: 171 1 67  a
: 172 1 69 ny
: 173 3 67 more
: 176 4 64 -
- 181
: 182 2 64 Cause
: 184 1 67  i
: 186 1 69  love
: 187 14 69  you
- 203
: 206 1 76  Yes
: 208 1 76  i
: 209 2 74  love
: 211 2 76  you
: 213 7 74 -
- 221
: 222 4 79 Ohhh
: 226 3 78  how
: 230 2 78  i
: 232 4 76  love
: 236 12 76  you
: 248 11 74 -
- 261
: 285 2 71 Gaz
: 287 2 69 ing
: 289 2 67  at
: 291 2 69  peop
: 293 6 69 le
- 301
: 309 2 71 some
: 311 2 69  hand
: 313 2 67  in
: 315 10 69  hand
- 327
: 333 2 76 Just
: 335 2 74  what
: 337 2 72  i'm
: 339 2 74  go
: 341 4 74 ing
: 345 7 69  through
- 353
: 355 2 69 They
: 357 2 69  can't
: 360 1 67  un
: 361 2 69 der
: 363 6 67 stand
: 369 4 64 -
- 375
: 381 2 71 Some
: 383 2 69  try
: 385 1 67  to
: 386 2 69  tell
: 389 8 67  me
- 399
: 401 2 71 Thoughts
: 403 1 71  they
: 405 1 71  can
: 406 2 69 not
: 408 1 67  de
: 409 8 67 fend
- 419
: 428 2 76 Just
: 430 1 74  what
: 431 2 72  you
: 434 2 72  want
: 436 4 74  to
: 440 5 71  be
- 447
: 449 1 69 You'
: 450 1 69 ll
: 451 2 69  be
: 453 1 67  in
: 454 3 69  the
: 457 4 67  end
: 461 2 64 -
- 464
: 465 1 64 And
: 466 2 67  i
: 468 2 69  love
: 470 13 69  you
- 485
: 488 1 76  Yes
: 489 1 76  i
: 491 2 74  love
: 493 6 76  you
: 499 1 74 -
- 502
: 505 4 79 Ohhh
: 509 3 78  how
: 512 2 78  i
: 514 3 76  love
: 517 14 76  you
: 531 6 74 -
- 538
: 538 3 79 Ohhh
: 541 1 78  how
: 542 1 78  i
: 543 7 76  love
: 550 16 76  you
- 568
: 852 2 71 Nights
: 855 1 69  in
: 856 2 67  white
: 858 2 69  sat
: 860 8 69 in
- 870
: 874 1 71 Nev
: 875 1 71 er
: 876 2 71  reach
: 878 3 69 ing
: 881 1 67  the
: 882 10 69  end
- 894
: 900 1 76 Lett
: 901 2 74 ers
: 903 2 72  i've
: 905 2 74  writt
: 907 10 71 en
- 919
: 921 1 69 Nev
: 922 1 69 er
: 923 2 69  mean
: 925 2 67 ing
: 927 2 69  to
: 929 1 67  send
: 930 10 64 -
- 942
: 946 2 71 Beau
: 948 2 69 ty
: 950 2 67  i'd
: 952 5 69  always
: 957 6 69  missed
- 964
: 966 1 71 With
: 967 2 71  these
: 969 2 71  eyes
: 971 2 69 -
: 973 1 67  be
: 974 9 69 fore
- 985
: 991 2 76 Just
: 993 1 74  what
: 994 1 72  the
: 995 5 74  truth
: 1000 8 71  is
- 1009
: 1010 1 69 I
: 1012 1 69  can't
: 1013 3 69  say
: 1016 1 67  a
: 1017 1 69 ny
: 1018 2 67 more
: 1020 6 64 -
- 1026
: 1026 1 64 Cause
: 1027 1 67  i
: 1029 2 69  love
: 1031 16 69  you
- 1048
: 1050 1 76  Yes
: 1051 1 76  i
: 1053 1 74  love
: 1054 6 76  you
: 1060 1 74 -
- 1063
: 1065 4 79 Ohhh
: 1069 3 78  how
: 1073 1 78  i
: 1074 4 76  love
: 1078 12 76  you
: 1090 7 74 -
- 1098
: 1098 2 79 Ohhh
: 1100 1 78  how
: 1101 1 78  i
: 1103 6 76  love
: 1109 13 76  you
- 1124
: 1138 2 64 Cause
: 1140 1 67  i
: 1141 2 69  love
: 1143 16 69  you
- 1160
: 1161 1 76  Yes
: 1163 1 76  i
: 1164 2 74  love
: 1166 7 76  you
: 1173 1 74 -
- 1175
: 1177 4 79 Ohhh
: 1181 1 78  how
: 1183 1 78  i
: 1185 3 76  love
: 1189 12 76  you
: 1201 6 74 -
- 1208
: 1209 2 79 Ohhh
: 1211 1 78  how
: 1213 1 78  i
: 1214 6 76  love
: 1220 10 76  you
: 1230 19 74 -
E