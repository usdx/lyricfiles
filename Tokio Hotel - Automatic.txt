#TITLE:Automatic
#ARTIST:Tokio Hotel
#MP3:Tokio Hotel - Automatic.mp3
#VIDEO:Tokio Hotel - Automatic.mp4
#COVER:Tokio Hotel - Automatic.jpg
#BPM:260.02
#GAP:4450
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Pop
#YEAR:2009
#AUTHOR:michas17j
: 0 2 3 Au
: 4 2 3 to
: 8 2 7 ma
: 12 2 3 tic
- 16
: 64 2 3 Au
: 68 2 3 to
: 72 2 7 ma
: 76 2 5 tic
- 80
: 124 2 3 You're 
: 128 2 3 au
: 132 2 3 to
: 136 2 7 ma
: 140 2 3 tic
- 143
: 144 2 3 And 
: 148 2 3 your 
: 152 6 3 heart's 
: 160 2 3 like 
: 164 2 3 an 
: 168 6 2 en
: 176 12 3 gine
- 190
: 196 2 -2 I 
: 200 6 -2 die 
: 212 2 -2 with 
: 216 6 -2 e
: 228 2 -2 very 
: 232 14 -2 beat
- 248
: 252 2 3 You're 
: 256 2 3 au
: 260 2 3 to
: 264 2 7 ma
: 268 2 3 tic
- 271
: 272 2 3 And 
: 276 2 3 your 
: 280 6 3 voice 
: 288 2 3 is 
: 292 2 3 e
: 296 6 2 le
: 304 12 3 cric
- 318
: 324 2 -2 Why 
: 328 6 -2 do 
: 340 2 -2 I 
: 344 6 -2 sill 
: 356 2 -2 be
: 360 14 -2 live
- 376
: 380 2 3 It's 
: 384 2 3 au
: 388 2 3 to
: 392 2 7 ma
: 396 2 3 tic
- 399
: 400 2 3 E
: 404 2 3 very
: 408 6 3 where 
: 416 2 3 in 
: 420 2 3 your 
: 424 2 2 le
: 428 2 3 ~t
: 432 12 3 ter
- 446
: 452 2 -2 A 
: 456 6 -2 lie 
: 468 2 -2 that 
: 472 6 -2 makes 
: 484 2 -2 me 
: 488 14 -2 bleed
- 504
: 508 2 3 It's 
: 512 2 3 au
: 516 2 3 to
: 520 2 7 ma
: 524 2 3 tic
- 527
: 528 2 3 When 
: 532 2 3 you 
: 536 6 3 say 
: 544 2 3 things 
: 548 2 3 get 
: 552 2 2 be
: 556 2 3 ~t
: 560 12 3 ter
- 574
: 576 2 3 But 
: 580 2 3 they 
: 584 6 2 ne
: 592 22 3 ver...
- 616
: 620 2 3 There's 
: 624 6 3 no 
: 632 12 12 real 
: 648 10 7 love 
: 660 2 5 in 
: 664 12 5 you
- 678
: 684 2 3 There's 
: 688 6 3 no 
: 696 12 12 real 
: 712 10 7 love 
: 724 2 5 in 
: 728 12 5 you
- 742
: 748 2 3 There's 
: 752 6 3 no 
: 760 12 12 real 
: 776 10 7 love 
: 788 2 5 in 
: 792 12 5 you
- 806
: 812 2 3 Why 
: 816 2 3 do 
: 820 2 3 I 
: 824 2 12 kee
: 828 8 14 ~p 
: 840 10 7 lov
: 852 2 5 ing 
: 856 18 5 you
- 876
: 892 2 3 It's 
: 896 2 3 au
: 900 2 3 to
: 904 2 7 ma
: 908 2 3 tic
- 911
: 912 2 3 Count
: 916 2 3 ing 
: 920 6 3 cars 
: 928 2 3 on 
: 932 2 3 a 
: 936 6 2 cross
: 944 12 3 road
- 958
: 964 2 -2 They 
: 968 6 -2 come 
: 980 2 -2 and 
: 984 6 -2 go 
: 996 2 -2 like 
: 1000 14 -2 you
- 1016
: 1020 2 3 It's 
: 1024 2 3 au
: 1028 2 3 to
: 1032 2 7 ma
: 1036 2 3 tic
- 1039
: 1040 2 3 Wat
: 1044 2 3 ching 
: 1048 6 3 fa
: 1056 2 3 ces 
: 1060 2 3 I 
: 1064 6 2 don't 
: 1072 12 3 know
- 1086
: 1092 2 -2 E
: 1096 6 -2 rase 
: 1108 2 -2 the 
: 1112 6 -2 face 
: 1124 2 -2 of 
: 1128 14 -2 you
- 1144
: 1148 2 3 It's 
: 1152 2 3 au
: 1156 2 3 to
: 1160 2 7 ma
: 1164 2 3 tic
- 1167
: 1168 2 3 Sys
: 1172 2 3 te
: 1176 2 7 ma
: 1180 2 3 tic
- 1183
: 1184 2 3 So 
: 1188 2 3 trau
: 1192 2 7 ma
: 1196 2 3 tic
- 1200
: 1212 2 7 You're 
: 1216 2 7 au
: 1220 2 8 to
: 1224 2 7 ma
: 1228 26 5 tic
- 1256
: 1260 2 3 There's 
: 1264 6 3 no 
: 1272 12 12 real 
: 1288 10 7 love 
: 1300 2 5 in 
: 1304 12 5 you
- 1318
: 1324 2 3 There's 
: 1328 6 3 no 
: 1336 12 12 real 
: 1352 10 7 love 
: 1364 2 5 in 
: 1368 12 5 you
- 1382
: 1388 2 3 There's 
: 1392 6 3 no 
: 1400 12 12 real 
: 1416 10 7 love 
: 1428 2 5 in 
: 1432 12 5 you
- 1446
: 1452 2 3 Why 
: 1456 2 3 do 
: 1460 2 3 I 
: 1464 2 12 kee
: 1468 8 14 ~p 
: 1480 10 7 lov
: 1492 2 5 ing 
: 1496 28 5 you
- 1526
: 1536 2 3 Au
: 1540 2 3 to
: 1544 2 7 ma
: 1548 2 3 tic
- 1552
: 1600 2 3 Au
: 1604 2 3 to
: 1608 2 7 ma
: 1612 2 5 tic
- 1616
: 1664 2 3 Au
: 1668 2 3 to
: 1672 2 7 ma
: 1676 2 3 tic
- 1680
: 1728 2 3 Au
: 1732 2 3 to
: 1736 2 7 ma
: 1740 2 5 tic
- 1744
: 1808 6 0 Each 
: 1816 10 3 step
- 1828
: 1840 6 2 You 
: 1848 10 5 make
- 1860
: 1872 6 3 Each 
: 1880 10 7 breath
- 1892
: 1904 6 5 You 
: 1912 6 8 ta
: 1920 6 7 ~ke
- 1928
: 1936 6 0 Your 
: 1944 10 3 heart
- 1956
: 1968 6 2 Your 
: 1976 10 5 soul
- 1988
: 2000 6 3 Re
: 2008 10 7 mote
- 2020
: 2032 6 5 Con
: 2040 6 8 tro
: 2048 6 7 ~lled
- 2056
: 2064 6 7 This 
: 2072 2 3 li
: 2076 2 0 ~fe 
: 2080 6 0 is 
: 2088 10 5 so
: 2100 2 2 ~ 
: 2104 10 2 sick
- 2116
: 2128 6 7 You're 
: 2136 2 3 a
: 2140 2 0 ~
: 2144 6 0 to
: 2152 10 5 ma
: 2164 2 2 ~
: 2168 10 2 tick 
: 2180 2 0 to 
: 2184 18 0 me
- 2204
: 2220 2 3 (There's 
: 2224 6 3 no 
: 2232 12 12 real 
: 2248 10 7 love 
: 2260 2 5 in 
: 2264 12 5 you)
- 2278
: 2284 2 3 (There's 
: 2288 6 3 no 
: 2296 12 12 real 
: 2312 10 7 love 
: 2324 2 5 in 
: 2328 12 5 you)
- 2342
: 2348 2 3 There's 
: 2352 6 3 no 
: 2360 12 12 real 
: 2376 10 7 love 
: 2388 2 5 in 
: 2392 12 5 you
- 2406
: 2476 2 3 There's 
: 2480 6 3 no 
: 2488 12 12 real 
: 2504 10 7 love 
: 2516 2 5 in 
: 2520 12 5 you
- 2534
: 2604 2 3 There's 
: 2608 6 3 no 
: 2616 12 12 real 
: 2632 10 7 love 
: 2644 2 5 in 
: 2648 12 5 you
- 2662
: 2668 2 3 There's 
: 2672 6 3 no 
: 2680 12 12 real 
: 2696 10 7 love 
: 2708 2 5 in 
: 2712 12 5 you
- 2726
: 2732 2 3 There's 
: 2736 6 3 no 
: 2744 12 12 real 
: 2760 10 7 love 
: 2772 2 5 in 
: 2776 12 5 you
- 2790
: 2796 2 3 Why 
: 2800 2 3 do 
: 2804 2 3 I 
: 2808 2 12 kee
: 2812 8 14 ~p 
: 2824 10 7 lov
: 2836 2 5 ing 
: 2840 28 5 you
- 2870
: 2880 2 3 Au
: 2884 2 3 to
: 2888 2 7 ma
: 2892 2 3 tic
- 2896
: 2916 6 10 (There's 
: 2924 6 10 no 
: 2932 6 10 real)
- 2940
: 2944 2 3 Au
: 2948 2 3 to
: 2952 2 7 ma
: 2956 2 5 tic
- 2960
: 2980 6 10 (Love 
: 2988 6 3 in 
: 2996 2 3 you
: 3000 6 2 ~)
- 3007
: 3008 2 3 Au
: 3012 2 3 to
: 3016 2 7 ma
: 3020 2 3 tic
- 3024
: 3044 6 10 (Why 
: 3052 6 8 do 
: 3060 2 7 I
: 3064 1 7 ~
: 3066 4 5 ~)
- 3071
: 3072 2 3 Au
: 3076 2 3 to
: 3080 2 7 ma
: 3084 2 5 tic
- 3088
: 3100 6 10 (Keep 
: 3108 6 10 lov
: 3116 6 10 ing 
: 3124 6 10 you)
- 3132
: 3136 2 3 Au
: 3140 2 3 to
: 3144 2 7 ma
: 3148 2 3 tic
E