#TITLE:Samba De Janeiro
#ARTIST:Bellini
#MP3:Bellini - Samba De Janeiro.mp3
#VIDEO:Bellini - Samba De Janeiro.mp4
#COVER:Bellini - Samba De Janeiro.jpg
#BPM:265.94
#GAP:727
#ENCODING:UTF8
#PREVIEWSTART:70,018
#LANGUAGE:Portuguese
#GENRE:Pop
#EDITION:Fepo
#YEAR:1984
#CREATOR:Fepo
: 0 3 0 Sam
: 4 3 0 ba!
- 9
: 224 2 -3 Sam
: 228 2 -3 ba
: 236 2 -3  de
: 239 2 -3  ja
: 242 5 -3 nei
: 248 3 -3 ro
- 253
: 752 2 0 Sam
: 756 2 0 ba,
- 760
: 768 2 0 Sam
: 772 2 0 ba
: 776 2 0  de
: 779 2 0  ja
: 782 5 0 nei
: 788 3 0 ro
- 793
: 1024 2 0 Sam
: 1028 2 0 ba
: 1036 2 0  de
: 1039 2 0  ja
: 1042 5 0 nei
: 1048 4 0 ro!
- 1054
: 1176 2 9 Sem
: 1179 1 9 pre
: 1181 1 9  as
: 1184 4 4 sim
- 1190
: 1196 2 -3 Em
: 1200 2 7  ci
: 1204 2 7 ma,
: 1212 2 -3  em
: 1216 2 7  ci
: 1220 2 4 mi
: 1224 2 7  ci
: 1228 2 7 mi
: 1232 2 4  ci
: 1236 2 -3 ma
- 1239
: 1240 2 9 Sem
: 1243 1 9 pre
: 1245 1 9  as
: 1248 4 4 sim
- 1254
: 1260 2 -3 Em
: 1264 2 4  bai
: 1268 3 7 xo
: 1276 2 -1  em
: 1280 2 7  bai
: 1284 2 4 xo
: 1288 2 4  bai
: 1292 2 7 xo
: 1296 2 1  bai
: 1300 2 -3 xo
- 1303
: 1303 2 9 Sem
: 1306 1 9 pre
: 1308 1 9  as
: 1311 4 4 sim
- 1317
: 1324 2 -3 Em
: 1328 2 7  ci
: 1332 2 7 ma,
: 1340 2 -3  em
: 1344 2 7  ci
: 1348 2 4 mi
: 1352 2 7  ci
: 1356 2 7 mi
: 1360 2 4  ci
: 1364 2 -3 ma
- 1367
: 1368 2 9 Sem
: 1371 1 9 pre
: 1373 1 9  as
: 1376 4 4 sim
- 1382
: 1388 2 -3 Em
: 1392 2 4  bai
: 1396 3 7 xo
: 1404 2 -1  em
: 1408 2 7  bai
: 1412 2 4 xo
: 1416 2 4  bai
: 1420 2 7 xo
: 1424 2 1  bai
: 1428 2 -3 xo
- 1431
: 1432 2 9 Sem
: 1435 1 9 pre
: 1437 1 9  as
: 1440 4 4 sim
- 1446
: 1452 2 -3 Em
: 1456 2 7  ci
: 1460 2 7 ma,
: 1468 2 -3  em
: 1472 2 7  ci
: 1476 2 4 mi
: 1480 2 7  ci
: 1484 2 7 mi
: 1488 2 4  ci
: 1492 2 -3 ma
- 1495
: 1495 2 9 Sem
: 1498 1 9 pre
: 1500 1 9  as
: 1503 4 4 sim
- 1509
: 1516 2 -3 Em
: 1520 2 4  bai
: 1524 3 7 xo
: 1532 2 -1  em
: 1536 2 7  bai
: 1540 2 4 xo
: 1544 2 4  bai
: 1548 2 7 xo
: 1552 2 1  bai
: 1556 2 -3 xo
- 1559
: 1560 2 9 Sem
: 1563 1 9 pre
: 1565 1 9  as
: 1568 4 4 sim
- 1574
: 1580 2 -3 Em
: 1584 2 7  ci
: 1588 2 7 ma,
: 1596 2 -3  em
: 1600 2 7  ci
: 1604 2 4 mi
: 1608 2 7  ci
: 1612 2 7 mi
: 1616 2 4  ci
: 1620 2 -3 ma
- 1623
: 1624 2 9 Sem
* 1627 1 9 pre
: 1629 1 9  as
* 1632 4 4 sim
- 1638
: 1644 2 -3 Em
: 1648 2 4  bai
: 1652 3 7 xo
: 1660 2 -1  em
: 1664 2 7  bai
: 1668 2 4 xo
: 1672 2 4  bai
: 1676 2 7 xo
: 1680 2 1  bai
: 1684 2 -3 xo
- 1688
: 1712 2 6 Sam
: 1716 4 -3 ba!
- 1722
: 1984 2 -3 Sam
: 1988 2 -3 ba
: 1991 2 -3  de
: 1994 2 -3  ja
: 1997 5 -3 nei
: 2003 3 -3 ro
- 2008
: 2287 2 -3 Sam
: 2291 4 -3 ba
: 2299 2 -3  de
: 2302 2 -3  ja
: 2305 5 -3 nei
: 2311 3 -3 ro
- 2316
: 2832 2 -3 Sam
: 2836 3 -3 ba
: 2848 2 -3  sam
: 2851 2 -3 ba
: 2855 2 -3  de
: 2858 2 -3  ja
: 2861 5 -3 nei
: 2867 3 -3 ro
- 2872
: 2896 3 0 Sam
: 2900 3 0 ba
E