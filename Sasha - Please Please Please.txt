#TITLE:Please Please Please
#ARTIST:Sasha
#MP3:Sasha - Please Please Please.mp3
#VIDEO:Sasha - Please Please Please.avi
#COVER:Sasha - Please Please Please[CO].jpg
#BPM:114
#GAP:17600
#ENCODING:UTF8
#LANGUAGE:English
: 0 2 0 E
: 2 1 1 very
: 3 2 1 bo
: 5 1 2 dy
: 6 1 2 's 
: 7 2 1 tal
: 9 1 0 king 
- 10
: 11 2 0 but 
: 13 1 -2 I'm 
: 15 3 0 out 
: 18 2 0 of 
: 20 1 -1 at
: 21 2 0 ten
: 23 2 0 tion
- 26
: 28 1 1 I 
: 29 2 1 can 
: 32 1 3 on
: 33 1 2 ly 
: 35 2 3 feel
- 37
: 37 2 2 the 
: 39 1 1 in
: 40 2 0 for
: 42 2 0 ma
: 44 1 0 tion 
: 45 2 -1 pas
: 47 2 -1 sing 
: 49 3 0 by
- 53
: 54 1 0 I'm 
: 55 3 1 deep 
: 58 1 1 in
: 59 2 1 side 
: 61 2 0 a 
: 63 2 0 bub
: 65 1 1 ble
- 67
: 67 1 0 and 
: 68 2 1 there's 
: 70 2 0 no 
: 72 2 1 one 
: 74 2 0 here
- 76
: 76 1 0 to 
: 77 2 -1 shout 
: 79 3 1 me 
: 82 6 1 out
- 90
: 110 1 -3 The 
: 111 3 0 storm 
: 114 1 0 in
: 115 2 1 side 
: 117 2 1 my 
: 119 3 1 head
- 123
: 123 2 0 got 
: 125 1 -1 me 
: 126 2 0 tos
: 128 2 0 sing 
: 130 2 1 and 
: 133 2 1 turn
: 135 2 2 ing
- 140
: 141 2 2 Each 
: 143 1 0 and 
: 144 2 1 e
: 146 1 1 very 
: 147 2 2 night
- 149
: 150 1 0 I've 
: 151 2 1 been 
: 153 1 0 a 
: 155 1 0 vic
: 156 2 0 tim 
: 158 1 0 of 
: 159 2 0 my 
: 161 4 1 brain
- 166
: 167 1 0 I 
: 168 2 1 know 
: 171 1 3 I've 
: 172 2 0 got 
: 174 1 1 to 
: 175 2 2 take 
: 177 1 1 a 
: 178 4 0 rest
- 182
: 183 1 0 but 
: 184 2 1 I 
: 186 4 1 just 
: 190 2 0 don't 
: 192 3 2 know 
: 195 3 3 how
- 228
: 230 1 0 My 
: 231 4 1 twis
: 235 3 2 ted 
: 238 3 3 u
: 241 2 4 ni
: 243 4 2 verse
- 247
: 248 3 2 just 
: 251 2 1 won't
: 253 5 0 ~ 
: 261 2 3 let 
: 263 1 3 me 
* 264 3 4 go
* 267 4 7 ~
- 284
: 286 1 1 It 
: 287 3 2 fin
: 290 1 1 al
: 291 3 2 ly 
: 294 3 3 got 
: 297 3 4 me 
: 300 3 3 on 
: 303 4 2 my 
* 308 14 1 knees
* 322 9 2 ~
- 334
* 336 7 5 Please 
* 343 6 10 please 
* 349 4 4 please
* 353 2 4 ~
- 360
: 365 2 4 I 
: 367 3 1 don't 
: 370 4 2 need
- 374
: 375 2 3 an
: 377 5 4 other 
: 382 3 0 night 
: 385 4 2 like 
: 389 3 1 that
- 392
* 393 7 6 Please 
* 400 5 10 please 
* 405 3 5 please
* 408 4 6 ~
- 419
: 421 2 4 Leave 
: 423 2 2 me 
: 425 1 0 a
: 426 4 1 lone
- 431
: 432 1 3 and 
: 433 4 4 don't 
: 437 2 3 you 
: 439 1 3 dare 
: 440 2 2 to 
: 442 3 1 come 
: 445 4 2 back
- 450
: 451 2 4 I 
: 453 2 4 don't 
: 455 2 5 want 
* 457 5 4 you
- 464
: 465 2 4 You 
: 467 2 5 just 
: 469 3 6 came 
* 472 4 4 to
- 476
: 477 2 4 Keep 
: 479 2 4 me 
: 481 1 2 a
: 482 4 3 wake
- 486
: 486 2 3 just 
: 488 1 3 a 
: 489 5 4 mind 
: 494 1 3 on 
: 495 1 2 a 
: 496 5 3 sheet, 
: 501 2 2 so
- 504
* 505 7 5 Please 
* 512 5 5 please 
* 517 3 4 please
* 520 3 3 ~
- 524
: 526 2 0 let 
: 529 3 2 me 
: 532 6 1 sleep
- 580
: 589 2 0 This 
: 591 2 1 is 
: 593 2 1 what 
: 595 1 2 I 
: 596 2 2 call
- 598
: 598 1 0 a 
: 599 3 2 case 
: 602 1 1 of 
: 603 4 -1 mind 
: 607 2 0 o
: 609 1 1 ver 
: 610 2 1 mat
: 612 2 0 ter
- 615
: 617 2 2 Locked 
: 619 2 3 up 
: 621 1 2 in 
: 622 1 3 a 
: 624 2 2 bed
: 626 2 1 room
- 628
: 628 2 1 with 
: 630 1 0 a 
: 631 4 0 world 
: 635 1 0 to 
: 636 5 1 change
- 643
: 644 1 0 The 
: 645 3 2 ghost 
: 648 1 1 in 
: 649 2 2 my 
: 651 1 1 ma
: 652 2 2 chine
- 654
: 654 2 0 is 
: 656 1 2 hav
: 657 2 1 ing 
: 659 2 0 such 
: 661 2 0 a 
: 663 2 -1 good 
: 665 3 0 time
- 668
* 669 3 3 right 
* 672 1 4 now
* 673 1 3 ~
* 674 1 1 ~
- 703
: 707 1 2 And 
: 708 3 1 still 
: 712 2 3 that 
: 714 4 3 troub
: 718 3 4 ling 
: 721 2 3 mad
: 723 5 2 ness
- 727
: 728 7 2 won't 
: 738 2 4 let 
: 740 2 4 me 
* 742 2 6 go
* 744 4 5 ~
- 754
: 762 2 1 So 
: 764 3 2 he
: 767 1 1 re 
: 768 4 2 I 
: 772 3 3 am 
: 775 2 4 down 
: 778 2 3 on 
: 780 3 2 my 
* 785 15 1 knees
* 800 9 2 ~
- 810
* 813 7 5 Please 
* 820 6 10 please 
* 826 4 4 please
* 830 2 4 ~
- 837
: 842 2 4 I 
: 844 3 1 don't 
: 847 4 2 need
- 851
: 852 2 3 an
: 854 5 4 other 
: 859 3 0 night 
: 862 4 2 like 
: 866 3 1 that
- 869
* 870 7 6 Please 
* 877 5 10 please 
* 882 3 5 please
* 885 7 6 ~
- 894
: 898 2 4 Leave 
: 900 2 2 me 
: 902 1 0 a
: 903 4 1 lone
- 908
: 909 1 3 and 
: 910 4 4 don't 
: 914 2 3 you 
: 916 1 3 dare 
: 917 2 2 to 
: 919 3 1 come 
: 922 4 2 back
- 927
: 928 2 4 I 
: 930 2 4 don't 
: 932 2 5 want 
* 934 4 4 you
- 940
: 941 2 4 You 
: 943 2 5 just 
: 945 3 6 came 
* 948 4 4 to
- 953
: 954 2 4 Keep 
: 956 2 4 me 
: 958 1 2 a
: 959 4 3 wake
- 963
: 963 2 3 just 
: 965 1 3 a 
: 966 5 4 mind 
: 971 1 3 on 
: 972 1 2 a 
: 973 5 3 sheet, 
: 978 2 2 so
- 980
* 982 7 5 Please 
* 989 5 5 please 
* 994 3 4 please
* 997 3 3 ~
- 1001
: 1003 2 0 let 
: 1005 4 2 me 
: 1009 7 1 sleep
- 1020
: 1036 2 -1 Well, 
: 1038 2 -2 this 
: 1040 3 0 is 
: 1043 4 -1 not
- 1047
: 1047 3 -1 a 
: 1050 3 0 heal
: 1053 4 0 thy
- 1057
: 1057 4 2 way 
: 1061 3 3 to 
* 1064 4 3 be
* 1068 5 2 ~
- 1079
: 1092 2 1 I 
: 1094 2 1 think 
: 1096 3 0 you
- 1099
: 1099 4 2 had 
: 1103 2 3 your 
: 1105 5 4 fun
- 1110
* 1110 3 4 now 
* 1113 3 1 set 
* 1116 4 2 me 
* 1120 11 3 free
* 1131 2 4 ~
* 1133 2 3 ~
* 1135 1 2 ~
* 1136 2 1 ~
* 1138 4 -1 ~
- 1148
* 1276 7 6 Please 
* 1283 5 10 please 
* 1288 3 5 please
* 1291 4 5 ~
- 1300
: 1304 3 4 I 
: 1307 3 1 don't 
: 1310 4 2 need
- 1315
: 1315 2 3 an
: 1317 5 4 other 
: 1322 3 0 night 
: 1325 4 2 like 
: 1329 3 1 that
- 1332
* 1332 7 6 Please 
* 1339 5 10 please 
* 1344 3 5 please
* 1347 6 6 ~
- 1355
: 1361 2 4 Leave 
: 1363 2 2 me 
: 1365 1 0 a
: 1366 4 1 lone
- 1371
: 1372 1 3 and 
: 1373 4 4 don't 
: 1377 2 3 you 
: 1379 1 3 dare 
: 1380 2 2 to 
: 1382 3 1 come 
: 1385 4 2 back
- 1390
: 1391 2 4 I 
: 1393 2 4 don't 
: 1395 2 5 want 
* 1397 5 4 you
- 1403
: 1404 2 4 You 
: 1406 2 5 just 
: 1408 3 6 came 
* 1411 4 4 to
- 1416
: 1417 2 4 Keep 
: 1419 2 4 me 
: 1421 1 2 a
: 1422 4 3 wake
- 1426
: 1426 2 3 just 
: 1428 1 3 a 
: 1429 4 4 mind 
: 1433 1 3 on 
: 1434 1 2 a 
: 1435 6 3 sheet, 
: 1441 2 2 so
- 1444
* 1445 7 5 Please 
* 1452 5 5 please 
* 1457 3 4 please
* 1460 3 3 ~
- 1464
: 1465 3 0 let 
: 1469 3 2 me 
: 1472 5 1 sleep
E