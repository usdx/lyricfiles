#TITLE:Über sieben Brücken mußt Du gehn
#ARTIST:Peter Maffay
#MP3:Peter Maffay - Über sieben Brücken mußt Du gehn.mp3
#VIDEO:Peter Maffay - Über sieben Brücken mußt Du gehn [VD#-9,2].mp4
#COVER:Peter Maffay - Über sieben Brücken mußt Du gehn [CO].jpg
#BACKGROUND:Peter Maffay - Über sieben Brücken mußt Du gehn [BG].jpg
#BPM:152
#GAP:14000
#VIDEOGAP:-9,2
#ENCODING:UTF8
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
: 0 2 11 Manch
: 4 3 14 mal
* 8 3 16  geh'
: 12 2 14  ich
: 16 3 14  mei
: 20 2 9 ne
- 23
: 24 3 11 Stra
: 29 2 7 ße
: 33 3 9  oh
: 37 1 11 ne
: 39 4 11  Blick.
- 45
: 65 3 11 Manch
: 70 3 14 mal
: 74 3 16  wünsch'
: 78 2 14  ich
: 82 3 14  mir
- 86
: 86 3 11 mein
: 90 3 11  Schau
: 94 3 9 kel
: 99 3 9 pferd
: 105 1 9  zu
: 107 3 9 rück.
- 112
: 130 2 9 Manch
: 134 3 9 mal
: 138 2 12  bin
: 142 2 12  ich
- 145
: 146 3 12 oh
: 150 3 12 ne
: 154 4 11  Rast
: 162 2 7  und
: 166 3 4  Ruh.
- 171
: 178 3 7 Manch
: 182 3 7 mal
: 187 4 9  schließ'
: 194 2 9  ich
: 198 6 9  a
: 206 2 9 lle
- 209
: 211 3 9 Tü
: 215 2 9 ren
: 219 2 11  nach
: 223 2 9  mir
: 227 2 9  zu.
- 231
: 259 2 11 Manch
: 263 3 14 mal
: 267 2 16  ist
: 271 2 14  mir
: 275 3 14  kalt
- 279
: 280 2 9 und
: 283 2 11  manch
: 287 5 7 mal
: 295 2 11  he
* 298 4 11 ~iß.
- 304
: 324 3 11 Manch
: 328 3 14 mal
: 332 3 16  weiß
: 337 2 14  ich
: 340 2 14  nicht
: 344 3 11  mehr,
- 348
: 348 2 11 was
: 352 2 9  ich
: 356 5 9  weiß.
- 363
: 389 2 9 Manch
: 393 3 9 mal
: 397 2 12  bin
: 401 2 12  ich
: 405 3 12  schon
- 409
: 409 2 12 am
: 413 5 11  Mor
: 421 2 7 gen
: 425 6 4  müd.
- 433
: 446 2 12 Und
: 450 2 12  dann
: 454 2 12  such
: 458 2 14  ich
: 462 3 11  Trost
- 466
: 466 2 14 in
: 470 1 11  e
: 472 1 9 ~i
: 474 1 7 nem
: 476 2 9  Lie
* 479 15 7 ~d.
- 496
: 501 3 7 Ü
: 505 2 9 ber
: 510 7 9  sie
: 518 2 9 ben
* 522 6 11  Brü
: 530 2 11 cken
- 533
: 534 2 11 musst
: 538 1 7  du
: 540 5 9  geh'n,
- 547
: 565 3 9 sie
: 569 2 9 ben
: 573 6 11  dunk
: 581 2 14 le
- 584
* 585 6 16 Jah
: 593 2 14 re
: 597 2 11  ü
: 601 1 7 ber
: 603 7 9 steh'n!
- 612
: 628 3 9 Sie
: 632 2 9 ben
: 636 5 9  mal
: 644 2 9  wirst
: 648 5 11  du
- 654
: 656 2 11 die
: 660 2 11  A
: 663 2 7 sche
: 666 6 4  sein,
- 674
: 692 3 9 A
: 696 2 11 ber
: 700 3 12  ein
: 704 2 12 mal
: 708 2 12  auch
- 711
: 712 2 14 der
: 716 3 11  he
: 720 3 9 ~
: 724 3 7 lle
: 728 2 9  Schei
: 731 24 7 ~n.
- 757
: 870 3 11 Manch
: 874 3 14 mal
: 878 4 16  scheint
: 883 2 14  die
: 887 3 14  Uhr
: 891 2 9  des
- 894
: 895 3 11 Le
: 899 2 7 bens
: 903 2 9  still
: 907 1 11  zu
: 909 6 11  steh'n.
- 917
: 935 3 11 Manch
: 939 3 14 mal
* 943 3 16  scheint
: 947 2 14  man
: 951 3 14  i
: 955 2 11 mmer
- 958
: 960 2 11 nur
: 964 2 9  im
: 968 4 9  Kreis
: 975 1 9  zu
: 977 5 9  geh'n.
- 984
: 1000 2 9 Manch
: 1004 3 9 mal
: 1008 2 12  ist
: 1012 2 12  man
: 1016 2 12  wie
- 1019
: 1020 2 12 von
: 1024 4 11  Fern
: 1032 2 7 weh
: 1036 5 4  krank.
- 1043
: 1048 2 7 Manch
: 1052 2 7 mal
: 1056 3 9  sitzt
: 1063 2 9  man
: 1067 4 9  still
- 1073
: 1075 2 9 auf
: 1079 3 11  ei
: 1083 1 9 ner
: 1085 4 9  Bank.
- 1091
: 1127 3 11 Manch
: 1131 3 14 mal
* 1135 3 16  greift
: 1140 2 14  man
: 1144 2 14  nach
- 1147
: 1147 2 9 der
: 1151 3 11  gan
: 1155 4 7 zen
: 1163 2 11  We
: 1166 4 11 ~lt.
- 1172
: 1191 2 11 Manch
: 1195 3 14 mal
: 1199 3 16  meint
: 1203 2 14  man,
- 1206
: 1207 2 14 dass
: 1211 2 11  der
: 1215 2 11  Glück
: 1219 2 9 stern
: 1223 4 9  fällt.
- 1229
: 1254 2 9 Manch
: 1258 3 9 mal
: 1262 2 12  nimmt
: 1266 3 12  man,
- 1270
: 1270 3 12 wo
: 1274 3 12  man
: 1278 3 11  lie
: 1282 2 9 ~
: 1286 2 7 ber
: 1290 5 4  gibt.
- 1297
: 1310 2 12 Manch
: 1314 3 12 mal
: 1318 2 12  hasst
: 1322 3 14  man
: 1326 2 11  das,
- 1329
: 1330 2 11 was
: 1334 1 11  ma
: 1336 1 9 ~n
: 1338 1 7  doch
: 1340 1 9  lie
* 1342 13 7 ~bt.
- 1357
: 1365 3 7 Ü
: 1369 2 9 ber
: 1373 6 9  sie
: 1381 2 9 ben
- 1384
: 1384 5 11 Brü
: 1392 2 11 cken
: 1396 2 11  musst
: 1400 2 7  du
: 1403 4 9  geh'n,
- 1409
: 1428 3 9 sie
: 1432 2 9 ben
: 1436 3 11  du
: 1440 2 14 ~
: 1444 2 14 nkle
- 1447
* 1448 6 16 Jah
: 1456 2 14 re
: 1460 2 11  ü
: 1464 1 7 ber
: 1466 5 9 steh'n!
- 1473
: 1491 3 9 Sie
: 1495 2 9 ben
: 1499 5 9  mal
: 1507 2 9  wirst
: 1511 4 11  du
: 1519 2 11  die
: 1523 1 11  A
: 1525 2 7 sche
: 1529 5 4  sein,
- 1536
: 1555 3 9 a
: 1559 2 11 ber
: 1563 3 12  ein
: 1567 2 12 mal
: 1571 2 12  auch
- 1574
: 1575 2 14 der
: 1579 3 11  he
: 1583 2 9 ~
: 1587 2 7 lle
: 1591 2 9  Schei
* 1594 15 7 ~n.
- 1611
: 1619 3 7 Ü
: 1623 2 9 ber
: 1627 6 9  sie
: 1635 2 9 ben
- 1638
* 1639 6 11 Brü
: 1647 2 11 cken
: 1651 2 11  musst
: 1655 1 7  du
: 1657 5 9  geh'n,
- 1664
: 1684 3 9 sie
: 1688 2 9 ben
: 1692 3 11  du
: 1696 2 14 ~
: 1700 2 14 nkle
- 1703
* 1704 6 16 Ja
: 1712 2 14 hre
: 1716 3 11  ü
: 1720 1 7 ber
: 1722 5 9 steh'n!
- 1729
: 1748 3 9 Sie
: 1752 2 9 ben
: 1756 5 9  mal
: 1764 2 9  wirst
: 1768 3 11  du
- 1773
: 1776 2 11 die
: 1780 1 11  A
: 1782 2 7 sche
: 1787 4 4  sein,
- 1793
: 1813 3 9 a
: 1817 2 11 ber
: 1821 3 12  ein
: 1825 2 12 mal
: 1829 2 12  auch
- 1832
: 1833 2 14 der
: 1837 3 11  he
: 1841 2 9 ~
: 1845 2 7 lle
: 1849 2 9  Schei
* 1852 26 7 ~n.
- 1880
: 2134 3 7 Ü
: 2138 2 9 ber
* 2143 6 9  sie
: 2151 2 9 ben
- 2154
: 2155 5 11 Brü
: 2163 2 11 cken
: 2167 2 11  musst
: 2171 1 7  du
: 2173 4 9  geh'n,
- 2179
: 2199 3 9 sie
: 2203 2 9 ben
: 2207 3 11  du
: 2211 2 14 ~
: 2215 2 14 nkle
- 2218
* 2219 6 16 Ja
: 2227 2 14 hre
: 2231 2 11  ü
: 2235 1 7 ber
: 2237 5 9 steh'n!
- 2244
: 2263 3 9 Sie
: 2267 2 9 ben
: 2271 4 9  mal
: 2279 2 9  wirst
: 2283 3 11  du
- 2288
: 2291 2 11 die
: 2295 1 11  A
: 2297 2 7 sche
: 2301 5 4  sein,
- 2308
: 2327 3 9 a
: 2331 2 11 ber
: 2335 3 12  ein
: 2339 2 12 mal
: 2343 2 12  auch
: 2347 2 14  der
- 2350
: 2351 3 11 he
: 2355 2 9 ~
: 2359 2 7 lle
: 2363 2 9  Schei
* 2366 23 7 ~n.
E