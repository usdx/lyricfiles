#TITLE:Seemann
#ARTIST:Rammstein
#EDITION:[SC]-Songs
#LANGUAGE:German
#MP3:Rammstein - Seemann.mp3
#COVER:Rammstein - Seemann [CO].jpg
#BACKGROUND:Rammstein - Seemann [BG].jpg
#VIDEO:Rammstein - Seemann [VD#0].avi
#PREVIEWSTART:116,052
#MedleyStartBeat:1783
#MedleyEndBeat:2251
#BPM:280,34
#GAP:20650
: 0 36 1 Komm
: 48 6 -1  in
: 59 3 -4  mein
: 64 15 -1  Boot!
- 81
: 123 2 -1 Ein
: 128 6 -4  Sturm
: 136 2 -3  kommt
: 140 7 -3  auf
- 149
: 168 4 -3 und
: 176 2 -3  es
: 180 6 -3  wird
: 192 5 -4  Nacht.
- 199
* 256 36 1 Wo
: 304 6 -1  willst
: 316 2 -4  du
: 320 10 -1  hin?
- 332
: 379 2 1 So
: 384 5 -4  ganz
: 392 2 -3  al
: 396 7 -3 lein
- 405
: 424 4 -3 treibst
: 432 2 -3  du
: 436 8 -3  da
: 448 10 -4 von.
- 460
: 512 36 1 Wer
: 551 5 1  hält
: 560 10 -1  dei
: 572 2 -4 ne
: 576 13 -1  Hand,
- 591
: 640 2 -3 wenn
: 644 4 -3  es
: 652 6 -3  dich
- 660
: 676 5 -3 nach
: 684 6 -3  un
: 696 3 -4 ten
: 704 16 -4  zieht?
- 722
: 1024 38 1 Wo
: 1072 7 -1  willst
: 1084 2 -4  du
: 1088 13 -1  hin?
- 1103
: 1148 2 -1 So
: 1152 5 -4  u
: 1160 2 -3 fer
: 1164 11 -3 los,
- 1177
: 1192 4 -4 die
* 1200 6 1  kal
: 1212 2 -3 te
: 1216 14 -4  See.
- 1232
: 1280 37 1 Ko
: 1320 5 -1 ~mm
: 1328 8 -1  in
: 1340 2 -4  mein
: 1345 11 -1  Boot!
- 1358
: 1405 2 -1 Der
: 1409 5 -4  Herbst
: 1417 2 -3 wind
: 1421 11 -3  hält
- 1434
: 1449 5 -3 die
: 1457 3 -3  Se
: 1462 5 -3 gel
: 1474 7 -4  straff.
- 1483
: 1783 4 -13 Jetzt
: 1793 8 -11  stehst
: 1805 2 -11  du
: 1809 12 -11  da
: 1833 3 -11  an
: 1840 7 -9  der
: 1851 3 -8  La
* 1857 11 -9 ter
: 1873 6 -13 ne,
- 1881
: 1911 4 -6 mit
: 1921 12 -6  Trä
: 1937 19 -6 nen
: 1969 6 -8  im
: 1980 2 -9  Ge
: 1985 5 -8 sicht.
- 1992
: 2040 4 -13 Das
: 2048 9 -11  Ta
: 2060 2 -11 ges
: 2064 9 -11 licht
: 2088 5 -11  fällt
: 2097 7 -9  auf
: 2108 2 -8  die
: 2113 11 -9  Sei
: 2129 11 -13 te,
- 2142
: 2172 2 -6 der
: 2177 4 -6  Herbst
: 2184 3 -6 wind
: 2189 10 -6  fegt
: 2213 5 -6  die
: 2225 7 -8  Stra
: 2237 2 -9 ße
* 2241 10 -8  leer.
- 2253
: 2433 44 1 Komm
: 2481 6 -1  in
: 2493 2 -3  mein
: 2497 18 -1  Boot!
- 2517
: 2557 2 -1 Die
: 2561 6 -3  Sehn
: 2571 3 -3 sucht
: 2577 7 -3  wird
- 2586
: 2600 4 -3 der
: 2609 3 -3  Steu
: 2614 5 -3 er
: 2625 8 -4 mann.
- 2635
: 2690 36 1 Ko
: 2729 6 -1 ~mm
: 2737 7 -1  in
: 2749 3 -1  mein
: 2754 15 -1  Boot!
- 2771
: 2813 3 -1 Der
: 2817 4 -3  bes
: 2825 2 -3 te
* 2829 6 -3  See
: 2838 8 -3 mann
- 2848
: 2865 8 -3 war
: 2879 2 -4  doch
: 2884 5 -4  ich.
- 2891
: 2937 3 -9 Jetzt
: 2945 6 -11  stehst
: 2957 2 -11  du
: 2961 10 -11  da
: 2985 5 -11  an
: 2993 7 -9  der
: 3005 2 -8  La
: 3009 11 -9 ter
: 3025 11 -9 ne,
- 3038
: 3065 3 -6 hast
* 3073 14 -6  Trä
: 3090 14 -6 nen
: 3121 7 -8  im
: 3133 2 -9  Ge
: 3137 6 -8 sicht.
- 3145
: 3193 3 -11 Das
: 3201 3 1  Feu
: 3205 5 -11 er
: 3217 9 -11  nimmst
: 3241 4 -11  du
: 3249 7 -9  von
: 3261 2 -8  der
: 3265 9 -9  Ker
: 3281 9 -13 ze.
- 3292
: 3326 2 -6 Die
: 3330 4 -6  Zeit
: 3338 2 -6  steht
: 3342 12 -6  still
: 3367 6 -6  und
: 3377 3 -8  es
: 3382 6 -9  wird
: 3394 8 -8  Herbst.
- 3404
: 3449 3 -9 Sie
: 3458 9 -11  spra
: 3470 2 -11 chen
: 3474 11 -11  nur
: 3497 5 -11  von
: 3506 7 -9  dei
: 3517 3 -8 ner
: 3521 5 -9  Mut
: 3529 10 -13 ter.
- 3541
: 3577 5 -6 So
: 3585 7 -6  gna
: 3594 2 -6 den
* 3598 13 -6 los
: 3624 4 -6  ist
: 3634 7 -8  nur
: 3645 3 -9  die
: 3650 6 -8  Nacht.
- 3658
: 3704 4 -9 Am
: 3713 6 -11  En
: 3725 2 -11 de
: 3730 9 -11  bleib'
: 3752 4 -9  ich
: 3762 5 -8  doch
: 3774 2 -8  al
: 3778 10 -9 lei
: 3794 11 -9 ne.
- 3807
: 3836 3 -4 Die
: 3841 5 -6  Zeit
: 3848 2 -6  steht
: 3853 9 -6  still
- 3864
: 3878 6 -6 und
: 3890 3 -8  mir
: 3894 4 -9  ist
: 3906 10 -8  kalt,
- 3918
: 3938 11 -8 kalt,
- 3951
: 3969 3 -6 ka
: 3973 4 -8 ~lt,
- 3979
: 3999 10 -8 kalt,
- 4011
: 4034 6 -8 kalt...
E
