#TITLE:Hips Don´t Lie
#ARTIST:Shakira feat. Wyclef Jean
#MP3:Shakira - Hips Dont Lie.mp3
#VIDEO:Shakira - Hips Dont Lie.mkv
#COVER:shakira hips.jpg
#BPM:300
#GAP:9350
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:R&B
#EDITION:Ante Eppinger
: 0 1 13 I
: 3 2 17  ne
: 6 1 17 ver
: 9 2 17  real
: 12 1 17 ly
: 15 2 17  knew
- 18
: 18 2 15 that
: 21 1 13  she
: 24 1 15  could
: 27 3 17  dance
: 33 3 20  like
: 39 7 20  this.
- 48
: 54 2 15 She
: 57 2 15  makes
: 60 1 15  a
: 63 2 15  ma
: 66 1 13 ~n
- 68
: 69 2 13 wants
: 72 1 15  to
: 75 2 17  speak
: 81 3 20  Spa
: 87 7 20 nish.
- 96
: 102 2 15 Co
: 105 2 15 mo
: 108 2 17  se
: 111 3 15  lla
: 117 4 13 ma,
- 123
: 132 2 17 bo
: 135 3 15 ni
: 141 5 13 ta?
- 148
: 156 2 17 Mi
: 159 3 15  ca
: 165 4 13 sa,
- 171
: 180 2 17 su
: 183 3 15  ca
: 189 5 13 sa.
- 196
: 201 3 17 Oh
: 207 2 15  ba
: 210 2 13 by,
: 213 2 13  when
: 216 1 15  you
: 219 3 17  talk
: 225 3 20  like
* 231 5 20  that,
- 238
: 249 2 20 you
: 252 2 22 ~
: 255 5 20  make
: 264 2 17  a
: 267 2 20  wo
: 270 2 17 man
: 273 3 15  go
: 279 6 13  mad.
- 287
: 300 2 17 So
: 303 3 15  be
: 309 5 13  wise
- 316
: 324 2 17 and
: 327 2 17  kee
: 330 1 18 ~p
: 334 6 15  on
- 342
: 352 3 15 rea
: 357 2 15 ding
: 360 1 13  the
: 363 3 15  signs
: 369 1 15  of
: 372 1 15  my
: 375 4 15  bo
: 381 4 10 dy.
- 387
: 399 10 22 I'm
: 411 3 20  on
: 417 2 22  to
: 420 3 20 night,
- 424
: 426 2 20 you
: 429 1 20  know
: 432 1 20  my
: 435 3 17  hips
: 441 3 20  don't
: 447 4 17  lie
- 452
: 453 2 17 and
: 456 1 17  I'm
: 459 2 15  star
: 462 2 15 ting
: 465 1 17  to
: 468 3 15  feel
: 473 2 13  it's
: 477 5 10  right.
- 484
: 489 1 10 All
: 491 1 10  the
: 493 1 8  at
: 495 3 10 trac
: 501 5 8 tion,
- 508
: 516 2 10 the
: 519 3 12  ten
: 525 4 10 sion,
- 531
: 537 2 10 don't
: 540 2 10  you
* 543 3 13  see
: 549 2 10  ba
: 552 1 10 by,
- 554
: 556 2 13 this
: 559 1 10  is
: 564 2 10  per
: 567 3 13 fec
: 573 5 10 tion?
- 580
: 586 4 5 Hey
: 592 6 3  Girl,
- 600
: 605 2 3 I
: 609 2 1  can
: 613 2 3  see
: 617 3 3  your
: 622 2 1  bo
: 625 2 3 dy
: 629 4 3  mo
: 636 6 -2 ving,
- 644
: 647 2 -4 and
: 650 2 -4  it's
: 654 3 -4  dri
: 658 2 -2 ving
: 661 2 1  me
* 666 4 3  cra
: 672 5 1 zy.
- 679
: 682 2 3 A
: 685 1 5 ~nd
: 688 6 3  I
- 696
: 701 2 3 did
: 704 1 3 n't
: 706 2 1  have
: 709 2 3  the
: 714 3 3  sligh
: 718 2 1 test
: 722 3 3  i
: 727 3 3 de
: 732 4 -2 a,
- 738
: 741 2 -4 un
: 744 2 -4 til
: 747 3 -4  I
: 753 2 -2  saw
: 756 2 1  you
: 761 3 3  dan
: 767 5 1 cing.
- 773
: 775 1 5 And
: 778 1 5  when
: 781 1 5  you
* 784 2 8  walk
: 787 2 5  up
: 790 1 3  on
: 793 2 1  the
: 796 4 3  dance
: 804 2 3  floor,
- 807
: 809 2 5 no
: 812 2 3 bo
: 815 1 1 dy
: 818 1 0  can
: 821 2 -2 not
: 824 2 5  i
: 829 4 5 gnore
- 835
: 841 1 1 the
: 844 2 1  way
: 847 2 0  you
: 850 2 -2  move
: 853 2 0  your
: 856 2 1  bo
: 859 1 0 dy,
: 862 5 -2  girl.
- 868
: 870 2 5 And
: 873 2 5  e
: 876 2 5 very
: 879 2 8 thing's
: 882 2 5  so
: 885 2 3  un
: 888 2 1 ex
: 893 3 3 pec
: 898 2 3 ted,
- 901
: 901 2 1 the
: 904 2 1  way
: 907 2 0  you
: 910 2 -2  right
: 913 2 0  and
: 916 2 1  le
: 919 1 0 ~ft
: 923 4 -2  it,
- 929
: 936 2 -2 so
: 939 2 1  you
: 942 2 0  can
: 945 2 -2  keep
: 948 2 0  on
: 951 2 1  ta
: 954 1 0 king
: 957 2 -2  it.
- 960
: 960 2 13 I
: 963 2 17  ne
: 966 1 17 ver
: 969 2 17  real
: 972 1 17 ly
: 975 2 17  knew,
- 978
: 978 2 15 that
: 981 2 13  she
: 984 1 15  could
: 987 3 17  dance
: 993 3 20  like
: 999 5 20  this.
- 1006
: 1014 2 15 She
: 1017 2 15  makes
: 1020 1 15  a
: 1023 2 15  ma
: 1026 1 13 ~n
- 1028
: 1029 2 13 want
: 1032 1 15  to
: 1035 2 17  speak
: 1041 3 20  Spa
: 1047 5 20 nish.
- 1054
: 1062 2 15 Co
: 1065 1 15 mo
: 1068 2 17  se
: 1071 4 15  lla
: 1077 4 13 ma,
- 1083
: 1092 2 17 bo
: 1095 4 15 ni
: 1101 4 13 ta?
- 1107
: 1116 2 17 Mi
: 1120 2 15  ca
: 1125 6 13 sa,
- 1133
* 1140 2 17 su
: 1143 3 15  ca
: 1149 5 13 sa.
- 1156
: 1161 4 17 Oh
: 1167 2 15  ba
: 1170 2 13 by,
: 1173 2 13  when
: 1176 1 15  you
: 1179 3 17  talk
: 1185 3 20  like
: 1191 6 20  that,
- 1199
: 1208 2 20 you
* 1211 2 22 ~
: 1215 6 20  make
: 1224 2 17  a
: 1227 2 20  wo
: 1230 2 17 man
: 1233 3 15  go
: 1239 6 13  mad.
- 1247
: 1259 2 17 So
: 1263 3 15  be
: 1269 6 13  wise
- 1277
: 1284 2 17 and
: 1287 2 17  kee
: 1290 1 18 ~p
: 1293 6 15  on
- 1301
: 1311 3 15 rea
: 1317 2 15 ding
: 1320 1 13  the
: 1323 4 15  signs
: 1329 1 15  of
: 1332 1 15  my
: 1335 4 15  bo
: 1341 5 10 dy.
- 1348
* 1359 8 22 I'm
: 1371 3 20  on
: 1377 1 22  to
: 1380 4 20 night,
- 1385
: 1385 2 20 you
: 1388 2 20  know
: 1391 1 20  my
: 1394 3 17  hips
: 1401 3 20  don't
: 1407 3 17  lie,
- 1411
: 1413 2 17 and
: 1416 1 17  I'm
: 1419 2 15  star
: 1422 2 15 ting
: 1425 1 17  to
: 1428 3 15  feel
: 1433 2 13  you
: 1437 4 10  boy.
- 1443
: 1449 2 10 Come
: 1452 2 10  on
: 1455 3 13  let's
: 1461 2 10  go
: 1464 2 8 ~,
- 1468
: 1479 3 13 real
: 1485 4 10  slow.
- 1491
: 1497 2 10 Don't
: 1500 1 10  you
: 1503 3 13  see,
: 1507 2 10  ba
: 1510 1 10 by,
: 1512 1 10  a
: 1515 2 13  si
: 1518 2 10  es
: 1523 2 10  per
: 1527 2 13 fec
: 1533 5 10 to?
- 1540
: 1554 2 22 I
: 1557 2 22  know
: 1560 1 22  I'm
: 1563 3 20  on
: 1569 1 22  to
: 1572 6 20 night,
- 1579
: 1581 2 20 so
: 1584 1 20  my
: 1587 2 17  hips
: 1593 3 20  don't
: 1599 4 17  lie,
- 1604
: 1605 1 17 and
: 1607 2 17  I'm
: 1611 2 15  star
: 1614 2 15 ting
: 1617 1 17  to
: 1620 3 15  feel
: 1625 1 13  it's
: 1629 4 10  right.
- 1635
: 1641 1 10 All
: 1643 1 10  the
: 1645 1 8  a
: 1647 2 10 trac
: 1653 5 8 tion,
- 1660
: 1668 2 10 the
: 1671 3 13  ten
: 1677 5 10 sion.
- 1684
: 1689 2 10 Don't
: 1692 1 10  you
: 1695 3 13  see
: 1700 2 10  ba
: 1703 1 10 by,
: 1707 2 13  this
: 1711 1 10  is
: 1715 2 10  per
: 1719 3 13 fec
: 1725 5 10 tion?
- 1732
: 1738 4 17 Oh
* 1744 7 15  boy,
- 1753
: 1756 2 15 I
: 1759 2 13  can
: 1764 2 15  see
: 1767 2 13  your
: 1771 3 13  bo
: 1775 2 15 dy
: 1779 5 15  mo
: 1789 3 10 ving,
- 1794
: 1797 3 8 half
: 1803 2 8  a
: 1806 2 10 ni
: 1809 2 13 mal,
: 1813 2 10  half
: 1818 5 15  ma
: 1824 6 13 ~n.
- 1834
: 1834 3 17 I
: 1840 6 15  don't,
- 1848
: 1851 2 15 don't
: 1855 2 13  real
: 1859 2 15 ly
: 1863 2 15  know
: 1867 2 13  what
: 1871 2 15  I'm
: 1876 4 15  doi
: 1884 5 10 ng,
- 1893
: 1893 2 8 but
: 1896 1 8  you
: 1899 2 8  seem
: 1902 1 10  to
: 1905 2 13  have
: 1908 2 10  a
* 1914 5 15  pla
: 1920 3 13 ~n.
- 1924
: 1926 4 20 My
: 1931 2 22 ~
: 1936 5 17  will
: 1943 1 18  and
: 1947 4 20  self
: 1953 2 22  res
: 1959 2 17 trai
: 1962 2 15 ~
: 1965 5 13 ~nt
- 1974
: 1974 2 15 ha
: 1977 3 17 ~ve
: 1984 4 12  come
- 1992
: 1992 1 12 to
: 1995 2 13  fai
: 1998 2 12 ~l
: 2001 3 10  now,
: 2007 2 13  fai
: 2010 2 12 ~l
: 2013 2 10  now.
- 2017
: 2023 3 17 See,
: 2028 2 17  I'm
* 2031 2 20  do
: 2034 2 17 ing
: 2037 2 15  what
: 2040 1 13  I
: 2043 4 15  can,
- 2048
: 2049 2 15 but
: 2052 1 15  I
: 2055 2 17  ca
: 2058 1 15 ~n't,
: 2061 2 13  so
: 2064 2 12  you
: 2067 2 10  know,
- 2070
: 2070 3 17 that's
: 2076 2 17  a
: 2079 2 18  bit
: 2085 3 13  too
: 2090 3 13  hard
: 2096 2 15  to
: 2099 1 13  exp
: 2103 2 13 lai
: 2106 4 15 ~n.
- 2112
: 2124 3 22 Bai
: 2129 2 22 la
: 2132 1 22  en
: 2134 2 22  la
: 2139 4 20  cal
: 2145 3 20 le
: 2155 2 17  de
: 2160 3 20  no
: 2166 3 20 che.
- 2170
: 2172 3 20 Bai
: 2178 2 20 la
: 2181 1 20  en
: 2183 2 20  la
: 2187 4 17  cal
: 2193 3 17 le
: 2203 2 15  de
: 2209 3 17  di
: 2214 3 10 a.
- 2218
: 2220 3 17 Bai
: 2225 2 17 la
: 2228 1 17  en
: 2230 2 17  la
: 2235 3 15  cal
: 2241 3 15 le
: 2251 3 17  de
: 2256 3 15  no
: 2262 3 15 che.
- 2266
: 2268 4 15 Bai
: 2273 2 15 la
: 2276 1 15  en
: 2278 2 15  la
: 2283 2 13  cal
: 2286 2 12 ~
: 2289 5 10 le...
- 2296
: 2304 2 13 I
: 2307 2 17  ne
: 2310 1 17 ver
: 2313 2 17  real
: 2316 1 17 ly
: 2319 2 17  knew,
- 2322
: 2322 2 15 that
: 2325 2 13  she
: 2328 1 15  could
: 2331 3 17  dance
: 2337 3 20  like
* 2343 8 20  this.
- 2353
: 2358 1 15 She
: 2361 2 15  makes
: 2364 2 15  a
: 2367 2 15  ma
: 2370 2 13 ~n
- 2373
: 2373 2 13 wants
: 2376 1 15  to
: 2379 3 17  speak
: 2385 3 20  Spa
: 2391 8 20 nish.
- 2401
: 2406 2 15 Co
: 2409 1 15 mo
: 2412 2 17  se
: 2415 3 15  lla
: 2421 4 13 ma,
- 2427
: 2436 2 17 bo
: 2439 3 15 ni
: 2445 5 13 ta?
- 2452
: 2460 2 17 Mi
: 2463 3 15  ca
: 2469 4 13 sa,
- 2475
: 2484 2 17 su
: 2487 3 15  ca
: 2493 5 13 sa.
- 2500
: 2505 3 17 Oh
: 2511 2 15  ba
: 2514 2 13 by,
: 2517 2 13  when
: 2520 1 15  you
: 2523 3 17  talk
: 2529 3 20  like
: 2535 5 20  that,
- 2542
: 2553 2 20 you
: 2556 2 22 ~
: 2559 5 20  know,
: 2568 1 17  you
: 2571 1 20  got
: 2574 1 17  me
: 2577 2 15  hyp
: 2580 2 17 no
: 2583 5 13 tized.
- 2590
: 2604 1 17 So
: 2607 3 15  be
: 2613 6 13  wise,
- 2621
: 2628 1 17 and
: 2631 2 17  kee
* 2634 2 18 ~p
: 2638 6 15  on
- 2646
: 2656 3 15 rea
: 2661 2 15 ding
: 2664 1 15  the
: 2667 3 15  signs
: 2673 1 15  of
: 2676 1 15  my
: 2679 4 15  bo
: 2685 4 10 dy.
- 2691
: 2695 2 13 Se
: 2700 2 17 no
: 2703 3 15 ri
: 2709 4 13 ta,
- 2715
: 2719 3 13 feel
: 2724 2 17  the
: 2727 3 15  con
: 2733 4 13 ga.
- 2738
: 2739 2 13 Let
: 2742 1 13  me
: 2745 1 13  see
: 2748 2 17  you
: 2751 4 15  move,
- 2756
: 2757 2 13 like
: 2760 2 13  you
: 2763 3 13  come
: 2769 2 13  from
: 2772 2 17  Co
: 2775 3 15 lom
: 2781 5 13 bia.
- 2788
: 2933 2 10 Mi
: 2936 2 13 ~
: 2939 2 10 ra
: 2943 3 17  en
: 2947 2 15  Bar
: 2951 2 13 ran
: 2955 2 15 quil
: 2959 2 13 la
- 2962
: 2964 2 12 se
: 2967 3 13  bai
: 2972 2 12 la~a
: 2976 2 10 sí.
* 2981 5 22  Say!
- 2988
: 2991 3 17 En
: 2996 3 15  Bar
: 3001 2 13 ran
: 3006 2 15 quil
: 3010 2 13 la
: 3014 2 12  se
: 3017 3 13  bai
: 3021 2 12 la~a
: 3026 4 10 sí
- 3032
* 3519 8 22 I'm
: 3531 3 20  on
: 3537 2 22  to
: 3540 7 20 night,
- 3551
: 3551 2 20 my
: 3555 3 17  hips
: 3561 3 20  don't
: 3567 3 17  lie,
- 3571
: 3573 2 17 and
: 3576 1 17  I'm
: 3579 2 15  star
: 3582 1 15 ting
: 3585 1 17  to
: 3588 3 15  feel
: 3593 2 13  you,
: 3597 4 10  boy.
- 3603
: 3609 2 10 Come
: 3612 2 10  on
* 3615 2 13  let's
: 3621 2 10  go
: 3624 2 8 ~
- 3628
: 3639 2 13 real
: 3645 4 10  slow.
- 3651
: 3663 3 13 Ba
: 3667 2 10 by,
: 3671 2 10  like
: 3675 2 13  this
: 3679 1 10  is
: 3683 2 10  per
: 3687 2 13 fec
: 3693 4 10 to.
- 3699
: 3714 2 22 You
: 3717 2 22  know
: 3720 1 22  I'm
: 3723 3 20  on
: 3729 1 22  to
: 3732 6 20 night,
- 3740
: 3744 2 20 my
: 3747 2 17  hips
: 3753 3 20  don't
: 3759 4 17  lie,
- 3764
: 3765 2 17 and
: 3768 1 17  I'm
: 3771 2 15  star
: 3774 2 15 ting
: 3777 1 17  to
: 3780 3 15  feel
: 3785 1 13  it's
: 3789 4 10  right.
- 3795
: 3802 1 10 The
: 3804 1 8  a
: 3806 2 10 trac
: 3813 3 8 tion,
- 3818
: 3827 2 10 the
: 3831 3 13  ten
: 3837 4 10 sion.
- 3843
: 3856 3 13 Ba
: 3860 2 10 by,
: 3863 2 10  like
: 3867 2 13  this
: 3871 1 10  is
: 3875 2 10  per
: 3879 2 13 fec
: 3884 6 10 tion.
E