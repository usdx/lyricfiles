#TITLE:Si seulement je pouvais lui manquer
#ARTIST:Calogero
#MP3:Calogero - Si seulement je pouvais lui manquer.mp3
#VIDEO:Calogero - Si seulement je pouvais lui manquer.mp4
#COVER:Calogero - Si seulement je pouvais lui manquer.jpg
#BPM:323.97
#GAP:16900
#VIDEOGAP:-0.4
#ENCODING:UTF8
#LANGUAGE:French
#GENRE:Pop
#YEAR:2004
#CREATOR:mustangfred and asb
: 0 2 -3 Il
: 4 4 -1 ~
: 12 1 -3  suf
: 17 3 -4 fi
: 22 4 -3 rait
: 27 2 -4 ~
: 36 2 -3  sim
: 42 1 -6 ple
: 46 7 -4 ment
- 63
: 92 2 -4 Qu'il
: 97 2 -3  m'ap
: 104 7 -4 pelle
- 121
: 141 2 -4 Qu'il
: 146 3 -3  m'ap
: 151 9 -4 pelle
- 170
: 193 3 -4 D'oů
: 199 6 -6  vient
: 206 1 -8 ~
: 210 3 -8  ma
: 216 6 -6  vie
- 231
: 235 2 -9 Cer
: 240 4 -8 tain'
: 246 4 -9 ment
- 260
: 285 2 -9 Pas
: 288 3 -6  du
: 294 9 -8  ciel
- 324
: 384 4 -3 Lui
: 389 5 -1 ~
: 396 2 -3  ra
: 401 2 -4 con
: 407 6 -3 ter
: 420 2 -4  mon
: 424 1 -6  en
: 430 7 -4 fan
: 441 2 -4 ce
- 453
: 478 1 -4 Son
: 481 3 -3  ab
: 488 6 -4 sen
: 497 1 -4 ce
- 508
: 526 2 -4 Tous
: 529 3 -3  les
: 534 6 -4  jours
- 550
: 577 3 -4 Com
: 582 6 -6 ment
: 590 2 -8 ~
: 595 3 -8  bri
: 601 8 -6 ser
: 620 1 -9  le
: 625 3 -8  si
: 630 6 -9 len
: 641 2 -9 ce?
- 653
: 668 2 -6 Qui
: 672 4 -6  l'en
: 678 8 -8 toure
- 696
: 726 1 -8 Aus
: 733 3 1 si
: 739 4 3  vrai
: 745 4 4  que
: 751 4 6  de
: 757 4 8  loin
: 765 2 -4  je
* 769 3 6  lui
* 773 11 8 ~
: 787 25 6  parle
- 824
: 828 2 1 J'ap
: 834 3 3 prends
: 840 2 4  tout
: 846 5 6  seul
: 853 1 8  ŕ
* 858 4 9  faire
: 867 13 9  mes
: 883 21 8  ar
: 906 1 8 mes
- 914
: 917 2 -4 Aus
: 924 3 1 si
: 930 2 3  vrai
: 936 4 4  qu'j'ar
: 942 2 6 ręte
* 950 2 8  pas
: 955 1 -4  d'y
: 961 3 6  pen
: 966 6 8 ~
: 979 9 6 ser
- 995
: 998 1 8 Si
* 1003 3 9  seul
: 1009 8 3 'ment
- 1024
: 1027 1 3 Je
: 1032 6 3  pou
: 1040 6 4 vais
: 1048 4 6  lui
* 1056 4 6  man
: 1065 37 8 quer
- 1108
: 1110 1 -4 Est-
: 1115 3 1 c'qu'il
: 1121 5 3  va
: 1129 2 4  me
: 1137 5 6  faire
* 1147 1 8  un
* 1154 14 8  si
: 1171 25 6 gne?
- 1203
: 1206 2 -1 Man
: 1212 4 1 quer
: 1218 4 3  d'a
: 1224 4 4 mour
* 1230 2 6  n'est
: 1237 3 8  pas
: 1243 2 9  un
: 1252 13 9  cri
: 1267 24 8 me
- 1300
: 1303 1 -4 J'ai
: 1308 3 1  qu'une
: 1315 4 3  pri
: 1320 5 4 čre
: 1326 4 6  ŕ
: 1333 4 8  lui
: 1339 1 -4  a
* 1345 3 6 dres
* 1350 8 8 ~
: 1364 8 6 ser
- 1378
: 1381 2 8 Si
: 1387 3 9  seul'
: 1393 35 3 ment
- 1438
: 1458 3 1 Je
: 1465 6 3  pou
: 1472 9 3 vais
: 1483 4 3  lui
: 1489 5 3  man
: 1498 36 1 quer
- 1555
: 1584 3 -3 Je
: 1589 6 -1 ~
: 1598 2 -3  vous
: 1602 2 -4  di
: 1606 8 -3 rais
: 1622 2 -4  sim
: 1628 2 -6 ple
: 1631 7 -4 ment
- 1648
: 1676 2 -3 Qu'ŕ
: 1681 5 -4  part
: 1689 6 -4  ça
- 1705
: 1724 2 -4 Tout
: 1729 3 -3  va
: 1734 5 -4  bien
- 1749
: 1777 1 -4 A
: 1783 4 -6  part
: 1789 2 -8 ~
: 1793 4 -8  d'un
: 1802 8 -6  pčre
- 1818
: 1821 1 -8 Je
: 1824 2 -8  ne
: 1829 5 -9  man
: 1839 2 -9 que
- 1851
: 1873 3 -6 de
: 1878 7 -8  rien
- 1906
: 1970 3 -3 Je
: 1974 4 -1 ~
: 1982 2 -3  vis
: 1987 2 -4  dans
: 1993 10 -3  un
: 2005 2 -4  au
: 2010 2 -6 tre
: 2015 7 -4  mon
: 2024 3 -4 de
- 2037
: 2062 2 -4 Je
: 2066 3 -3  m'ac
: 2073 8 -4 croche
- 2091
: 2110 2 -4 Tous
: 2113 3 -3  les
: 2119 10 -4  jours
- 2139
: 2168 2 -4 Je
: 2174 3 -6  bri
: 2180 2 -8 se
: 2183 9 -6 rai
: 2203 1 -9  le
: 2210 2 -8  si
: 2214 10 -9 len
: 2227 2 -9 ce
- 2239
: 2252 2 -6 Qui
: 2257 4 -6  m'en
: 2264 9 -8 toure
- 2283
: 2311 1 -4 Aus
: 2317 4 1 si
: 2323 4 3  vrai
: 2330 4 4  que
: 2336 3 6  de
: 2342 4 8  loin,
: 2349 2 -4  je
* 2353 16 8  lui
: 2372 26 6  parle
- 2410
: 2414 3 1 J'ap
: 2420 2 3 prends
: 2426 2 4  tout
: 2432 5 6  seul
: 2438 1 8  ŕ
: 2444 4 9  faire
: 2451 14 9  mes
: 2468 23 8  ar
: 2492 1 8 mes
- 2501
: 2504 1 -4 Aus
: 2510 4 1 si
: 2516 3 3  vrai
: 2522 4 4  qu'j'ar
: 2528 2 6 ręte
: 2535 3 8  pas
: 2541 1 -4  d'y
* 2547 2 6  pen
* 2550 8 8 ~
: 2565 9 6 ser
- 2580
: 2583 1 8 Si
: 2589 3 9  seul
: 2594 9 3 'ment
- 2609
: 2612 1 3 Je
: 2619 5 3  pou
: 2626 6 4 vais
: 2634 4 6  lui
: 2642 2 6  man
: 2645 2 8 ~
: 2650 38 8 quer
- 2694
: 2696 1 -4 Est-
: 2702 5 1 c'qu'il
: 2709 4 3  va
: 2715 2 4  me
: 2723 6 6  faire
* 2732 1 8  un
* 2739 16 8  si
: 2757 25 6 gne?
- 2789
: 2792 3 -1 Man
: 2799 3 1 quer
: 2804 2 3  d'un
: 2810 4 4  pčre
* 2816 3 6  n'est
: 2822 4 8  pas
: 2829 3 9  un
: 2836 15 9  cri
: 2853 24 8 me
- 2886
: 2889 1 -4 J'ai
: 2894 3 1  qu'une
: 2900 5 3  pri
: 2906 5 4 čre
: 2912 4 6  ŕ
: 2918 4 8  lui
: 2924 2 -4  a
: 2930 3 6 dres
: 2935 9 8 ~
: 2949 9 6 ser
- 2964
: 2966 2 8 Si
: 2972 3 9  seul'
: 2978 10 3 ment
- 2994
: 2996 2 3 Je
: 3004 7 3  pou
: 3012 6 3 vais
: 3020 2 3  lui
: 3026 5 3  man
: 3035 85 1 quer
- 3163
: 3464 1 -4 Est-
: 3469 5 1 c'qu'il
: 3476 4 3  va
: 3482 3 4  me
: 3492 4 6  faire
* 3498 2 8  un
: 3507 11 8  si
: 3520 2 6 ~
: 3524 22 6 gne?
- 3556
: 3560 3 -1 Man
: 3567 3 1 quer
: 3572 2 3  d'un
: 3578 4 4  pčre
: 3584 2 6  n'est
: 3590 4 8  pas
: 3597 3 9  un
* 3606 13 9  cri
: 3621 25 8 me
- 3653
: 3656 1 -4 J'ai
: 3662 3 1  qu'une
: 3667 5 3  pri
: 3673 5 4 čre
: 3680 4 6  ŕ
: 3685 5 8  lui
: 3692 2 -4  a
: 3698 2 6 dres
: 3702 9 8 ~
: 3717 8 6 ser
- 3732
: 3735 2 8 Si
: 3740 3 9  seul'
: 3747 32 3 ment
- 3789
: 3816 1 3 Je
: 3822 7 3  pou
: 3831 10 3 vais
: 3843 4 3  lui
: 3852 6 3  man
: 3864 50 1 quer
E