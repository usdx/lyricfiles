#TITLE:Bewegungslos
#ARTIST:Farin Urlaub Racing Team
#MP3:Farin Urlaub Racing Team - Bewegungslos.mp3
#COVER:Farin Urlaub Racing Team - Bewegungslos [CO].jpg
#BACKGROUND:Farin Urlaub Racing Team - Bewegungslos [BG].jpg
#BPM:321,87
#GAP:52980
#ENCODING:UTF8
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
: 0 4 3 Du
: 8 3 3  hat
: 13 3 2 test
: 23 6 3  Recht,
- 31
: 64 4 3 als
: 72 5 3  du
: 80 4 3  ge
* 88 6 7 sagt
: 101 6 2  hast:
- 109
: 112 4 2 "Es
: 120 3 7  wä
: 125 3 5 re
: 132 5 3  gar
: 140 5 2  nicht
: 152 6 3  schlecht,
- 160
: 200 4 0 nur
: 208 4 2  noch
: 216 4 3  das
: 224 2 5  zu
: 228 4 3  tun,
- 234
: 240 3 5 was
: 245 4 3  uns
: 256 3 5  tat
: 261 4 3 säch
: 269 4 2 lich
: 280 7 3  freut."
- 289
: 320 4 3 Denn
: 328 4 3  wer
: 336 4 3  nur
: 344 7 7  schweigt,
- 353
: 368 4 2 weil
* 376 4 7  er
: 384 3 5  Kon
: 389 4 3 flik
: 396 3 2 te
: 406 8 3  scheut,
- 416
: 456 5 0 der
: 464 4 2  macht
: 472 5 3  Sa
: 480 2 5 chen,
: 484 5 3  die
- 491
: 496 3 5 er
: 501 5 3  hin
: 512 3 5 ter
: 517 6 3 her
: 528 3 2  be
: 532 9 3 reut.
- 543
: 552 4 3 Und
: 560 4 3  das
: 568 4 7  ist
: 580 7 3  ver
: 592 9 2 kehrt,
- 603
: 608 5 2 denn
: 616 4 2  es
: 624 4 2  ist
* 632 6 7  nicht
: 644 5 2  so
: 656 10 3  schwer,
- 668
: 712 5 0 je
: 720 3 2 den
: 728 4 3  Tag
: 736 2 5  zu
: 741 7 3  tun,
- 752
: 752 2 5 als
: 757 3 3  ob's
: 768 2 5  der
: 772 6 3  Letz
: 784 2 2 te
: 788 7 3  wär.
- 797
: 808 4 3 Und
: 816 4 3  du
: 824 7 7  schaust
: 836 7 3  mich
: 848 7 2  an
- 857
: 864 4 2 und
: 872 4 2  fragst
: 880 5 2  ob
: 888 7 8  ich
: 900 6 7  das
: 912 7 3  kann.
- 921
: 968 5 0 Und
: 976 4 2  ich
: 984 5 3  denk,
: 992 3 5  ich
: 998 6 3  werd
- 1008
: 1008 2 5 mich
: 1012 5 3  än
: 1020 4 -2 dern
: 1032 5 3  ir
: 1040 2 2 gend
* 1044 80 3 wann.
- 1126
: 1472 4 3 Jetzt
: 1480 2 3  ist
: 1484 5 2  es
: 1496 5 3  Nacht.
- 1503
: 1535 4 3 Wir
: 1543 5 3  ham
: 1551 3 3  ge
: 1559 8 7 schla
: 1572 5 2 fen
- 1578
: 1580 6 2 und
* 1592 4 7  nicht
: 1600 2 5  da
: 1604 5 3 ran
: 1612 5 2  ge
: 1622 6 3 dacht,
- 1630
: 1672 4 0 dass
: 1680 4 2  es
: 1688 5 3  Kei
: 1696 2 5 nen
: 1700 4 3  gibt,
- 1706
: 1712 3 5 der
: 1716 3 3  hier
: 1728 2 5  ü
: 1732 3 3 ber
: 1740 5 2  uns
: 1752 6 3  wacht.
- 1760
: 1792 3 3 Jetzt
: 1800 4 3  bleibt
: 1808 4 3  kaum
: 1816 8 7  Zeit,
- 1826
: 1840 5 3 die
: 1848 4 7  Luft
: 1856 2 5  wird
: 1860 5 3  lang
: 1868 5 2 sam
: 1880 4 3  knapp.
- 1886
: 1928 5 0 Wir
: 1936 5 2  um
: 1944 5 3 ar
* 1952 2 5 men
: 1956 5 3  uns,
- 1963
: 1968 2 5 denn
: 1972 7 3  dies
: 1984 2 5  wird
: 1988 6 3  un
: 1996 5 2 ser
: 2008 5 3  Grab.
- 2015
: 2023 6 3 Für
: 2032 3 3  die
: 2040 7 7  E
: 2052 6 3 wig
: 2064 8 2 keit
- 2074
: 2096 4 2 in
* 2104 7 7  Dun
: 2116 5 2 kel
: 2128 7 3 heit.
- 2137
: 2184 4 0 Wir
: 2192 5 2  ver
: 2200 5 3 har
: 2208 2 5 ren
: 2212 6 3  in
- 2220
: 2224 2 5 Be
: 2228 6 3 we
: 2239 3 5 gungs
: 2246 4 3 lo
: 2254 3 2 sig
: 2263 6 3 keit.
- 2271
: 2288 5 3 Wie
: 2296 5 7  tot
: 2308 5 3  ge
: 2318 9 2 bor'n,
- 2329
: 2352 4 2 wie
* 2360 6 8  ein
: 2372 5 7 ge
: 2384 9 3 fror'n.
- 2395
: 2440 4 0 Noch
: 2448 4 2  ein
: 2456 4 3  letz
: 2463 2 5 ter
: 2468 5 3  Kuss
- 2475
: 2480 2 5 und
: 2484 5 3  dann
: 2495 2 5  sind
: 2500 5 3  wir
: 2512 2 2  be
* 2516 5 3 reit.
E