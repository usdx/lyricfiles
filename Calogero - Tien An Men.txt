#TITLE:Tien An Men
#ARTIST:Calogero
#MP3:Calogero - Tien An Men.mp3
#VIDEO:Calogero - Tien An Men.flv
#COVER:Calogero - Tien An Men.jpg
#BPM:289,4
#GAP:19799
#ENCODING:UTF8
#LANGUAGE:French
#GENRE:Pop
#YEAR:2003
#CREATOR:mustangfred and PatTwo
: 0 4 10 Quand
: 5 2 10  les
: 8 9 10  yeux
- 27
: 51 1 10 Ont
: 54 3 10  tout
* 58 6 10  vu
: 69 2 8  et
: 74 3 8  tout
: 81 2 6  su
: 85 3 6 bi
: 89 6 5 ~
- 105
: 116 1 -1 Que
: 119 6 5  même
: 126 2 6  les
: 130 7 6  dieux
- 147
: 174 1 6 Ont
: 176 4 6  per
: 181 6 6 dus
: 189 3 5  de
: 193 5 5  leur
: 200 2 6  ma
: 205 7 8 gie
- 222
* 243 6 10 Quand
: 250 3 10  les
: 254 7 10  mots
- 271
: 298 3 10 Ne
* 302 6 10  vous
: 310 1 8  ré
: 314 6 8 pon
: 321 1 6 dent
: 325 8 6  plus
- 343
: 356 2 1 On
: 360 6 5  cour
: 369 1 6 be
: 372 1 6  le
: 375 7 6  dos
- 392
: 417 3 6 Un
: 422 3 6  jour
: 426 2 5  le
: 429 6 5  bout
: 436 7 5  de
: 444 7 6  la
: 452 8 8  rue
: 461 3 6 ~
- 466
: 468 5 6 Vous
: 475 7 8  mène
* 485 10 10  là
- 505
* 528 2 10 À
* 532 3 8  Tien
* 536 4 10  An
* 543 11 11  Men
- 564
: 586 2 5 À
: 589 2 5  pas
: 592 2 5  bais
: 597 1 5 ser
: 600 1 5  les
* 603 11 10  bras
- 624
: 646 5 3 Seul
: 653 2 3  face
: 657 1 3  à
: 660 3 3  soi
: 665 8 8  même
- 683
: 708 1 5 On
: 711 1 5  se
: 714 4 5  voit
: 721 1 5  faire
: 724 1 5  le
: 727 10 10  pas
- 756
: 776 2 6 De
* 779 1 6  don
* 781 1 8 ner
: 783 1 10  ses
: 788 12 11  chaînes
- 810
: 826 2 5 Parce
: 832 1 5  qu'on
: 834 1 5  a
: 839 3 5  plus
: 845 1 5  que
: 849 11 10  ça
- 870
: 889 2 3 P'tê-
: 893 1 3 tre
: 896 1 3  que
: 900 2 3  Tien
: 903 4 3  An
: 910 10 8  Men
- 930
: 951 1 5 Est
: 955 1 5  plus
: 960 6 5  près
: 970 1 6  que
: 974 1 5  ce
: 979 1 3  qu'on
* 985 9 10  croit
- 1004
: 1013 2 1 Que
: 1016 1 1  nos
: 1020 3 6  guerres
: 1026 1 8  quo
: 1028 2 10 ti
: 1032 12 11 diennes
- 1054
: 1074 2 5 Valent
: 1077 1 5  aus
: 1081 1 5 si
: 1084 4 5  la
: 1092 10 10  peine
- 1112
: 1131 2 3 Mais
: 1134 3 3  on
: 1139 2 3  ne
: 1142 2 3  les
: 1146 4 3  voit
: 1153 8 8  pas
- 1180
: 1219 5 11 Quand
: 1225 3 11  les
: 1230 8 11  gestes
- 1248
: 1274 3 11 Flé
: 1281 4 11 chissent
: 1291 3 9  sous
: 1295 2 9  le
: 1299 2 7  plus
: 1305 5 6  fort
: 1311 2 7 ~
- 1323
: 1336 3 6 Qu'il
: 1340 3 7  ne
: 1344 3 6  vous
: 1348 5 6  reste
- 1372
: 1394 2 4 Plus
: 1397 2 7  qu'à
: 1402 2 7  se
: 1405 6 6  ren
: 1412 6 6 dre
: 1420 4 7  d'ac
: 1428 7 9 cord
- 1445
: 1462 5 11 Quand
: 1470 2 11  plus
: 1473 10 11  rien
- 1493
: 1519 2 11 N'est
: 1523 3 11  à
: 1529 7 9  per
: 1538 1 9 dre
: 1543 3 9  ou
: 1547 1 7  à
: 1551 5 6  pren
: 1558 3 6 dre
- 1573
: 1577 2 2 On
: 1580 2 6  ne
: 1584 2 6  vous
: 1587 2 6  re
: 1591 7 7 tient
- 1608
: 1636 3 4 Un
: 1641 2 7  jour
: 1645 2 7  la
: 1649 6 6  fin
: 1657 6 6  des
: 1665 6 7  mé
: 1672 9 9 an
: 1683 2 7 dres,
: 1688 2 7  vous
: 1692 8 9  mè
: 1702 3 9 ne
: 1706 13 11  là
- 1729
: 1747 2 11 À
: 1752 3 9  Tien
: 1756 5 11  An
: 1763 17 12  Men
- 1790
: 1805 3 6 À
: 1809 2 6  pas
: 1812 2 6  bais
: 1817 2 6 ser
: 1820 2 6  les
: 1823 10 11  bras
- 1843
: 1866 5 4 Seul
: 1874 2 4  face
: 1878 1 4  à
: 1881 3 4  soi-
: 1885 5 9 mê
: 1893 2 9 me
- 1905
: 1928 1 6 On
: 1931 1 6  se
: 1934 4 6  voit
: 1940 2 6  faire
: 1943 1 6  le
: 1947 12 11  pas
- 1969
: 1993 1 2 De
: 1996 3 7  don
: 2000 1 9 ner
: 2004 1 11  ses
: 2008 12 12  chaînes
- 2030
: 2046 2 6 Parce
: 2051 3 6  qu'on
: 2055 1 6  a
: 2059 3 6  plus
: 2064 1 6  que
: 2070 8 11  ça
- 2088
: 2107 1 4 Peut-
: 2110 3 4 être
: 2116 2 4  que
: 2122 3 4  Tien
: 2126 4 4  An
: 2131 10 9  Men
- 2151
: 2169 3 4 Est
: 2174 2 6  plus
: 2180 7 6  près
: 2190 1 6  que
: 2195 3 7  ce
: 2199 2 4  qu'on
: 2205 14 11  croit
- 2229
: 2233 1 11 Que
: 2236 1 11  nos
: 2240 2 11  pe
: 2243 2 9 tits
: 2247 4 11  com
: 2252 10 12 bats
- 2272
: 2293 3 6 Valent
: 2297 2 6  aus
: 2301 3 6 si
: 2305 3 6  la
: 2312 15 11  peine
- 2337
: 2352 1 4 Mais
: 2355 3 4  on
: 2359 2 4  ne
: 2362 2 4  les
: 2365 5 4  voit
: 2373 10 9  pas
- 2402
: 2438 4 6 Parce
: 2445 3 11  qu'on
: 2449 11 11  a
- 2470
: 2494 4 11 en
: 2499 4 11 core
: 2507 6 9  ça
: 2518 3 9  dans
: 2522 2 7  les
: 2526 5 6  vei
: 2533 3 6 nes
- 2546
: 2553 1 2 Et
: 2556 3 6  pas
: 2560 2 6  d'au
: 2563 2 6 tres
: 2568 6 6  choix
- 2593
: 2614 2 4 Un
: 2618 2 7  jour
: 2621 2 6  le
: 2625 4 6  des
: 2633 2 6 tin
: 2637 3 6  vous
: 2641 6 7  em
: 2648 19 9 mène
- 2686
: 2724 2 5 À
: 2728 3 10  Tien
: 2732 5 12  An
: 2739 35 13  Men
E