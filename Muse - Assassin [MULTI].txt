#TITLE:Assassin
#ARTIST:Muse
#LANGUAGE:English
#GENRE:Rock
#YEAR:2006
#CREATOR:gun88
#MP3:Muse - Assassin.mp3
#COVER:Muse - Assassin [CO].jpg
#BACKGROUND:Muse - Assassin [BG].jpg
#VIDEO:Muse - Assassin.mp4
#BPM:250
#MEDLEYSTARTBEAT:578
#MEDLEYENDBEAT:1251
#GAP:47160
#DUETSINGERP1:Matt
#DUETSINGERP2:Choir
P 1
: 0 39 5 War
: 56 5 -3  is
: 66 15 5  o
: 84 4 4 ver
: 90 28 4 due
- 128
: 140 4 2 The
: 148 31 2  time
: 200 4 2  has
: 207 9 2  come
: 219 6 1  for
: 228 34 2  you
- 272
: 280 6 -3 To
: 290 37 5  shoot
: 342 7 -3  your
: 355 11 5  lea
: 372 3 4 ders
: 379 31 4  down
- 419
: 423 7 2 Join
: 436 46 2  for
: 486 9 2 ces
: 499 8 2  un
: 510 5 1 der
: 518 16 2 ground
- 550
* 578 10 9 Lose
: 590 6 7 ~
: 598 3 5 ~
: 603 6 5  con
: 612 16 0 trol
- 635
: 638 3 0 And
* 642 9 9  in
: 654 6 7 crea
: 662 10 5 sing
: 676 18 4  pace
- 701
* 704 8 10 Warped
: 718 7 9  and
: 728 4 7  be
: 738 16 2 witched
- 763
: 766 4 2 Is
: 772 8 10  time
: 782 8 9  to
: 794 4 7  e
: 802 16 5 rase
- 827
: 830 2 0 What
* 834 8 9 e
* 846 8 7 ver
: 858 4 5  they
: 866 15 0  say
- 890
: 894 3 0 This
* 898 8 9  peo
: 910 8 7 ple
: 922 4 5  are
: 934 16 4  torn
- 959
: 962 8 10 Wild
: 972 8 9  and
: 984 4 7  be
: 994 14 2 reft
- 1017
: 1020 3 2 As
* 1024 8 10 sas
: 1036 4 9 sin
: 1048 4 7  is
: 1060 11 5  born
- 1074
: 1076 3 4 Whoa
: 1080 4 5 ~
: 1086 21 4 ~
: 1114 12 5 ~
: 1130 11 7 ~
: 1146 17 7 ~
: 1174 14 5 ~
: 1198 8 8 ~
: 1208 43 9 ~
- 1284
F 1326 14 0 Aim
- 1344
F 1346 14 0 Shoot
- 1370
F 1378 7 0 Kill
F 1386 3 0  your
F 1390 11 0  lea
F 1402 14 0 ders
- 1449
: 1578 6 -3 Op
: 1586 38 5 pose
: 1641 6 -3  and
: 1654 11 5  di
: 1668 5 4 sa
: 1676 24 4 gree
- 1710
: 1720 5 2 De
: 1728 40 2 stroy
: 1778 6 2  de
: 1786 5 2 mo
: 1792 8 2 no
: 1803 6 1 cra
: 1811 38 2 cy
- 1859
: 1874 10 9 Lose
: 1886 6 7 ~
: 1894 3 5 ~
: 1899 6 5  con
: 1908 16 0 trol
- 1931
: 1934 2 0 And
* 1938 9 9  in
: 1950 6 7 crea
: 1959 9 5 sing
: 1973 17 4  pace
- 1997
: 2000 8 10 Warped
: 2014 7 9  and
: 2024 4 7  be
: 2035 15 2 witched
- 2059
: 2062 2 2 Is
* 2066 8 10  time
: 2078 8 9  to
: 2090 4 7  e
: 2098 16 5 rase
- 2123
: 2126 2 0 What
* 2130 8 9 e
: 2142 8 7 ver
: 2154 4 5  they
: 2162 15 0  say
- 2186
: 2190 2 0 This
: 2194 8 9  peo
: 2206 8 7 ple
: 2218 6 5  are
: 2228 18 4  torn
- 2255
: 2258 8 10 Wild
: 2268 8 9  and
: 2280 4 7  be
: 2290 14 2 reft
- 2313
: 2316 3 2 As
* 2320 8 10 sas
: 2332 7 9 sin
: 2344 6 7  is
: 2353 14 5  born
- 2370
: 2372 3 4 Whoa
: 2376 5 5 ~
: 2382 20 4 ~
- 2406
F 2408 6 7 Go!
P 2
: 80 3 5  Whoo
* 84 4 7 ~
* 90 5 4 ~
* 96 4 5 ~
: 101 5 4 ~
: 107 7 0 ~
: 117 18 -5 ~
- 144
: 148 6 -5 Time
: 156 6 -2  time
* 164 6 2  time
: 172 22 5  time
- 204
: 224 2 2 Pa
: 228 2 2  pa
: 232 2 2  pa
: 236 2 2  pa
: 240 2 2  pa
: 244 2 2  pa
: 248 2 2  pa
: 252 1 2  pa
- 254
: 256 2 2 Pa
: 260 2 2  pa
: 264 2 2  pa
: 268 2 2  pa
: 272 2 2  pa
: 276 2 2  pa
: 280 2 2  pa
: 284 2 2  pa
- 319
: 368 3 5 Woh
* 372 4 7  ~
* 378 5 4 ~
: 384 4 5 ~
: 389 5 4 ~
: 395 7 0 ~
: 405 18 -5 ~
- 430
: 433 6 -5 For
: 441 6 -2  for
* 449 6 2  for
: 457 10 5  for
: 468 15 5 ces
- 493
: 512 2 2 Pa
: 516 2 2  pa
: 520 2 2  pa
: 524 2 2  pa
: 528 2 2  pa
: 532 2 2  pa
: 536 2 2  pa
: 540 1 2  pa
- 542
: 544 2 2 Pa
: 548 2 2  pa
: 552 2 2  pa
: 556 2 2  pa
: 560 2 2  pa
: 564 2 2  pa
: 568 2 2  pa
: 572 2 2  pa
- 590
: 608 10 0 Lo
: 620 9 0 sing
: 633 6 0  con
: 642 20 0 trol
- 669
: 672 9 0 In
: 684 7 0 crea
: 694 10 0 sing
: 706 22 2  pace
- 734
: 736 8 2 Warped
: 748 7 2  and
: 762 4 2  be
: 768 20 2 witched
- 798
: 802 8 5 Time
: 812 8 5  to
: 824 4 0  e
: 832 16 0 rase
- 860
: 864 8 0 E
: 876 8 0 ver
: 888 4 0  they
: 894 21 0  say
- 924
: 928 8 0 Peo
: 940 8 0 ple
: 952 4 0  are
: 959 19 2  torn
- 988
: 992 8 2 Wild
: 1002 8 2  and
: 1014 4 2  be
: 1021 21 2 reft
- 1051
: 1054 8 5 Assas
* 1066 7 2 sin
* 1078 7 5  is
: 1090 18 4  born
- 1141
F 1326 14 0 Aim
- 1344
F 1346 14 0 Shoot
- 1370
F 1378 7 0 Kill
F 1386 3 0  your
F 1390 11 0  lea
F 1402 14 0 ders
- 1449
: 1663 3 5 Who
* 1667 4 7 ~
: 1673 5 4 ~
: 1679 4 5 ~
: 1684 8 4 ~
: 1693 5 0 ~
: 1700 18 -5 ~
- 1727
: 1730 6 -5 De
: 1738 6 -2  de
* 1746 6 2  de
* 1754 5 5  des
: 1760 12 5 troy
- 1788
: 1808 2 2 Pa
: 1812 2 2  pa
: 1816 2 2  pa
: 1820 2 2  pa
: 1824 2 2  pa
: 1828 2 2  pa
: 1832 2 2  pa
: 1836 1 2  pa
- 1838
: 1840 2 2 Pa
: 1844 2 2  pa
: 1848 2 2  pa
: 1852 2 2  pa
: 1856 2 2  pa
: 1860 2 2  pa
: 1864 2 2  pa
: 1868 1 2  pa
- 1885
: 1904 10 0 Lo
: 1916 9 0 sing
: 1929 6 0  con
: 1938 20 0 trol
- 1966
: 1969 9 0 In
: 1981 7 0 crea
: 1991 10 0 sing
: 2003 22 2  pace
- 2031
: 2033 8 2 Warped
: 2045 7 2  and
: 2059 4 2  be
: 2065 20 2 witched
- 2095
: 2099 8 5 Time
: 2109 8 5  to
: 2121 4 0  e
: 2129 16 0 rase
- 2157
: 2161 8 0 E
: 2173 8 0 ver
: 2185 4 0  they
: 2191 21 0  say
- 2221
: 2225 8 0 Peo
: 2237 8 0 ple
: 2249 4 0  are
: 2256 19 2  torn
- 2285
: 2289 8 2 Wild
: 2299 8 2  and
: 2311 4 2  be
: 2318 21 2 reft
- 2348
: 2351 8 5 Assas
* 2362 7 2 sin
* 2372 7 5  is
: 2381 20 4  born
- 2406
F 2408 6 7 Go!
E
