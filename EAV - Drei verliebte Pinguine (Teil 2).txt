#TITLE:Drei verliebte Pinguine (Teil 2)
#ARTIST:EAV
#MP3:EAV - Drei verliebte Pinguine (Teil 2).mp3
#COVER:EAV - Drei verliebte Pinguine (Teil 2) [CO].jpg
#BACKGROUND:EAV - Drei verliebte Pinguine (Teil 2) [BG].jpg
#BPM:300
#GAP:11200
#ENCODING:UTF8
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
: 0 7 4 Drei
: 8 5 4  ver
: 14 5 6 lieb
: 21 6 6 te
: 28 7 4  Pin
: 36 5 4 gu
: 42 6 2 i
: 49 4 1 ne
- 54
: 55 6 4 flo
: 62 7 4 gen
: 70 5 2  durch
: 76 5 1  die
* 83 11 -1  Nacht,
- 96
: 110 6 4 vor
: 117 4 4 ne
: 123 6 6  schwarz
: 131 4 6  und
: 137 5 4  hin
: 144 5 4 ten
: 150 6 2  weiß,
- 157
: 158 5 4 was
: 165 6 6  ha
: 172 5 6 ben
: 178 8 6  wir
: 188 3 4  ge
* 192 11 4 lacht.
- 205
: 218 6 9 Drei
: 226 6 9  ver
: 233 6 9 lieb
: 240 6 9 te
: 247 5 8  Pin
: 253 4 9 gu
* 258 6 11 i
: 266 4 11 ne
- 271
: 273 6 12 flo
: 280 7 12 gen
: 288 6 7  ü
: 295 4 9 ber's
* 301 19 9  Meer...
- 322
: 353 2 0 Und
: 356 4 0  hin
: 360 2 0 ter
: 363 2 0 her
: 366 3 0  ein
: 369 3 0  Hub
: 372 4 0 schrau
: 376 5 0 ber,
- 382
: 382 3 0 der
: 385 3 0  tat
: 388 3 0  sich
: 391 3 0  halb
: 394 2 0  so
: 397 6 0  schwer.
E