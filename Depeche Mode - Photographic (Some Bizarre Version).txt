#TITLE:Photographic (Some Bizarre Version)
#ARTIST:Depeche Mode
#MP3:Depeche Mode - Photographic (Some Bizarre Version).mp3
#COVER:Depeche Mode - Photographic (Some Bizarre Version) [CO].jpg
#BACKGROUND:Depeche Mode - Photographic (Some Bizarre Version) [BG].jpg
#BPM:160
#GAP:11750
#ENCODING:UTF8
: 0 1 63 A
: 4 2 63  white
: 10 2 63  house
- 15
: 16 1 66 A
: 20 2 66  white
: 26 4 63  room
- 32
: 34 1 63 the
: 36 4 65  pro
: 40 4 65 gram
: 44 2 63  of
: 48 2 61  to
: 50 2 63 day
- 56
: 68 2 63 Lights
: 74 2 63  on,
: 84 2 66  switch
: 90 2 63  on
- 92
: 94 4 63 Your
: 100 4 65  eyes
: 104 4 65  are
: 108 4 63  far
: 112 2 61  a
: 114 4 63 way
- 120
: 128 2 63 The
: 132 2 63  map
: 140 2 63  re
: 142 4 66 pre
: 146 4 66 sents
: 150 4 63  you
- 152
: 156 2 63 And
: 160 2 63  the
: 162 4 65  tape
: 172 2 63  is
: 174 2 61  your
: 178 4 63  voice
- 184
: 196 2 63 Fol
: 198 4 63 low
: 204 2 63  all
: 208 2 66  a
: 210 4 66 long
: 214 4 63  you
- 218
: 220 2 63  till
: 224 2 63  you
: 228 2 65  re
: 230 4 65 cog
: 234 6 63 nize
: 240 2 61  the
: 242 4 63  choice
- 248
: 292 6 51 I
: 298 6 51  take
: 308 4 49  pic
: 314 4 46 tures
- 324
: 356 4 51 Pho
: 360 2 51 to
: 362 4 51 gra
: 366 2 51 phic
: 372 4 49  pic
: 378 4 46 tures
- 382
: 388 4 51 Bright
: 392 4 51  light,
: 420 4 51  Dark
: 424 4 51  room
- 430
: 452 4 51 Bright
: 456 4 51  light,
: 484 4 51  Dark
: 488 4 51  room
- 496
: 544 2 63 I
: 548 2 63  said
: 550 4 63  I'd
: 556 4 63  write
: 560 4 66  a
: 564 2 66  let
: 566 4 63 ter
- 568
: 572 2 63 But
: 574 4 63  I
: 578 4 65  ne
: 582 4 65 ver
: 588 2 63  got
: 592 2 61  the
: 594 4 63  time
- 600
: 608 2 63 And
: 610 2 63  I'm
: 612 2 63  look
: 614 6 63 ing
: 620 4 63  to
: 624 2 66  the
: 626 4 66  day
- 630
: 640 2 63 I
: 644 2 65  mes
: 648 2 65 me
: 650 4 63 rize
: 656 2 61  the
: 658 4 63  light
- 664
: 672 4 63 In
: 676 4 63  years
: 680 4 63  I
: 684 4 63  spent
: 688 4 66  just
: 692 4 66  think
: 696 2 63 ing
- 698
: 700 2 63 Of
: 702 4 63  a
: 706 4 65  mo
: 710 2 65 ment
: 716 2 63  we
: 718 2 61  both
: 722 4 63  knew
- 728
: 736 4 63 A
: 740 2 63  se
: 742 4 63 cond
: 746 4 63  passed
: 752 2 63  like
: 754 2 66  an
: 756 4 66  emp
: 760 2 63 ty
: 762 2 63  room
- 764
: 766 2 63 It
: 770 6 65  seems
: 776 4 65  it
: 780 4 63  can't
: 784 4 61  be
: 788 4 63  true
- 796
: 836 6 51 I
: 842 6 51  take
: 852 2 49  pic
: 858 4 46 tures
- 866
: 900 4 51 Pho
: 904 2 51 to
: 906 4 51 gra
: 910 2 51 phic
: 916 2 49  pic
: 922 4 46 tures
- 928
: 932 4 51 Bright
: 936 4 51  light,
: 964 4 51  Dark
: 968 4 51  room
- 976
: 996 4 51 Bright
: 1000 4 51  light,
: 1028 4 51  Dark
: 1032 4 51  room
- 1060
: 1380 6 51 I
: 1386 6 51  take
: 1396 2 49  pic
: 1402 4 46 tures
- 1411
: 1445 4 51 Pho
: 1449 2 51 to
: 1451 4 51 gra
: 1455 2 51 phic
: 1461 2 49  pic
: 1467 4 46 tures
- 1471
: 1477 4 63 Bright
: 1481 4 63  light,
: 1509 4 63  Dark
: 1513 4 63  room
- 1521
: 1541 4 63 Bright
: 1545 4 63  light,
: 1573 4 63  Dark
: 1577 4 63  room
E