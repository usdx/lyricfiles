#TITLE:Il changeait la vie
#ARTIST:Jean-Jacques Goldman
#MP3:Jean-Jacques Goldman - Il changeait la vie.mp3
#VIDEO:Jean-Jacques Goldman - Il changeait la vie.avi
#COVER:Jean-Jacques Goldman - Il changeait la vie.jpg
#BPM:402
#GAP:29028
#ENCODING:UTF8
#LANGUAGE:French
#GENRE:Pop
#YEAR:1987
: 0 2 2 C'e
: 4 2 6 tait
: 11 2 9  un
: 16 5 9  cor
: 24 2 9 don
: 27 7 9 nier,
- 44
: 48 2 2 Sans
: 51 5 4  rien
: 60 2 9  d'par
: 65 2 9 ti
: 71 3 9 cu
: 76 5 9 lier
- 92
: 96 2 9 Dans
: 99 5 9  un
: 108 2 7  vil
: 112 5 7 la
: 118 1 7 ge
: 125 7 7  dont
: 135 4 6  le
: 143 3 6  nom
: 148 5 6  m'a
: 155 1 4  é
: 160 3 4 chap
: 167 4 4 pé
- 181
: 196 1 2 Qui
: 198 3 6  fai
: 203 3 9 sait
: 207 2 9  des
: 214 3 9  sou
: 219 5 9 liers
- 235
: 239 2 6 Si
: 244 5 4  jo
: 251 1 9 lis,
: 256 4 9  si
: 263 1 9  lé
: 267 4 6 gers
- 281
: 288 2 7 Que
: 292 4 9  nos
: 300 1 7  vies
: 304 5 7  sem
: 311 2 7 blaient
: 316 4 7  un
- 324
: 326 3 6 Peu
: 332 6 6  moins
: 340 4 6  lourdes
: 347 1 4  ŕ
: 352 2 4  por
: 359 4 4 ter
- 373
: 387 2 11 Il
: 390 2 11  y
* 395 2 14  met
* 400 5 14 tait
: 407 1 14  du
: 412 10 14  temps,
: 424 3 14  du
: 431 3 14  ta
: 435 6 16 lent
: 444 2 16  et
: 447 3 16  du
: 455 3 13  coeur
: 459 5 9 ~
- 474
: 481 1 9 Ain
: 484 2 18 si
: 491 1 18  pas
: 496 3 18 sait
: 503 2 18  sa
* 508 8 18  vie
: 519 5 18  au
: 527 2 18  mi
: 531 6 16 lieu
: 540 2 14  de
: 543 4 13  nos
: 552 8 14  heures
- 572
: 576 2 9 Et
: 579 5 11  loin
: 587 1 14  des
: 592 3 14  beaux
: 599 2 14  dis
: 605 7 14 cours,
- 614
: 616 4 14 Des
: 623 3 14  gran
: 627 3 16 des
: 636 2 16  thé
: 640 4 14 o
: 647 4 16 ries
- 660
: 664 2 14 A
: 670 2 16  sa
* 676 3 17  tâ
* 684 3 16 che,
: 692 1 14  cha
: 697 2 16 que
: 702 5 17  jour,
- 714
: 717 2 10 On
: 720 2 14  pou
: 725 4 19 vait
: 733 3 17  di
: 741 1 16 re
: 744 2 17  de
: 748 58 19  lui
- 816
* 838 2 14 Il
* 846 5 19  chan
* 854 4 17 geait
* 860 4 16  la
* 868 3 17  vie
* 872 4 16 ~
* 877 4 14 ~
- 934
: 1249 1 2 C'e
: 1251 2 6 tait
: 1258 2 9  un
: 1264 3 9  pro
: 1271 1 9 fes
: 1276 6 9 seur,
- 1291
: 1295 1 2 Un
: 1299 4 4  sim
: 1307 1 9 ple
: 1313 1 9  pro
: 1319 1 11 fes
: 1324 6 9 seur
- 1340
: 1344 2 9 Qui
: 1348 3 9  pen
: 1355 2 7 sait
: 1361 1 7  que
: 1367 3 7  sa
: 1373 8 7 voir
: 1385 2 6  é
: 1391 2 6 tait
: 1396 4 6  un
: 1403 3 4  grand
: 1411 2 4  tré
: 1415 4 4 sor
: 1423 2 4 ~
- 1435
: 1444 1 2 Que
: 1446 3 6  tous
: 1451 2 9  les
: 1456 3 9  moins
: 1463 2 9  que
: 1467 7 9  rien
- 1484
: 1488 2 4 N'a
: 1492 2 4 vaient
: 1499 2 9  pour
: 1505 4 9  s'en
: 1512 5 9  sor
: 1521 2 6 tir
- 1533
: 1537 3 9 Que
: 1541 2 9  l'é
: 1547 4 7 cole
: 1552 4 7  et
: 1558 2 7  le
: 1562 7 7  droit
- 1575
: 1577 2 6 Qu'a
: 1583 2 6  cha
: 1589 4 6 cun
: 1596 1 4  de
: 1600 3 4  s'in
: 1607 7 4 struire
- 1624
: 1633 3 9 Il
: 1637 3 11  y
: 1643 2 14  met
: 1649 4 14 tait
: 1656 1 14  du
* 1661 9 14  temps,
: 1673 1 14  du
: 1680 3 14  ta
: 1684 7 16 lent
: 1692 2 16  et
: 1696 2 16  du
: 1703 1 13  coeur
: 1705 3 11 ~
: 1711 3 9 ~
- 1726
: 1730 1 9 Ain
: 1733 2 18 si
* 1739 2 18  pas
* 1744 3 18 sait
: 1752 3 18  sa
: 1757 9 18  vie
: 1768 4 18  au
: 1775 4 18  mi
: 1780 6 16 lieu
: 1788 2 14  de
: 1793 3 13  nos
: 1800 10 14  heures
- 1819
: 1823 2 9 Et
: 1827 4 11  loin
: 1836 2 14  des
: 1841 4 14  beaux
: 1848 2 14  dis
: 1853 7 14 cours,
- 1863
: 1865 2 14 Des
: 1871 3 14  gran
: 1877 2 16 des
: 1883 4 16  thé
: 1888 4 14 o
: 1895 3 16 ries
- 1909
: 1913 2 14 A
: 1919 2 16  sa
: 1925 3 17  tâ
: 1933 3 16 che
: 1940 2 14  cha
: 1945 2 16 que
: 1950 3 17  jour
: 1954 2 17 ~
- 1965
: 1969 2 10 On
: 1972 1 14  pou
: 1974 4 19 vait
: 1981 2 17  di
: 1986 3 16 re
: 1992 3 17  de
: 1997 57 19  lui
- 2064
: 2086 2 17 Il
* 2094 6 19  chan
: 2102 5 17 geait
: 2109 4 16  la
: 2118 2 17  vie
: 2121 2 16 ~
: 2124 2 14 ~
- 2179
: 3026 1 2 C'é
: 3029 2 6 tait
: 3036 1 9  un
: 3041 3 9  petit
: 3047 2 9  bo
: 3052 4 9 nhomme,
- 3068
: 3072 2 6 Rien
: 3077 3 4  qu'un
: 3084 1 9  tout
: 3090 3 9  petit
: 3095 3 9  bo
: 3100 5 9 nhomme
- 3117
: 3121 2 9 Mal
: 3124 3 9 ha
: 3132 2 7 bile
: 3136 3 7  et
: 3145 2 7  rę
: 3150 4 7 veur,
- 3160
: 3162 2 6 Un
: 3168 2 6  peu
: 3172 4 6  lou
: 3181 3 4 pé
: 3185 3 4  en
: 3192 3 4  som
: 3196 2 4 me
- 3208
: 3216 1 2 Se
: 3220 6 6  cro
: 3227 2 9 yait
: 3233 5 9  i
: 3240 1 9 nu
: 3245 5 9 tile,
- 3262
: 3266 2 2 Ban
: 3269 4 4 ni
: 3276 3 9  des
: 3282 3 9  au
: 3289 5 11 tres
: 3300 3 9  hom
: 3304 3 9 mes
- 3333
: 3379 3 4 Il
: 3386 3 4  pleu
: 3391 3 4 rait
: 3400 2 4  sur
: 3408 4 6  son
: 3421 3 4  sa
: 3429 2 2 xo
: 3438 4 2 phone
- 3468
: 3505 3 9 Il
: 3510 3 11  y
: 3516 2 14  mit
* 3521 5 14  tant
: 3528 1 14  de
: 3533 8 14  temps,
- 3543
: 3545 5 14 De
: 3552 2 14  la
: 3556 1 14 rmes
: 3558 4 16  et
: 3564 1 16  de
* 3568 6 16  dou
* 3576 3 13 leur
* 3582 1 11 ~
* 3585 2 9 ~
- 3596
: 3600 1 9 Les
* 3603 5 18  rę
* 3612 1 18 ves
: 3615 3 18  de
: 3622 3 18  sa
: 3629 7 18  vie,
- 3638
: 3640 2 18 Les
: 3647 2 18  pri
: 3652 6 16 sons
: 3660 1 14  de
: 3665 1 13  son
: 3672 12 14  coeur
- 3693
: 3696 2 11 Et
: 3700 4 11  loin
: 3709 2 14  des
* 3713 4 14  beaux
: 3721 2 14  dis
: 3726 6 14 cours,
- 3735
: 3737 3 14 Des
: 3744 3 16  gran
: 3750 1 14 des
: 3757 3 16  thé
: 3761 4 14 o
: 3768 4 16 ries
- 3781
: 3784 2 14 In
: 3792 4 16 spi
: 3799 2 17 ré
: 3806 5 16  jour
: 3813 1 14  a
: 3818 2 16 prčs
: 3822 5 17  jour
- 3833
: 3836 1 14 De
: 3840 2 14  son
: 3846 4 19  souf
: 3853 1 19 fle
: 3855 1 17  et
: 3859 2 16  de
: 3864 4 17  ses
: 3874 58 19  cris
- 3958
: 4006 2 16 Il
: 4014 4 19  chan
: 4022 2 17 geait
: 4030 4 16  la
: 4038 3 17  vie
: 4042 4 16 ~
: 4047 3 14 ~
- 4103
: 4163 2 22 Oh
: 4166 8 22 ~
: 4175 6 22 ~
: 4182 6 21 ~
: 4190 3 21 ~
: 4194 2 21 ~
- 4199
: 4201 2 17 Il
: 4208 4 19  chan
: 4216 4 17 geait
: 4223 7 16  la
: 4236 3 17  vie
: 4240 4 16 ~
: 4245 3 14 ~
- 4301
: 4391 2 14 Il
: 4398 4 19  chan
: 4407 4 17 geait
: 4414 3 16  la
: 4418 3 16 ~
: 4426 3 17  vie
: 4431 3 16 ~
: 4435 2 14 ~
- 4490
: 4554 1 22 Oh
: 4558 5 22 ~
: 4565 8 21 ~
: 4574 3 21 ~
: 4578 3 21 ~
: 4582 1 21 ~
- 4584
: 4586 1 17 Il
: 4590 6 21  chan
: 4598 7 19 geait
: 4607 3 17  la
: 4613 4 21  vie
: 4618 3 19 ~
: 4622 3 21 ~
: 4626 5 21 ~
- 4684
: 4775 2 17 Il
: 4782 4 19  chan
: 4791 3 17 geait
: 4798 4 16  la
: 4806 4 17  vie
: 4811 3 16 ~
: 4815 4 14 ~
E