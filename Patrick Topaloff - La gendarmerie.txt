#TITLE:La gendarmerie
#ARTIST:Patrick Toplaoff
#MP3:Patrick Topaloff - La gendarmerie.mp3
#COVER:Patrick Topaloff - La gendarmerie.jpg
#BACKGROUND:Patrick Topaloff - La gendarmerie.jpg
#BPM:252
#GAP:6785
#ENCODING:UTF8
: 0 2 4 Quand
: 4 2 6  un
: 8 3 7  gen
: 12 5 8 dar
: 18 1 8 me
: 20 8 11  rit
- 30
: 32 2 9 Dans
: 36 2 9  la
: 40 3 9  gen
: 44 5 8 dar
: 50 1 6 me
: 52 7 9 rie
- 62
: 64 2 8 Tous
: 68 2 8  les
: 72 3 5  gen
: 76 5 6 dar
: 82 1 4 mes
: 84 8 8  rient
- 94
: 96 2 6 Dans
: 100 2 6  la
: 104 4 6  gen
: 109 4 7 dar
: 114 2 6 me
: 117 7 6 rie
- 126
: 128 2 4 Quand
: 132 2 6  un
: 136 3 7  gen
: 140 5 8 dar
: 146 1 8 me
: 148 8 11  rit
- 158
: 160 2 9 Dans
: 164 2 9  la
* 168 3 9  gen
* 172 5 8 dar
* 178 1 6 me
* 180 7 9 rie
- 190
: 192 2 8 Tous
: 196 2 8  les
: 200 3 5  gen
: 204 5 6 dar
: 210 1 4 mes
: 212 8 8  rient
- 222
: 224 2 6 Dans
: 228 2 6  la
: 232 4 6  gen
: 237 4 7 dar
: 242 2 7 me
: 245 19 8 rie
- 274
: 296 2 9 Ah
: 300 2 11  quel
: 304 2 9  beau
: 308 3 9  mé
: 312 2 9 tier
: 316 2 9  je
: 320 2 9  fais
: 324 3 9  vrai
: 328 2 8 ment
: 332 2 6  je
: 336 2 8  suis
: 340 4 8  ra
: 345 7 7 vi
- 358
: 360 2 8 J'ai
: 364 2 8  un
: 368 2 8  beau
: 372 3 8  sif
: 376 2 8 flet
- 378
: 380 2 8 un
: 384 2 8  beau
: 388 3 7  bâ
: 392 2 6 ton
: 396 2 8  un
: 400 2 9  beau
: 404 4 9  ké
: 409 7 9 pi
- 420
: 422 2 4 Mes
: 426 3 14  a
: 430 2 13 mis
: 434 2 13  et
: 438 2 13  moi
- 440
: 442 2 13 nous
: 446 3 13  som
: 450 2 13 mes
: 454 2 11  tous
: 458 2 12  des
: 462 4 14  grands
: 468 2 14  en
: 471 7 11 fants
- 484
: 486 2 11 C'est
: 490 2 9  bien
: 494 2 15  simpl'
: 498 2 15  tout'
: 502 2 15  la
: 506 3 11  jour
: 510 2 11 née
- 512
: 514 3 11 en
: 518 2 11 tre
: 522 2 9  nous
: 526 2 8  on
: 530 3 0  ri
: 534 2 2 gol'
: 538 2 13  tout
: 542 6 16  l'temps
- 549
: 551 3 21 Te
: 555 2 21 nez
: 559 3 9  jus
: 563 3 9 te
: 567 2 9 ment
: 571 2 9  hier
: 575 2 9  soir
- 577
: 579 1 9 on
: 581 1 9  s'est
: 583 1 9  ca
: 585 1 8 ché
: 587 2 6  dans
: 591 2 8  les
: 595 4 8  feuil
: 600 7 8 lag's
- 614
: 617 3 6 His
: 621 2 7 toir'
: 625 3 8  d'at
: 629 3 8 tra
: 633 2 8 per
: 637 2 8  tous
: 641 2 8  ceux
- 643
: 645 2 4 qui
: 649 3 6  dou
: 653 2 8 blaient
: 657 2 9  dans
: 661 4 9  l'vi
: 666 7 9 rag'
- 681
: 684 2 6 On
: 688 2 9  en
: 692 2 13  a
: 696 2 13  pris
: 700 2 13  un
- 702
: 704 2 11 et
: 708 2 11  on
: 712 2 11  a
: 716 3 11  gar
: 720 2 12 dé
: 724 4 14  ses
: 730 2 10  pa
: 733 7 9 piers
- 746
: 748 2 11 Si
: 752 2 9  bien
: 756 2 15  que
: 760 2 15  ce
: 764 2 15  brav'
: 768 4 15  mon
: 772 2 15 sieur
: 776 2 13  a
: 780 2 11  du
: 784 4 11  ren
: 788 2 11 trer
: 792 2 11  chez
: 796 2 11  lui
: 800 2 11  a
: 804 2 11  pied
- 806
: 808 1 16 Les
: 810 2 16  gen
: 813 9 4 darm's
: 840 2 4  sont
: 844 4 4  char
: 848 5 16 mants
- 854
: 856 2 4 A
: 858 2 16 mu
: 860 8 11 sants
- 874
: 876 1 16 Et
: 878 3 16  tou
: 882 2 16 jours
: 888 3 16  sou
: 892 3 16 ri
: 896 3 16 ants
- 899
: 901 2 4 Quand
: 905 2 6  un
: 909 3 7  gen
: 913 5 8 dar
: 919 1 8 me
: 921 8 11  rit
- 931
: 933 2 9 Dans
: 937 2 9  la
: 941 3 9  gen
: 945 5 8 dar
: 951 1 6 me
: 953 7 9 rie
- 963
: 965 2 8 Tous
: 969 2 8  les
: 973 3 5  gen
: 977 5 6 dar
: 983 1 4 mes
: 985 8 8  rient
- 995
: 997 2 6 Dans
: 1001 2 6  la
: 1005 4 6  gen
: 1010 4 7 dar
: 1015 2 6 me
: 1018 7 6 rie
- 1027
: 1029 2 4 Quand
: 1033 2 6  un
: 1037 3 7  gen
: 1041 5 8 dar
: 1047 1 8 me
: 1049 8 11  rit
- 1059
: 1061 2 9 Dans
: 1065 2 9  la
* 1069 3 9  gen
* 1073 5 8 dar
* 1079 1 6 me
* 1081 7 9 rie
- 1091
: 1093 2 8 Tous
: 1097 2 8  les
: 1101 3 5  gen
: 1105 5 6 dar
: 1111 1 4 mes
: 1113 8 8  rient
- 1123
: 1125 2 6 Dans
: 1129 2 6  la
: 1133 4 6  gen
: 1138 4 7 dar
: 1143 2 7 me
: 1146 19 8 rie
- 1181
: 1204 2 9 Des
: 1208 3 11  l'ma
: 1212 2 9 tin
: 1216 2 19  pour
: 1220 2 18  m'fair'
: 1224 2 9  un
: 1228 2 9  p'tit
: 1232 2 9  peu
: 1236 2 8  de
: 1240 3 6  mus
: 1244 3 8 cu
: 1248 4 8 la
: 1253 7 6 tur'
- 1266
: 1268 3 10 A
: 1272 2 8 vec
: 1276 2 8  les
: 1280 3 8  co
: 1284 2 8 pains
- 1286
: 1288 2 8 j'vais
: 1292 2 8  mettr'
: 1296 2 4  des
: 1300 3 6  sa
: 1304 2 8 bots
: 1308 2 9  aux
: 1312 4 9  voi
: 1317 7 9 tur's
- 1330
: 1332 2 6 A
: 1336 2 9  deux
: 1340 2 13  heur's
: 1344 2 13  pour
: 1348 3 12  di
: 1352 3 12 gé
: 1356 2 13 rer
- 1358
: 1360 2 13 c'est
: 1364 2 13  les
: 1368 3 13  con
: 1372 3 12 tra
: 1376 3 12 ven
: 1380 8 11 tions
- 1394
: 1396 2 14 Et
: 1400 2 15  la
: 1404 2 15  nuit
: 1408 2 15  pour
: 1412 3 15  ri
: 1416 3 13 go
: 1420 2 15 ler
- 1422
: 1424 2 13 on
: 1428 2 11  s'fait
: 1432 2 2  des
: 1436 2 2  p'tit's
: 1440 3 2  per
: 1444 3 2 qui
: 1448 3 12 si
: 1452 6 16 tions
- 1458
: 1460 2 16 Mais
: 1464 2 16  le
: 1468 2 9  plus
: 1472 2 9  beau
: 1476 2 9  jour
: 1480 2 9  ah
: 1484 2 9  ca
: 1488 2 9  je
: 1492 3 9  n'l'ou
: 1496 3 9 blie
: 1500 2 8 rai
: 1504 4 6  ja
: 1509 7 5 mais
- 1522
: 1524 3 6 C'é
: 1528 2 4 tait
: 1532 2 8  a
: 1536 3 8  Pa
: 1540 2 8 ris
: 1544 2 8  y'a
: 1548 2 8  plus
: 1552 2 4  d'vingt
: 1556 2 6  ans
: 1560 2 7  au
: 1564 4 9  mois
: 1570 1 9  de
: 1572 8 9  mai
- 1586
: 1588 2 6 On
: 1592 2 9  est
: 1596 3 13  al
: 1600 2 13 lé
: 1604 2 13  tous
: 1608 3 13  en
: 1612 2 13 sembl'
: 1616 2 13  a
: 1620 2 13  Saint
: 1624 3 13  Ger
: 1628 4 14 main
: 1634 1 11  des
: 1636 8 10  Prés
- 1650
: 1652 2 15 Et
: 1656 3 15  a
: 1660 2 15 vec
: 1664 2 15  des
: 1668 3 15  é
: 1672 3 15 tu
: 1676 2 15 diants
: 1680 2 15  on
: 1684 2 15  a
: 1688 3 14  jou
: 1692 2 13 é
: 1696 2 14  a
: 1700 2 16  chat
: 1704 3 14  per
: 1708 2 13 ché
- 1716
: 1718 1 16 Les
: 1720 2 16  gen
: 1723 9 4 darm's
: 1750 2 4  sont
: 1754 4 4  char
: 1758 5 16 mants
- 1764
: 1766 2 4 A
: 1768 2 16 mu
: 1770 8 11 sants
- 1784
: 1786 1 16 Et
: 1788 3 16  tou
: 1792 2 16 jours
: 1795 3 16  sou
: 1799 3 16 ri
: 1803 2 16 ants
- 1805
: 1806 2 4 Quand
: 1810 2 6  un
: 1814 3 7  gen
: 1818 5 8 dar
: 1824 1 8 me
: 1826 8 11  rit
- 1836
: 1838 2 9 Dans
: 1842 2 9  la
: 1846 3 9  gen
: 1850 5 8 dar
: 1856 1 6 me
: 1858 7 9 rie
- 1868
: 1870 2 8 Tous
: 1874 2 8  les
: 1878 3 5  gen
: 1882 5 6 dar
: 1888 1 4 mes
: 1890 8 8  rient
- 1900
: 1902 2 6 Dans
: 1906 2 6  la
: 1910 4 6  gen
: 1915 4 7 dar
: 1920 2 6 me
: 1923 7 6 rie
- 1932
: 1934 2 4 Quand
: 1938 2 6  un
: 1942 3 7  gen
: 1946 5 8 dar
: 1952 1 8 me
: 1954 8 11  rit
- 1964
: 1966 2 9 Dans
: 1970 2 9  la
: 1974 3 9  gen
: 1978 5 8 dar
: 1984 1 6 me
: 1986 7 9 rie
- 1996
: 1998 2 8 Tous
: 2002 2 8  les
: 2006 3 5  gen
: 2010 5 6 dar
: 2016 1 4 mes
: 2018 8 8  rient
- 2028
* 2030 2 6 Dans
* 2034 2 6  la
* 2038 4 6  gen
* 2043 4 7 dar
* 2048 2 7 me
* 2051 19 8 rie
- 2103
: 2234 2 4 Quand
: 2238 2 6  un
: 2242 3 7  gen
: 2246 5 8 dar
: 2252 1 8 me
: 2254 8 11  rit
- 2264
: 2266 2 9 Dans
: 2270 2 9  la
: 2274 3 9  gen
: 2278 5 8 dar
: 2284 1 6 me
: 2286 7 9 rie
- 2296
: 2298 2 8 Tous
: 2302 2 8  les
: 2306 3 5  gen
: 2310 5 6 dar
: 2316 1 4 mes
: 2318 8 8  rient
- 2328
: 2330 2 6 Dans
: 2334 2 6  la
: 2338 4 6  gen
: 2343 4 7 dar
: 2348 2 6 me
: 2351 7 6 rie
- 2360
: 2362 2 4 Quand
: 2366 2 6  un
: 2370 3 7  gen
: 2374 5 8 dar
: 2380 1 8 me
: 2382 8 11  rit
- 2392
: 2394 2 9 Dans
: 2398 2 9  la
: 2402 3 9  gen
: 2406 5 8 dar
: 2412 1 6 me
: 2414 7 9 rie
- 2424
: 2426 2 8 Tous
: 2430 2 8  les
: 2434 3 5  gen
: 2438 5 6 dar
: 2444 1 4 mes
: 2446 8 8  rient
- 2456
: 2458 2 6 Dans
: 2462 2 6  la
: 2466 4 6  gen
: 2471 4 7 dar
: 2476 2 7 me
: 2479 6 8 rie
- 2486
: 2488 2 4 Quand
: 2492 2 6  un
: 2496 3 7  gen
: 2500 5 8 dar
: 2506 1 8 me
: 2508 8 11  rit
- 2518
: 2520 2 9 Dans
: 2524 2 9  la
: 2528 3 9  gen
: 2532 5 8 dar
: 2538 1 6 me
: 2540 7 9 rie
- 2550
: 2552 2 8 Tous
: 2556 2 8  les
: 2560 3 5  gen
: 2564 5 6 dar
: 2570 1 4 mes
: 2572 8 8  rient
- 2582
: 2584 2 6 Dans
: 2588 2 6  la
: 2592 4 6  gen
: 2597 4 7 dar
: 2602 2 6 me
: 2605 7 6 rie
- 2614
: 2616 2 4 Quand
: 2620 2 6  un
: 2624 3 7  gen
: 2628 5 8 dar
: 2634 1 8 me
: 2636 8 11  rit
- 2646
: 2648 2 9 Dans
: 2652 2 9  la
: 2656 3 9  gen
: 2660 5 8 dar
: 2666 1 6 me
: 2668 7 9 rie
- 2678
: 2680 2 8 Tous
: 2684 2 8  les
: 2688 3 5  gen
: 2692 5 6 dar
: 2698 1 4 mes
: 2700 8 8  rient
- 2710
: 2712 2 6 Dans
: 2716 2 6  la gendarmerie
E