#TITLE:Fruit de la Passion
#ARTIST:Francky Vincent
#MP3:Francky Vincent - Fruits de la passion.mp3
#VIDEO:Francky Vincent - Fruits de la passion.mp4
#COVER:Francky Vincent - Fruits de la passion.jpg
#BPM:240
#GAP:19182
#VIDEOGAP:0.75
#ENCODING:UTF8
: 0 3 6 I
: 9 1 6  Ja
: 11 2 6  Sin
: 15 5 8  ké
: 27 2 9  cé
: 31 2 6  lin
: 35 8 8  di
- 43
: 45 2 9 I
: 49 2 8  lé
: 53 2 8  pou
: 57 1 9  nous
: 59 4 9  aye
: 65 2 7  tra
: 69 8 6  vail
- 81
: 83 2 6 Ou
: 87 2 1  pa
: 91 2 8  vlé
: 95 1 9  léve
: 97 4 6  ou
: 103 2 6  a
: 107 8 8  mouré
- 119
: 121 2 9 En
: 125 2 6  nous
: 129 2 8  fé
: 133 1 9  ma
: 135 4 11  lé
: 141 2 9  li
: 145 8 6  vé
- 157
: 159 1 6 Pa
: 161 2 18  rait
: 165 4 18  -il
: 171 1 18  leur
: 173 2 16  ma
: 177 4 14  ti
: 183 8 13  nal
- 195
: 197 1 11 A
: 199 1 11  pré on
: 201 2 11  week
: 205 2 11  -end
: 209 1 13  Ki
: 211 2 13  in
: 215 2 11  fer
: 219 8 9  nal
- 234
: 237 1 6 Se oh
: 239 1 4  bi
: 241 4 16  tin
: 247 1 14  fon
: 249 2 14  da
: 253 2 14  men
: 257 8 13  tal
- 272
: 275 1 11 (Ma
: 277 1 11  rie
: 279 2 11  -Chan
: 283 8 9  tal)
- 301
: 308 2 6 En
: 311 1 6  ka
: 313 2 6  sen
: 317 2 8  ti
: 321 2 7  vous
: 325 2 6  tou
: 329 2 6  tou
: 333 8 8  ni
- 347
: 349 1 9 Et
: 351 2 8  moin
: 355 2 8  min
: 359 1 9  me en
: 361 4 11  tou
: 367 2 9  tou
: 371 8 6  ni
- 383
: 385 2 6 Ca
: 389 1 7  resse
: 391 4 8  ka
: 397 1 6  tran
: 399 4 6  po
: 405 2 6  té
: 409 8 8  moin
- 421
: 423 2 9 On
: 427 1 6  man
: 429 2 8  nié
: 433 2 9  trans
: 437 2 11  po
: 441 4 9  en
: 447 8 10  Kommin
- 457
: 459 4 6 Ti
: 465 1 16  boug
: 467 4 17  la
: 473 1 16  i
: 475 2 16  bien
: 479 2 14  coin
: 483 8 13  cé
- 495
: 497 1 11 Ke
: 499 2 11  aye
: 503 1 11  con
: 505 2 11  tent i
: 509 2 11  ma
: 513 2 13  lé
: 517 2 11  li
: 521 8 9  vé
- 533
: 535 2 4 A
: 539 1 4  dan
: 541 1 4  on
: 543 2 16  sé
: 547 1 16  lé lé
: 549 2 16  sé
: 553 4 14  al
: 559 8 13  lé
- 574
: 577 1 11 (Kon
: 579 1 11  ti
: 581 1 11  ni
: 583 8 9  é)
- 597
: 599 2 8 Ché
: 602 2 8  rie
: 605 2 6  tu
: 609 1 9  m'don
: 611 1 6  nes
: 613 2 9  ta
: 617 2 6  pas
: 621 8 9  sion
- 635
: 637 1 6 Et
: 639 2 7  je
: 643 2 6  trouve
: 647 2 7  ca
: 651 1 9  fa
: 653 2 9  bu
: 657 8 9  leux
- 672
: 675 1 8 Je
: 677 2 8  n'suis
: 681 4 6  pas
: 687 1 9  bran
: 689 1 9  ché
: 691 1 9  sen
: 693 2 9  ti
: 697 8 6  ment
- 709
: 711 2 6 J'suis
: 715 2 6  plu
: 719 4 6  tôt
: 725 1 6  su
: 727 1 6  per
: 729 2 6  a
: 733 8 4  mant
- 747
: 749 2 6 Au
: 753 2 4  jourd'hui
: 757 1 6  tu
: 759 2 9  vas
: 763 2 6  ou
: 767 2 9  bli
: 771 8 9  er
- 785
: 787 1 9 Tous
: 789 2 6  les
: 793 1 6  to
: 795 2 9  cards
: 799 1 6  qui
: 801 2 9  n'ont
: 805 1 8  pas
: 807 1 9  as
: 809 2 9  su
: 813 8 9  ré
- 823
: 825 1 7 Y'a
: 827 4 6  pas
: 833 2 6  qu'la
: 837 1 9  fesse
: 839 2 9  dans
: 843 1 6  la
: 845 8 9  vie
- 860
: 863 1 6 Y'a
: 865 4 6  le
: 871 4 6  sexe
: 877 2 6  aus
: 881 8 6  si
- 893
: 895 4 6 Vas
: 901 1 6  -y
: 903 2 6  Fran
: 907 6 6  ky
: 915 2 6  c'est
* 919 5 6  bon
- 930
: 933 2 9 Vas
: 937 2 6  -y
: 941 2 7  Fran
: 945 6 6  ky
: 953 2 6  c'est
: 957 2 6  bon
: 961 4 6  bon
: 967 2 6  bon
- 969
: 971 4 6 Vas
: 977 1 6  -y
: 979 2 6  Fran
: 983 6 6  ky
: 991 2 6  c'est
: 995 5 6  bon
- 1006
: 1009 2 9 Vas
* 1013 2 6  -y
* 1017 2 7  Fran
* 1021 6 6  ky
: 1029 2 6  c'est
: 1033 2 6  bon
: 1037 4 6  bon
: 1043 2 6  bon
- 1045
: 1047 4 6 Vas
: 1053 1 6  -y
: 1055 2 6  Fran
: 1059 6 6  ky
: 1067 2 6  c'est
: 1071 8 6  bon
- 1083
: 1085 2 9 Vas
: 1089 2 6  -y
: 1093 2 7  Fran
: 1097 6 6  ky
: 1105 2 6  c'est
: 1109 2 6  bon
: 1113 4 6  bon
: 1119 2 6  bon
- 1121
: 1123 4 6 Vas
: 1129 1 6  -y
: 1131 2 6  Fran
: 1135 6 6  ky
: 1143 2 6  c'est
: 1147 5 6  bon
- 1158
: 1161 2 9 Vas
: 1165 2 6  -y
: 1169 2 7  Fran
: 1173 6 6  ky
: 1181 2 6  c'est
: 1185 2 6  bon
: 1189 4 6  bon
: 1195 2 6  bon
- 1229
: 1501 1 6 Fruits
: 1503 1 4  de
: 1505 2 6  la
: 1509 1 6  pas
: 1511 8 6  sion
- 1519
: 1521 8 6 (J'aime quand tu touches)
- 1536
: 1539 1 6 Fruits
: 1541 1 6  de
: 1543 1 6  la
: 1545 2 6  pas
: 1549 6 6  sion
- 1555
: 1557 8 6 (Oh c'est super !)
- 1575
: 1579 1 6 Fruits
: 1581 1 6  de
: 1583 1 6  la
: 1585 1 6  pas
: 1587 8 6  sion
- 1595
: 1597 8 6 (Oh Franky c'est génial !)
- 1614
: 1617 1 6 Fruits
: 1619 1 6  de
: 1621 1 6  la
: 1623 1 6  pas
: 1625 8 6  sion
- 1633
: 1635 8 6 (Bon dessert mon amour !)
- 1650
: 1653 1 6 Fruits
: 1655 2 6  de
: 1659 1 6  la
: 1661 1 6  pas
: 1663 8 6  sion
- 1671
: 1673 8 6 (Décidément c'est dément !)
- 1688
: 1691 2 6 Fruits
: 1695 1 6  de
: 1697 1 6  la
: 1699 1 6  pas
: 1701 8 6  sion
- 1709
: 1711 8 6 (Quelle aventure !)
- 1726
: 1729 1 6 Fruits
: 1731 1 6  de
: 1733 1 6  la
: 1735 2 6  pas
: 1739 6 6  sion
- 1745
: 1747 8 6 (Ca me fait soupirer )
- 1787
: 2102 4 6 Vas
: 2108 1 6  -y
: 2110 2 6  Fran
: 2114 6 6  ky
: 2122 2 6  c'est
: 2126 4 6  bon
- 2137
: 2140 2 9 Vas
: 2144 2 6  -y
: 2148 2 7  Fran
: 2152 6 6  ky
: 2160 2 6  c'est
: 2164 2 6  bon
: 2168 4 6  bon
: 2174 2 6  bon
- 2176
: 2178 4 6 Vas
: 2184 1 6  -y
: 2186 2 6  Fran
: 2190 6 6  ky
: 2198 2 6  c'est
: 2202 5 6  bon
- 2213
: 2216 2 9 Vas
: 2220 2 6  -y
* 2224 2 7  Fran
* 2228 6 6  ky
: 2236 2 6  c'est
: 2240 2 6  bon
: 2244 4 6  bon
: 2250 1 6  bon
- 2251
: 2252 4 6 Vas
: 2258 1 6  -y
: 2260 2 6  Fran
: 2264 6 6  ky
: 2272 2 6  c'est
: 2276 5 6  bon
- 2287
: 2290 2 9 Vas
: 2294 2 6  -y
: 2298 2 7  Fran
: 2302 6 6  ky
: 2310 2 6  c'est
: 2314 2 6  bon
: 2318 4 6  bon
: 2324 2 6  bon
- 2326
: 2328 4 6 Vas
: 2334 1 6  -y
: 2336 2 6  Fran
: 2340 6 6  ky
: 2348 2 6  c'est
: 2352 5 6  bon
- 2363
: 2366 2 9 Vas
: 2370 2 6  -y
: 2374 2 7  Fran
: 2378 6 6  ky
: 2386 2 6  c'est
* 2390 2 6  bon
* 2394 4 6  bon
* 2400 2 6  bon
- 2402
: 2404 1 6 Fruits
: 2406 1 4  de
: 2408 2 6  la
: 2412 1 6  pas
: 2414 8 6  sion
- 2422
: 2424 8 6 (J'aime quand tu touches)
- 2439
* 2442 1 6 Fruits
* 2444 1 6  de
* 2446 1 6  la
* 2448 2 6  pas
* 2452 6 6  sion
- 2458
: 2460 8 6 (Oh c'est super !)
- 2477
: 2480 1 6 Fruits
: 2482 1 6  de
: 2484 1 6  la
: 2486 1 6  pas
: 2488 8 6  sion
- 2496
: 2498 8 6 (Oh Franky c'est génial !)
- 2515
: 2518 1 6 Fruits
: 2520 1 6  de
: 2522 1 6  la
: 2524 1 6  pas
: 2526 8 6  sion
- 2534
: 2536 8 6 (Bon dessert mon amour !)
- 2551
: 2554 1 6 Fruits
: 2556 2 6  de
: 2560 1 6  la
: 2562 1 6  pas
: 2564 8 6  sion
- 2572
: 2574 8 6 (Décidément c'est dément !)
- 2589
: 2592 2 6 Fruits
: 2596 1 6  de
: 2598 1 6  la
: 2600 1 6  pas
: 2602 8 6  sion
E