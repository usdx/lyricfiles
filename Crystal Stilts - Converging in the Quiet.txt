#TITLE:Converging in the Quiet
#ARTIST:Crystal Stilts
#MP3:Crystal Stilts - Converging in the Quiet.mp3
#VIDEO:Crystal Stilts - Converging in the Quiet.mkv
#COVER:Crystal Stilts - Converging in the Quiet [CO].png
#BACKGROUND:Crystal Stilts - Converging in the Quiet [BG].jpg
#BPM:320.74
#GAP:21936.5
#VIDEOGAP:0
#ENCODING:UTF8
#PREVIEWSTART:21.936
#LANGUAGE:English
#GENRE:post-rock; indie
#YEAR:2008
#CREATOR:skizzo
: 0 7 -4 I
: 8 7 -4  dis
: 15 6 -1 cern
: 22 9 -1  a
: 32 10 -1  subtle
: 43 5 -4  str
: 48 23 -1 eam
- 72
: 74 5 -4 Con
: 79 15 -1 verging
: 95 7 -2  in
: 103 4 -2  the
: 108 10 -2  qui
: 118 21 -4 et
- 140
: 142 6 -1 Just
: 149 18 -1  behind
: 168 3 -1  the
: 172 26 -4  silence
- 199
: 201 4 -4 My
: 206 9 -2  mind
: 216 6 -2  has
: 223 7 -2  slipped
: 231 5 -2  in
: 236 2 -4 ~
: 238 8 -2 side
: 247 22 -4  it
- 270
: 272 9 -1 Feel
: 281 4 -1  the
: 286 6 -1  past
: 293 5 -1  be
: 298 4 -4 ing
: 303 6 -1  fed
: 310 16 -1  me
- 326
: 328 3 -1 Se
: 331 4 -4 cond
: 335 7 -2  hand
: 342 16 -2  Futures
: 359 12 -2  misled
: 372 31 -4  me
- 424
: 453 8 -6 Second
: 462 6 -2  hand
: 469 16 -2  futures
: 486 3 -2  mis
: 489 3 -4 ~
: 492 21 -2 led
: 513 3 -1 ~
: 516 3 -2 ~
: 519 2 -3 ~
: 522 29 -4  me
- 561
* 582 7 -6 Second
* 590 6 -2  hand
* 597 17 -2  futures
* 615 3 -2  mis
* 618 3 -4 ~
* 621 6 -2 led
: 628 36 -4  me
- 706
: 1534 7 -4 To
: 1542 7 -4  dev
: 1549 7 -1 our
: 1557 6 -1  my
: 1564 12 -1  mem
: 1576 3 -4 o
: 1579 31 -1 ries
- 1611
: 1613 8 -1 In
: 1622 4 -1  a
: 1627 12 -2  single
: 1640 12 -2  sitt
: 1652 21 -4 ing
- 1675
: 1677 8 -1 Seems
: 1686 7 -1  the
: 1694 12 -1  only
: 1707 29 -4  means
- 1738
: 1740 3 -4 The
: 1744 11 -1  only
: 1756 10 -2  means
: 1767 3 -1  be
: 1770 11 -2 fitt
: 1781 18 -4 ing
- 1799
: 1800 2 -4 A
: 1803 3 -4  re
: 1806 13 -1 union
: 1820 1 -1  with
: 1822 6 -1  my
: 1829 5 -1  be
: 1834 4 -4 ~
: 1838 21 -1 loved
- 1860
: 1862 2 -4 A
: 1865 4 -4  re
: 1869 15 -1 union
: 1885 5 -2  with
: 1891 6 -2  the
: 1898 46 -4  sun
- 1965
: 1988 4 -4 A
: 1993 4 -4  re
: 1997 13 -1 union
: 2011 7 -1  with
: 2019 4 -1  the
: 2024 15 -2  stars
: 2039 45 -4 ~
- 2094
: 2119 2 -6 A
: 2122 4 -6  re
: 2126 14 -2 union
: 2141 5 -2  with
: 2147 3 -2  the
: 2151 50 -4  sun
- 2243
: 3196 7 -5 Though
: 3204 5 -4  I
: 3210 8 -1  know
: 3218 8 -1  Endless
: 3227 9 -1  dawn
: 3237 3 -1  a
: 3240 4 -4 ~
: 3244 16 -1 waits
- 3261
: 3263 3 -4 Still
: 3267 9 -4  I
: 3277 5 -1  ro
: 3282 10 -2 tate
: 3293 8 -2  at
: 3302 2 -2  the
: 3305 4 -2  g
: 3309 8 -1 ate
: 3317 14 -4 ~
- 3332
: 3334 6 -4 To
: 3341 8 -1  watch
: 3350 6 -1  my
: 3357 9 -1  life
: 3367 7 -4  es
: 3374 12 -1 cape
- 3386
: 3388 14 -4 Never
: 3403 16 -1  turning
: 3420 9 -2  as
: 3430 2 -2  it
: 3433 9 -2  runs
: 3442 18 -4 ~
- 3460
: 3462 4 -4 My
: 3467 4 -4  re
: 3471 13 -1 union
: 3485 5 -1  with
: 3491 5 -1  the
: 3497 4 -4  sun
: 3501 22 -1 ~
- 3523
: 3525 5 -4 Never
: 3531 16 -1  turning
: 3548 7 -2  as
: 3556 3 -2  it
: 3560 24 -2  runs
: 3584 4 -1 ~
: 3588 4 -2 ~
: 3592 28 -4 ~
- 3630
: 3651 6 -6 Never
: 3658 4 -6  tur
: 3662 14 -2 ning
: 3677 6 -2  as
: 3684 3 -2  it
: 3688 33 -1  runs
: 3721 38 -4 ~
- 3769
: 3779 6 -6 Never
: 3786 5 -6  tur
: 3791 13 -2 ning
: 3805 6 -2  as
: 3812 3 -2  it
: 3816 10 -1  runs
: 3826 33 -4 ~
- 3901
: 3964 8 -4 I
: 3973 8 -4  dis
: 3981 7 -1 cern
: 3989 5 -1  a
: 3995 12 -1  subtle
: 4008 5 -4  stream
: 4013 25 -1 ~
- 4039
: 4041 4 -4 Con
: 4045 12 -2 verging
: 4058 9 -2  in
: 4068 4 -2  the
: 4073 10 -2  quiet
: 4083 23 -4 ~
- 4107
: 4109 7 -1 Just
: 4117 3 -4  be
: 4120 12 -1 hind
: 4133 2 -1  the
: 4136 29 -4  silence
- 4165
: 4167 6 -4 My
: 4174 8 -2  mind
: 4183 5 -2  has
: 4189 8 -2  slipped
: 4198 2 -2  in
: 4200 2 -4 ~
: 4202 9 -2 side
: 4212 37 -4  it
- 4259
* 4291 7 -4 My
* 4299 8 -2  mind
* 4308 5 -2  has
* 4314 9 -2  slipped
* 4324 4 -2  in
* 4328 4 -4 ~
* 4332 21 -2 side
* 4353 3 -1 ~
* 4356 3 -2 ~
* 4359 2 -3 ~
* 4362 39 -4  it
- 4411
: 4421 5 -6 My
: 4427 9 -2  mind
: 4437 6 -2  has
: 4444 8 -2  slipped
: 4453 13 -2  inside
: 4467 36 -4  it
E