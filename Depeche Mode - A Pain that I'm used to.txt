#TITLE:A Pain that I'm used to
#ARTIST:Depeche Mode
#MP3:Depeche Mode - A Pain that I'm used to.mp3
#VIDEO:Depeche Mode - A Pain that I'm used to.mp4
#COVER:Depeche Mode - A Pain that I'm used to.jpg
#BPM:219.83
#GAP:25587
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Pop
#YEAR:2005
#CREATOR:mustangfred and subkei
: 0 2 2 I'm
: 4 1 4  not
: 8 7 5  sure
: 17 2 2  what
: 20 3 4  I'm
: 25 2 5  look
: 29 1 4 ing
: 33 5 2  for
: 41 3 5  an
: 45 3 4 y
: 49 3 2 more
- 61
: 65 2 2 I
: 69 2 4  just
: 73 6 5  know
: 81 2 2  that
: 85 2 4  I'm
: 89 2 5  hard
: 93 2 5 er
: 97 2 2  to
* 101 2 5  con
* 105 2 8 sole
* 109 2 7 ~
* 113 2 5 ~
* 117 2 2 ~
- 128
: 131 3 2 I
: 135 1 4  don't
* 139 4 5  see
: 144 1 2 ~
: 147 2 2  who
: 151 1 4  I'm
: 155 3 5  try
: 159 2 5 ing
: 163 1 4  to
: 165 2 2  be
: 168 1 4  in
: 172 2 5 stead
: 175 2 4  of
: 180 2 2  me
- 192
: 196 1 2 But
: 200 1 4  the
: 205 3 5  key
: 209 2 2 ~
: 212 2 2  is
: 216 1 4  a
: 221 1 5  ques
: 224 3 4 tion
: 229 1 2  of
: 232 2 4  con
: 237 4 8 trol
: 242 2 7 ~
: 245 3 5 ~
: 249 2 2 ~
- 280
: 521 3 2 Can
: 525 1 4  you
: 530 4 5  say
: 535 1 2 ~
: 538 1 2  what
: 542 1 4  you're
: 546 3 5  try
: 550 1 5 ing
: 554 1 4  to
: 556 3 2  play
- 560
: 562 3 5 an
: 566 3 4 y
: 570 3 2 way
- 583
: 587 2 2 I
: 591 1 4  just
: 595 4 5  pay
: 600 1 2 ~
: 603 3 2  while
: 607 1 4  you're
- 609
: 611 2 5 break
: 615 3 4 ing
: 619 3 2  all
: 623 2 4  the
: 627 4 8  rules
: 632 2 7 ~
: 635 2 5 ~
: 638 4 2 ~
- 649
: 652 2 2 All
: 655 1 4  the
* 660 5 5  signs
: 668 3 2  that
: 672 1 4  I
: 676 6 5  find
: 684 2 2  have
: 688 3 4  been
: 692 2 5  un
: 696 3 4 der
: 701 3 2 lined
- 713
: 717 3 2 Dev
: 721 1 4 ils
: 725 7 5  thrive
: 734 1 2  on
: 737 1 4  the
: 741 6 5  drive
: 749 3 2  that
: 753 1 4  is
: 758 3 8  fu
: 762 3 7 ~
: 766 2 5 ~
: 769 2 2 eled
- 778
: 781 3 2 All
: 786 1 5  this
* 789 3 7  run
* 793 4 9 ning
: 798 2 7  a
: 802 9 5 round
- 812
: 814 2 2 Well
: 818 1 5  it's
: 821 3 7  get
: 825 3 9 ting
: 829 2 7  me
: 833 10 5  down
: 845 2 2  just
- 851
: 853 2 7 give
: 858 2 9  me
: 861 1 7  a
: 866 6 5  pain
: 874 2 2  that
: 878 4 5  I'm
* 885 4 9  used
* 893 5 5  to
- 906
: 909 3 2 I
: 914 2 5  don't
: 917 2 7  need
: 921 1 9  to
: 926 2 7  be
: 929 9 5 lieve
- 939
: 941 3 2 All
: 946 1 5  the
: 950 1 7  dreams
: 954 1 9  you
: 958 1 7  con
: 961 7 5 ceive
- 972
: 974 1 2 You
: 977 1 5  just
: 982 1 7  need
: 985 2 9  to
: 989 1 7  a
: 994 3 5 chieve
- 1000
: 1002 1 2 Some
: 1005 1 5 thing
: 1009 1 2  that
* 1014 2 8  rings
: 1021 6 5  true
- 1056
: 1353 3 2 There's
: 1358 2 4  a
: 1362 6 5  hole
: 1369 4 2  in
: 1374 1 4  your
* 1379 6 5  soul
: 1387 1 2  like
: 1391 2 4  an
: 1395 3 5  an
: 1399 2 4 i
: 1403 5 2 mal
- 1416
: 1419 2 2 With
: 1423 2 4  no
: 1427 6 5  con
: 1436 1 2 science,
: 1440 1 4  re
: 1443 6 5 pen
: 1452 1 2 tance,
: 1456 1 4  oh
: 1459 6 8  no
: 1466 2 7 ~
: 1469 2 5 ~
: 1472 8 2 ~
- 1482
: 1484 2 2 Close
: 1489 2 4  your
: 1492 6 5  eyes
- 1500
: 1502 2 2 pay
: 1505 1 4  the
: 1509 5 5  price
: 1517 2 2  for
: 1521 2 4  your
: 1525 3 5  par
: 1529 2 4 a
: 1533 6 2 dise
- 1547
: 1550 3 2 Dev
: 1554 1 4 ils
: 1558 6 5  feed
: 1565 3 2  on
: 1570 1 4  the
: 1574 5 5  seeds
: 1582 2 2  of
: 1586 1 4  the
* 1590 4 8  soul
* 1595 3 7 ~
* 1599 2 5 ~
* 1602 7 2 ~
- 1638
: 1871 1 2 I
: 1875 2 2  can't
: 1879 1 4  con
: 1883 6 5 ceal
: 1891 3 2  what
: 1895 2 4  I
: 1900 5 5  feel,
: 1907 3 2  what
: 1911 3 4  I
: 1915 4 5  know
: 1920 1 4  is
: 1925 2 2  real
- 1936
: 1940 3 2 No
: 1944 1 4  mis
: 1948 6 5 tak
: 1957 1 2 ing
: 1961 1 4  the
: 1965 5 5  fak
: 1973 2 2 ing,
: 1977 1 4  I
: 1982 4 8  care
: 1987 2 7 ~
: 1990 2 5 ~
: 1993 5 2 ~
- 2003
: 2005 3 2 With
: 2009 1 4  a
: 2013 5 5  pra
: 2019 2 4 yer
: 2022 2 2  in
: 2025 4 4  the
: 2030 5 5  air
: 2037 4 2  I
: 2042 2 4  will
: 2046 2 5  leave
: 2050 1 4  it
: 2054 3 2  there
- 2066
: 2070 4 2 On
: 2075 1 4  a
: 2079 5 5  note
: 2087 3 2  full
: 2091 2 4  of
: 2095 5 5  hope
: 2103 2 2  not
: 2107 1 5  de
: 2111 8 8 spair
: 2120 2 7 ~
: 2124 1 5 ~
: 2127 1 2 ~
- 2133
: 2135 3 2 All
: 2139 1 4  this
: 2143 3 7  run
: 2147 3 8 ning
: 2151 3 7  a
: 2155 7 5 round
- 2166
: 2168 2 2 Well
: 2171 1 5  it's
* 2175 3 7  get
: 2179 2 9 ting
: 2183 2 7  me
: 2187 6 5  down
- 2197
: 2199 2 2 Just
: 2207 2 7  give
: 2211 3 9  me
: 2215 1 7  a
: 2220 5 5  pain
: 2228 1 2  that
: 2231 3 5  I'm
: 2239 4 9  used
: 2247 4 5  to
- 2260
: 2264 2 2 I
: 2267 2 5  don't
: 2271 1 7  need
: 2275 1 9  to
: 2279 3 7  be
: 2283 7 5 lieve
- 2293
: 2295 3 2 All
: 2299 1 5  the
: 2303 2 7  dreams
: 2308 1 9  you
: 2311 1 7  con
: 2315 6 5 ceive
- 2325
: 2327 1 2 You
: 2331 1 5  just
: 2335 2 7  need
: 2339 2 9  to
: 2343 1 7  a
: 2347 3 5 chieve
- 2353
: 2355 2 2 some
: 2359 1 5 thing
: 2363 1 2  that
: 2367 4 8  rings
: 2375 4 5  true
- 2388
: 2391 2 2 All
: 2394 1 5  this
: 2399 3 7  run
: 2403 2 9 ning
: 2407 3 7  a
: 2411 8 5 round
- 2421
: 2423 2 2 Well
: 2427 1 5  it's
: 2431 3 7  get
: 2435 2 9 ting
: 2439 2 7  me
: 2443 7 5  down
- 2453
: 2455 2 2 Just
: 2462 2 7  give
: 2467 2 9  me
: 2470 2 7  a
: 2475 5 5  pain
: 2483 2 2  that
: 2487 2 5  I'm
: 2494 4 9  used
: 2503 3 5  to
- 2515
: 2519 3 2 I
: 2523 2 5  don't
: 2527 2 7  need
: 2531 2 9  to
: 2534 3 7  be
: 2539 7 5 lieve
- 2549
: 2551 2 2 All
: 2555 1 5  the
: 2559 2 7  dreams
: 2563 1 9  you
: 2567 1 7  con
: 2571 6 5 ceive
- 2581
: 2583 1 2 You
: 2586 1 5  just
: 2591 2 7  need
: 2595 2 9  to
: 2599 1 7  a
: 2603 3 5 chieve
- 2609
: 2611 1 2 Some
: 2614 2 5 thing
: 2619 1 2  that
: 2622 4 8  rings
: 2630 5 5  true
E