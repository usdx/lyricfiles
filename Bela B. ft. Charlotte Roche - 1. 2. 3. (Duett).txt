#TITLE:1. 2. 3. (Duett)
#ARTIST:Bela B. ft. Charlotte Roche
#MP3:Bela B. ft. Charlotte Roche - 1. 2. 3. (Duett).mp3
#VIDEO:Bela B. ft. Charlotte Roche - 1. 2. 3. (Duett) [VD#0].avi
#COVER:Bela B. ft. Charlotte Roche - 1. 2. 3. (Duett) [CO].jpg
#BPM:258
#GAP:18450
#ENCODING:UTF8
#PREVIEWSTART:18,450
#LANGUAGE:German
#GENRE:Rock
#YEAR:2006
P1
: 0 3 3 Fünf
: 4 2 3  Uhr
: 7 3 3  früh
: 11 3 3  in
: 15 3 3  der
: 19 7 3  Nacht
: 27 10 5 bar
- 39
: 47 3 5 noch
* 51 9 9  längst
: 63 6 5  kein
: 72 4 3  Schlaf
: 77 6 5  in
: 84 8 5  Sicht
- 94
: 108 3 5 Ich
: 112 2 5  bin
: 115 4 5  zum
: 120 4 5  Den
: 125 6 3 ken
: 132 4 5  hier
- 138
: 143 4 5 star
: 147 4 5 re
: 151 4 5  in
: 156 6 3  mein
: 163 7 5  Bier
- 172
: 180 3 -4 Nur
: 184 6 0  re
: 192 6 0 den
: 200 4 -2  will
: 205 6 -2  ich
: 212 6 -7  nicht
- 219
: 220 3 3 Da
: 224 3 3  tippt
: 228 3 3  mir
: 232 3 3  was
: 236 3 3  auf
: 240 2 3  die
: 244 6 3  Schul
: 252 5 5 ter
- 259
: 276 3 3 und
: 280 6 3  grinst
: 287 7 3  sich
: 295 4 3  in
: 300 3 3  mei
: 304 3 5 ne
: 308 6 5  Welt
- 316
: 367 4 5 Sagt,
: 372 3 5  sie
: 376 4 5  tut
: 381 5 3  ihm
: 387 7 5  leid,
- 396
: 399 3 5 sei
: 403 3 3 ne
: 407 3 5  Auf
: 411 4 5 dring
: 416 3 3 lich
: 420 5 5 keit
- 427
: 432 3 5 Nennt
: 436 3 3  mich
: 440 3 5  Dirk
: 444 3 5  und
: 448 3 5  hat
: 452 5 3  uns
: 459 4 5  Schnaps
: 464 3 3  be
: 468 6 5 stellt
- 475
: 476 3 3 Und
: 480 2 3  ich
: 484 6 3  sag
: 491 8 5  ihm
- 501
: 505 5 9 Ers
: 511 9 5 tens:
: 527 4 3  Du
: 532 6 3  machst
: 539 4 5  hier
: 544 3 5  ei
: 547 3 3 nen
: 552 7 5  Feh
: 560 4 8 ler
- 565
: 567 7 9 Zwei
: 575 7 5 tens:
: 591 3 3  Für
: 596 5 3  dich
: 604 3 5  im
: 608 3 5 mer
: 612 3 3  noch
: 616 6 5  Be
: 624 4 8 la
- 629
: 631 7 10 Drit
: 639 8 5 tens:
: 655 4 3  Ich
: 660 7 3  trin
: 668 3 5 ke
: 672 3 5  nicht
: 676 3 3  mit
: 680 7 5  je
: 688 5 8 dem
- 694
: 695 7 9 Vier
: 703 8 5 tens:
: 719 4 3  Ich
: 724 6 3  will
: 732 3 5  nicht
: 736 3 5  mit
: 740 2 3  dir
: 744 7 5  re
: 752 6 8 den
- 760
: 785 2 3 Ich
: 788 6 3  will
: 796 3 5  nicht
: 800 3 5  mit
: 804 3 3  dir
: 808 6 5  re
: 816 6 -4 den
- 824
: 897 3 3 Ehr
: 901 2 3 lich
: 904 4 3 keit
: 909 2 3  ir
: 912 3 3 ri
: 916 7 3 tiert
: 925 8 5  ihn
- 935
: 945 5 5 und
* 952 5 9  ich
: 960 6 5  zieh
: 968 4 3  mich
: 973 6 5  zu
: 981 8 5 rück
- 991
: 1004 3 5 Da
: 1008 4 5  plötz
: 1013 3 5 lich
: 1017 4 5  seh
: 1023 3 3  ich
: 1028 5 5  sie,
- 1034
: 1036 2 5 die
: 1039 4 5  Frau
: 1044 3 5  zum
* 1048 4 5  Nie
* 1053 5 3 der
: 1059 7 5 knien
- 1068
: 1076 3 -4 Und
: 1080 4 0  ich
: 1088 5 0  ver
: 1095 4 -2 such
: 1100 6 -2  mein
: 1107 4 -7  Glück
- 1113
: 1120 5 3 Willst
: 1127 4 3  du
: 1132 5 3  'nen
: 1140 6 5  Drink
- 1148
* 1184 4 9 Hey,
: 1191 3 3  komm
: 1195 3 3  doch
: 1199 4 3  mal
: 1204 6 3  nä
: 1211 5 5 her
- 1218
: 1248 4 3 Denn
: 1271 6 9  er
: 1280 6 5 stens:
: 1299 5 3  Hi,
: 1307 3 3  ich
: 1311 4 3  bin
: 1316 3 3  der
: 1320 6 3  Be
: 1328 4 5 la
- 1333
: 1335 6 9 Zwei
: 1343 7 5 tens:
: 1359 4 3  Ich
: 1364 5 3  mag
: 1371 4 5  Nel
: 1376 3 5 son
: 1380 3 3  Man
: 1384 7 5 de
: 1392 4 8 la
- 1398
: 1400 6 10 Drit
: 1408 6 5 tens:
: 1423 3 3  Ich
: 1427 3 3  bin
: 1431 4 3  kei
: 1435 3 5 ner
: 1440 3 5  von
: 1444 3 3  den
: 1448 7 5  Blö
: 1456 4 8 den
- 1462
: 1464 6 9 Vier
: 1472 6 5 tens:
: 1488 3 3  Ich
: 1492 3 3  will
: 1499 3 5  nur
: 1503 4 5  mit
: 1508 3 3  dir
: 1512 7 5  re
: 1520 4 8 den
- 1526
: 1548 3 3 Nur
: 1552 3 3  ein
: 1556 5 3  biss
: 1563 3 5 chen
: 1567 3 5  mit
: 1571 3 3  dir
: 1575 6 5  re
: 1583 5 -4 den
- 1590
: 1628 5 5 Made
: 1633 4 5 moi
: 1638 6 5 selle
- 1646
: 1821 5 5 Let's
: 1828 10 5  swing
: 1844 3 5  and
: 1848 7 5  dance,
: 1856 10 5  babe!
- 1868
: 1914 3 3 Und
: 1918 3 5  sie
: 1923 9 5  sagt
- 1934
: 1944 6 9 Ers
: 1952 6 5 tens:
- 1960
: 2007 5 9 Zwei
: 2015 7 5 tens:
- 2024
: 2071 6 3 Drit
: 2079 7 5 tens:
- 2088
: 2135 5 9 Vier
: 2143 6 5 tens:
- 2151
: 2264 6 9 Ers
: 2272 6 5 tens:
: 2291 7 3  Hey,
: 2300 3 3  ich
: 2304 3 3  komm
: 2308 3 3  gleich
: 2312 6 3  wie
: 2320 4 5 der
- 2325
: 2327 7 9 Zwei
: 2335 7 5 tens:
: 2355 7 3  Hol
: 2363 3 3  ihn
: 2367 3 3  schon
: 2371 3 3  mal
: 2375 7 3  rü
: 2383 4 5 ber
- 2389
: 2392 6 10 Drit
: 2399 7 5 tens:
: 2419 7 3  Bit
: 2427 4 3 te
: 2431 3 3  nicht
: 2435 7 3  falsch
: 2442 3 5  ver
: 2446 4 5 ste
: 2450 3 5 hen
- 2454
: 2456 6 9 Vier
: 2464 6 5 tens:
: 2476 3 3  Das
: 2479 4 3  sieht
: 2484 5 3  nur
: 2491 4 5  so
: 2496 4 5  aus,
- 2500
: 2500 3 3 als
: 2504 3 5  würd'
: 2508 3 5  ich
: 2512 6 -4  gehen
- 2520
: 2528 6 3 Das
: 2539 6 3  sieht
: 2547 6 3  nur
: 2555 4 5  so
: 2560 3 5  aus,
- 2564
: 2564 2 3  als
: 2567 4 5  würd'
: 2572 3 5  ich
: 2576 5 -4  gehen
- 2583
: 2592 8 3 Das
: 2603 6 3  sieht
: 2610 8 5  nur
: 2619 3 5  so
: 2623 4 5  aus
- 2628
: 2628 3 3  als
: 2632 3 5  würd'
: 2636 3 5  ich
: 2640 7 -4  gehen
- 2649
: 2656 6 3 Das
: 2666 7 5  sieht
* 2675 7 8  so
: 2684 6 5  aus,
: 2691 4 3  als
: 2696 4 5  wür
: 2700 3 8 de
* 2704 5 8  ich
: 2711 4 5  ge
: 2716 4 5 hen
- 2722
: 2741 2 5 Bei
: 2744 3 5  uns
: 2748 2 5  zu
: 2750 3 5  Hau
: 2753 2 5 se
: 2756 1 5  gehn
: 2758 2 5  al
: 2760 1 5 le
: 2762 2 5  rück
: 2765 4 5 wärts
P2
: 1967 3 3  Ich
: 1971 6 3  bin
: 1979 3 5  mit
: 1983 4 5  mei
: 1987 2 3 nem
: 1991 7 5  Freund
: 1999 4 8  hier
- 2005
: 2030 3 3  Er
: 2035 7 3  ist
: 2043 3 5  ein
* 2047 3 5  Me
* 2051 3 3 ga-
: 2055 3 5 Fan
: 2060 3 5  von
: 2064 3 -3  dir
- 2069
: 2091 4 5  Ich
: 2096 3 5  werd'
: 2100 6 3  ihn
: 2107 4 5  mal
: 2112 3 5  rü
: 2115 3 3 ber
: 2120 6 5 win
: 2128 4 8 ken
- 2134
: 2156 3 3  Er
: 2160 3 3  würd
: 2164 5 3  gern
: 2171 3 5  was
: 2175 3 5  mit
: 2179 3 3  dir
: 2183 7 5  trin
: 2191 4 -3 ken
- 2197
: 2229 5 3 Nur
: 2236 3 5  was
: 2240 3 5  mit
: 2244 3 3  dir
: 2248 7 5  trin
: 2256 5 -4 ken
E