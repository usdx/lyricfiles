#TITLE:Stimmen Im Wind
#ARTIST:Juliane Werding
#MP3:Juliane Werding - Stimmen Im Wind.mp3
#COVER:Juliane Werding - Stimmen Im Wind [CO].jpg
#BACKGROUND:Juliane Werding - Stimmen Im Wind [BG].jpg
#BPM:116
#GAP:17600
: 0 3 59 schwar
: 3 3 59 ze
: 6 5 64  Vö
: 11 6 68 gel,
: 17 3 69  ro
: 20 3 68 ter
: 23 4 66  Him
: 27 6 64 mel
- 37
: 40 4 60 Frau
: 44 2 60  am
: 46 8 60  Meer
- 59
: 65 3 59 riecht
: 68 3 59  an
: 71 4 64  Blu
: 75 4 68 men
- 81
: 82 5 69 aber
: 87 6 66  ih
: 93 2 64 re
: 95 9 60  Hand
: 108 3 60  ist
: 111 7 59  leer
- 123
: 128 3 66 sieht
: 131 3 66  ein
: 134 7 65  Schiff
: 141 3 66  im
: 144 5 68  Sturm
: 149 2 66  ver
: 151 4 65 sin
: 155 5 66 ken
- 162
: 165 3 64 hört
: 168 3 64  Men
: 171 3 64 schen
: 174 9 63  schrein
- 187
: 193 4 61 sie
: 197 4 66  ist
: 201 3 65  nicht
: 204 3 66  ver
: 207 4 68 las
: 211 4 66 sen
: 217 4 64  nur
: 221 2 63  al
: 223 8 64 lein
- 237
: 256 3 64 Stim
: 259 2 64 men
: 261 2 63  im
: 263 8 64  Wind
- 274
: 277 2 63 die
: 279 2 64  sie
: 281 2 66  ru
: 283 2 64 fen,
: 285 2 63  wenn
: 287 2 61  der
: 289 2 64  Ab
: 291 2 64 end
: 293 2 63  be
: 295 7 64 ginnt
- 304
: 308 3 63 sei
: 311 2 64  nicht
: 313 2 66  trau
: 315 2 64 rig,
: 317 2 59  Su
: 319 6 61 zanne
- 330
: 341 2 64 es
: 343 2 64  fängt
: 345 2 66  al
: 347 2 64 les
: 349 2 63  erst
: 351 7 64  an
- 365
: 384 3 64 Stim
: 387 2 64 men
: 389 2 63  im
: 391 7 64  Wind
- 401
: 405 2 63 die
: 407 1 64  so
: 408 3 66  zärt
: 411 2 64 lich
: 413 2 63  und
: 415 1 61  so
: 416 2 64  lie
: 418 2 64 be
: 420 3 63 voll
: 423 5 64  sind
- 431
: 436 3 63 sei
: 439 2 64  nicht
: 441 2 66  trau
: 443 2 64 rig,
: 445 2 59  Su
: 447 6 61 zanne
- 458
: 469 2 64 es
: 471 2 64  fängt
: 473 2 66  al
: 475 2 64 les
: 477 2 63  erst
: 479 7 64  an
- 493
: 512 3 59 Läch
: 515 4 59 eln
: 519 6 64  in
: 525 2 68  er
: 527 4 69 schrock
: 531 4 68 nen
: 535 4 66  Au
: 539 6 64 gen
- 549
: 552 3 60 blind
: 555 3 60  vom
: 558 9 60  Licht,
- 571
: 576 3 59 Trä
: 579 3 59 nen
: 582 6 64  wie
: 588 4 68  aus
: 592 4 69  Eis
: 596 2 68  ver
: 598 4 66 bren
: 602 6 64 nen
- 612
: 617 2 60 ihr
: 619 3 60  Ge
: 622 11 59 sicht
- 636
: 640 4 66 Päar
: 644 3 66 chen
: 647 5 65  auf
: 652 4 66  ver
: 656 3 68 gilb
: 659 3 66 ten
: 662 4 65  Fo
: 666 6 66 tos
- 674
: 676 4 64 der
: 680 3 64  Phan
: 683 3 64 ta
: 686 7 63 sie
- 697
: 704 4 61 Men
: 708 5 66 schen,
: 713 2 65  die
: 715 4 66  sich
: 719 5 68  lie
: 724 3 66 ben
: 727 4 64  ster
: 731 3 63 ben
: 734 9 64  nie
- 749
: 767 3 64 Stim
: 770 3 64 men
: 773 2 63  im
: 775 6 64  Wind
- 784
: 788 2 62 die
: 790 2 64  sie
: 792 2 66  ru
: 794 2 64 fen,
: 796 3 63  wenn
: 799 2 61  der
: 801 2 64  Ab
: 803 2 64 end
: 805 2 63  be
: 807 6 64 ginnt
- 816
: 820 2 63 sei
: 822 2 64  nicht
: 824 2 66  trau
: 826 2 64 rig,
: 828 2 59  Su
: 830 7 61 zanne
- 843
: 852 2 64 es
: 854 2 64  fängt
: 856 2 66  al
: 858 2 64 les
: 860 3 63  erst
: 863 7 64  an
- 877
: 896 2 64 Stim
: 898 3 64 men
: 901 1 63  im
: 902 7 64  Wind
- 912
: 916 2 63 die
: 918 2 64  so
: 920 2 66  zärt
: 922 3 64 lich
: 925 1 63  und
: 926 2 61  so
: 928 2 64  lie
: 930 2 64 be
: 932 2 63 voll
: 934 9 64  sind
- 945
: 948 2 63 sei
: 950 2 64  nicht
: 952 2 66  trau
: 954 2 64 rig,
: 956 2 59  Su
: 958 7 61 zanne
- 971
: 980 2 64 es
: 982 2 64  fängt
: 984 2 66  al
: 986 2 64 les
: 988 3 63  erst
: 991 7 64  an
- 1006
: 1024 2 66 und
: 1026 3 66  der
: 1029 6 65  Mann,
: 1035 5 66  mit
: 1040 2 68  dem
: 1042 4 66  sie
: 1046 4 65  re
: 1050 6 66 det
- 1058
: 1060 4 64 bleibt
: 1064 2 64  un
: 1066 4 64 sicht
: 1070 7 63 bar
- 1081
: 1088 3 61 Men
: 1091 5 66 schen,
: 1096 2 65  die
: 1098 4 66  sich
: 1102 6 68  lie
: 1108 4 66 ben
: 1112 2 64  sind
: 1114 4 63  sich
: 1118 9 64  nah
- 1127
: 1151 3 64 Stim
: 1154 3 64 men
: 1157 1 63  im
: 1158 8 64  Wind
- 1169
: 1172 2 63 die
: 1174 2 64  sie
: 1176 2 66  ru
: 1178 2 64 fen,
: 1180 2 63  wenn
: 1182 2 61  der
: 1184 2 64  Ab
: 1186 2 64 end
: 1188 2 63  be
: 1190 7 64 ginnt
- 1201
: 1204 2 63 sei
: 1206 2 64  nicht
: 1208 2 66  trau
: 1210 2 64 rig,
: 1212 2 59  Su
: 1214 7 61 zanne
- 1227
: 1236 2 64 es
: 1238 2 64  fängt
: 1240 2 66  al
: 1242 2 64 les
: 1244 2 63  erst
: 1246 7 64  an
- 1259
: 1279 3 64 Stim
: 1282 2 64 men
: 1284 2 63  im
: 1286 6 64  Wind
- 1295
: 1300 2 63 die
: 1302 1 64  so
: 1303 3 66  zärt
: 1306 2 64 lich
: 1308 2 63  und
: 1310 2 61  so
: 1312 2 64  lie
: 1314 2 64 be
: 1316 2 63 voll
: 1318 7 64  sind
- 1328
: 1332 2 63 sei
: 1334 2 64  nicht
: 1336 2 66  trau
: 1338 2 64 rig,
: 1340 2 59  Su
: 1342 7 61 zanne
- 1355
: 1364 2 64 es
: 1366 2 64  fängt
: 1368 2 66  al
: 1370 2 64 les
: 1372 2 63  erst
: 1374 7 64  an
E

