#TITLE:Flashlight (World Championship)
#ARTIST:Pitch Perfect
#LANGUAGE:English
#MP3:Pitch Perfect - Flashlight (World Championship).mp3
#COVER:Pitch Perfect - Flashlight (World Championship).jpg
#VIDEO:Pitch Perfect - Flashlight (World Championship).mp4
#BPM:390.61
#GAP:28707
#AUTHOR:mulf2
: 3 5 -3 When
: 9 2 -3  to
: 13 5 2 mor
: 20 4 2 row
: 25 21 1  comes
- 47
: 49 4 -3 I'll
: 54 3 -3  be
: 60 3 2  on
: 64 4 2  my
: 70 16 1  own
- 88
: 91 2 -3 Fee
: 94 4 -3 ling
: 100 5 2  frigh
: 106 6 2 tened
: 114 11 1  of
- 127
: 129 3 -3 The
: 133 4 -3  things
: 138 4 -3  that
: 144 2 2  I
: 149 4 2  don't
: 154 11 1  know
- 167
* 173 4 2 When
* 179 2 4  to
* 185 3 6 mor
* 190 3 4 row
* 195 16 2  comes
- 213
: 215 4 2 When
: 222 2 4  to
: 227 4 6 mor
: 233 3 4 row
: 240 11 2  comes
- 253
: 257 5 2 When
: 264 2 4  to
: 270 4 6 mor
: 278 4 4 row
: 287 19 2  comes
- 308
: 339 3 -3 And
: 344 4 -3  though
: 349 2 -3  the
: 352 5 2  road
: 360 2 2  is
* 364 13 1  long
- 379
: 381 2 -3 I
: 384 5 -3  look
: 391 2 -3  up
: 394 3 2  to
: 398 3 2  the
* 403 22 1  sky
- 427
: 429 3 -3 Dark
: 434 3 -3 ness
: 439 3 2  all
: 444 1 2  a
: 446 12 1 round
- 460
: 464 2 -3 I
: 468 5 -3  hope
: 474 3 -3  that
: 479 2 2  I
: 482 3 1  could
: 486 17 2  fly
- 505
: 512 2 2 Then
: 516 2 4  I
: 520 5 6  sing
: 527 3 4  a
: 532 14 2 long
- 548
* 552 3 2 Then
* 556 3 4  I
* 560 6 6  sing
* 568 2 4  a
* 571 14 2 long
- 587
: 593 4 2 Then
: 599 2 4  I
: 604 8 6  sing
: 614 3 9  a
: 620 14 6 long
- 636
: 687 2 6 I
: 691 4 6  got
: 698 3 6  all
: 703 2 6  I
: 706 4 6  need
- 712
: 714 3 4 When
: 719 1 2  I
: 722 3 4  got
: 728 4 6  you
: 734 2 6  and
: 739 14 4  I
- 755
: 758 4 4 'Cause
: 765 2 4  I
: 768 4 4  look
: 775 1 4  a
: 778 8 4 round
: 788 3 4  me
- 792
: 794 3 -3 And
: 798 5 -3  see
: 804 2 -3  your 
* 807 10 7 sweet
* 819 19 6  life
- 840
: 845 2 6 I'm
: 849 10 6  stuck
: 862 2 6  in
: 866 3 6  the
: 870 6 6  dark
: 877 3 4 ness
- 881
: 883 2 2 You're
: 887 2 4  my
* 890 13 6  flash
* 904 19 7 light
- 925
: 927 3 2 You're
: 932 5 4  get
: 938 3 6 tin'
: 942 8 6  me
- 952
: 954 4 2 Get
: 958 3 4 tin'
: 962 6 6  me
- 970
: 976 3 6 Through
: 980 1 9  the
: 983 8 6  night
: 991 7 4 ~
: 998 4 2 ~
- 1004
: 1011 2 2 You
: 1014 4 6  kick
: 1019 8 6 start
: 1029 4 6  my
: 1035 8 6  heart
- 1045
: 1047 3 6 When
: 1051 2 6  you
: 1055 6 6  shine
: 1062 3 6  it
: 1067 3 6  in
: 1071 5 7  my
* 1079 10 4  eyes
- 1091
: 1093 1 4 I
: 1095 10 4  can't
: 1106 15 4  lie
- 1123
: 1130 3 4 It's
: 1134 1 4  a
: 1136 10 7  sweet
* 1149 4 6  li
* 1154 6 4 ~
* 1161 12 2 fe
- 1174
: 1174 3 6 I'm
: 1179 8 6  stuck
: 1190 4 6  in
: 1195 4 6  the
: 1201 4 6  dark
- 1206
: 1207 2 4 But
: 1211 3 2  you're
: 1216 2 4  my
: 1220 10 6  flash
: 1231 17 19 light
- 1250
: 1260 2 2 You're
: 1263 4 2  get
: 1268 3 4 tin'
: 1272 5 6  me
- 1279
: 1283 10 6 Through
: 1296 7 9  the
: 1309 36 6  night
- 1347
: 1336 16 9 Oh
: 1354 18 2 oh
: 1374 7 4 oh
: 1383 11 6 oh
: 1396 75 4 ooh
- 1473
: 1486 29 9 Oh
: 1516 19 2 oh
: 1536 9 4 oh
: 1546 11 6 oh
: 1558 89 4 ohh
- 1649
: 1687 10 -3 'cause
: 1699 3 -3  you're
: 1703 4 -3  my
: 1708 15 7  flash
: 1724 10 6 light
- 1736
: 1733 4 -3 'cause
: 1739 4 -3  you're
: 1744 4 -3  my
: 1750 13 7  flash
: 1764 11 6 light
- 1776
: 1777 3 -3 'cause
: 1781 3 -3  you're
: 1785 4 -3  my
* 1790 14 7  flash
* 1805 9 6 light
- 1815
: 1817 3 0 'cause
: 1821 3 0  you're
: 1825 4 0  my
: 1830 14 11  flash
: 1847 11 9 light
- 1860
: 1869 3 -3 You're
: 1873 2 -3  my
: 1876 10 7  flash
: 1887 18 6 light
- 1906
: 1907 3 2 You're
: 1911 4 2  get
: 1916 4 4 tin'
: 1923 2 6  me
- 1927
: 1933 10 6 Through
: 1946 8 9  the
: 1960 28 6  night
- 1990
: 1994 2 7 I
: 1998 3 7  got
: 2002 2 7  all
: 2008 2 7  I
: 2013 4 7  need
- 2018
: 2019 5 5 When
: 2026 2 3  I
: 2030 3 5  got
: 2036 3 7  you
: 2040 3 7  and
: 2048 7 5  I
- 2057
: 2064 5 5 'Cause
: 2071 2 5  I
: 2074 4 5  look
: 2080 2 5  a
: 2084 8 5 round
: 2094 3 5  me
- 2098
: 2100 2 5 And
: 2104 3 5  see
: 2108 2 5  your
* 2111 10 8  sweet
* 2127 16 7  life
- 2145
: 2151 2 7 I'm
: 2154 10 7  stuck
: 2168 2 7  in
: 2172 2 7  the
: 2175 4 7  dark
- 2181
: 2184 2 5 But
: 2187 3 3  you're
: 2191 2 5  my
: 2194 11 7  flash
: 2207 12 8 light
- 2221
: 2232 3 3 You're
: 2236 4 3  get
: 2241 5 5 tin'
: 2247 2 7  me
- 2251
: 2269 3 7 Through
: 2274 4 10  the
: 2288 23 7  night
- 2312
: 2314 3 3 You
: 2318 4 7  kick
: 2323 6 7 start
: 2332 3 7  my
: 2336 7 7  heart
- 2345
: 2349 3 7 When
: 2353 3 7  you
: 2357 5 7  shine
: 2365 2 7  it
: 2370 3 7  in
: 2375 3 8  my
* 2380 12 5  eyes
- 2393
: 2395 2 5 I
: 2399 6 5  can't
: 2407 9 5  lie
- 2418
: 2430 3 5 It's
: 2435 1 5  a
: 2437 7 8  sweet
* 2448 4 7  li
* 2454 3 5 ~
* 2460 12 7 fe
- 2474
: 2476 3 7 I'm
: 2481 6 7  stuck
: 2489 3 7  in
: 2495 3 7  the
: 2499 3 7  dark
- 2503
: 2505 4 5 But
: 2510 4 3  you're
: 2517 2 5  my
: 2521 8 7  flash
: 2530 15 8 light
- 2547
: 2557 2 3 You're
: 2562 3 3  get
: 2567 4 5 tin'
: 2572 7 7  me
- 2580
: 2582 12 7 Through
: 2597 4 10  the
: 2606 30 7  night
- 2638
: 2666 6 0 'cause
: 2673 4 0  you're
: 2678 2 0  my
: 2682 12 9  flash
: 2696 12 8 light
- 2710
F 2712 3 0 Cause
F 2718 4 0  you
F 2726 21 0  are
- 2749
: 2743 6 -3 'cause
: 2751 4 -3  you're
: 2756 3 -3  my
: 2760 15 6  flash
: 2775 13 5 light
- 2789
: 2789 4 0 'cause
: 2795 3 0  you're
: 2800 3 0  my
: 2804 13 11  flash
: 2817 17 9 light
- 2836
: 2840 2 -3 I'm
: 2843 2 -3  your
: 2846 15 6  flash
: 2861 18 5 light
- 2880
: 2881 4 3 You're
: 2886 4 3  get
: 2890 5 5 tin'
: 2896 4 7  me
- 2902
: 2907 11 7 Through
: 2922 9 10  the
: 2934 35 7  night
- 2970
: 3033 5 -3 'cause
: 3039 4 -3  you're
: 3044 3 -3  my
: 3048 15 6  flash
: 3064 13 5 light
- 3079
F 3074 4 0 'cause
F 3080 4 0  you're
F 3089 49 0  my
- 3140
: 3206 9 5 Get
: 3217 5 7 tin'
: 3223 6 9  me
* 3230 14 9  through
* 3247 8 12  the
* 3260 20 9  ni
* 3281 13 7 ~
* 3295 31 5 ght
E
