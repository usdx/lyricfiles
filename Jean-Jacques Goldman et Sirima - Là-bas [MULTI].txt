#TITLE:Lŕ-bas
#ARTIST:Jean-Jacques Goldman et Sirima
#LANGUAGE:French
#GENRE:Pop
#YEAR:1987
#MP3:Jean-Jacques Goldman et Sirima - Lŕ-bas.mp3
#COVER:Jean-Jacques Goldman et Sirima - Lŕ-bas.jpg
#VIDEO:Jean-Jacques Goldman et Sirima - Lŕ-bas.avi
#BPM:360
#GAP:26982
#DUETSINGERP1:Jean-Jacques Goldman
#DUETSINGERP2:Sirima
P 1
: 0 2 2 Lŕ-
: 4 5 2 bas
- 57
: 109 1 2 Tout
: 113 2 2  est
: 117 5 4  neuf
: 124 1 4  et
: 129 1 2  tout
: 134 7 4  est
: 150 2 9  sau
: 154 12 6 vage
- 214
: 265 2 2 Li
: 269 2 2 bre
: 275 2 4  con
: 280 2 4 ti
: 285 4 2 nent
: 301 6 4  sans
: 311 5 6  gril
: 319 12 2 lage
- 355
: 423 1 2 I
: 427 3 2 ci
: 432 5 4  nos
: 438 6 4  rę
: 446 2 2 ves
: 453 4 4  sont
: 463 1 6  é
: 467 8 2 troits
- 485
: 511 13 14 Mmm
: 526 5 13 ~
: 533 3 13 ~
: 538 6 11 ~
: 546 3 9  ~
: 550 1 2 ~
- 561
: 580 1 6 C'est
: 584 2 6  pour
: 590 1 7  ça
: 594 2 7  que
: 599 3 6  j'i
: 603 7 4 rai
: 618 3 1  lŕ-
: 623 5 2 bas
- 676
: 785 3 2 Lŕ-
: 790 5 2 bas
- 843
: 894 2 2 Faut
: 897 2 2  du
: 903 7 4  coeur
: 912 1 4  et
: 915 2 2  faut
: 920 5 4  du
: 934 3 9  cou
: 939 14 6 rage
- 1001
: 1050 2 2 Mais
: 1055 2 2  tout
: 1060 2 4  est
: 1065 2 4  pos
: 1071 5 2 si
: 1080 2 2 ble
: 1083 2 4  ŕ
: 1088 4 6  mon
: 1095 17 2  âge
- 1160
: 1208 2 2 Si
: 1212 3 2  tu
: 1216 3 4  as
: 1220 2 4  la
: 1226 5 2  for
: 1236 2 2 ce
: 1239 2 4  et
: 1242 2 6  la
: 1250 5 2  foi
- 1265
: 1291 18 14 Mmm
: 1311 7 13 ~
: 1320 3 13 ~
: 1324 5 11 ~
: 1330 5 9 ~
: 1336 2 2 ~
- 1348
: 1364 5 6 L'or
: 1370 1 6  est
: 1374 2 7  ŕ
: 1379 3 7  por
: 1385 2 6 tée
: 1390 4 4  de
: 1403 2 1  tes
: 1406 7 2  doigts
- 1423
: 1439 3 2 Mmm
: 1443 3 2 ~
: 1447 5 2 ~
: 1453 6 2 ~
- 1483
: 1522 2 6 C'est
: 1527 3 6  pour
: 1533 2 7  ça
: 1537 2 7  que
: 1542 3 6  j'i
: 1546 8 4 rai
: 1563 2 1  lŕ-
: 1567 3 2 bas
- 1618
: 2662 1 2 I
: 2666 1 2 ci
: 2671 1 5  tout
: 2675 5 5  est
: 2684 7 5  joué
: 2696 5 5  d'a
: 2705 18 5 vance
- 2733
: 2740 3 2 Et
: 2744 3 2  on
: 2749 2 7  n'y
* 2755 7 7  peut
: 2765 4 7  rien
: 2774 17 7  chan
: 2794 3 7 ger
: 2798 2 5 ~
: 2801 3 4 ~
: 2805 1 2 ~
- 2815
: 2818 2 2 Tout
* 2822 1 2  dé
* 2827 8 6 pend
: 2838 4 9  de
: 2847 7 9  ta
: 2857 2 9  nais
: 2863 16 9 sance
- 2888
: 2891 3 9 Et
* 2896 2 9  moi
: 2902 2 9  je
: 2906 3 9  ne
: 2914 6 9  suis
: 2925 5 7  pas
: 2935 3 6  bien
: 2941 7 7  né
: 2949 5 5 ~
: 2955 49 2 ~
: 3008 8 2  yeah
: 3017 7 2 ~
: 3025 4 2 ~
: 3030 1 -1 ~
: 3032 1 -3 ~
- 3081
: 3738 1 -3 J'au
: 3740 3 6 rai
: 3745 2 6  ma
: 3750 6 6  chan
: 3760 3 6 ce,
: 3765 3 6  j'au
: 3770 3 4 rai
: 3776 1 2  mes
: 3780 11 6  droits
- 3801
: 3815 2 -3 Et
: 3818 2 2  la
: 3823 3 2  fier
: 3830 4 2 té
: 3838 1 2  qu'i
: 3843 5 2 ci
: 3853 3 2  je
: 3858 1 2  n'ai
: 3863 3 -3  pas
- 3876
: 3895 1 6 Tout
: 3897 3 6  ce
: 3903 2 6  que
: 3907 3 6  tu
: 3912 3 6  mé
: 3917 3 6 rites
: 3922 7 7  est
: 3932 4 6  ŕ
: 3941 2 4  toi
: 3944 1 2 ~
- 3955
: 3972 1 -3 I
: 3977 3 2 ci
: 3981 2 2  les
: 3986 2 2  au
: 3991 2 2 tres
: 3994 3 2  im
: 4001 4 2 po
: 4006 2 2 ~
: 4010 1 -3 sent
: 4015 3 -3  leurs
: 4020 5 -3  lois
- 4035
: 4050 2 2 Je
: 4054 3 6  te
* 4059 7 9  per
* 4068 3 9 drai
: 4075 2 9  peut
: 4080 5 9  ę
: 4088 4 7 tre
: 4094 3 6  lŕ-
: 4099 4 6 bas
: 4104 3 4 ~
: 4108 1 2 ~
- 4119
: 4133 2 2 Je
: 4136 2 2  me
: 4143 6 2  perds
: 4153 2 -1  si
: 4158 2 -3  je
: 4163 6 2  res
: 4172 2 2 te
: 4176 3 2  lŕ
: 4180 1 2 ~
- 4191
: 4208 2 -3 La
: 4212 3 6  vie
: 4216 3 6  ne
: 4221 3 6  m'a
: 4227 6 6  pas
: 4235 2 6  lais
* 4241 12 7 sé
: 4255 5 6  le
: 4264 3 4  choix
: 4268 1 2 ~
- 4279
: 4287 2 -3 Toi
: 4290 3 2  et
: 4295 3 2  moi
: 4301 2 2  se
: 4307 5 2  s'ra
: 4315 1 2  lŕ-
: 4320 8 7 bas
: 4329 2 6  ou
: 4335 4 4  pas
: 4340 6 4 ~
- 4356
: 4368 2 6 Tout
: 4373 3 6  est
: 4377 4 6  neuf
: 4385 3 6  et
: 4392 5 6  tout
: 4403 2 4  est
: 4409 2 2  sau
: 4414 4 6 vage
: 4419 3 5 ~
: 4423 4 6 ~
- 4437
: 4448 2 2 Li
: 4452 2 2 bre
: 4457 2 2  con
: 4462 2 -1 ti
: 4467 3 -3 nent
: 4477 7 2  sans
: 4486 4 2  gril
: 4491 5 -3 lage
- 4506
: 4525 3 6 Beau
: 4531 4 6  comme
: 4536 4 6  on
: 4543 3 6  i
: 4549 5 6 ma
: 4559 9 7 gi
: 4570 4 6 ne
: 4580 2 4  pas
: 4583 1 2 ~
- 4596
: 4600 1 -3 I
: 4605 3 2 ci
: 4610 3 2  mę
: 4614 3 2 me
: 4619 3 2  nos
: 4625 3 -1  rę
: 4630 1 -3 ves
: 4634 2 2  sont
* 4640 5 2  é
: 4648 3 -1 troits
: 4652 1 -3 ~
- 4663
: 4683 2 6 C'est
: 4688 2 6  pour
* 4693 6 6  ça
: 4703 2 6  que
: 4708 3 2  j'i
: 4713 3 4 rai
: 4717 2 2  lŕ-
: 4723 10 6 bas
- 4743
: 4758 2 2 On
: 4761 2 2  ne
: 4766 5 2  m'a
: 4775 5 2  pas
: 4782 3 2  lais
: 4791 7 2 sé
: 4801 1 2  le
: 4806 3 -3  choix
- 4819
: 4841 2 6 Je
: 4845 1 6  me
: 4850 3 6  perds
: 4858 3 6  si
* 4865 7 9  je
: 4876 5 7  res
: 4885 5 6 te
: 4895 3 6  lŕ
: 4899 3 4 ~
: 4903 1 2 ~
- 4913
: 4917 2 2 C'est
: 4923 2 2  pour
: 4928 3 2  ça
: 4934 3 2  que
* 4941 3 2  j'i
* 4948 7 7 rai
* 4956 3 6 ~
* 4960 8 4 ~
- 4974
: 4976 3 1 Lŕ-
: 4982 7 2 bas
P 2
: 1722 2 1 N'y
: 1728 1 1  va
: 1734 6 2  pas
- 1764
: 1834 4 2 Y'a
: 1840 1 2  des
: 1845 3 4  tem
: 1852 3 4 pętes
: 1860 1 2  et
: 1865 6 4  des
* 1879 4 9  nau
: 1886 16 6 frages
- 1926
: 1993 2 -3 Le
: 1998 3 2  feu
: 2003 2 4  les
: 2008 9 4  dia
: 2020 2 4 bles
: 2023 3 2  et
: 2028 3 4  les
* 2034 4 6  mi
: 2041 13 2 rages
- 2078
: 2149 2 -3 Je
: 2154 2 2  te
: 2160 2 4  sais
: 2167 2 4  si
: 2173 3 2  fra
: 2181 2 4 gile
: 2193 2 6  par
: 2199 7 2 fois
- 2216
: 2239 13 14 Mmm
: 2253 8 13 ~
* 2262 6 11 ~
: 2275 4 9 ~
: 2281 9 2 ~
- 2300
: 2307 3 6 Reste
: 2312 1 6  au
: 2317 8 7  creux
: 2327 2 6  de
: 2332 5 4  moi
: 2338 2 2 ~
- 2344
: 2346 3 2 On
: 2350 2 2  a
* 2356 7 5  tant
: 2366 2 5  d'a
: 2372 8 5 mour
: 2381 3 5  ŕ
: 2390 9 5  faire
- 2409
: 2425 3 2 Tant
: 2429 2 2  de
: 2435 4 7  bo
: 2440 7 7 nheur
: 2450 6 7  ŕ
: 2460 5 7  ve
: 2470 2 5 nir
: 2473 4 4 ~
: 2478 5 2 ~
- 2493
: 2503 2 2 Je
: 2509 1 2  te
: 2513 7 6  veux
: 2523 8 9  ma
: 2534 4 9 ri
: 2539 3 9  et
* 2548 2 7  pč
* 2551 8 9 re
- 2569
: 2577 1 9 Et
: 2581 3 11  toi
: 2587 3 13  tu
* 2592 7 14  rę
* 2602 4 14 ves
: 2611 1 14  de
: 2616 5 14  par
: 2625 25 14 tir
- 2698
: 3100 4 -3 Lŕ-
: 3107 3 2 bas
- 3158
: 3208 5 -3 Loin
: 3216 2 2  de
: 3221 3 4  nos
: 3226 5 4  vies
: 3235 2 2  de
: 3240 5 4  nos
: 3256 3 9  vil
: 3261 5 6 la
: 3273 2 6 ges
- 3299
: 3366 2 -1 J'ou
: 3371 2 2 blie
: 3376 2 4 rai
: 3382 2 4  ta
: 3387 8 2  voix
: 3402 4 4  ton
: 3411 4 6  vi
: 3419 4 2 sa
: 3431 2 2 ge
- 3457
: 3523 3 -3 J'ai
: 3529 1 2  beau
: 3534 2 4  te
: 3540 3 4  ser
: 3544 3 2 rer
: 3550 8 4  dans
: 3564 1 6  mes
: 3569 8 2  bras
- 3587
: 3611 14 14 Mmm
: 3626 10 13 ~
: 3637 5 11 ~
: 3649 5 9 ~
: 3656 5 2 ~
- 3671
: 3681 2 6 Tu
: 3686 1 6  m'é
: 3692 5 7 chappes
: 3706 6 6  dé
: 3716 3 4 jŕ
: 3720 2 4 ~
: 3725 2 2  lŕ-
: 3731 3 2 bas
- 3758
: 3796 2 2 N'y
: 3800 2 2  vas
: 3805 4 2  pas
- 3833
: 3878 2 1 Lŕ-
: 3883 4 2 bas
- 3911
: 3952 1 2 N'y
: 3957 1 2  vas
: 3962 3 2  pas
- 3989
: 4032 1 1 N'y
: 4036 1 1  vas
: 4040 3 2  pas
- 4067
: 4109 2 2 N'y
: 4114 1 2  vas
: 4119 2 2  pas
- 4145
: 4192 2 -3 Lŕ-
: 4197 3 2 bas
- 4224
: 4269 2 2 n'y
: 4272 1 2  vas
: 4277 2 2  pas
- 4303
: 4351 1 2 Lŕ-
: 4354 4 2 bas
- 4382
: 4424 1 2 N'y
: 4428 1 2  vas
: 4434 2 2  pas
- 4460
: 4506 2 1 Lŕ-
: 4511 3 2 bas
- 4538
: 4582 1 1 N'y
: 4586 1 1  vas
: 4591 2 2  pas
- 4617
: 4664 1 1 Lŕ-
: 4669 4 2 bas
- 4697
: 4737 3 6 N'y
: 4742 2 6  vas
: 4748 2 2  pas
- 4774
: 4820 2 1 Lŕ-
: 4826 2 2 bas
- 4852
: 4900 1 2 n'y
: 4902 1 2  vas
: 4905 3 2  pas
- 4932
: 4993 1 2 N'y
: 4995 1 2  vas
: 5002 3 2  pas
- 5053
: 5129 2 4 N'y
: 5134 2 2  vas
: 5139 3 2  pas
- 5166
: 5212 1 2 N'y
: 5214 1 2  vas
: 5218 2 2  pas
- 5244
: 5290 1 2 N'y
: 5293 1 2  vas
: 5297 2 2  pas
- 5309
: 5330 1 -1 N'y
: 5332 1 -1  vas
: 5337 2 -1  pas
- 5347
: 5350 1 2 N'y
: 5352 1 2  vas
: 5356 1 2  pas
E
