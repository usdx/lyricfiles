#TITLE:Moonlit Masquerade
#ARTIST:Grailknights
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
#MP3:Grailknights - Moonlit Masquerade.mp3
#COVER:Grailknights - Moonlit Masquerade [CO].jpg
#BACKGROUND:Grailknights - Moonlit Masquerade [BG].jpg
#VIDEO:Grailknights - Moonlit Masquerade [VD#0].divx
#BPM:300
#GAP:7170
: 0 25 2 Ye
: 26 25 1 ~
: 52 7 -1 ~
: 60 5 -6 ~
: 67 4 -1 ~
: 73 4 1 ~
: 79 5 2 ~
: 85 2 1 ~
: 88 5 -1 ~ah!
- 95 96
: 106 26 2 Ye
: 134 25 1 ~
: 161 29 3 ~
: 194 5 4 ~
: 201 5 3 ~
: 207 3 1 ~
: 211 1 -1 ~ah!
- 213
: 215 23 2 Ye
: 240 23 1 ~
: 265 7 -1 ~
: 273 4 -6 ~
: 280 3 -1 ~
: 286 5 1 ~
: 294 5 2 ~
: 300 3 1 ~
: 304 5 -1 ~ah!
- 311 312
: 322 22 2 Ye
: 346 24 1 ~
: 372 34 3 ~ah!
- 408 466
: 486 21 2 Ye
: 508 19 1 ~
: 529 5 -1 ~
: 535 4 -6 ~
: 540 4 -1 ~
: 545 4 1 ~
: 550 5 2 ~
: 556 3 1 ~
: 560 4 -1 ~ah!
- 566
: 573 19 2 Ye
: 594 20 1 ~
* 615 21 3 ~ah!
- 638 639
: 659 21 2 Ye
: 681 21 1 ~
: 704 5 -1 ~
: 710 5 -6 ~
: 716 4 -1 ~
: 721 4 1 ~
: 726 4 2 ~
: 731 2 1 ~
: 734 4 -1 ~ah!
- 740
: 747 21 2 Ye
: 769 21 1 ~
* 791 26 3 ~ah!
- 819 1535
F 1555 4 0 A
F 1564 3 0  batt
F 1569 4 0 le,
F 1583 3 0  raged
F 1593 2 0  in
F 1598 2 0  a
F 1603 2 0  ga
F 1605 2 0 la
F 1608 2 0 xy
F 1612 3 0  far
F 1616 2 0  a
F 1620 5 0 way -
- 1627
F 1635 2 0 a
F 1639 7 0  long,
F 1657 4 0  long
F 1664 3 0  time
F 1671 2 0  a
F 1676 15 0 go.
- 1693 1694
F 1708 2 0 With
F 1712 3 0 out
F 1719 2 0  in
F 1725 3 0 ter
F 1731 2 0 ven
F 1735 2 0 tion,
- 1738
F 1740 3 0 no
F 1744 2 0  one'd
F 1748 5 0  live
F 1754 2 0  to
F 1757 5 0  see
F 1763 2 0  the
F 1767 5 0  end.
- 1773
F 1774 2 0 A
F 1777 2 0  thou
F 1781 3 0 sand
F 1785 5 0  dreams
F 1800 2 0  wiped
F 1804 3 0  out
F 1811 3 0  at
F 1817 2 0  one
F 1823 18 0  blow.
- 1843 1844
: 1859 13 4 Fly,
: 1878 5 -1  war
: 1887 5 4 rior
: 1896 3 7  seek
: 1902 3 6  the
: 1906 3 6  o
: 1911 2 2 pen
: 1915 4 -1  sky.
- 1921
: 1925 3 6 O
: 1930 2 7 ur
: 1935 3 6  trust,
: 1942 3 4  o
: 1947 2 2 ur
: 1952 3 2  fate,
: 1959 3 4  o
: 1965 2 6 ur
: 1970 3 4  life
: 1975 2 4  is
: 1980 2 4  at
: 1985 2 2  your
: 1989 4 -1  feet.
- 1995
: 2003 4 6 We're
: 2008 6 4  deaf,
: 2021 4 4  we're
: 2026 6 -1  blind,
- 2034
: 2036 2 7 we
: 2040 2 7  are
: 2045 2 6  seeds
: 2049 2 7  in
: 2054 2 6  rot
: 2058 2 2 ting
: 2062 3 -1  earth.
- 2067
: 2069 2 7 We
: 2072 3 7  can
: 2076 3 7 not
: 2081 8 6  live
: 2094 2 2  but
: 2100 4 2  di
: 2106 4 4 ~e
: 2113 3 6  for
: 2118 6 4  you.
- 2126 2144
F 2164 4 0 My
F 2174 4 0  whole
F 2182 6 0  life
F 2196 3 0  I
F 2201 3 0  wait
F 2205 2 0 ed
F 2209 4 0  for
F 2218 5 0  you.
- 2225 2226
F 2236 4 0 My
F 2246 4 0  whole
F 2255 4 0  life
F 2273 2 0  I
F 2278 2 0  would
F 2283 2 0  give
F 2288 2 0  to
F 2293 4 0  you.
- 2299
F 2305 3 0 I
F 2310 2 0  tra
F 2315 2 0 veled
F 2319 4 0  so
F 2329 4 0  long.
- 2335 2336
F 2344 4 0  I
F 2349 2 0  saw
F 2353 2 0  life
F 2357 2 0  come
F 2361 2 0  and
F 2365 5 0  go.
- 2372 2373
F 2381 2 0 I'm
F 2385 3 0  so
F 2394 2 0  ti
F 2400 7 0 red,
- 2409
F 2411 2 0 take
F 2416 2 0  your
F 2421 2 0  sword
F 2425 2 0  and
F 2430 2 0  lead
F 2435 2 0  this
F 2440 2 0  ar
F 2445 2 0 my
F 2450 2 0  home.
- 2453
F 2453 2 0 (Un)boun
F 2456 3 0 ded
F 2460 5 0  space
F 2465 2 0  so
F 2467 5 0  large
F 2473 4 0  and
F 2478 4 0  wide,
- 2482
F 2482 2 0 lu
F 2485 2 0 ring
F 2488 2 0  mists
F 2492 2 0  de
F 2497 5 0 vo
F 2502 3 0 ur
F 2506 8 0  me.
- 2516 2517
F 2525 4 0 Oh,
F 2534 2 0  join
F 2539 2 0  my
F 2543 3 0  moon
F 2549 2 0 lit
F 2553 2 0  mas
F 2558 2 0 que
F 2563 11 0 rade.
- 2576 2577
F 2594 2 0 I
F 2599 2 0  ride
F 2603 2 0  the
F 2606 2 0  wind,
F 2612 2 0  dis
F 2617 3 0 pel
F 2621 2 0  your
F 2625 3 0  fears,
- 2630
F 2633 2 0 I'm
F 2637 2 0  dy
F 2641 2 0 ing
F 2646 2 0  to
F 2650 2 0  be
F 2655 6 0  free.
- 2663 2664
F 2673 3 0 Oh,
F 2682 2 0  join
F 2687 2 0  my
F 2691 3 0  moon
F 2696 2 0 lit
F 2700 2 0  mas
F 2705 2 0 que
F 2710 14 0 rade.
- 2726 2768
F 2788 75 0 Yeah!
- 2865 3447
F 3467 4 0 That
F 3476 3 0  night
F 3491 3 0  I
F 3495 3 0  felt
F 3503 3 0  like
F 3509 2 0  I'd
F 3514 2 0  nev
F 3517 2 0 er
F 3520 3 0  felt
F 3526 2 0  be
F 3531 4 0 fore -
- 3537
F 3539 2 0 The
F 3543 2 0  re
F 3547 2 0 in
F 3552 3 0 force
F 3557 5 0 ments
F 3569 2 0  slow
F 3573 2 0 ly
F 3577 2 0  pass
F 3582 2 0 ing
F 3588 11 0  by.
- 3601 3602
F 3620 2 0 They
F 3624 4 0  came
F 3637 2 0  to
F 3642 3 0  lead
F 3651 5 0  o
F 3656 4 0 ur
F 3661 3 0  na
F 3667 4 0 tion
F 3674 2 0  back
F 3679 4 0  home.
- 3685
F 3693 2 0 In
F 3698 6 0  peace,
F 3712 2 0  to
F 3717 3 0  live
F 3724 3 0  and
F 3731 3 0  to
F 3736 17 0  die.
- 3755 3756
: 3772 10 4 Draw
: 3785 2 -1  the
: 3790 4 -1  cur
: 3795 3 -1 tains
: 3799 3 6  leave
: 3804 3 7  the
: 3808 4 6  the
: 3813 2 7 a
: 3816 3 6 tre
: 3821 2 2  of
: 3827 5 -1  war.
- 3834
: 3841 3 6 In
: 3845 3 6  so
: 3848 2 6 li
: 3851 3 6 tude
: 3855 4 6  on
: 3860 3 7 ly
: 3864 3 2  weak
: 3871 4 4  hearts
: 3878 3 6  re
: 3883 10 4 ma
: 3896 2 6 ~
: 3899 1 4 ~
: 3902 3 2 ~in.
- 3907
: 3911 2 6 In
: 3916 2 6  the
: 3920 6 4  end,
: 3938 5 -1  there's
: 3948 4 7  a
: 3957 3 6  whole
: 3961 2 7  new
: 3966 2 6  world
: 3971 2 2  to
: 3975 4 -1  find
- 3981
: 3989 4 6 and
: 3994 5 6  there
: 4001 3 4  we
: 4007 2 2  may
: 4012 4 2  me
: 4018 3 4 ~et
: 4026 2 6  a
: 4031 7 4 gain.
- 4040 4056
F 4076 4 0 My
F 4085 4 0  whole
F 4095 6 0  life
F 4108 2 0  I
F 4114 2 0  wait
F 4118 2 0 ed
F 4122 5 0  for
F 4131 6 0  you.
- 4139 4140
F 4150 4 0 My
F 4159 4 0  whole
F 4168 6 0  life
F 4187 2 0  I
F 4192 2 0  would
F 4196 2 0  give
F 4200 2 0  to
F 4205 4 0  you.
- 4211 4212
F 4220 2 0 I
F 4224 2 0  tra
F 4229 2 0 veled
F 4234 5 0  so
F 4243 5 0  long.
- 4250
F 4257 2 0 I
F 4262 2 0  saw
F 4266 2 0  life
F 4271 2 0  come
F 4275 2 0  and
F 4279 4 0  go.
- 4285
F 4293 2 0 I'm
F 4299 3 0  so
F 4307 2 0  ti
F 4312 4 0 red,
- 4318
F 4324 2 0 take
F 4329 3 0  your
F 4334 2 0  sword
F 4339 2 0  and
F 4344 2 0  lead
F 4349 2 0  this
F 4354 3 0  ar
F 4358 3 0 my
F 4362 1 0  home.
- 4363
F 4363 4 0 (Un)boun
F 4367 4 0 ded
F 4372 5 0  space
F 4378 2 0  so
F 4382 3 0  large
F 4386 3 0  and
F 4391 4 0  wide,
- 4396
F 4396 2 0 lu
F 4398 2 0 ring
F 4401 3 0  mists
F 4405 3 0  de
F 4410 3 0 vo
F 4415 2 0 ur
F 4420 6 0  me.
- 4428 4429
F 4437 5 0 Oh,
F 4446 3 0  join
F 4451 1 0  my
F 4456 3 0  moon
F 4460 3 0 lit
F 4465 3 0  mas
F 4470 2 0 que
F 4475 12 0 rade.
- 4489 4490
F 4506 2 0 I
F 4511 2 0  ride
F 4516 2 0  the
F 4519 3 0  wind,
F 4525 2 0  dis
F 4530 3 0 pel
F 4535 2 0  your
F 4538 4 0  fears.
- 4543
F 4545 2 0 I'm
F 4549 3 0  dy
F 4554 1 0 ing
F 4559 2 0  to
F 4563 2 0  be
F 4566 7 0  free.
- 4575 4576
F 4585 5 0 Oh,
F 4594 3 0  join
F 4599 2 0  my
F 4603 3 0  moon
F 4608 2 0 lit
F 4613 3 0  mas
F 4618 2 0 que
F 4623 18 0 rade.
- 4643 4644
: 4659 16 2 Ye
: 4678 15 1 ~
: 4697 4 -1 ~
: 4703 2 -6 ~
: 4707 2 -1 ~
: 4711 2 1 ~
: 4715 2 2 ~
: 4719 1 1 ~
: 4722 3 -1 ~ah!
- 4727
: 4734 14 2 Yeah!
- 4749
: 4751 3 1 Moon
: 4756 2 -1 lit
: 4760 2 -3  mas
: 4764 2 1 que
* 4768 12 -1 rade!
- 4782 4783
: 4806 18 2 Ye
* 4825 17 1 ~
: 4843 5 -1 ~
: 4849 3 -6 ~
: 4854 2 -1 ~
: 4858 2 1 ~
: 4862 4 2 ~
: 4867 1 1 ~
: 4869 3 -1 ~ah!
- 4874
: 4881 14 2 Yeah!
- 4897
* 4899 3 1 Moon
* 4904 2 -1 lit
* 4909 3 -3  mas
* 4915 3 1 que
* 4919 28 -1 rade!
- 4949 6115
* 6135 19 -1 Ye
: 6157 3 -6 ~
: 6162 3 -1 ~
: 6167 2 1 ~
* 6172 20 2 ~
: 6195 3 4 ~
: 6200 3 2 ~
: 6205 2 1 ~
* 6210 16 -3 ~
: 6233 3 -3 ~
: 6238 2 -1 ~
: 6242 2 1 ~
* 6247 20 2 ~
: 6269 3 4 ~
: 6274 3 2 ~
: 6279 3 1 ~
* 6284 19 -1 ~
: 6306 2 -5 ~
: 6311 3 -1 ~
: 6316 2 1 ~
* 6321 17 4 ~ah!
- 6340
: 6343 2 4 We
: 6347 3 4  will
: 6351 3 6  sur
* 6356 65 6 vive!
- 6423
F 6426 3 0 Un
F 6431 2 0 bound
F 6436 2 0 ed
F 6440 2 0  space
F 6445 2 0  so
F 6448 2 0  large
F 6454 3 0  and
F 6458 4 0  wide,
- 6462
F 6462 3 0 lur
F 6465 2 0 ing
F 6468 2 0  mists
F 6473 2 0  de
F 6477 2 0 vo
F 6481 2 0 ur
F 6486 6 0  me.
- 6494 6495
F 6504 6 0 Oh,
F 6513 3 0  join
F 6519 2 0  my
F 6522 4 0  moon
F 6528 3 0 lit
F 6532 3 0  mas
F 6537 2 0 que
F 6542 16 0 rade.
- 6560 6561
F 6573 3 0 I
F 6578 2 0  ride
F 6583 2 0  the
F 6587 3 0  wind,
F 6593 3 0  dis
F 6598 2 0 pel
F 6602 2 0  your
F 6605 3 0  fears.
- 6610
F 6612 2 0 I'm
F 6616 2 0  dy
F 6621 2 0 ing
F 6625 1 0  to
F 6629 2 0  be
F 6634 5 0  free.
- 6641 6642
F 6654 4 0 Oh,
F 6662 2 0  join
F 6667 2 0  my
F 6671 3 0  moon
F 6676 3 0 lit
F 6681 3 0  mas
F 6686 2 0 que
F 6690 21 0 rade.
- 6713 6714
: 6727 16 2 Ye
: 6746 15 1 ~
: 6764 3 -1 ~
: 6769 2 -6 ~
: 6774 2 -1 ~
: 6779 2 1 ~
: 6783 1 2 ~
: 6787 1 1 ~
: 6790 3 -1 ~ah!
- 6795
: 6800 15 2 Yeah!
- 6816
: 6818 2 1 Moon
: 6823 2 -1 lit
: 6828 2 -3  mas
: 6833 2 1 que
: 6837 13 -1 rade!
- 6852 6853
: 6874 16 2 Ye
: 6894 15 1 ~
: 6912 3 -1 ~
: 6918 2 -6 ~
: 6922 2 -1 ~
: 6927 2 1 ~
: 6931 2 2 ~
: 6935 1 1 ~
: 6938 3 -1 ~h!
- 6943
: 6948 16 2 Yeah!
- 6965
: 6966 2 1 Moon
: 6971 2 -1 lit
: 6976 2 -3  mas
: 6981 2 1 que
: 6984 12 -1 rade!
- 6998 6999
: 7021 16 2 Ye
: 7040 16 1 ~
: 7059 3 -1 ~
: 7064 2 -6 ~
: 7069 2 -1 ~
: 7073 2 1 ~
: 7077 1 2 ~
: 7081 2 1 ~
: 7084 3 -1 ~ah!
- 7089
: 7095 16 2 Yeah!
- 7113
* 7115 3 1 Moon
* 7120 2 -1 lit
* 7124 3 -3  mas
* 7129 3 1 que
* 7133 14 -1 rade!
E
