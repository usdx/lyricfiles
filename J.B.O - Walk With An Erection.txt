#TITLE:Walk With An Erection
#ARTIST:J.B.O
#MP3:J.B.O - Walk With An Erection.mp3
#VIDEO:J.B.O - Walk With An Erection.mp4
#COVER:J.B.O - Walk With An Erection.jpg
#BPM:223.96
#GAP:13712
#ENCODING:UTF8
: 0 4 2 Es
: 4 4 2  war
: 8 4 2  im
: 12 6 2  Ju
: 18 4 0 li,
- 23
: 24 4 17 Die
: 28 3 14  Son
: 31 3 14 ne
: 34 3 15  stach
: 38 2 10  und
: 40 4 10  es
: 44 2 10  hat
: 46 4 10 te
: 50 3 8  drei
: 53 3 8 ßig
: 56 3 10  Grad,
- 60
: 64 4 17 Ich
: 68 3 14  dach
: 72 3 14 te
: 75 2 14  mir,
* 82 6 12  oo
* 88 4 17 ee
* 92 3 14 oo,
- 95
: 96 3 14 Da
: 99 3 15  geh'
: 102 2 10  ich
: 104 4 10  ins
: 108 4 10  nächs
: 112 3 10 te
: 115 3 8  Frei
: 118 4 10 bad
- 125
: 128 4 5 Ich
: 132 4 2  geh'
: 136 4 2  zum
: 140 5 2  Ki
: 147 3 0 osk
- 150
: 152 4 17 Und
: 156 4 14  denk
: 160 2 14  mir
: 162 2 15  nix
: 166 2 10  da
: 168 3 10 bei,
: 173 3 10  kau
: 176 2 10 fe
: 178 3 8  mir
: 182 3 10  ein
: 185 2 10  Eis,
- 190
: 193 4 5 Doch
: 197 4 2  was
: 201 4 2  kommt
: 205 3 2  da,
* 211 5 0  oo
* 216 4 5 ee
* 220 3 2 oo,
- 223
: 224 3 14 A
: 227 3 15  9
: 230 2 10 er
: 232 3 10  Frau
: 236 2 10  und
: 238 4 10  mir
: 242 3 8  wird
: 245 3 8  ganz
: 248 4 10  heiß
- 253
: 256 2 3 Und
: 258 2 3  schon
: 260 3 3  kommt
: 264 3 3  die
: 268 2 3  Ba
: 270 2 3 de
: 272 2 3 ho
: 274 2 2 sen
: 276 4 3 beu
: 280 3 5 le,
- 283
* 284 9 3 Ee
* 293 8 1 oo
* 301 8 5 ee
* 309 11 1 oo
* 320 8 3 ee
* 328 4 -2 oo
* 332 8 1 ee
* 340 39 3 oo
- 381
: 384 5 10 Walk
: 389 6 10  with
: 395 2 5  an
: 397 4 8  E
: 401 8 10 rec
: 409 14 5 tion
- 467
: 511 5 2 Schnell
: 516 4 2  dreh
: 520 4 2  ich
: 524 5 2  mich
: 530 4 0  um,
- 534
: 536 4 17 Da
: 540 4 14 mit
: 544 2 14  sie
: 546 3 15  nicht
: 550 2 10  meine
: 552 4 10 ~
: 556 3 10  Beu
: 559 3 10 le
: 562 3 8  se
: 565 3 8 hen
: 568 3 10  kann,
- 571
: 576 2 2 Doch
: 578 3 2  was
: 581 3 2  macht
: 585 4 2  die
: 589 4 2  Frau,
* 595 5 0  oo
* 600 4 5 ee
* 604 3 2 oo,
- 607
: 608 3 14 Sie
: 611 4 15  lacht
: 615 2 10  mich
: 617 4 10  aus
: 621 3 10  und
: 624 2 10  ich
: 626 3 8  lauf
: 629 3 8  rot
: 632 3 10  an
- 637
: 640 4 5 Ich
: 645 3 2  kann
: 649 3 2  nichts
: 653 3 2  tun,
- 657
: 659 5 12 Schließ
: 664 5 17 lich
: 669 4 14  bin
: 673 2 14  ich
: 675 4 15  doch
: 679 2 10  nur
: 681 4 10  ein
: 685 4 10  ganz
: 689 2 10  nor
: 691 3 8 ma
: 694 2 8 ler
: 696 4 10  Mann
- 702
: 704 4 5 Die
: 708 4 2  weiß
: 712 4 2  ja
: 716 4 2  net,
: 723 5 0  oo
: 728 4 5 ee
: 732 3 2 oo,
- 735
: 736 1 0 Wie
: 739 1 0  schwer
: 743 1 0  des
: 745 1 0  is'
: 748 1 0  mit
: 751 1 0  vorn
: 755 1 0  so
: 757 1 0  was
: 760 3 0  dran
- 765
: 768 2 3 Denn
: 770 2 3  es
: 772 2 3  ist
: 777 2 3  gar
: 779 2 1  nicht
: 781 2 3  leicht
: 785 2 3  mit
: 787 2 1  der
: 789 3 3  Beu
: 792 3 5 le
- 796
* 799 6 3 Ee
* 805 8 1 oo
* 813 8 5 ee
* 821 11 1 oo
* 832 8 3 ee
* 840 5 -2 oo
* 845 7 1 ee
* 852 36 3 oo
- 892
: 896 5 10 Walk
: 901 6 10  with
: 907 2 5  an
: 909 4 8  E
: 913 7 10 rec
: 920 17 5 tion
- 1204
: 1472 5 5 Jetzt
: 1477 4 2  is'
: 1481 4 2  mir
: 1485 4 2  wurscht
- 1490
: 1491 5 12 Und
: 1496 4 17  es
: 1501 4 14  macht
: 1505 2 14  mir
: 1507 2 15  nix
: 1511 2 10  mehr
: 1513 4 10  aus,
: 1517 3 10  dass
: 1520 2 10  sie
: 1522 3 8  al
: 1525 2 8 le
: 1527 3 10  schau'n
- 1533
: 1536 4 2 Mit
: 1541 4 2  so
: 1545 4 2  ei'm
: 1549 6 2  Rohr,
* 1555 5 0  oo
* 1560 4 5 ee
* 1564 2 2 oo,
- 1566
: 1568 3 14 Da
: 1571 2 15  klappt
: 1575 2 10  das
: 1577 4 10  ver
: 1581 2 10 steck
: 1583 4 10 en
: 1587 3 8  wohl
: 1590 2 8  auch
: 1592 5 10  kaum.
- 1598
: 1600 4 2 Er
: 1604 5 2  wird
: 1609 4 2  nicht
: 1613 5 2  klei
: 1618 4 0 ner,
- 1622
: 1624 4 17 Ich
: 1628 5 14  weiß
: 1633 2 14  nicht,
: 1635 3 15  wie
: 1639 2 10  lang
: 1641 4 10  das
: 1645 4 10  noch
: 1649 2 10  so
: 1651 4 8  wei
: 1655 2 8 ter
: 1657 3 10  geht
- 1660
: 1665 4 5 Ich
: 1669 2 2  zeig
: 1671 2 2  ihn
: 1673 4 2  euch
: 1677 4 2  jetzt,
* 1683 4 0  oo
* 1688 4 5 ee
* 1693 2 2 oo,
- 1695
: 1696 3 14 So
: 1699 4 15  lang
: 1703 2 10  er
: 1705 3 10  noch
: 1708 6 10  so
: 1714 6 8  schön
: 1720 6 10  steht
- 1726
: 1728 4 5 Ich
: 1732 5 2  geh'
: 1737 4 2  zum
: 1741 5 2  Fünf-
: 1747 6 0 Me
: 1753 3 5 ter-
: 1756 3 2 Brett,
- 1759
: 1760 3 14 Da
: 1763 3 15  sieht's
: 1767 2 10  je
: 1769 4 10 der,
: 1773 3 10  und
: 1776 3 10  ich
: 1779 4 8  geh'
: 1783 2 8  hi
: 1785 5 10 nauf
- 1791
: 1793 3 2 Doch
: 1796 4 2  ich
: 1800 4 2  bleib
: 1804 2 2  hän
: 1806 4 2 gen,
* 1811 6 0  oo
* 1817 4 5 ee
* 1821 3 2 oo,
- 1824
: 1824 1 0 Und
: 1825 2 0  es
: 1827 2 0  reißt
: 1831 1 0  mir
: 1833 1 0  die
: 1836 1 0  Ba
: 1839 1 0 de
: 1842 1 0 hose
: 1844 1 0 ~
: 1847 2 0  auf,
- 1851
: 1855 2 3 Ich
: 1857 2 3  steh'im
: 1859 2 3 ~
: 1861 3 3  Frei'n
: 1865 2 3  und
: 1867 2 3  die
: 1869 2 3  Kin
: 1871 4 1 der
: 1875 4 3  schrei
: 1879 3 5 en
- 1882
* 1886 7 3 Ee
* 1893 8 1 oo
* 1901 8 5 ee
* 1909 11 1 oo
* 1920 9 3 ee
* 1929 4 -2 oo
* 1933 8 1 ee
* 1941 40 3 oo
- 1983
: 1985 4 10 Walk
: 1989 6 10  with
: 1995 2 5  an
: 1997 3 8  E
: 2001 6 10 rec
: 2010 17 5 tion
- 2038
: 2048 5 10 Walk
: 2053 5 10  with
: 2058 3 5  an
: 2061 4 8  E
: 2065 9 10 rec
: 2074 25 5 tion
- 2139
: 2175 1 0  
E