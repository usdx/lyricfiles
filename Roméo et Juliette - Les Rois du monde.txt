#TITLE:Les Rois du monde
#ARTIST:Roméo et Juliette
#MP3:Roméo et Juliette - Les Rois du monde.mp3
#VIDEO:Roméo et Juliette - Les Rois du monde.mp4
#COVER:Roméo et Juliette - Les Rois du monde.jpg
#BPM:345.6
#GAP:17767
#ENCODING:UTF8
#LANGUAGE:French
#GENRE:Comédie musicale
#YEAR:2000
#CREATOR:bibalice, mustangfred and PatTwo
: 0 5 7 Les
: 6 5 6  rois
: 12 1 7  du
: 18 3 7  mon
: 22 6 10 ~
: 34 1 10 de
- 45
: 49 4 5 Vivent
: 54 2 5  au
: 60 2 5  som
: 66 3 9 met
: 70 6 10 ~
- 86
: 95 6 8 Ils
: 102 4 7  ont
: 107 2 8  la
* 113 3 15  plus
* 117 5 17 ~
: 126 3 8  belle
: 133 8 8  vue,
: 144 5 7  mais
: 150 5 6  y'a
: 156 3 7  un
: 162 2 13  mais
: 165 9 15 ~
- 186
: 190 1 11 Ils
: 192 4 12 ~
: 197 3 12  ne
: 203 2 14  savent
: 209 2 15  pas
: 212 12 15 ~
: 229 1 15  ce
: 233 3 17  qu'on
: 239 4 10  pense
: 246 4 10  d'eux
: 251 6 15  en
* 258 14 15  bas
- 282
: 289 3 12 Ils
: 294 2 12  ne
: 299 3 14  savent
: 306 10 15  pas
- 321
: 323 4 17 Qu'i
: 330 2 15 ci
: 335 6 19  c'est
: 342 4 17  nous
: 347 5 15  les
* 353 3 15  rois
* 357 14 17 ~
- 380
: 384 5 7 Les
: 390 5 6  rois
: 396 5 7  du
: 402 3 7  monde
: 406 14 10 ~
- 424
: 426 1 8 Font
: 428 2 5 ~
: 433 1 5  tout
: 439 4 5  c'qu'ils
: 444 2 8  veu
: 447 7 10 ~
: 455 2 10 lent
- 467
: 479 5 8 Ils
: 486 4 7  ont
: 491 5 8  du
* 498 1 14  monde
* 500 8 17 ~
* 509 1 14 ~
: 511 2 8  au
: 517 4 8 tour
: 522 5 8  d'eux
: 528 4 7  mais
- 533
: 535 2 7 Ils
: 540 4 7  sont
* 547 2 14  seuls
* 550 14 15 ~
- 573
: 576 5 12 Dans
: 582 3 12  leurs
: 588 4 14  châ
: 594 1 14 teaux
: 596 16 15 ~
: 613 5 17  lŕ-
: 619 11 10 haut,
: 631 3 10  ils
: 638 1 14  s'en
* 640 2 15 ~
* 643 16 15 nuient
- 668
: 672 5 12 Pen
: 678 4 12 dant
: 684 5 14  qu'en
: 690 2 14  bas
: 693 14 15 ~
: 708 5 17  nous
: 714 3 15  on
: 719 2 17  danse
: 722 2 19 ~
: 726 4 17  toute
: 732 4 17  la
: 737 3 15  nuit
: 741 13 17 ~
- 764
: 768 5 6 Nous
: 774 3 -2  on
: 780 4 3  fait
: 785 6 6  l'a
: 792 6 6 mour
: 799 2 -4  on
: 805 4 6  vit
: 810 5 8  la
: 817 3 5  vie
- 821
: 823 2 -4 Jour
: 829 3 1  a
: 835 3 5 prčs
: 840 4 5  jour
: 846 4 5  nuit
: 852 4 10  a
: 858 5 5 prčs
: 865 3 6  nuit
- 869
: 871 3 -2 A
: 877 2 3  quoi
: 882 3 6  ça
: 888 5 6  sert
: 895 2 -4  d'ętre
: 901 3 6  sur
: 906 4 8  la
: 912 3 5  terre
- 917
: 919 1 -4 Si
: 924 2 1  c'est
: 930 3 5  pour
: 936 5 5  faire
: 943 3 5  nos
: 949 3 10  vies
: 954 3 5  ŕ
: 961 3 6  g'noux
- 965
: 967 1 -2 On
: 973 2 3  sait
: 978 2 6  qu'le
: 983 4 6  temps
: 991 1 -4  c'est
: 996 4 6  comme
: 1002 4 8  le
: 1008 4 5  vent
- 1013
: 1015 1 -4 De
: 1020 4 1  vi
: 1027 3 5 vre
: 1032 2 5  y'a
: 1038 4 5  qu'ça
: 1044 4 10  d'im
: 1050 4 5 por
: 1056 4 6 tant
- 1062
: 1064 1 -2 On
: 1069 2 3  s'fout
: 1074 3 6  pas
: 1080 5 6  mal
: 1087 2 -4  de
: 1092 4 6  la
: 1098 5 8  mo
: 1104 4 5 rale
- 1109
: 1111 1 -4 On
: 1116 3 1  sait
: 1121 4 5  bien
: 1127 4 5  qu'on
: 1134 2 5  fait
: 1139 3 10  pas
: 1145 4 5  d'mal
- 1150
: 1152 3 7 Les
: 1159 4 6  rois
: 1164 3 7  du
: 1169 13 10  mon
: 1183 1 10 de
- 1196
: 1200 1 5 Ont
: 1206 5 5  peur
: 1214 2 5  de
: 1219 4 10  tout
- 1233
: 1248 4 8 C'est
: 1255 3 7  qu'ils
: 1261 2 8  con
: 1267 1 16 fon
: 1269 7 17 ~
: 1282 2 17 dent
- 1288
: 1290 1 8 Les
: 1296 5 7  chiens
: 1302 5 6  et
: 1308 5 7  les
: 1314 5 15  loups
- 1329
: 1344 4 12 Ils
: 1351 5 12  font
: 1357 2 14  des
: 1363 5 14  pičges
: 1369 6 15 ~
: 1381 4 17  oů
: 1386 2 17  ils
: 1392 5 10  tom
: 1398 5 10 b'ront
: 1404 5 15  un
: 1410 8 15  jour
- 1428
: 1440 4 12 Ils
: 1446 2 12  se
: 1451 3 14  pro
: 1457 5 14 tčgent
: 1463 8 15 ~
: 1475 4 17  de
: 1481 5 15  tout
* 1487 8 19  męme
: 1497 2 17  de
: 1500 4 17  l'a
: 1505 6 15 mour
: 1512 6 17 ~
- 1530
: 1534 7 7 Les
: 1542 6 6  rois
: 1550 1 7  du
: 1554 4 9  monde
: 1559 6 10 ~
- 1575
: 1582 4 5 Se
: 1590 4 4  battent
: 1597 2 5  entre
: 1603 10 10  eux
- 1623
: 1632 4 8 C'est
: 1638 5 7  qu'y'a
: 1645 2 8  d'la
* 1649 5 15  place
* 1655 8 17 ~
- 1667
: 1669 2 8 Mais
: 1676 2 8  pour
: 1679 5 7  un
: 1685 5 6  pas
: 1693 3 7  pour
: 1697 12 15  deux
- 1719
: 1727 6 12 Et
: 1734 4 12  nous
: 1739 5 14  en
: 1745 17 15  bas
: 1763 5 17  leur
: 1770 5 17  guerre
: 1776 5 10  on
: 1782 2 10  la
: 1787 5 15  f'ra
: 1793 9 15  pas
- 1808
: 1810 3 15 On
: 1818 4 15  sait
: 1823 5 12  męme
: 1830 4 12  pas
: 1836 3 14  pour
: 1842 16 15 quoi
- 1860
: 1862 2 17 Tout
: 1866 4 15  ça
: 1873 4 19  c'est
: 1878 5 17  jeux
: 1884 5 15  de
: 1890 16 17  rois
- 1917
: 1921 4 6 Nous
: 1926 2 -2  on
: 1932 3 3  fait
: 1937 6 6  l'a
: 1944 5 6 mour
- 1950
: 1952 1 -4 On
: 1957 2 6  vit
: 1961 6 8  la
: 1969 3 5  vie
- 1973
: 1975 2 -4 Jour
: 1980 4 1  a
: 1986 4 5 prčs
: 1993 3 5  jour
: 1998 4 5  nuit
: 2004 5 10  a
: 2010 5 5 prčs
: 2017 3 6  nuit
- 2021
: 2023 2 -2 A
: 2029 2 3  Quoi
: 2034 3 6  ça
: 2040 5 6  sert
: 2047 2 -4  d'ętre
: 2053 3 6  sur
: 2058 3 8  la
: 2064 3 5  terre?
- 2069
: 2071 1 -4 Si
: 2076 3 1  c'est
: 2082 2 5  pour
: 2088 5 5  faire
: 2095 4 5  nos
: 2101 4 10  vies
: 2106 3 5  ŕ
: 2113 3 6  g'noux
- 2117
: 2119 1 -2 On
: 2123 4 3  sait
: 2131 2 6  qu'le
: 2136 3 6  temps
: 2143 1 -4  c'est
: 2148 4 6  comme
: 2153 5 8  le
: 2160 4 5  vent
- 2165
: 2167 1 -4 De
: 2172 4 1  vi
: 2178 5 5 vre
: 2184 2 5  y'a
: 2190 4 5  qu'ça
: 2196 4 10  d'im
: 2202 3 5 por
: 2208 3 6 tant
- 2212
: 2214 2 6 C'est
: 2220 6 6  comme
: 2228 2 6  le
* 2232 12 6  vent
- 2254
: 2263 1 5 C'est
: 2267 6 5  comme
: 2275 3 5  le
: 2280 24 5  vent
: 2305 1 5 ~
: 2307 50 10 ~
- 2403
: 2689 5 6 Nous
: 2695 2 -2  on
: 2699 4 3  fait
: 2705 5 6  l'a
: 2712 6 6 mour
: 2720 1 -4  on
: 2725 3 6  vit
: 2730 5 8  la
: 2737 3 5  vie
- 2741
: 2743 2 -4 Jour
: 2749 3 1  a
: 2755 3 5 prčs
: 2761 3 5  jour
: 2766 5 5  nuit
: 2772 4 10  a
: 2779 4 5 prčs
: 2785 3 6  nuit
- 2789
: 2791 2 -2 A
: 2796 2 3  quoi
: 2802 2 6  ça
: 2808 5 6  sert
: 2815 2 -4  d'ętre
: 2822 2 3  sur
: 2826 4 8  la
: 2832 3 5  terre
- 2837
: 2839 1 -4 Si
: 2844 2 1  c'est
: 2850 3 5  pour
: 2856 4 5  faire
: 2862 4 5  nos
: 2868 4 10  vies
: 2874 3 5  ŕ
: 2881 2 6  g'noux
- 2884
: 2886 3 6 A
: 2893 2 6  quoi
: 2899 2 6  ça
: 2905 18 6  sert
- 2931
: 2934 3 5 d'ętre
: 2940 4 5  sur
: 2946 3 5  la
: 2952 23 5  terre
: 2976 43 6 ~
- 3042
: 3078 3 6 On
: 3085 2 6  s'fout
: 3090 4 6  pas
: 3096 14 6  mal
- 3122
: 3126 5 5 de
: 3132 5 5  la
* 3139 4 5  mo
* 3144 13 5 rale
- 3167
: 3174 4 6 On
: 3181 3 6  sait
: 3186 5 6  bien
: 3193 3 6  qu'on
: 3198 4 6  fait
* 3204 3 8  pas
: 3210 23 8  d'mal
- 3238
: 3240 2 5 On
: 3246 2 5  fait
: 3251 2 5  pas
: 3254 1 6 ~
* 3258 9 6  d'mal
- 3268
: 3270 3 6 On
: 3277 2 6  sait
: 3281 4 6  qu'le
: 3288 14 6  temps
- 3314
: 3318 4 5 C'est
: 3324 4 5  comme
: 3330 5 5  le
: 3336 26 5  vent
- 3364
: 3366 4 6 On
: 3372 3 6  s'fout
: 3377 3 6  pas
: 3384 5 6  mal
: 3390 5 6  de
: 3396 4 6  la
: 3402 4 8  mo
: 3408 42 8 rale
: 3454 40 10 ~
- 3517
: 3559 3 -2 A
: 3565 2 3  quoi
: 3570 2 6  ça
: 3576 5 6  sert
: 3583 2 -4  d'ętre
: 3589 3 3  sur
: 3594 4 8  la
: 3601 2 5  terre
- 3605
: 3607 1 -4 Si
: 3613 2 1  c'est
: 3619 2 5  pour
: 3624 4 5  faire
: 3632 2 5  nos
: 3637 3 10  vies
: 3642 3 5  ŕ
: 3649 3 6  g'noux
- 3653
: 3655 1 -2 On
: 3661 2 3  sait
: 3666 2 6  qu'le
: 3672 3 6  temps
: 3679 1 -4  c'est
: 3684 4 6  comme
: 3690 4 8  le
: 3696 4 5  vent
- 3701
: 3703 1 -4 De
: 3709 3 1  vi
: 3714 3 5 vre
: 3719 3 5  y'a
: 3727 3 5  qu'ça
: 3732 4 10  d'im
: 3738 4 5 por
: 3744 4 6 tant
- 3750
: 3752 1 -2 On
: 3757 2 3  s'fout
: 3762 3 6  pas
: 3768 4 6  mal
: 3775 2 -4  de
: 3780 4 6  la
: 3786 5 8  mo
: 3792 4 5 rale
- 3797
: 3799 1 -4 On
: 3804 3 1  sait
: 3809 4 5  bien
: 3816 3 5  qu'on
: 3822 2 5  fait
: 3827 2 10  pas
: 3833 4 5  d'mal
- 3839
: 3841 5 6 Nous
: 3847 1 -2  on
: 3852 4 3  fait
: 3858 4 6  l'a
: 3865 4 6 mour
: 3872 1 -4  on
: 3877 2 6  vit
: 3882 4 8  la
: 3889 2 5  vie
- 3893
: 3895 1 -4 Jour
: 3901 2 1  a
: 3907 3 5 prčs
: 3913 3 5  jour
: 3918 5 5  nuit
: 3924 4 10  a
: 3931 4 5 prčs
: 3937 3 6  nuit
- 3941
: 3943 2 -2 A
: 3948 2 3  quoi
: 3954 2 6  ça
: 3960 5 6  sert
: 3967 2 -4  d'ętre
: 3973 2 3  sur
: 3978 4 8  la
: 3985 2 5  terre
- 3989
: 3991 1 -4 Si
: 3996 2 1  c'est
: 4002 3 5  pour
: 4008 4 5  faire
: 4014 4 5  nos
: 4020 4 10  vies
: 4026 3 5  ŕ
: 4033 2 6  g'noux
E