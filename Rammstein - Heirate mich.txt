#TITLE:Heirate mich
#ARTIST:Rammstein
#LANGUAGE:Deutsch
#EDITION:UltraStar Rammstein
#GENRE:Metal
#MP3:08-Heirate mich.mp3
#COVER:rammstein-herzeleid.jpg
#BACKGROUND:rammstein.jpg
#VIDEO:Rammstein - Heirate mich.avi
#RELATIVE:yes
#BPM:110
#GAP:5250
:0 2 56 Man 
:2 2 56 sieht 
:4 2 56 ihn 
:6 3 56 um 
:9 2 56 die 
:12 2 56 Kir
:14 2 54 che 
:17 4 55 schlei
:21 4 56 chen 
-30 47
:0 2 56 seit 
:2 2 56 ein
:4 2 56 em 
:6 4 56 Jahr  
:10 2 55 ist 
:15 3 54 er 
:18 2 53 al
:20 2 55 lein 
-26 45
:0 1 56 die 
:2 2 56 Trau
:4 1 56 er 
:6 2 56 nahm 
:9 2 56 ihm 
:13 2 60 al
:15 1 59 le 
:20 2 59 Sin
:22 2 58 ne 
-26 46
:0 2 56 schläft 
:3 2 56 je
:5 1 56 de 
:7 3 56 Nacht 
:11 2 54 bei 
:14 2 56 ih
:16 3 56 rem 
:23 7 58 Stei
:30 2 59 n
-35 443
:0 3 56 dort 
:4 2 56 bei 
:6 2 56 den 
:9 2 56 Glo
:11 2 55 cken 
:13 3 56 schläft 
:16 2 56 ein 
:18 4 58 Stei
:22 2 59 n
-25 25
:0 2 56 ich 
:2 2 56 al
:4 2 55 lein
:6 1 56 e 
:7 2 56 kann 
:10 2 57 ihn 
:12 2 58 lesen
-14 14
:1 2 56 und 
:3 2 56 auf 
:5 2 56 dem 
:7 2 56 Zaun 
:9 2 56 der 
:12 2 56 ro
:14 2 55 te 
:16 2 56 Hahn
-20 22
:0 1 56 ist 
:1 1 56 sei
:2 2 55 ner 
:4 2 56 Zeit 
:7 2 55 dein 
:9 2 56 Herz 
:12 2 55 ge
:14 2 56 wes
:16 2 55 en
-24 32
:0 3 56 Die 
:4 2 56 Furcht 
:6 2 55 auf 
:8 2 56 die
:10 2 55 sen 
:12 2 56 Zaun 
:15 2 55 ge
:17 2 56 spiesst
-20 21
:0 2 56 geh 
:2 2 56 ich 
:4 2 55 nun 
:6 2 56 gra
:8 2 55 ben 
:10 2 56 je
:12 2 55 de 
:14 2 56 Nacht
-17 18
:0 2 56 zu 
:2 2 56 se
:4 2 55 hen 
:6 2 56 was 
:9 2 55 noch 
:11 2 56 ü
:13 2 55 brig 
:15 2 56 ist 
:17 1 55 ~
:18 1 54 ~
:19 1 53 ~
-21 22
:0 1 56 von 
:1 2 56 dem 
:3 1 55 Ge
:4 2 56 sicht 
:6 2 55 das 
:9 2 60 mir 
:11 2 57 ge
:13 2 58 lacht
-16 18
:0 3 56 dort 
:3 2 56 bei 
:5 2 56 den 
:8 2 56 Glo
:12 2 55 cken 
:15 2 55 ver
:17 2 54 bring 
:21 2 54 ich 
:25 2 54 die 
:27 2 54 Nacht
-30 36
:0 3 56 dort 
:4 2 56 zwi
:6 3 56 schen 
:9 3 56 Schne
:12 2 55 cken 
:16 2 55 ein 
:19 3 56 ein
:22 3 55 sa
:25 2 56 mes 
:27 2 56 Tier
:29 1 55 ~
:30 1 54 ~
-34 37
:0 4 56 tags
:4 3 55 über 
:10 2 56 lauf 
:12 2 55 ich 
:17 2 55 der 
:19 2 56 Nacht 
:23 3 55 hin
:26 2 54 ter
:28 2 55 her
-34 40
:0 2 56 zum 
:2 2 56 zwei
:4 2 55 ten
:6 2 56 mal 
:16 3 55 ent
:19 1 55 -
:21 2 54 kommst 
:23 2 55 du 
:25 2 55 mir
-29 34
:0 2 56 Hei
:2 2 54 ra
:4 2 56 te 
:9 2 55 mich
-14 22
:0 2 62 Hei- 
:5 2 62 Hei- 
:10 2 62 Hei-
-15 18
:0 2 56 Hei
:2 2 54 ra
:4 2 56 te 
:9 2 55 mich
-13 19
:0 2 62 Hei- 
:5 2 62 Hei- 
:10 2 62 Hei-
-14 16
:0 2 56 Hei
:2 2 54 ra
:4 2 56 te 
:9 2 55 mich
-13 21
:0 2 62 Hei- 
:5 2 62 Hei- 
:10 2 62 Hei-
-14 17
:0 2 56 Hei
:2 2 54 ra
:4 2 56 te 
:9 2 55 mich
-13 20
:0 2 62 Hei- 
:5 2 62 Hei- 
:7 2 62 Hei-
-10 13
:0 2 56 Mit 
:4 2 56 mei
:6 2 55 nen 
:8 2 56 Hän
:10 2 55 den 
:12 2 56 grab 
:14 2 55 ich 
:16 2 56 tief
-19 20
:1 2 56 zu 
:3 2 56 fin
:5 2 55 den 
:7 2 56 was 
:9 2 55 ich 
:11 3 56 so 
:14 2 55 ver
:16 2 58 misst
-18 19
:0 2 56 und 
:3 2 56 als 
:5 2 55 der 
:7 2 56 Mond 
:9 2 55 im 
:12 2 55 schön
:14 2 55 sten 
:16 2 57 Kleid
:18 1 56 -
:19 1 55 -
-20 21
:0 2 57 hab 
:2 1 57 dei
:3 2 56 nen 
:5 2 57 kal
:7 2 56 ten 
:9 2 57 Mund 
:11 2 56 ge
:13 2 57 küsst
:15 1 56 -
:16 1 55 -
-22 32
:0 3 55 Ich 
:4 2 56 nehm 
:6 2 55 dich 
:8 2 56 zärt
:10 2 55 lich 
:12 2 56 in 
:14 2 55 den 
:16 3 56 Arm
-20 21
:0 2 55 doch 
:3 1 56 dei
:4 2 55 ne 
:6 2 56 Haut 
:9 3 55 reisst 
:12 2 56 wie 
:14 1 55 Pa
:15 2 56 pier
-18 19
:0 2 55 und 
:2 2 56 Tei
:4 2 55 le 
:6 2 55 fal
:8 1 55 len 
:10 2 56 von 
:12 2 55 dir 
:14 2 55 ab
-17 18
:0 2 55 zum 
:3 2 56 zwei
:5 2 56 ten
:7 2 56 mal 
:9 2 55 ent
:11 2 56 kommst 
:13 2 54 du 
:15 2 56 mir
-18 21
:0 3 56 dort 
:3 2 56 bei 
:5 2 56 den 
:8 2 56 Glo
:12 2 55 cken 
:15 2 55 ver
:17 2 54 bring 
:21 2 54 ich 
:25 2 54 die 
:27 2 54 Nacht
-30 36
:0 3 56 dort 
:4 2 56 zwi
:6 3 56 schen 
:9 3 56 Schne
:12 2 55 cken 
:16 2 55 ein 
:19 3 56 ein
:22 3 55 sa
:25 2 56 mes 
:29 2 56 Tier
:31 1 55 ~
:32 1 54 ~
-34 37
:0 4 56 tags
:4 3 55 über 
:10 2 56 lauf 
:12 2 55 ich 
:17 2 55 der 
:19 2 56 Nacht 
:23 3 55 hin
:26 2 54 ter
:28 2 55 her
-34 40
:0 2 56 zum 
:2 2 56 zwei
:4 2 55 ten
:6 2 56 mal 
:16 3 55 ent
:19 1 55 -
:21 2 54 kommst 
:23 2 55 du 
:25 2 55 mir
-29 34
:0 2 56 Hei
:2 2 54 ra
:4 2 56 te 
:9 2 55 mich
-14 22
:0 2 62 Hei- 
:5 2 62 Hei- 
:10 2 62 Hei-
-15 17
:0 2 56 Hei
:2 2 54 ra
:4 2 56 te 
:9 2 55 mich
-14 20
:0 2 62 Hei- 
:5 2 62 Hei- 
:10 2 62 Hei-
-14 16
:0 2 56 Hei
:2 2 54 ra
:4 2 56 te 
:9 2 55 mich
-13 21
:0 2 62 Hei- 
:5 2 62 Hei- 
:10 2 62 Hei-
-14 17
:0 2 56 Hei
:2 2 54 ra
:4 2 56 te 
:9 2 55 mich
-13 20
:0 2 62 Hei- 
:5 2 62 Hei- 
:7 2 62 Hei-
-15 165
:0 2 56 Hei
:2 2 54 ra
:4 2 56 te 
:9 2 55 mich
-14 22
:0 2 62 Hei- 
:5 2 62 Hei- 
:10 2 62 Hei-
-15 17
:0 2 56 Hei
:2 2 54 ra
:4 2 56 te 
:9 2 55 mich
-14 20
:0 2 62 Hei- 
:5 2 62 Hei- 
:10 2 62 Hei-
-14 16
:0 2 56 Hei
:2 2 54 ra
:4 2 56 te 
:9 2 55 mich
-13 21
:0 2 62 Hei- 
:5 2 62 Hei- 
:10 2 62 Hei-
-14 17
:0 2 56 Hei
:2 2 54 ra
:4 2 56 te 
:9 2 55 mich
-13 20
:0 2 62 Hei- 
:5 2 62 Hei- 
:7 2 62 Hei-
-10 12
:0 4 55 So 
:5 3 56 nehm 
:8 2 55 ich 
:10 5 56 was 
:16 3 55 noch 
:21 3 56 ü
:24 2 55 brig 
:28 3 55 ist
-33 37
:0 3 55 die 
:4 3 56 Nacht 
:8 3 55 ist 
:11 5 56 heiss
-16 17
:1 3 55 und 
:5 3 56 wir 
:15 2 56 sind
:17 1 55 -
:18 1 54 - 
:30 5 58 nackt
:35 1 57 -
:36 1 56 -
-40 45
:0 2 56 zum 
:2 2 56 Fluch 
:4 2 56 der 
:6 3 56 Hahn 
:11 2 56 den 
:15 3 56 Mor
:18 2 55 gen 
:22 2 55 grüsst
-25 30
:0 3 56 ich 
:3 3 56 hab 
:8 2 56 den 
:10 3 56 Kopf 
:18 2 55 ihm
-21 23
:0 4 56 ab
:8 4 58 ge
:18 6 65 hackt
:24 2 64 -
:26 2 63 -
:28 2 62 -
:30 2 61 -
:32 2 60 -
:34 2 59 -
:36 2 58 -
:38 2 57 -
:40 2 56 -
E
