#ARTIST:James Morrison
#TITLE:I Wont Let You Go
#MP3:James Morrison - I Wont Let You Go.mp3
#VIDEO:James Morrison - I Wont Let You Go.m3u
#COVER:James Morrison - I Wont Let You Go.jpg
#GENRE:Pop
#BPM:211.74
#GAP:8404
: 0 3 4 When 
: 4 3 4 it's 
: 8 18 4 black 
- 29
: 47 4 4 Take 
: 53 2 4 a 
: 56 3 4 lit
: 60 2 2 tle 
: 63 6 2 time 
: 72 2 0 to 
: 78 5 2 hold 
: 85 2 0 your
: 88 15 0 self 
- 105
: 112 3 4 Take 
: 116 2 4 a 
: 120 3 4 lit
: 124 2 2 tle 
: 127 6 2 time 
: 137 1 0 to 
: 139 8 2 feel 
: 149 2 0 a
: 153 6 -3 rou
: 161 7 0 nd 
- 172
: 179 3 4 Be
: 187 4 4 fore 
: 193 4 2 it's 
: 199 27 0 gone 
- 228
: 271 2 -5 You 
: 275 5 0 want 
: 282 6 4 let 
: 291 9 0 go 
- 304
: 304 2 4 But 
: 307 2 5 you 
: 309 9 4 still 
: 320 6 2 keep 
: 327 2 -3 on 
: 331 8 2 fal
: 340 4 4 ling 
: 346 3 2 do
: 351 5 0 wn
- 359
: 367 3 4 Re
: 371 3 4 mem
: 375 6 4 ber 
: 383 5 0 how 
: 390 2 -3 you 
: 394 7 2 saved 
: 403 3 4 me 
: 408 3 2 n
: 412 3 0 o
: 417 3 -3 w 
- 423
: 430 4 -3 From 
: 436 3 5 all 
: 441 3 4 of 
: 447 5 2 my 
* 454 44 0 wrongs 
* 502 19 4 yeah 
- 523
: 533 2 7 If 
: 536 2 7 there's 
: 539 3 7 love 
: 543 2 7 just 
: 546 4 7 feel 
: 551 5 7 it 
- 558
: 562 1 4 And 
: 564 2 7 if 
: 567 3 7 there's 
: 571 3 7 life 
: 575 2 7 we'll 
: 578 4 7 see 
: 583 4 7 it 
- 589
: 593 2 0 This 
: 596 1 7 is 
: 598 2 4 no 
: 601 5 7 time 
: 607 2 4 to 
: 610 2 7 be 
: 613 2 4 a
: 616 4 7 lo
: 622 4 9 ne 
: 629 2 0 a
: 634 4 7 lo
: 640 2 9 ne 
: 644 2 2 ye
: 648 2 0 ah 
- 651
* 653 10 7 I
* 665 8 5 ~
* 675 4 4 ~
* 681 4 2 ~
* 687 6 0 ~ 
: 695 3 2 won't 
: 699 3 2 let 
: 704 6 0 you 
: 712 17 0 go 
- 731
: 781 5 4 Say 
: 787 3 4 those 
: 791 14 4 wo
: 806 2 2 r
: 809 2 0 ds 
- 815
: 815 3 4 Say 
: 819 3 4 those 
: 823 9 4 words 
: 833 1 2 is 
: 835 2 2 like 
: 838 3 -3 there's 
: 844 4 2 no
: 850 3 4 thing 
: 855 2 0 el
: 859 8 -3 se 
- 869
* 878 5 9 Close 
: 884 2 7 your 
: 888 4 4 ey
: 893 2 2 e
: 896 2 0 s 
: 899 3 2 and 
: 903 1 -3 you 
: 906 6 2 might 
: 914 2 4 be
: 919 5 -3 lie
: 928 4 0 ve 
- 933
: 933 3 0 That 
: 938 4 5 there 
: 944 2 4 is 
: 950 6 2 some 
: 959 7 0 way 
: 968 25 0 out 
- 995
: 1006 3 -3 Oh 
* 1013 6 4 ye
* 1023 6 2 ah 
- 1031
* 1038 3 7 Op
* 1042 3 7 en 
: 1047 4 7 u
: 1052 2 5 
: 1055 4 4 
: 1060 2 2 
: 1063 2 0 p 
- 1067
* 1075 2 9 Op
* 1078 3 7 en 
: 1083 2 7 up 
: 1086 6 0 your 
* 1098 11 9 heart 
: 1111 3 7 to 
: 1118 5 5 me 
: 1126 5 4 now 
- 1133
: 1140 2 4 Let 
: 1142 2 4 it 
: 1147 2 4 a
: 1150 2 2 l
: 1153 2 0 l 
: 1158 3 -3 come 
: 1162 7 2 pou
: 1171 4 4 ring 
: 1176 4 2 o
: 1181 2 0 u
: 1184 2 -3 t 
- 1187
: 1189 3 0 There's 
* 1194 4 4 no
: 1199 2 2 thi
: 1202 2 0 ng 
: 1206 5 2 I 
: 1214 5 2 can't 
: 1221 17 0  t
: 1241 14 2 ak
: 1257 12 4 e 
: 1272 4 5 y
: 1277 2 4 e
: 1280 2 2 a
: 1284 2 0 h 
- 1288
: 1296 1 0 And 
: 1299 2 7  if 
: 1302 3 7 there's 
: 1306 3 7 love 
: 1310 2 7 just 
: 1314 4 7 feel 
: 1319 4 7 it 
- 1325
: 1328 1 0 And 
: 1331 2 7 if 
: 1334 2 7 there's 
: 1338 3 7 life 
: 1342 2 7 we'll 
: 1345 4 7 see 
: 1350 5 7 it 
- 1356
: 1358 2 0 This 
: 1361 2 7 is 
: 1364 2 4 no 
: 1367 5 7 time 
: 1373 2 4 to 
: 1377 2 7 be 
: 1380 2 4 a
: 1384 3 7 lo
: 1390 3 9 ne 
: 1396 2 4 a
: 1400 4 7 lo
: 1407 2 9 ne 
: 1411 2 2 ye
: 1414 2 0 ah 
- 1418
: 1422 8 7 I
: 1434 5 5 ~
: 1443 5 4 ~
: 1450 4 2 ~
: 1456 4 0 ~ 
: 1462 3 0 won't 
: 1466 3 2 let 
: 1471 6 0 you 
: 1479 28 0 go 
- 1510
* 1553 3 7 If 
* 1557 2 7 your 
* 1560 5 7 sky 
* 1566 3 7 is 
* 1570 3 7 fal
* 1574 2 7 li
* 1577 2 4 ng 
- 1580
: 1581 2 4 Just 
: 1584 5 7 take 
: 1590 2 7 my 
: 1593 4 7 hand 
: 1598 2 7 and 
: 1601 4 7 hold 
: 1606 2 7 i
: 1609 2 4 t 
- 1613
: 1616 3 7 You 
: 1620 2 4 don't 
: 1623 3 7 have 
: 1627 2 4 to 
: 1630 2 7 be 
: 1634 2 4 a
: 1638 4 7 lo
: 1644 4 9 ne 
: 1652 2 0 a
: 1656 3 7 lo
: 1662 3 9 ne 
: 1666 3 2 ye
: 1670 2 0 ah 
- 1676
: 1676 10 7 I
: 1688 9 5 ~
: 1699 6 4 ~
: 1707 4 2 ~
: 1713 4 0 ~ 
: 1719 2 0 won't 
: 1722 3 2 let 
: 1727 4 0 you 
: 1733 4 5 g
: 1739 5 4 
: 1746 2 2 
: 1750 6 0 
: 1758 2 2 
: 1761 2 4 o 
- 1765
: 1783 2 0 And 
: 1786 3 4 if 
: 1790 3 7 you 
* 1796 28 9 feel 
: 1826 2 7 the 
: 1830 10 7 fad
: 1841 3 7 ing 
: 1846 11 5 of 
: 1859 2 4 the 
: 1863 21 4 li
: 1886 4 2 g
: 1892 3 0 ht 
- 1897
: 1909 3 0 And 
: 1914 3 4 your 
: 1919 4 7 too 
* 1926 27 9 weak 
: 1957 2 9 to 
* 1962 5 11 car
* 1969 3 9 ry 
: 1975 7 7 on 
: 1985 2 4 the 
: 1989 22 4 fi
: 2013 4 2 g
: 2019 5 0 ht 
- 2026
: 2037 2 0 And 
: 2041 4 4 all 
: 2047 3 7 your 
* 2052 27 12 friends 
: 2082 2 9 that 
: 2086 4 11 you 
: 2093 5 9 can 
: 2100 6 4 all 
: 2109 5 4 ha
: 2115 2 2 v
: 2118 2 0 e 
: 2121 4 4 dis
: 2126 4 4 a
: 2132 15 9 pp
: 2150 2 5 ea
: 2153 4 4 r
: 2158 2 2 e
: 2162 4 0 d 
- 2168
: 2173 3 4 I'll 
: 2177 3 4 be 
: 2181 6 4 here 
: 2190 6 0 not 
: 2198 4 -5 go
: 2204 4 -8 ne 
- 2212
: 2212 3 4 For
: 2216 2 4 e
: 2219 4 2 v
: 2224 5 0 er 
: 2236 13 2 hold
: 2251 3 4 ing 
* 2256 4 2 o
* 2263 27 0 n 
* 2293 4 4 w
* 2299 4 4 o
* 2305 4 2 a
* 2311 2 0 h 
- 2315
: 2319 2 0 And 
: 2322 2 7 if 
: 2325 3 7 there's 
: 2329 3 7 love 
: 2333 2 7 just 
: 2337 3 7 feel 
: 2342 4 7 it 
- 2350
: 2350 2 0 And 
: 2353 3 7 if 
: 2357 2 7 there's 
: 2360 4 7 life 
: 2365 2 7 we'll 
: 2369 3 7 see 
: 2374 5 4 it 
- 2380
: 2381 2 0 This 
: 2384 2 7 is 
: 2387 1 4 no 
: 2390 5 7 time 
: 2397 2 4 to 
: 2401 2 7 be 
: 2404 1 4 a
: 2407 3 7 lo
: 2413 3 9 ne 
: 2419 1 4 a
: 2423 3 7 lo
: 2429 3 9 ne 
: 2434 2 2 ye
: 2437 2 0 ah 
- 2441
: 2441 11 7 I
: 2454 8 5 ~
: 2464 4 4 ~
: 2471 3 2 ~
: 2476 6 0 ~ 
: 2484 3 2 won't 
: 2488 3 2 let 
: 2493 6 0 you 
: 2501 3 5 g
: 2505 4 4 
: 2510 2 2 
: 2513 11 0 
: 2525 2 2 
: 2528 2 4 o 
- 2532
* 2577 2 7 If 
* 2580 2 7 your 
* 2583 4 7 sky 
* 2589 2 7 is 
* 2592 4 7 fal
* 2597 2 7 l
* 2600 2 4 ing 
- 2602
: 2602 2 4 Just 
* 2605 5 12 take 
: 2611 3 11 my 
: 2615 5 9 hand 
: 2621 1 9 and 
: 2624 6 7 hold 
: 2631 2 4 it 
- 2635
: 2640 2 7 You 
: 2643 2 4 don't 
: 2646 3 7 have 
: 2650 2 4 to 
: 2653 2 7 be 
: 2656 2 4 a
: 2660 3 7 lo
: 2665 3 9 ne 
: 2675 1 4 a
: 2678 3 7 lo
: 2683 3 9 ne 
: 2688 2 2 ye
: 2691 3 0 ah 
- 2695
: 2697 10 7 I
: 2709 9 5 ~
: 2720 5 4 ~
: 2727 4 2 ~
: 2733 4 0 ~ 
: 2740 3 2 won't 
: 2744 3 2 let 
: 2749 5 0 you 
: 2756 26 0 go 
- 2784
* 2815 2 4 Ye
* 2819 2 2 a
* 2823 2 0 h 
- 2826
: 2827 3 0 I 
: 2832 6 12 won't 
: 2840 4 12 let 
: 2848 3 2 you 
: 2852 2 2 g
: 2855 4 4 o 
: 2860 4 0 no 
: 2865 3 0  I 
: 2869 8 12 won't 
: 2879 5 7 let 
- 2886
: 2892 3 0 I 
: 2897 5 12 won't 
: 2904 4 12 let 
: 2911 4 2 you 
: 2916 4 2 g
: 2921 2 4 o 
: 2924 2 0 no 
: 2928 3 0 I 
: 2932 5 14 won't 
: 2938 8 12 let 
- 2948
: 2954 4 0 I 
: 2960 7 12 won't 
: 2969 4 12 let 
: 2975 4 2 you 
: 2981 2 2 g
: 2984 4 4 o 
: 2990 2 0 no 
: 2997 6 5 won't 
: 3005 3 4 let 
: 3009 2 0 you 
: 3013 3 2 g
: 3019 57 0 o 

