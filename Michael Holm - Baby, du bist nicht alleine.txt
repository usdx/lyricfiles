#TITLE:Baby, du bist nicht alleine
#ARTIST:Michael Holm
#MP3:Michael Holm - Baby, du bist nicht alleine.mp3
#COVER:Michael Holm - Baby, du bist nicht alleine [CO].jpg
#BACKGROUND:Michael Holm - Baby, du bist nicht alleine [BG].jpg
#BPM:72,5
#GAP:500
#ENCODING:UTF8
: 1 2 57 Ich 
: 3 2 57 hab' 
: 5 2 57 Dich 
: 7 5 57 nie 
: 12 2 57 vor
: 14 2 59 her 
: 16 2 61 ge
: 18 6 52 seh'n.
- 29
: 34 2 57 Dar
: 36 2 57 um 
: 38 2 57 kann 
: 40 2 57 ich 
: 42 2 57 es 
: 44 5 59 kaum 
: 49 1 62 ver
: 50 7 54 steh'n?
- 62
: 67 2 54 In 
: 69 2 59 mei
: 71 1 59 nem 
: 72 5 59 Herz 
: 77 3 59 warst 
: 80 1 61 im
: 81 3 62 mer 
: 84 6 54 Du 
- 96
: 101 2 56 Am 
: 103 1 56 U
: 104 2 56 fer 
: 106 4 56 der 
: 111 2 56 Er
: 113 1 57 in
: 114 3 59 ne
: 117 6 52 rung.
- 128
: 135 2 57 Wenn 
: 137 1 57 man 
: 138 2 57 sich 
: 140 4 57 trifft 
: 144 2 57 und 
: 146 1 59 hat 
: 147 3 61 sich 
: 150 6 52 lieb
- 161
: 167 2 57 Wenn 
: 169 1 57 man 
: 170 1 57 sich 
: 171 5 57 mehr 
: 176 2 57 als 
: 178 2 59 Küs
: 180 2 61 se 
: 182 7 54 gibt
- 192
: 197 3 59 Dann 
: 200 2 59 muß 
: 202 2 59 man 
: 204 2 59 sa
: 206 3 59 gen 
: 209 2 61 was 
: 211 3 62 man 
: 214 7 54 fühlt
- 225
: 230 1 52 Da
: 231 1 56 mit 
: 232 1 56 die 
: 233 2 56 Flam
: 235 2 57 men 
: 237 3 59 nicht 
: 240 4 61 ver
: 244 9 57 glüh'n.
- 255
: 258 2 61 Ba
: 260 3 57 by 
: 268 2 61 Du 
: 270 1 61 bist 
: 271 1 59 nicht 
: 272 1 57 al
: 273 3 54 lei
: 276 3 59 ne
- 282
: 284 2 61 Mit 
: 286 1 61 all 
: 287 1 59 Dei
: 288 1 57 nen 
: 289 3 54 Träu
: 292 4 59 men
- 298
: 300 1 61 Mit 
: 301 1 61 all 
: 302 1 59 Dei
: 303 1 57 ner 
: 304 3 61 Lie
: 307 2 57 be.
- 314
: 320 2 59 Ba
: 322 3 57 by 
: 330 2 61 Du 
: 332 1 61 bist 
: 333 1 59 nicht 
: 334 1 57 al
: 335 2 54 lei
: 337 4 59 ne.
- 344
: 346 1 61 Du 
: 347 1 61 darfst 
: 348 1 59 nicht 
: 349 2 57 mehr 
: 351 2 54 wei
: 353 4 57 nen
- 360
: 363 2 56 Denn 
: 365 1 57 bei 
: 366 4 59 mir 
: 370 2 59 bist 
: 372 2 61 Du 
: 374 2 57 zu 
: 376 7 57 Haus'.
- 388
: 395 2 52 Viel
: 397 2 57 leicht 
: 399 2 57 war 
: 401 3 57 da 
: 404 2 57 ein 
: 406 1 59 and
: 407 3 61 'rer 
: 410 7 52 Mann
- 420
: 425 2 57 Der 
: 427 3 57 Dir 
: 430 2 57 vor 
: 432 2 57 Jah
: 434 2 57 ren 
: 436 4 59 weh 
: 440 1 61 ge
: 441 7 54 tan.
- 452
: 457 2 59 Daß 
: 459 1 59 Du 
: 460 1 59 den 
: 461 4 59 Mund 
: 465 1 59 da
: 466 3 61 vor 
: 469 1 62 ver
: 470 8 54 schließt
- 482
: 490 2 52 Was 
: 492 1 52 man 
: 493 1 52 in 
: 494 2 64 Dei
: 496 1 61 nen 
: 497 2 59 Au
: 499 4 57 gen 
: 503 9 59 liest.
- 514
: 517 2 66 Ba
: 519 4 64 by 
: 526 2 61 Du 
: 528 2 61 bist 
: 530 1 59 nicht 
: 531 1 57 al
: 532 2 59 lei
: 534 4 54 ne 
- 540
: 543 2 61 Mit 
: 545 1 61 all 
: 546 1 59 Dei
: 547 1 57 nen 
: 548 2 54 Träu
: 550 3 59 men
- 555
: 558 2 61 Mit 
: 560 1 61 all 
: 561 1 59 Dei
: 562 1 57 ner 
: 563 2 61 Lie
: 565 3 57 be 
- 572
: 579 2 64 Ba
: 581 3 61 by 
: 589 2 61 Du 
: 591 1 61 bist 
: 592 1 59 nicht 
: 593 1 57 al
: 594 2 54 lei
: 596 3 59 ne.
- 602
: 605 1 61 Du 
: 606 1 61 darfst 
: 607 1 59 nicht 
: 608 2 57 mehr 
: 610 2 54 wei
: 612 3 59 nen
- 618
: 621 2 56 Denn 
: 623 2 57 bei 
: 625 3 59 mir 
: 628 2 59 bist 
: 630 2 61 Du 
: 632 2 57 zu 
: 634 5 57 Haus'.
- 700
: 707 2 57 Wenn 
: 709 2 57 man 
: 711 2 57 sich 
: 713 4 57 trifft 
: 720 1 57 und 
: 721 1 59 hat 
: 722 3 61 sich 
: 725 8 52 lieb
- 735
: 739 2 57 Wenn 
: 741 2 57 man 
: 743 2 57 sich 
: 745 3 57 mehr 
: 748 2 57 als 
: 750 2 59 Küs
: 752 3 61 se 
: 755 7 54 gibt
- 766
: 771 1 59 Dann 
: 772 2 59 muß 
: 774 2 59 man 
: 776 3 59 sa
: 779 2 59 gen 
: 781 2 61 was 
: 783 3 62 man 
: 786 10 54 fühlt
- 798
: 801 2 52 Da
: 803 2 56 mit 
: 805 2 56 die 
: 807 3 56 Flam
: 810 1 57 men 
: 811 3 59 nicht 
: 814 5 61 ver
: 819 7 57 glüh'n
- 828
: 831 2 59 Ba
: 833 3 57 by 
: 841 2 61 Du 
: 843 1 61 bist 
: 844 1 59 nicht 
: 845 2 57 al
: 847 2 54 lei
: 849 3 59 ne
- 855
: 858 1 61 Mit 
: 859 1 61 all 
: 860 1 59 Dei
: 861 1 57 nen 
: 862 3 54 Träu
: 865 3 59 men
- 870
: 873 1 52 Mit 
: 874 1 64 all 
: 875 1 64 Dei
: 876 1 64 ner 
: 877 3 61 Lie
: 880 2 57 be
- 886
: 893 2 59 Ba
: 895 3 57 by 
: 903 2 61 Du 
: 905 1 61 bist 
: 906 1 59 nicht 
: 907 1 57 al
: 908 2 54 lei
: 910 3 59 ne.
- 915
: 919 1 61 Du 
: 920 2 61 darfst 
: 922 1 59 nicht 
: 923 1 57 mehr 
: 924 2 54 wei
: 926 4 57 nen
- 933
: 936 2 56 Denn 
: 938 2 57 bei 
: 940 2 59 mir 
: 942 2 59 bist 
: 944 2 61 Du 
: 946 2 57 zu 
: 948 3 57 Haus'.
- 953
: 955 2 59 Ba
: 957 3 57 by 
: 965 2 61 Du 
: 967 1 61 bist 
: 968 1 59 nicht 
: 969 1 57 al
: 970 2 54 lei
: 972 3 57 ne
- 978
: 981 2 61 Mit 
: 983 1 61 all 
: 984 1 59 Dei
: 985 1 57 nen 
: 986 2 54 Träu
: 988 4 57 men
- 994
: 997 1 52 Mit 
: 998 1 61 all 
: 999 1 59 Dei
: 1000 1 57 ner 
: 1001 3 61 Lie
: 1004 2 57 be 
- 1010
: 1017 2 64 Ba
: 1019 3 61 by 
: 1027 2 61 Du 
: 1029 1 61 bist 
: 1030 1 59 nicht 
: 1031 1 57 al
: 1032 3 54 lei
: 1035 5 59 ne.
- 1050
: 1060 2 57 Denn 
: 1062 2 57 bei 
: 1064 3 59 mir 
: 1067 1 52 bist 
: 1068 2 61 Du 
: 1070 1 59 zu 
: 1071 4 57 Haus'.
E