#TITLE:Never let me down again
#ARTIST:Depeche Mode
#MP3:Depeche Mode - Never let me down again.mp3
#VIDEO:Depeche Mode - Never let me down again.avi
#COVER:Depeche Mode - Never let me down again [CO].jpg
#BACKGROUND:Depeche Mode - Never let me down again [BG].jpg
#BPM:317,58
#GAP:33446
#ENCODING:UTF8
#EDITION:UltraStar Depeche Mode
: 0 12 -3 I'm 
: 22 5 -3 ta
: 29 5 -3 king 
: 36 5 -4 a 
: 42 10 -3 ride 
- 54
: 59 4 -3 With 
: 65 5 -5 my 
: 72 8 -3 best 
: 83 14 -3 friend 
- 99
: 192 6 -3 I 
: 199 6 -4 - 
: 209 12 -3 hope 
: 227 6 -3 he 
: 234 8 -3 ne
: 245 6 -4 ver 
- 252
: 252 5 -3 Lets 
: 258 3 -4 me 
: 263 5 -3 down 
: 270 5 -4 a
: 276 13 -3 gain 
- 291
: 383 5 -3 He 
* 389 10 -3 knows 
: 402 9 -3 where 
: 414 9 -3 he's 
: 426 10 -3 ta
: 437 9 -3 king 
: 449 16 -3 me, 
- 467
: 479 7 -3 Ta
: 487 6 -3 king 
: 497 10 -3 me 
: 509 5 -3 where 
: 516 5 -3 I 
: 522 3 -4 - 
: 527 6 -3 want 
: 534 9 -3 to 
: 546 13 -3 be 
- 561
* 577 13 -3 I'm 
: 599 6 -3 ta
: 606 5 -3 king 
: 613 3 -4 a 
: 618 12 -3 ride 
- 632
: 636 4 -3 With 
: 642 4 -5 my 
: 648 8 -3 best 
: 659 12 -3 friend 
- 867
* 960 13 12 We're 
: 982 8 12 fly
: 991 6 12 ing 
: 1000 13 10 high, 
- 1015
: 1026 3 10 We're 
: 1030 5 10 wat
: 1036 6 10 ching 
: 1044 3 10 the 
: 1049 16 10 world 
- 1067
: 1079 6 10 Pass 
: 1087 8 10 us 
: 1097 20 12 by. 
- 1119
: 1163 5 12 Ne
: 1169 4 10 ver 
: 1175 5 12 want 
: 1181 5 10 to 
: 1187 5 12 come 
: 1193 11 10 down, 
- 1206
: 1211 5 10 Ne
: 1217 4 9 ver 
: 1222 5 10 want 
: 1228 4 9 to 
: 1235 5 10 put 
: 1241 4 9 my 
: 1246 9 10 feet 
: 1259 6 10 back 
- 1267
: 1271 6 10 Down 
: 1278 4 10 on 
: 1283 4 10 the 
: 1289 11 12 ground. 
- 1302
: 1536 13 9 I'm 
: 1558 6 9 ta
: 1565 4 9 king 
: 1572 4 8 a 
: 1577 12 9 ride 
- 1591
: 1595 4 9 With 
: 1600 5 7 my 
: 1607 8 9 best 
: 1619 7 9 friend. 
- 1628
: 1728 8 9 I 
: 1737 6 8 - 
: 1745 13 9 hope 
: 1764 3 9 he 
* 1769 10 9 ne
: 1781 4 8 ver 
- 1787
: 1787 4 9 Lets 
: 1793 5 8 me 
: 1799 6 9 down 
: 1806 4 8 a
: 1811 14 9 gain. 
- 1827
: 1920 4 9 Pro
: 1925 4 9 mi
: 1930 5 8 ses 
: 1937 6 9 me 
- 1945
: 1950 8 9 I'm 
: 1962 4 8 as 
: 1967 11 9 safe 
: 1980 4 9 as 
: 1986 10 9 hou
: 1997 8 8 ses, 
- 2007
: 2009 4 9 As 
: 2016 5 9 long 
: 2022 4 9 as 
: 2027 4 9 I 
: 2032 6 9 re
: 2039 5 9 mem
: 2045 5 8 ber 
- 2051
: 2051 7 9 Who's 
: 2063 6 9 wea
: 2070 4 9 ring 
: 2076 3 9 the 
* 2080 11 8 trou
: 2092 9 9 sers. 
- 2103
: 2112 12 9 I 
: 2135 10 9 hope 
: 2147 5 9 he 
: 2153 11 9 ne
: 2165 5 8 ver 
- 2170
: 2171 5 9 Lets 
: 2178 4 8 me 
: 2183 5 9 down 
: 2190 5 7 a
: 2196 13 9 gain. 
- 2211
: 2494 15 12 We're 
: 2517 8 12 fly
: 2526 7 12 ing 
: 2536 17 10 high, 
- 2555
: 2560 5 10 We're 
: 2567 5 10 wat
: 2573 4 10 ching 
: 2579 4 10 the 
: 2585 13 10 world 
- 2600
: 2614 7 10 Pass 
: 2622 9 10 us 
* 2633 13 12 by. 
- 2648
: 2698 5 12 Ne
: 2704 4 10 ver 
: 2710 5 12 want 
: 2716 4 10 to 
: 2722 5 12 come 
: 2728 13 10 down, 
- 2743
: 2747 5 10 Ne
: 2753 4 9 ver 
: 2758 5 10 want 
: 2764 4 9 to 
: 2770 5 10 put 
: 2776 4 9 my 
* 2782 8 10 feet 
: 2794 7 10 back 
- 2803
: 2806 6 10 Down 
: 2813 4 10 on 
: 2818 5 10 the 
: 2824 15 11 ground. 
- 2841
: 2877 14 12 We're 
: 2902 6 12 fly
: 2909 8 12 ing 
: 2921 13 10 high, 
- 2936
: 2945 4 10 We're 
: 2951 5 10 wat
: 2957 5 10 ching 
: 2963 4 10 the 
: 2969 11 10 world 
- 2982
: 2999 5 10 Pass 
: 3006 9 10 us 
: 3017 12 12 by. 
- 3031
: 3083 5 12 Ne
: 3089 3 10 ver 
: 3094 5 12 want 
: 3100 5 10 to 
: 3106 6 12 come 
: 3113 10 10 down, 
- 3125
: 3132 4 10 Ne
: 3137 3 9 ver 
: 3143 4 10 want 
: 3148 6 9 to 
: 3155 4 10 put 
: 3160 5 9 my 
: 3166 10 10 feet 
: 3179 8 10 back 
- 3189
: 3191 5 10 Down 
: 3197 4 10 on 
: 3203 4 10 the 
* 3209 14 11 ground. 
- 3225
: 3658 7 9 Ne
: 3666 4 8 ver 
: 3671 9 9 let 
: 3684 5 9 me 
: 3690 27 9 down, 
- 3719
: 3755 6 9 Ne
: 3762 4 8 ver 
: 3768 9 9 let 
: 3780 4 9 me 
* 3785 24 9 down. 
- 3811
: 3851 5 9 Ne
: 3857 3 8 ver 
: 3863 10 9 let 
: 3876 4 9 me 
: 3881 22 9 down, 
- 3905
: 3945 7 9 Ne
: 3953 4 8 ver 
: 3959 9 9 let 
: 3971 4 9 me 
: 3976 14 9 down. 
- 3992
: 4032 9 14 See 
: 4043 2 14 the 
* 4048 17 14 stars, 
- 4067
: 4067 5 12 They're 
: 4073 16 14 shi
: 4091 6 12 ning 
: 4098 13 14 bright, 
- 4113
: 4128 8 14 E
: 4137 7 10 very 
: 4144 5 9 - 
: 4150 11 9 thing's 
: 4162 6 9 al
: 4170 13 10 right 
: 4187 5 9 to
: 4193 23 5 night. 
- 4218
* 4224 9 14 See 
: 4235 4 14 the 
: 4241 15 14 stars, 
- 4258
: 4259 5 12 They're 
: 4265 18 14 shi
: 4284 4 12 ning 
: 4289 18 14 bright, 
- 4309
: 4320 8 14 E
: 4329 4 10 very 
: 4334 4 9 - 
: 4340 15 9 thing's 
: 4356 5 9 al
: 4362 17 10 right 
: 4380 5 9 to
: 4387 18 5 night. 
- 4407
: 4417 10 14 See 
: 4429 3 14 the 
: 4434 10 14 stars, 
- 4446
: 4452 3 12 They're 
: 4457 19 14 shi
: 4477 5 12 ning 
: 4483 13 14 bright, 
- 4498
: 4513 11 14 E
: 4524 5 10 very 
: 4530 5 9 - 
: 4537 11 9 thing's 
: 4549 5 9 al
: 4555 17 10 right 
: 4573 5 9 to
: 4579 19 5 night. 
E