#TITLE:Unter dem Meer
#ARTIST:Disney's Arielle
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
#MP3:Disney's Arielle - Unter dem Meer.mp3
#COVER:Disney's Arielle - Unter dem Meer [CO].jpg
#BACKGROUND:Disney's Arielle - Unter dem Meer [BG].jpg
#VIDEO:Disney's Arielle - Unter dem Meer [VD#0].mpg
#VIDEOGAP:0
#BPM:198,6
#GAP:10270
: 0 2 5 Der
: 3 3 10  See
: 7 2 10 tang
: 11 2 10  blüht
: 14 2 9  i
: 17 2 12 mmer
: 20 3 10  grü
: 25 2 5 ner,
- 29
: 33 1 2 wenn
: 35 2 5  er
: 39 2 5  dich
: 42 2 2  von
: 45 2 0  fern
: 48 2 5  er
: 52 4 2 freut,
- 58
: 64 2 5 des
: 67 2 10 halb
: 71 2 10  willst
: 74 1 10  du
: 76 2 9  zu
: 80 2 12  den
: 84 3 10  Men
: 89 2 5 schen,
- 93
: 96 2 2 doch
: 99 2 5  das
: 103 2 5  hast
: 107 1 2  du
: 109 2 0  schnell
: 113 2 5  be
: 116 5 2 reut.
- 123
: 129 1 7 Schau
: 131 3 10  dei
: 135 1 10 ne
: 137 2 10  Welt
: 141 2 5  doch
: 145 2 10  ge
: 148 2 5 nau
: 152 2 10  an,
- 156
: 160 2 12 ja
: 163 2 12  hier,
: 166 2 12  wo
: 169 2 10  du
: 172 2 14  schwimmst
: 176 2 12  und
: 180 3 10  lebst!
- 185
: 191 2 13 Voll
* 194 3 14  Wun
: 198 1 10 der
: 200 2 7  siehst
: 204 2 5  du
: 208 2 10  das
: 213 3 5  Blau
: 217 2 10  dann.
- 221
: 225 1 12 Sag
: 227 2 12  selbst,
: 231 2 12  was
: 235 1 10  du
: 237 2 14  noch
: 240 2 12  er
: 245 3 10 strebst
- 250 251
: 259 3 14 un
: 264 1 12 ter
: 266 2 10  dem
: 270 6 7  Meer,
- 278 279
: 291 2 14 un
: 295 2 12 ter
: 298 2 10  dem
: 302 5 12  Meer!
- 309 310
: 322 3 14 Wo
: 326 2 12  wär
: 329 2 10  das
: 332 2 7  Wa
: 336 2 3 sser
- 339
: 339 2 14 be
: 343 1 12 sser
: 345 2 10  und
: 349 2 7  na
: 353 2 5 sser
- 356
: 356 2 14 als
: 360 1 12  es
: 362 2 10  hier
* 366 9 14  wär?
- 377 378
: 387 2 14 Die
: 390 3 12  dro
: 394 2 10 ben
: 397 2 7  schuf
: 400 2 10 ten
: 403 2 10  wie
: 407 2 7  ver
: 410 2 12 rückt,
- 414
: 419 2 12 d'rum
: 422 2 10  wir
: 425 2 12 ken
: 428 2 14  sie
: 431 2 14  auch
: 435 3 12  so
: 439 2 10  be
: 443 3 7 drückt.
- 448
: 451 2 14 Wo
: 454 2 12  hasst
: 458 2 10  man
: 461 2 7  stre
: 464 2 5 ben?
- 467
: 467 2 14  Wo
: 470 3 14  lebt
: 474 1 10  man
: 476 4 7  e
: 481 2 5 ben?
- 484
: 485 2 14 Un
: 489 1 12 ter
: 491 2 10  dem
* 494 10 10  Meer.
- 506 540
: 560 1 5 Bei
: 562 2 10  uns
: 566 2 10  sind
: 570 1 10  die
: 572 2 9  Fi
: 576 2 12 sche
: 579 2 10  glück
: 583 2 5 lich.
- 587
: 591 2 2 Man
: 594 2 5  tu
: 597 2 5 mmelt
: 600 2 2  sich
: 603 2 0  und
: 607 2 5  hat
: 611 4 2  Spaß.
- 617
: 622 2 5 An
: 625 2 10  Land
: 629 2 10  bist
: 632 2 10  du
: 635 2 9  gar
: 639 2 12  nicht
: 643 2 10  glück
: 647 2 5 lich.
- 651
: 654 2 2 Du
: 657 2 5  lan
: 661 2 5 dest
: 665 1 2  in
: 667 3 0  ei
: 671 2 5 nem
: 675 4 2  Glas.
- 681
: 686 2 7 Dem
: 689 2 10  Schick
: 693 1 10 sal
: 695 2 10  bist
: 698 2 5  du
: 701 2 10  echt
: 706 3 5  schnu
: 710 2 10 ppe.
- 714
: 718 1 10 Du
: 720 3 12  bist
: 725 1 12  schon
: 727 2 10  ein
* 730 3 14  ar
: 734 2 12 mer
: 738 3 10  Fisch.
- 743
: 750 1 10 Hat
: 752 2 10  dein
: 755 2 7  Boss
: 759 2 10  mal
: 763 2 5  Lust
: 767 2 10  auf
: 771 3 5  Su
: 775 2 10 ppe,
- 779
: 783 2 -24 schwupps,
: 787 2 -24  bist
: 791 1 -24  du
: 793 1 -26  auf
: 795 3 -22  sei
: 799 2 -24 nem
: 803 3 -26  Tisch.
- 807
F 807 1 14 Oh
F 809 5 10  nein!
- 816
: 818 3 14 Un
: 822 1 12 ter
: 824 2 10  dem
: 828 6 7  Meer,
- 836 837
: 849 3 14 un
: 853 2 12 ter
: 856 2 10  dem
* 860 5 12  Meer,
- 867 868
: 880 2 14 nie
: 883 2 12 mand
: 887 2 10  fri
: 890 2 7 tiert
: 894 1 3  dich,
- 896
* 897 2 14 brät
: 900 2 12  und
: 903 2 10  ser
: 906 2 7 viert
: 910 2 5  dich
- 913
: 913 2 14 hier
: 916 2 12  zum
: 919 2 10  Ver
* 922 7 14 zehr.
- 931 932
: 945 2 14 Der
: 948 2 12  Mensch
: 951 2 10  hat
: 954 2 7  uns
: 958 2 10  zum
: 961 2 10  fre
: 964 2 7 ssen
: 968 4 12  gern,
- 974
: 977 2 12 doch
: 980 2 10  hier
: 983 2 12  sind
: 986 3 14  sei
: 990 1 14 ne
: 992 3 12  Ha
: 997 2 10 ken
: 1000 3 7  fern.
- 1005
: 1010 2 14 Lass
: 1014 2 12  Stress
: 1017 1 10  und
: 1019 2 7  Hass
: 1022 3 3  sein!
- 1026
: 1026 1 14 Le
: 1028 1 12 ben
: 1030 2 10  muss
* 1034 3 7  Spaß
: 1039 3 5  sein
- 1042
: 1042 2 14 un
: 1046 1 12 ter
: 1048 2 10  dem
: 1051 5 10  Meer,
- 1058 1059
: 1075 2 14 un
: 1078 1 12 ter
: 1080 2 10  dem
: 1084 5 12  Meer.
- 1091 1092
: 1103 2 14 Ist
: 1106 1 12  es
: 1108 2 10  nicht
: 1112 2 7  toll
: 1116 2 3  hier,
- 1119
: 1120 2 14 ganz
: 1123 2 12  wun
: 1126 2 10 der
: 1129 2 7 voll
: 1132 2 5  hier,
- 1135
: 1136 2 14 vo
: 1139 1 12 ller
: 1141 2 10  E
* 1146 5 14 sprit?
- 1153 1154
: 1169 2 14 So
: 1172 1 12 gar
: 1174 2 10  die
: 1177 3 7  Spro
: 1182 1 10 tten
: 1184 2 10  und
: 1187 2 7  der
: 1191 3 12  Lachs,
- 1196
: 1200 2 12 die
: 1203 1 10  spie
: 1205 2 12 len
: 1209 2 14  spo
: 1213 2 14 ttend
: 1216 2 12  mit
: 1220 2 10  zum
: 1223 4 7  Flachs.
- 1229
: 1233 2 14 Hier
: 1236 1 12  spie
: 1238 1 10 len
: 1240 3 7  a
: 1244 2 3 lle,
- 1247
* 1248 2 14 Zan
: 1251 1 12 der
: 1253 2 10  und
: 1257 3 7  Qua
: 1261 2 5 lle,
- 1264
: 1264 2 14 un
: 1267 1 12 ter
: 1269 2 10  dem
: 1273 10 10  Meer.
- 1285 1286
: 1301 2 5 Die
: 1304 2 9  Kröt'
: 1308 1 9  spielt
: 1310 2 9  die
: 1313 2 9  Flöt'.
- 1316
: 1316 2 5 Die
: 1319 2 5  Larv'
: 1323 2 5  zupft
: 1326 1 5  die
: 1328 2 5  Harf'.
- 1331
: 1332 2 9 Die
: 1335 2 5  Brass'
: 1339 2 5  schlägt
: 1342 1 5  den
: 1344 2 5  Bass.
- 1347
: 1348 1 5 Klingt
: 1350 2 5  der
: 1354 2 5  Sound
: 1358 1 5  nicht
: 1360 2 5  scharf?
- 1363
: 1365 1 5 Der
: 1367 2 7  Barsch
: 1371 2 7  bläst
: 1374 1 7  den
: 1376 2 10  Marsch.
- 1379
: 1380 2 7 Die
: 1383 2 5  Schlei'
: 1387 2 5  spielt
: 1390 2 5  Schal
: 1393 2 10 mei
- 1396
: 1398 2 10 und
: 1401 2 9  hier
: 1404 2 9  ist
: 1407 1 10  der
: 1409 2 12  King
: 1412 2 9  of
* 1416 5 10  Soul.
- 1422
F 1424 2 5 Yeah!
- 1427
: 1428 2 5 Der
: 1431 2 9  Hecht
: 1435 2 9  jazzt
: 1439 1 9  nicht
: 1441 2 9  schlecht.
- 1444
: 1444 2 5 Der
: 1447 2 5  Schell
: 1451 2 5 fisch
: 1454 1 5  singt
: 1456 2 5  hot.
- 1459
: 1460 1 7 Der
: 1462 3 5  Wal
: 1467 2 5  singt
: 1471 1 5  na
: 1473 2 5 sal.
- 1476
: 1476 2 5 Der
: 1479 2 5  Butt
: 1483 2 5  am
: 1486 1 5  Fa
: 1488 2 5 gott.
- 1491
: 1492 1 5 Der
: 1494 2 7  Molch
: 1498 2 7  und
: 1502 1 7  der
: 1504 2 10  Lurch,
- 1507
: 1508 2 7 die
: 1511 2 5  schwi
: 1515 1 5 mmen
: 1517 1 5  nur
: 1519 2 10  durch
- 1522
: 1524 1 10 und
: 1526 1 10  der
: 1528 2 9  Fisch,
: 1533 1 10  der
: 1535 3 12  bläst
: 1540 1 10  sich
: 1542 6 10  auf
* 1550 5 14 ~ .
- 1557 1867
F 1887 10 16 Ja!
- 1899
: 1904 2 16 Un
: 1908 1 14 ter
: 1910 2 12  dem
: 1913 8 9  Meer,
- 1923 1924
: 1934 2 16 un
: 1938 1 14 ter
: 1940 2 12  dem
* 1944 8 14  Meer.
- 1954 1955
: 1966 2 16 Wenn
: 1969 2 14  der
: 1972 2 12  Del
: 1975 2 9 phin
- 1978
: 1979 2 16 be
: 1982 2 16 ginnt
: 1985 1 16  die
: 1987 3 16  Be
: 1991 3 16 guine,
- 1995
: 1996 1 7 will
: 1998 2 16  ich
: 2002 1 14  i
: 2004 2 12 mmer
* 2007 9 16  mehr.
- 2018 2019
: 2030 2 16 Was
: 2033 1 16  ha
: 2035 2 16 ben
: 2038 2 16  die
: 2041 2 9  au
: 2045 2 12 ßer
: 2048 2 9  viel
: 2053 3 14  Sand?
- 2058
: 2062 2 12 Nur
: 2065 2 14  wir
: 2068 2 16  sind
: 2071 3 16  au
: 2075 2 16 ßer
: 2078 2 14  Rand
: 2082 2 12  und
: 2085 3 9  Band,
- 2090
: 2095 2 16 denn
: 2098 1 14  je
: 2100 2 12 des
: 2103 2 12  Tier
: 2107 2 9  hier,
- 2110
: 2110 2 16 das
: 2113 1 16  mu
: 2115 2 14 si
: 2118 3 16 ziert
: 2123 2 9  hier
- 2126
: 2127 2 16 un
: 2130 1 14 ter
: 2132 2 12  dem
* 2135 8 12  Meer.
- 2145 2146
: 2158 2 16 Selbst
: 2162 1 14  je
: 2164 2 12 de
: 2167 2 9  Schne
: 2170 2 5 cke
- 2173
: 2174 2 16 kriecht
: 2177 2 14  aus
: 2180 1 12  der
: 2182 2 9  E
: 2185 2 7 cke
- 2188
: 2188 2 16 un
: 2192 1 14 ter
: 2194 2 12  dem
* 2197 7 12  Meer.
- 2206 2207
: 2222 1 16 Je
: 2224 1 14 de
: 2226 2 12  Lan
: 2229 2 9 gu
: 2233 2 5 ste
- 2236
: 2236 2 16 kommt
: 2239 2 14  aus
: 2242 2 12  der
: 2245 2 9  Pus
: 2248 2 7 te.
- 2251
: 2253 2 16 Siehst
: 2256 2 14  un
: 2259 1 12 ter
: 2261 3 12  Wa
: 2265 2 9 sser:
- 2268
* 2269 3 16 Hei
: 2273 1 14 sser
: 2275 2 12  und
: 2279 2 14  na
: 2282 2 12 sser.
- 2285
: 2287 2 16 Uns
: 2291 2 12  geht's
: 2295 3 14  toll
: 2299 2 12  hier,
- 2302
: 2302 3 16 ganz
: 2307 1 14  wun
: 2309 2 12 der
: 2312 2 14 voll
: 2315 2 12  hier
- 2318
: 2320 2 16 un
: 2323 1 14 ter
: 2325 2 12  dem
* 2329 52 12  Meer.
E
