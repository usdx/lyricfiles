#ARTIST:Moulin Rouge
#TITLE:Lady Marmalade (Christina ft. Mya, Pink & Lil Kim)
#MP3:Moulin Rouge - Lady Marmalade (Christina ft. Mya, Pink & Lil Kim).mp3
#AUTHOR:Made in StarForg
#Cover:moulin+collectors+edition.jpg
#VIDEO:Christina Aguilera - Lady Marmalade.flv
#VIDEOGAP:7
#LANGUAGE:English
#BPM:118
#GAP:12500
: 1 2 65 He 
: 3 2 67 met 
: 6 2 70 mar
: 8 2 70 ma
: 10 4 67 lade 
- 14
: 14 2 70 Down 
: 17 2 70 in 
: 19 4 67 old 
: 23 2 70 mou
: 25 2 67 lin 
: 27 2 70 ro
: 29 1 69 u
: 30 1 68 ge 
- 31
: 38 2 67 Strut
: 40 2 70 tin 
: 42 2 70 her 
: 44 4 67 stu
: 48 2 70 ff 
: 50 2 70 on 
: 52 2 67 the 
: 55 2 70 street 
- 57 68
: 69 1 63 She 
: 70 4 63 said, 
: 75 2 69 "Hel
: 77 2 63 lo, 
: 79 2 67 Hey 
: 81 2 67 Joe 
- 83
: 87 1 63 You 
: 88 1 67 wan
: 89 1 67 na 
: 90 1 67 give 
: 91 1 67 it 
: 92 2 67 a 
: 94 2 70 go?" 
- 97
: 107 2 70 Gou
: 109 2 70 chie, 
: 111 2 70 gou
: 113 2 70 chie, 
: 116 2 67 ya 
: 118 2 67 ya 
: 123 2 69 da
: 125 4 67 da 
- 138
: 142 2 70 Gou
: 144 2 70 chie, 
: 146 2 70 gou
: 148 2 70 chie, 
: 150 1 67  
: 152 2 67 Ya 
: 154 3 67 ya 
: 158 5 69 here 
- 164
: 176 2 70 Mo
: 178 2 70 cha 
: 180 2 70 cho
: 182 2 70 ca
: 184 3 67 la
: 186 2 67 ta 
: 188 2 68 ya 
: 192 4 69 ya 
: 196 2 67 ya 
- 198
: 211 1 61 Cr
: 212 1 62 eo
: 213 2 61 le 
: 215 2 60 la
: 217 2 62 dy 
- 219
: 219 2 60 Mar
: 221 3 58 ma
: 225 25 55 lade 
- 256
: 262 2 69 Vou
: 264 2 69 lez 
: 266 2 69 vous 
: 268 2 69 cou
: 270 2 69 cher 
: 272 2 69 a 
: 274 2 69 vec 
: 276 4 67 moi 
: 282 2 69 Ce 
: 284 4 67 soir 
- 292
: 298 2 69 Vou
: 300 2 69 lez 
: 302 2 69 vous 
: 304 2 69 cou
: 306 2 69 cher 
: 308 2 69 a 
: 310 2 69 vec 
: 312 2 67 moi 
- 316
: 330 2 67 He 
: 332 2 70 sat 
: 334 2 70 in 
: 336 4 67 her 
: 340 2 70 bou
: 342 2 70 doir 
- 344
: 344 4 67 While 
: 348 2 70 she 
: 350 2 70 fresh
: 352 2 67 ened 
: 354 3 67 up 
- 357
: 369 1 67 Bo
: 370 2 67 y 
: 372 2 67 drank 
: 374 1 67 all 
: 375 2 67 that 
: 377 1 67 ma
: 378 2 67 gno
: 380 2 67 li
: 382 1 67 a 
: 384 2 67 wine 
: 386 2 66  
- 389
: 396 3 67 All 
: 399 3 67 her 
: 403 4 65 black 
: 407 3 65 sa
: 410 2 62 tin 
: 412 2 65 sheets, 
: 418 2 65 Suede's, 
: 420 3 65 dark 
: 423 9 62 greens 
- 433
: 434 2 62 Gou
: 436 2 62 chie, 
: 438 2 62 gou
: 440 2 62 chie, 
: 442 2 60 ya 
: 444 4 60 ya 
: 448 4 60 da
: 452 1 58 da 
: 453 2 55  
- 459
: 468 2 65 Gou
: 470 2 65 chie, 
: 472 2 65 gou
: 474 2 65 chie, 
: 476 2 62 ya 
: 478 4 65 ya 
: 482 4 64 he
: 486 2 63 ~
: 488 2 60 re 
- 492
: 503 2 70 Mo
: 505 2 70 cha 
: 507 2 70 cho
: 509 2 70 ca 
: 511 2 67 la
: 513 4 67 ta 
: 517 4 67 ya 
: 521 2 67 ya 
- 523
: 537 2 74 Cre
: 539 2 72 ole 
: 541 2 74 dy 
: 543 2 72 la
: 545 2 72 mar
: 547 4 70 ma
: 551 32 67 lade 
- 588
: 592 2 69 Voulez 
: 594 2 69 vous 
: 596 2 69 cou 
: 598 2 69 cher 
: 600 2 69 a 
: 602 2 69 vec 
: 604 4 67 moi 
: 609 2 69 Ce 
: 611 2 67 soir 
- 616
: 622 2 69 Vou
: 624 2 69 lez 
: 626 2 69 vous 
: 628 2 69 cou 
: 630 2 69 cher 
: 632 2 69 a 
: 634 2 69 Cec 
: 636 3 67 moi 
- 713
F 716 1 1 When 
F 717 2 1 I 
F 719 1 1 can 
F 720 1 1 spen
F 721 1 1 d 
F 722 2 1 yours 
- 950
: 950 2 3 Marma
: 953 17 9 la
: 970 2 7 ade 
- 972
: 986 3 3 Marma
: 989 23 9 la
: 1012 2 4 ade 
- 1014
: 1048 8 76 Hey 
: 1056 8 80 hey 
: 1064 4 81 he
: 1068 11 83 y! 
- 1083
: 1086 2 74 Touch 
: 1088 2 75 of 
: 1090 3 74 her 
: 1093 2 73 skin 
- 1096
: 1096 2 74 Feel
: 1098 3 74 ing 
: 1101 3 75 sil
: 1104 1 72 ky 
: 1105 4 74 smooth 
- 1111
: 1121 2 67 Co
: 1123 2 70 lor 
: 1125 2 70 of 
: 1127 4 67 cof
: 1131 2 70 fe 
: 1133 2 70 au 
: 1135 2 67 la
: 1137 1 70 it 
- 1141
: 1144 1 65 Al 
: 1145 2 67 right 
- 1151
: 1152 3 67 Made 
: 1155 2 67 the 
: 1157 1 67 savage 
: 1158 7 65 beast 
- 1165
: 1165 2 65 Inside 
: 1167 2 67 roar 
: 1169 1 65 until 
: 1170 2 65 he 
: 1172 16 62 cried, 
- 1188
: 1189 7 62 More 
- 1197
: 1197 7 66 More 
- 1205
: 1205 14 69 More 
- 1222
: 1224 2 70 Now 
: 1226 2 70 he's 
: 1228 4 70 back 
: 1232 2 70 home 
: 1234 2 70 doin' 
: 1236 2 70 nine 
: 1238 2 67 to 
: 1240 8 67 five 
- 1248
: 1258 2 68 Slee
: 1260 2 70 pin' 
: 1262 2 70 the 
: 1264 4 68 grey 
: 1268 2 70 flan
: 1270 2 70 nel 
: 1272 1 68 li
: 1273 4 70 fe 
- 1279
: 1288 3 67 But 
: 1291 2 67 when 
: 1293 1 67 he 
: 1294 7 67 turns 
- 1301
: 1301 2 67 Off 
: 1303 1 67 to 
: 1304 5 65 sleep 
: 1309 4 67  
: 1313 3 65 Me
: 1316 2 65 mo
: 1318 1 65 ries 
: 1319 3 62 creep, 
- 1324
: 1324 6 62 More 
- 1333
: 1333 7 66 More 
- 1342
: 1345 14 69 More 
- 1362
: 1363 2 74 Gou
: 1365 2 72 chie, 
: 1367 2 72 gou
: 1369 2 70 chie, 
: 1371 2 72 ya 
: 1373 2 72 ya 
: 1375 4 72 da
: 1379 2 70 da 
: 1381 1 67  
- 1387
: 1395 2 70 Gou
: 1397 2 70 chie, 
: 1399 2 70 gou
: 1401 2 70 chie, 
: 1403 2 67 ya 
: 1405 4 67 ya 
: 1409 4 69 here 
- 1418
: 1428 2 70 Mo
: 1430 2 70 cha 
: 1432 2 70 cho
: 1434 2 67 ca 
: 1436 2 67 la
: 1438 2 67 ta 
: 1440 4 69 ya 
: 1444 1 67 ya 
: 1445 2 64  
- 1451
: 1464 2 74 Cre
: 1466 1 72 ole 
: 1468 2 72 la
: 1470 1 74 dy 
- 1473
: 1473 2 72 Mar
: 1475 3 70 ma
: 1479 34 74 lade 
- 1513
: 1517 2 69 Vou
: 1519 2 69 lez 
: 1521 2 69 vous 
: 1523 2 69 cou 
: 1525 2 69 cher 
: 1527 2 69 a 
: 1529 2 69 vec 
: 1531 3 67 moi 
: 1535 2 68  
: 1537 2 69 Ce 
: 1539 1 67 soir 
- 1543
: 1550 2 69 Vou
: 1552 2 69 lez 
: 1554 2 69 vous 
: 1556 2 69 cou 
: 1558 2 69 cher 
: 1560 2 69 a 
: 1562 2 69 vec 
: 1564 5 67 moi 
- 1572
: 1583 2 69 Vou
: 1585 2 69 lez 
: 1587 2 69 vous 
: 1589 2 69 cou 
: 1591 2 69 cher 
: 1593 2 69 a - 
: 1595 2 69 vec 
: 1598 3 67 moi 
: 1601 2 68 C
: 1605 2 69 e 
: 1607 3 67 soir 
- 1612
: 1618 2 69 Vou
: 1620 2 69 lez 
: 1622 2 69 vous 
: 1624 2 69 cou 
: 1626 2 69 cher 
: 1628 2 69 a 
: 1630 3 69 vec 
: 1633 5 67 moi 

