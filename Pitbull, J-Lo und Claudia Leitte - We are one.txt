#ARTIST:Pitbull Feat. Jennifer Lopez & Claudia Leitte
#TITLE:We Are One (Ole Ola)
#MP3:Pitbull & Jennifer Lopez feat. Claudia Leitte - We Are One .mp3
#AUTHOR:Fepo
#EDITION:Fepo
#GENRE:Pop
#YEAR:2014
#LANGUAGE:English
#BPM:250,02
#GAP:15830
#VIDEO:Pitbull & Jennifer Lopez feat. Claudia Leitte - We Are One .avi

: 0 2 -1 Put
: 4 2 1  your
: 8 3 4  flags
: 12 2 1  up
: 16 2 -1  in
: 20 2 1  the
: 24 9 1  sky
- 35
: 64 4 -1 A
: 69 2 1 ~nd
: 72 2 4  wave
: 76 2 1  them
: 80 2 -1  side
: 84 2 -3  to
: 88 9 -1  side
- 99
: 128 2 -1 Show
: 132 2 1  the
: 136 3 4  wo
: 140 2 1 ~rld
: 144 2 -1  where
: 148 2 -3  you're
: 152 6 -1  from
- 160
: 192 2 -1 Show
: 196 2 1  the
: 200 3 4  wo
: 204 2 1 ~rld
: 208 2 -1  we
: 212 2 -3  are
: 216 8 -3  one
- 226
: 260 2 -1 O
: 264 7 1 le
: 276 2 -1  o
: 280 7 1 le
: 292 2 -1  o
: 296 7 1 le
: 305 2 1  o
: 308 2 -1 ~
: 312 6 1 la
- 320
: 324 2 -1 O
: 328 7 1 le
: 340 2 -3  o
: 344 8 -1 le
: 356 2 -3  o
: 360 6 -1 le
: 369 2 -1  o
: 372 3 -3 ~
: 377 3 -1 la
- 382
: 388 2 -3 O
: 392 7 -1 le
: 404 2 -3  o
: 408 8 -1 le
: 420 2 -3  o
: 424 6 -1 le
: 434 2 -1  o
: 437 2 -3 ~
: 440 5 -1 la
- 447
: 452 2 -3 O
: 456 7 -1 le
: 468 2 -3  o
: 472 7 -3 le
: 484 2 -3  o
: 488 7 -3 le
: 497 5 -3  o
: 504 6 -3 la
- 512
F 540 1 -1 When
F 542 1 -1  the
F 545 1 -1  go
F 547 1 -1 ing
F 549 1 1  gets
F 552 3 1  tough
- 557
F 574 1 -3 The
F 577 2 -1  tough
F 581 2 -1  get
F 584 2 1  go
F 588 2 -1 ing
- 592
F 604 2 -1 One
F 608 6 -1  love,
F 620 2 -1  one
F 624 4 -1  life,
F 636 2 -1  one
F 640 6 -1  world
- 648
F 652 2 1 One
F 656 4 1  fight,
F 668 2 1  whole
F 672 4 1  world,
F 684 2 1  one
F 688 4 1  night,
F 700 2 1  one
F 704 4 1  place
- 710
F 718 2 1 Bra
F 722 3 1 zil,
F 732 1 1  eve
F 734 1 1 ry
F 736 1 1 bo
F 738 1 1 dy
F 740 1 1  put
F 742 1 1  your
F 745 2 1  flags
F 748 2 1  In
F 751 1 1  the
F 753 4 1  sky
- 758
F 760 1 1 And
F 762 1 1  do
F 764 1 1  what
F 766 1 1  you
F 769 7 1  feel
- 778
F 795 2 1 It's
F 798 2 1  your
F 801 3 1  world,
F 805 2 1  my
F 809 3 1  world,
F 814 2 1  our
F 817 2 1  world
F 820 2 1  to
F 823 2 1 day
- 826
F 826 2 1 And
F 829 1 1  we
F 831 1 1  in
F 833 1 1 vite
F 835 1 1  the
F 837 2 1  whole
F 841 2 1  world,
F 845 2 1  whole
F 848 3 1  world
F 852 1 1  to
F 854 3 1  play
- 858
F 859 2 1 It's
F 862 2 1  your
F 865 3 1  world,
F 869 2 1  my
F 873 3 1  world,
F 878 2 1  our
F 881 2 1  world
F 884 2 1  to
F 887 2 1 day
- 890
F 891 2 1 And
F 894 1 1  we
F 896 1 1  in
F 898 1 1 vite
F 900 1 1  the
F 902 2 1  whole
F 906 2 1  world,
F 910 2 1  whole
F 913 3 1  world
F 917 1 1  to
F 919 3 1  play
- 924
F 926 2 1 Es
F 929 2 1  mi
F 933 2 1  mun
F 937 2 1 do,
F 944 2 1  tu
F 948 3 1  mun
F 952 2 1 do,
F 960 3 1  el
F 964 3 1  mun
F 969 2 1 do
F 972 2 1  de
F 975 2 1  no
F 978 2 1 so
F 981 2 1 tros
- 985
: 1024 2 -1 Put
: 1028 2 1  your
: 1032 3 4  flags
: 1036 2 1  up
: 1040 2 -1  in
: 1044 2 1  the
: 1048 9 1  sky
- 1059
: 1088 4 -1 A
: 1093 2 1 ~nd
: 1096 2 4  wave
: 1100 2 1  them
: 1104 2 -1  side
: 1108 2 -3  to
: 1112 9 -1  side
- 1123
: 1152 2 -1 Show
: 1156 2 1  the
: 1160 3 4  wo
: 1164 2 1 ~rld
: 1168 2 -1  where
: 1172 2 -3  you're
: 1176 6 -1  from
- 1184
: 1216 2 -1 Show
: 1220 2 1  the
: 1224 3 4  wo
: 1228 2 1 ~rld
: 1232 2 -1  we
: 1236 2 -3  are
: 1240 8 -3  one
- 1250
: 1284 2 -1 O
: 1288 7 1 le
: 1300 2 -1  o
: 1304 7 1 le
: 1316 2 -1  o
: 1320 7 1 le
: 1329 2 1  o
: 1332 2 -1 ~
: 1336 6 1 la
- 1344
: 1348 2 -1 O
: 1352 7 1 le
: 1364 2 -3  o
: 1368 8 -1 le
: 1380 2 -3  o
: 1384 6 -1 le
: 1393 2 -1  o
: 1396 3 -3 ~
: 1401 3 -1 la
- 1406
: 1412 2 -3 O
: 1416 7 -1 le
: 1428 2 -3  o
: 1432 8 -1 le
: 1444 2 -3  o
: 1448 6 -1 le
: 1457 2 -1  o
: 1460 3 -3 ~
: 1465 5 -1 la
- 1472
: 1476 2 -3 O
: 1480 7 -1 le
: 1492 2 -3  o
: 1496 7 -3 le
: 1508 2 -3  o
: 1512 7 -3 le
: 1524 2 -3  o
: 1528 6 -3 la
- 1536
: 1565 2 4 One
: 1568 2 4  night
: 1572 2 4  watch
: 1575 1 4  the
: 1577 3 6  world
: 1581 2 4  u
: 1584 2 4 nite
- 1587
: 1589 2 4 Two
: 1592 3 4  sides,
: 1597 2 4  one
: 1601 2 4  fight
: 1605 1 4  and
: 1607 1 4  a
: 1609 2 6  mil
: 1613 3 4 lion
: 1618 5 4  eyes
- 1625
: 1628 2 4 Full
: 1632 2 4  heart's
: 1636 1 4  gon
: 1638 1 4 na
: 1640 2 6  work
: 1644 2 4  so
: 1648 4 4  hard
- 1652
: 1652 2 4 Shoot,
: 1656 3 4  fall,
: 1661 2 4  the
: 1665 2 4  stars,
: 1668 2 4  fist
: 1672 2 4  raised
: 1676 2 4  up
: 1679 1 4  to
: 1681 3 9 wards
: 1685 2 8  the
: 1688 4 6  sky
- 1693
: 1693 2 6 To
: 1697 2 6 night
: 1701 1 6  watch
: 1703 1 6  the
: 1705 3 8  world
: 1709 2 6  u
: 1712 3 6 nite,
: 1717 3 8  world
: 1721 2 6  u
: 1724 3 6 nite,
: 1729 3 8  world
: 1733 2 6  u
: 1736 2 6 nite
- 1739
: 1740 2 6 For
: 1743 1 6  the
: 1745 2 6  fight,
: 1748 2 6  fight,
: 1752 2 6  fight,
: 1756 2 6  one
: 1760 3 6  night
- 1764
: 1765 1 6 Watch
: 1767 1 6  the
: 1769 3 8  world
: 1773 2 6  u
: 1776 3 6 nite
- 1780
: 1780 2 6 Two
: 1784 2 6  sides,
: 1788 2 6  one
: 1792 2 6  fight
: 1796 1 6  and
: 1798 1 6  a
: 1801 2 6  mil
: 1805 2 6 lion
: 1809 4 4  ey
: 1814 2 2 ~es
- 1817
: 1817 3 9 Hey,
: 1824 4 9  hey,
: 1832 4 9  hey,
: 1840 2 9  for
: 1844 2 8 ça
: 1848 2 9  for
: 1852 2 8 ça
: 1856 2 9  come
: 1860 2 8  and
: 1865 3 11  sing
: 1870 2 9  with
: 1874 6 8  me
- 1881
: 1882 4 8 Hey,
: 1889 4 8  hey,
: 1896 3 8  hey,
: 1901 2 6  al
: 1905 2 8 lez
: 1909 2 6  al
: 1913 2 8 lez
: 1916 2 8  come
: 1920 3 8  shout
: 1925 3 8  it
: 1929 2 8  out
: 1933 2 9  with
: 1936 7 8  me
- 1944
: 1945 5 9 Hey,
: 1952 4 9  hey,
: 1960 4 9  hey,
: 1968 2 8  come
: 1971 1 8  on
: 1973 2 8  now
- 1976
: 1977 5 9 Hey,
: 1984 4 9  hey,
: 1992 4 9  hey,
: 2000 2 8  come
: 2003 1 8  on
: 2005 2 8  now
- 2008
: 2009 5 9 Hey,
: 2017 5 9  hey,
* 2025 5 9  hey,
* 2033 5 9  hey,
* 2041 5 9  hey
- 2047
: 2048 2 -1 Put
: 2052 2 1  your
* 2056 3 4  flags
: 2060 2 1  up
: 2064 2 -1  in
: 2068 2 1  the
* 2072 9 1  sky
- 2083
: 2112 4 -1 A
: 2117 2 1 ~nd
: 2120 2 4  wave
: 2124 2 1  them
: 2128 2 -1  side
: 2132 2 -3  to
: 2136 9 -1  side
- 2147
: 2176 2 -1 Show
: 2180 2 1  the
: 2184 3 4  wo
: 2188 2 1 ~rld
: 2192 2 -1  where
: 2196 2 -3  you're
: 2200 6 -1  from
- 2208
: 2240 2 -1 Show
: 2244 2 1  the
: 2248 3 4  wo
: 2252 2 1 ~rld
: 2256 2 -1  we
: 2260 2 -3  are
: 2264 8 -3  one
- 2274
: 2308 2 -1 O
: 2312 7 1 le
: 2324 2 -1  o
: 2328 7 1 le
: 2340 2 -1  o
: 2344 7 1 le
: 2353 2 1  o
: 2356 2 -1 ~
: 2360 6 1 la
- 2368
: 2374 2 -1 O
: 2378 7 1 le
: 2390 2 -3  o
: 2394 8 -1 le
: 2406 2 -3  o
: 2410 6 -1 le
: 2419 2 -1  o
: 2422 3 -3 ~
: 2427 3 -1 la
- 2432
: 2436 2 -3 O
: 2440 7 -1 le
: 2452 2 -3  o
: 2456 8 -1 le
: 2468 2 -3  o
: 2472 6 -1 le
: 2481 2 -1  o
: 2484 3 -3 ~
: 2489 5 -1 la
- 2496
: 2500 2 -3 O
: 2504 7 -1 le
: 2516 2 -3  o
: 2520 7 -3 le
: 2532 2 -3  o
: 2536 7 -3 le
: 2546 4 -3  o
: 2552 6 -3 la
- 2560
F 2556 2 1 Clau
F 2559 1 1 di
F 2561 1 1 a
F 2563 2 1  Leit
F 2566 2 1 te,
F 2572 2 1  o
F 2575 2 1 bri
F 2578 2 1 ga
F 2581 2 1 do
- 2585
: 2620 3 9 É
: 2625 4 4  me
: 2630 2 1 u,
: 2636 2 9  é
: 2640 4 4  se
: 2645 2 1 u
- 2649
: 2652 2 4 Ho
: 2655 1 1 je
: 2657 1 1  é
* 2659 7 6  tu
: 2668 2 4 do
: 2672 3 1  nos
: 2677 3 -3 so
- 2682
: 2686 1 -1 Quan
: 2688 1 -3 do
: 2690 1 -1  cha
: 2692 1 -1 mo
: 2694 1 -3  mundo
: 2696 1 -3  in
: 2698 1 -1 tei
: 2700 1 -1 ro
: 2702 1 -1  pra
: 2704 1 -3  jo
: 2706 2 -1 gar
: 2709 2 -1  pra
: 2712 2 -3  mos
: 2715 3 6 trar
: 2720 2 4  que
: 2724 5 -1  po
: 2731 6 -3 tu
- 2739
: 2748 2 -1 Tor
: 2752 4 -1 cer,
: 2764 4 1  cho
: 2770 5 -1 rar,
: 2780 3 -1  so
* 2785 6 -1 rrir,
* 2796 3 6  gri
* 2801 6 4 tar
- 2809
: 2812 2 1 Nă
: 2815 1 1 o
: 2817 1 1  im
: 2819 2 1 por
: 2822 1 1 ta
: 2824 2 1  re
: 2827 2 1 sul
: 2830 2 1 ta
: 2833 2 1 do,
: 2836 2 1  va
: 2839 2 1 mos
: 2842 2 1  am
: 2845 3 1 va
- 2848
: 2848 2 -1 Put
: 2852 2 1  your
: 2856 3 4  flags
: 2860 2 1  up
: 2864 2 -1  in
: 2868 2 1  the
: 2872 9 1  sky
- 2883
: 2912 4 -1 A
: 2917 2 1 ~nd
: 2920 2 4  wave
: 2924 2 1  them
: 2928 2 -1  side
: 2932 2 -3  to
: 2936 9 -1  side
- 2947
: 2976 2 -1 Show
: 2980 2 1  the
: 2984 3 4  wo
: 2988 2 1 ~rld
: 2992 2 -1  where
: 2996 2 -3  you're
: 3000 6 -1  from
- 3008
: 3040 2 -1 Show
: 3044 2 1  the
: 3048 3 4  wo
: 3052 2 1 ~rld
: 3056 2 -1  we
: 3060 2 -3  are
: 3064 8 -3  one
- 3074
: 3108 2 -1 O
: 3112 7 1 le
: 3124 2 -1  o
: 3128 7 1 le
: 3140 2 -1  o
: 3144 7 1 le
: 3153 2 1  o
: 3156 2 -1 ~
: 3160 6 1 la
- 3168
: 3236 2 -1 O
: 3240 7 1 le
: 3252 2 -3  o
: 3256 8 -1 le
: 3268 2 -3  o
: 3272 6 -1 le
: 3281 2 -1  o
: 3284 3 -3 ~
: 3289 3 -1 la
- 3294
: 3300 2 -3 O
: 3304 7 -1 le
: 3316 2 -3  o
: 3320 7 -3 le
: 3332 2 -3  o
: 3336 7 -3 le
: 3346 4 -3  o
: 3352 6 -3 la

