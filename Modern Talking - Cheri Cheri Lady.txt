#TITLE:Cheri Cheri Lady
#ARTIST:Modern Talking
#MP3:Modern Talking - Cheri Cheri Lady.mp3
#COVER:Modern Talking - Cheri Cheri Lady [CO].jpg
#BACKGROUND:Modern Talking - Cheri Cheri Lady [BG].jpg
#BPM:228,20
#GAP:18273,44
#ENCODING:UTF8
#LANGUAGE:Englisch
#EDITION:Singstar - '80s
: 0 3 58 Oh
: 4 2 61  I
: 8 6 62  can
: 16 2 62 not
: 18 2 62  ex
: 22 6 62 plain
- 30
: 32 2 64 Ev
: 36 2 65 ery
* 40 6 67  time
: 48 2 65  it's
: 51 2 65  the
: 54 6 64  same
: 60 2 57 ~
- 63
: 64 4 57 More
: 68 4 64  I
: 72 4 62  feel
- 78
: 80 2 60 That
: 82 2 64  it's
: 84 6 62  real
- 92
: 96 4 60 Take
: 100 4 57  my
: 104 8 62  heart
- 114
: 128 3 57 I've
: 132 2 60  been
: 136 6 62  lone
: 144 3 62 ly
: 148 2 62  too
: 150 4 62  long
- 156
: 160 3 64 Oh
: 164 2 65  I
: 168 5 67  can't
: 176 3 65  be
: 180 2 64  so
: 184 4 64  strong
- 190
: 192 3 57 Take
: 195 2 64  the
: 198 4 62  chance
- 204
: 208 3 59 For
: 211 2 64  ro
: 213 4 62 mance
- 219
: 224 4 60 Take
: 228 4 57  my
: 232 8 62  heart
- 242
: 252 3 60 I
: 256 3 57  need
: 260 2 55  you
: 264 4 55  so
: 268 10 57 ~
- 280
: 288 3 57 There's
: 292 2 60  no
: 296 4 60  time
: 300 9 62 ~
- 311
: 316 3 58 I'll
: 320 3 57  e
: 323 2 55 ver
: 326 2 55  go
: 328 19 57 ~
- 348
: 348 4 60 Oh
: 352 4 57 ~
: 356 4 55 ~
: 360 6 57 ~
- 368
: 376 4 57 Che
: 380 2 60 ri,
: 384 4 62  che
: 388 2 65 ri
: 392 4 62  la
: 397 2 62 dy
- 401
: 408 4 57 Go
: 412 2 60 ing
: 416 4 60  through
: 420 2 65  e
: 424 4 64 mo
: 429 3 64 tion
- 434
: 440 4 57 Love
: 444 2 60  is
: 448 4 60  where
: 452 2 65  you
* 456 4 62  find
: 462 2 62  it
- 466
: 472 3 60 Lis
: 476 2 57 ten
: 480 4 57  to
: 484 2 65  your
: 486 8 62  heart
- 496
: 504 4 57 Che
: 508 2 60 ri,
: 512 4 62  che
: 516 2 65 ri
: 520 4 62  la
: 525 3 62 dy
- 530
: 535 4 57 Li
: 539 2 60 ving
: 544 4 60  in
: 548 2 65  de
: 550 6 64 vo
: 557 3 64 tion
- 562
: 568 4 57 S'al
: 572 2 60 ways
: 576 4 60  like
: 580 2 65  the
: 584 4 62  first
: 589 3 62  time
- 594
: 600 3 60 Let
: 604 2 57  me
: 608 3 57  take
: 612 2 65  a
: 614 8 62  part
- 624
: 632 4 74 Che
: 636 2 72 ri,
: 640 4 69  che
: 644 2 67 ri
: 648 4 69  la
: 653 2 69 dy
- 657
: 664 2 74 Like
: 668 3 72  there's
: 672 2 69  no
: 676 3 67  to
: 680 6 69 mor
: 686 2 67 row
- 690
: 696 2 74 Take
: 700 3 72  my
: 704 3 69  heart,
: 708 2 67  don't
: 712 4 69  lose
: 717 2 67  it
- 721
: 728 3 60 Lis
: 732 2 60 ten
: 736 3 60  to
: 740 2 69  your
: 742 6 69  heart
- 750
: 760 4 74 Che
: 764 2 72 ri,
: 768 4 69  che
: 772 2 67 ri
: 776 4 69  la
: 781 2 69 dy
- 785
: 790 2 69 To
* 792 4 74  know
: 796 3 72  you
: 800 2 69  is
: 804 3 67  to
: 808 5 69  love
: 813 2 67  you
- 817
: 824 2 74 If
: 828 2 72  you
: 832 3 69  call
: 836 2 67  me
: 840 4 69  ba
: 845 2 67 by
- 849
: 856 3 60 I'll
: 860 2 60  be
: 864 3 60  al
: 867 6 67 ways
: 873 2 65 ~
: 875 3 62 ~
: 879 10 62  yours
- 891
: 1184 3 57 I
: 1188 2 60  get
: 1192 3 62  up
- 1197
: 1200 3 62 I
: 1203 2 62  get
: 1205 6 62  down
- 1213
: 1216 2 64 All
: 1220 2 65  my
: 1224 5 67  world
: 1232 3 65  turns
: 1236 2 65  a
: 1238 6 64 round
- 1246
: 1248 4 57 Who
: 1252 2 64  is
* 1255 4 62  right?
- 1261
: 1264 2 57 Who
: 1266 2 64  is
: 1268 6 62  wrong?
- 1276
: 1280 4 60 I
: 1284 4 57  don't
: 1288 8 62  know
- 1298
: 1312 3 57 I've
: 1316 2 60  got
: 1320 6 62  pain
: 1328 3 62  in
: 1332 2 62  my
: 1334 4 62  heart
- 1340
: 1344 3 64 Got
: 1348 2 65  a
: 1352 5 67  love
: 1360 3 65  in
: 1364 2 64  my
: 1367 4 64  soul
- 1373
: 1375 3 57 Ea
: 1380 2 64 sy
: 1383 7 62  come
- 1391
: 1392 2 57 But
: 1394 2 64  I
: 1397 3 62  think
: 1400 2 60 ~
- 1404
: 1408 4 60 Ea
: 1412 4 57 sy
: 1416 14 62  go
- 1432
: 1436 3 60 I
: 1440 3 57  need
: 1444 2 55  you
: 1448 4 55  so
: 1452 9 57 ~
- 1463
: 1472 3 57 Al
: 1476 2 60 though
: 1480 4 60  times
: 1484 9 62 ~
- 1495
: 1500 3 60 I
: 1504 3 57  move
: 1508 2 55  so
* 1512 19 57  slow
- 1532
: 1532 4 60 Oh
: 1536 4 57 ~
: 1540 4 55 ~
: 1544 6 57 ~
- 1552
: 1560 4 57 Che
: 1564 2 60 ri,
: 1568 4 62  che
: 1572 2 65 ri
: 1576 4 62  la
: 1581 3 62 dy
- 1586
* 1592 4 57 Go
: 1596 2 60 ing
: 1600 4 60  through
: 1604 2 65  e
: 1608 4 64 mo
: 1613 5 64 tion
- 1620
: 1624 4 57 Love
: 1628 2 60  is
: 1632 4 60  where
: 1636 2 65  you
: 1640 4 62  find
: 1646 3 62  it
- 1651
: 1656 3 60 Lis
: 1660 2 57 ten
: 1664 4 57  to
: 1668 2 65  your
: 1670 8 62  heart
- 1680
: 1688 4 57 Che
: 1692 2 60 ri,
: 1696 4 60  che
: 1700 2 65 ri
: 1704 4 62  la
: 1709 3 62 dy
- 1714
: 1720 4 57 Li
: 1724 2 60 ving
: 1728 4 60  in
: 1732 2 65  de
: 1736 5 64 vo
: 1742 3 64 tion
- 1747
: 1752 4 57 S'al
: 1756 2 60 ways
: 1760 4 60  like
: 1764 2 65  the
: 1768 4 62  first
: 1773 6 62  time
- 1781
: 1784 3 60 Let
: 1788 2 57  me
: 1792 3 57  take
: 1796 2 65  a
: 1798 8 62  part
- 1808
: 1816 4 74 Che
: 1820 2 72 ri,
: 1824 4 69  che
: 1828 2 67 ri
: 1832 4 69  la
: 1837 2 69 dy
- 1841
: 1848 2 74 Like
: 1852 3 72  there's
: 1856 2 69  no
: 1860 3 67  to
* 1864 6 69 mor
: 1870 2 67 row
- 1874
: 1880 2 74 Take
: 1884 3 72  my
: 1888 3 69  heart,
: 1892 2 67  don't
: 1896 4 69  lose
: 1901 2 67  it
- 1905
: 1912 3 60 Lis
: 1916 2 60 ten
: 1920 3 60  to
: 1924 2 69  your
: 1926 6 69  heart
- 1934
* 1944 4 74 Che
: 1948 2 72 ri,
: 1952 4 69  che
: 1956 2 67 ri
: 1960 4 69  la
: 1965 2 69 dy
- 1969
: 1974 2 69 To
: 1976 4 74  know
: 1980 3 72  you
: 1984 2 69  is
: 1988 3 67  to
: 1992 5 69  love
: 1997 2 67  you
- 2001
: 2008 2 74 If
: 2012 2 72  you
: 2016 3 69  call
: 2020 2 67  me
: 2024 4 69  ba
: 2029 2 67 by
- 2033
: 2040 3 60 I'll
: 2044 2 60  be
: 2048 3 60  al
* 2051 7 67 ways
: 2058 2 65 ~
: 2060 3 62 ~
: 2064 10 62  yours
- 2076
: 2296 4 74 Che
: 2300 2 72 ri,
: 2304 4 69  che
: 2308 2 67 ri
: 2312 4 69  la
: 2318 2 69 dy
- 2322
: 2328 2 74 Like
: 2332 3 72  there's
: 2336 2 69  no
: 2340 3 67  to
: 2344 6 69 mor
: 2350 2 67 row
- 2354
: 2360 2 74 Take
: 2364 3 72  my
: 2368 3 69  heart,
: 2372 2 67  don't
* 2376 4 69  lose
: 2381 2 67  it
- 2385
: 2392 3 60 Lis
: 2396 2 60 ten
: 2400 3 60  to
: 2404 2 69  your
: 2406 5 69  heart
- 2413
: 2424 4 74 Che
: 2428 2 72 ri,
: 2432 4 69  che
: 2436 2 67 ri
: 2440 4 69  la
: 2445 2 69 dy
- 2449
: 2454 2 69 To
: 2456 2 74  know
: 2460 3 72  you
: 2464 2 69  is
: 2468 3 67  to
: 2472 6 69  love
: 2478 2 67  you
- 2482
: 2488 2 74 If
: 2492 3 72  you
: 2496 3 69  call
: 2500 2 67  me
: 2504 4 69  ba
: 2510 2 67 by
- 2514
: 2520 3 60 I'll
: 2524 2 62  be
: 2528 6 67  yours
: 2534 2 65 ~
: 2536 3 62 ~
E