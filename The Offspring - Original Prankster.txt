#TITLE:Original Prankster
#ARTIST:The Offspring
#MP3:The Offspring - Original Prankster.mp3
#COVER:The Offspring - Original Prankster [CO].jpg
#BACKGROUND:The Offspring - Original Prankster [BG].jpg
#BPM:146,5
#GAP:19160
#ENCODING:UTF8
: 0 2 57 You
: 2 2 57  can
: 4 2 69  do
: 6 2 57  it!
- 10
: 58 2 69 Un
: 60 2 69 til
: 62 2 69  the
: 64 3 69  break
: 68 2 69  of
: 72 4 69  dawn
- 76
: 76 4 69 Life
: 80 4 69  life
- 85
: 86 4 69 Can
: 90 4 69  not
: 94 2 67  go
: 96 2 69  by
: 98 2 69  the
: 100 2 69  let
: 102 2 64 ter
- 106
: 108 4 69 Time
: 112 4 69  time
- 117
: 118 3 69 Pro
: 122 3 69 zac
: 126 2 67  can
: 128 2 69  make
: 130 2 69  it
: 132 2 69  bet
: 134 4 64 ter
- 139
: 140 4 69 Noise
: 144 4 69  noise
- 149
: 150 4 69 A
: 154 2 69 ny
: 156 2 69  kind
: 158 2 69  will
: 160 4 69  do
- 164
: 164 2 69 Can
: 166 2 67  you
: 168 2 67  feel
: 170 2 67  it
: 172 2 67  slip
: 174 2 67  a
: 176 3 67 way
- 180
: 180 2 67 When
: 182 2 67  it's
: 184 3 68  all
: 188 3 68  on
: 192 3 68  you
- 197
: 200 4 69 Hey
: 204 4 69  crime
: 208 4 69  crime
- 213
: 214 3 69 Rock
: 218 4 69 in'
: 222 2 67  like
: 224 2 69  Ja
: 226 2 69 net
: 228 2 69  Re
: 230 4 64 no
- 235
: 236 4 69 Time
: 240 4 69  time
- 245
: 246 4 69 Eigh
: 250 4 69 teen
: 254 2 67  and
: 256 2 69  life
: 258 2 69  in
: 260 2 69  Chi
: 262 2 64 no
- 266
: 268 4 69 Freud
: 272 4 69  Freud
- 277
: 278 4 69 All
: 282 2 69  a
: 284 2 69 long
: 286 2 69  it's
: 288 4 69  true
- 292
: 292 2 69 Well
: 294 2 67  you
: 296 2 67  see
: 298 2 67  the
: 300 2 67  co
: 302 2 67 ming
: 304 4 67  day
- 308
: 308 2 67 Catch
: 310 2 67 es
: 312 3 68  up
: 316 3 68  to
: 320 3 68  you
: 324 2 69  ye
: 326 4 68 ah
- 331
: 332 3 69 Knock
: 336 3 76  down
: 340 2 72  the
: 342 4 74  walls,
- 347
: 348 2 74  it's
: 350 2 72  a
: 352 4 74 live
: 356 2 72  in
: 358 4 69  you
- 363
: 364 3 69 Knock
: 368 3 76  down
: 372 2 72  the
: 374 4 74  place,
- 379
: 380 2 74  you're
: 382 2 72  a
: 384 4 74 lone
: 388 2 72  it's
: 390 4 69  true
- 395
: 396 3 69 Knock
: 400 3 76  down
: 404 2 72  the
: 406 4 74  world,
- 411
: 412 2 74  it's
: 414 2 72  a
: 416 4 74 live
: 420 2 72  in
: 422 4 69  you
- 426
: 426 2 67 You
: 428 2 67  got
: 430 2 67 ta
: 432 4 67  keep
- 437
: 438 2 67  your
: 440 2 68  head
: 442 2 68  up
: 444 2 68  through
: 446 2 68  it
: 448 2 68  all
- 450
: 450 2 68 You're
: 452 2 68  gon
: 454 2 69 na
: 456 4 72  bust
: 460 4 69  out
: 464 2 67  on
: 466 4 69  it
- 470
: 470 2 55 (o
: 472 2 57 ri
: 474 2 59 gi
: 476 2 60 nal
: 480 3 60  prank
: 483 4 55 ster)
- 488
: 488 4 72 Break
: 492 4 69  out
: 496 2 67  ye
: 498 4 69 ah
: 502 2 55  (o
: 504 2 57 ri
: 506 2 59 gi
: 508 2 60 nal,
: 512 4 60  yeah)
- 518
: 520 4 72 Bust
: 524 4 69  out
: 528 2 67  on
: 530 4 69  it
: 534 2 55  (o
: 536 2 57 ri
: 538 2 59 gi
: 540 2 60 nal...)
- 544
: 546 2 72 You
: 548 2 72  nev
: 550 2 72 er
: 552 4 72  stop
: 556 2 69  now,
: 562 4 72  stop
: 566 4 69  now
- 570
: 570 2 69 That's
: 572 2 67  what
: 574 2 67  the
: 576 4 67  main
: 580 4 68  man
: 584 8 69  say
- 594
: 640 2 57 You
: 642 2 57  can
: 644 2 69  do
: 646 2 57  it
- 650
: 698 2 69 You
: 700 2 69  know
: 702 2 69  it
: 704 4 69  smells
: 708 2 69  like
: 712 2 69  shit
- 715
: 716 3 69 God
: 720 4 69  damn
- 725
: 726 4 69 Tag
: 730 4 69  team
: 734 2 67  the
: 736 2 69  dou
: 738 2 69 ble
: 740 2 69  head
: 742 2 64 er
- 746
: 748 2 69 Son
: 750 2 69  of
: 752 4 69  Sam
- 757
: 758 3 69 Fire
: 762 4 69  al
: 766 2 67 ways
: 768 2 69  makes
: 770 2 69  it
: 772 2 69  bet
: 774 3 64 ter
- 778
: 780 4 69 Navi
: 784 3 69 gate
- 787
: 787 2 69 With
: 790 5 69  style
: 796 1 69  and
: 797 1 69  a
: 799 5 69  plomb
- 804
: 804 2 69 Cause
: 806 2 67  where
: 808 2 67  ev
: 810 2 67 er
: 812 2 67  you're
: 815 3 67  at
- 819
: 820 2 67 That's
: 822 2 67  the
: 824 3 68  tip
: 828 3 68  you's
: 832 4 68  on
- 838
: 840 4 69 Hey
: 844 4 69  lies
: 848 4 69  lies
- 853
: 854 4 69 Says
: 858 2 69  he's
: 860 1 67  down
: 862 1 69  in
: 863 2 69  the
: 865 2 69  Ba
: 867 2 64 ha
: 869 5 64 mas
- 875
: 876 4 69 Tries
: 880 4 69  tries
- 885
: 886 3 69 Ban
: 889 2 69 gin'
: 891 2 67  lit
: 893 2 69 tle
: 895 4 69  hoochie
: 899 2 69  ma
: 901 5 64 mas
- 907
: 908 3 69 No
: 912 3 69  way
- 916
: 918 4 69 None
: 922 2 69  of
: 924 2 69  this
: 926 2 69  is
: 928 3 69  true
- 932
: 932 2 69 When
: 934 2 67  he'll
: 936 2 67  see
: 938 2 67  the
: 940 2 67  com
: 942 2 67 ing
: 944 2 67  day
- 947
: 948 2 67 When
: 950 2 67  the
: 952 4 68  joke's
: 956 4 68  on
: 960 4 68  you
: 964 2 69  ye
: 966 4 68 ah
- 971
: 972 3 69 Knock
: 976 3 76  down
: 980 2 72  the
: 982 4 74  walls,
- 987
: 988 2 74  it's
: 990 2 72  a
: 992 4 74 live
: 996 2 72  in
: 998 4 69  you
- 1003
: 1004 3 69 Knock
: 1008 3 76  down
: 1012 2 72  the
: 1014 4 74  place,
- 1019
: 1020 2 74  you're
: 1022 2 72  a
: 1024 4 74 lone
: 1028 2 72  it's
: 1030 4 69  true
- 1035
: 1036 3 69 Knock
: 1040 3 76  down
: 1044 2 72  the
: 1046 4 74  world,
- 1051
: 1052 2 74  it's
: 1054 2 72  a
: 1056 4 74 live
: 1060 2 72  in
: 1062 3 69  you
- 1066
: 1066 2 67 You
: 1068 2 67  got
: 1070 2 67 ta
: 1072 2 67  keep
- 1076
: 1078 2 67  your
: 1080 2 68  head
: 1082 2 68  up
: 1084 2 68  through
: 1086 2 68  it
: 1088 2 68  all
- 1090
: 1090 2 68 You're
: 1092 2 68  gon
: 1094 2 69 na
: 1096 4 72  bust
: 1100 4 69  out
: 1104 2 67  on
: 1106 3 69  it
- 1109
: 1109 2 55 (o
: 1111 2 57 ri
: 1113 2 59 gi
: 1115 2 60 nal
: 1119 3 60  prank
: 1122 4 55 ster)
- 1127
: 1128 4 72 Break
: 1132 4 69  out
: 1136 2 67  ye
: 1138 3 69 ah
: 1141 2 55  (o
: 1143 2 57 ri
: 1145 2 59 gi
: 1147 2 60 nal,
: 1151 5 60  yeah)
- 1158
: 1160 4 72 Bust
: 1164 4 69  out
: 1168 2 67  on
: 1170 3 69  it
- 1173
: 1173 2 55 (o
: 1175 2 57 ri
: 1177 2 59 gi
: 1179 2 60 nal...)
- 1183
: 1185 2 72 You
: 1187 2 72  nev
: 1189 2 72 er
: 1191 4 72  stop
: 1195 4 69  now,
: 1201 4 72  stop
: 1205 4 69  now
- 1209
: 1209 2 69 That's
: 1211 2 67  what
: 1213 2 67  the
: 1215 4 67  main
: 1219 4 68  man
: 1223 7 69  say
- 1232
: 1343 2 57 You
: 1345 2 57  can
: 1347 2 69  do
: 1349 2 57  it!
- 1353
: 1355 4 57 Dime
: 1359 4 57  dime
- 1364
: 1365 4 62 so
: 1369 4 62  good
: 1373 2 60  to
: 1375 4 62  see
: 1379 4 60  ya
- 1385
: 1387 4 57 Nine
: 1391 4 57  nine
- 1396
: 1397 3 60 don't
: 1401 3 60  wan
: 1405 2 57 na
: 1407 4 60  be
: 1411 5 57  ya
- 1417
: 1419 4 57 Dime
: 1423 4 57  dime
- 1428
: 1429 3 62 so
: 1433 3 62  good
: 1437 2 60  to
: 1439 4 62  see
: 1443 4 60  ya
- 1449
: 1451 4 57 Nine
: 1455 4 57  nine
- 1460
: 1461 3 60 don't
: 1465 3 60  wan
: 1468 2 57 na
: 1471 4 60  be
: 1475 5 57  ya
- 1481
: 1483 4 69 Crime
: 1487 4 69  crime
- 1492
: 1493 4 69 fine
: 1497 4 69  sen
: 1501 2 67 si
: 1503 4 69 mil
: 1507 5 69 la
- 1513
: 1515 4 69 Crime
: 1519 4 69  crime
- 1524
: 1525 4 69 fine
: 1529 4 69  sen
: 1533 2 67 si
: 1535 4 69 mil
: 1539 5 69 la
- 1545
: 1547 4 69 Crime
: 1551 4 69  crime
- 1556
: 1557 4 69 fine
: 1561 2 69  sen
: 1563 2 69 sim
: 1565 2 69 il
: 1567 2 69 la
- 1570
: 1570 2 69 When
: 1572 2 67  you'll
: 1574 2 67  see
: 1576 2 67  the
: 1578 2 67  com
: 1580 2 67 ing
: 1582 4 67  day
- 1586
: 1586 2 67 Catch
: 1588 2 67 es
: 1590 4 68  up
: 1594 4 68  to
: 1598 4 71  you
: 1602 2 71  ye
: 1604 4 69 ah
- 1609
: 1610 3 69 Knock
: 1614 3 76  down
: 1618 2 72  the
: 1620 4 74  walls,
- 1625
: 1626 2 74  it's
: 1628 2 72  a
: 1630 4 74 live
: 1634 2 72  in
: 1636 4 69  you
- 1641
: 1642 3 69 Knock
: 1646 3 76  down
: 1650 2 72  the
: 1652 6 74  place,
- 1658
: 1658 2 74  you're
: 1660 2 72  a
: 1662 4 74 lone
: 1666 2 72  it's
: 1668 6 69  true
- 1674
: 1674 3 69 Knock
: 1678 3 76  down
: 1682 2 72  the
: 1684 4 74  world,
- 1689
: 1690 2 74  it's
: 1692 2 72  a
: 1694 4 74 live
: 1698 2 72  in
: 1700 4 69  you
- 1704
: 1704 2 67 You
: 1706 2 67  got
: 1708 2 67 ta
: 1710 4 67  keep
- 1715
: 1716 2 67  your
: 1718 2 68  head
: 1720 2 68  up
: 1722 2 68  through
: 1724 2 68  it
: 1726 2 68  all
- 1728
: 1728 2 68 You're
: 1730 2 68  gon
: 1732 2 69 na
: 1734 4 72  bust
: 1738 4 69  out
: 1742 2 67  on
: 1744 4 69  it
- 1748
: 1748 2 55 (o
: 1750 2 57 ri
: 1752 2 59 gi
: 1754 2 60 nal
: 1758 3 60  prank
: 1761 5 55 ster)
- 1766
: 1766 4 72 Break
: 1770 4 69  out
: 1774 2 67  ye
: 1776 4 69 ah
: 1780 2 55  (o
: 1782 2 57 ri
: 1784 2 59 gi
: 1786 2 60 nal,
: 1790 6 60  yeah)
- 1797
: 1798 4 72 Bust
: 1802 4 69  out
: 1806 2 67  on
: 1808 4 69  it
- 1812
: 1812 2 55 (o
: 1814 2 57 ri
: 1816 2 59 gi
: 1818 2 60 nal...)
- 1822
: 1824 2 72 You
: 1826 2 72  ne
: 1828 2 72 ver
: 1830 4 72  stop
: 1834 4 69  now,
: 1840 4 72  stop
: 1844 4 69  now
- 1850
: 1862 4 72 Stop
: 1866 4 69  now
: 1872 4 72  stop
: 1876 4 69  now
- 1880
: 1880 2 67 That's
: 1882 2 67  what
: 1884 2 67  the
: 1886 4 67  main
: 1890 4 68  man
: 1894 10 69  say
E