#TITLE:Dein Anblick
#ARTIST:Schandmaul
#MP3:Schandmaul - Dein Anblick.mp3
#COVER:Schandmaul1.jpg
#BACKGROUND:schandmaul.jpg
#BPM:253,4
#GAP:30500
#MedleyStartBeat:774
#MedleyEndBeat:1434
#ENCODING:UTF8
#PREVIEWSTART:76,317
#LANGUAGE:german
#AUTHOR:Myrthan
: 0 2 9 Hätt 
: 4 6 11 ich 
: 12 2 9 ei
: 16 4 7 nen 
: 22 4 6 Pin
: 28 2 6 sel 
: 32 2 4 zu 
: 38 4 6 zeich
: 46 2 6 nen 
: 49 2 7 dein 
: 52 4 6 Ant
: 58 2 2 litz 
- 62
: 64 2 2 Den 
: 68 4 -3 Glanz 
: 76 2 2 dei
: 80 2 6 ner 
: 84 4 4 Au
: 90 2 6 gen 
: 96 2 7 den 
: 100 3 6 lieb
: 105 2 4 li
: 109 3 4 chen 
: 116 4 4 Mund 
- 122
: 128 2 11 Ich 
: 133 5 11 mal
: 142 2 9 te 
: 145 3 7 die 
: 150 5 6 Wim
: 158 2 6 per 
: 161 2 4 die 
: 164 3 6 Brau
: 167 5 6 e 
: 176 2 6 dein 
: 180 2 6 Lä
: 183 5 2 cheln 
- 190
: 194 2 2 Wie 
: 197 2 -3 ich 
: 201 2 2 es 
: 208 2 6 er
: 213 2 4 kann
: 218 2 6 te 
: 223 2 7 in 
: 230 6 6 Je
: 238 2 4 ner 
: 244 6 4 Stund 
- 252
: 512 2 9 Hätt 
: 518 4 11 ich 
: 524 2 9 ei
: 528 2 7 ne 
: 533 7 6 Flö
: 542 2 6 te 
: 545 1 4 zu 
: 548 7 6 spie
: 557 2 6 len 
: 561 1 7 die 
: 564 3 6 Klän
: 568 3 2 ge 
- 573
: 576 2 2 Die 
: 581 2 -3 von 
: 587 2 2 dei
: 592 2 6 ner 
: 597 2 4 An
: 602 2 6 mut 
: 608 2 7 und 
: 613 3 6 Schön
: 618 4 7 heit 
: 624 2 6 er
: 630 4 4 zähl'n 
- 636
: 642 2 11 Ich 
: 646 4 11 spiel
: 655 2 9 te 
: 659 1 7 den 
: 662 6 6 Rei
: 670 2 6 gen 
: 673 2 4 der 
: 676 4 6 himm
: 682 1 6 li
: 685 2 6 schen 
: 689 3 6 Tän
: 694 4 2 ze 
- 700
: 706 2 2 Wie 
: 709 2 -3 in 
: 715 2 2 den 
: 720 2 6 Ge
: 725 2 4 dan
: 730 2 6 ken 
: 736 2 7 die 
: 742 2 6 mich 
: 747 3 6 seit
: 752 2 6 her 
: 756 6 4 quäl'n 
- 764
: 774 2 6 Doch 
: 780 4 6 we
: 786 2 6 der 
: 790 4 11 Bil
: 796 2 7 der 
: 800 2 11 noch 
: 804 4 12 Klän
: 810 4 11 ge 
: 816 2 9 noch 
: 820 8 11 Wort 
- 830
: 838 6 -1 Könn
: 846 2 -1 ten 
: 849 1 6 be
: 854 4 11 schrei
: 860 2 9 ben 
: 864 1 7 was 
: 868 3 6 an 
: 876 2 7 je
: 880 2 6 nem 
: 884 6 4 Ort 
- 892
: 904 3 6 Mit 
: 909 2 6 mir 
: 913 2 6 ge
: 918 4 11 scheh'n 
: 924 3 7 a
: 928 3 14 ~ls 
: 934 4 12 ich 
: 941 2 12 dich 
: 945 2 9 ge
: 950 8 11 sehn 
- 960
: 964 4 -1 Du 
: 969 2 -1 in 
: 972 2 -1 je
: 976 2 6 ner 
: 980 4 11 Nacht 
: 992 2 7 den 
: 998 6 6 Schein 
: 1006 1 6 hast 
: 1008 2 6 ent
: 1012 6 4 facht 
- 1020
: 1058 2 6 Die 
: 1061 2 6 Son
: 1065 5 6 ne 
: 1072 2 6 die 
: 1077 6 6 Ster
: 1085 2 6 ne 
: 1089 2 6 tra
: 1092 2 6 gen 
: 1095 4 6 Kun
: 1100 1 7 de 
: 1104 4 6 von 
: 1110 6 4 dir 
- 1118
: 1130 4 6 je
: 1136 4 6 der 
: 1142 3 6 Luft
: 1147 3 6 hauch 
: 1152 3 6 er
: 1157 6 9 zählt 
: 1165 1 7 mir 
: 1168 4 6 von 
: 1174 6 4 dir 
- 1182
: 1196 3 6 je
: 1201 2 6 der 
: 1205 7 6 A
: 1213 2 4 tem
: 1217 3 2 zug 
: 1226 3 6 je
: 1231 2 6 der 
: 1237 3 4 Schritt
- 1242
: 1252 4 6 trägt 
: 1260 3 4 dei
: 1265 3 2 nen 
: 1270 8 2 Na
: 1280 4 -1 men 
: 1286 3 1 weit 
: 1291 3 2 mit 
: 1296 3 1 sich 
: 1302 5 -1 mit 
- 1309
: 1314 2 6 Die 
: 1317 2 6 Son
: 1321 5 6 ne 
: 1328 2 6 die 
: 1334 6 6 Ster
: 1342 2 6 ne 
: 1345 1 6 tra
: 1347 1 6 gen 
: 1350 4 6 Kun
: 1356 2 7 de 
: 1360 4 6 von 
: 1368 8 4 dir 
- 1378
: 1386 4 6 je
: 1392 4 6 der 
: 1398 3 6 Luft
: 1403 3 6 hauch 
: 1408 3 6 er
: 1414 4 9 zählt 
: 1420 2 7 mir 
: 1424 3 6 von 
: 1429 5 4 dir 
- 1436
: 1450 4 6 je
: 1456 2 6 der 
: 1460 6 6 A
: 1468 1 4 tem
: 1471 3 2 zug 
: 1482 4 6 je
: 1488 2 6 der 
: 1492 4 4 Schritt
- 1498
: 1508 4 6 trägt 
: 1516 2 4 dei
: 1520 4 2 nen 
: 1526 8 2 Na
: 1536 4 -1 men 
: 1542 2 1 weit 
: 1547 2 2 mit 
: 1552 4 1 sich 
: 1558 4 -1 mit 
- 1564
: 1824 2 9 Hätt 
: 1828 4 11 ich 
: 1836 2 9 ei
: 1840 2 7 ne 
: 1844 7 6 Fe
: 1853 2 6 der 
: 1857 2 4 zu 
: 1861 7 6 schrei
: 1870 2 6 ben 
: 1874 2 7 die 
: 1878 4 6 Wor
: 1884 2 2 te 
- 1888
: 1892 4 -3 Die 
: 1898 2 2 dich 
: 1904 4 6 um
: 1910 4 4 gar
: 1916 3 6 nen 
: 1921 3 7 wie 
: 1926 3 6 sil
: 1930 3 7 ber
: 1935 3 6 nes 
: 1940 4 4 Licht 
- 1946
: 1952 2 11 Ich 
: 1956 6 11 schrie
: 1965 2 9 be 
: 1969 2 7 von 
: 1973 6 6 lie
: 1981 2 6 be 
: 1985 2 4 von 
: 1989 3 6 Nä
: 1994 3 6 he 
: 1999 3 6 und 
: 2004 3 6 Hoff
: 2009 5 2 nung 
- 2016
: 2020 4 -3 Und 
: 2026 4 2 schrieb 
: 2032 3 6 die 
: 2037 3 4 Sehn
: 2042 3 6 sucht 
: 2047 3 7 hi
: 2053 3 6 naus 
: 2059 3 6 in 
: 2064 2 6 das 
: 2068 6 4 Nichts 
- 2076
: 2086 3 6 Doch 
: 2092 3 6 we
: 2098 2 6 der 
: 2103 4 11 Bil
: 2110 1 7 der 
: 2113 2 11 noch 
: 2117 3 12 Klän
: 2122 3 11 ge 
: 2128 3 9 noch 
: 2134 7 11 Wort 
- 2143
: 2148 6 -1 Könn
: 2157 2 -1 ten 
: 2161 1 6 be
: 2165 5 11 schrei
: 2172 2 9 ben 
: 2176 2 7 was 
: 2180 3 6 an 
: 2185 5 7 je
: 2192 4 6 nem 
: 2198 8 4 Ort 
- 2208
: 2212 4 6 Mit 
: 2218 4 6 mir 
: 2224 2 6 ge
: 2228 4 11 scheh'n 
: 2236 2 7 a
: 2239 5 14 ~ls 
: 2248 2 12 ich 
: 2252 2 12 dich 
: 2256 3 9 ge
: 2262 6 11 sehn 
- 2270
: 2276 2 -1 Du 
: 2280 2 -1 in 
: 2284 2 -1 je
: 2288 2 6 ner 
: 2294 5 11 Nacht 
: 2304 2 7 den 
: 2309 5 6 Schein 
: 2316 2 6 hast 
: 2320 2 6 ent
: 2324 6 4 facht 
- 2332
: 2368 2 6 Die 
: 2372 2 6 Son
: 2376 6 6 ne 
: 2384 2 6 die 
: 2390 4 6 Ster
: 2396 2 6 ne 
: 2401 1 6 tra
: 2403 1 6 gen 
: 2406 2 6 Kun
: 2412 2 7 de 
: 2417 3 6 von 
: 2423 7 4 dir 
- 2432
: 2442 4 6 je
: 2448 4 6 der 
: 2454 2 6 Luft
: 2458 4 6 hauch 
: 2464 4 6 er
: 2470 5 9 zählt 
: 2477 2 7 mir 
: 2481 2 6 von 
: 2485 6 4 dir 
- 2493
: 2504 6 6 je
: 2512 2 6 der 
: 2516 8 6 A
: 2525 3 4 tem
: 2530 4 2 zug 
: 2538 4 6 je
: 2544 2 6 der 
: 2549 5 4 Schritt
- 2556
: 2564 4 6 trägt 
: 2572 3 4 dei
: 2577 3 2 nen 
: 2582 8 2 Na
: 2592 3 -1 men 
: 2597 3 1 weit 
: 2603 3 2 mit 
: 2609 3 1 sich 
: 2614 4 -1 mit 
- 2620
: 2626 2 6 Die 
: 2629 3 6 Son
: 2634 3 6 ne 
: 2640 2 6 die 
: 2646 4 6 Ster
: 2652 2 6 ne 
: 2656 2 6 tra
: 2659 1 6 gen 
: 2662 4 6 Kun
: 2668 2 7 de 
: 2672 4 6 von 
: 2680 8 4 dir 
- 2690
: 2698 5 6 je
: 2705 4 6 der 
: 2711 3 6 Luft
: 2716 3 6 hauch 
: 2721 2 6 er
: 2726 5 9 zählt 
: 2733 1 7 mir 
: 2736 3 6 von 
: 2740 6 4 dir 
- 2748
: 2762 4 6 je
: 2768 4 6 der 
: 2774 6 6 A
: 2782 2 4 tem
: 2786 4 2 zug 
: 2794 5 6 je
: 2801 2 6 der 
: 2805 3 4 Schritt
- 2810
: 2820 6 6 trägt 
: 2828 4 4 dei
: 2834 2 2 nen 
: 2838 8 2 Na
: 2848 4 -1 men 
: 2854 4 1 weit 
: 2860 3 2 mit 
: 2865 2 1 sich 
: 2869 3 -1 mit 
- 2874
: 2882 2 6 Die 
: 2885 2 6 Son
: 2889 5 6 ne 
: 2896 2 6 die 
: 2900 6 6 Ster
: 2908 2 6 ne 
: 2912 2 6 tra
: 2915 1 6 gen 
: 2918 4 6 Kun
: 2924 2 7 de 
: 2928 5 6 von 
: 2936 8 4 dir 
- 2946
: 2954 4 6 je
: 2960 4 6 der 
: 2966 4 6 Luft
: 2972 3 6 hauch 
: 2977 2 6 er
: 2981 5 9 zählt 
: 2988 2 7 mir 
: 2992 4 6 von 
: 2998 4 4 dir 
- 3004
: 3018 4 6 je
: 3024 4 6 der 
: 3030 6 6 A
: 3038 2 4 tem
: 3042 4 2 zug 
: 3050 4 6 je
: 3056 2 6 der 
: 3060 4 4 Schritt
- 3066
: 3076 4 6 trägt 
: 3082 4 4 dei
: 3088 4 2 nen 
: 3094 8 2 Na
: 3104 4 -1 men 
: 3110 2 1 weit 
: 3114 3 2 mit 
: 3120 3 1 sich 
: 3124 6 -1 mit 
- 3132
: 3652 1 6 Die 
: 3654 2 6 Son
: 3658 4 6 ne 
: 3664 2 6 die 
: 3670 5 6 Ster
: 3677 2 6 ne 
: 3681 2 6 tra
: 3684 2 6 gen 
: 3688 3 6 Kun
: 3693 2 7 de 
: 3697 3 6 von 
: 3702 8 4 dir 
- 3712
: 3724 4 6 je
: 3730 2 6 der 
: 3736 2 6 Luft
: 3741 3 6 hauch 
: 3746 2 6 er
: 3752 4 9 zählt 
: 3758 2 7 mir 
: 3762 3 6 von 
: 3768 4 4 dir 
- 3774
: 3786 6 6 je
: 3794 2 6 der 
: 3798 6 6 A
: 3806 2 4 tem
: 3810 3 2 zug 
: 3818 4 6 je
: 3824 2 6 der 
: 3829 4 4 Schritt
- 3835
: 3844 4 6 trägt 
: 3852 4 4 dei
: 3858 4 2 nen 
: 3864 8 2 Na
: 3874 4 -1 men 
: 3882 4 1 weit 
: 3888 4 2 mit 
: 3896 4 1 sich 
: 3908 22 -1 mit 
E