#TITLE:Maman
#ARTIST:Louane
#MP3:Louane - Maman.mp3
#VIDEO:Louane - Maman.mp4
#COVER:Louane - Maman.jpg
#BPM:176,0
#GAP:3140
#ENCODING:UTF8
: 0 2 0 Les
: 3 7 7  amants
: 11 7 5  passent
: 19 2 3  de
: 22 2 3  lit
: 26 5 8  en
: 32 6 7  lit
- 49
: 69 2 3 Dans
: 72 2 3  les
: 75 3 3  ho
: 79 5 5 tels,
: 88 2 2  sur
: 91 3 2  les
: 95 3 3  par
: 99 4 0 kings
- 114
: 128 3 0 Pour
: 132 2 7  fuir
: 135 2 7  tout'
: 138 1 5  cette
: 142 12 3  mélan
: 155 3 8 co
: 160 4 7 lie
- 175
: 197 1 3 Le
: 199 2 3  coeur
: 202 1 3  des
: 204 7 5  villes,
: 214 4 2  la
: 219 2 2  mau
: 222 2 3 vaise
: 225 4 0  mine
- 239
: 246 2 0 Des
: 249 3 5  coups
: 253 2 7  de
: 256 6 8  blues,
: 263 2 7  des
: 266 3 5  coups
: 270 2 3  de
: 273 4 5  fil
- 277
: 278 2 3 Tout
: 282 3 3  re
: 286 3 5 com
: 290 3 7 men
: 294 4 5 c'ra
: 299 2 3  au
: 303 4 2  prin
: 308 3 3 temps
- 317
: 319 2 0 Sauf
: 322 2 5  les
: 325 2 3  a
: 328 3 2 mours
: 332 6 0  in
: 339 5 2 dé
: 345 8 3 lé
: 354 4 0 biles
- 370
: 374 2 0 Les
: 377 3 5  reves
: 381 4 7  s'en
: 386 4 8 tassent
: 391 3 7  dans
: 395 3 5  les
: 399 3 3  mé
: 403 4 5 tros
- 407
: 408 3 3 Les
: 412 2 3  grattes-
: 415 3 5 ciel
: 419 2 7  nous
: 422 1 5  re
: 424 4 3 gardent
: 429 4 2  de
: 434 4 3  haut
- 443
: 445 2 0 Comme
: 448 3 5  un
: 452 2 3  oi
: 456 4 2 seau
: 461 5 0  sous
: 467 6 2  les
: 475 5 3  bar
: 482 4 0 reaux
- 496
: 509 2 7 J'suis
: 512 2 8  pas
: 515 3 7  bien
: 519 3 5  dans
: 523 3 3  ma
: 527 6 5  tete,
: 540 6 7  ma
: 547 4 7 man
- 561
: 565 4 3 J'ai
: 570 3 3  per
: 574 3 3 du
: 578 2 5  le
: 582 2 3  gout
: 585 2 2  de
: 588 2 0  la
: 591 5 2  fete,
: 604 3 3  ma
: 609 4 0 man
- 623
: 634 2 5 Re
: 637 3 7 garde
: 642 2 8  comme
: 646 2 7  ta
: 650 3 5  fille
: 655 2 3  est
: 659 4 5  faite,
: 670 2 7  ma
: 673 3 7 man
- 687
: 700 3 3 J'trouve
: 704 2 5  pas
: 707 4 3  d'sens
: 712 2 2  a
: 715 3 0  ma
: 719 5 2  quete,
: 734 3 3  ma
: 738 4 0 man
- 753
: 769 2 0 A
: 772 4 7  l'heure
: 777 2 7  ou
: 780 2 5  les
: 783 3 3  bars
: 787 5 5  se
: 794 7 8  rem
: 803 8 7 plissent
- 822
: 839 2 3 Cette
: 842 2 3  meme
: 845 2 3  heure
: 848 3 5  ou
: 852 3 2  les
: 856 4 2  coeurs
: 861 3 3  se
: 865 4 0  vident
- 880
: 896 1 0 Ces
: 898 3 7  nuits
: 902 2 7  ou
: 905 2 5  les
: 908 4 3  pro
: 913 6 5 messes
: 921 8 8  se
: 930 12 7  tissent
- 953
: 967 2 3 Aus
: 970 1 3 si
: 972 3 3  vite
: 976 6 5  qu'elles
: 983 2 2  se
: 986 2 2  di
: 989 2 3 la
: 992 4 0 pident
- 1006
: 1014 2 0 Des
: 1017 3 5  coups
: 1021 2 7  de
: 1024 3 8  blues,
: 1028 4 7  des
: 1033 4 5  coups
: 1038 3 3  de
: 1042 3 5  fil
- 1045
: 1046 2 3 Tout
: 1050 3 3  re
: 1054 3 5 com
: 1058 3 7 men
: 1062 4 5 c'ra
: 1067 2 3  au
: 1071 4 2  prin
: 1076 3 3 temps
- 1085
: 1087 2 0 Sauf
: 1090 2 5  les
: 1093 2 3  a
: 1096 3 2 mours
: 1100 6 0  in
: 1107 5 2 dé
: 1113 8 3 lé
: 1122 4 0 biles
- 1138
: 1142 2 0 Les
: 1145 3 5  reves
: 1149 4 7  s'en
: 1154 4 8 tassent
: 1159 3 7  dans
: 1163 3 5  les
: 1167 3 3  mé
: 1171 4 5 tros
- 1175
: 1176 3 3 Les
: 1180 2 3  grattes-
: 1183 3 5 ciel
: 1187 2 7  nous
: 1190 1 5  re
: 1192 4 3 gardent
: 1197 4 2  de
: 1202 4 3  haut
- 1212
: 1214 2 0 Comme
: 1217 3 5  un
: 1221 2 3  oi
: 1225 4 2 seau
: 1230 5 0  sous
: 1236 6 2  les
: 1244 5 3  bar
: 1251 4 0 reaux
- 1265
: 1277 2 7 J'suis
: 1280 2 8  pas
: 1283 3 7  bien
: 1287 3 5  dans
: 1291 3 3  ma
: 1295 6 5  tete,
: 1308 6 7  ma
: 1315 4 7 man
- 1329
: 1333 4 3 J'ai
: 1338 3 3  per
: 1342 3 3 du
: 1346 2 5  le
: 1350 2 3  gout
: 1353 2 2  de
: 1356 2 0  la
: 1359 5 2  fete,
: 1372 3 3  ma
: 1377 4 0 man
- 1391
: 1402 2 5 Re
: 1405 3 7 garde
: 1410 2 8  comme
: 1414 2 7  ta
: 1418 3 5  fille
: 1423 2 3  est
: 1427 4 5  faite,
: 1438 2 7  ma
: 1441 3 7 man
- 1455
: 1468 3 3 J'trouve
: 1472 2 5  pas
: 1475 4 3  d'sens
: 1480 2 2  a
: 1483 3 0  ma
: 1487 5 2  quete,
: 1502 3 3  ma
: 1506 4 0 man
- 1533
: 1944 3 5 Re
: 1947 5 7 garde
: 1953 3 8  comme
: 1957 3 7  ta
: 1961 4 5  fille
: 1966 4 3  est
: 1971 16 5  faite,
: 1988 6 7  ma
: 1994 4 7 man
- 2009
: 2037 3 3 J'trouve
: 2041 7 5  pas
: 2049 4 3  d'sens
: 2054 4 2  a
: 2059 3 0  ma
: 2063 4 2  quete,
: 2082 5 3  ma
: 2087 2 0 man
E