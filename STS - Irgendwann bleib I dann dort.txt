#TITLE:Irgendwann bleib´I dann dort
#ARTIST:STS
#LANGUAGE:Deutsch
#GENRE:Austropop
#MP3:STS - Irgendwann bleib I dann dort.mp3
#COVER:STS - Irgendwann bleib I dann dort.jpg
#VIDEO:STS - Irgendwann bleib I dann dort.mkv
#PREVIEWSTART:50,394
#MedleyStartBeat:782
#MedleyEndBeat:949
#BPM:211,5
#GAP:1600
: 0 4 14 Da
: 4 4 14  letz
: 8 2 19  te
: 10 4 19  Som
: 14 6 21 mer
: 20 4 21  war
: 24 4 23  sehr
: 28 8 23  schen,
- 38
: 67 2 23 i
: 69 3 23  bin
: 72 2 23  in
: 75 5 24  ir
: 80 4 23 gen
: 84 4 21 da
: 88 4 19 na
: 92 11 21  Bucht
: 104 8 16  g'leg'n,
- 114
: 128 4 23 die 
: 132 6 24 Sunn
: 138 3 24  wie
: 141 5 24  Fei
: 146 4 23 er
: 150 3 21  auf
: 153 3 19  da
: 156 11 23  Haut,
- 169
: 194 4 23 du
: 198 5 23  riachst
: 203 2 23  des
: 205 5 24  Wass
: 210 4 23 er
: 214 5 19  und
: 219 9 21  nix
: 228 6 23  is
: 234 10 16  laut,
- 246
: 262 4 23 Irg
: 266 2 24 end
: 268 6 24 wo
: 274 3 23  in
: 277 5 21  Grie
: 282 4 19 chen
: 286 6 21 land,
- 292
: 292 6 24 je
: 298 2 24 de
: 300 4 24  Me
: 304 4 23 nge
: 308 6 21  weiss
: 314 2 19 er
: 316 8 21  Sand,
- 324
: 324 6 21 auf
: 330 4 23  mein
: 334 4 24  Ruck
: 338 2 23 en
: 340 6 19  nur
: 346 5 21  dei
: 351 9 19  Hand.
- 362
: 386 4 14 Nach
: 390 6 14  zwei,
: 396 4 19  drei
: 400 8 19  Wochen
: 408 2 21  hab
: 410 2 21  i's
: 412 7 23  g'spi
: 419 9 23 at,
- 430
: 455 3 23 i
: 458 4 23  hab
: 462 3 23  des
: 465 5 24  Le
: 470 4 23 bens
: 474 6 21 g'fuehl
: 480 2 19  do
: 482 3 21 rt
: 485 5 23  in
: 490 14 16 haliert,
- 506
: 522 4 24 die
: 526 2 24  Ged
: 528 6 24 ank
: 534 4 23 en
: 538 6 21  drah'n
: 544 3 19  se
: 547 9 23  um,
- 558
: 588 5 23 was
: 593 5 23  z'Haus
: 598 4 24  wicht
: 602 4 23 ig
: 606 4 21  war
: 610 4 19  is
: 614 4 21  jetzt
: 618 8 23  ganz
: 626 8 16  dumm,
- 636
: 650 4 24 du
: 654 6 24  sitzt
: 660 3 24  bei
: 663 5 24  an
: 668 2 23  O
: 670 6 21 li
: 676 3 19 ven
: 679 9 21 bam
- 688
: 688 4 24 und
: 692 2 24  du
: 694 8 24  spielst
: 702 3 23  di
: 705 3 21  mit
: 708 2 19  an
: 710 8 21  Stan,
- 718
: 718 4 21 es
: 722 2 21  is
: 724 2 23  so
: 726 4 24  an
: 730 6 23 ders
: 736 4 19  als
: 740 2 21  da
: 742 11 19 ham.
- 755
: 782 2 19 Und
* 784 4 19  ir
* 788 3 19 gend
* 790 7 29 wann
: 797 3 29  bleib
: 800 4 29  i
: 804 6 28  dann
: 810 8 28  dort,
- 820
: 825 4 24 lass
: 829 2 28  all
: 831 2 28 es
: 833 8 28  lieg'n
: 841 3 26  und
: 844 8 26  steh'n,
- 852
: 852 3 21 geh
: 855 2 21  vo
: 857 2 23  da
: 859 6 24 ham
: 865 4 24  fuer
: 869 2 24  im
: 871 2 23 mer
: 873 8 23  fort.
- 883
: 915 4 19 Da
: 919 4 19 rauf
* 923 3 29  gib
* 926 2 29  i
: 928 7 29  dir
: 935 4 28  mei
: 939 7 28  Wort,
- 946
: 946 3 24 wie
: 949 3 24 viel
: 952 6 28  Jahr
: 958 3 28  a
: 961 4 28  no
: 965 4 26  ver
: 969 7 26 geh`n,
- 977
: 977 4 21 ir
: 981 2 23 gend
: 983 6 24 wann
: 989 4 23  bleib
: 993 4 19  i
: 997 4 21  dann
: 1001 10 19  dort.
- 1013
: 1040 2 14 In
: 1042 3 14  uns'
: 1045 4 19 rer
: 1049 4 19  Hek
: 1053 2 21 to
: 1055 4 21 ma
: 1059 4 23 tik
: 1063 10 23 welt,
- 1075
: 1106 4 23 draht
: 1110 3 23  se
: 1113 3 24  al
: 1116 3 23 les
: 1119 6 21  nur
: 1125 4 19  um
: 1129 8 21  Macht
: 1137 6 23  und
: 1143 10 16  Geld,
- 1155
: 1167 2 24 Fi
: 1169 5 24 -nanz
: 1174 4 24  und
: 1178 4 24  Ban
: 1182 4 23 ken
: 1186 8 21  steig'n ma
: 1194 9 23  drauf,
- 1205
: 1234 3 23 de
: 1237 3 23  Rech
: 1240 6 23 -nung,
: 1246 2 24  de
: 1248 3 23  geht
: 1251 3 21  so
: 1254 3 19 wie
: 1257 7 21 so
: 1266 8 23  nie
: 1274 8 16  auf,
- 1284
: 1298 3 24 und
: 1301 4 24  ir
: 1305 2 24 gend
: 1307 5 24 wann
: 1312 6 23  fragst
: 1318 4 21  de,
: 1322 2 19  wie
: 1324 8 21 so
- 1334
: 1340 2 24 quael
: 1342 2 24  i
: 1344 2 24  mi
: 1346 4 24  da
: 1350 4 23  so
: 1354 6 21  schreck
: 1360 2 19 lich
: 1362 2 21  a
- 1364
: 1364 5 21 und
: 1369 4 21  bin
: 1373 3 23  net
: 1376 6 24  laengst
: 1382 4 23  scho
: 1386 4 19  was
: 1390 4 21  Gott
: 1394 10 19  wo.
- 1406
: 1820 4 14 A
: 1824 2 19 ber
: 1828 4 19  no
: 1832 4 21  is
: 1836 5 21  net
: 1841 2 23  so
: 1843 9 23  weit,
: 1880 5 23  noch
: 1885 4 23  was
: 1889 2 23  zu
: 1891 7 24  tun,
- 1898
: 1898 2 23 be
: 1900 6 21 fiehlt
: 1906 2 19  die
: 1908 4 21  Eit
: 1912 6 23 el
: 1918 10 16 keit.
- 1930
: 1948 5 23 Doch
: 1953 3 24  be
: 1956 2 24 vo
: 1958 4 24 r
: 1962 2 23  da
: 1964 5 21  Herz
: 1969 3 19 in
: 1972 10 23 farkt,
- 1984
: 2012 3 23 mi
: 2015 4 23  mit
: 2019 5 24  vierz
: 2024 4 23 ig
: 2028 3 21  in
: 2031 3 19  die
: 2034 6 21  Wind
: 2040 7 23 eln
: 2047 11 16  prackt,
- 2060
: 2072 4 24 lieg
: 2076 3 24  i
: 2079 4 24  scho
: 2083 5 24  ir
: 2088 4 23 gend
: 2092 4 21 wo
: 2096 2 19  am
: 2098 10 21  Strand,
- 2109
: 2110 4 24 a
: 2114 3 24  Bot
: 2117 3 24 tle
* 2120 4 24  Rot
* 2124 4 23 wein
: 2128 3 21  in
: 2131 2 19  da
: 2133 7 21  Hand,
- 2140
: 2140 3 21 und
: 2143 5 21  steck
: 2148 2 23  die
: 2150 7 24  Fiass
: 2157 3 23  in
: 2160 4 19  weis
: 2164 3 21 sen
: 2167 11 19  Sand.
- 2180
: 2204 2 19 Und
: 2206 4 19  ir
: 2210 3 19 gend
: 2212 7 29 wann
: 2219 3 29  bleib
: 2222 4 29  i
: 2226 6 28  dann
: 2232 8 28  dort,
- 2242
: 2244 4 24 lass
: 2248 2 28  al
: 2250 2 28 les
: 2252 8 28  lieg'n
: 2260 3 26  und
: 2263 8 26  steh'n,
- 2271
: 2271 3 21 geh
: 2274 2 21  vo
: 2276 2 23  da
: 2278 6 24 ham
: 2283 4 24  fuer
: 2287 3 24  im
: 2290 2 23 mer
: 2292 8 23  fort.
- 2302
: 2334 4 19 Da
: 2338 4 19 rauf
* 2342 2 29  gib
: 2344 3 29  i
: 2347 7 29  dir
: 2354 4 28  mei
: 2358 8 28  Wort,
- 2366
: 2366 2 24 wie
: 2368 3 24 viel
: 2371 6 28  Jahr
: 2377 3 28  a
: 2380 4 28  no
: 2384 4 26  ver
: 2388 8 26 geh'n,
- 2396
: 2396 4 21 ir
: 2400 3 23 gend
: 2403 5 24 wann
: 2408 4 23  bleib
: 2412 4 19  i
: 2416 4 21  dann
* 2420 8 19  dort.
- 2430
: 2460 3 19 Und
: 2463 4 19  ir
: 2467 3 19 gend
: 2470 5 29 wann
: 2475 4 29  bleib
: 2479 4 29  i
: 2483 4 28  dann
: 2487 8 28  dort,
- 2496
: 2498 4 24 lass
: 2502 3 28  al
: 2505 3 28 les
: 2508 7 28  lieg`n
: 2515 2 26  und
: 2517 8 26  steh'n,
- 2525
: 2525 4 21 geh
: 2529 3 21  vo
: 2532 2 23  da
: 2534 4 24 ham
: 2538 2 24  fuer
: 2540 3 24  im
: 2543 3 23 mer
: 2546 8 23  fort.
- 2556
: 2587 3 19 Da
: 2590 6 19 rauf
: 2596 2 29  gib
: 2598 2 29  i
: 2600 7 29  dir
: 2607 4 28  mei
: 2611 8 28  Wort,
- 2619
: 2619 3 24 wie
: 2622 2 24 viel
: 2624 7 28  Jahr
: 2631 4 28  a
: 2635 3 28  no
: 2638 3 26  ver
: 2641 8 26 geh'n,
- 2649
: 2649 3 21 ir
: 2652 4 23 gend
: 2656 6 24 wann
: 2662 4 23  bleib
: 2666 4 19  i
: 2670 4 21  dann
: 2674 10 19  dort.
E
