#TITLE:Stay the Night
#ARTIST:Zedd feat. Hayley Williams
#GENRE:Dance
#LANGUAGE:English
#YEAR:2013
#MP3:Zedd feat. Hayley Williams - Stay the Night.mp3
#COVER:Zedd feat. Hayley Williams - Stay the Night.jpg
#VIDEO:Zedd feat. Hayley Williams - Stay the Night.mp4
#VIDEOGAP:-1
#BPM:256.04
#GAP:490
#AUTHOR:Coppo
: 0 3 12 I
: 4 5 10  know
: 12 3 8  that
: 16 5 7  we
: 24 3 5  are
: 28 3 3  up
: 36 3 5 side
: 40 9 3  down
- 60
: 64 2 3 So
: 68 7 5  hold
: 76 2 3  your
: 80 3 3  tongue
- 86
: 88 2 8 And
: 92 7 5  hear
: 100 2 3  me
: 104 9 3  out
- 124
: 128 3 12 I
: 132 5 10  know
: 140 3 8  that
* 144 5 7  we
* 152 3 5  were
* 156 3 3  made
: 164 2 5  to
: 168 3 3  break
- 174
: 176 7 3 So
: 184 7 8  what
: 192 5 7  I
: 204 5 5  don't
: 216 11 0  mind
: 228 11 -2 ~
: 240 7 -4 ~
- 253
: 256 2 12 You
: 260 5 10  cue
: 268 3 8  the
: 272 5 7  lights
- 278
: 280 2 5 I'll
: 284 5 3  draw
: 292 2 5  the
: 296 9 3  blinds
- 316
: 320 2 3 Don't
: 324 7 5  dull
* 332 2 3  the
* 336 5 3  spar
* 344 3 8 kle
: 348 9 10  in
: 360 3 12  your
: 364 5 12  eyes
- 380
: 384 3 12 I
: 388 5 10  know
: 396 3 8  that
: 400 5 7  we
: 408 3 5  were
: 412 5 3  made
: 420 3 5  to
: 424 5 3  break
- 430
: 432 7 3 So
: 440 7 8  what
: 448 5 7  I
: 460 5 5  don't
: 472 11 0  mind
: 484 11 -2 ~
: 496 7 -4 ~
- 509
: 512 7 -4 Are
: 520 2 -4  you
: 524 7 3  gon
: 532 3 3 na
: 540 11 -2  stay
: 552 2 0  the
: 556 5 0  night
- 572
: 576 7 3 Are
: 584 2 3  you
: 588 7 5  gon
: 596 3 5 na
: 604 11 7  stay
: 616 2 8  the
: 620 5 8  night
- 636
: 640 3 12 Oh
: 644 5 10 ~
: 652 3 8 ~
: 656 5 7 ~
: 664 3 5 ~
: 668 5 3 ~
: 676 3 5 ~
: 680 5 3 ~
- 686
: 688 7 8 Are
: 696 5 5  you
: 704 11 3  gon
: 716 7 3 na
: 728 3 0  stay
: 732 9 -2 ~
: 744 5 -4  the
: 752 5 -4  night
- 765
: 768 7 -4 Are
: 776 2 -4  you
: 780 7 3  gon
: 788 3 3 na
: 796 11 -2  stay
: 808 2 0  the
: 812 5 0  night
- 828
: 832 6 3 Does
: 840 2 3 n't
: 844 7 5  mean
: 852 5 5  we're
: 860 11 7  bound
: 872 2 8  for
: 876 5 8  life
- 892
: 896 3 12 So
: 900 5 10 ~
: 908 3 8 ~
: 912 5 7 ~
: 920 3 5 ~
: 924 5 3 ~
: 932 3 5 ~
: 936 5 3 ~
- 942
: 944 7 8 Are
: 952 5 5  you
: 960 11 3  gon
: 972 7 3 na
* 984 3 0  stay
* 988 9 -2 ~
* 1000 5 -4  the
* 1008 5 -4  night
- 1047
: 1280 7 -4 Are
: 1288 2 -4  you
: 1292 7 3  gon
: 1300 3 3 na
: 1308 11 -2  stay
: 1320 2 0  the
: 1324 5 0  night
- 1340
: 1344 6 3 Does
: 1352 2 3 n't
: 1356 7 5  mean
: 1364 5 5  we're
: 1372 11 7  bound
: 1384 2 8  for
: 1388 5 8  life
- 1404
: 1408 3 12 So
: 1412 5 10 ~
: 1420 3 8 ~
: 1424 5 7 ~
: 1432 3 5 ~
: 1436 5 3 ~
: 1444 3 5 ~
: 1448 5 3 ~
- 1454
: 1456 7 8 Are
: 1464 5 5  you
: 1472 11 3  gon
: 1484 7 3 na
: 1496 3 0  stay
: 1500 9 -2 ~
: 1512 5 -4  the
: 1520 5 -4  night
- 1542
: 1568 3 12 I
: 1572 5 10  am
: 1580 2 8  a
: 1584 7 7  fire
: 1592 2 5 ~
: 1596 5 3  ga
: 1604 3 5 so
: 1608 7 3 line
- 1625
: 1632 2 3 Come
: 1636 7 5  pour
: 1644 2 3  your
: 1648 3 3 self
- 1654
: 1656 4 8 All
: 1662 5 5  o
: 1668 2 3 ver
: 1672 9 3  me
- 1692
: 1696 3 12 We'll
: 1700 5 10  let
: 1708 3 8  this
: 1712 5 7  place
: 1720 3 5  go
: 1724 5 3  down
: 1732 3 5  in
: 1736 5 3  flames
- 1742
: 1744 5 3 On
: 1752 7 8 ly
: 1760 5 7  one
* 1772 5 5  more
* 1784 11 0  time
* 1796 11 -2 ~
* 1808 7 -4 ~
- 1821
: 1824 2 12 You
: 1828 5 10  cue
: 1836 2 8  the
: 1840 5 7  lights
- 1846
: 1848 2 5 I'll
: 1852 7 3  draw
: 1860 2 5  the
: 1864 9 3  blinds
- 1884
: 1888 2 3 Don't
: 1892 7 5  dull
: 1900 2 3  the
: 1904 5 3  spar
: 1912 3 8 kle
: 1916 9 10  in
: 1928 2 12  your
: 1932 7 12  eyes
- 1948
: 1952 3 12 I
: 1956 5 10  know
: 1964 3 8  that
: 1968 5 7  we
: 1976 3 5  were
: 1980 5 3  made
: 1988 3 5  to
: 1992 5 3  break
- 1998
: 2000 3 3 So
: 2008 7 8  what
: 2016 7 10  I
* 2028 5 12  don't
* 2040 11 10  mind
* 2052 13 8 ~
- 2076
: 2080 7 -4 Are
: 2088 2 -4  you
: 2092 7 3  gon
: 2100 3 3 na
: 2108 11 -2  stay
: 2120 2 0  the
: 2124 5 0  night
- 2140
: 2144 7 3 Are
: 2152 2 3  you
: 2156 7 5  gon
: 2164 3 5 na
: 2172 11 7  stay
: 2184 2 8  the
: 2188 5 8  night
- 2204
: 2208 3 12 Oh
: 2212 5 10 ~
: 2220 3 8 ~
: 2224 5 7 ~
: 2232 3 5 ~
: 2236 5 3 ~
: 2244 3 5 ~
: 2248 5 3 ~
- 2254
: 2256 7 8 Are
: 2264 5 5  you
: 2272 11 3  gon
: 2284 7 3 na
: 2296 3 0  stay
: 2300 9 -2 ~
: 2312 5 -4  the
: 2320 5 -4  night
- 2333
: 2336 7 -4 Are
: 2344 2 -4  you
: 2348 7 3  gon
: 2356 3 3 na
: 2364 11 -2  stay
: 2376 2 0  the
: 2380 5 0  night
- 2396
: 2400 6 3 Does
: 2408 2 3 n't
: 2412 7 5  mean
: 2420 5 5  we're
* 2428 11 7  bound
* 2440 2 8  for
* 2444 5 8  life
- 2460
: 2464 3 12 So
: 2468 5 10 ~
: 2476 3 8 ~
: 2480 5 7 ~
: 2488 3 5 ~
: 2492 5 3 ~
: 2500 3 5 ~
: 2504 5 3 ~
- 2510
: 2512 7 8 Are
: 2520 5 5  you
: 2528 11 3  gon
: 2540 7 3 na
: 2552 3 0  stay
: 2556 9 -2 ~
: 2568 5 -4  the
: 2576 5 -4  night
- 2582
F 2584 5 -4 Night,
F 2592 5 -4  night,
F 2600 5 -4  night,
F 2608 5 -2  night
- 2614
F 2616 5 -2 Night,
F 2624 5 -2  night,
F 2632 5 -2  night,
F 2640 5 0  night
- 2646
F 2648 5 0 Night,
F 2656 5 0  night,
F 2664 5 0  night,
F 2672 5 2  night
- 2678
F 2680 5 2 Night,
F 2688 5 3  night,
F 2696 5 3  night,
F 2704 5 5  night
- 2710
F 2712 5 5 Night,
F 2720 2 6  night,
F 2724 2 6  night,
F 2728 1 6  night
- 2730
F 2732 2 6 Night,
F 2736 2 8  night,
F 2740 2 8  night,
F 2744 1 8  night
- 2746
F 2748 2 8 Night,
F 2752 2 8  night,
F 2756 2 8  night,
F 2760 2 8  night
- 2766
: 2768 7 8 Are
: 2776 5 5  you
: 2784 11 3  gon
: 2796 7 3 na
: 2808 3 0  stay
: 2812 9 -2 ~
: 2824 5 -4  the
: 2832 5 -4  night
- 2871
: 3104 7 -4 Are
: 3112 2 -4  you
: 3116 7 3  gon
: 3124 3 3 na
: 3132 11 -2  stay
: 3144 2 0  the
: 3148 5 0  night
- 3164
: 3168 6 3 Does
: 3176 2 3 n't
: 3180 7 5  mean
: 3188 5 5  we're
: 3196 11 7  bound
: 3208 2 8  for
: 3212 5 8  life
- 3228
: 3232 3 12 So
: 3236 5 10 ~
: 3244 3 8 ~
* 3248 5 7 ~
* 3256 3 5 ~
* 3260 5 3 ~
: 3268 3 5 ~
: 3272 5 3 ~
- 3278
: 3280 7 8 Are
: 3288 5 5  you
: 3296 11 3  gon
: 3308 7 3 na
: 3320 3 0  stay
: 3324 9 -2 ~
: 3336 5 -4  the
: 3344 5 -4  night
- 3357
F 3360 7 -4 Are
F 3368 2 -4  you
F 3372 7 3  gon
F 3380 3 3 na
F 3388 11 -2  stay
F 3400 2 0  the
F 3404 5 0  night
- 3420
F 3424 7 3 Are
F 3432 2 3  you
F 3436 7 5  gon
F 3444 3 5 na
F 3452 11 7  stay
F 3464 2 8  the
F 3468 5 8  night
- 3484
F 3488 3 12 Oh
F 3492 5 10 ~
F 3500 3 8 ~
F 3504 5 7 ~
F 3512 3 5 ~
F 3516 5 3 ~
F 3524 3 5 ~
F 3528 5 3 ~
- 3534
F 3536 7 8 Are
F 3544 5 5  you
F 3552 11 3  gon
F 3564 7 3 na
F 3576 3 0  stay
F 3580 9 -2 ~
F 3592 5 -4  the
F 3600 5 -4  night.
E
