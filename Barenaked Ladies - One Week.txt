#TITLE:One Week
#ARTIST:Barenaked Ladies
#MP3:Barenaked Ladies - One Week.mp3
#VIDEO:Barenaked Ladies - One Week [VD#0].avi
#COVER:Barenaked Ladies - One Week [CO].jpg
#BACKGROUND:Barenaked Ladies - One Week [CO].jpg
#BPM:340
#GAP:2338,24
#VIDEOGAP:0
#ENCODING:UTF8
#LANGUAGE:Englisch
#GENRE:Pop
#EDITION:SingStar '90s
#YEAR:1998
: 0 5 54 It's
: 7 5 57  been
- 14
: 20 4 61 One
: 27 4 61  week
: 33 2 59  since
: 36 2 57  you
: 39 3 59  looked
: 42 3 57  at
: 48 5 57  me
- 55
: 66 3 61 Cocked
: 72 2 61  your
: 75 2 61  head
: 78 2 61  to
: 81 2 61  the
: 85 4 62  side
- 90
: 90 2 57 And
: 94 2 57  said
: 96 2 57  ''I'm
: 99 4 59  an
: 104 5 57 gry''
- 111
: 117 3 61 Five
: 123 4 61  days
: 129 2 59  since
- 132
: 132 3 57 You
: 135 5 59  laughed
: 141 2 57  at
: 145 4 57  me
: 152 2 57  say
: 154 3 57 ing
- 158
: 159 2 61 ''Get
: 163 2 61  back
: 167 2 61  to
: 172 2 61 ge
: 175 2 61 ther
- 179
: 181 4 62 Come
: 186 3 57  back
: 192 3 57  and
: 195 4 59  see
: 201 5 57  me''
- 208
: 214 5 61 Three
: 220 4 61  days
: 226 3 59  since
: 229 3 57  the
: 232 2 59  liv
: 235 4 57 ing
* 241 5 57  room
- 248
: 254 2 54 I
: 257 2 61  re
: 259 2 61 a
: 262 2 61 lised
: 266 2 61  it's
: 269 2 61  all
: 271 2 61  my
: 277 5 62  fault
- 283
: 283 2 57 But
: 286 2 57  could
: 290 2 57 n't
: 292 5 59  tell
: 298 5 57  you
- 304
: 305 2 61 Yes
: 308 2 61 ter
: 312 4 61 day
: 323 2 59  you'd
: 326 3 57  for
: 330 2 59 gi
: 333 3 57 ven
: 338 4 57  me
- 343
: 344 2 54 But
: 347 2 57  it
: 349 3 55 'll
: 353 2 61  still
: 357 3 61  be
: 362 2 61  two
: 366 3 61  days
- 370
: 371 3 61 'Til
: 374 4 62  I
: 380 2 57  say
: 383 4 57  I'm
: 389 5 59  sor
: 395 6 57 ry
- 403
: 405 3 64 Hold
: 411 2 56  it
: 413 2 56  now
- 416
: 417 2 57 And
: 420 2 57  watch
: 422 2 57  the
: 425 3 57  Hood
: 432 4 61 wink
- 438
: 440 2 55 As
: 442 2 57  I
: 444 2 57  make
: 446 3 57  you
: 450 3 57  stop
: 456 2 61  think
- 460
: 465 2 57 You'll
: 468 2 57  think
: 471 3 57  you're
: 474 2 64  look
: 477 2 64 in'
- 480
: 480 3 62 At
: 485 2 61  A
: 488 2 61 qua
: 491 5 57 man
- 498
: 502 3 55 I
: 505 2 55  sum
: 508 2 57 mon
: 511 4 57  fish
: 517 2 57  to
: 520 2 57  the
: 523 3 57  dish
- 527
: 527 2 57 Al
: 529 2 57 though
: 532 2 57  I
: 535 2 57  like
: 538 2 57  the
: 541 2 57  Cha
: 544 2 57 let
: 547 2 57  Swiss
- 550
: 550 2 57 I
: 553 2 57  like
: 556 2 57  the
: 559 2 57  su
: 562 2 57 shi
- 565
: 565 2 57 'Cause
: 568 2 57  it's
: 571 2 64  ne
: 575 2 64 ver
- 578
: 578 2 61 Touched
: 581 2 62  a
: 584 2 59  fry
: 587 2 61 ing
: 590 4 57  pan
- 595
: 596 4 57 Hot
: 602 2 57  like
: 605 2 57  wa
: 608 2 57 sa
: 610 2 57 bi
- 613
: 613 3 57 When
: 616 3 57  I
: 620 4 57  bust
: 626 5 61  rhymes
- 633
: 636 2 57 Big
: 638 3 57  like
: 641 3 57  Le
: 644 4 57 Ann
* 649 4 61  Rimes
- 655
: 659 2 57 Be
: 662 2 57 cause
: 664 2 57  I'm
: 668 2 61  all
: 671 3 64  a
: 674 3 62 bout
: 680 4 61  va
: 685 5 57 lue
- 692
: 695 3 57 Bert
: 701 4 57  Kaemp
: 707 2 57 fert's
: 711 2 57  got
: 714 3 57  the
: 717 3 57  mad
: 722 3 61  hits
- 727
: 731 2 57 You
: 734 2 57  try
: 737 2 57  to
: 739 3 57  match
: 745 2 61  wits
- 749
: 755 2 57 You
: 759 2 57  try
: 762 3 57  to
: 765 2 64  hold
: 768 2 64  me
- 771
: 771 2 62 But
: 774 3 62  I
: 777 3 61  bust
: 783 5 57  through
- 789
: 791 2 57 Gon
: 793 2 57 na
: 795 2 57  make
: 797 2 57  a
: 800 2 57  break
: 804 2 57  and
: 807 2 57  take
: 810 2 57  a
: 813 2 57  fake
- 816
: 816 2 57 I'd
: 819 2 57  like
: 822 2 57  a
: 825 2 57  stink
: 828 2 57 in'
: 831 2 57  ach
: 834 2 57 in'
: 837 2 57  shake
- 840
: 840 2 57 I
: 843 2 57  like
: 846 2 57  va
: 849 2 57 nil
: 852 2 57 la
: 855 2 57  it's
: 858 2 57  the
: 862 2 64  fin
: 865 2 64 est
- 868
: 868 2 62 Of
: 870 2 62  the
: 873 2 61  fla
* 879 4 57 vours
- 885
: 889 1 57 Got
: 890 2 57 ta
: 892 2 57  see
: 895 2 57  the
: 898 2 57  show
- 901
: 901 2 57 'Cause
: 904 2 57  then
: 907 2 57  you'll
: 910 2 57  know
- 913
: 913 2 57 The
: 916 2 57  ver
: 919 2 57 ti
: 922 2 57 go
: 925 2 57  is
: 928 2 57  gon
: 931 2 57 na
: 934 2 57  grow
- 937
: 937 2 57 Cause
: 940 3 57  it's
: 943 2 57  so
: 946 3 57  dan
: 949 2 57 ge
: 952 2 57 rous
- 955
: 955 2 57 You'll
: 958 2 64  have
: 961 2 64  to
: 964 2 62  sign
: 966 2 62  a
: 970 3 61  wai
: 975 6 57 ver
- 983
: 985 2 57 How
: 987 2 64  can
: 991 2 64  I
: 994 2 64  help
: 997 2 64  it
: 1000 2 61  if
- 1003
: 1003 2 61 I
: 1006 2 59  think
: 1009 3 59  you're
: 1012 3 61  fun
: 1015 2 61 ny
- 1018
: 1018 2 59 When
: 1020 3 57  you're
: 1024 5 57  mad?
- 1031
: 1036 2 59 Try
: 1039 2 59 in'
: 1043 4 59  hard
: 1049 2 59  not
: 1052 2 57  to
: 1055 3 59  smile
- 1059
: 1060 2 59 Though
: 1063 2 61  I
: 1066 5 59  feel
: 1072 5 57  bad
- 1079
: 1085 2 64 I'm
: 1089 2 64  the
: 1092 2 64  kind
: 1095 3 64  of
: 1098 2 61  guy
- 1101
: 1101 2 61 Who
: 1104 4 62  laughs
: 1110 2 61  at
: 1113 2 61  a
: 1115 3 59  fu
: 1118 3 57 ne
: 1121 5 57 ral
- 1127
: 1128 3 59 Can't
: 1133 2 59  un
: 1136 3 59 der
: 1140 3 59 stand
: 1145 2 59  what
: 1148 4 57  I
: 1152 4 59  mean?
- 1157
: 1158 2 59 Well
: 1160 2 61  you
* 1164 4 59  soon
: 1169 5 57  will
- 1176
: 1180 3 57 I
: 1183 2 69  have
: 1185 2 69  the
: 1188 2 69  ten
: 1191 2 69 den
: 1193 2 64 cy
- 1196
: 1196 2 64 To
: 1200 2 62  wear
: 1203 2 62  my
: 1207 4 61  mind
: 1212 2 59  on
: 1215 2 57  my
: 1218 6 57  sleeve
- 1225
: 1227 2 57 I
: 1230 2 59  have
: 1233 2 59  a
: 1236 2 59  his
: 1239 2 59 to
: 1242 2 59 ry
: 1245 2 59  of
- 1248
: 1248 2 59 Tak
: 1252 2 57 ing
: 1254 2 59  off
: 1257 2 61  my
: 1260 4 59  shirt
- 1265
: 1266 3 54 It's
: 1271 6 57  been
- 1278
: 1280 3 61 One
: 1285 3 61  week
: 1290 2 59  since
: 1294 2 57  you
: 1296 2 59  looked
: 1300 3 57  at
: 1306 7 57  me
- 1315
: 1324 5 61 Threw
: 1331 2 61  your
: 1334 2 61  arms
: 1337 2 61  in
: 1339 2 61  the
: 1342 5 62  air
- 1348
: 1348 2 57 And
: 1351 2 57  said
: 1354 2 57  ''you're
: 1357 6 59  cra
: 1363 6 57 zy''
- 1371
: 1375 3 61 Five
: 1381 4 61  days
: 1387 3 59  since
: 1390 2 57  you
: 1393 3 59  tack
: 1396 4 57 led
: 1402 6 57  me
- 1410
: 1414 3 57 I've
: 1417 2 61  still
: 1421 2 61  got
: 1425 2 61  the
: 1430 2 61  rug
: 1434 3 61  burns
- 1438
: 1439 4 62 On
: 1444 2 57  both
: 1448 3 57  my
: 1453 5 59  knees
- 1459
: 1459 4 57 It's
: 1464 5 57  been
- 1471
: 1474 3 61 Three
: 1479 4 61  days
: 1485 2 59  since
: 1488 2 57  the
: 1490 3 59  af
: 1495 3 57 ter
: 1500 5 57 noon
- 1507
: 1511 2 54 You
: 1514 2 61  re
: 1517 2 61 a
: 1520 2 61 lised
: 1523 2 61  it's
: 1526 3 61  not
: 1529 3 61  my
: 1535 5 62  fault
- 1541
: 1541 2 57 Not
: 1543 1 57  a
: 1544 2 57  mo
: 1547 2 57 ment
: 1550 4 59  too
: 1556 5 57  soon
- 1562
: 1562 2 61 Yes
: 1565 2 61 ter
: 1568 4 61 day
: 1572 7 59 ~
- 1580
: 1580 2 59 You'd
: 1584 3 57  for
: 1587 3 59 gi
: 1590 5 57 ven
: 1596 7 57  me
- 1604
: 1604 2 54 And
: 1606 3 57  now
: 1609 2 54  I
: 1612 2 61  sit
: 1615 2 61  back
: 1620 3 61  and
: 1624 3 61  wait
- 1628
: 1629 3 61 'Til
: 1633 3 62  you
: 1637 3 57  say
: 1642 3 57  you're
: 1647 6 59  sor
: 1655 10 57 ry
- 1667
: 2047 2 57 Chi
: 2050 2 57 cki
: 2053 2 57 ty
: 2056 3 57  Chi
: 2062 3 57 na
- 2066
: 2068 2 57 The
: 2072 3 57  Chi
: 2077 2 57 nese
: 2083 2 57  chi
: 2087 2 61 cken
- 2090
: 2092 2 54 You
: 2095 2 57  have
: 2098 2 57  a
: 2101 3 57  drum
: 2107 2 57 stick
- 2111
: 2113 2 57 And
: 2116 2 57  your
: 2119 3 57  brain
: 2125 2 57  stops
: 2131 2 57  tick
: 2134 3 61 in'
- 2139
: 2144 2 57 Watch
: 2147 2 57 in'
: 2149 3 57  X
: 2155 2 57 Files
- 2159
: 2161 2 57 With
: 2164 2 57  no
: 2167 3 57  lights
: 2173 3 61  on
- 2178
: 2182 2 57 We're
: 2185 2 57  dans
: 2188 2 57  la
: 2191 3 57  mai
: 2197 2 61 son
- 2201
: 2206 2 57 I
: 2209 2 57  hope
: 2212 2 57  the
: 2216 2 64  Smok
: 2219 2 64 ing
: 2222 2 62  Man's
- 2225
: 2225 2 62 In
* 2228 2 61  this
: 2233 3 57  one
- 2237
: 2237 2 57 Like
: 2240 2 57  Har
: 2242 2 57 ri
: 2245 3 57 son
: 2252 2 57  Ford
- 2255
: 2255 2 57 I'm
: 2258 2 57  get
: 2261 2 57 ting
: 2264 3 57  fran
: 2270 3 61 tic
- 2275
: 2279 2 57 Like
: 2282 2 57  Sting
: 2285 3 57  I'm
: 2288 3 57  tan
: 2294 3 61 tric
- 2299
: 2303 2 57 Like
: 2306 2 57  Snick
: 2309 2 57 ers
- 2312
: 2312 2 64 Gua
: 2315 3 64 ran
: 2318 2 62 teed
: 2322 2 62  to
: 2325 2 61  sa
: 2327 2 61 tis
: 2330 4 57 fy
- 2336
: 2339 2 52 Like
: 2342 3 57  Ku
: 2345 2 57 ro
: 2348 2 57 sa
: 2351 2 57 wa
- 2354
: 2354 2 57 I
: 2357 2 57  make
: 2360 3 57  mad
: 2366 2 61  films
- 2370
: 2375 2 57 'Kay
: 2377 3 57  I
: 2380 2 57  dont
: 2384 3 57  make
: 2390 2 61  films
- 2394
: 2399 2 57 But
: 2402 2 57  if
: 2405 2 57  I
: 2408 2 64  did
- 2411
: 2411 2 64 They'd
: 2414 2 62  have
: 2417 2 62  a
: 2421 2 61  sa
: 2424 2 61 mu
: 2427 5 57 rai
- 2434
: 2436 2 57 Gon
: 2438 2 57 na
: 2440 2 57  get
: 2443 2 57  a
: 2446 2 57  set
: 2449 2 57  of
: 2452 2 57  bet
: 2455 2 57 ter
: 2458 2 57  clubs
- 2461
: 2461 1 57 Gon
: 2462 1 57 na
: 2464 2 57  find
: 2467 2 57  the
: 2470 2 57  kind
- 2473
: 2473 2 57 With
: 2476 2 57  ti
: 2479 2 57 ny
: 2482 2 57  nubs
- 2485
: 2485 2 57 Just
: 2488 2 57  so
: 2491 2 57  my
: 2494 3 57  irons
: 2497 2 57  aren't
: 2500 3 57  al
: 2503 2 57 ways
- 2506
: 2506 2 64 Fly
: 2509 2 64 ing
: 2511 2 62  off
: 2515 3 62  the
: 2518 3 61  back
: 2524 5 57 swing
- 2531
: 2533 1 52 Got
: 2534 1 52 ta
: 2537 2 57  get
: 2540 2 57  in
: 2542 2 57  tune
- 2545
: 2545 2 57 With
: 2548 2 57  Sai
: 2551 2 57 lor
: 2555 2 57  Moon
- 2558
: 2558 2 57 'Cause
: 2561 2 57  that
: 2563 2 57  car
: 2566 2 57 toon
: 2569 2 57  has
: 2573 2 57  got
: 2575 2 57  the
- 2578
* 2578 5 57 Boom
: 2585 2 57  a
: 2587 2 57 ni
: 2590 2 57 me
: 2593 3 57  babes
- 2597
: 2599 2 57 That
: 2602 3 64  make
: 2605 3 64  me
: 2609 2 62  think
: 2611 2 62  the
: 2615 5 61  wrong
: 2620 5 57  thing
- 2627
: 2629 3 57 How
: 2632 2 64  can
: 2635 2 64  I
: 2639 2 64  help
: 2642 3 64  it
: 2645 2 61  if
- 2648
: 2648 2 61 I
: 2651 2 62  think
: 2654 2 62  you're
: 2657 2 61  fun
: 2660 2 61 ny
- 2663
: 2663 3 59 When
: 2666 3 57  you're
: 2669 5 57  mad?
- 2676
: 2680 3 59 Try
: 2683 2 59 in'
: 2687 4 59  hard
: 2693 3 59  not
: 2696 2 57  to
* 2699 4 59  smile
- 2704
: 2705 2 61 Though
: 2708 3 61  I
: 2711 3 59  feel
: 2717 5 57  bad
- 2724
: 2729 2 64 I'm
: 2732 2 64  the
: 2736 3 64  kind
: 2739 3 64  of
: 2742 2 61  guy
- 2745
: 2745 2 61 Who
: 2748 3 62  laughs
: 2753 2 61  at
: 2756 2 61  a
: 2759 2 59  fu
: 2762 3 59 ne
: 2765 5 57 ral
- 2771
: 2771 4 59 Can't
: 2777 2 59  un
: 2780 2 59 der
: 2783 4 59 stand
: 2789 2 57  what
: 2792 3 57  I
: 2795 3 59  mean?
- 2800
: 2805 3 61 You
: 2808 6 59  soon
* 2814 5 57  will
- 2821
: 2823 2 57 I
: 2826 2 64  have
: 2829 2 64  the
: 2833 2 64  ten
: 2835 2 64 den
: 2839 2 61 cy
- 2842
: 2842 2 61 To
: 2845 3 62  wear
: 2848 2 62  my
: 2850 2 61  mind
: 2856 2 59  on
: 2859 3 59  my
: 2863 3 57  sleeve
- 2868
: 2872 2 57 I
: 2874 2 59  have
: 2878 2 59  a
: 2881 3 59  his
: 2884 2 59 to
: 2886 2 59 ry
: 2889 2 59  of
- 2892
: 2893 3 59 Los
: 2898 3 57 ing
: 2901 2 61  my
: 2905 3 59  shirt
- 2909
: 2910 4 54 It's
: 2915 4 57  been
- 2920
: 2922 3 61 One
: 2929 3 61  week
: 2935 2 59  since
: 2938 3 57  you
: 2941 2 59  looked
: 2944 3 57  at
: 2950 5 57  me
- 2957
: 2970 2 61 Dropped
: 2974 2 61  your
: 2978 2 61  arms
: 2981 2 61  to
: 2984 3 61  the
: 2987 4 62  sides
- 2992
: 2992 2 57 And
: 2995 2 57  said
: 2998 2 57  ''I'm
: 3001 4 59  sor
: 3007 5 57 ry''
- 3014
: 3019 3 61 Five
: 3025 4 61  days
: 3031 3 59  since
: 3035 2 57  I
: 3037 3 59  laughed
: 3043 2 54  at
: 3047 4 57  you
- 3052
: 3052 2 55 And
: 3056 3 57  said
- 3060
: 3062 2 61 ''You
: 3066 2 61  just
: 3071 2 61  did
: 3075 2 61  just
: 3080 2 61  what
: 3082 4 62  I
: 3088 2 57  thought
- 3091
: 3091 2 57 You
: 3094 2 57  were
: 3097 2 57  gon
: 3100 2 54 na
: 3103 3 57  do''
- 3108
: 3116 3 61 Three
* 3122 5 61  days
: 3128 3 59  since
: 3131 3 57  the
: 3134 2 59  liv
: 3137 3 57 ing
: 3143 5 57  room
- 3150
: 3154 2 54 We
: 3158 2 61  re
: 3161 2 61 a
: 3163 2 61 lised
: 3168 2 61  we're
: 3170 4 61  both
: 3176 2 61  to
: 3179 5 62  blame
- 3185
: 3185 2 57 But
: 3188 4 57  what
: 3192 2 57  could
: 3194 5 59  we
: 3200 5 55  do?
- 3206
: 3206 2 61 Yes
: 3210 2 61 ter
: 3213 3 61 day
- 3218
: 3225 2 59 You
: 3228 2 57  just
: 3231 5 59  smiled
: 3236 2 57  at
: 3240 4 57  me
- 3245
: 3245 3 54 'Cause
: 3248 2 57  it
: 3251 2 55 'll
: 3255 2 61  still
: 3258 3 61  be
: 3263 2 61  two
: 3267 3 61  days
- 3271
: 3273 3 61 'Til
: 3276 3 62  we
: 3282 2 57  say
: 3285 4 57  we're
: 3291 7 59  sor
: 3299 9 57 ry
- 3310
: 3345 2 54 It
: 3347 3 54 'll
: 3351 2 61  still
: 3355 2 61  be
: 3360 2 61  two
: 3364 2 61  days
- 3367
: 3369 3 61 'Til
: 3373 3 62  we
: 3378 3 57  say
: 3382 4 57  we're
* 3387 7 59  sor
: 3394 6 57 ry
- 3402
: 3442 2 54 It
: 3445 2 54 'll
: 3449 2 61  still
: 3453 2 61  be
: 3457 2 61  two
: 3461 3 61  days
- 3465
: 3466 3 61 'Til
: 3470 4 62  we
: 3475 2 57  say
: 3479 3 57  we're
: 3485 5 59  sor
: 3491 7 57 ry
- 3500
: 3545 2 61 Birch
: 3550 2 61 mount
: 3554 2 61  Sta
: 3557 2 61 di
: 3560 2 61 um
- 3564
: 3567 4 62 Home
: 3572 3 57  of
: 3578 4 57  the
: 3582 5 59  Rob
: 3589 5 57 bie
E