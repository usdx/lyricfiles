#TITLE:Symphony of destruction
#ARTIST:Megadeth
#MP3:Megadeth - Symphony of destruction.mp3
#VIDEO:Megadeth - Symphony of destruction.mpg
#COVER:Megadeth - Symphony of destruction [CO].jpg
#BPM:139,93
#GAP:14000
#VIDEOGAP:-1,3
#ENCODING:UTF8
#EDITION:UltraStar Metal
#START:1,3
: 57 3 2 You
: 60 4 2  take
: 64 3 4  a
: 67 3 2  mor
: 70 3 4 tal
: 73 4 2  man,
- 97
: 121 4 5 And
: 125 4 2  puthim
: 129 4 4  in
: 133 4 7  con
: 137 4 7 trol
- 161
: 185 4 4 Watch
: 189 4 11  im
: 193 2 11  be
: 195 3 9 come
: 198 3 11  a
: 201 4 10  god,
- 224
: 248 5 11 Watch
: 253 2 2  peo
: 255 2 4 ples
: 257 4 2  heads
: 261 5 7  a'
: 266 4 7 roll,
- 281
: 297 4 2 A'
: 301 11 9 roll
: 329 3 7  A'
: 332 44 7 roll
- 395
: 403 6 12 Just
: 409 4 14  like
: 413 2 13  the
: 415 6 13  Pied
: 421 6 12  Pi
: 427 4 9 per
- 431
: 435 6 6 Led
: 441 6 12  rats
: 447 6 9  through
: 453 2 14  the
: 455 4 11  streets
- 460
: 465 8 9 Dance
: 473 6 4  like
: 479 2 14  ma
: 481 2 12 ri
: 483 2 14 o
: 485 4 12 nettes,
- 492
: 499 1 11 Swa
: 500 5 12 ying
: 505 4 12  to
: 509 2 9  the
: 511 2 9  Sym
: 513 2 14 pho
: 515 4 14 ny
- 519
: 521 4 14 Of
: 525 2 4  Des
: 527 6 2 truc
: 533 4 7 tion
- 551
: 569 7 2 Acting
: 576 3 2  like
: 579 3 2  a
: 582 3 4  ro
: 585 4 2 bot,
- 609
: 633 4 5 Its
: 637 4 2  metal
: 641 4 4  brain
: 645 4 7  co
: 649 4 7 rrodes
- 673
: 697 4 4 You
: 701 4 11  try
: 705 2 11  to
: 707 3 9  take
: 710 3 11  its
: 713 4 10  pulse,
- 736
: 760 1 11 Be
: 761 4 2 fore
: 765 2 4  the
: 767 2 4  he
: 769 4 2 ad
: 773 5 7  ex
: 778 4 7 plodes
- 793
: 809 4 2 Ex
: 813 8 9 plodes
: 840 4 7  Ex
: 844 50 7 plodes
- 907
: 915 6 12 Just
: 921 4 14  like
: 925 2 13  the
: 927 6 13  Pied
: 933 6 12  Pi
: 939 4 9 per
- 943
: 947 6 6 Led
: 953 6 12  rats
: 959 6 9  through
: 965 2 14  the
: 967 4 11  streets
- 972
: 977 8 9 Dance
: 985 6 4  like
: 991 2 14  ma
: 993 2 12 ri
: 995 2 14 o
: 997 4 12 nettes,
- 1004
: 1011 6 11 Swaying
: 1017 4 12  to
: 1021 2 9  the
: 1023 2 9  Sym
: 1025 2 14 pho
: 1027 4 14 ny
- 1035
: 1043 6 14 Just
: 1049 4 14  like
: 1053 2 13  the
: 1055 6 13  Pied
: 1061 6 12  Pi
: 1067 4 9 per
- 1071
: 1075 6 6 Led
: 1081 6 12  rats
: 1087 6 9  through
: 1093 2 14  the
: 1095 4 11  streets
- 1100
: 1105 8 9 Dance
: 1113 6 4  like
: 1119 2 14  ma
: 1121 2 12 ri
: 1123 2 14 o
: 1125 4 12 nettes,
- 1132
: 1139 1 11 Swa
: 1140 5 12 ying
: 1145 4 12  to
: 1149 2 9  the
: 1151 2 9  Sym
: 1153 2 14 pho
: 1155 4 14 ny
- 1163
: 1171 1 14 Swa
: 1172 5 12 ying
: 1177 4 12  to
: 1181 2 9  the
: 1183 2 9  Sym
: 1185 2 14 pho
: 1187 4 14 ny
- 1191
: 1194 2 14 Of
: 1196 3 16  Des
: 1199 3 14 truc
: 1202 4 16 tion
- 1349
: 1497 3 16 The
: 1500 3 2  earth
: 1503 2 4  s
: 1505 3 2 tarts
: 1508 1 4  to
: 1510 8 2  rumble
- 1540
: 1564 5 5 World
: 1569 3 2  po
: 1572 5 4 wers
: 1577 11 7  fall
- 1599
: 1625 8 7 A'wa
: 1633 2 11 rring
: 1635 3 11  for
: 1638 3 9  the
: 1641 4 11  hea
: 1645 7 10 vens,
- 1665
: 1689 2 11 A
: 1691 2 2  pea
: 1693 2 4 ce
: 1695 2 4 ful
: 1697 4 2  man
: 1701 6 7  stands
: 1709 7 7  tall
- 1723
: 1740 11 2 Tall
: 1772 43 7  Tall
- 1835
: 1843 6 12 Just
: 1849 4 14  like
: 1853 2 13  the
: 1855 6 13  Pied
: 1861 6 12  Pi
: 1867 4 9 per
- 1871
: 1874 7 6 Led
: 1881 5 12  rats
: 1886 7 9  through
: 1893 1 14  the
: 1894 4 11  streets
- 1899
: 1905 8 9 Dance
: 1913 6 4  like
: 1919 1 14  ma
: 1920 3 12 ri
: 1923 1 14 o
: 1924 4 12 nettes,
- 1931
: 1939 1 11 Swa
: 1940 4 12 ying
: 1944 5 12  to
: 1949 2 9  the
: 1951 2 9  Sym
: 1953 2 9 pho
: 1955 4 9 ny
- 1962
: 1970 7 14 Just
: 1977 4 14  like
: 1981 1 13  the
: 1982 7 13  Pied
: 1989 5 12  Pi
: 1994 4 9 per
- 1998
: 2003 6 6 Led
: 2009 6 12  rats
: 2015 5 9  through
: 2020 3 14  the
: 2023 4 11  streets
- 2027
: 2032 8 9 Dance
: 2040 7 4  like
: 2047 2 14  ma
: 2049 2 12 ri
: 2051 2 14 o
: 2053 4 12 nettes,
- 2059
: 2066 2 11 Swa
: 2068 5 12 ying
: 2073 4 12  to
: 2077 1 9  the
: 2078 3 9  Sym
: 2081 1 9 pho
: 2082 4 9 ny
- 2090
: 2099 1 9 Swa
: 2100 5 7 ying
: 2105 3 7  to
: 2108 3 6  the
: 2111 1 9  Sym
: 2112 3 14 pho
: 2115 4 14 ny
- 2119
: 2122 2 9 Of
: 2124 3 16  Des
: 2127 5 14 truc
: 2132 2 16 tion
E