#TITLE:Uhn Tiss Uhn Tiss Uhn Tiss
#ARTIST:Bloodhound Gang
#LANGUAGE:German
#MP3:Bloodhound Gang - Uhn Tiss Uhn Tiss Uhn Tiss.mp3
#COVER:Bloodhound Gang - Uhn Tiss Uhn Tiss.jpg
#VIDEO:Bloodhound Gang - Uhn Tiss Uhn Tiss Uhn Tiss.flv
#BPM:268
#GAP:480
: 802 2 7 Dog 
: 807 2 5 will 
: 812 4 7 hunt 
- 817
: 818 2 7 I'm 
: 821 2 5 the 
: 826 4 10 front 
: 832 4 7 end 
: 840 4 5 loa
: 846 2 7 der 
- 850
: 853 2 7 Tra
: 857 2 10 vol
: 862 4 7 tin' 
: 870 4 5 o
: 878 4 7 ver 
- 883
: 885 2 7 So 
: 889 2 10 try 
: 893 2 7 my 
: 897 6 7 slam 
: 905 2 5 on 
: 909 2 7 for 
: 913 5 7 size 
- 920
: 929 5 7 Drive-
: 938 2 7 stick 
- 942
: 945 2 7 With 
: 949 2 5 that 
: 953 2 10 kung-
: 959 3 7 fu 
: 966 4 5 grip 
- 971
: 973 2 7 Let 
: 977 2 7 the 
: 981 2 5 ba
: 985 2 10 na
: 989 3 7 na 
: 996 4 5 split 
- 1002
: 1005 3 7 And 
: 1015 3 10 watch 
: 1021 2 7 it 
: 1025 2 7 go 
: 1028 2 7 right 
: 1032 2 5 to 
: 1036 2 7 your 
: 1040 6 7 tighs 
- 1048
: 1056 3 7 Cop 
: 1060 2 5 a 
: 1064 4 7 feel 
- 1072
: 1072 3 7 Cop
: 1076 2 7 per
: 1080 5 10 field 
: 1088 5 7 style 
- 1094
: 1096 3 5 Ab
: 1100 3 7 ra
: 1104 3 7 ca
: 1108 4 10 da
: 1116 4 7 bra 
: 1124 4 5 that 
: 1131 4 7 bra 
- 1136
: 1136 1 7 Do 
: 1139 2 5 you 
: 1143 3 10 think 
- 1147
: 1148 3 7 I 
: 1152 2 5 can 
: 1156 4 5 pull 
: 1163 2 7 it 
: 1167 4 7 off 
- 1173
: 1183 2 7 Wan
: 1187 2 5 na 
: 1192 3 7 bang 
: 1196 3 7 a
: 1204 4 10 round 
- 1209
: 1211 2 7 Just 
: 1215 2 7 jot 
: 1219 2 5 me 
: 1223 3 7 down 
- 1226
: 1226 3 7 On 
: 1231 3 7 your 
: 1235 2 7 to-
: 1240 4 10 do 
: 1248 4 3 list 
- 1254
: 1258 2 0 Un
: 1262 3 0 der 
: 1267 2 2 "put 
: 1270 2 3 out 
: 1274 3 7 like 
: 1278 3 7 a 
: 1283 3 7 fire" 
- 1290
: 1290 6 7 'cause 
* 1302 4 7 I 
: 1311 3 0 got 
: 1318 2 0 some
: 1323 3 0 thin' 
- 1330
* 1330 2 0 And 
: 1334 2 7 it 
: 1339 4 5 goes 
: 1346 5 7 thum
: 1354 2 7 pin' 
* 1358 5 10 like 
: 1366 4 3 this 
- 1372
: 1421 2 0 All 
: 1425 2 0 you 
* 1430 8 7 need 
: 1442 6 0 is 
- 1450
: 1457 2 0 My 
: 1461 2 -2 uhn 
: 1466 4 -2 tiss 
: 1473 2 -2 uhn 
: 1478 4 -2 tiss 
: 1484 6 -2 uhn 
: 1494 4 -2 tiss 
- 1500
* 1556 4 7 I 
: 1565 2 0 got 
: 1572 2 0 some
: 1577 2 0 thin' 
- 1581
* 1584 2 0 And 
: 1588 2 7 it 
: 1593 3 5 goes 
: 1600 6 7 thum
: 1608 2 7 pin' 
: 1612 4 10 like 
* 1620 4 12 this 
- 1626
: 1676 2 12 All 
: 1680 2 12 you 
* 1684 8 15 need 
: 1698 4 12 is 
- 1704
: 1710 2 12 My 
: 1716 2 10 uhn 
: 1720 4 10 tiss 
: 1728 2 10 uhn 
: 1732 4 10 tiss 
: 1740 4 7 uhn 
* 1748 4 0 tiss 
- 1754
- 1812
: 2072 3 7 E
: 2077 2 5 di
: 2081 3 7 ble 
: 2089 4 7 strange 
- 2097
: 2097 3 10 How 
: 2102 3 7 do 
: 2108 4 5 I 
: 2116 4 7 get 
- 2122
: 2128 2 10 In 
: 2132 4 7 your 
: 2140 4 5 pants 
: 2148 4 7 when 
- 2154
: 2160 2 10 You're 
: 2166 4 7 tick 
: 2174 4 5 to
: 2180 2 7 ckin' 
: 2186 4 7 them 
- 2192
: 2200 2 7 Se
: 2204 2 5 ri
: 2207 2 7 ous 
: 2212 4 7 le
: 2220 4 10 vis 
- 2226
: 2232 4 7 So 
: 2239 2 5 tight 
: 2243 2 5 can't 
: 2247 2 7 be 
: 2252 3 10 clas
: 2260 4 7 si
: 2268 4 5 fied 
- 2273
: 2274 2 7 That's 
: 2279 2 7 why 
: 2283 2 5 I'm 
: 2288 2 10 here 
- 2291
: 2291 2 7 To 
: 2295 3 7 fill 
: 2299 2 5 that 
: 2302 2 5 o
: 2306 2 7 pe
: 2312 4 7 ning 
- 2318
: 2326 2 7 Make 
: 2330 2 5 a 
: 2335 2 7 sea
: 2340 4 7 soned 
: 2348 4 10 pass 
- 2354
: 2360 2 7 To 
: 2366 3 5 mount 
: 2372 4 7 that 
: 2380 4 10 ass 
- 2386
: 2392 4 7 Bob 
: 2398 2 7 hope 
: 2402 2 7 that 
: 2406 2 7 i 
: 2410 2 7 might 
- 2413
: 2414 2 10 One 
: 2418 4 7 night 
: 2426 4 5 stand 
: 2433 2 7 a 
: 2438 4 7 chance 
- 2444
: 2453 2 7 Let's 
: 2457 2 5 go 
: 2461 2 7 feng 
: 2466 4 7 shui the 
: 2475 4 10 fuck 
- 2480
: 2482 2 7 A
: 2485 2 7 round 
: 2490 2 7 my 
: 2494 6 7 digs 
- 2501
: 2502 2 7 Like 
: 2505 2 7 a 
: 2510 2 10 su
: 2514 2 10 per
: 2519 4 3 ball 
- 2525
: 2533 2 0 Bring 
: 2537 2 0 that 
: 2541 2 3 sun
: 2545 2 7 ny 
: 2549 2 7 side 
: 2554 4 7 up 
- 2559
: 2561 6 5 And 
* 2573 4 7 I 
: 2582 3 0 got 
: 2589 2 0 some
: 2594 3 0 thin' 
- 2601
* 2601 2 0 And 
: 2605 2 7 it 
: 2610 4 5 goes 
: 2617 5 7 thum
: 2625 2 7 pin' 
* 2629 4 10 like 
: 2637 4 3 this 
- 2643
: 2692 2 0 All 
: 2696 2 0 you 
* 2701 8 7 need 
: 2713 6 0 is 
- 2721
: 2728 2 0 My 
: 2732 2 -2 uhn 
: 2737 4 -2 tiss 
: 2744 2 -2 uhn 
: 2749 4 -2 tiss 
: 2755 6 -2 uhn 
: 2765 4 -2 tiss 
- 2771
* 2826 4 7 I 
: 2835 2 0 got 
: 2842 2 0 some
: 2847 2 0 thin' 
- 2851
* 2854 2 0 And 
: 2858 2 7 it 
: 2863 3 5 goes 
: 2870 6 7 thum
: 2878 2 7 pin' 
: 2882 4 10 like 
* 2890 4 12 this 
- 2896
: 2946 2 12 All 
: 2950 2 12 you 
* 2954 8 15 need 
: 2968 4 12 is 
- 2974
: 2980 2 12 My 
: 2986 2 10 uhn 
: 2990 4 10 tiss 
: 2998 2 10 uhn 
: 3002 4 10 tiss 
: 3010 4 7 uhn 
* 3018 4 0 tiss 
- 3024
- 3080
- 3112
- 3138
- 3176
- 3202
- 3240
- 3266
- 3302
- 3328
- 3366
- 3392
- 3430
- 3456
- 3494
- 3520
- 3556
- 3582
* 3589 4 7 I 
: 3598 3 0 got 
: 3605 2 0 some
: 3610 3 0 thin' 
- 3617
* 3617 2 0 And 
: 3621 2 7 it 
: 3626 4 5 goes 
: 3633 5 7 thum
: 3641 2 7 pin' 
* 3645 4 10 like 
: 3653 4 3 this 
- 3659
: 3709 2 0 All 
: 3713 2 0 you 
* 3718 8 7 need 
: 3730 6 0 is 
- 3738
: 3744 2 0 My 
: 3748 2 -2 uhn 
: 3753 4 -2 tiss 
: 3760 2 -2 uhn 
: 3765 4 -2 tiss 
: 3771 6 -2 uhn 
: 3781 4 -2 tiss 
- 3787
* 3844 4 7 I 
: 3853 2 0 got 
: 3860 2 0 some
: 3865 2 0 thin' 
- 3871
* 3871 2 0 And 
: 3875 2 7 it 
: 3880 3 5 goes 
: 3887 6 7 thum
: 3895 2 7 pin' 
: 3899 4 10 like 
* 3907 4 12 this 
- 3913
: 3962 2 12 All 
: 3966 2 12 you 
* 3970 8 15 need 
: 3984 4 12 is 
- 3990
: 3997 2 12 My 
: 4003 2 10 uhn 
: 4007 4 10 tiss 
: 4015 2 10 uhn 
: 4019 4 10 tiss 
: 4027 4 7 uhn 
* 4035 4 0 tiss 
- 4041
- 4098
- 4130
- 4156
- 4194
- 4220
- 4258
- 4284
- 4320
- 4346
- 4384
- 4410
- 4448
- 4474
- 4512
- 4538
- 4576
E
