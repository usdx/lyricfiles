#TITLE:Theme
#ARTIST:Batman
#MP3:Batman - Theme.mp3
#COVER:Batman - Theme [CO].jpg
#BACKGROUND:Batman - Theme [BG].jpg
#BPM:156
#GAP:36872
#ENCODING:UTF8
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
: 0 2 19 Bat
: 4 7 19 man!
- 16
: 32 2 19 Bat
: 36 7 19 man!
- 49
: 65 2 24 Bat
: 69 7 24 man!
- 81
: 97 2 19 Bat
: 101 7 19 man!
- 113
* 129 3 26 Bat
* 134 7 26 man!
- 145
: 145 3 24 Bat
: 150 7 24 man!
- 162
: 162 2 19 Bat
: 166 7 19 man!
- 178
* 194 3 26 Bat
* 199 7 26 man!
- 210
: 210 3 24 Bat
: 215 7 24 man!
- 227
: 227 2 19 Bat
: 231 7 19 man!
- 241
: 252 1 24 Da
: 254 1 24 da
: 256 1 25 da
: 258 1 25 da
: 260 1 26 da
: 262 1 26 da
: 264 1 25 da
: 266 1 25 da
: 268 1 24 da
: 270 1 24 da
: 272 1 25 da
: 274 1 25 da,
* 276 2 26  da!
- 282
: 285 2 31 Bat
* 289 21 31 man!
E