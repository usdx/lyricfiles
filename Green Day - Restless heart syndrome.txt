#TITLE:Restless heart syndrome
#ARTIST:Green Day
#EDITION:UltraStar Green Day
#MP3:Green Day - Restless heart syndrome.mp3
#COVER:Green Day - Restless heart syndrome [CO].jpg
#BACKGROUND:Green Day - Restless heart syndrome [BG].jpg
#BPM:232
#GAP:13735
: 0 5 2 I've
: 6 4 0  got
: 10 5 -5  a
: 16 4 -3  real
: 20 5 -2 ly
: 26 5 -3  bad
: 31 5 -2  dis
: 37 25 -5 ease
- 65
: 89 5 -5 It's
: 95 5 -5  got
: 101 5 -3  me
: 107 5 -2  beg
: 113 3 0 ging
: 116 7 2  On
: 124 5 -2  my
: 130 5 0  hands
: 136 11 -2  and
: 147 21 -5  knees
- 168
: 169 5 -5 So
* 174 11 7  take
* 186 11 5  me
* 198 17 2  to
: 215 5 -2  e
: 221 5 0 mer
: 227 11 -2 gen
: 238 23 -5 cy
- 264
: 271 5 -5 'cause
: 277 5 -5  some
: 283 3 -3 thing
: 286 8 -2  seems
: 294 2 0  to
: 296 5 2  be
: 303 5 -2  miss
: 308 25 -5 ing
- 336
: 362 5 2 Some
: 368 5 0 bo
: 374 5 -5 dy
: 379 5 -3  take
: 385 2 -2  the
: 387 5 -3  pain
: 393 5 -2  a
: 399 25 -5 way
- 427
: 453 5 -5 It's
: 459 5 -5  like
: 464 2 -3  an
: 466 5 -2  ul
: 472 5 0 cer
: 478 7 2  bleed
: 486 5 -2 ing
: 492 5 0  in
: 497 11 -2  my
: 509 19 -5  brain
- 530
: 532 5 -5 So
: 538 9 7  send
: 548 11 5  me
: 559 17 2  to
: 577 5 -2  the
: 582 5 0  phar
: 588 11 -2 ma
: 600 23 -5 cy
- 626
: 633 5 -5 So
: 638 5 -5  I
: 644 5 -3  can
: 650 5 -2  lose
: 656 4 0  my
: 660 5 2  mem
: 666 5 -2 o
: 671 25 -5 ry
- 711
: 729 5 0 I'm
* 735 5 3  e
* 741 11 7 la
* 753 5 5 t
* 758 11 3 ed
- 772
: 776 4 0 Med
: 780 5 3 i
: 785 11 7 ca
: 797 5 5 t
: 803 11 3 ed
- 814
: 814 5 7 Lord
: 820 5 5  knows
: 826 5 0  I
: 832 5 2  tried
: 838 2 3  to
: 840 5 2  find
: 845 7 3  a
: 855 23 0  way
- 880
: 882 5 0 to
: 888 5 0  run
: 894 4 0  a
* 898 77 2 way
- 978
: 996 5 2 I
: 1002 5 0  think
: 1008 4 -5  they
: 1012 5 -3  found
: 1017 4 -2  an
: 1021 5 -3 oth
: 1027 5 -2 er
: 1033 25 -5  cure
- 1061
: 1087 4 -5 For
: 1091 5 -5  bro
: 1097 5 -3 ken
: 1102 5 -2  hearts
- 1107
: 1108 4 0 and
: 1112 7 2  feel
: 1120 5 -2 ing
: 1126 5 0  in
: 1131 11 -2 se
: 1143 23 -5 cure
- 1166
: 1166 4 -5 You'd
* 1170 11 7  be
* 1182 11 5  sur
* 1193 17 2 prised
- 1210
: 1211 5 -2 what
: 1217 5 0  I
: 1222 11 -2  en
: 1234 23 -5 dure
- 1260
: 1267 5 -5 What
: 1273 5 -5  makes
: 1278 5 -3  you
: 1284 5 -2  feel
- 1289
: 1290 2 0 so
: 1292 5 2  self
: 1298 5 -2  as
: 1304 25 -5 sured?
- 1332
: 1358 5 2 I
: 1363 5 0  need
: 1369 5 -5  to
: 1375 5 -3  find
: 1381 2 -2  a
: 1383 5 -3  place
: 1389 5 -2  to
: 1398 25 -5  hide
- 1426
: 1449 5 -5 You
: 1454 5 -5  nev
: 1460 5 -3 er
: 1466 5 -2  know
: 1472 4 0  what
: 1476 5 2  could
: 1481 5 -2  be
- 1486
: 1487 5 0 Wait
: 1493 4 -2 ing
: 1497 7 -5  out
: 1505 23 -5 side
- 1528
: 1528 5 -5 The
* 1534 11 7  ac
* 1545 9 5 ci
* 1555 7 2 dents
- 1565
: 1572 5 -2 that
: 1578 5 0  you
: 1584 11 -2  could
: 1595 23 -5  find
- 1621
: 1630 4 -5 It's
: 1634 5 -5  like
: 1640 3 -3  some
: 1643 8 -2  kind
: 1652 3 0  of
: 1655 5 2  su
: 1661 5 -2 i
: 1667 25 -5 cide
- 1707
: 1725 5 0 So
: 1731 5 3  what
* 1737 11 7  ails
* 1748 17 5  you
- 1766
: 1768 3 0 is
: 1771 5 0  what
* 1777 5 3  im
* 1783 9 7 pales
* 1793 5 5  you
* 1798 11 3 ~
- 1809
: 1810 5 7 I
: 1816 5 5  feel
: 1822 5 0  like
: 1827 5 2  i've
: 1833 2 3  been
: 1835 5 2  cru
: 1841 7 3 ci
: 1851 23 0 fied
- 1875
: 1876 2 0 to
: 1878 5 0  be
: 1884 3 0  sat
: 1887 5 0 is
* 1893 79 2 fied
- 1975
: 1998 5 -5 I'm
: 2003 5 -2  a
: 2009 9 2  vic
: 2019 5 0 tim
: 2025 11 -2 ~
- 2039
: 2042 5 -5 of
: 2048 5 -2  my
: 2054 11 2  symp
: 2065 5 0 tom
: 2071 11 -2 ~
- 2082
: 2083 5 -5 I
: 2088 5 -5  am
: 2094 4 -2  my
: 2098 5 2  own
: 2104 5 0  worst
: 2110 5 0  en
: 2116 3 -2 e
: 2119 25 0 my
: 2145 22 -2 ~
- 2170
: 2177 5 -5 You're
: 2183 5 -2  a
: 2189 11 2  vic
: 2201 5 0 tim
: 2206 11 -2 ~
- 2220
: 2224 5 -5 of
: 2230 5 -2  your
: 2235 11 2  symp
: 2247 4 0 tom
: 2251 11 -2 ~
- 2262
: 2262 5 -5 You
: 2268 5 -5  are
: 2274 5 -2  your
* 2280 5 2  own
* 2286 5 0  worst
* 2291 5 0  en
* 2297 2 -2 e
* 2299 25 0 my
- 2325
: 2326 4 -2 Know
: 2330 5 -3  your
: 2336 5 -3  en
: 2342 4 -2 e
: 2346 19 -5 my
- 2395
: 2707 5 -5 I'm
: 2713 5 -2  e
* 2719 11 2 la
: 2730 4 0 ~
: 2734 11 -2 ted
- 2747
: 2750 5 -5 Med
: 2755 4 -2 i
: 2759 11 2 ca
: 2771 5 0 ~
: 2777 9 -2 ted
- 2786
: 2786 5 -5 I
: 2792 5 -5  am
: 2798 5 -2  my
: 2804 4 2  own
: 2808 5 0  worst
: 2813 5 0  en
: 2819 2 -2 e
: 2821 23 0 my
: 2846 15 -2 ~
- 2864
: 2877 4 -5 So
: 2881 5 -2  what
* 2887 11 2  ails
: 2899 5 0  you
: 2904 11 -2 ~
- 2915
: 2916 2 -5 is
: 2918 5 -5  what
: 2924 5 -2  im
: 2929 9 2 pales
: 2939 5 0  you
: 2945 11 -2 ~
- 2956
: 2957 3 -5 You
: 2960 5 -5  are
: 2966 5 -2  your
: 2972 5 2  own
: 2978 5 0  worst
* 2984 3 0  en
* 2987 4 -2 e
* 2991 21 0 my
* 3014 17 -2 ~
- 3034
: 3045 5 -5 You're
: 3051 5 -2  a
: 3057 9 2  vic
: 3067 5 0 tim
: 3073 11 -2 ~
- 3085
: 3087 5 -5 of
: 3093 5 -2  the
: 3099 9 2  sys
: 3108 5 0 tem
: 3114 11 -2 ~
- 3125
: 3126 5 -5 You
: 3131 4 -5  are
: 3135 5 -2  your
: 3141 5 2  own
: 3147 5 0  worst
: 3153 5 0  en
: 3158 2 -2 e
: 3160 20 0 my
: 3181 15 -2 ~
- 3199
: 3213 5 -5 You're
: 3218 5 -2  a
: 3224 9 2  vic
: 3234 5 0 tim
: 3240 11 -2 ~
- 3254
: 3257 4 -5 of
: 3261 5 -2  the
: 3267 11 2  sys
: 3278 5 0 tem
: 3284 7 -2 ~
- 3291
: 3292 5 -5 You
: 3298 5 -2  are
* 3304 3 2  your
* 3307 5 0  own
* 3313 5 0  worst
* 3319 4 -2  en
* 3323 4 0 e
* 3327 52 0 my
E
