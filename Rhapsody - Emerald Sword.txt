#TITLE:Emerald Sword
#ARTIST:Rhapsody
#MP3:Rhapsody - Emerald Sword.mp3
#COVER:Rhapsody - Emerald Sword [CO].jpg
#BACKGROUND:Rhapsody - Emerald Sword [BG].jpg
#BPM:180,05
#GAP:32730
#ENCODING:UTF8
#GENRE:Metal
#EDITION:[SC]-Songs
: 0 6 0 I
: 6 6 2  crossed
: 12 4 3  the
: 16 6 2  va
: 22 5 -2 lleys
: 28 4 -2  the
: 32 6 3  dust
: 38 6 5  of
: 44 4 7  mid
: 48 8 5 lands
- 58
: 60 4 5 to
: 64 6 5  search
: 70 6 3  for
: 76 4 2  the
: 80 6 2  third
: 86 5 3  key
: 92 4 5  to
: 96 6 7  o
: 102 6 2 pen
: 108 4 0  the
: 112 8 2  gates
- 122
: 128 6 7 Now
: 136 3 7  I'm
: 139 3 5  near
: 142 2 3  the
: 144 6 3  al
: 150 5 2 tar
* 156 4 10  the
* 160 6 10  se
* 166 6 12 cret
: 172 4 10  in
: 176 6 5 side
- 184
: 193 2 8 as
: 195 4 8  le
: 199 2 8 gend
: 201 7 8  told
: 209 2 8  my
: 211 3 7  be
: 214 3 5 loved
: 217 6 7  sun
- 224
: 225 4 9  light
: 229 4 9  the
: 233 4 7  dra
: 237 4 6 gons
: 241 8 7  eyes
- 251
: 253 2 7 On
: 255 2 10  the
: 257 8 12  way
: 265 4 10  to
: 269 4 5  the
: 273 8 7  glo
: 281 4 3 ry
: 285 4 3  I'll
: 289 8 8  ho
: 297 4 7 nour
: 301 4 5  my
: 305 9 7  sword
- 315
: 317 4 3 to
: 321 8 12  serve
: 329 4 10  right
: 333 4 5  i
: 337 8 7 deals
: 349 4 7  and
: 353 8 8  jus
: 361 4 10 tice
: 365 4 8  for
* 369 12 12  all
- 383
: 517 6 3 Fi
: 523 6 2 na
: 529 4 3 lly
: 533 6 2  ha
: 539 5 -2 ppend
: 545 4 -2  the
: 549 6 3  sun
: 555 6 5  lit
: 561 4 7  their
: 565 8 5  eyes
- 575
: 577 4 5 the
: 581 6 5  spell
: 587 6 3  was
: 593 4 2  cre
: 597 6 2 a
: 603 7 3 ting
: 613 6 6  strange
: 619 6 3  games
: 625 4 0  of
: 629 8 2  light
- 639
: 645 6 7 Thanks
: 653 3 7  to
: 656 3 5  hi
: 659 2 3 dden
: 661 6 3  mi
: 667 5 2 rrors
: 673 4 10  I
: 677 6 10  found
* 683 6 12  my
: 689 4 10  lost
: 693 6 5  way
- 701
: 709 2 8 o
: 711 4 8 ver
: 715 2 8  the
: 717 7 8  stones
- 725
: 725 2 8 I
: 727 4 7  reached
: 731 2 7  the
: 733 6 7  place
: 740 1 6  it
: 741 4 9  was
: 745 4 9  a
: 749 4 7  se
: 753 4 6 cret
: 757 8 7  cave
- 767
: 769 2 7 In
: 771 2 10  a
: 773 8 12  long
: 781 4 10  bloo
: 785 4 5 dy
: 789 8 7  ba
: 797 4 3 ttle
: 801 4 3  that
: 805 8 8  pro
: 813 4 7 phe
: 817 4 5 cies
: 821 9 7  told
- 831
: 833 4 3 the
: 837 8 12  light
: 845 4 10  will
: 849 4 5  pre
: 853 8 7 vail
: 865 4 7  hence
: 869 8 8  wis
: 877 4 10 dom
: 881 4 8  is
* 885 12 12  gold
- 899
: 909 4 8 For
: 913 4 10  the
: 917 8 12  king
: 925 4 13  for
: 929 4 12  the
: 933 8 10  land
: 941 4 8  for
: 945 4 7  the
: 949 16 8  moun
: 965 6 7 tains
- 972
: 973 4 5 For
: 977 4 5  the
: 981 8 17  green
: 989 4 17  va
: 993 4 17 lleys
: 997 8 15  where
: 1005 4 10  dra
: 1009 3 13 gons
* 1013 22 12  fly
- 1036
: 1037 4 8 For
: 1041 4 10  the
: 1045 8 12  glo
: 1053 4 13 ry
: 1057 4 12  the
: 1061 8 15  po
: 1069 4 15 wer
: 1073 4 15  to
* 1077 8 20  win
: 1085 4 19  the
: 1089 4 17  black
: 1093 8 12  lord
- 1101
: 1101 4 10 I
: 1105 4 12  will
: 1109 8 13  search
: 1117 4 15  for
: 1121 4 17  the
: 1125 8 15  e
: 1133 8 19 merald
* 1141 12 20  sword
- 1155
: 1773 6 0 On
: 1779 6 2 ly
: 1785 4 3  a
: 1789 6 2  wa
: 1795 5 -2 rrior
: 1805 6 3  with
: 1811 6 5  a
: 1817 4 7  clear
: 1821 8 5  heart
- 1831
: 1837 6 5 could
: 1843 6 7  have
: 1849 4 8  the
: 1853 6 8  ho
: 1859 4 7 nour
: 1863 2 7  to
: 1865 2 7  be
: 1869 6 9  kissed
: 1875 6 11  by
: 1881 4 12  the
: 1885 8 11  sun
- 1895
* 1901 6 12 Yes,
: 1909 3 12  I
: 1912 3 10 'm
: 1915 2 8  that
: 1917 6 8  wa
: 1923 5 7 rrior
: 1929 4 10  I
: 1933 6 10  fo
: 1939 6 12 llowed
: 1945 4 10  my
: 1949 6 5  way
- 1957
: 1965 2 8 led
: 1967 4 8  by
: 1971 2 8  the
: 1973 7 8  force
: 1981 2 8  of
: 1983 4 7  cos
: 1987 2 5 mic
: 1989 7 7  soul
- 1997
: 1997 4 9 I
: 2001 4 9  can
: 2005 4 7  reach
: 2009 4 6  the
: 2013 8 7  sword
- 2023
: 2025 2 7 On
: 2027 2 10  the
: 2029 8 12  way
: 2037 4 10  to
: 2041 4 5  the
: 2045 8 7  glo
: 2053 4 3 ry
: 2057 4 3  I'll
: 2061 8 8  ho
: 2069 4 7 nour
: 2073 4 5  my
: 2077 9 7  sword
- 2087
: 2089 4 3 to
: 2093 8 12  serve
: 2101 4 10  right
: 2105 4 5  i
: 2109 8 7 deals
: 2121 4 7  and
: 2125 8 8  jus
: 2133 4 10 tice
: 2137 4 8  for
: 2141 12 12  all
- 2155
: 2165 4 8 For
: 2169 4 10  the
: 2173 8 12  king
: 2181 4 13  for
: 2185 4 12  the
: 2189 8 10  land
: 2197 4 8  for
: 2201 4 7  the
: 2205 16 8  moun
: 2221 6 7 tains
- 2228
: 2229 4 5 For
: 2233 4 5  the
: 2237 8 17  green
: 2245 4 17  va
: 2249 4 17 lleys
: 2253 8 15  where
: 2261 4 10  dra
: 2265 3 13 gons
: 2269 22 12  fly
- 2292
: 2293 4 8 For
: 2297 4 10  the
: 2301 8 12  glo
: 2309 4 13 ry
: 2313 4 12  the
: 2317 8 15  po
: 2325 4 15 wer
: 2329 4 15  to
* 2333 8 20  win
: 2341 4 19  the
: 2345 4 17  black
: 2349 8 12  lord
- 2357
: 2357 4 10 I
: 2361 4 12  will
: 2365 8 13  search
: 2373 4 15  for
: 2377 4 17  the
: 2381 8 15  e
: 2389 8 19 merald
* 2397 12 20  sword
- 2411
: 2421 4 8 For
: 2425 4 10  the
: 2429 8 12  king
: 2437 4 13  for
: 2441 4 12  the
: 2445 8 10  land
: 2453 4 8  for
: 2457 4 7  the
: 2461 16 8  moun
: 2477 6 7 tains
- 2484
: 2485 4 5 For
: 2489 4 5  the
: 2493 8 17  green
: 2501 4 17  va
: 2505 4 17 lleys
: 2509 8 15  where
: 2517 4 10  dra
: 2521 3 13 gons
* 2525 22 12  fly
- 2548
: 2549 4 8 For
: 2553 4 10  the
: 2557 8 12  glo
: 2565 4 13 ry
: 2569 4 12  the
: 2573 8 15  po
: 2581 4 15 wer
: 2585 4 15  to
* 2589 8 20  win
: 2597 4 19  the
: 2601 4 17  black
: 2605 8 12  lord
- 2613
: 2613 4 10 I
: 2617 4 12  will
: 2621 8 13  search
: 2629 4 15  for
: 2633 4 17  the
: 2637 8 15  e
: 2645 8 19 merald
* 2653 12 20  sword
E