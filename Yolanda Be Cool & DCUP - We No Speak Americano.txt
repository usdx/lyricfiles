#TITLE:We No Speak Americano
#ARTIST:Yolanda Be Cool & DCUP
#MP3:Yolanda Be Cool & DCUP - We No Speak Americano.mp3
#VIDEO:Yolanda Be Cool & DCUP - We No Speak Americano.mp4
#COVER:Yolanda Be Cool & DCUP - We No Speak Americano[CO].jpg
#BPM:250,04
#GAP:2060
#ENCODING:UTF8
#PREVIEWSTART:2,960
#LANGUAGE:Other
#GENRE:Dance
#YEAR:2010
: 0 2 10 Com
: 4 1 12 me
: 6 1 13  te
: 8 2 15  po'
: 12 1 13  ca
: 14 1 13 pi
: 16 2 12 ~
: 20 2 15  chi
: 24 2 14  te
: 28 2 12  vo
: 32 2 12  be
: 36 2 10 ne
- 40
: 64 2 12 Si
: 68 1 12  tu
: 70 1 13  le
: 72 2 15  par
: 76 2 13 le
: 80 2 12  'mmiezzo
: 84 2 15  A
: 88 1 13 me
: 90 2 12 ri
: 94 2 12 ca
: 98 2 10 no?
- 102
: 128 2 12 Quan
: 132 1 12 do
: 134 1 12  se
: 136 2 12  fa
: 140 2 13  lam
: 144 2 15 mor
: 148 2 12  sot
: 152 2 13 to
: 156 2 15  'a
: 160 1 15  lu
: 162 2 13 ~
: 166 6 13 na
- 174
: 192 2 12 Co
: 196 1 12 me
: 198 1 12  te
: 200 2 12  ve
: 204 2 13 ne
: 208 2 12  'ca
: 212 1 10 pa
: 214 1 10  e
: 216 1 9  di:
: 218 1 9  "I
: 220 2 10  love
: 224 1 12  you!?"
- 227
: 276 2 13 Fa
: 280 1 13  fa
: 282 1 12  l'a
: 284 1 13 me
: 286 1 12 ri
: 288 2 12 ca
: 292 2 10 no!
- 296
: 660 2 13 Fa
: 664 1 13  fa
: 666 1 12  l'a
: 668 1 13 me
: 670 1 12 ri
: 672 2 12 ca
: 676 2 10 no!
- 680
: 916 2 13 Fa
: 920 1 13  fa
: 922 1 12  l'a
: 924 1 13 me
: 926 1 12 ri
: 928 2 12 ca
: 932 2 10 no!
- 936
: 1328 1 13 Fa
: 1330 1 12  l'a
: 1332 1 13 me
: 1334 1 12 ri
: 1336 2 12 ca
: 1340 2 10 no!
- 1344
: 1580 2 13 Fa
: 1584 1 13  fa
: 1586 1 12  l'a
: 1588 1 13 me
: 1590 1 12 ri
: 1592 2 12 ca
: 1596 2 10 no!
- 1600
: 2444 2 13 Fa
: 2448 1 13  fa
: 2450 1 12  l'a
: 2452 1 13 me
: 2454 1 12 ri
: 2456 2 12 ca
: 2460 2 10 no!
- 2464
: 2700 2 13 Fa
: 2704 1 13  fa
: 2706 1 12  l'a
: 2708 1 13 me
: 2710 1 12 ri
: 2712 2 12 ca
: 2716 2 10 no!
- 2720
: 2960 2 13 Whis
: 2964 2 10 ky
: 2968 2 12  so
: 2972 1 10 da
: 2974 1 10  e
: 2976 2 9  ro
: 2980 1 12 ck'en
: 2982 2 10  'roll
- 2986
: 2992 2 13 Whis
: 2996 2 10 ky
: 3000 2 12  so
: 3004 1 10 da
: 3006 1 10  e
: 3008 2 9  ro
: 3012 1 12 ck'en
: 3014 2 10  'roll
- 3018
: 3024 2 13 Whis
: 3028 2 10 ky
: 3032 2 12  so
: 3036 1 10 da
: 3038 1 10  e
: 3040 2 9  ro
: 3044 1 12 ck'en
: 3046 2 10  'roll
E