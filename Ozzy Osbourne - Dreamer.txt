#TITLE:Dreamer
#ARTIST:Ozzy Osbourne
#MP3:Ozzy Osbourne - Dreamer.mp3
#VIDEO:Ozzy Osbourne - Dreamer [VD#0].mp4
#COVER:Ozzy Osbourne - Dreamer [CO].jpg
#BACKGROUND:Ozzy Osbourne - Dreamer [BG].jpg
#BPM:75
#GAP:9690
#ENCODING:UTF8
#LANGUAGE:English
#EDITION:UltraStar Charts Vol. 2
: 0 2 8 Ga
: 2 2 5 zing 
: 4 2 8 through 
: 6 1 5 the 
: 7 2 8 win
: 9 2 5 dow 
- 11
: 11 2 8 at 
: 13 1 5 the 
: 14 3 10 world 
: 17 2 12 out
: 19 6 10 side 
- 26
: 30 2 8 Won
: 32 1 5 de
: 33 3 8 ring 
: 36 2 5 will 
: 38 1 8 mot
: 39 1 5 her 
: 41 2 8 earth 
: 43 2 12 sur
: 45 6 10 vive
- 55
: 60 2 13 Hop
: 62 2 10 ing 
: 64 2 13 that 
: 66 2 10 man
: 68 2 13 kind 
: 70 2 10 will 
: 72 2 13 stop 
: 74 1 10 a
: 75 2 15 bu
: 77 2 17 sing 
: 79 3 15 her 
- 83
: 85 4 10 some
* 89 10 12 time 
- 105
: 121 1 8 Af
: 123 1 5 ter 
: 124 2 8 all 
: 126 2 5 there's 
: 128 1 8 on
: 130 2 5 ly 
: 132 2 8 just 
: 134 1 5 the 
: 136 2 10 two 
: 138 1 12 of 
: 139 3 10 us
- 146
: 149 2 5 And 
: 151 2 8 here 
: 153 1 5 we 
: 154 2 8 are 
: 156 2 5 still 
: 158 2 8 figh
: 160 1 5 ting 
: 162 2 8 for 
: 164 2 12 our 
: 166 7 12 lives
- 176
: 181 2 13 Wat
: 183 1 10 ching 
: 184 3 13 all 
: 187 1 10 of 
: 189 1 13 his
: 190 1 10 to
: 192 2 13 ry 
: 194 2 10 re
: 196 2 15 peat 
: 198 2 17 it
: 200 3 15 self
- 205
: 206 2 10 Time 
: 208 1 10 af
: 209 1 10 ter 
: 211 1 12 ti
* 212 10 8 ~me
- 226
: 236 1 8 I'm 
: 237 2 8 just 
: 239 2 8 a 
: 242 3 15 drea
* 245 2 17 ~
: 247 3 13 mer
- 253
: 255 1 13 I 
: 256 3 12 dream 
: 259 2 13 my 
: 262 3 12 life 
: 266 2 8 a
: 270 6 12 way
- 285
: 296 2 8 I'm 
: 298 2 10 just 
: 300 2 8 a 
: 302 4 15 drea
: 306 2 17 ~
: 308 4 13 mer
- 313
: 315 2 13 Who 
: 317 3 12 dreams 
: 320 2 13 of 
: 322 3 12 bet
: 326 4 8 ter 
: 330 7 12 days
- 345
: 360 2 8 I 
: 362 2 8 watch 
: 364 2 5 the 
: 366 2 8 sun 
: 368 2 5 go 
: 370 2 8 down 
: 372 2 5 like 
: 374 1 8 eve
: 375 2 5 ry
: 377 2 10 one 
: 379 2 12 of 
: 381 3 10 us
- 387
: 391 1 5 I'm 
: 393 1 8 hop
: 394 2 5 ing 
: 396 2 8 that 
: 398 1 5 the 
: 399 3 8 dawn 
: 402 1 5 will 
: 404 1 8 bring 
: 406 1 12 a 
* 408 6 12 sign
- 414
: 421 2 10 A 
: 423 1 13 bet
: 424 1 10 ter 
: 426 2 13 place 
: 428 2 10 for 
: 430 2 13 those 
: 432 1 10 who 
: 434 2 13 will 
: 436 1 10 come 
: 438 2 17 af
: 440 1 15 ter 
: 441 3 15 us 
- 445
: 448 3 10 This 
: 451 2 12 ti
* 453 9 8 ~me
- 466
: 477 2 8 I'm 
: 479 2 8 just 
: 481 2 8 a 
: 483 3 15 drea
* 487 2 15 ~
: 489 4 13 mer
- 494
: 496 2 13 I 
: 498 4 12 dream 
: 502 2 13 my 
: 504 4 12 life 
: 508 3 8 a
: 511 7 12 way 
: 524 2 12 oh 
: 527 6 10 yeah
- 533
: 538 1 17 I'm 
* 540 1 20 just 
: 542 1 17 a 
: 544 3 15 drea
: 547 2 17 ~
: 549 4 13 mer
- 554
: 556 2 13 Who 
: 558 4 12 dreams 
: 562 2 13 of 
: 564 4 12 bet
: 568 4 8 ter 
: 572 6 12 days
- 587
: 602 1 15 Your 
* 604 1 15 hig
* 605 2 13 her 
: 608 1 15 po
: 609 1 13 wer 
: 611 2 15 may 
: 613 2 13 be 
- 615
: 615 2 10 God 
: 617 1 13 or 
* 619 1 17 Je
: 621 1 15 sus 
: 623 2 15 Christ
- 628
: 633 1 13 It 
: 634 2 15 doe
: 636 2 13 sn't 
: 638 1 15 rea
: 639 1 13 lly 
: 641 2 15 mat
: 643 1 13 ter 
: 644 3 10 much 
: 647 2 13 to 
: 649 2 13 me
: 651 2 12 ~
- 658
: 663 1 15 With
: 664 2 15 out 
: 666 2 13 each 
: 668 2 15 others 
: 671 2 13 help 
: 673 2 15 the 
: 675 2 10 ain't 
: 677 2 13 no 
- 679
: 679 2 17 hope 
: 681 2 13 for 
: 683 3 15 us
- 690
: 693 1 13 I'm 
: 694 2 15 liv
: 696 2 13 ing 
: 698 1 15 in 
: 699 1 13 a 
: 701 3 15 dream 
: 704 1 13 of 
: 705 2 10 fan
: 707 2 13 ta
: 709 2 13 sy
: 711 3 12 ~
- 714
: 715 1 17 Oh 
: 716 4 20 yeah, 
* 720 4 17 yeah, 
: 724 6 15 yeah
- 735
: 873 2 5 If 
: 875 1 8 on
: 877 1 5 ly 
: 879 1 8 we 
: 880 2 5 could 
: 882 2 8 all 
: 885 1 5 the 
: 887 1 8 find 
: 888 1 5 se
: 890 2 10 re
: 892 1 12 ni
: 894 3 10 ty
- 900
: 904 1 5 It 
: 906 2 8 would 
: 908 1 5 be 
: 909 2 8 nice 
: 912 1 5 if 
: 914 1 8 we 
: 915 1 5 could 
: 917 1 8 live 
: 919 1 12 as 
: 921 5 10 one
- 931
: 936 2 13 When 
: 938 1 10 will 
: 939 2 13 all 
: 941 2 10 this 
: 943 2 13 an
: 945 1 10 ger, 
: 947 2 13 hate 
: 949 2 10 and 
: 951 2 17 big
: 953 1 15 got
: 954 4 15 ry
- 958
: 961 3 10 Be 
: 964 2 10 go
* 966 9 12 ~ne?
- 978
: 991 1 8 I'm 
: 992 2 8 just 
: 994 2 8 a 
: 996 4 15 drea
: 1000 2 13 ~
: 1002 5 13 mer
- 1008
: 1010 1 13 I 
: 1011 4 12 dream 
: 1015 2 13 my 
: 1017 4 12 life 
: 1021 3 8 a
: 1025 6 12 way
- 1033
: 1038 2 12 To
: 1040 5 10 day
- 1045
: 1051 1 8 I'm 
: 1053 1 8 just 
: 1055 1 8 a 
: 1057 3 15 drea
: 1060 3 17 ~
: 1063 4 13 mer
- 1068
: 1070 2 13 Who 
: 1072 3 12 dreams 
: 1076 1 13 of 
: 1078 3 12 bet
: 1081 4 8 ter 
: 1085 5 12 days
- 1093
: 1098 2 12 O
* 1100 4 10 kay
- 1105
: 1111 2 17 I'm 
* 1113 2 20 just 
: 1115 2 17 a 
: 1117 4 15 drea
: 1121 2 17 ~
: 1123 4 13 mer
- 1128
: 1130 2 13 Who's 
: 1132 4 12 search
: 1136 2 13 ing 
: 1138 4 12 for 
: 1143 2 8 the 
: 1145 5 12 way
- 1153
: 1158 2 12 To
: 1160 6 10 day
- 1167
: 1172 1 8 I'm 
: 1173 2 8 just 
: 1175 1 8 a 
: 1177 4 15 drea
: 1181 2 17 ~
: 1183 5 13 mer
- 1190
: 1192 3 12 Dream
* 1195 2 12 ing 
: 1197 2 13 my 
: 1199 4 12 life 
: 1204 2 8 a
: 1206 6 12 way
- 1214
: 1219 2 17 Oh 
: 1222 7 20 yeah, 
: 1230 6 17 yeah, 
* 1237 2 15 ye
* 1239 16 13 ~ah
E