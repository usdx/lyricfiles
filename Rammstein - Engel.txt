#TITLE:Engel
#ARTIST:Rammstein
#LANGUAGE:GErman
#MP3:Rammstein - Engel.mp3
#COVER:Rammstein - Engel [CO].jpg
#BACKGROUND:Rammstein - Engel [BG].jpg
#VIDEO:Rammstein - Engel [VD#0].avi
#PREVIEWSTART:148,287
#MedleyStartBeat:1591
#MedleyEndBeat:2342
#BPM:288,27
#GAP:40159
F 0 3 14 Wer
F 6 2 14  zu
F 12 3 14  Leb
F 18 3 14 zeit
F 24 3 14  gut
F 30 2 14  auf
F 33 5 14  Er
F 42 3 14 den
- 47
F 90 3 14 wird
F 96 3 14  nach
F 102 3 14  dem
F 108 3 14  Tod
F 114 3 14  ein
F 120 2 14  Eng
F 123 4 14 el
F 129 6 14  wer
F 138 3 15 den
- 143
F 186 3 14 den
F 192 3 14  Blick
F 198 3 14  gen
F 204 2 14  Him
F 207 5 14 mel
F 216 3 14  fragst
F 222 2 14  du
F 225 7 14  dann
- 234
F 282 3 14 wa
F 288 3 14 rum
F 294 3 14  man
F 300 4 14  sie
F 306 3 14  nicht
F 312 2 14  seh
F 315 4 14 en
F 324 5 14  kann
- 331
: 391 3 14 Erst
: 397 4 14  wenn
: 403 3 14  die
: 409 8 14  Wol
: 421 5 14 ken
: 433 8 16  schla
: 443 3 17 fen
: 451 12 16  gehn
- 465
: 493 3 14 kann
: 499 3 14  man
: 505 6 14  uns
: 517 5 14  am
* 529 4 17  Him
: 537 5 14 mel
: 548 9 14  sehn
- 559
: 583 2 21 wir
: 589 4 21  ha
: 595 2 21 ben
: 598 6 21  Angst
: 607 3 21  und
: 613 5 21  sind
: 620 2 19  al
: 623 15 19 lein
- 640
: 685 5 19 Gott
: 697 4 19  weiss
: 703 3 19  ich
: 709 3 19  will
: 715 4 21  kein
: 721 4 21  Eng
: 727 5 14 el
* 739 15 14  sein
- 756
F 1189 8 14 Sie
F 1201 4 14  le
F 1207 2 14 ben
F 1213 4 14  hin
F 1219 3 14 term
F 1225 3 14  Son
F 1229 2 14 nen
F 1237 18 14 schein
- 1257
F 1290 3 14 ge
F 1296 4 14 trennt
F 1302 2 14  von
F 1305 6 14  uns
F 1314 2 14  un
F 1317 5 14 end
F 1326 3 14 lich
F 1333 8 14  weit
- 1343
F 1387 3 14 sie
F 1393 3 14  mĂźs
F 1399 2 14 sen
F 1405 3 14  sich
F 1411 2 14  an
F 1417 4 14  Ster
F 1423 2 14 ne
F 1429 2 14  kral
F 1432 4 14 len
- 1438
F 1483 4 14 da
F 1489 3 14 mit
F 1495 3 14  sie
F 1501 3 14  nicht
F 1507 3 14  vom
F 1513 2 14  Him
F 1517 3 14 mel
F 1525 2 14  fal
F 1528 5 14 len
- 1535
: 1591 3 14 Erst
: 1597 3 14  wenn
: 1603 3 14  die
: 1609 8 14  Wol
: 1621 5 14 ken
: 1633 6 16  schla
: 1644 3 17 fen
: 1652 9 16  gehn
- 1663
: 1694 4 14 kann
: 1700 4 14  man
: 1706 5 14  uns
: 1718 5 14  am
: 1730 4 17  Him
: 1738 4 14 mel
: 1748 10 14  sehn
- 1760
: 1783 3 21 wir
: 1789 4 21  ha
: 1795 2 21 ben
: 1798 6 21  Angst
: 1808 3 21  und
: 1814 3 21  sind
: 1820 2 19  al
* 1824 15 19 lein
- 1841
: 1885 4 31 Gott
: 1897 4 31  weiss
: 1903 4 31  ich
: 1909 4 31  will
: 1915 4 33  kein
: 1921 4 33  Eng
: 1927 7 26 el
: 1939 16 26  sein
- 1957
: 2078 5 22 Gott
: 2090 3 21  weiss
: 2096 3 21  ich
: 2102 3 21  will
: 2108 4 22  kein
: 2114 6 21  Eng
: 2122 6 14 el
: 2132 18 14  sein
- 2152
* 2270 4 22 Gott
: 2282 4 21  weiss
: 2288 4 21  ich
: 2294 4 21  will
: 2300 4 22  kein
: 2306 4 21  Eng
: 2312 5 14 el
: 2324 18 14  sein
- 2344
: 2936 3 14 Erst
: 2942 3 14  wenn
: 2948 3 14  die
: 2954 6 14  Wol
: 2966 6 14 ken
: 2978 6 16  schla
: 2989 4 17 fen
: 2997 14 16  gehn
- 3013
: 3038 4 14 kann
: 3044 4 14  man
: 3051 5 14  uns
: 3063 6 14  am
: 3075 5 17  Him
: 3082 5 14 mel
: 3093 10 14  sehn
- 3105
: 3128 3 21 wir
: 3134 4 21  ha
: 3140 2 19 ben
: 3143 6 21  Angst
: 3152 3 21  und
: 3158 4 21  sind
: 3165 2 19  al
: 3168 16 19 lein
- 3186
: 3230 4 19 Gott
: 3242 4 19  weiss
: 3248 3 19  ich
: 3254 3 19  will
: 3260 4 21  kein
: 3266 4 21  Eng
: 3272 6 21 el
* 3284 66 21  sein
- 3352
: 3422 5 22 Gott
: 3434 4 21  weiss
: 3440 3 21  ich
: 3446 3 21  will
: 3452 4 22  kein
: 3458 4 21  Eng
: 3464 7 14 el
: 3476 25 14  sein
- 3503
: 3614 6 22 Gott
: 3626 4 21  weiss
: 3632 4 21  ich
: 3638 4 21  will
: 3644 4 22  kein
: 3650 4 21  Eng
: 3656 7 14 el
: 3668 21 14  sein
- 3691
: 3806 4 22 Gott
: 3818 4 21  weiss
: 3824 3 21  ich
: 3830 4 21  will
* 3836 4 22  kein
: 3842 4 21  Eng
: 3848 8 14 el
: 3860 20 14  sein
- 3882
: 3998 4 22 Gott
: 4010 4 21  weiss
: 4016 3 21  ich
: 4022 3 21  will
: 4028 4 22  kein
: 4034 4 21  Eng
: 4040 6 14 el
: 4052 16 14  sein
E
