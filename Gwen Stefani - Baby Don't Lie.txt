#TITLE:Baby Dont Lie
#ARTIST:Gwen Stefani
#MP3:Gwen Stefani - Baby Don?t Lie.mp3
#VIDEO:Gwen Stefani - Baby Don?t Lie.avi
#COVER:Gwen Stefani - Baby Don?t Lie.jpg
#BPM:200,02
#GAP:20500
#VIDEOGAP:0,2
#ENCODING:UTF8
: 0 1 -4 We've
: 2 1 -2  been
: 4 2 1  walk
: 8 2 1 ing
: 12 2 1  down
: 16 2 1  this
: 20 2 3  road 
: 24 2 5 ~
- 28
: 32 2 -2 Some
: 36 4 -4  time
- 42
: 64 1 -4 And
: 66 1 -2  you
: 68 2 1  love,
* 72 1 -4  and
* 74 1 -2  you
* 76 2 1  love
: 80 2 1  me
: 84 2 3  good 
: 88 2 5 ~
- 92
: 96 2 -2 No
: 100 4 -4  lie
- 106
: 128 1 1 But
: 130 1 1  there's
* 132 2 8  some
: 136 1 1 thing
: 138 1 1  be
: 140 2 8 hind
: 144 2 1  those
: 148 2 1  eyes
- 152
: 160 2 1 Those
: 164 4 1  eyes
- 170
: 192 1 1 That
: 194 1 1  you
: 196 2 8  can't,
: 200 1 1  that
: 202 1 1  you
: 204 2 8  can't
: 208 2 1  dis
: 212 2 1 guise
- 216
: 224 2 1 Dis
: 228 4 1 guise
- 234
: 248 1 1 Ba
: 250 1 0 by
: 252 2 -2  don't,
* 256 1 1  ba
* 258 1 0 by
* 260 2 -2  don't
- 263
: 264 1 1 Ba
: 266 1 0 by
: 268 2 -2  don't
: 272 6 -2  lie 
: 280 2 -4 ~
- 283
: 284 1 6 I
: 286 1 6  don't
: 288 1 6  wan
: 290 1 6 na
: 292 2 5  cry
: 296 2 5  no
: 300 2 3  lon
: 304 2 3 ger
- 308
: 312 1 1 Ba
: 314 1 0 by
: 316 2 -2  don't,
* 320 1 1  ba
* 322 1 0 by
* 324 2 -2  don't
- 327
: 328 1 1 Ba
: 330 1 0 by
: 332 2 -2  don't
: 336 6 -2  lie 
: 344 2 -4 ~
- 347
: 348 1 6 I'm
: 350 1 6 ma
: 352 1 6  need
: 354 1 6  a
: 356 2 5  love
: 360 2 5  that's
: 364 2 3  strong
: 368 2 3 er
- 372
: 378 1 1 I
: 380 1 1  tell
: 382 1 1  you
: 384 1 1  know,
: 386 1 1  I
: 388 1 5  tell
: 390 1 5  you
: 392 1 5  know
- 394
: 394 1 5 I
* 396 1 8  tell
* 398 1 8  you
: 400 2 1  know,
: 404 4 1  why
- 410
: 416 2 1 Know
: 420 4 1  why
- 426
: 444 1 5 If
: 446 1 5  we
: 448 1 5  ev
: 450 1 5 er
: 452 2 8  give
: 456 2 10  up
- 459
: 460 1 1 Then
: 462 1 1  we're
: 464 1 1  gon
: 466 1 1 na
: 468 2 5  die
- 472
: 476 1 -2 Look
: 478 1 -2  me
: 480 1 -2  in
: 482 1 -2  the
: 484 2 1  eye,
: 488 1 1  ba
: 490 1 1 by
: 492 2 1  don't
: 496 2 1  lie
- 500
: 552 1 1 Ba
: 554 1 1 by
: 556 2 1  don't
: 560 4 1  lie
- 566
: 640 1 -4 We've
: 642 1 -2  been
: 644 2 1  walk
: 648 2 1 ing
: 652 2 1  down
: 656 2 1  this
: 660 2 3  road 
: 664 2 5 ~
- 668
: 672 2 -2 Some
: 676 4 -4  time
- 682
: 704 1 -4 And
: 706 1 -2  you
: 708 2 1  love,
* 712 1 -4  and
* 714 1 -2  you
* 716 2 1  love
: 720 2 1  me
: 724 2 3  good 
: 728 2 5 ~
- 732
: 736 2 -2 No
: 740 4 -4  lie
- 746
: 768 1 1 But
: 770 1 1  there's
* 772 2 8  some
: 776 1 1 thing
: 778 1 1  be
: 780 2 8 hind
: 784 2 1  those
: 788 2 1  eyes
- 792
: 800 2 1 Those
: 804 4 1  eyes
- 810
: 832 1 1 That
: 834 1 1  you
: 836 2 8  can't,
: 840 1 1  that
: 842 1 1  you
: 844 2 8  can't
: 848 2 1  dis
: 852 2 1 guise
- 856
: 864 2 1 Dis
: 868 4 1 guise
- 874
: 888 1 -2 Ba
: 890 1 1 by
: 892 2 5  tell
: 896 1 5  me
: 898 1 5  there's
: 900 2 8  noth
: 904 1 8 ing
: 906 1 8  but
: 908 2 8  love 
: 912 2 10 ~
- 916
: 952 1 -2 Ba
: 954 1 1 by
: 956 2 5  tell
: 960 1 5  me
: 962 1 5  there's
: 964 2 8  noth
: 968 1 8 ing
: 970 1 8  but
: 972 2 8  love 
: 976 2 10 ~
- 980
* 1016 1 1 Ba
* 1018 1 0 by
* 1020 2 -2  don't,
: 1024 1 1  ba
: 1026 1 0 by
: 1028 2 -2  don't
- 1031
: 1032 1 1 Ba
: 1034 1 0 by
: 1036 2 -2  don't
: 1040 6 -2  lie 
: 1048 2 -4 ~
- 1051
: 1052 1 6 I
: 1054 1 6  don't
: 1056 1 6  wan
: 1058 1 6 na
: 1060 2 5  cry
: 1064 2 5  no
: 1068 2 3  lon
: 1072 2 3 ger
- 1076
* 1080 1 1 Ba
* 1082 1 0 by
* 1084 2 -2  don't,
: 1088 1 1  ba
: 1090 1 0 by
: 1092 2 -2  don't
- 1095
: 1096 1 1 Ba
: 1098 1 0 by
: 1100 2 -2  don't
: 1104 6 -2  lie 
: 1112 2 -4 ~
- 1115
: 1116 1 6 I'm
: 1118 1 6 ma
: 1120 1 6  need
: 1122 1 6  a
: 1124 2 5  love
: 1128 2 5  that's
: 1132 2 3  strong
: 1136 2 3 er
- 1140
: 1146 1 1 I
: 1148 1 1  tell
: 1150 1 1  you
: 1152 1 1  know,
: 1154 1 1  I
: 1156 1 5  tell
: 1158 1 5  you
: 1160 1 5  know
- 1162
: 1162 1 5 I
* 1164 1 8  tell
* 1166 1 8  you
: 1168 2 1  know,
: 1172 4 1  why
- 1178
: 1184 2 1 Know
: 1188 4 1  why
- 1194
: 1212 1 5 If
: 1214 1 5  we
: 1216 1 5  ev
: 1218 1 5 er
: 1220 2 8  give
: 1224 2 10  up
- 1227
: 1228 1 1 Then
: 1230 1 1  we're
: 1232 1 1  gon
: 1234 1 1 na
: 1236 2 5  die
- 1240
: 1244 1 -2 Look
: 1246 1 -2  me
: 1248 1 -2  in
: 1250 1 -2  the
: 1252 2 1  eye,
: 1256 1 1  ba
: 1258 1 1 by
: 1260 2 1  don't
: 1264 2 6  lie 
: 1268 4 5 ~
- 1274
: 1320 1 1 Ba
: 1322 1 1 by
: 1324 2 1  don't
* 1328 2 6  lie 
* 1332 4 5 ~
- 1338
: 1448 2 8 Yeah
: 1452 2 8  yeah
: 1456 4 5  yeah
- 1462
: 1536 1 5 What
: 1538 1 3  you
: 1540 2 1  hid
: 1544 1 -2 ing
: 1546 4 1  boy?
- 1552
: 1568 1 8 What
: 1570 1 8  you
: 1572 2 10  hid
: 1576 1 1 ing
: 1578 4 1  boy?
- 1584
: 1592 1 8 I
: 1594 1 8  can
: 1596 1 8  tell
: 1598 1 1  what
: 1600 1 1  you've
: 1602 1 1  been
: 1604 2 1  hid
: 1608 1 1 ing
: 1610 4 1  boy
- 1616
: 1622 1 1 And
: 1624 1 10  you
: 1626 1 13  can
: 1628 1 10  tell
: 1630 1 1  me
: 1632 1 1  if
: 1634 1 1  I'm
: 1636 1 1  get
: 1640 1 1 ting
: 1642 4 1  warm
- 1648
: 1664 1 0 Am
: 1666 1 0  I
: 1668 2 0  get
: 1672 1 1 ting
: 1674 8 1  warm?
- 1684
: 1696 1 1 Am
: 1698 1 1  I
: 1700 2 1  get
: 1704 1 1 ting
: 1706 8 1  warm?
- 1716
: 1728 1 5 Am
: 1730 1 5  I
: 1732 2 5  get
: 1736 1 5 ting
: 1738 8 5  warm?
- 1748
: 1750 1 1 And
: 1752 1 10  you
: 1754 1 13  can
: 1756 1 10  tell
: 1758 1 1  me
: 1760 1 1  if
: 1762 1 1  I'm
: 1764 1 1  get
: 1768 1 1 ting
: 1770 4 1  warm
- 1776
* 1784 1 1 Ba
* 1786 1 0 by
* 1788 2 -2  don't,
: 1792 1 1  ba
: 1794 1 0 by
: 1796 2 -2  don't
- 1799
: 1800 1 1 Ba
: 1802 1 0 by
: 1804 2 -2  don't
: 1808 6 -2  lie 
: 1816 2 -4 ~
- 1819
: 1820 1 6 I
: 1822 1 6  don't
: 1824 1 6  wan
: 1826 1 6 na
: 1828 2 5  cry
: 1832 2 5  no
: 1836 2 3  lon
: 1840 2 3 ger
- 1844
* 1848 1 1 Ba
* 1850 1 0 by
* 1852 2 -2  don't,
: 1856 1 1  ba
: 1858 1 0 by
: 1860 2 -2  don't
- 1863
: 1864 1 1 Ba
: 1866 1 0 by
: 1868 2 -2  don't
: 1872 6 -2  lie 
: 1880 2 -4 ~
- 1883
: 1884 1 6 I'm
: 1886 1 6 ma
: 1888 1 6  need
: 1890 1 6  a
: 1892 2 5  love
: 1896 2 5  that's
: 1900 2 3  strong
: 1904 2 3 er
- 1908
: 1914 1 1 I
: 1916 1 1  tell
: 1918 1 1  you
: 1920 1 1  know,
: 1922 1 1  I
: 1924 1 5  tell
: 1926 1 5  you
: 1928 1 5  know
- 1930
: 1930 1 5 I
: 1932 1 8  tell
: 1934 1 8  you
: 1936 2 1  know,
: 1940 4 1  why
- 1946
: 1952 2 1 Know
: 1956 4 1  why
- 1962
: 1980 1 5 If
: 1982 1 5  we
: 1984 1 5  ev
: 1986 1 5 er
* 1988 2 8  give
: 1992 2 10  up
- 1995
: 1996 1 1 Then
: 1998 1 1  we're
: 2000 1 1  gon
: 2002 1 1 na
: 2004 2 5  die
- 2008
: 2012 1 -2 Look
: 2014 1 -2  me
: 2016 1 -2  in
: 2018 1 -2  the
: 2020 2 1  eye,
: 2024 1 1  ba
: 2026 1 1 by
: 2028 2 1  don't
: 2032 4 1  lie 
- 2038
: 2088 1 1 Ba
: 2090 1 1 by
: 2092 2 1  don't
: 2096 4 1  lie 
- 2102
: 2152 2 8 Yeah
: 2156 2 8  yeah
: 2160 4 5  yeah
- 2166
: 2216 1 1 Ba
: 2218 1 1 by
: 2220 2 1  don't
: 2224 4 1  lie 
E