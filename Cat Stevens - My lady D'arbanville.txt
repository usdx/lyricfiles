#TITLE:My lady D'arbanville
#ARTIST:Cat Stevens
#MP3:Cat Stevens - My lady D'arbanville.mp3
#COVER:Cat Stevens - My lady D'arbanville [CO].jpg
#BACKGROUND:Cat Stevens - My lady D'arbanville [BG].jpg
#BPM:179
#GAP:12200
: 0 5 71 My
: 5 6 71  La
: 11 5 69 dy
: 16 6 67  d'Ar
: 22 3 66 ban
: 25 10 64 ville,
- 39
: 65 5 64 Why
: 70 3 66  do
: 73 2 67  you
: 75 5 69  sleep
: 80 6 71  so
: 86 13 69  still?
- 108
: 134 4 71 I'll
: 138 7 71  wake
: 145 6 69  you
: 151 3 67  to
: 154 5 66 mo
: 159 10 64 rrow,
- 175
: 201 4 64 And
: 205 3 66  you
: 208 3 67  will
: 211 6 69  be
: 217 3 67  my
: 220 11 66  fill,
- 234
: 237 4 62 Yes
: 241 3 64  you
: 244 2 66  will
: 246 3 67  be
: 249 7 62  my
: 256 12 64  feel.
- 274
: 282 5 71 My
: 287 6 71  La
: 293 6 69 dy
: 299 4 67  d'Ar
: 303 4 66 ban
: 307 11 64 ville,
- 324
: 343 4 62 Why
: 347 3 66  does
: 350 3 67  it
: 353 4 69  grieve
: 357 4 71  me
: 361 14 69  so ?
- 382
: 400 2 64 But
: 402 4 71  your
: 406 4 71  heart
: 410 6 69  seems
: 416 2 67  so
: 418 6 66  si
: 424 9 64 lent.
- 440
: 460 3 62 Why
: 463 2 66  do
: 465 3 67  you
: 468 5 69  breath
: 473 3 67  so
: 476 7 66  low ?
- 485
: 488 3 62 Why
: 491 3 64  do
: 494 3 66  you
: 497 3 67  breath
: 500 6 62  so
: 506 7 64  low ?
- 515
: 517 3 71 My
: 520 7 71  La
: 527 6 69 dy
: 533 3 67  d'Ar
: 536 4 66 ban
: 540 10 64 ville,
- 556
: 575 4 62 Why
: 579 3 66  do
: 582 2 67  you
: 584 4 69  sleep
: 588 6 71  so
: 594 13 69  still ?
- 612
: 631 4 71 I'll
: 635 5 71  wake
: 640 5 69  you
: 645 3 67  to
: 648 5 66 mo
: 653 11 64 rrow,
- 670
: 688 3 62 And
: 691 3 66  you
: 694 2 67  will
: 696 3 69  be
: 699 3 67  my
: 702 8 66  fill.
- 712
: 714 4 62 Yes
: 718 2 64  you
: 720 4 66  will
: 724 2 67  be
: 726 5 62  my
: 731 25 64  fill.
- 762
: 889 5 71 My
: 894 6 71  La
: 900 6 69 dy
: 906 4 67  d'Ar
: 910 3 66 ban
: 913 11 64 ville,
- 930
: 952 4 64 You
: 956 3 66  look
: 959 4 67  so
: 963 7 69  cold
: 970 2 71  to
: 972 8 69 night.
- 990
: 1012 4 71 Your
: 1016 8 71  lips
: 1024 4 69  feel
: 1028 4 67  like
: 1032 4 66  win
: 1036 10 64 ter.
- 1052
: 1074 3 64 Your
: 1077 4 66  skin
: 1081 3 67  has
: 1084 6 69  turned
: 1090 2 67  to
: 1092 9 66  white.
- 1103
: 1106 3 62 Your
: 1109 4 64  skin
: 1113 3 66  has
: 1116 9 67  turned
: 1125 3 62  to
: 1128 12 64  white.
- 1144
: 1150 5 71 My
: 1155 7 71  La
: 1162 6 69 dy
: 1168 3 67  d'Ar
: 1171 4 66 ban
: 1175 11 64 ville,
- 1192
: 1211 4 62 Why
: 1215 2 66  do
: 1217 2 67  you
: 1219 4 69  sleep
: 1223 6 71  so
: 1229 13 69  still?
- 1248
: 1268 4 71 I'll
: 1272 5 71  wake
: 1277 4 69  you
: 1281 4 67  to
: 1285 4 66 mo
: 1289 10 64 rrow,
- 1305
: 1324 3 62 And
: 1327 2 66  you
: 1329 3 67  will
: 1332 4 69  be
: 1336 3 67  my
: 1339 9 66  fill.
- 1350
: 1351 4 62 Yes
: 1355 2 64  you
: 1357 3 66  will
: 1360 3 67  be
: 1363 5 62  my
: 1368 8 64  fill.
- 1378
: 1379 3 71 La
: 1382 5 71  la
: 1387 5 69  la
: 1392 4 67  la
: 1396 4 66  la
: 1400 10 64  la
- 1416
: 1436 3 62 la
: 1439 3 66  la
: 1442 3 67  la
: 1445 3 69  la
: 1448 5 71  la
: 1453 11 69  la
- 1473
: 1491 3 71 la
: 1494 5 71  la
: 1499 5 69  la
: 1504 4 67  la
: 1508 3 66  la
: 1511 19 64  la
- 1538
: 1546 3 62 la
: 1549 4 66  la
: 1553 3 67  la
: 1556 3 69  la
: 1559 2 67  la
: 1561 7 66  la
- 1571
: 1573 4 62 la
: 1577 3 64  la
: 1580 3 66  la
: 1583 3 67  la
: 1586 4 62  la
: 1590 52 64  la.
- 1644
: 1646 4 71 My
: 1650 5 71  La
: 1655 5 69 dy
: 1660 4 67  d'Ar
: 1664 4 66 ban
: 1668 12 64 ville,
- 1690
: 1704 3 62 Why
: 1707 3 66  do
: 1710 3 67  you
: 1713 4 69  greet
: 1717 3 71  me
: 1720 16 69  so ?
- 1744
: 1758 2 64 But
: 1760 3 71  your
: 1763 5 71  heart
: 1768 5 69  seems
: 1773 3 67  so
: 1776 5 66  si
: 1781 10 64 lent.
- 1800
: 1816 4 62 Why
: 1820 2 66  do
: 1822 3 67  you
: 1825 4 69  breath
: 1829 4 67  so
: 1833 8 66  low ?
- 1843
: 1844 4 62 Why
: 1848 2 64  do
: 1850 3 66  you
: 1853 4 67  breath
: 1857 4 62  so
: 1861 11 64  low ?
- 1873
: 1874 2 71 I
: 1876 6 71  loved
: 1882 4 69  you
: 1886 4 67  my
: 1890 5 66  La
: 1895 10 64 dy.
- 1916
: 1930 3 62 Though
: 1933 2 66  in
: 1935 3 67  your
: 1938 4 69  grave
: 1942 5 71  you
: 1947 13 69  lie,
- 1969
: 1984 6 71 I'll
: 1990 4 71  al
: 1994 5 69 ways
: 1999 3 67  be
: 2002 4 66  with
: 2006 16 64  you.
- 2030
: 2041 3 63 This
: 2044 4 65  rose
: 2048 5 67  will
: 2053 2 69  ne
: 2055 2 67 ver
: 2057 7 66  die.
- 2066
: 2068 4 62 This
: 2072 3 64  rose
: 2075 4 66  will
: 2079 3 67  ne
: 2082 2 65 ver
: 2084 10 64  die.
- 2095
: 2096 3 71 I
: 2099 6 71  loved
: 2105 5 69  you
: 2110 3 67  my
: 2113 4 66  La
: 2117 8 64 dy.
- 2132
: 2152 3 63 Though
: 2155 3 66  in
: 2158 3 67  your
: 2161 5 69  grave
: 2166 5 71  you
: 2171 13 69  lie,
- 2190
: 2205 6 71 I'll
: 2211 5 71  al
: 2216 5 69 ways
: 2221 3 67  be
: 2224 3 66  with
: 2227 10 64  you.
- 2244
: 2260 5 63 This
: 2265 4 65  rose
: 2269 4 67  will
: 2273 3 69  ne
: 2276 2 67 ver
: 2278 8 66  die.
- 2288
: 2289 3 62 This
: 2292 4 64  rose
: 2296 4 66  will
: 2300 2 67  ne
: 2302 2 65 ver
: 2304 24 64  die.
E
