#TITLE:Friedenspanzer
#ARTIST:Die Δrzte
#LANGUAGE:German
#GENRE:Rock
#CREATOR:Eggman
#MP3:Die Δrzte - Friedenspanzer.mp3
#COVER:Die Δrzte - Friedenspanzer[CO].jpg
#VIDEO:Die Δrzte - Friedenspanzer.avi
#BPM:210
#GAP:19100
: 0 3 -3 Die
: 4 2 -3  Ta
: 7 3 -5 ges
: 11 3 -7 schau
: 15 3 -5  ist
: 19 3 -3  nicht
: 23 2 -5  mein
: 26 7 -7  Fall
- 35
: 64 2 -3 nichts
: 66 2 -3  als
: 68 2 -5  Mord
: 70 2 -7  und
: 72 2 -5  Mas
: 74 2 -3 sen
: 76 2 -5 ster
: 78 2 -7 ben
: 81 2 -5  ό
: 84 3 -3 ber
: 88 3 -5 all
: 92 4 -7 ~
- 98
: 123 2 -3 Die
: 126 3 -3  Hunde
: 130 1 -4  des
: 132 2 -5  Krie
: 134 2 -3 ges
: 137 1 -5  wie
: 138 1 -7 der
: 140 3 -5  los
: 144 2 -3 ge
: 147 4 -5 las
: 152 4 -7 sen
- 158
: 182 2 -7 Wenn
: 184 2 -5  Schw
: 186 2 -3 es
: 188 2 -5 tern
: 191 1 -7  und
: 193 2 -5  Brό
: 196 2 -3 der
: 198 2 -5  sich
: 201 3 -5  wie
: 204 3 -3 der
: 208 5 -5  has
: 213 6 -7 sen
- 221
: 254 5 -15 Wenn
: 261 6 -12  Bom
: 269 5 -8 ben
: 276 7 -10  fallen,
: 314 6 -15  Ter
: 321 6 -12 ror
: 329 7 -8  re
: 337 8 -10 giert
- 347
: 374 2 -8 und
: 378 2 -8  der
: 382 2 -8  Mensch
: 386 2 -8  im
: 390 2 -8  All
: 394 3 -8 ge
: 398 6 -5 mei
: 406 4 -7 nen
: 436 6 -15  zum
: 443 6 -12  Hass
: 450 7 -8  ten
: 458 9 -10 diert
- 469
: 488 3 3 Das
: 492 2 3  was
: 496 2 3  mir
: 499 2 3  da
: 501 3 3 zu
: 506 2 1  ein
: 509 6 0 fδllt
- 519
: 519 3 3 fόr
: 523 2 3  die
: 526 3 3  Ret
: 529 2 3 tung
: 533 2 3  die
: 536 3 1 ser
: 540 5 0  Welt
- 549
: 549 6 0 Frie
: 557 6 -4 dens
: 565 5 0 pan
: 572 16 -4 zer
- 590
: 610 2 3 Er
: 613 3 3  schiesst
: 617 2 3  Lie
: 619 3 3 be
: 623 3 3  in
: 627 3 1  dein
: 631 4 0  Herz
- 637
: 640 3 3 bringt
: 644 3 3  den
: 648 2 3  Frie
: 650 3 3 den
: 654 3 3  oh
: 658 3 1 ne
: 662 4 0  Schmerz
- 668
: 671 5 0 Frie
: 678 6 -4 dens
: 686 6 0 pan
: 694 21 -4 zer,
: 731 6 0  Frie
: 739 6 -4 dens
: 747 6 0 pan
: 755 21 -4 zer
- 778
: 909 2 -3 Er
: 912 3 -3  schieίt
: 916 3 -3  mit
: 920 3 -3  Blu
: 924 2 -3 men
: 928 2 -2  statt
: 931 3 -3  Gra
: 935 3 -5 na
: 939 5 -4 ten
- 946
: 973 2 -7 er
: 976 2 -5  tri
: 978 2 -3 fft
: 981 2 -5  je
: 984 3 -5 den
: 988 3 -3  auch
: 992 3 -5  die
: 996 2 -3  Har
: 999 3 -5 ten
: 1003 4 -4 ~
- 1009
: 1027 2 -5 an
: 1029 2 -5 stel
: 1031 1 -5 le
: 1033 2 -3  Gift
: 1036 3 -3 gas
: 1041 3 -5  gibt
: 1045 3 -5  es
: 1049 4 -3  Ro
: 1054 5 -3 sen
: 1060 6 -5 du
: 1067 4 -4 ft
- 1073
: 1094 3 -5 schwδn
: 1097 2 -3 gert
: 1100 2 -5  mit
: 1102 2 -5  Weih
: 1104 2 -3 rauch
: 1106 2 -5  die
: 1108 2 -7  ver
: 1111 4 -5 schmutz
: 1115 4 -3 te
: 1121 5 -5  Lu
: 1126 5 -7 ft
- 1133
: 1164 4 -15 Er
: 1170 5 -12  lδίt
: 1177 5 -8  uns
: 1184 7 -10  nie
: 1223 6 -15 mals
: 1231 6 -12  mehr
: 1239 4 -8  al
: 1245 8 -10 lein
- 1255
: 1283 3 -8 und
: 1287 2 -8  auch
: 1291 2 -8  Du
: 1295 2 -8  und
: 1299 2 -8  ich
: 1303 3 -8  und
: 1307 5 -5  al
: 1313 6 -7 le
- 1321
: 1344 3 -15  wer
: 1347 3 -15 den
: 1351 3 -12  sei
: 1355 2 -12 ne 
: 1359 3 -8 Mu
: 1363 3 -8 ni
: 1367 9 -8 tion
: 1380 11 -10  sein
- 1393
: 1397 3 3 Das
: 1401 3 3  was
: 1405 2 3  mir
: 1407 2 3  da
: 1410 3 3 zu
: 1414 3 1  ein
: 1418 6 0 fδllt
- 1425
: 1427 3 3 fόr
: 1431 3 3  die
: 1435 2 3  Ret
: 1437 3 3 tung
: 1441 2 3  die
: 1444 3 1 ser
: 1448 6 0  Welt
- 1458
: 1458 5 0 Frie
: 1465 6 -4 dens
: 1473 6 0 pan
: 1481 24 -4 zer
- 1507
: 1518 3 3 Er
: 1522 3 3  schiesst
: 1526 2 3  Lie
: 1528 2 3 be
: 1531 3 3  in
: 1535 3 1  dein
: 1539 5 0  Herz
- 1548
: 1548 3 3 bringt
: 1552 3 3  den
: 1556 2 3  Frie
: 1558 3 3 den
: 1562 3 3  oh
: 1565 3 1 ne
: 1569 5 0  Schmerz
- 1578
: 1578 6 0 Frie
: 1586 6 -4 dens
: 1594 5 0 pan
: 1601 19 -4 zer,
: 1639 6 0  Frie
: 1647 6 -4 dens
: 1655 6 0 pan
: 1663 25 -4 zer
- 1690
: 1821 2 -3 Er
: 1825 2 -3  nδht
: 1829 3 -3  das
: 1833 3 -2  O
: 1837 3 0 zon
: 1841 3 -2 loch
: 1845 5 -3  zu
- 1852
: 1881 3 -3 pflanzt
: 1884 2 -3  nen
: 1886 1 -5  neu
: 1887 1 -7 en
: 1889 3 -5  Re
: 1893 3 -3 gen
: 1897 3 0 wald
: 1901 3 -3  im
: 1905 3 -5  Nu
: 1909 9 -3 uh
- 1920
: 1939 2 -3 stopft
: 1941 1 -3  die
: 1942 2 -3  Hun
: 1944 3 -3 gers
: 1947 3 -3 nφ
: 1950 2 -3 te
: 1952 3 -3  mit
: 1955 1 -3  der
: 1956 2 -3  To
: 1958 2 -3 fu
: 1960 3 -5 ka
: 1964 7 -7 no
: 1972 4 -7 ne
- 1978
: 2000 1 -3 er
: 2002 1 -3 klδrt
: 2004 1 -3  die
: 2006 1 -3  gan
: 2008 1 -3 ze
: 2010 1 -3  Welt
: 2012 1 -3  zur
: 2014 2 -3  An
: 2016 2 -3 ti
: 2019 3 0 wal
: 2023 5 -3 fang
: 2030 7 -5 zone
: 2037 4 -3 ~
- 2043
: 2052 3 -3 Selbst
: 2056 3 -3  die
: 2060 2 -3  Neu
: 2064 16 4 tro
: 2085 3 2 nen
: 2089 7 2 bom
: 2097 12 1 be
: 2123 4 1  hat
: 2130 3 1  ver
: 2135 12 -3 sagt
- 2149
: 2172 3 -3 Hier
: 2176 3 -3  ist
: 2180 3 -3  als
: 2184 18 4  Lφ
: 2205 4 2 sung
: 2211 2 2  wirk
: 2215 10 1 lich
: 2233 3 1  nur
: 2238 4 1  noch
: 2244 5 1  eins
: 2251 3 1  ge
: 2256 17 -3 fragt
- 2275
: 2304 3 3 Das
: 2308 3 3  was
: 2312 2 3  mir
: 2315 2 3  da
: 2318 3 3 zu
: 2322 3 1  ein
: 2326 4 0 fδllt
- 2332
: 2335 3 3 fόr
: 2339 3 3  die
: 2343 2 3  Ret
: 2345 3 3 tung
: 2349 3 3  die
: 2353 3 1 ser
: 2357 4 0  Welt
- 2365
: 2365 6 0 Frie
: 2373 7 -4 dens
: 2381 7 0 pan
: 2389 18 -4 zer
- 2409
: 2426 3 3 Er
: 2430 3 3  schiesst
: 2434 2 3  Lie
: 2436 3 3 be
: 2440 3 3  in
: 2444 3 1  dein
: 2448 4 0  Herz
- 2456
: 2456 3 3 bringt
: 2460 3 3  den
: 2464 2 3  Frie
: 2466 3 3 den
: 2470 3 3  oh
: 2474 2 1 ne
: 2477 4 0  Schmerz
- 2483
: 2486 6 0 Frie
: 2494 6 -4 dens
: 2502 6 0 pan
: 2510 18 -4 zer
- 2530
: 2608 6 0 Frie
: 2616 6 -4 dens
: 2624 6 0 pan
: 2632 15 -4 zer
- 2649
: 2729 6 0 Frie
: 2737 4 -4 dens
: 2743 8 0 pan
: 2753 24 -4 zer
- 2779
: 2789 6 0 Frie
: 2797 6 -4 dens
: 2805 6 0 pan
: 2813 8 -4 zer
- 2823
: 2850 6 0 Frie
: 2858 6 -4 dens
: 2866 6 0 pan
: 2874 8 -4 zer
- 2884
: 2911 6 0 Frie
: 2919 6 -4 dens
: 2927 6 0 pan
: 2935 7 -4 zer
- 2944
: 2972 5 0 Frie
: 2979 6 -4 dens
: 2987 6 0 pan
: 2995 8 -4 zer
E
