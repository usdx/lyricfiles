#ARTIST:The Script
#TITLE:Six Degrees Of Seperation
#MP3:The Script - Six Degrees Of Seperation.mp3
#VIDEO:The Script - Six Degrees Of Seperation.mp4
#AUTHOR:Nicolai
#EDITION:Songs von aac
#GENRE:Pop
#LANGUAGE:English
#BPM:394,6
#GAP:252
: -1 2 -1 You've
: 3 8 4  read
: 14 2 6  the
: 20 9 6  books,
- 30
: 32 6 -1 You've
: 46 7 4  watched
: 56 2 6  the
: 61 15 6  shows,
- 78
: 88 3 6 What's
: 94 3 6  the
: 100 8 6  best
: 111 6 8  way
: 121 13 3  no
: 137 6 4  one
: 147 8 6  knows,
: 158 6 -1  yeah,
- 166
: 174 9 4 Me
: 185 2 6 di
: 190 6 6 tate,
: 200 8 -1  get
: 217 8 4  hyp
: 228 2 6 no
: 233 15 6 tized.
- 250
: 259 4 4 A
: 265 3 6 ny
: 271 4 6 thing
: 276 3 6  to
: 281 5 6  take
* 292 7 11  from
: 302 3 8  your
: 309 8 6  mind.
- 318
: 320 3 4 But
: 324 3 3  it
* 332 13 1  won't
- 347
: 365 5 8 Go
: 371 7 6 ~
- 380
: 409 3 6 You're
: 415 9 6  do
: 425 5 6 ing
: 431 3 6  all
: 436 4 6  these
: 442 9 6  things
: 452 5 6  out
: 458 2 6  of
: 463 7 6  des
: 473 4 4 pe
: 480 3 8 ra
: 488 8 4 tion,
- 498
* 502 9 1 Ohhh
- 513
: 536 5 8 Woah
: 542 6 6 ~,
- 550
: 578 5 6 You're
: 584 8 6  going
: 595 3 6  through
: 600 4 6  six
: 606 3 6  de
: 611 10 6 grees
* 622 7 6  of
: 633 4 6  se
: 639 8 4 pa
: 650 5 8 ra
: 660 12 4 tion.
- 674
: 682 2 -1 You
: 687 7 4  hit
: 697 3 6  the
: 702 8 6  drink,
: 712 6 -1  you
: 728 9 4  take
: 739 2 6  a
: 744 10 6  toke.
- 756
: 771 4 6 Watch
: 776 3 6  the
: 781 8 6  past
: 793 6 8  go
: 804 13 3  up
: 820 5 4  in
: 829 11 6  smoke.
- 842
: 858 7 4 Fake
: 867 2 6  a
* 873 8 6  smile,
: 883 4 -1  yeah,
: 898 9 4  lie
: 909 3 6  and
: 915 9 6  say
- 925
: 926 4 4 I'm
: 931 4 4  bet
: 936 3 6 ter
: 942 5 6  now
: 948 4 6  than
: 955 2 6  e
* 958 4 6 ver,
: 965 3 4  and
: 969 3 4  your
* 975 8 11  life's
: 986 3 8  o
: 992 6 6 kay
- 1000
: 1002 3 4 Well
: 1006 5 3  it's
: 1016 11 1  not.
- 1029
: 1048 5 8 No
* 1055 11 6 ~
- 1068
: 1091 3 6 You're
: 1097 9 6  do
: 1107 5 6 ing
: 1113 3 6  all
: 1118 4 6  these
: 1124 9 6  things
: 1134 5 6  out
: 1140 2 6  of
: 1145 7 6  des
: 1155 4 4 pe
: 1162 3 8 ra
: 1170 8 4 tion,
- 1180
: 1184 9 1 Ohhh
- 1195
: 1218 5 8 Woah
* 1224 6 6 ~,
- 1232
: 1261 5 6 You're
: 1267 8 6  going
: 1278 3 6  through
: 1283 4 6  six
: 1289 3 6  de
: 1294 10 6 grees
* 1305 7 6  of
: 1316 4 6  se
: 1322 8 4 pa
: 1333 5 8 ra
: 1343 12 4 tion.
- 1356
* 1358 12 8 First,
- 1372
: 1385 2 4 You
: 1390 3 4  think
: 1396 3 6  the
: 1401 8 8  worst
: 1412 3 8  is
: 1417 3 6  a
: 1423 5 8  bro
: 1432 3 6 ken
: 1439 10 8  heart
- 1451
: 1469 3 4 What's
: 1475 3 4  gon
: 1481 3 6 na
: 1486 4 8  kill
: 1492 4 8  you
: 1497 4 8  is
: 1502 3 8  the
* 1507 4 11  se
: 1512 6 9 cond
: 1522 12 8  part
- 1536
: 1560 4 4 And
: 1565 3 6  the
* 1571 13 8  third,
- 1586
: 1598 3 4 Is
: 1603 3 4  when
: 1608 4 6  your
: 1615 7 8  world
: 1624 9 8  splits
: 1635 8 9  down
: 1646 3 8  the
: 1651 4 8  mid
: 1657 20 6 dle
- 1679
: 1683 10 4 And
: 1698 11 8  fourth,
- 1711
: 1725 4 4 You're
: 1731 4 4  gon
: 1736 3 6 na
* 1742 8 8  think
: 1752 3 8  that
: 1758 3 6  you
: 1763 8 8  fixed
* 1774 3 9  your
: 1780 14 8 self
- 1796
: 1805 10 8 Fifth,
- 1817
: 1821 4 8 You
: 1827 4 8  see
: 1833 3 8  them
: 1838 3 8  out
: 1843 3 8  with
* 1848 5 11  some
* 1856 7 9 one
* 1866 11 8  else
- 1878
: 1880 4 8 And
: 1886 3 6  the
* 1891 8 8  sixth,
- 1901
: 1906 5 6 Is
: 1913 7 8  when
: 1923 4 8  you
: 1930 2 6  ad
: 1937 7 8 mit
- 1945
: 1945 3 6 That
: 1949 4 6  you
: 1956 4 8  may
: 1961 4 8  have
: 1967 6 8  messed
: 1977 6 9  up
: 1987 2 8  a
: 1992 5 8  lit
: 1998 24 6 tle
: 2026 15 4 ~.
- 2043
: 2046 4 4 (No,
: 2051 4 4  no,
: 2057 3 4  there
: 2061 6 4  ain't
: 2068 7 4  no
* 2078 8 6  help,
: 2089 4 4  it's
: 2094 4 4  eve
: 2099 3 4 ry
: 2104 6 4  man
: 2111 4 4  for
: 2116 2 4  him
: 2122 9 6 self)
- 2133
: 2217 4 4 (No,
: 2222 4 4  no,
: 2228 3 4  there
: 2232 6 4  ain't
: 2239 6 4  no
: 2249 8 6  help,
: 2260 4 4  it's
: 2265 4 4  eve
: 2270 3 4 ry
: 2275 6 4  man
: 2282 4 4  for
: 2287 2 4  him
: 2293 9 6 self)
- 2304
: 2384 5 1 You
: 2392 7 4  tell
: 2402 3 6  your
: 2408 10 6  friends,
: 2420 5 1  yeah,
: 2435 8 4  stran
: 2444 4 6 gers
: 2451 7 6  too,
- 2460
: 2478 3 6 A
: 2482 4 6 ny
: 2488 4 6 one
: 2494 3 6 'll
: 2499 5 8  throw
: 2505 4 3  an
: 2511 7 3  arm
: 2521 3 3  a
: 2526 7 4 round
: 2537 7 6  you,
: 2547 5 -1  yeah
- 2554
: 2562 7 4 Ta
: 2573 3 6 rot
* 2581 14 6  cards,
- 2597
: 2605 8 4 Gems
: 2616 2 6  and
: 2621 8 6  stones,
- 2631
: 2633 3 6 Be
: 2637 4 6 lie
: 2643 3 6 ving
: 2648 4 6  all
: 2654 3 6  that
: 2660 4 6  shit
: 2665 2 6  is
: 2670 4 4  gon
: 2676 3 4 na
* 2682 6 11  heal
: 2691 2 8  your
: 2696 9 6  soul.
- 2706
: 2707 4 4 We'll
: 2712 5 3  it's
: 2723 14 1  not,
- 2739
: 2754 5 8 No
: 2760 11 6 ~
- 2773
: 2796 5 6 You're
: 2804 7 6  on
: 2813 3 6 ly
: 2819 4 6  do
: 2824 3 6 ing
* 2830 8 6  things
: 2841 4 6  out
: 2846 3 6  of
: 2853 6 6  des
: 2862 3 4 pe
: 2868 5 8 ra
: 2877 9 4 tion,
- 2888
: 2890 11 1 Ohhh
- 2903
: 2925 5 8 Woah
: 2933 9 6 ~,
- 2944
: 2969 3 6 You're
: 2974 7 6  goin'
: 2984 3 6  through
: 2989 4 6  six
: 2995 3 6  de
: 3001 7 6 grees
: 3011 7 6  of
: 3023 3 6  se
: 3027 10 4 pa
: 3039 7 8 ra
: 3048 11 4 tion.
- 3061
* 3064 12 8 First,
- 3078
: 3091 2 4 You
: 3096 3 4  think
: 3102 3 6  the
: 3107 8 8  worst
: 3118 3 8  is
: 3123 3 6  a
: 3129 5 8  bro
: 3138 3 6 ken
: 3145 10 8  heart
- 3157
: 3175 3 4 What's
: 3181 3 4  gon
: 3187 3 6 na
: 3192 4 8  kill
: 3198 4 8  you
: 3203 4 8  is
: 3208 3 8  the
* 3213 4 11  se
: 3218 6 9 cond
: 3228 12 8  part
- 3242
: 3266 4 4 And
: 3271 3 6  the
* 3277 13 8  third,
- 3292
: 3304 3 4 Is
: 3309 3 4  when
: 3314 4 6  your
: 3321 7 8  world
: 3330 9 8  splits
: 3341 8 9  down
: 3352 3 8  the
: 3357 4 8  mid
: 3363 20 6 dle
- 3385
: 3389 10 4 And
: 3404 11 8  fourth,
- 3417
: 3431 4 4 You're
: 3437 4 4  gon
: 3442 3 6 na
* 3448 8 8  think
: 3458 3 8  that
: 3464 3 6  you
: 3469 8 8  fixed
* 3480 3 9  your
: 3486 14 8 self
- 3502
: 3511 10 8 Fifth,
- 3523
: 3527 4 8 You
: 3533 4 8  see
: 3539 3 8  them
: 3544 3 8  out
: 3549 3 8  with
* 3554 5 11  some
* 3562 3 11 one
* 3566 3 13 ~
* 3572 11 8  else
- 3584
: 3586 4 8 And
: 3592 3 6  the
* 3597 8 8  sixth,
- 3607
: 3612 5 6 Is
: 3619 7 8  when
: 3629 4 8  you
: 3636 2 6  ad
: 3643 7 8 mit
- 3651
: 3651 3 6 That
: 3655 4 6  you
: 3662 4 8  may
: 3667 4 8  have
: 3673 6 8  messed
: 3683 6 9  up
: 3693 2 8  a
: 3698 5 8  lit
: 3704 24 6 tle
: 3732 15 4 ~.
- 3749
: 3773 3 1 No
: 3779 5 1  there's
: 3786 5 1  no
* 3796 7 8  star
: 3804 5 6 ting
: 3811 5 4  o
: 3820 7 3 ver
: 3831 19 1 ~,
- 3852
: 3858 5 1 Wit
: 3865 5 1 hout
: 3876 7 8  fin
: 3885 5 6 ding
: 3892 7 4  clo
: 3903 9 6 sure
: 3917 14 1 ~,
- 3933
: 3944 3 1 You'd
: 3949 4 1  take
: 3954 4 1  them
: 3960 3 1  back,
- 3964
: 3966 3 1 No
: 3971 3 1  he
: 3976 3 1 si
: 3981 4 4 ta
* 3988 8 1 tion,
- 3998
: 4002 5 1 That's
: 4008 2 1  when
: 4011 1 1  you
: 4013 4 1  know
- 4018
: 4019 3 1 You've
: 4024 4 1  reached
: 4030 2 1  the
: 4035 4 1  sixth
: 4040 3 1  de
: 4045 5 1 gree
: 4051 2 1  of
: 4056 3 1  se
: 4060 5 1 pa
: 4067 4 4 ra
: 4072 6 1 tion
- 4080
: 4088 22 1 No
: 4115 4 1  no
: 4121 6 1  there's
: 4129 4 1  no
: 4139 6 8  star
: 4146 7 6 ting
: 4154 8 4  o
: 4165 6 3 ver
: 4175 16 1 ~,
- 4193
: 4200 4 1 Wit
: 4206 5 1 hout
: 4219 6 8  fin
: 4226 6 6 ding
: 4234 7 4  clo
: 4244 11 6 sure
: 4259 15 1 ~,
- 4276
: 4285 3 1 You'd
: 4291 3 1  take
: 4296 3 1  them
: 4301 4 1  back,
- 4306
* 4307 2 1 No
: 4312 3 1  he
: 4317 3 1 si
: 4322 5 4 ta
: 4328 8 1 tion,
- 4338
: 4344 4 1 That's
: 4349 2 1  when
: 4352 1 1  you
: 4354 4 1  know
- 4359
: 4360 3 1 You've
: 4365 4 1  reached
: 4371 3 1  the
: 4376 3 1  sixth
: 4381 4 1  de
: 4387 3 1 gree
: 4391 3 1  of
: 4397 3 1  se
: 4402 3 1 pa
: 4407 5 4 ra
: 4414 6 1 tion
- 4422
* 4429 12 8 First,
- 4443
: 4456 2 4 You
: 4461 3 4  think
: 4467 3 6  the
: 4472 8 8  worst
: 4483 3 8  is
: 4488 3 6  a
: 4494 5 8  bro
: 4503 3 6 ken
: 4510 10 8  heart
- 4522
: 4540 3 4 What's
: 4546 3 4  gon
: 4552 3 6 na
: 4557 4 8  kill
: 4563 4 8  you
: 4568 4 8  is
: 4573 3 8  the
* 4578 4 11  se
: 4583 6 9 cond
: 4593 12 8  part
- 4607
: 4631 4 4 And
: 4636 3 6  the
* 4642 13 8  third,
- 4657
: 4669 3 4 Is
: 4674 3 4  when
: 4679 4 6  your
: 4686 7 8  world
: 4695 9 8  splits
: 4706 8 9  down
: 4717 3 8  the
: 4722 4 8  mid
: 4728 20 6 dle
- 4750
: 4754 10 4 And
: 4769 11 8  fourth,
- 4782
: 4796 4 4 You're
: 4802 4 4  gon
: 4807 3 6 na
* 4813 8 8  think
: 4823 3 8  that
: 4829 3 6  you
: 4834 8 8  fixed
* 4845 3 9  your
: 4851 14 8 self
- 4867
: 4876 10 8 Fifth,
- 4888
: 4892 4 8 You
: 4898 4 8  see
: 4904 3 8  them
: 4909 3 8  out
: 4914 3 8  with
* 4919 5 11  some
* 4927 7 11 one
* 4937 11 13  else
- 4949
: 4951 4 11 And
: 4957 3 11  the
* 4962 8 11  sixth,
- 4972
: 4977 5 8 Is
: 4984 7 9  when
: 4994 4 8  you
: 5001 2 6  ad
: 5008 7 8 mit
- 5016
: 5016 3 6 That
: 5020 4 6  you
: 5027 4 8  may
: 5032 4 8  have
: 5038 6 8  messed
: 5048 6 9  up
: 5058 2 8  a
: 5063 5 8  lit
: 5069 24 6 tle
: 5097 15 4 ~.
- 5114
: 5117 4 4 (No,
: 5122 4 4  no,
: 5128 3 4  there
: 5132 6 4  ain't
: 5139 7 4  no
* 5149 8 6  help,
: 5160 4 4  it's
: 5165 4 4  eve
: 5170 3 4 ry
: 5175 6 4  man
: 5182 4 4  for
: 5187 2 4  him
: 5193 9 6 self)
- 5204
: 5288 4 4 (No,
: 5293 4 4  no,
: 5299 3 4  there
: 5303 6 4  ain't
: 5310 7 4  no
* 5320 8 6  help,
: 5331 4 4  it's
: 5336 4 4  eve
: 5341 3 4 ry
: 5346 6 4  man
: 5353 4 4  for
: 5358 2 4  him
: 5364 9 6 self)
- 5375
: 5457 4 4 (No,
: 5462 4 4  no,
: 5468 3 4  there
: 5472 6 4  ain't
: 5479 7 4  no
* 5489 8 6  help,
: 5500 4 4  it's
: 5505 4 4  eve
: 5510 3 4 ry
: 5515 6 4  man
: 5522 4 4  for
: 5527 2 4  him
: 5533 9 6 self)
- 5544
: 5628 4 4 (No,
: 5633 4 4  no,
: 5639 3 4  there
: 5643 6 4  ain't
: 5650 7 4  no
* 5660 8 6  help,
: 5671 4 4  it's
: 5676 4 4  eve
: 5681 3 4 ry
: 5686 6 4  man
: 5693 4 4  for
* 5698 2 9  him
* 5704 9 8 self)

