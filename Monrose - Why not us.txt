#TITLE:Why Not Us
#ARTIST:Monrose
#MP3:Monrose - Why not us.mp3
#VIDEO:Monrose - Why not us.mp4
#COVER:Monrose - Why not us.jpg
#BPM:320.79
#GAP:14368
#ENCODING:UTF8
#PREVIEWSTART:37,902
#LANGUAGE:English
#GENRE:Pop
#EDITION:Holy_Shit
#CREATOR:Fepo
: 0 2 -6 When
: 4 2 -2  we
: 8 2 -2  first
: 12 2 -2  met
: 16 2 -2  you
: 20 3 -6  said
- 24
: 24 2 -6 It's
: 28 2 -2  com
: 32 2 -2 pli
: 36 5 -2 cate
: 44 2 3  take
: 48 2 -2  it
: 52 5 -2  slow
- 59
: 64 2 -1 Dee
: 68 2 -1 per
: 72 2 -1  the
: 76 2 1  wa
: 80 2 -1 ter
: 84 2 -4  goes
: 88 2 -1  the
: 92 2 -1  more
: 96 2 -1  you
: 100 2 -1  wan
: 104 2 -1 na
: 108 2 -1  dip
: 112 2 -1  your
: 116 5 -1  toe
- 123
: 128 2 -4 I
: 132 3 -4  coul
: 136 2 -4 dn't
: 140 2 -2  get
: 144 2 -4  e
: 148 3 -6 nough
- 152
: 152 2 -6 I
: 156 2 -4  wan
: 160 2 -4 ted
: 164 2 -4  you
: 168 2 -6  right
: 172 4 -4  there
: 180 3 -4  and
: 188 8 -2  then
- 198
: 256 2 -2 Ig
: 260 2 -2 no
: 264 2 -2 ring
: 268 3 -2  all
: 273 2 -4  the
: 276 3 -6  signs
: 280 2 -4  I
: 284 2 -2  thought
: 288 2 -2  that
: 292 2 -2  you
: 296 2 -2  and
: 300 2 3  I
: 304 2 -2  were
: 308 4 -2  cool
- 314
: 321 2 -1 Said
: 325 2 -1  I
: 329 2 -1  was
: 333 2 -1  beau
: 337 2 -1 ti
: 341 2 -4 ful
: 345 2 -1  you
: 349 2 -1  put
: 353 2 -1  me
: 357 2 -1  on
: 361 2 -1  a
: 365 2 3  pe
: 369 2 -1 des
: 373 7 -1 tal
- 382
: 384 2 -4 I
: 388 2 -4  would
: 392 2 -4  do
: 396 2 -2  a
: 400 2 -4 ny
: 404 2 -4 thing
: 408 2 -4  to
: 412 2 -4  get
: 416 2 -4  back
: 420 4 -4  to
: 425 2 -6  those
: 429 6 -4  days
: 440 4 -1  a
: 446 10 -2 gain
- 458
: 508 14 3 Don't
* 540 14 5  know
* 556 12 6  what's
: 572 15 3  wrong
- 589
: 604 15 8 Want
: 621 11 6  to
: 635 14 5  stay
- 651
: 668 14 2 In
: 684 12 5  your
: 700 15 3  arms
- 717
: 774 2 6 If
: 778 2 6  two
: 782 2 6  peo
: 786 2 6 ple
: 790 4 5  can
: 796 7 5  have
: 805 3 3  it
: 811 6 3  all
- 819
: 837 3 6 Live
: 842 2 3  the
* 846 6 6  dream
: 854 4 3  and
* 861 6 8  break
: 869 2 6  the
: 873 12 5  rules
- 887
: 909 5 6 Tell
: 917 9 6  me
: 941 5 6  why
: 950 6 5  not
: 958 10 3  us
- 970
: 1031 2 6 If
: 1035 2 6  two
: 1039 2 6  peo
: 1043 2 5 ple
: 1047 2 5  can
: 1055 6 5  fall
: 1063 2 3  in
: 1067 15 3  Love
- 1084
: 1093 4 6 Des
: 1098 3 3 ti
: 1103 6 6 ny
: 1113 4 3  should
: 1119 6 8  be
: 1127 2 6  e
: 1131 10 5 nough
- 1143
: 1162 2 3 So
: 1167 4 6  tell
: 1174 9 6  me
: 1198 5 6  why
: 1207 6 5  not
: 1215 10 3  us
- 1227
: 1269 9 3 If
: 1280 12 6  this
: 1296 12 6  ain't
: 1311 5 6  right
: 1319 5 6  what
: 1327 6 6  are
: 1335 6 11  we
: 1343 7 10  fi
: 1352 5 6 ~gh
: 1360 12 6 ting
: 1376 7 6  for
- 1385
: 1391 5 6 Like
: 1400 6 10  two
: 1408 12 8  worlds
: 1424 9 5  at
: 1438 7 5  war
- 1447
: 1455 6 5 What
: 1464 6 8  the
: 1472 10 6  heart
: 1487 10 5  beats
: 1503 13 3  for
: 1520 6 5  is
: 1534 12 6  Love
- 1548
: 1551 13 6 Turns
: 1567 6 6  eve
: 1575 6 6 ry
: 1583 4 6 thing
: 1591 6 11  to
: 1599 12 10  dust,
: 1615 10 6  oh
- 1627
: 1668 2 -2 We're
: 1672 2 -2  out
: 1676 2 -2  for
: 1680 2 -2  din
: 1684 2 -4 ner
: 1688 2 -6  and
: 1692 2 -4  I
: 1696 2 -2  no
: 1700 2 -2 tice
: 1704 2 -2  you
: 1708 2 -2  don't
: 1712 2 3  look
: 1716 2 -2  my
: 1720 5 -2  way
- 1727
: 1732 2 -1 Re
: 1736 2 -1 mem
: 1740 2 -1 ber
: 1744 2 -1  when
: 1748 2 -2  you
: 1752 2 -4  used
: 1756 2 -2  to
: 1760 2 -1  hang
: 1764 2 -1  on
: 1768 2 -1  eve
: 1772 2 -1 ry
: 1776 2 3  word
: 1780 2 -1  I
: 1784 6 -1  say
- 1792
: 1796 2 -6 And
: 1800 2 -4  com
: 1804 2 -4 pli
: 1808 2 -4 ment
: 1812 2 -6  me
: 1816 2 -7  on
: 1820 2 -6  a
: 1824 2 -4  dress
: 1832 2 -4  I
: 1836 2 -6  would
: 1840 6 -4  choose
: 1852 2 -1  to
: 1856 8 -2  wear
- 1866
: 1920 14 3 Don't
* 1952 14 5  know
* 1968 12 6  what's
: 1984 15 3  wrong
- 2001
: 2016 15 8 Want
: 2033 11 6  to
: 2047 14 5  stay
- 2063
: 2080 14 2 In
: 2096 12 5  your
: 2112 16 3  arms
- 2130
: 2146 12 10 In
: 2160 12 8  your
: 2176 7 6  arms
- 2184
: 2185 2 6 If
: 2189 2 6  two
: 2193 2 6  peo
: 2197 2 6 ple
: 2201 4 5  can
: 2207 7 5  have
: 2216 3 3  it
: 2222 6 3  all
- 2230
: 2248 3 6 Live
: 2253 2 3  the
* 2257 6 6  dream
: 2265 4 3  and
* 2272 6 8  break
: 2280 2 6  the
: 2284 12 5  rules
- 2298
: 2320 5 6 Tell
: 2328 9 6  me
* 2352 5 6  why
* 2361 6 5  not
: 2369 10 3  us
- 2381
: 2443 2 6 If
: 2447 2 6  two
: 2451 2 6  peo
: 2455 2 5 ple
: 2459 2 5  can
: 2467 6 5  fall
: 2475 2 3  in
: 2479 15 3  Love
- 2496
: 2505 4 6 Des
: 2511 2 3 ti
: 2515 6 6 ny
: 2525 4 3  should
: 2531 6 8  be
: 2539 2 6  e
: 2543 10 5 nough
- 2555
: 2574 2 3 So
: 2579 4 6  tell
: 2586 9 6  me
: 2610 5 6  why
: 2619 6 5  not
: 2627 10 3  us
- 2639
: 2681 6 3 If
: 2690 12 6  this
: 2707 11 6  ain't
: 2722 5 6  right
: 2730 5 6  what
: 2738 6 6  are
: 2746 6 11  we
: 2754 7 10  fi
: 2763 5 6 ~gh
: 2771 12 6 ting
: 2786 7 6  for
- 2795
: 2801 5 6 Like
: 2810 6 10  two
: 2818 12 8  worlds
: 2834 9 5  at
: 2850 7 5  war
- 2859
: 2865 6 5 What
: 2874 6 8  the
: 2882 10 6  heart
: 2897 10 5  beats
: 2913 13 3  for
: 2930 6 5  is
: 2944 12 6  Love
- 2958
: 2964 13 6 Turns
: 2980 6 6  eve
: 2988 6 6 ry
: 2996 4 6 thing
: 3004 6 11  to
: 3012 12 10  dust,
: 3028 9 6  oh
- 3039
: 3043 4 10 Why
: 3048 3 11 ~
: 3052 3 10 ~
: 3056 2 8 ~
: 3059 2 8 ~
: 3062 2 6 ~
: 3065 2 5 ~
: 3069 4 3  no
: 3074 2 1 ~t
: 3079 4 3  us
- 3085
: 3148 2 6 If
: 3152 2 6  two
: 3156 2 6  peo
: 3160 2 6 ple
: 3164 4 5  can
: 3170 7 5  have
: 3179 3 3  it
: 3185 6 3  all
- 3193
: 3212 3 6 Live
: 3217 2 3  the
* 3221 6 6  dream
: 3229 4 3  and
* 3236 6 8  break
: 3244 2 6  the
: 3248 12 5  rules
- 3262
: 3283 5 6 Tell
: 3291 9 6  me
: 3315 5 6  why
: 3324 6 5  not
: 3332 10 3  us
- 3344
: 3404 2 6 If
: 3408 2 6  two
: 3412 2 6  peo
: 3416 2 5 ple
: 3420 2 5  can
: 3428 6 5  fall
: 3436 2 3  in
: 3440 15 3  Love
- 3457
: 3467 4 6 Des
: 3472 3 3 ti
: 3477 6 6 ny
: 3487 4 3  should
: 3493 6 8  be
: 3501 2 6  e
: 3505 10 5 nough
- 3517
: 3534 2 3 So
: 3539 4 6  tell
: 3546 9 6  me
: 3570 5 6  why
: 3579 6 5  not
: 3587 10 3  us
- 3599
: 3660 2 6 If
: 3664 2 6  two
: 3668 2 6  peo
: 3672 2 5 ple
: 3676 2 5  can
: 3684 6 5  have
: 3692 2 3  it
: 3696 15 3  all
- 3713
: 3724 3 6 Live
: 3729 2 3  the
* 3733 6 6  dream
: 3741 4 3  and
* 3748 6 8  break
: 3756 2 6  the
: 3760 12 5  rules
- 3774
: 3795 6 6 Tell
: 3804 9 6  me
: 3828 5 6  why
: 3837 6 5  not
: 3845 10 3  us
- 3857
: 3916 2 6 If
: 3920 2 6  two
: 3924 2 6  peo
: 3928 2 5 ple
: 3932 2 5  can
: 3940 6 5  fall
: 3948 2 3  in
: 3952 15 3  Love
- 3969
: 3980 3 6 Des
: 3985 3 3 ti
: 3990 6 6 ny
: 4000 4 3  should
: 4006 6 8  be
: 4014 2 6  e
: 4018 10 5 nough
- 4030
: 4048 2 3 So
: 4053 4 6  tell
: 4060 9 6  me
: 4084 5 6  why
: 4093 6 5  not
: 4101 10 3  us
E