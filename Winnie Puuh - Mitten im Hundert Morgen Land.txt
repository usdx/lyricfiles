#TITLE:Mitten im Hundert Morgen Land
#ARTIST:Winnie Puuh
#LANGUAGE:German
#EDITION:SingStar - Best of Disney
#MP3:Winnie Puuh - Mitten im Hundert Morgen Land.mp3
#COVER:Winnie Puuh - Mitten im Hundert Morgen Land [CO].jpg
#BACKGROUND:Winnie Puuh - Mitten im Hundert Morgen Land [BG].jpg
#VIDEO:Winnie Puuh - Mitten im Hundert Morgen Land [VD#0].avi
#VIDEOGAP:0
#BPM:340
#GAP:2029,41
: 0 9 71 Mit
: 10 7 70 ten
: 19 9 68  im
: 30 18 65  Hun
: 50 10 63 dert
: 60 21 61 Mor
: 81 10 63 gen
: 91 10 61 Wald
- 103 104
: 114 7 70 Liegt
: 124 7 71  Chris
: 137 7 70 to
: 146 9 68 pher
: 155 16 65  Ro
: 171 9 68 bins
* 182 35 70  Welt
- 219 220
: 235 8 70 Und
: 245 7 71  dort
: 255 7 70  ist
: 264 7 68  der
: 274 17 65  Tie
: 292 7 68 re
- 300
* 301 20 73 Auf
: 323 11 70 ent
: 336 14 66 halt
- 352 353
: 365 7 61 Die
: 374 7 63  Chris
: 383 7 65 to
: 392 8 66 pher
: 400 9 65  sich
: 409 9 63  zu
: 418 8 61 ge
: 427 34 66 sellt
- 463 474
: 494 12 70 Der
: 508 7 71  m체r
: 517 2 70 ri
: 521 8 68 sche
* 535 10 65  E
: 547 11 63 sel
- 559
: 561 9 61 Ist
: 573 4 63  sein
: 581 10 61  Freund
- 593 594
: 607 3 70 Und
: 612 6 71  K채n
: 621 5 70 ga
: 633 2 68  die
: 638 9 65  klei
: 651 13 68 ne
: 664 25 70  Ruh
- 691 692
: 700 9 73 Ka
: 712 7 71 nin
: 722 2 70 chen
- 726
: 734 3 68 Und
: 738 6 65  Fer
: 748 4 68 kel
- 754
: 760 4 68 Die
* 766 9 73  Eu
: 779 8 70 le
: 792 10 66  auch
- 804
: 810 4 61 Sein
: 815 5 63  bes
: 822 3 65 ter
: 827 7 66  Freund
: 835 3 66  ist
- 839
: 839 6 65 Win
: 847 3 63 nie
: 851 9 61  der
: 862 23 66  Puuh
- 887 888
: 909 5 65 Win
* 916 2 63 nie
: 920 4 65  der
: 929 5 63  Puuh
- 936 937
: 951 6 65 Win
: 959 2 63 nie
: 962 5 65  der
: 972 5 63  Puuh
- 979 980
: 994 3 61 Knu
: 1000 2 61 del
* 1004 5 71  du
: 1011 3 71 del
: 1014 4 70  rund
- 1020
: 1025 3 68 Ist
: 1030 5 68  der
: 1036 5 66  Ted
: 1045 4 65 dy
: 1054 4 63 b채r
: 1065 9 64  ist
- 1075
: 1076 6 65 Win
: 1083 2 63 nie
: 1087 6 65  der
* 1096 3 63  Puuh
- 1101 1102
: 1114 7 65 Win
: 1121 2 63 nie
: 1124 3 65  der
: 1134 6 63  Puuh
- 1142 1143
: 1154 5 61 Al
: 1161 2 61 le
: 1165 5 71  Kin
: 1172 3 71 der
: 1175 5 70  lie
: 1181 3 70 ben
: 1185 5 68  ihn
: 1194 22 66  sehr
- 1218 1219
* 1230 6 65 Win
: 1236 2 63 nie
: 1239 3 65  der
: 1247 4 63  Puuh
- 1253 1254
: 1264 5 65 Win
: 1269 2 63 nie
: 1272 4 65  der
: 1279 5 63  Puuh
- 1286 1287
: 1298 3 61 Knu
: 1303 2 61 del
: 1306 4 71  du
: 1312 3 71 del
: 1315 3 70  rund
- 1320
: 1322 2 68 Ist
: 1326 4 68  der
: 1331 4 66  Ted
: 1339 3 65 dy
: 1347 3 63 b채r
: 1355 7 64  ist
- 1363
: 1363 3 65 Win
: 1368 2 63 nie
* 1371 3 65  der
: 1379 4 63  Puuh
- 1385 1386
: 1395 6 65 Win
: 1401 2 63 nie
: 1404 4 65  der
: 1412 5 63  Puuh
- 1419 1420
: 1432 3 61 Al
: 1437 1 61 le
: 1441 4 71  Kin
: 1447 3 71 der
: 1451 3 70  lie
: 1457 3 70 ben
* 1462 6 68  ihn
: 1473 36 66  sehr
E
