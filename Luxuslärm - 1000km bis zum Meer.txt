#TITLE:1000km bis zum Meer
#ARTIST:Luxuslärm
#MP3:Luxuslärm - 1000km bis zum Meer.mp3
#VIDEO:Luxuslärm - 1000km bis zum Meer.mp4
#COVER:Luxuslärm - 1000km bis zum Meer.jpg
#BPM:238
#GAP:10159
#ENCODING:UTF8
#GENRE:Rock
#EDITION:Ultrastar - Luxuslärm Edition
#YEAR:2008
#AUTHOR:torpedo, fepo
: 0 2 1 Wir
: 5 2 1  könn'
: 10 2 3  die
: 16 3 4  nächs
: 24 4 1 te
: 33 3 4  Aus
: 40 4 8 fahrt
: 48 5 6  nehm'
- 55
: 64 2 4 O
: 69 2 6 der
: 74 2 8  noch
: 81 4 6  wei
: 89 3 4 ter
: 96 7 1  fahrn
- 105
: 133 2 1 Ja,
: 138 4 3  ich
: 145 4 4  weiß,
: 154 2 1  es
: 161 3 4  ist
: 168 4 8  schon
: 176 7 8  spät
- 185
: 261 2 1 Komm,
: 265 2 3  wir
: 271 7 4  fah
: 280 3 1 ren
: 288 3 4  durch
: 296 5 8  die
: 305 3 6  Nacht
- 310
: 320 2 4 Bis
: 324 3 6  wir
: 329 4 8  die
: 337 4 8  Son
: 346 3 4 ne
: 353 11 4  sehn
- 366
: 389 2 4 Sag
: 394 3 1  mir
: 401 3 4  nicht,
: 408 4 1  dass
: 417 3 4  das
: 424 4 8  nicht
: 433 2 6  geht
: 437 3 4 ~
- 442
: 512 2 1 Denn
: 516 3 1  so 
: 522 4 3 weit
: 528 4 4  ist
: 537 3 3  das
: 546 5 -1  nicht
- 553
: 556 3 -1 Es
: 561 5 1  sind
: 576 2 -1  nur
: 581 2 1  10
: 585 2 1 00
: 590 2 4  Ki
: 593 2 4 lo
: 597 5 6 me
: 604 3 8 ter
: 610 2 6  bis
: 617 3 8  zum
: 624 10 1  Meer
- 636
: 641 3 6 Bis
: 649 3 8  zum
: 657 10 -1  Meer
- 669
: 673 3 6 Bis
: 681 3 8  zum
: 689 10 1  Meer
- 701
: 704 2 1 Da
: 708 2 4  schau
: 712 2 4 en
: 716 2 4  wir
: 720 2 4  den
: 725 3 6  Wel
: 730 3 8 len
: 736 4 6  hin
: 744 4 8 ter
: 752 7 1 her
- 761
* 769 3 6 Hin
* 776 6 8 ter
* 785 7 -1 her
- 794
: 801 3 6 Hin
: 808 6 8 ter
: 817 7 1 her
- 826
: 837 2 4 10
: 841 2 4 00 
: 845 2 4 Ki
: 849 2 4 lo
: 853 5 6 me
: 860 3 8 ter
: 865 3 6  bis
: 872 4 8  zum
: 881 12 1  Meer
- 895
: 1028 3 1 Ich
: 1034 4 3  hab
: 1041 5 4  Fern
: 1049 5 1 weh
: 1058 3 4  nach
: 1066 4 8  der
: 1074 4 6  Welt
- 1080
: 1088 2 1 Und
: 1093 2 4  muss
: 1099 3 8  jetzt
: 1106 5 6  wei
: 1114 5 4 ter
: 1123 7 1  ziehn
- 1132
: 1156 3 1 Und
: 1161 3 3  ich
: 1169 4 4  weiß,
: 1177 4 8  Du
: 1184 4 6  kennst
: 1193 3 8  das
: 1201 2 6  Ziel
: 1205 3 4 ~
- 1210
: 1281 2 1 Doch
: 1285 2 1  ich
: 1291 3 3  fühl
: 1297 4 4  mich
: 1305 3 1  erst
: 1313 6 4  wie
: 1321 5 8 der
: 1329 8 6  wohl
- 1339
: 1345 2 4 Wenn
: 1349 2 4  ich
: 1354 2 8  nach
: 1362 4 8  Hau
: 1369 4 4 se
: 1376 7 4  komm
- 1385
: 1412 3 1 Und
: 1416 3 3  ich
: 1425 4 4  weiß,
: 1433 3 1  dass
: 1441 3 4  Du's
: 1448 4 8  ver
: 1457 2 6 stehst
: 1461 2 4 ~
- 1465
: 1536 2 1 Viel
: 1541 2 1 leicht
: 1546 4 3  ge
: 1553 3 4 fällt's
: 1562 3 3  nicht
: 1570 4 1  je
: 1577 3 -1 dem
: 1585 4 1  hier
- 1591
: 1596 3 -1 Da
: 1601 3 1 bei
: 1612 2 1  sind's
: 1616 2 4  nur
: 1621 5 6  10
: 1629 2 4 00
: 1633 4 6  Ki
: 1640 6 4 lo
: 1648 5 6 me
: 1657 6 8 ter
- 1665
: 1697 3 6 Bis
: 1705 3 8  zum
: 1713 6 1  Meer
- 1721
: 1729 3 6 Bis
: 1737 3 8  zum
: 1745 6 -1  Meer
- 1753
: 1761 3 6 Bis
: 1769 3 8  zum
: 1777 6 1  Meer
- 1785
: 1792 2 1 Nur
: 1796 2 1  10
: 1800 2 4 00
: 1804 2 4  Ki
: 1808 2 4 lo
: 1812 6 6 me
: 1820 3 8 ter
: 1825 3 6  bis
: 1833 3 8  zum
: 1840 8 1  Meer
- 1850
: 1857 3 6 Bis
: 1865 3 8  zum
: 1873 8 -1  Meer
- 1883
: 1889 3 6 Bis
: 1897 3 8  zum
: 1905 8 1  Meer
- 1915
: 1920 2 1 Da
: 1925 3 1  schau
: 1930 2 1 en
: 1934 2 4  wir
: 1937 2 4  den
: 1941 4 6  Wel
: 1947 3 8 len
: 1953 4 6  hin
: 1961 5 8 ter
: 1969 7 1 her
- 1978
: 1985 4 6 Hin
: 1993 6 8 ter
: 2001 8 -1 her
- 2011
: 2017 4 6 Hin
: 2025 6 8 ter
: 2033 8 1 her
- 2043
: 2049 2 1 Nur
: 2053 2 1  10
: 2057 2 4 00
: 2061 2 4  Ki
: 2065 2 4 lo
: 2069 6 6 me
: 2077 3 8 ter
: 2082 3 6  bis
: 2090 3 8  zum
: 2097 8 8  Meer
- 2107
: 2243 2 1 Wenn
: 2246 2 1  ich
: 2250 2 3  jetzt
: 2256 5 4  all
: 2266 6 1  die
: 2274 4 4  Din
: 2281 6 8 ge
: 2290 5 6  zähl
- 2297
: 2306 2 4 Die
: 2310 3 8  wirk
: 2317 3 8 lich
: 2323 3 6  wich
: 2330 3 4 tig
: 2337 5 1  sind
- 2344
: 2369 2 4 Dann
: 2373 4 8  blei
: 2379 2 8 ben
: 2385 6 6  gar
: 2393 3 8  nicht
: 2401 4 4  mal
: 2409 4 6  so
: 2417 2 6  viel
: 2420 2 4 ~
- 2424
: 2502 3 1 Du
: 2507 3 3  hier
: 2513 6 4  ne
: 2521 2 3 ben
: 2528 8 -1  mir
: 2539 2 -1  und
: 2545 6 1  nur
- 2553
: 2560 2 1 Nur
: 2564 2 1  10
: 2568 2 4 00
: 2572 2 4  Ki
: 2576 2 4 lo
: 2580 6 6 me
: 2588 3 8 ter
: 2593 3 6  bis
: 2601 3 8  zum
: 2608 8 1  Meer
- 2618
: 2625 4 6 Bis
: 2633 4 8  zum
: 2641 11 -1  Meer
- 2654
: 2658 3 6 Bis
: 2664 4 8  zum
: 2673 10 1  Meer
- 2685
: 2687 2 1 Da
: 2692 3 1  schau
: 2697 2 1 en
: 2701 2 4  wir
: 2704 2 4  den
: 2708 4 6  Wel
: 2714 3 8 len
: 2720 4 6  hin
: 2728 5 8 ter
: 2736 7 1 her
- 2745
: 2753 3 6 Hin
: 2761 6 8 ter
: 2769 9 -1 her
- 2780
: 2785 3 6 Hin
: 2792 6 8 ter
: 2800 9 1 her
- 2811
: 2816 2 1 Nur
: 2820 2 1  10
: 2824 2 4 00
: 2828 2 4  Ki
: 2832 2 4 lo
: 2836 6 6 me
: 2844 3 8 ter
: 2849 3 6  bis
: 2857 3 8  zum
: 2864 8 1  Meer
- 2874
: 2880 4 6 Bis
: 2888 4 8  zum
: 2896 12 -1  Meer
- 2910
: 2912 4 6 Bis
: 2920 5 8  zum
: 2928 14 1  Meer
- 2943
: 2943 2 1 Da
: 2948 3 1  schau
: 2953 2 1 en
: 2957 2 4  wir
: 2960 2 4  den
: 2964 4 6  Wel
: 2970 3 8 len
: 2976 4 6  hin
: 2984 5 8 ter
: 2992 7 1 her
- 3001
: 3009 3 8 Hin
: 3016 6 4 ter
: 3025 8 3 her
- 3035
: 3041 4 8 Hin
: 3049 6 3 ter
: 3060 11 3 her
- 3073
: 3077 2 4 10
: 3081 2 4 00
: 3085 2 4  Ki
: 3089 2 4 lo
: 3093 6 6 me
: 3101 3 8 ter
: 3106 3 8  bis
: 3114 3 8  zum
: 3121 2 8  Meer
: 3125 4 4 ~
- 3131
: 3137 3 6 Bis
: 3144 3 4  zum
: 3151 10 3  Meer
- 3163
: 3168 3 8 Bis
: 3176 3 6  zum
: 3183 3 3  Meer
: 3188 4 -1 ~
- 3194
: 3200 2 1 Da
: 3205 2 4  schau
: 3209 2 4 en
: 3213 2 4  wir
: 3217 2 4  den
: 3221 4 6  Wel
: 3227 2 8 len
: 3234 3 6  hin
: 3242 4 8 ter
: 3249 4 8 her
: 3255 5 4 ~
- 3262
: 3267 4 8 Hin
: 3274 6 4 ter
: 3283 6 3 her
- 3291
: 3298 5 8 Hin
: 3306 6 4 ter
: 3315 10 1 her
- 3327
: 3329 2 1 Nur
: 3333 2 1  10
: 3337 2 4 00
: 3341 2 4  Ki
: 3345 2 4 lo
: 3349 6 6 me
: 3357 3 8 ter
: 3362 3 6  bis
: 3370 3 4  zum
: 3377 8 1  Meer
E