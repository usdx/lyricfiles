#TITLE:Jungle Drum
#ARTIST:Emiliana Torrini
#LANGUAGE:English
#MP3:Emiliana Torrini - Jungle Drum.mp3
#COVER:Emiliana Torrini - Jungle Drum [CO].jpg
#VIDEO:Emiliana Torrini - Jungle Drum [VD#3].mp4
#VIDEOGAP:3
#BPM:297,5
#GAP:4340
: 0 6 16  Hey
- 8
: 29 2 16  I'm
: 33 2 14  in
: 38 5 11  love
- 45
: 77 2 11  My
: 80 2 16  fin
: 84 3 14 gers
: 88 3 16  keep
: 93 3 14  on
: 98 3 16  clic
: 103 3 14 king
- 108
: 109 2 16  To
: 113 2 14  the
: 117 3 16  bea
: 123 3 14 ting
: 129 3 16  of
: 134 2 14  my
: 140 6 11  heart
- 148
: 161 7 16  Hey
- 170
: 183 2 16  I
: 186 2 14  can't
: 190 3 16  stop
: 194 3 14  my
: 199 6 11  feet
- 207
: 240 2 16  E
: 244 3 14 bo
: 248 3 16 ny
: 254 3 14  and
: 259 2 16  I
: 263 2 14 vo
: 268 3 16 ry
: 274 3 14  and
: 278 3 16  dan
: 283 3 14 cing
: 289 3 16  in
: 294 3 14  the
: 299 7 11  street
- 308
: 321 6 16  Hey
- 329
: 346 2 14  It's
: 349 1 14  be
: 351 2 16 cause
: 354 3 14  of
: 359 8 11  you
: 369 5 14 ~
- 376
: 397 2 14  The
: 401 3 16  world
: 406 2 14  is
: 410 2 16  in
: 415 3 14  a
: 420 3 16  cra
: 424 3 14 zy
: 429 3 16  ha
: 434 3 15 zy
: 440 30 16  hue
- 472
: 476 3 14  My
: 481 3 16  heart
: 486 3 16  is
: 490 3 16  bea
: 494 2 16 ting
: 497 2 16  like
: 499 2 16  a
: 502 7 14  Jun
: 513 5 19 gle
: 523 17 18  Drum
- 542
F 557 2 14  Ba
F 560 2 14  ru
F 562 2 14  du
F 567 2 14 ke
F 570 2 14  dung
F 575 2 14  du
F 578 2 14 ke
F 583 2 14  dung
F 588 2 14  dung
F 593 2 14  dung
- 596
: 598 3 14  My
: 603 3 16  heart
: 608 2 16  is
: 611 3 16  bea
: 615 2 16 ting
: 618 2 16  like
: 620 2 16  a
: 623 6 14  Jun
: 633 6 19 gle
: 643 23 18  Drum
- 668
F 677 1 14  Ra
F 680 1 14  ke
F 682 1 14  ra
F 684 1 14  ke
F 687 2 14  de
F 690 2 14  kun
F 695 1 14  ge
F 697 1 14  du
F 700 1 14  ke
F 701 2 14  ru
F 704 2 14  ke
F 707 3 14  dung
F 712 4 14  dung
- 717
: 717 3 14  My
: 722 3 16  heart
: 727 2 16  is
: 730 2 16  bea
: 735 2 16 ting
: 737 2 16  like
: 740 2 16  a
: 743 4 14  Jun
: 753 5 11 gle
: 763 7 10  Drum
- 772
: 843 10 16  Man
- 855
: 869 2 14  You
: 872 2 16  got
: 876 4 14  me
: 882 3 11  bur
: 888 6 10 ning
- 896
: 918 2 14  I'm
: 921 2 11  the
: 924 2 16  mo
: 927 3 16 ment
: 931 1 14  be
: 934 4 16 tween
- 939
: 939 1 14  The
: 941 4 16  stri
: 947 3 14 king
: 952 3 16  and
: 957 3 14  the
: 962 6 11  fire
- 970
: 1004 8 16  Hey
- 1014
: 1034 3 16  Read
: 1038 2 14  my
: 1043 9 11  lips
- 1054
: 1082 2 14  Cause
: 1085 2 16  all
: 1089 2 14  they
: 1092 4 16  say
: 1098 3 14  is
- 1102
: 1103 3 16  Kiss
: 1108 2 14  Kiss
: 1112 3 16  Kiss
: 1117 3 14  Kiss
: 1124 7 11  Kiss
- 1133
: 1166 20 16  No
: 1189 2 16  it
: 1192 3 14  will
: 1196 2 16  ne
: 1199 2 14 ver
: 1203 11 11  stop
: 1215 12 14 ~
- 1229
: 1240 2 11  My
: 1245 3 16  hands
: 1249 3 14  are
: 1253 3 16  in
: 1259 3 14  the
: 1263 3 16  air
: 1268 4 14  yes
: 1273 4 16  I'm
: 1278 3 14  in
: 1284 32 16  love
- 1320
: 1320 3 16  My
: 1325 3 16  heart
: 1330 3 16  is
: 1334 3 16  bea
: 1338 2 16 ting
: 1340 2 16  like
: 1342 2 16  a
: 1345 8 14  Jun
: 1356 5 19 gle
: 1366 21 18  Drum
- 1389
F 1401 2 14  Ra
F 1404 2 14  ke
F 1407 2 14  dung
F 1412 2 14  de
F 1414 2 14  dung
F 1419 2 14  ru
F 1421 2 14  ke
F 1424 2 14  du
F 1427 2 14  ke
F 1431 3 14  dung
F 1436 4 14  dum
- 1441
: 1441 3 14  My
: 1446 3 16  heart
: 1451 2 16  is
: 1454 2 16  bea
: 1458 2 16 ting
: 1461 2 16  like
: 1463 2 16  a
: 1467 4 14  Jun
: 1477 6 19 gle
: 1487 20 18  Drum
- 1509
F 1520 2 16  Ra
F 1523 2 16  ke
F 1526 2 16  du
F 1529 2 16  ke
F 1532 2 16  de
F 1535 2 16  ku
F 1538 2 16  ku
F 1541 2 16  tu
F 1544 2 16  ke
F 1547 2 16  tu
F 1550 2 16  ke
F 1553 2 16  dung
F 1556 4 16  dung
- 1561
: 1562 2 14  My
: 1567 3 16  heart
: 1572 2 16  is
: 1575 3 16  bea
: 1579 2 16 ting
: 1581 2 16  like
: 1583 2 16  a
: 1586 8 14  Jun
: 1597 5 19 gle
: 1607 11 18  Drum
- 1620
F 1641 12 16 Brrrru
F 1655 4 16  dung
F 1661 3 16  do
F 1665 4 16  rung
F 1671 5 16  kung
F 1677 4 16  kung
- 1682
: 1682 4 14  My
: 1687 4 16  heart
: 1692 2 16  is
: 1695 3 16  bea
: 1699 2 16 ting
: 1701 2 16  like
: 1703 2 16  a
: 1706 6 14  Jun
: 1717 5 11 gle
: 1727 7 10  Drum
- 1736
: 2124 3 21  My
: 2129 4 19  heart
: 2134 2 19  is
: 2137 3 19  bea
: 2142 2 19 ting
: 2144 2 19  like
: 2147 2 19  a
: 2150 6 17  Jun
: 2160 5 22 gle
: 2170 26 21  Drum
- 2198
: 2201 6 21  My
: 2210 3 19  heart
: 2215 3 19  is
: 2219 3 19  bea
: 2223 2 19 ting
: 2225 2 19  like
: 2227 2 19  a
: 2230 6 17  Jun
: 2240 5 22 gle
: 2250 14 21  Drum
- 2266
: 2278 8 21  My
: 2290 3 19  heart
: 2295 2 19  is
: 2299 3 19  bea
: 2304 2 19 ting
: 2306 2 19  like
: 2308 2 19  a
: 2311 7 17  Jun
: 2321 6 22 gle
: 2331 22 21  Drum
- 2355
: 2363 6 21  My
: 2371 4 19  Heart
: 2376 2 19  is
: 2380 3 19  bea
: 2384 2 19 ting
: 2386 2 19  like
: 2388 2 19  a
: 2391 6 17  Jun
: 2401 6 22 gle
- 2411
F 2411 3 16  Ra
F 2416 2 16  ke
F 2419 4 16  dung
F 2424 2 16  de
F 2427 1 16  de
F 2429 2 16  ba
F 2431 2 16  ru
F 2433 2 16  ke
F 2436 3 16  dung
F 2441 3 16  dung
E
