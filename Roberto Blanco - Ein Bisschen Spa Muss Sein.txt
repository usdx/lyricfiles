#TITLE:Ein Bisschen Spa Muss Sein
#ARTIST:Roberto Blanco
#MP3:Roberto Blanco - Ein Bisschen Spa Muss Sein.mp3
#VIDEO:Roberto Blanco - Ein Bisschen Spa Muss Sein [VD#0].avi
#COVER:Roberto Blanco - Ein Bisschen Spa Muss Sein [CO].jpg
#BPM:267
#GAP:2584,27
#VIDEOGAP:0
#ENCODING:UTF8
#EDITION:SingStar Sch DE
: 0 3 52 Ein
: 4 3 52  biss
: 8 3 52 chen
: 12 5 54  Spaß
: 20 3 52  muss
: 24 9 56  sein
- 35
: 66 3 52 Dann
: 70 3 56  ist
: 74 4 59  die
: 78 5 62  Welt
: 86 6 61  voll
- 93
: 94 6 59 Son
: 102 4 57 nen
: 107 10 61 schein
- 119
: 131 2 57 So
: 135 2 57  gut
: 139 3 57  wie
: 143 5 57  wir
: 151 4 56  uns
- 156
: 156 5 59 Heu
: 164 5 59 te
: 171 3 57  ver
: 176 9 56 stehn
- 187
: 195 2 59 So
: 199 2 59  soll
: 203 3 59  es
: 207 6 59  wei
: 215 4 57 ter
* 220 24 61 gehn
- 246
: 259 2 52 Ein
: 263 2 52  biss
: 267 3 52 chen
: 271 6 54  Spaß
: 279 2 52  muss
: 283 3 52  sein
: 287 9 56 ~
- 298
: 324 3 52 Dann
: 328 3 56  kommt
: 332 4 59  das
: 336 5 62  Glück
: 344 6 61  von
- 351
: 352 6 59 Ganz
: 361 3 57  al
* 364 13 61 lein
- 379
: 388 2 57 Drum
: 392 2 57  sin
: 396 2 57 gen
: 400 6 57  wir
: 408 3 56  tag
: 412 5 59 aus
- 418
: 420 5 59 Und
: 428 3 57  tag
: 432 9 56 ein
- 443
: 452 3 52 Ein
: 456 3 56  biss
: 460 3 59 chen
: 464 5 62  Spaß
: 472 4 61  muss
: 478 2 59  sein
: 480 7 57 ~
- 489
: 598 3 54 Heu
: 602 3 52 te
: 606 5 57  Nacht
- 613
: 629 3 57 Fei
: 633 3 57 ern
: 637 6 54  wir
- 645
: 662 3 57 Ma
: 666 3 57 chen
: 670 5 56  durch
- 677
: 686 5 59 Bis
* 694 5 57  um
: 702 4 56  vier
: 706 7 52 ~
- 715
: 726 3 52 Fra
: 730 3 52 gen
: 734 5 57  nicht
- 741
* 758 4 61 Nach
: 763 4 59  Zeit
: 770 3 57  und
: 774 3 57  Geld
: 778 6 54 ~
- 786
: 790 3 62 Weil
: 794 3 62  es
: 798 6 61  dir
: 806 3 61  und
: 810 3 61  auch
: 814 7 59  mir
- 822
: 822 3 59 So
: 826 3 59  ge
: 830 2 59 fällt
: 832 6 57 ~
- 840
: 871 2 52 Ein
: 875 2 52  biss
: 879 3 52 chen
: 883 6 54  Spaß
: 891 3 52  muss
: 895 12 56  sein
- 909
: 936 3 52 Dann
: 940 3 56  ist
: 944 3 59  die
: 948 6 62  Welt
: 956 6 61  voll
- 963
: 964 7 59 Son
: 972 3 57 nen
: 976 10 61 schein
- 988
: 1000 2 57 So
: 1004 2 57  gut
: 1008 2 57  wie
: 1012 6 57  wir
: 1021 3 56  uns
- 1025
: 1025 5 59 Heu
: 1033 6 59 te
: 1041 3 57  ver
: 1045 9 56 stehn
- 1056
: 1065 2 59 So
: 1069 2 59  soll
: 1073 2 59  es
: 1077 6 59  wei
: 1085 3 57 ter
: 1089 25 61 gehn
- 1116
: 1129 2 52 Ein
: 1133 2 52  biss
: 1137 3 52 chen
: 1141 6 54  Spaß
: 1149 3 52  muss
: 1153 13 56  sein
- 1168
: 1193 3 52 Dann
: 1197 4 56  kommt
: 1201 4 59  das
: 1205 6 62  Glück
: 1213 6 61  von
- 1220
: 1221 6 59 Ganz
: 1229 3 57  al
: 1233 15 61 lein
- 1250
: 1257 3 57 Drum
: 1261 3 57  sin
: 1265 3 57 gen
: 1269 6 57  wir
: 1277 3 56  tag
: 1281 5 59 aus
- 1287
: 1289 6 59 Und
: 1297 3 57  tag
: 1301 9 56 ein
- 1312
: 1321 3 52 Ein
: 1325 3 56  biss
: 1329 3 59 chen
* 1333 6 62  Spaß
: 1341 5 61  muss
: 1346 3 59  sein
: 1349 11 57 ~
- 1362
: 1466 3 52 Drau
: 1470 3 52 ßen
: 1474 6 57  wird's
- 1482
: 1497 3 57 Lang
: 1503 3 57 sam
: 1508 3 57  hell
: 1511 5 54 ~
- 1518
: 1530 3 57 Und
: 1534 3 57  die
: 1538 6 56  Zeit
- 1546
: 1554 3 56 Geht
* 1561 5 59  viel
: 1568 4 57  zu
: 1575 2 54  schnell
: 1577 6 52 ~
- 1585
: 1594 3 52 Noch
: 1598 3 52  ein
* 1603 6 57  Glas
- 1611
: 1626 5 61 Und
: 1633 5 59  ei
: 1640 5 57 nen
: 1645 7 54  Kuss
- 1654
: 1659 3 57 Ja
: 1663 3 57  und
: 1667 6 57  dann
: 1675 2 57  ist
: 1679 2 57  noch
- 1682
: 1683 9 57 Lange
: 1694 3 57  nicht
: 1698 5 57  Schluss
- 1705
: 1740 2 53 Ein
: 1744 2 53  biss
: 1748 2 53 chen
: 1752 5 55  Spaß
: 1760 3 53  muss
* 1764 9 57  sein
- 1775
: 1805 3 53 Dann
: 1809 3 57  ist
: 1813 3 60  die
: 1817 6 63  Welt
: 1825 7 62  voll
- 1833
: 1833 7 60 Son
: 1841 3 58 nen
: 1845 13 62 schein
- 1860
: 1869 2 58 So
: 1873 2 58  gut
: 1877 2 58  wie
: 1881 6 58  wir
: 1889 3 57  uns
- 1893
: 1893 5 60 Heu
: 1901 6 60 te
: 1909 3 58  ver
: 1913 9 57 stehn
- 1924
: 1933 2 60 So
: 1937 2 60  soll
: 1941 2 60  es
: 1945 6 60  wei
: 1953 3 58 ter
: 1957 32 62 gehn
- 1991
: 1997 2 53 Ein
: 2001 2 53  biss
: 2005 3 53 chen
: 2009 7 55  Spaß
: 2017 3 53  muss
: 2021 14 57  sein
- 2037
: 2062 3 53 Dann
: 2066 3 57  kommt
: 2070 3 60  das
: 2074 5 63  Glück
: 2082 5 62  von
- 2088
: 2090 6 60 Ganz
: 2098 3 58  al
: 2102 11 62 lein
- 2115
: 2126 2 58 Drum
: 2130 2 58  sin
: 2134 2 58 gen
: 2138 6 58  wir
: 2146 3 57  tag
: 2150 5 60 aus
- 2156
: 2158 6 60 Und
: 2166 2 58  tag
: 2170 9 57 ein
- 2181
: 2190 3 53 Ein
: 2194 3 57  biss
: 2198 2 60 chen
: 2202 6 63  Spaß
* 2210 6 62  muss
: 2218 4 58  sein
- 2224
: 2226 5 54 Ein
: 2234 5 54  biss
: 2242 5 54 chen
: 2250 5 56  Spaß
: 2258 5 54  muss
: 2266 12 58  sein
- 2280
: 2302 3 54 Dann
: 2306 3 58  ist
: 2310 4 61  die
: 2314 5 64  Welt
: 2322 6 63  voll
- 2329
: 2330 7 61 Son
: 2338 3 59 nen
* 2342 14 63 schein
- 2358
: 2366 3 59 So
: 2370 3 59  gut
: 2374 3 59  wie
: 2378 6 59  wir
: 2386 3 58  uns
- 2390
: 2390 5 61 Heu
: 2398 6 61 te
: 2406 4 59  ver
: 2410 11 58 stehn
E