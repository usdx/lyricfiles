#TITLE:Lied vom Scheitern
#ARTIST:Die Ärzte
#MP3:Die Ärzte - Lied vom Scheitern.mp3
#COVER:Die Ärzte - Lied vom Scheitern [CO].jpg
#BACKGROUND:Die Ärzte - Lied vom Scheitern [BG].jpg
#BPM:300
#GAP:1200
#ENCODING:UTF8
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
: 0 2 7 Ich
: 4 2 7  wus
: 8 2 7 ste
: 12 2 7  stets,
: 16 2 8  was
: 20 2 7  ich
: 24 4 10  will,
- 30
: 39 2 7 doch
: 43 3 5  das
: 48 2 5  wol
: 53 2 5 len
: 58 3 10  vie
: 66 2 5 le,
- 69
: 71 2 5 trotz
: 75 2 5 dem
: 79 3 3  setz
: 84 2 5 te
: 88 2 3  ich
: 92 3 7  mich
- 97
: 103 3 7 zwi
: 108 3 3 schen
: 116 2 5  al
: 120 2 3 le
: 125 3 7  Stüh
: 133 3 3 le
- 138
: 142 2 5 und
: 145 3 7  mach
: 150 2 10 te
: 154 3 10  es
: 162 2 10  mir
: 167 2 7  be
: 175 4 5 quem -
- 181
: 195 3 7 bis
: 200 3 7  hier
: 208 5 3 hin
: 227 4 3  kein
: 235 3 2  Pro
: 240 5 -2 blem.
- 247
: 272 2 7 Ich
: 276 2 7  streng
: 281 2 8 te
: 286 2 7  mich
: 290 3 10  an,
- 295
: 298 2 10 ge
: 302 2 10 hör
: 306 2 10 te
: 311 2 5  doch
: 315 2 7  nie
: 320 2 5  zu
: 324 3 10  de
: 331 4 5 nen
- 337
: 339 2 5 und
: 343 2 3  schwelg
: 348 2 5 te
: 352 2 3  doch
: 356 5 7  nur
- 362
: 364 2 7 in
: 368 2 7  un
: 372 1 7 er
: 377 2 8 reich
: 381 2 8 ba
: 386 2 7 ren
: 390 3 3  Plä
: 397 2 5 nen
- 400
: 402 2 3 und
: 406 2 3  am
: 410 2 7  En
: 415 3 10 de
: 426 3 10  war
: 434 3 7  der
: 439 5 5  Lohn
: 459 3 7  Fru
: 465 3 7 stra
: 473 7 3 tion.
- 482
: 539 1 7 Ich
: 543 2 8  dach
: 547 2 8 te,
: 551 3 8  ich
: 559 3 8  könn
: 567 2 8 te
: 572 3 8  es
: 580 3 7  er
: 588 4 3 zwin
: 596 4 5 gen.
- 602
: 604 2 5 Der
: 609 3 7  Selbst
: 616 2 7 be
: 621 3 7 trug
: 629 3 7  tat
: 637 4 7  mir
: 646 4 5  nichts
: 654 4 8  brin
: 662 7 7 gen,
- 671
: 682 4 10 denn
: 691 2 3  du
: 696 2 3  bist
: 700 2 5  im
: 704 2 3 mer
: 708 5 7  dann
: 732 2 3  am
: 737 3 5  bes
: 745 4 7 ten,
- 751
: 758 3 3  wenn's
: 766 3 5  dir
: 774 3 7  ei
: 782 3 7 gent
: 790 4 7 lich
: 799 2 3  e
: 805 4 5 gal
: 812 4 7  ist.
- 818
: 823 2 3 Du
: 828 2 3  bist
: 832 2 5  im
: 836 2 3 mer
* 840 6 7  dann
: 864 2 3  am
: 869 3 5  bes
: 877 4 7 ten,
- 883
: 890 3 3 wenn
: 898 3 5  du
: 906 3 7  ein
: 914 3 7 fach
: 923 3 7  ganz
: 931 2 3  nor
: 936 3 5 mal
: 943 4 7  bist.
- 949
: 956 2 7 Du
: 960 2 7  bist
: 965 2 7  im
: 969 2 7 mer
* 974 5 3  dann
: 998 2 2  am
: 1003 3 2  bes
: 1010 6 3 ten,
- 1018
: 1035 2 10 du
: 1039 3 10  musst
: 1047 3 10  das
: 1055 2 10  nicht
: 1060 3 7  mehr
: 1068 4 12  te
: 1076 4 11 sten,
- 1082
: 1088 6 12 je
: 1096 4 14 des
* 1105 30 12  Mal.
- 1137
: 1166 3 5 Dein
: 1171 3 5  Spie
: 1176 3 5 gel
: 1184 4 5 bild
: 1192 4 7  ist
: 1200 4 8  an
: 1209 4 7 de
: 1217 4 3 ren
: 1225 5 0  e
* 1234 16 3 gal.
- 1252
: 1395 2 7 Ich
: 1399 2 7  war
: 1403 2 7  nicht
: 1407 2 7  mehr
: 1412 2 7  ich
: 1416 4 10  selbst,
- 1422
: 1432 2 7 es
: 1436 2 5  wur
: 1440 2 7 de
: 1444 2 5  ge
: 1448 4 10 fähr
: 1456 3 5 lich.
- 1460
: 1461 2 5 Tat,
: 1465 2 5  was
: 1470 2 3  an
: 1475 2 5 d´re
: 1478 2 3  ver
: 1482 2 7 lang
: 1487 2 7 ten,
- 1490
: 1491 2 7 war
: 1495 2 7  zu
: 1499 2 7  mir
: 1503 2 8  selbst
: 1508 3 8  nicht
: 1515 2 7  mehr
: 1519 2 3  ehr
: 1523 3 5 lich.
- 1528
: 1532 2 5 Wer
: 1536 3 7  Wahr
: 1541 4 10 heit
: 1551 3 10  si
: 1557 3 7 mu
: 1565 6 5 liert,
- 1573
: 1585 3 7 wird
: 1590 3 7  nur
: 1598 8 3  kurz
: 1618 3 3  ak
: 1623 3 2 zep
: 1631 7 -2 tiert.
- 1640
: 1656 2 8 Ich
: 1660 2 8  mach
: 1664 2 8 te
: 1669 2 7  es
: 1673 2 8  al
: 1677 2 7 len
: 1681 4 10  recht,
- 1687
: 1693 2 10 al
: 1697 2 10 le
: 1701 2 5  soll
: 1705 2 7 ten
: 1710 2 5  mich
: 1714 4 10  lie
: 1722 3 5 ben.
- 1726
: 1727 2 5 Sah
: 1731 4 3  nicht
: 1738 3 5  die
: 1743 2 3  Dä
: 1747 3 7 mo
: 1752 2 7 nen,
- 1756
: 1760 3 7 die
: 1765 3 8  mich
: 1771 2 8  da
: 1776 2 7 zu
: 1780 4 3  trie
: 1788 3 5 ben.
- 1792
: 1793 2 3 War
: 1797 2 5  ge
: 1801 2 7 fan
: 1806 3 10 gen
: 1813 2 10  und
: 1818 2 10  nicht
: 1823 3 7  mehr
: 1831 6 5  frei
- 1839
: 1850 3 7 und
: 1856 4 7  ich
: 1864 7 3  ging
: 1879 2 3  ka
: 1884 4 3 putt
: 1892 3 2  da
: 1897 8 -2 bei.
- 1907
: 1929 2 3 Man
: 1933 4 8  kann
: 1941 3 8  die
: 1946 4 8  Welt
: 1954 4 8  nicht
: 1962 4 8  e
: 1971 4 7 wig
: 1978 4 3  blen
: 1987 4 5 den.
- 1993
: 1996 2 3 Ich
: 2000 4 7  muss
: 2007 4 7  den
: 2016 4 7  Quatsch
: 2024 3 7  so
: 2029 4 7 fort
: 2037 4 5  be
: 2045 5 8 en
: 2053 9 7 den,
- 2064
: 2074 4 7 denn
: 2082 3 3  du
: 2086 2 3  bist
: 2090 2 5  im
: 2094 2 3 mer
* 2099 8 7  dann
: 2122 3 3  am
: 2128 3 5  bes
: 2135 5 7 ten,
- 2142
: 2147 4 3 wenns
: 2156 4 5  dir
: 2164 4 7  ei
: 2173 3 7 gent
: 2181 4 7 lich
: 2189 4 3  e
: 2194 4 5 gal
: 2202 5 7  ist.
- 2209
: 2214 2 3 Du
: 2218 2 3  bist
: 2223 2 5  im
: 2227 2 3 mer
* 2231 7 7  dann
: 2255 3 3  am
: 2260 3 5  bes
: 2268 4 7 ten,
- 2274
: 2280 3 3 weil
: 2288 4 5  der
: 2297 4 7  Ehr
: 2305 4 7 geiz
: 2313 4 7  dich
: 2321 3 3  sonst
: 2327 3 5  auf
: 2335 4 7 frisst.
- 2341
: 2347 2 7 Du
: 2351 3 7  bist
: 2356 2 7  im
: 2360 2 7 mer
* 2364 6 3  dann
: 2387 3 3  am
: 2392 4 2  bes
: 2400 6 3 ten,
- 2408
: 2425 2 10 du
: 2429 3 10  musst
: 2437 4 10  das
: 2446 3 10  nicht
: 2451 4 7  aus
: 2459 4 12 tes
: 2467 5 11 ten,
- 2474
: 2478 5 12 nicht
: 2487 4 14  noch
* 2496 29 12  mal.
- 2527
: 2557 2 5 Dein
: 2562 3 5  Spie
: 2567 3 5 gel
: 2575 4 5 bild
: 2583 4 7  ist
: 2591 5 8  an
: 2600 4 7 de
: 2608 4 3 ren
: 2617 5 0  e
* 2628 13 3 gal.
- 2643
: 2740 2 8 Du
: 2744 3 7  kannst
: 2749 3 7  für
: 2753 3 7  ei
: 2757 2 7 ne
: 2761 2 10  Wei
: 2765 3 10 le
- 2770
: 2773 3 10 dein
: 2777 3 5  Um
: 2782 3 5 feld
: 2787 2 5  be
: 2791 3 10 lü
: 2796 3 10 gen.
- 2801
: 2803 2 10 Doch
: 2807 2 10  dein
: 2812 2 3  ei
: 2816 2 3 ge
: 2821 1 3 nes
: 2825 4 7  Herz
- 2831
: 2836 3 7  wirst
: 2842 4 8  du
: 2850 3 8  nicht
: 2855 3 7  be
: 2860 4 3 trü
: 2867 4 5 gen.
- 2873
: 2876 3 5 Man
: 2882 3 7  ern
: 2888 4 10 tet,
: 2900 3 10  was
: 2906 4 7  man
: 2914 5 5  sät,
- 2921
: 2930 3 5 drum
: 2936 3 7  wirds
: 2944 3 7  dein
: 2950 4 3  Herz
: 2957 4 3  sein,
: 2967 3 3  das
: 2972 4 3  dich
: 2980 3 2  ver
: 2985 8 -2 rät.
- 2995
: 3017 2 8 Ich
: 3021 3 8  will
: 3026 3 7  deinen
: 3034 2 7  E
: 3039 4 10 lan
- 3044
: 3046 2 10 doch
: 3050 2 10  ü
: 3055 2 10 ber
: 3059 2 5 haupt
: 3063 4 5  nicht
: 3071 4 10  dämp
: 3079 4 10 fen.
- 3085
: 3087 2 10 Wenn
: 3091 2 3  du
: 3095 2 5  et
: 3100 2 3 was
: 3104 4 7  willst,
- 3110
: 3116 2 7 musst
: 3121 4 8  du
: 3128 2 8  da
: 3133 2 7 rum
: 3137 4 3  kämp
: 3145 4 5 fen.
- 3151
: 3154 2 5 Nur
: 3159 2 7  ei
: 3164 3 10 nes
: 3170 2 10  ver
: 3175 3 10 sprichst
: 3181 4 7  du
: 3187 5 5  mir:
- 3194
: 3203 2 7 bleib
: 3207 2 7  im
: 3212 2 7 mer
: 3216 2 7  du
: 3221 6 3  selbst
: 3236 3 3  und
: 3240 4 3  bleib
: 3248 4 2  bei
* 3257 10 -2  dir.
- 3269
: 3372 3 3 Ich
: 3376 2 3  bin
: 3380 2 5  im
: 3385 2 3 mer
* 3389 9 7  dann
: 3414 3 3  am
: 3419 3 5  bes
: 3426 4 7 ten,
- 3432
: 3439 4 3  wenn's
: 3447 4 5  mir
: 3455 4 7  ei
: 3464 5 7 gent
: 3472 4 7 lich
: 3480 3 3  e
: 3485 4 5 gal
: 3493 5 7  ist.
- 3500
: 3505 3 3 Ich
: 3509 2 3  bin
: 3513 3 5  im
: 3517 2 3 mer
* 3522 9 7  dann
: 3546 3 3  am
: 3551 3 5  bes
: 3559 4 7 ten,
- 3565
: 3571 4 3 wenn
: 3579 4 5  mir
: 3587 4 7  kei
: 3596 4 7 ner
: 3604 4 7  ins
: 3613 3 3  Re
: 3618 3 5 gal
: 3625 5 7  pisst.
- 3632
: 3637 2 3 Ich
: 3641 2 3  bin
: 3646 2 5  im
: 3650 2 3 mer
* 3655 7 7  dann
: 3678 3 3  am
: 3683 4 5  bes
: 3691 8 7 ten -
- 3701
: 3716 2 7 am
: 3720 3 7  zweit-,
: 3728 4 7  dritt-,
: 3736 3 7  o
: 3741 2 7 der
: 3746 2 3  zehnt
: 3751 3 5 bes
: 3758 5 7 ten.
- 3765
: 3770 2 7 Von
: 3775 3 7  mir
: 3780 3 7  aus
: 3786 3 5  auch
: 3794 3 3  mal
: 3800 3 3  nicht
: 3808 4 3  am
: 3816 4 2  bes
: 3824 9 3 ten.
- 3835
: 3849 2 10 Ich
: 3853 3 10  muss
: 3861 4 10  das
: 3869 3 10  nicht
: 3874 3 7  aus
: 3881 5 12 tes
: 3890 5 11 ten,
- 3897
: 3899 5 12 nicht
: 3907 5 14  noch
* 3919 34 12  mal.
- 3955
: 3992 4 3 Mein
: 3999 5 3  Spie
: 4007 4 3 gel
: 4015 13 5 bild
: 4046 5 7  ist
: 4060 5 8  an
: 4073 6 7 de
: 4082 8 3 ren
* 4102 5 0  e
* 4111 9 -2 gal.
E