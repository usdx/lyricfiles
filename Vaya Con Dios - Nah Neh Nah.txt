#TITLE:Nah Neh Nah
#ARTIST:Vaya Con Dios
#MP3:Vaya Con Dios - Nah Neh Nah.mp3
#VIDEO:Vaya Con Dios - Nah Neh Nah.avi
#COVER:Vaya Con Dios - Nah Neh Nah.jpg
#BPM:223.45
#GAP:22395
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Jazz
#YEAR:1990
#CREATOR:mustangfred and asb
: 0 2 -1 I
: 3 2 0  got
: 6 2 1  on
: 9 1 0  the
: 13 3 1  phone
: 17 2 0  and
: 21 2 1  called
: 24 1 0  the
: 28 2 1  girls,
* 32 2 6  said
- 36
: 38 2 1 Meet
: 41 2 0  me
: 46 2 1  down
: 49 2 0  at
: 54 2 1  Curl
: 57 1 0 y
: 60 3 1  Pearls,
: 64 1 1  for
: 66 1 -6  a
- 71
* 73 1 -1 Ney
* 75 1 1 ~,
: 78 2 -1  Nah
: 81 2 -3  Nah
: 84 2 -4  Nah
- 98
: 102 1 -1 Ney
: 104 3 1 ~,
: 109 2 -1  Nah
: 112 2 -3  Nah
: 116 4 -4  Nah
- 125
: 127 1 -1 In
: 129 1 1 ~
: 131 1 0  my
: 133 1 1  high-
: 137 2 0 heeled
: 142 2 1  shoes
: 145 1 0  and
: 149 2 1  fan
: 152 1 0 cy
: 155 1 1  fads
- 157
: 159 1 1 I
: 161 2 1  ran
: 165 3 1  down
: 169 1 0  the
: 173 3 0  stairs
: 177 3 0  hailed
: 181 2 1  me
: 184 2 0  a
* 187 3 1  cab,
: 191 2 1  go
: 194 1 1 ing
- 199
: 201 2 1 Ney,
: 205 2 -1  Nah
: 208 2 -3  Nah
: 211 2 -4  Nah
- 225
: 229 1 -1 Ney
: 231 4 1 ~,
: 236 2 -1  Nah
: 239 2 -3  Nah
: 243 3 -4  Nah
- 257
: 261 1 -1 Ney
: 263 2 1 ~,
: 269 2 -1  Nah
: 272 2 -3  Nah
: 275 3 -4  Nah
- 289
: 293 1 -1 Ney
: 295 4 1 ~,
: 301 2 -1  Nah
: 304 2 -3  Nah
: 307 3 -4  Nah,
: 312 1 -3  Oh
: 314 4 -1 ~
: 320 3 -1  Nah
: 325 1 -1  Ney
: 327 2 -3 ~,
- 331
: 333 2 -3 Nah
: 336 2 -4  Nah
: 339 1 -6  Nah
- 350
: 357 1 -3 Ney
: 359 4 -1 ~,
: 364 3 -3  Nah
: 368 2 -4  Nah
: 371 2 -6  Nah
- 374
: 376 1 -4 Nah
: 378 4 -3 ~
: 384 2 -1  Nah
: 388 2 -1  Nah
: 392 20 1 ~
- 426
: 448 1 4 When
: 450 1 6 ~
: 452 1 4  I
: 455 1 6  pushed
: 457 1 4  the
: 460 3 6  door,
: 464 2 6  I
: 468 1 4  saw
* 471 1 6  E
* 473 1 9 lea
* 476 2 9 nor
: 479 2 6  and
- 483
: 485 2 9 Ma
: 488 2 6 ry-
: 491 2 6 Lou
* 495 3 6  swing
: 499 1 4 ing
: 501 2 6  on
: 504 1 9  the
: 507 2 9  floor,
: 511 1 9  go
: 513 1 9 ing
- 518
* 520 3 9 Ney,
* 524 3 8  Nah
: 528 2 6  Nah
: 531 3 8  Nah
- 545
: 549 1 8 Ney
: 551 4 9 ~,
: 556 2 8  Nah
* 559 1 6  Nah
: 563 4 8  Nah
- 576
* 579 3 9 Sue
: 583 2 6  came
: 587 2 6  in,
: 590 2 6  in
: 593 2 4  a
: 596 2 6  silk
: 600 1 9  sa
: 602 3 9 rong
- 606
: 608 2 6 She
: 611 3 9  walzed
: 615 1 6  a
: 618 1 6 cross
- 620
: 622 2 6 As
: 625 1 4  they
: 628 2 6  played
: 631 1 6  that
: 634 3 9  song
: 638 1 9  that
: 640 2 9  meant
- 646
* 648 3 9 Ney,
: 652 2 8  Nah
: 655 2 6  Nah
: 658 3 8  Nah
- 672
: 676 1 8 Ney
: 678 5 9 ~,
: 684 2 8  Nah
: 687 1 6  Nah
: 690 4 8  Nah
- 703
: 707 1 8 Ney
: 709 5 9 ~,
: 715 2 8  Nah
: 718 1 6  Nah
: 721 4 8  Nah
- 736
: 740 1 8 Ney
: 742 5 9 ~,
: 748 2 8  Nah
: 751 1 6  Nah
: 754 4 8  Nah
* 759 1 -1  Oh
* 761 4 1 ~
: 766 3 1  Nah
: 771 3 1  Ney,
- 778
: 780 2 -1 Nah
: 783 1 1  Nah
: 786 2 -1  Nah
- 800
: 804 1 -1 Ney
: 806 4 1 ~,
: 812 2 -1  Nah
: 815 1 1  Nah
: 818 4 -1  Nah
: 824 6 -3  Nah
: 831 4 -1  Nah
: 836 2 -1  Nah
: 839 22 1 ~
- 890
: 1353 3 6 An
: 1357 1 4 nie
: 1360 3 6  was
: 1364 3 4  a
: 1368 2 6  lit
: 1371 2 9 tle
: 1374 2 9  late,
: 1378 3 6  she
: 1382 3 6  had
- 1386
: 1388 1 4 To
: 1391 4 6  get
: 1396 2 6  out
: 1399 2 6  of
: 1402 1 9  a
* 1405 2 9  date
: 1409 1 9  with
: 1411 1 9  a
- 1416
: 1418 1 8 Ney
: 1420 2 9 ~,
* 1423 2 8  Nah
: 1426 2 6  Nah
: 1429 3 8  Nah
- 1442
: 1446 1 8 Ney
: 1448 5 9 ~,
: 1454 2 8  Nah
: 1457 1 6  Nah
: 1460 5 8  Nah
- 1474
* 1477 3 9 Curl
* 1481 1 6 y
: 1485 3 6  fixed
: 1490 1 6  an
: 1494 1 6 oth
: 1496 1 9 er
: 1499 3 9  drink
: 1503 3 9  as
: 1507 1 9  the
* 1510 2 9  pia
* 1513 2 6 no
: 1517 1 6  man
- 1519
: 1521 1 4 Be
: 1524 2 6 gan
: 1528 1 9  to
: 1532 2 9  sing,
: 1535 2 9  that
: 1539 2 9  song
- 1543
: 1545 2 9 Ney,
: 1549 2 8  Nah
: 1552 2 6  Nah
: 1555 3 8  Nah
- 1568
: 1572 2 8 Ney
: 1575 3 9 ~,
: 1580 2 8  Nah
: 1583 1 6  Nah
: 1586 4 8  Nah
- 1599
: 1603 1 8 Ney
: 1605 4 9 ~,
: 1611 2 8  Nah
: 1614 2 6  Nah
: 1617 3 8  Nah
- 1631
: 1635 2 8 Ney
: 1638 4 9 ~,
: 1643 2 8  Nah
: 1646 1 6  Nah
: 1649 4 8  Nah
: 1655 5 1  Oh
: 1662 3 1  Nah
: 1667 3 1  Ney,
: 1675 2 -1  Nah
: 1678 1 1  Nah
: 1681 2 -1  Nah
- 1695
: 1699 1 -1 Ney
: 1701 3 1 ~,
: 1706 2 -1  Nah
: 1709 1 1  Nah
: 1712 3 -1  Nah
: 1718 6 -3  Nah
: 1725 4 -1  Nah
: 1731 2 -1  Nah
: 1734 24 1 ~
- 1772
: 1790 2 6 It
: 1793 5 6  was
: 1799 3 4  al
: 1803 2 6 read
: 1806 1 4 y
: 1809 2 6  half
: 1813 2 9  past
* 1817 3 9  three
: 1821 1 6  but
- 1823
: 1825 1 4 The
: 1827 1 6  night
: 1831 1 4  was
: 1834 2 6  young
: 1838 2 4  and
: 1842 1 6  so
: 1845 2 9  were
* 1848 2 9  we
: 1851 2 9  danc
: 1855 2 9 ing
- 1860
* 1862 2 9 Ney,
: 1865 2 8  Nah
: 1868 2 6  Nah
: 1871 3 8  Nah
- 1886
: 1890 1 8 Ney
: 1892 4 9 ~,
: 1898 2 8  Nah
: 1901 1 6  Nah
: 1904 5 8  Nah
- 1914
* 1916 5 9 Oh
* 1922 1 6 ~
: 1924 2 6  Lord,
: 1928 2 6  did
: 1932 2 4  we
: 1935 3 6  have
: 1939 1 9  a
: 1942 3 9  ball
- 1946
: 1948 2 9 Still
: 1952 3 9  sing
: 1956 2 8 ing,
: 1960 2 9  walk
: 1963 2 9 ing
: 1967 2 9  down
: 1971 2 8  that
: 1974 2 9  hall
: 1977 3 9  that
- 1985
: 1987 3 9 Ney,
: 1991 2 8  Nah
: 1994 2 6  Nah
: 1997 4 8  Nah
- 2011
: 2015 1 8 Ney
: 2017 5 9 ~,
: 2023 2 8  Nah
: 2026 1 6  Nah
: 2029 4 8  Nah
- 2042
: 2046 1 8 Ney
: 2048 4 9 ~,
: 2054 2 8  Nah
: 2057 1 6  Nah
: 2059 4 8  Nah
- 2074
: 2078 1 8 Ney
: 2080 4 9 ~,
: 2085 2 8  Nah
: 2088 1 6  Nah
: 2091 4 8  Nah
- 2096
: 2098 2 -1 Oh
: 2101 2 1 ~
: 2104 3 1  Nah
: 2109 4 1  Ney,
: 2117 2 -1  Nah
: 2120 2 1  Nah
: 2123 3 -1  Nah
- 2137
: 2141 1 -1 Ney
: 2143 4 1 ~,
: 2148 2 -1  Nah
: 2151 2 1  Nah
: 2154 4 -1  Nah
: 2160 5 -3  Nah
: 2167 3 -1  Nah
: 2172 2 -1  Nah
: 2175 28 1 ~
E