#TITLE:Rising Force
#ARTIST:Yngwie J. Malmsteen
#MP3:Yngwie J. Malmsteen - Rising Force.mp3
#COVER:Yngwie J. Malmsteen - Rising Force [CO].jpg
#BACKGROUND:Yngwie J. Malmsteen - Rising Force [BG].jpg
#BPM:236,82
#GAP:50000
#ENCODING:UTF8
: 134 6 64 Out
: 140 5 65  of
: 145 4 67  the
: 149 6 69  dark
: 155 6 67 ness
: 161 5 65  the
: 166 6 67  voic
: 172 5 65 es
: 177 5 64  are
: 182 5 65  call
: 187 6 64 ing
- 194
: 196 7 64 Rid
: 203 6 65 ing
: 209 4 67  the
: 213 7 69  wings
: 220 4 67  of
: 224 2 69  a
: 226 24 71  song
- 252
: 256 3 64 The
: 259 5 64  Fu
: 264 7 65 ry
: 271 5 67  is
: 276 7 69  scream
: 283 6 67 ing
: 289 4 65  and
: 293 5 67  Hea
: 298 5 65 ven
: 303 5 64  is
: 308 7 65  fal
: 315 6 64 ling
- 322
: 323 6 64 I
: 329 7 65  feel
: 336 5 67  it
: 341 6 69  com
: 347 4 67 ing
: 351 6 74  on
: 357 22 71  strong.
- 381
: 387 6 72 The
: 393 6 71  light
: 399 3 69 ning
: 402 15 65  strikes
: 417 11 71  crack
: 428 4 69 ing
: 432 3 67  the
: 435 14 67  night
- 451
: 454 6 69 It
: 460 4 67  feels
: 464 4 64  like
: 468 5 63  nev
: 473 7 64 er
: 480 3 65  be
: 483 29 67 fore
- 514
: 518 5 72 Thun
: 523 3 72 der
: 526 2 72  and
: 528 16 72  sparks
: 544 2 69  in
: 546 3 69  the
: 549 7 71  Heart
: 556 4 72  of
: 560 2 71  the
: 562 15 69  Dark
- 579
: 583 5 69 I
: 588 4 67  hear
: 592 4 64  a
: 596 8 63  Ris
: 604 5 63 ing
: 610 16 64  Force.
- 628
: 639 6 64 Sear
: 645 3 65 ch
: 648 4 67 ing
: 652 3 69  my
: 655 13 67  soul
: 668 2 65  I
: 670 7 67  find
: 677 5 65  some
: 682 2 64 th
: 684 4 65 ing
: 688 10 64  else
- 700
: 703 6 64 I
: 709 6 65  take
: 715 5 67  my
: 720 7 69  life
: 727 5 67  in
: 732 5 69  my
: 737 25 71  hands
- 763
: 763 5 64 From
: 768 5 64  the
: 773 7 65  gates
: 780 5 67  of
: 785 6 69  Heav
: 791 5 67 en
: 796 3 65  to
: 799 2 67  the
: 801 5 65  al
: 806 5 64 tars
: 811 5 65  of
: 816 9 64  Hell
- 826
: 827 3 64 The
: 830 15 65  Power
: 845 4 67  is
: 849 4 69  at
: 853 7 67  my
: 860 4 74  com
: 864 20 71 mand.
- 886
: 895 5 72 The
: 900 6 71  light
: 906 4 69 ning
: 910 14 65  strikes
: 924 13 71  crack
: 937 2 69 ing
: 939 2 67  the
: 941 16 67  night
- 959
: 962 5 69 It
: 967 5 67  feels
: 972 4 64  like
: 976 6 63  ne
: 982 7 64 ver
: 989 3 65  be
: 992 26 67 fore
- 1020
: 1027 4 72 Thun
: 1031 3 72 der
: 1034 2 72  and
: 1036 12 72  spark
: 1050 3 69  in
: 1053 3 69  the
: 1057 7 71  Heart
: 1064 4 72  of
: 1068 2 71  the
: 1070 17 69  Dark
- 1089
: 1091 3 69 I
: 1094 5 67  feel
: 1099 4 64  a
: 1103 10 63  Ris
: 1113 6 63 ing
: 1119 16 64  Force.
: 1141 8 69  Uhhhh
- 1151
: 2319 7 64 Burned
: 2326 6 65  by
: 2332 4 67  the
: 2336 6 69  glo
: 2342 6 67 ry
: 2348 4 65  of
: 2352 5 67  a
: 2357 5 65  sa
: 2362 5 64 cred
: 2367 5 65  fi
: 2372 5 64 re
- 2379
: 2386 4 64 A
: 2390 7 65  Ris
: 2397 4 67 ing
: 2401 5 69  Force
: 2406 6 67  starts
: 2412 3 69  to
: 2415 25 71  shine
- 2442
: 2446 9 64 Alone
: 2455 3 64  in
: 2458 3 65  the
: 2461 4 67  in
: 2465 7 69 fer
: 2472 5 67 no
: 2477 4 65  it
: 2481 10 67  soars
: 2491 3 65  ev
: 2494 2 64 er
: 2496 6 65  high
: 2502 5 64 er
- 2509
: 2518 6 64 Leav
: 2524 4 65 ing
: 2528 3 67  the
: 2531 5 69  de
: 2536 4 67 mons
: 2540 4 74  be
: 2544 27 71 hind.
- 2573
: 2580 5 72 The
: 2585 6 71  light
: 2591 4 69 ning
: 2595 12 65  strikes
: 2612 8 71  crack
: 2620 5 69 ing
: 2625 2 67  the
: 2627 14 67  night
- 2643
: 2649 4 69 It
: 2653 5 67  feels
: 2658 4 64  like
: 2662 4 63  nev
: 2666 8 64 er
: 2674 4 65  be
: 2678 22 67 fore
- 2702
: 2714 5 72 Thun
: 2719 2 72 der
: 2721 3 72  and
: 2724 12 72  sparks
: 2739 2 69  in
: 2741 3 69  the
: 2744 9 71  Heart
: 2753 3 72  of
: 2756 2 71  the
: 2758 19 69  Dark
- 2779
: 2781 4 69 I
: 2785 5 67  hear
: 2790 3 64  a
: 2793 8 63  Ris
: 2801 8 63 ing
: 2809 13 67  Force.
- 2824
: 2842 5 72 The
: 2847 5 71  light
: 2852 4 69 ning
: 2856 14 65  strikes
: 2874 8 71  crack
: 2882 5 69 ing
: 2887 3 67  the
: 2890 15 67  night
- 2907
: 2911 4 69 I'm
: 2915 5 67  not
: 2920 1 64  the
: 2921 12 63  same
: 2933 4 64  any
: 2937 9 65 -
: 2946 23 67 more
- 2971
: 2976 4 72 Thun
: 2980 3 72 der
: 2983 3 72  and
: 2986 13 72  spark
: 2999 2 69  in
: 3001 3 69  the
: 3005 8 71  Heart
: 3013 3 72  of
: 3016 3 71  the
: 3019 18 69  Dark
- 3038
: 3040 5 69 I
: 3045 4 67  feel
: 3049 3 64  a
: 3052 9 63  Ris
: 3061 7 63 ing
: 3068 16 64  Force.
: 3092 5 69  Uhhhh
E