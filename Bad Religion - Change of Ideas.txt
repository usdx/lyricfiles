#TITLE:Change of Ideas
#ARTIST:Bad Religion
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
#MP3:Bad Religion - Change of Ideas.mp3
#COVER:Bad Religion - Change of Ideas [CO].jpg
#BACKGROUND:Bad Religion - Change of Ideas [BG].jpg
#BPM:600
#GAP:1000
: 0 3 13 Well
: 4 2 13  the
: 8 10 14  sheaves
: 19 3 14  have
: 23 8 13  all
: 32 4 13  been
: 37 14 11  brought,
- 53 54
: 66 4 9  but
: 71 3 9  the
: 75 10 9  fields
: 86 3 9  have
: 90 8 8  washed
: 99 4 8  a
: 104 14 6 way,
- 120 121
: 131 5 13 And
: 137 3 13  the
: 141 6 14  pa
: 148 4 14 la
: 153 10 13 ces
: 164 10 11  now
: 180 16 11  stand
- 197
: 198 5 9  where
: 204 3 9  the
: 208 4 9  co
: 213 6 9 ffins
: 221 8 11  all
: 230 2 11  were
* 233 22 13  laid,
- 257
: 264 5 13 And
: 270 3 13  the
: 274 8 14  times
: 283 2 14  we
: 286 8 13  see
: 295 4 13  a
: 300 16 11 head,
- 318 319
: 328 4 9  we
: 333 4 9  must
: 338 8 9  glaze
: 347 4 9  with
: 352 4 8  ro
: 357 4 8 zy
: 362 20 6  hues,
- 384 385
: 393 6 13 For
: 400 2 13  what
: 403 5 14  don't
: 410 5 14  wish
: 416 8 13  to
: 425 2 11  ad
: 428 16 11 mit
- 446 447
: 459 4 9  what
: 464 4 9  it
: 469 4 9  is
: 476 2 9  we
: 479 9 11  have
: 489 2 11  to
: 492 16 13  lo
: 509 8 14 ~
: 518 14 13 se.
- 534 633
: 653 8 13 Mi
: 662 8 13 lle
: 671 4 13 ni
: 676 10 13 a
: 688 6 13  in
: 696 10 13  co
: 707 60 13 ming,
- 769 770
: 788 6 13  the
: 796 6 13  mod
: 804 6 6 ern
: 811 12 6  age
: 824 6 8  is
: 831 25 6  here.
- 858 901
: 921 6 13 It
: 929 10 18  sanc
: 940 4 13 ti
: 947 9 13 fies
: 957 4 12  the
: 962 11 13  fu
* 974 55 13 tu
: 1031 15 11 ~re,
- 1048
: 1056 8 13  yet
: 1065 8 13  ren
: 1075 8 6 ders
: 1085 5 6  us
: 1091 6 8  with
: 1099 20 6  fear.
- 1121 1177
: 1197 12 13 So
: 1211 12 13  ma
: 1224 8 13 ny
: 1233 9 13  the
: 1243 5 13 o
: 1249 16 13 ries,
- 1265
: 1265 12 11  so
: 1278 12 11  ma
: 1291 6 13 ny
: 1298 8 9  pro
: 1307 3 8 phe
: 1311 12 6 cies,
- 1325
: 1332 8 9 What
: 1342 14 9  we
: 1357 8 9  do
: 1366 14 9  need
: 1382 8 11  is
: 1391 4 9  a
: 1396 14 8  change
: 1412 8 6  of
: 1421 6 4  i
: 1428 25 6 deas.
- 1455
: 1463 9 13 When
: 1476 9 13  we
: 1487 4 13  are
: 1493 12 13  scared
- 1507
: 1509 10 13  we
: 1520 6 13  can
: 1527 14 11  hide
: 1542 4 11  in
: 1547 8 13  our
: 1558 8 9  re
: 1568 8 8 ve
: 1578 8 6 ries
: 1594 8 9  but
- 1604
: 1608 8 9 What
: 1617 8 9  we
: 1626 14 9  need
: 1643 8 11  is
: 1652 4 9  a
: 1658 14 8  change
: 1674 8 6  of
: 1684 8 4  i
: 1693 23 6 deas.
- 1718
: 1726 14 13 Change
: 1741 10 13  of
: 1752 8 13  i
: 1761 26 13 deas,
- 1789
: 1792 14 11  change
: 1808 8 11  of
: 1817 8 13  i
: 1826 10 9 de
: 1837 3 8 ~
: 1841 10 6 ~as,
- 1853
: 1860 8 9 What
: 1869 13 9  we
: 1883 7 9  need
: 1891 12 9  now
: 1907 9 11  is
: 1917 4 9  a
: 1922 14 8  change
: 1937 10 6  of
: 1948 8 4  i
: 1957 53 6 deas.
E
