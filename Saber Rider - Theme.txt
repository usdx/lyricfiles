#TITLE:Theme
#ARTIST:Saber Rider
#MP3:Saber Rider - Theme.mp3
#VIDEO:Saber Rider - Theme [VD#0].flv
#COVER:Saber Rider - Theme [CO].jpg
#BPM:308
#GAP:6350
#ENCODING:UTF8
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
: 0 12 5 Sa
: 14 15 12 ber
: 34 10 10  Ri
: 46 29 17 der
- 77
: 89 3 5 And
: 93 3 7  the
: 97 9 8  Star
: 108 8 12  Sher
: 117 7 10 iffs
- 126
: 158 10 5 Sa
: 172 15 12 ber
: 192 10 10  Ri
* 204 29 17 der
- 235
: 248 3 5 And
: 252 3 7  the
: 256 10 8  Star
: 267 8 12  Sher
: 275 8 10 iffs
: 291 4 3  in
: 296 4 5  the
: 302 10 5  sky
- 314
: 326 3 12 Can
: 330 3 12  you
: 334 7 12  feel
: 342 3 12  the
: 346 9 13  thun
: 355 6 12 der
: 362 6 10  in
: 368 8 8 side
- 378
: 381 10 10 Sa
: 393 14 15 ber
: 413 12 12  Ri
: 426 35 17 der
- 463
: 483 4 12 Make
: 488 4 12  the
: 492 6 12  light
: 500 3 12 ning
: 504 11 13  crack
: 516 5 12  as
: 521 5 10  you
: 527 5 8  ride
- 534
: 538 12 10 Sa
: 551 16 15 ber
: 571 11 12  Ri
: 583 32 17 der
- 1061
: 1081 6 13 Your
: 1089 7 12  des
: 1097 7 13 ti
* 1105 14 15 ny
: 1120 4 13  will
: 1125 8 12  lead
: 1134 4 10  yo
: 1138 3 8 u
- 1143
: 1145 6 13 To
: 1153 6 12  where'
: 1161 6 13 ver
: 1169 16 17  peop
: 1185 3 15 le
: 1189 4 17  ne
: 1193 4 20 ed
: 1197 5 20  you
- 1204
: 1209 7 13 Though
: 1217 7 12  dan
: 1225 6 13 ger
: 1233 14 15  may
: 1248 4 13  have
: 1252 7 12  found
: 1260 4 10  yo
: 1264 4 8 u
- 1270
: 1273 6 13 You
: 1281 6 12  have
: 1289 6 13  your
: 1296 13 17  friends
: 1310 5 15  a
: 1315 4 17 rou
: 1319 5 20 nd
: 1325 3 20  yo
: 1328 3 17 u
: 1332 28 19  now
- 1362
: 1369 6 19 now
: 1376 6 19  now
: 1383 6 19  now
- 1391
: 1394 11 5 Sa
: 1406 14 12 ber
: 1425 13 10  Ri
* 1439 26 17 der
- 1467
: 1481 3 5 And
: 1485 3 7  the
: 1489 10 8  Star
: 1500 9 12  Sher
: 1509 7 10 iffs
- 1518
: 1552 11 5 Sa
: 1564 16 12 ber
: 1583 13 10  Ri
: 1597 26 17 der
- 1625
: 1640 3 5 And
: 1644 3 7  the
: 1648 8 8  Star
: 1658 8 12  Sher
: 1667 7 10 iffs
: 1683 3 3  in
: 1687 5 5  the
: 1694 7 5  sky
- 1703
: 1717 4 12 Can
: 1722 3 12  you
: 1726 6 12  feel
: 1733 4 12  the
: 1738 7 13  thun
: 1745 4 13 der
: 1750 5 12  in
: 1756 4 10 sid
: 1761 3 8 e
- 1766
: 1772 13 10 Sa
: 1786 16 15 ber
: 1804 11 12  Ri
: 1816 36 17 der
- 1854
: 1875 4 12 Make
: 1879 3 12  the
: 1884 6 12  light
: 1890 3 12 ning
: 1895 6 13  crack
: 1903 4 13  as
: 1907 4 12  you
: 1915 4 10  rid
: 1919 5 8 e
- 1926
: 1931 11 10 Sa
: 1943 17 15 ber
: 1961 11 12  Ri
* 1974 31 19 der
- 2014
: 2477 6 13 Your
: 2484 7 12  des
: 2492 7 13 ti
: 2500 16 15 ny
: 2517 4 13  will
: 2522 8 12  lead
: 2530 4 10  yo
: 2534 3 8 u
- 2539
: 2541 6 13 To
: 2549 6 12  where'
: 2557 6 13 ver
* 2565 15 17  peop
: 2580 3 15 le
: 2584 5 17  ne
: 2589 5 20 ed
: 2594 7 20  you
- 2602
: 2604 7 13 Though
: 2612 7 12  dan
: 2620 7 13 ger
: 2628 15 15  may
: 2644 4 13  have
: 2648 6 12  found
: 2655 4 10  yo
: 2659 4 8 u
- 2665
: 2668 7 13 You
: 2676 7 12  have
: 2684 7 13  your
: 2692 14 17  friends
: 2708 4 15  a
: 2712 4 17 rou
: 2716 4 20 nd
: 2721 3 20  yo
: 2724 3 17 u
: 2728 31 19  now
- 2761
: 2764 6 19 now
: 2771 6 19  now
: 2779 6 19  now
- 2787
: 2789 13 5 Sa
: 2803 15 12 ber
: 2822 12 10  Ri
* 2835 38 17 der
- 2875
: 2877 3 5 And
: 2881 3 7  the
: 2885 10 8  Star
: 2896 7 12  Sher
: 2904 6 10 iffs
- 2912
: 2947 13 5 Sa
: 2961 17 12 ber
: 2980 12 10  Ri
: 2993 29 17 der
- 3024
: 3034 4 5 And
: 3039 4 7  the
: 3044 9 8  Star
: 3054 8 12  Sher
: 3062 6 10 iffs
: 3078 4 3  in
: 3082 4 5  the
: 3089 7 5  sky
- 3098
: 3199 7 8 Sa
: 3207 6 15 ber
: 3215 4 15  R
: 3220 5 17 i
* 3228 34 17 der
E