#TITLE:It's Amazing
#ARTIST:Jem
#LANGUAGE:English
#EDITION:Sing It Pop Hits
#YEAR:2008
#MP3:Jem - It's Amazing.mp3
#COVER:Jem - It's Amazing.jpg
#VIDEO:Jem - It's Amazing.avi
#BPM:300
#GAP:430
: 381 6 60 Do
: 388 6 55  it
- 403
: 406 12 58 now
- 427
: 430 5 60 you
: 436 5 62  know
: 443 4 63  who
* 448 5 65  you
: 455 11 62  are
- 470
: 472 2 58 you
: 478 6 60  feel
: 485 5 62  it
: 491 4 63  in
: 497 4 65  your
: 503 9 62  heart
- 514
: 516 4 60 and
: 521 2 58  you're
: 527 6 60  burn
: 534 3 55 ing
: 538 3 55  it
- 544
: 546 2 60 and
: 551 3 60  wish
: 557 5 58 ing
- 567
: 569 2 55 but
: 574 15 60  first
- 596
: 599 10 62 wait
- 615
: 618 3 58 won't
: 624 5 60  get
: 630 5 62  it
: 636 5 63  on
: 642 3 65  a
: 648 12 62  plate
- 665
: 667 2 58 you're
: 673 5 60  gon
: 679 4 62 na
: 685 3 63  have
: 690 5 65  to
: 696 9 62  work
: 709 5 60  for
: 715 2 58  it
- 719
: 721 5 60 har
: 727 6 55 der
- 738
: 740 2 60 and
: 746 5 60  har
: 752 6 58 der
- 761
: 763 3 55 and
: 770 2 59  I
: 773 16 60 ~
: 793 13 62  know
- 812
: 815 1 55 'cause
* 818 1 62  I've
* 820 3 63 ~
: 824 5 62  been
: 831 3 60  there
: 836 2 58  be
: 841 13 55 fore
- 863
: 867 3 63 knock
: 872 6 62 ing
: 879 4 60  on
: 885 3 58  the
: 891 10 55  doors
: 904 2 55  with
: 909 4 58  re
: 914 5 60 jec
: 921 5 60 tion
- 936
: 958 2 55 and
: 964 17 60  you'll
: 986 14 62  see
- 1006
: 1008 2 55 'cause
: 1011 5 63  if
: 1018 2 62  it's
: 1023 5 60  meant
: 1030 3 58  to
: 1036 12 55  be
- 1057
: 1060 4 63 no
: 1066 4 62 thing
: 1072 6 60  can
: 1079 2 58  com
: 1086 8 55 pare
: 1096 5 55  to
: 1103 4 58  de
: 1109 6 60 serv
: 1116 5 60 ing
- 1126
: 1128 3 62 your
: 1133 24 59  dreams
- 1166
: 1169 5 63 it's
: 1176 2 63  a
: 1182 11 63 maz
: 1194 14 62 ing
- 1215
: 1218 5 62 it's
: 1225 3 63  a
: 1231 11 62 maz
: 1243 12 60 ing
- 1263
: 1266 5 60 all
: 1273 1 60  that
: 1275 2 62 ~
: 1280 9 60  you
: 1291 8 59  can
- 1301
: 1303 2 59 do
: 1306 37 62 ~
- 1353
: 1363 5 63 it's
: 1370 2 63  a
: 1376 11 63 maz
: 1388 12 65 ing
- 1408
: 1411 4 65 makes
: 1418 1 65  my
: 1420 3 67 ~
: 1424 5 65  heart
: 1430 3 63 ~
: 1436 3 65  sing
: 1440 7 67 ~
- 1456
: 1460 6 67 now
: 1467 4 68  it's
: 1473 2 65  up
: 1476 5 67 ~
: 1485 10 65  to
* 1497 2 65  you
* 1500 38 67 ~
- 1542
: 1544 6 60 pa
: 1551 7 55 tience
- 1567
: 1570 10 58 now
- 1585
: 1587 3 55 frus
: 1594 4 60 tra
: 1600 3 62 tion's
: 1606 4 63  in
: 1612 5 65  the
: 1618 11 62  air
- 1634
: 1636 3 58 and
: 1642 5 60  peo
: 1648 5 62 ple
: 1655 4 63  who
: 1661 4 65  don't
: 1667 8 62  care
- 1677
: 1679 5 60 well
: 1685 2 58  it's
: 1691 5 60  gon
: 1697 4 55 na
: 1703 3 55  get
: 1709 5 60  you
: 1716 7 58  down
- 1730
: 1733 3 55 and
* 1739 13 60  you'll
- 1760
: 1763 12 62 fall
- 1780
: 1782 2 58 yes
: 1788 5 60  you
: 1794 4 62  will
: 1799 6 63  hit
: 1806 3 65  a
: 1812 10 62  wall
- 1828
: 1830 2 58 but
: 1836 4 60  get
: 1842 4 62  back
: 1848 6 63  on
: 1855 3 65  your
: 1861 5 62  feet
- 1868
: 1870 2 62 and
: 1873 4 60  you'll
: 1878 2 58  be
: 1882 8 60  stron
: 1891 5 55 ger
- 1901
: 1903 2 60 and
: 1907 8 60  smar
: 1916 5 58 ter
- 1926
: 1928 3 55 and
: 1933 21 60  I
: 1957 13 62  know
- 1976
: 1978 2 55 'cause
: 1981 5 63  I've
: 1988 5 62  been
: 1994 4 60  there
: 2000 1 58  be
: 2005 12 55 fore
- 2027
: 2031 4 63 knock
: 2037 3 62 ing
: 2043 5 60  down
: 2049 3 58  the
: 2054 9 55  doors
- 2065
: 2067 3 55 won't
: 2072 4 58  take
: 2079 10 60  no
: 2091 4 60  for
: 2097 5 62  an
: 2104 3 60  an
: 2111 5 58 swer
- 2119
: 2121 2 55 and
: 2128 12 60  you'll
- 2146
: 2149 15 62 see
- 2170
: 2173 1 55 'cause
: 2175 4 63  if
: 2181 4 62  it's
: 2188 3 60  meant
: 2194 3 58  to
: 2201 9 55  be
- 2220
: 2224 3 63 no
: 2230 3 62 thing
: 2236 4 60  can
: 2242 2 58  com
: 2248 9 55 pare
- 2259
: 2261 3 55 to
: 2266 4 58  de
: 2272 6 60 serv
: 2279 5 60 ing
- 2289
: 2291 3 62 your
: 2297 11 59  dreams
- 2318
: 2332 5 63 it's
: 2339 3 63  a
: 2346 11 63 maz
: 2359 14 62 ing
- 2379
: 2382 4 62 it's
: 2388 3 63  a
: 2394 11 62 maz
: 2407 12 60 ing
- 2427
: 2430 5 60 all
: 2436 2 60  that
: 2439 2 62 ~
: 2443 10 60  you
: 2455 8 59  can
- 2465
: 2467 2 59 do
: 2470 37 62 ~
- 2517
: 2526 5 63 it's
: 2533 2 63  a
: 2539 11 63 maz
: 2551 13 65 ing
- 2571
: 2574 4 65 makes
: 2581 1 65  my
: 2583 3 67 ~
: 2587 5 65  heart
: 2593 3 63 ~
: 2600 2 65  sing
: 2603 7 67 ~
- 2619
: 2623 6 67 now
: 2630 3 68  it's
: 2636 1 65  up
: 2638 6 67 ~
: 2648 10 65  to
: 2660 2 65  you
: 2663 38 67 ~
- 2741
* 2914 11 72 oh
* 2926 11 70 ~
* 2938 11 67 ~
* 2950 23 68 ~
* 2974 13 67 ~
- 2995
: 2998 24 65 oh
: 3023 23 67 ~
: 3047 42 71 ~
- 3094
: 3096 3 60 don't
: 3101 1 60  be
: 3103 1 60  em
: 3106 3 60 bar
: 3110 5 60 rassed
- 3119
: 3121 4 58 don't
: 3126 1 58  be
: 3128 2 58  a
: 3132 5 58 fraid
- 3143
: 3145 2 58 don't
: 3148 3 58  let
: 3152 2 58  your
: 3156 3 58  dreams
: 3161 1 58  slip
: 3163 2 58  a
: 3168 5 58 way
- 3184
: 3188 2 55 it's
: 3191 1 55  de
: 3193 5 56 ter
: 3199 1 56 mi
: 3201 2 56 na
: 3206 4 56 tion
- 3213
: 3215 1 56 and
: 3218 3 55  u
: 3222 1 55 sing
: 3224 2 55  your
: 3227 5 55  gift
- 3243
: 3247 2 55 e
: 3250 2 55 very
: 3253 1 55 bo
: 3255 2 55 dy
: 3259 1 55  has
: 3261 2 55  a
: 3264 5 55  gift
- 3279
: 3291 2 60 ne
: 3294 2 60 ver
: 3297 1 60  give
: 3299 3 60  up
- 3311
: 3315 2 58 ne
: 3318 2 58 ver
: 3321 1 58  be
: 3322 1 58 lieve
: 3324 2 58  the
: 3327 3 58  hype
- 3336
: 3339 4 58 trust
: 3344 2 58  your
: 3347 2 58  in
: 3351 4 58 stincts
- 3358
: 3360 2 58 and
: 3363 2 55  most
: 3367 2 55  im
: 3371 3 55 por
: 3375 1 55 tant
: 3377 3 55 ly
- 3389
: 3393 2 56 you've
: 3396 1 56  got
: 3398 3 56  no
: 3404 2 56 thing
: 3407 2 56  to
: 3410 5 55  lose
- 3425
: 3440 1 55 so
: 3442 2 55  just
: 3447 5 55  go
: 3454 1 55  for
: 3457 4 55  it
- 3471
: 3497 4 63 it's
: 3503 3 63  a
: 3509 12 63 maz
: 3522 12 62 ing
- 3542
: 3545 4 62 it's
: 3550 3 63  a
: 3555 13 62 maz
: 3570 12 60 ing
- 3589
: 3592 5 60 all
: 3598 2 60  that
: 3601 2 62 ~
: 3606 9 60  you
: 3617 8 59  can
- 3627
: 3629 2 59 do
: 3632 37 62 ~
- 3679
: 3690 4 63 it's
: 3696 3 63  a
: 3703 11 63 maz
: 3715 13 65 ing
- 3735
: 3738 5 65 makes
: 3745 1 65  my
: 3747 3 67 ~
: 3751 4 65  heart
: 3756 4 63 ~
: 3764 2 65  sing
: 3767 7 67 ~
- 3783
: 3786 6 67 now
: 3793 4 68  it's
: 3800 1 65  up
: 3802 6 67 ~
: 3812 10 65  to
: 3823 3 65  you
: 3827 38 67 ~
- 3875
: 3884 5 63 it's
: 3891 2 63  a
: 3897 11 63 maz
: 3909 12 62 ing
- 3930
: 3933 3 62 it's
: 3938 4 63  a
: 3943 14 62 maz
: 3958 12 60 ing
- 3978
: 3981 4 60 all
: 3987 1 60  that
: 3989 2 62 ~
: 3994 9 60  you
: 4005 9 59  can
- 4016
: 4018 1 59 do
: 4020 37 62 ~
- 4067
: 4078 3 63 it's
: 4084 2 63  a
: 4090 11 63 maz
: 4102 14 65 ing
- 4122
: 4125 4 65 makes
: 4132 1 65  my
: 4134 3 67 ~
: 4138 5 65  heart
: 4144 3 63 ~
: 4152 1 65  sing
: 4154 8 67 ~
- 4171
: 4175 6 67 now
: 4182 3 68  it's
: 4188 2 65  up
: 4191 5 67 ~
: 4200 8 65  to
- 4210
: 4212 2 65 you
: 4215 38 67 ~
- 4263
: 4272 11 72 ah
: 4284 11 70 ~
: 4296 11 67 ~
: 4308 23 68 ~
: 4332 11 67 ~
- 4353
: 4357 23 65 ah
: 4381 23 67 ~
: 4405 40 71 ~
- 4455
: 4466 11 72 ah
: 4478 11 70 ~
: 4490 10 67 ~
: 4501 25 68 ~
: 4527 9 67 ~
- 4547
* 4551 23 65 ah
* 4575 23 67 ~
* 4599 43 71 ~
E
