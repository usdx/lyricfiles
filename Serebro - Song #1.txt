#TITLE:Song #1
#ARTIST:Serebro
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
#MP3:Serebro - Song #1.mp3
#COVER:Serebro - Song #1 [CO].jpg
#BACKGROUND:Serebro - Song #1 [BG].jpg
#VIDEO:Serebro - Song #1 [VD#0,17].avi
#VIDEOGAP:0,17
#BPM:250
#GAP:24306
: 0 3 5 When
: 4 2 3  I
: 8 2 5  catch
: 12 2 5  you
: 16 3 5  pi
: 20 2 3 cking
: 24 2 5  me
- 28
: 36 6 5 When
: 44 2 3  I
: 48 3 5  get
: 52 2 3  a
: 56 3 7  chill
- 61
: 64 3 5 Boy
: 68 2 3  you
: 72 3 5  wan
: 76 2 3 na
: 80 3 5  take
: 84 2 3  on
: 88 2 5  me
- 92
: 96 3 5 Cause
: 100 4 5  I'm
: 108 2 5  your
: 112 3 7  ki
: 116 2 3 lling
: 120 2 3  pill
- 124
: 128 3 5 Can't
: 132 2 3  you
: 136 2 5  see
: 140 2 3  the
: 144 3 5  way
: 148 2 3  I
: 152 3 5  move
- 157
: 160 2 3 My
: 164 3 5  dress,
: 172 2 3  my
: 176 3 5  fla
: 180 2 3 shy
* 184 3 7  skin
- 189
: 192 2 5 Lis
: 196 2 3 ten
: 200 2 5  up,
: 204 2 3  you
: 208 3 5  know
: 212 2 3  I
: 216 3 5  got
- 221
: 224 2 5 The
: 228 3 5  place
: 236 2 5  you've
: 240 3 7  ne
: 244 2 3 ver
: 248 3 0  been
- 253
: 256 5 3 Slow
: 264 7 0  down
- 273 274
: 288 3 0 Boy,
: 292 2 0  you
: 296 3 7  don't
: 300 3 7  wa
: 304 2 7 nna
: 308 5 5  let
: 320 2 3  me
: 324 5 3  down
- 331 332
: 348 3 0 You
: 352 3 3  bet
: 356 2 0 ter
: 360 4 7  stop,
: 368 4 7  you
: 376 4 7  know
: 384 4 7  what
- 390
* 392 6 12 Oh,
: 404 3 5  don't
: 408 3 5  call
: 412 2 3  me
: 416 3 5  fu
: 420 2 3 nny
: 424 3 6  bu
: 428 2 5 nny
- 432
: 436 3 6 I'll
: 440 3 5  blow
: 444 2 3  your
: 448 3 5  mo
: 452 2 3 ney,
: 456 3 6  mo
: 460 2 5 ney
- 464
: 468 3 5 I'll
: 472 2 5  get
: 476 2 3  you
: 480 2 5  to
: 484 2 3  my
: 488 4 7  bad
: 496 4 7  ass
: 504 3 5  spin
: 508 2 3 ning
: 512 3 0  for
: 516 2 0  you
- 519
* 520 7 12 Oh,
: 532 3 5  I'll
: 536 3 5  make
: 540 2 3  it
: 544 3 5  ea
: 548 2 3 sy,
: 552 3 6  ho
: 556 2 5 ney
- 560
: 564 2 6 I'll
: 568 3 5  take
: 572 2 3  your
: 576 3 5  mo
: 580 2 3 ney,
: 584 3 6  yum
: 588 2 5 my
- 592
: 596 2 5 I've
: 600 2 5  got
: 604 2 3  my
: 608 3 5  bit
: 612 2 3 ches
: 616 4 7  stan
: 624 4 7 ding
: 632 2 5  up
: 636 2 3  next
: 640 3 0  to
: 644 3 0  me
- 649 720
: 740 2 0 My
: 744 4 7  bad
: 752 4 7  ass
: 760 3 5  spin
: 764 2 3 ning
: 768 3 0  for
: 772 2 0  you
- 776 848
: 868 2 0 My
: 872 4 7  bad
: 880 4 7  ass
: 888 3 5  spin
: 892 2 3 ning
: 896 3 0  for
: 900 2 0  you
- 904 905
: 928 3 5 Keep
: 932 2 3  on
: 936 3 5  ta
: 940 2 3 king
: 944 3 5  o
: 948 2 3 ver
: 952 2 5  you
- 956
: 960 2 3 It's
* 964 5 5  kin
: 972 2 3 da
: 976 3 5  get
: 980 2 3 ting
: 984 3 7  free
- 989
: 992 3 5 Ba
: 996 2 3 by
: 1000 3 5  boy
: 1004 2 3  you
: 1008 2 5  know
: 1012 2 3  I
: 1016 3 5  still
- 1021
: 1024 3 5 Got
: 1028 5 5  sex
: 1036 2 5 y
: 1040 3 7  freak
: 1044 2 3  in
: 1048 2 3  me
- 1052
: 1056 3 5 Got
: 1060 2 3 ta
: 1064 2 5  tease
: 1068 2 3  you
: 1072 2 5  nas
: 1076 2 3 ty
: 1080 3 5  guy
- 1085
: 1088 2 3 So
: 1092 5 5  take
: 1100 2 3  it,
: 1104 3 5  don't
: 1108 2 3  be
: 1112 3 7  shy
- 1117
: 1120 3 5 Put
: 1124 2 3  your
: 1128 3 5  che
: 1132 2 3 rry
: 1136 3 5  on
: 1140 2 3  my
: 1144 3 5  cake
- 1149
: 1152 3 3 And
: 1156 3 5  taste
: 1164 2 3  my
: 1168 3 7  che
: 1172 2 3 rry
: 1176 3 0  pie
- 1181
: 1184 5 3 Slow
: 1192 6 0  down
- 1200 1201
: 1216 3 0 Boy,
: 1220 2 0  you
: 1224 3 7  don't
: 1228 3 7  wan
: 1232 2 7 na
: 1236 5 5  let
: 1248 2 3  me
: 1252 5 3  down
- 1259 1260
: 1276 3 0 You
: 1280 3 3  bet
: 1284 2 0 ter
: 1288 4 7  stop,
: 1296 3 7  you
: 1304 4 7  know
: 1312 4 7  what
- 1318
* 1320 6 12 Oh,
: 1332 2 5  don't
: 1336 3 5  call
: 1340 2 3  me
: 1344 3 5  fun
: 1348 2 3 ny
: 1352 3 6  bun
: 1356 2 5 ny
- 1360
: 1364 3 6 I'll
: 1368 3 5  blow
: 1372 2 3  your
: 1376 3 5  mo
: 1380 2 3 ney,
: 1384 3 6  mo
: 1388 2 5 ney
- 1392
: 1396 2 5 I'll
: 1400 2 5  get
: 1404 2 3  you
: 1408 2 5  to
: 1412 2 3  my
: 1416 4 7  bad
: 1424 4 7  ass
: 1432 3 5  spin
: 1436 2 3 ning
: 1440 3 0  for
: 1444 2 0  you
- 1447
* 1448 7 12 Oh,
: 1460 2 5  I'll
: 1464 3 5  make
: 1468 2 3  it
: 1472 3 5  ea
: 1476 2 3 sy,
: 1480 3 6  ho
: 1484 2 5 ney
- 1488
: 1492 2 6 I'll
: 1496 3 5  take
: 1500 2 3  your
: 1504 3 5  mo
: 1508 2 3 ney,
: 1512 3 6  yum
: 1516 2 5 my
- 1520
: 1524 2 5 I've
: 1528 2 5  got
: 1532 2 3  my
: 1536 3 5  bit
: 1540 2 3 ches
: 1544 4 7  stan
: 1552 3 7 ding
: 1560 2 5  up
: 1564 2 3  next
: 1568 2 0  to
: 1572 3 0  me
- 1577 1578
: 1596 3 0 May
: 1600 2 0 be
: 1604 2 0  I'll
: 1608 3 7  take
: 1612 2 7  you
: 1616 2 7  with
: 1620 3 5  me
: 1632 2 0  toni
: 1636 4 0 ght
- 1642 1643
: 1660 3 0 May
: 1664 2 0 be
: 1668 2 0  you'll
: 1672 3 7  show
: 1676 2 7  me
: 1680 3 7  a
: 1684 5 5 no
: 1696 2 0 ther
: 1700 3 0  way
: 1704 2 3 ~
- 1708 1709
: 1724 3 0 And
: 1728 3 0  find
: 1732 2 0  a
: 1736 3 7  rea
: 1740 2 7 son
: 1744 2 7  for
: 1748 4 5  me
: 1760 2 0  to
: 1764 3 0  stay
- 1769 1770
: 1788 3 0 But
: 1792 3 0  some
: 1796 2 0 thing
: 1800 4 7  I
: 1808 4 7  must
: 1816 3 7  tell
* 1824 4 7  you
- 1830 2036
* 2056 7 12 Oh,
: 2068 3 5  don't
: 2072 3 5  call
: 2076 2 3  me
: 2080 3 5  fun
: 2084 2 3 ny
: 2088 3 6  bun
: 2092 2 5 ny
- 2096
: 2100 2 6 I'll
: 2104 3 5  blow
: 2108 2 3  your
: 2112 3 5  mo
: 2116 2 3 ney,
: 2120 3 6  mo
: 2124 2 5 ney
- 2128
: 2132 2 5 I'll
: 2136 2 5  get
: 2140 2 3  you
: 2144 2 5  to
: 2148 2 3  my
: 2152 4 7  bad
: 2160 4 7  ass
: 2168 3 5  spinn
: 2172 2 3 ing
: 2176 3 0  for
: 2180 2 0  you
- 2183
* 2184 7 12 Oh,
: 2196 2 5  I'll
: 2200 3 5  make
: 2204 2 3  it
: 2208 3 5  ea
: 2212 2 3 sy,
: 2216 3 6  ho
: 2220 2 5 ney
- 2224
: 2228 2 6 I'll
: 2232 3 5  take
: 2236 2 3  your
: 2240 3 5  mo
: 2244 2 3 ney,
: 2248 3 6  yum
: 2252 2 5 my
- 2256
: 2260 2 5 I've
: 2264 2 5  got
: 2268 2 3  my
: 2272 3 5  bit
: 2276 2 3 ches
: 2280 4 7  stan
: 2288 3 7 ding
: 2296 2 5  up
: 2300 2 3  next
: 2304 2 0  to
: 2308 3 0  me
- 2313 2384
: 2404 2 0 My
: 2408 4 7  bad
: 2416 4 7  ass
: 2424 3 5  spinn
: 2428 2 3 ing
: 2432 2 0  for
: 2436 2 0  you
- 2440 2512
: 2532 2 0 My
: 2536 4 7  bad
: 2544 4 7  ass
: 2552 3 5  spinn
: 2556 2 3 ing
: 2560 2 0  for
: 2564 2 0  you
E
