#TITLE:I'll Never Break Your Heart
#ARTIST:Backstreet Boys
#MP3:Backstreet Boys - I'll Never Break Your Heart.mp3
#COVER:Backstreet Boys - I'll Never Break Your Heart [CO].jpg
#BACKGROUND:Backstreet Boys - I'll Never Break Your Heart [BG].jpg
#BPM:54,1
#GAP:18300
: 0 1 65 From
: 1 4 70  the
: 5 4 70  first
: 9 5 70  day
- 15
: 15 1 72 That
: 16 1 74  I
: 17 1 72  saw
: 18 3 70  your
: 21 2 70  smi
: 23 2 70 ling
: 25 4 70  face
- 29
: 31 1 72 Honey
: 32 1 74  I
: 33 3 72  knew
: 36 1 70  that
: 37 2 70  we
: 39 2 70  would
: 41 3 72  be
- 44
: 45 1 70 Toge
: 46 1 70 ther
: 48 1 70  for
: 49 1 70 e
: 50 4 65 ver
- 54
: 55 1 67 Oo
: 56 1 69 
: 57 1 70 
: 58 1 69 
: 59 2 67 
: 61 2 74 h
: 63 1 72  when
: 64 1 74  I
: 65 2 74  aske
: 67 1 72 
: 68 1 70 d
: 69 3 70  yo
: 72 1 74 u
: 73 3 74  out
- 77
: 79 1 74 You
: 80 1 77  said
: 81 3 79  no
: 84 1 74  but
: 85 3 72  I
: 88 1 70  found
: 89 2 70  o
: 91 1 72 u
: 92 2 74 t
- 94
: 95 1 75 Dar
: 96 1 74 ling
: 97 2 72  that
: 99 1 70  you'd
: 100 1 70  been
: 101 1 70  hu
: 102 2 72 r
: 104 1 74 t
- 106
: 107 1 65 You
: 108 1 67  felt
: 109 1 70  that you'd
: 110 2 72  ne
: 112 1 74 ver
: 113 1 74  love
: 114 2 72  a
: 116 2 72 gai
: 118 1 70 
: 119 2 74 
: 121 1 72 
: 122 1 70 
: 123 1 72 
: 124 2 70 n
- 126
: 129 1 72 I
: 130 2 70  de
: 132 1 70 serve
: 133 1 70  a
: 134 2 72  try
: 136 1 74  hon
: 137 2 74 ey
- 139
: 140 1 67 Just
: 141 2 70  onc
: 143 1 67 e
- 144
: 145 2 74 Give
: 147 1 72  me
: 148 1 70  a
: 149 2 70  chance
: 151 1 70  and
: 152 1 72  I'll
: 153 1 74  prove
: 154 1 72  this
: 155 2 70  all
: 157 1 70  wrong
- 159
: 159 1 74 You
: 160 1 77  walked
: 161 3 77  in you
: 164 2 77  were so
: 167 1 74  quick
: 168 1 75  to
: 169 2 77  ju
: 171 1 75 d
: 172 1 74 g
: 173 2 72 e
- 175
: 176 1 74 But
: 177 2 77  ho
: 179 1 77 ney
: 180 1 77  it's
: 181 1 77  no
: 182 2 75 thing
: 184 1 74  like
: 185 1 74  m
: 186 2 72 e
- 188
: 189 1 70 I'll
: 190 2 72  ne
: 192 1 74 ver
: 193 4 77  break
: 197 4 74  your
: 201 1 72  hear
: 202 2 70 t
- 204
: 205 1 70 I'll
: 206 2 72  ne
: 208 1 74 ver
: 209 4 79  make
: 213 4 74  you
: 217 2 72  cr
: 219 2 70 y
- 221
: 221 1 70 I'd
: 222 2 72  ra
: 224 1 74 ther
: 225 3 79  die
: 229 1 79  than
: 230 2 77  live
: 232 2 75  withou
: 234 1 77 t
- 236
: 239 1 74 I'll
: 240 1 74  give
: 241 2 72  you
: 243 1 70  all
: 244 1 70  of
: 245 2 70  me
- 247
: 247 1 72 Ho
: 248 1 74 ney
: 249 1 72  that's
: 250 2 70  no
: 252 1 70  lie
- 253
: 253 1 70 I'll
: 254 2 72  ne
: 256 1 74 ver
: 257 4 77  break
: 261 4 74  your
: 265 1 72  hear
: 266 2 70 t
- 268
: 269 1 70 I'll
: 270 2 72  ne
: 272 1 74 ver
: 273 4 79  make
: 277 4 74  you
: 281 1 72  cr
: 282 3 70 y
- 285
: 285 1 70 I'd
: 286 2 72  ra
: 288 1 74 ther
: 289 3 79  die
: 293 1 79  than
: 294 2 77  live
: 296 2 75  withou
: 298 1 77 t
- 299
: 303 1 74 I'll
: 304 1 74  give
: 305 2 72  you
: 307 1 70  all
: 308 1 70  of
: 309 1 70  me
- 311
: 311 1 72 Ho
: 312 1 74 ney
: 313 1 72  that's
: 314 2 70  no
: 316 3 70  lie
- 320
: 321 4 70 As 
: 325 2 70  tim
: 327 1 67 
: 328 1 70 e
: 329 3 70  goes
- 333
: 334 1 77 B
: 335 2 74 y
: 337 2 72  you
: 339 2 70  will
: 341 3 67  get
: 344 1 70  to
: 345 2 72  know
: 347 3 74  me
- 351
: 352 1 74 A
: 353 1 75  litt
: 354 2 74 le
: 356 1 70  more
: 357 4 72  bet
: 361 2 67 ter
- 364
: 365 1 79 Girl
: 366 2 74  that's
: 368 1 74  the
: 369 1 74  way
: 370 2 72  love
: 372 2 72  go
: 374 3 67 es
- 379
: 384 1 77 And
: 385 4 74  I
: 390 2 70  know
: 392 1 72  you're
: 393 4 74  afraid
- 399
: 400 1 79 To
: 401 2 77  let
: 404 1 74  your
: 405 3 72  fee
: 408 1 74 lings
: 409 2 70  sh
: 411 1 72 o
: 412 2 74 w
- 415
: 415 2 72 And
: 417 1 75  I
: 418 2 74 
: 420 4 72 
: 424 1 67  under
: 425 4 72 stand
- 429
: 429 1 79 Girl
: 430 2 79  it's
: 432 1 79  time
: 433 1 79  to
: 434 2 77  let
: 436 2 77  g
: 438 1 75 
: 439 2 74 
: 441 1 72 
: 442 2 70 o
- 445
: 449 2 72 I de
: 451 2 70 serve
: 453 1 70  a
: 454 2 70  try
: 456 1 77  hon
: 457 2 74 ey
: 460 1 70  just
: 461 4 74  once
- 465
: 465 1 74 Give
: 466 2 72  me
: 468 1 70  a
: 469 2 70  chance
: 471 1 70  and
: 472 1 72  I'll
: 473 1 74  prove
: 474 2 77  this
: 476 1 77  all
: 477 1 74  wron
: 478 1 72 g
- 479
: 479 1 77 You
: 480 1 77  walked
: 481 1 77  in
: 483 1 77  you
: 484 2 79  so
: 486 1 77  quick
: 488 1 75  to
: 489 2 74  ju
: 491 1 72 d
: 492 1 74 g
: 493 3 75 e
- 496
: 497 1 77 Ho
: 498 2 77 ney
: 500 1 77  it's
: 501 1 77  no
: 502 2 74 thing
: 504 1 75  like
: 505 1 77  me
- 506
: 506 2 77 Dar
: 508 1 77 ling
: 509 1 77  why
: 510 2 77  can`t
: 512 1 77  you
: 513 4 86  see
- 517
: 517 1 70 I'll
: 518 2 72  ne
: 520 1 74 ver
: 521 4 77  break
: 525 4 74  your
: 529 1 72  hear
: 530 2 70 t
- 532
: 533 1 70 I'll
: 534 2 72  ne
: 536 1 74 ver
: 537 4 79  make
: 541 4 74  you
: 545 1 72  cr
: 546 3 70 y
- 549
: 549 1 70 I'd
: 550 2 72  ra
: 552 1 74 ther
: 553 4 79  die
: 557 1 79  than
: 558 2 77  live
: 560 3 75  withou
: 563 1 77 t
- 564
: 567 1 74 I'll
: 568 1 74  give
: 569 1 72  you
: 570 2 70  all
: 572 1 70  of
: 573 1 70  me
- 575
: 575 1 72 Ho
: 576 1 74 ney
: 577 1 72  that's
: 578 2 70  no
: 580 1 70  lie
- 581
: 581 1 70 I'll
: 582 2 72  ne
: 584 1 74 ver
: 585 4 77  break
: 589 4 74  your
: 593 1 72  hear
: 594 2 70 t
- 596
: 597 1 70 I'll
: 598 2 72  ne
: 600 1 74 ver
: 601 4 79  make
: 605 4 74  you
: 609 1 72  cr
: 610 1 70 y
- 612
: 613 1 70 I'd
: 614 2 72  ra
: 616 1 74 ther
: 617 3 79  die
: 621 1 79  than
: 622 2 77  live
: 624 3 75  withou
: 627 1 77 t
- 629
: 631 1 74 I'll
: 632 1 74  give
: 633 1 74  you
: 634 1 72  all
: 635 2 70  of
: 637 1 70  me
- 639
: 639 1 70 Ho
: 640 1 72 ney
: 641 1 74  that's
: 642 1 72  no
: 643 2 70  li
: 645 1 70 e
- 646
: 650 2 72 No
: 652 2 72  way, 
: 658 2 74 No
: 660 2 74  how
- 673
: 682 2 77 No
: 684 2 77  way,
: 690 2 78  No
: 692 2 78  how
- 708
: 709 1 71 I'll
: 710 2 73  ne
: 712 1 75 ver
: 713 4 78  break
: 717 4 75  your
: 721 1 73  hear
: 722 2 71 t
- 724
: 725 1 71 I'll
: 726 2 73  ne
: 728 1 75 ver
: 729 4 80  make
: 733 4 75  you
: 737 1 73  cr
: 738 3 71 y
- 741
: 741 1 71 I'd
: 742 2 73  ra
: 744 1 75 ther
: 745 3 80  die
: 749 1 80  than
: 750 2 78  live
: 752 3 76  withou
: 755 1 78 t
- 757
: 758 2 75 I'll
: 760 1 75  give 
: 761 1 73  you
: 762 2 71  all
: 764 1 71  of
: 765 2 71  me
- 767
: 767 1 73 Ho
: 768 1 75 ney
: 769 1 73  that's
: 770 2 71  no
: 772 1 71  lie
-773
: 773 1 71 I'll
: 774 2 73  ne
: 776 1 75 ver
: 777 4 78  brake
: 781 4 75  your
: 785 1 73  hear
: 786 2 71 t
- 789
: 789 1 71 I'll
: 790 2 73  ne
: 792 1 75 ver
: 793 4 80  make
: 797 4 75  you
: 801 1 73  cr
: 802 3 71 y
- 805
: 805 1 71 I'd
: 806 2 73  ra
: 808 1 75 ther
: 809 3 80  die
: 813 1 80  than
: 814 2 78  live
: 816 3 76  withou
: 819 1 78 t
- 821
: 822 2 75 I'll
: 824 1 75  give
: 825 1 75  you
: 826 2 73  all
: 828 1 71  of
: 829 1 71  me
- 830
: 831 1 73 Ho
: 832 1 75 ney
: 833 1 73  that's
: 834 2 71  no
: 836 1 71  lie
- 837
: 837 1 71 I'll
: 838 2 73  ne
: 840 1 75 ver
: 841 4 78  break
: 845 4 75  your
: 849 1 73  hear
: 850 2 71 t
- 852
: 853 1 71 I'll
: 854 2 73  ne
: 856 1 75 ver
: 857 4 80  make
: 861 4 75  you
: 865 1 73  cr
: 866 3 71 y
- 869
: 869 1 71 I'd
: 870 2 73  ra
: 872 1 75 ther
: 873 3 80  die
E
