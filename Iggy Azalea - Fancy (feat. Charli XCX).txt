#ARTIST:Iggy Azalea
#TITLE:Fancy (feat. Charli XCX)
#MP3:Iggy Azalea - Fancy (feat. Charli XCX).mp3
#AUTHOR:amro98
#GENRE:Electro hop
#YEAR:2014
#LANGUAGE:English
#BPM:253.22
#GAP:11777
#VIDEO:Iggy Azalea - Fancy (Explicit) ft. Charli XCX.mp4
F 1 3 0 First
F 6 3 0  thing
F 11 3 0  first
F 17 2 0  I'm
F 20 1 0  the
F 22 6 0  realest
- 36
F 39 2 0 Drop
F 42 2 0  this
F 45 2 0  and
F 48 2 0  let
F 51 2 0  the
F 55 3 0  whole
F 59 3 0  world
F 65 3 0  feel
F 69 3 0  it
- 84
F 88 2 0 And
F 91 2 0  I'm
F 94 2 0  still
F 97 2 0  in
F 100 2 0  the
F 103 4 0  murda
F 108 4 0  bizness
- 112
F 113 2 0 I
F 116 1 0  can
F 118 2 0  hold
F 121 2 0  you
F 124 8 0  down,
- 132
F 133 2 0 like
F 136 2 0  I'm
F 139 4 0  givin'
F 144 4 0  lesson
F 149 1 0  in
F 151 5 0  physics
- 163
F 166 2 0 You
F 169 2 0  should
F 172 2 0  want
F 174 1 0  a
F 176 6 0  bad
F 183 4 0  bitch
F 188 5 0  like
F 194 6 0  this
- 201
F 203 3 0 Drop
F 207 1 0  it
F 209 2 0  low
F 212 1 0  and
F 214 3 0  pick
F 218 2 0  it
F 221 3 0  up
F 225 5 0  just
F 231 5 0  like
F 237 5 0  this
- 248
F 250 4 0 Cup
F 255 2 0  of
F 258 3 0  Ace,
- 261
F 262 2 0 cup
F 265 2 0  of
F 268 4 0  Goose,
- 272
F 273 3 0 cup
F 276 2 0  of
F 279 5 0  Cris
- 284
F 285 4 0 High
F 290 4 0  heels,
- 294
F 295 4 0 somethin'
F 300 3 0  worth
F 304 1 0  a
F 306 2 0  half
F 309 1 0  a
F 311 3 0  ticket
F 315 2 0  on
F 318 2 0  my
F 321 4 0  wrist
- 333
F 336 5 0 Takin'
F 342 2 0  all
F 345 2 0  the
F 348 3 0  liquor
F 352 5 0  straight,
- 357
F 358 5 0 never
F 364 4 0  chase
F 369 5 0  that
- 377
F 379 9 0 Rooftop
F 389 3 0  like
F 393 3 0  we
F 397 3 0  bringin'
F 401 8 0  '88
F 410 6 0  back
- 423
F 426 4 0 Bring
F 431 2 0  the
F 434 4 0  hooks
F 440 3 0  in,
- 443
F 444 2 0 where
F 447 1 0  the
F 449 5 0  bass
F 455 4 0  at?
- 466
F 469 9 0 Champagne
F 479 4 0  spillin',
- 483
F 485 2 0 you
F 488 1 0  should
F 491 3 0  taste
F 495 4 0  that
- 500
: 502 4 10 I'm
: 507 2 7 ~
: 510 4 7  so
: 515 2 5 ~
: 518 4 5  fancy
: 523 2 3 ~
: 528 7 3 ~
- 544
: 548 4 10 You
: 553 2 10  al
: 556 4 10 rea
: 561 3 12 dy
: 565 7 10  know
: 573 6 7 ~
- 579
: 581 4 3 I'm
: 587 3 10  in
: 591 2 7 ~
: 594 4 7  the
: 599 2 5 ~
: 602 4 5  fast
: 607 2 3 ~
: 612 7 3  lane,
- 629
: 636 3 5 from
: 640 5 5  L.
: 646 2 5 A
: 649 2 5  to
: 652 4 5  To
* 657 2 3 ky
* 660 2 0 o
- 669
: 672 4 10 I'm
: 677 2 7 ~
: 680 4 7  so
: 685 2 5 ~
: 688 4 5  fancy
: 693 2 3 ~
: 698 7 3 ~
- 716
: 720 3 10 Can't
: 724 2 10  you
: 727 3 10  taste
: 732 3 12  this
: 736 5 10  gold?
: 742 5 7 ~
- 752
: 754 2 3 Re
: 758 3 10 mem
: 762 2 7 ~
: 765 4 7 ber
: 770 2 5 ~
: 773 4 5  my
: 778 2 3 ~
: 781 7 3  name,
- 788
: 790 4 0 about
: 795 4 0  to
: 800 4 7  blow
* 805 2 5 ~
* 808 2 3 ~
: 811 4 5 ~
: 816 2 3 ~
: 819 2 2 ~
* 822 5 3 ~
* 828 4 2 ~
* 833 5 0 ~
- 838
F 839 2 0 I
F 842 2 0  said
F 845 6 0  baby,
- 851
F 851 2 0 I
F 854 2 0  do
F 857 3 0  this,
- 861
F 863 2 0 I
F 866 3 0  thought
F 870 2 0  that,
- 872
F 873 2 0 you
F 876 2 0  knew
F 879 4 0  this
- 884
F 886 3 0 Can't
F 890 3 0  stand
F 894 2 0  no
F 897 5 0  haters
F 904 2 0  and
F 908 6 0  honest,
- 914
F 915 3 0 the
F 919 2 0  truth
F 922 4 0  is
- 926
F 927 3 0 And
F 931 1 0  my
F 933 3 0  flow
F 937 8 0  retarded,
- 945
F 947 3 0 they
F 951 4 0  speaked
F 956 2 0  it,
- 958
F 959 4 0 depart
F 965 3 0  it
- 970
F 972 6 0 Swagger
F 979 3 0  on
F 983 4 0  super,
- 987
F 988 2 0 I
F 991 2 0  can't
F 994 2 0  shop
F 997 2 0  at
F 1000 2 0  no
F 1003 5 0  department
- 1008
F 1008 2 0 Better
F 1011 2 0  get
F 1014 2 0  my
F 1017 3 0  money
F 1021 1 0  on
F 1023 4 0  time,
- 1027
F 1028 2 0 if
F 1031 2 0  they
F 1034 1 0  not
F 1036 5 0  money,
F 1042 5 0  decline
- 1047
F 1048 3 0 And
F 1052 3 0  swear
F 1056 2 0  I
F 1059 2 0  meant
F 1062 2 0  that
F 1065 1 0  there
F 1067 1 0  so
F 1069 1 0  much
- 1070
F 1071 1 0 that
F 1073 1 0  they
F 1075 1 0  give
F 1077 1 0  that
F 1079 3 0  line
F 1083 2 0  a
F 1086 5 0  rewind
- 1091
F 1092 2 0 So
F 1095 2 0  get
F 1098 2 0  my
F 1101 3 0  money
F 1105 2 0  on
F 1108 3 0  time,
- 1111
F 1112 2 0 if
F 1115 2 0  they
F 1118 2 0  not
F 1121 5 0  money,
F 1128 5 0  decline
- 1133
F 1135 2 0 I
F 1138 2 0  just
F 1141 2 0  can't
F 1144 4 0  worry
F 1149 2 0  'bout
F 1152 2 0  no
F 1155 4 0  haters,
- 1159
F 1160 3 0 gotta
F 1164 3 0  stay
F 1168 2 0  on
F 1171 2 0  my
F 1174 3 0  grind
- 1177
F 1178 2 0 Now
F 1181 1 0  tell
F 1183 2 0  me,
F 1186 2 0  who
F 1189 3 0  dat,
F 1193 2 0  who
F 1196 3 0  dat?
- 1201
F 1203 3 0 That
F 1207 3 0  do
F 1211 3 0  that,
F 1215 2 0  do
F 1218 3 0  that
- 1221
F 1222 3 0 Put
F 1226 1 0  that
F 1228 4 0  paper
F 1233 4 0  over
F 1238 3 0  all,
- 1241
F 1242 2 0 I
F 1244 2 0  thought
F 1247 2 0  you
F 1250 2 0  knew
F 1253 3 0  that,
F 1257 2 0  knew
F 1260 3 0  that
- 1263
F 1263 2 0 I
F 1266 2 0  be
F 1268 2 0  the
F 1271 2 0  I-
F 1276 2 0 G-
F 1279 2 0 G-
F 1282 3 0 Y,
- 1285
F 1287 2 0 put
F 1290 1 0  my
F 1292 3 0  name
F 1296 2 0  in
F 1300 4 0  bold
- 1306
F 1308 3 0 I
F 1312 1 0  been
F 1314 3 0  workin',
- 1317
F 1318 3 0 I'm
F 1322 2 0  up
F 1325 2 0  in
F 1328 1 0  here
F 1330 2 0  with
F 1333 2 0  some
F 1336 4 0  change
F 1341 2 0  to
F 1344 5 0  throw
- 1353
* 1355 4 10 I'm
: 1360 2 7 ~
: 1363 4 7  so
: 1368 2 5 ~
: 1371 4 5  fan
: 1376 2 3 ~
: 1381 8 3 cy
- 1399
: 1403 4 10 You
: 1408 2 10  al
* 1411 4 10 rea
* 1416 3 12 dy
: 1420 5 10  know
: 1426 5 7 ~
- 1433
: 1435 4 3 I'm
: 1441 3 10  in
: 1445 2 7 ~
: 1448 4 7  the
: 1453 2 5 ~
: 1456 4 5  fast
: 1461 2 3 ~
: 1466 7 3  lane,
- 1485
: 1489 4 5 from
: 1494 5 5  L.
* 1500 2 5 A
* 1503 2 5  to
: 1506 4 5  To
: 1511 2 3 ky
: 1514 2 0 o
- 1522
: 1525 4 10 I'm
: 1530 2 7 ~
: 1533 4 7  so
: 1538 2 5 ~
: 1541 4 5  fancy
: 1546 2 3 ~
: 1551 7 3 ~
- 1569
: 1573 3 10 Can't
: 1577 2 10  you
: 1580 3 10  taste
: 1585 3 12  this
: 1590 5 10  gold?
: 1596 5 7 ~
- 1604
* 1606 4 3 Re
: 1612 3 10 mem
: 1616 2 7 ~
: 1619 4 7 ber
: 1624 2 5 ~
: 1627 4 5  my
: 1632 2 3 ~
: 1635 7 3  name,
- 1642
: 1644 4 0 about
: 1649 4 0  to
: 1654 4 7  blow
: 1659 2 5 ~
: 1662 2 3 ~
: 1665 4 5 ~
: 1670 2 3 ~
: 1673 2 2 ~
: 1676 5 3 ~
: 1682 4 2 ~
: 1687 5 0 ~
- 1695
: 1697 6 7 Trash
* 1708 4 0  the
: 1713 9 3  ho
: 1724 9 5 tel
- 1737
: 1739 4 7 Let's
: 1746 4 7  get
: 1751 4 7  drunk
: 1756 2 7  on
: 1759 2 8  the
: 1762 2 7  mi
: 1765 2 5 ni
: 1768 6 3  bar
- 1780
: 1782 9 7 Make
: 1792 4 0  the
: 1798 9 3  phone
: 1809 9 5  call
- 1823
: 1825 4 7 Feels
: 1832 4 7  so
: 1837 4 7  good
: 1842 2 7  get
: 1845 2 8 ting
: 1848 2 7  what
: 1851 2 5  I
: 1854 6 3  want
- 1860
* 1862 5 5 Yeah
: 1868 8 7  keep
: 1879 8 7  on
- 1888
* 1890 4 7 Tur
* 1895 2 7 ning
* 1898 2 5  it
: 1901 5 7  up
- 1909
: 1911 4 7 Chan
: 1916 2 7 de
: 1919 2 5 lier
: 1922 3 7  swing
: 1926 3 7 ing
: 1930 2 5  we
: 1933 2 7  don't
: 1936 2 7  give
: 1940 2 5  a
: 1943 6 7  f*ck
- 1951
: 1953 8 7 Film
: 1964 8 7  star,
- 1973
: 1975 3 7 Yeah
: 1979 3 7  I'm
: 1983 3 5  de
: 1987 4 7 luxe
- 1994
: 1996 3 7 Cla
: 2001 2 7 ssic,
- 2003
: 2004 2 5 Ex
* 2007 3 7 pen
* 2011 3 7 sive
- 2014
* 2015 2 5 You
* 2018 2 7  don't
: 2021 2 7  get
: 2025 2 5  to
: 2028 4 7  touch
- 2032
F 2033 7 0 Ow!
- 2073
F 2139 6 0 Still
F 2146 5 0  stunting,
- 2151
F 2152 2 0 how
F 2155 2 0  you
F 2158 3 0  love
F 2162 4 0  that
- 2170
F 2172 2 0 Got
F 2174 2 0  the
F 2177 3 0  whole
F 2181 5 0  world
F 2188 5 0  asking
F 2194 2 0  how
F 2197 2 0  I
F 2200 4 0  does
F 2205 5 0  that
- 2213
F 2215 4 0 Hi
F 2220 4 0  girl,
- 2224
F 2225 5 0 hands
F 2231 4 0  off,
- 2235
F 2236 3 0 don't
F 2240 7 0  touch
F 2248 4 0  that
- 2255
F 2257 4 0 Look
F 2262 1 0  at
F 2264 1 0  that
F 2266 2 0  I
F 2269 2 0  bet
F 2272 2 0  you
- 2274
F 2275 2 0 wishing
F 2278 2 0  you
F 2281 2 0  could
F 2285 4 0  clutch
F 2291 4 0  that
- 2305
F 2318 2 0 Just
F 2320 2 0  the
F 2322 2 0  way
F 2324 2 0  you
F 2326 2 0  like
F 2330 3 0  it,
F 2334 5 0  huh?
- 2341
F 2343 3 0 You're
F 2347 2 0  so
F 2350 3 0  good,
- 2353
F 2354 2 0 he's
F 2357 2 0  just
F 2360 3 0  wishing
F 2364 2 0  he
F 2366 2 0  could
F 2369 3 0  bite
F 2373 3 0  it,
F 2377 4 0  huh?
- 2392
F 2396 5 0 Never
F 2402 3 0  turn
F 2406 5 0  down
F 2412 9 0  money
- 2421
F 2423 4 0 Slayin'
F 2428 5 0  these
F 2434 4 0  hoes,
- 2438
F 2439 4 0 gold
F 2444 4 0  trigger
F 2449 3 0  on
F 2453 3 0  the
F 2457 4 0  gun
F 2462 3 0  like
- 2465
: 2465 4 10 I'm
* 2470 2 7 ~
: 2473 4 7  so
: 2478 2 5 ~
* 2481 4 5  fan
: 2486 2 3 ~
: 2491 7 3 cy
- 2508
: 2512 4 10 You
: 2517 3 10  al
: 2521 4 10 rea
: 2526 4 12 dy
: 2531 5 10  know
: 2537 5 7 ~
- 2543
: 2545 4 3 I'm
: 2551 3 10  in
: 2555 2 7 ~
: 2558 4 7  the
: 2563 2 5 ~
: 2566 4 5  fast
: 2571 2 3 ~
: 2576 7 3  lane,
- 2595
: 2599 4 5 from
: 2604 5 5  L.
: 2610 2 5 A
: 2613 2 5  to
: 2616 4 5  To
: 2621 2 3 ky
: 2624 2 0 o
- 2633
: 2636 4 10 I'm
: 2641 2 7 ~
: 2644 4 7  so
: 2649 2 5 ~
: 2652 4 5  fan
: 2657 2 3 ~
: 2662 7 3 cy
- 2680
: 2684 3 10 Can't
: 2688 2 10  you
: 2691 3 10  taste
: 2696 3 12  this
: 2701 5 10  gold?
: 2707 5 7 ~
- 2714
: 2716 4 3 Re
: 2722 3 10 mem
: 2726 2 7 ~
: 2729 4 7 ber
: 2734 2 5 ~
: 2737 4 5  my
: 2742 2 3 ~
: 2745 7 3  name,
- 2753
* 2755 4 0 about
* 2760 4 0  to
: 2765 4 7  blow
: 2770 2 5 ~
: 2773 2 3 ~
: 2776 4 5 ~
: 2781 2 3 ~
: 2784 2 2 ~
: 2787 5 3 ~
: 2793 4 2 ~
: 2798 5 0 ~
- 2805
F 2807 4 0 Who
F 2812 3 0  that,
F 2816 3 0  who
F 2820 3 0  that,
- 2827
F 2829 4 0 I-
F 2834 2 0 G-
F 2837 2 0 G-
F 2840 4 0 Y
- 2845
F 2847 3 0 That
F 2851 2 0  do
F 2854 2 0  that,
F 2858 2 0  do
F 2861 2 0  that,
- 2866
F 2868 3 0 I-
F 2872 3 0 I-
F 2877 2 0 G-
F 2880 2 0 G-
F 2883 4 0 Y
- 2890
F 2892 4 0 Who
F 2897 3 0  that,
F 2901 2 0  who
F 2904 3 0  that,
- 2907
F 2908 3 0 I-
F 2912 3 0 I-
F 2916 3 0 I-
F 2920 2 0 G-
F 2923 2 0 G
F 2926 4 0 -Y
- 2933
* 2935 4 7 Blow
: 2940 2 5 ~
* 2943 2 3 ~
: 2946 4 5 ~
: 2951 2 3 ~
* 2954 2 2 ~
: 2957 5 3 ~
: 2963 4 2 ~
: 2968 5 0 ~
- 2974
F 2976 5 0 Who
F 2982 3 0  that,
F 2986 3 0  who
F 2990 3 0  that,
- 2998
F 3000 4 0 I-
F 3005 2 0 G-
F 3008 2 0 G-
F 3011 5 0 Y
- 3016
F 3017 4 0 That
F 3022 2 0  do
F 3025 4 0  that,
F 3030 1 0  do
F 3032 4 0  that,
- 3038
F 3040 3 0 I-
F 3044 3 0 I-
F 3048 2 0 G-
F 3051 2 0 G
F 3054 5 0 -Y
- 3062
F 3064 3 0 Who
F 3068 2 0  that,
F 3071 3 0  who
F 3075 2 0  that,
- 3077
F 3079 3 0 I-
F 3083 2 0 I-
F 3086 3 0 I-
F 3090 2 0 G-
F 3093 2 0 G-
F 3096 5 0 Y
- 3104
: 3106 4 7 Blow
: 3111 2 5 ~
: 3114 2 3 ~
: 3117 4 5 ~
: 3122 2 3 ~
: 3125 2 2 ~
* 3128 5 3 ~
* 3134 4 2 ~
* 3139 5 0 ~

