#TITLE:Absolute beginners
#ARTIST:David Bowie
#MP3:David Bowie - Absolute beginners.mp3
#VIDEO:David Bowie - Absolute beginners.flv
#COVER:David Bowie - Absolute beginners.jpg
#BPM:114.2
#GAP:28850
#ENCODING:UTF8
: 0 2 11 Ba
: 4 2 11 ba
: 8 2 11 ba
: 10 16 14 uuu 
- 30
: 32 2 11 Ba
: 36 2 11 ba
: 40 2 11 ba
: 42 16 13 uuu 
- 62
: 64 2 11 Ba
: 68 2 11 ba
: 72 2 11 ba
: 74 16 13 uuu 
- 94
: 96 2 11 Ba
: 100 2 11 ba
: 104 2 11 ba
: 106 16 14 uuu 
- 124
: 128 2 11 Ba
: 132 2 11 ba
: 136 2 11 ba
: 138 16 14 uuu 
- 156
: 160 2 11 Ba
: 164 2 11 ba
: 168 2 11 ba
: 170 16 13 uuu 
- 190
: 192 2 11 Ba
: 196 2 11 ba
: 200 2 11 ba
: 202 16 13 uuu 
- 220
: 224 2 11 Ba
: 228 2 11 ba
: 232 2 11 ba
: 234 16 14 uuu 
- 252
: 256 2 -3 I've 
: 258 2 2 not
: 260 2 4 hing 
: 262 2 2 much 
: 264 2 4 to 
: 266 2 6 of
: 268 4 6 fer 
- 280
: 320 2 7 There's 
: 322 2 8 not
: 324 2 9 hing 
: 326 2 8 much 
: 328 2 9 to 
: 330 4 10 take 
- 340
: 382 1 2 I'm 
: 384 2 2 an 
: 386 1 11 ab
: 388 2 11 so
: 390 2 9 lute 
: 392 2 7 be
: 394 2 9 gin
: 396 3 2 ner 
- 410
: 448 1 2 And 
: 449 1 2 I'm 
: 450 1 11 ab
: 452 2 11 so
: 454 2 9 lute
: 456 2 7 ly 
: 458 5 9 sane 
- 470
: 512 1 -3 As 
: 514 2 2 long 
: 516 2 4 as 
: 518 2 2 we're 
: 520 2 4 to
: 522 2 6 get
: 524 5 6 her 
- 540
: 576 2 8 The 
: 578 2 8 rest 
: 580 2 9 can 
: 582 2 8 go 
: 584 2 9 to 
: 586 6 10 hell 
- 594
: 640 2 2 I 
: 642 1 11 ab
: 644 2 11 so
: 646 2 9 lute
: 648 2 7 ly 
: 650 2 9 love 
: 652 5 2 you 
- 660
: 702 1 2 But 
: 704 2 2 we're 
: 706 1 11 ab
: 708 2 11 so
: 710 2 9 lute 
: 712 2 7 be
: 714 2 9 gin
: 716 4 2 ners 
- 730
: 768 2 2 With 
: 770 5 11 eyes 
: 776 2 11 com
: 778 2 9 plete
: 780 2 7 ly 
: 782 2 9 o
: 784 5 2 pen 
- 797
: 829 1 2 But 
: 831 2 4 ner
: 833 2 2 vous 
: 835 2 4 all 
: 837 4 5 the 
: 841 6 2 same 
- 849
: 855 8 11 If 
: 863 8 13 our 
: 871 5 14 love 
: 877 6 14 song 
- 885
: 891 1 14 Could 
: 893 6 16 fly 
: 899 2 14 o
: 901 2 13 ver 
: 903 5 14 moun
: 909 7 14 tains 
- 919
: 923 1 14 Could 
: 925 6 16 laugh 
: 931 2 14 at 
: 933 2 13 the 
: 935 5 14 o
: 941 7 14 cean 
- 951
: 957 4 14 Just 
: 961 4 13 like 
: 965 2 14 the 
: 967 8 13 films 
- 977
: 983 8 11 There's 
: 991 8 13 no 
: 999 5 14 rea
: 1005 6 14 son 
- 1015
: 1019 1 14 To 
: 1021 6 16 feel 
: 1027 2 14 all 
: 1029 2 13 the 
: 1031 5 14 hard 
: 1037 7 14 times 
- 1047
: 1051 1 14 To 
: 1053 6 16 lay 
: 1059 2 14 down 
: 1061 2 13 the 
: 1063 5 14 hard 
: 1069 7 14 lines 
- 1079
: 1083 1 14 It's 
: 1085 4 14 ab
: 1089 2 13 so
: 1091 4 14 lute
: 1095 4 16 ly 
: 1099 12 13 true 
- 1113
: 1117 2 11 Ba
: 1121 2 11 ba
: 1125 2 11 ba
: 1127 16 13 uuu 
- 1145
: 1149 2 11 Ba
: 1153 2 11 ba
: 1157 2 11 ba
: 1159 16 13 uuu 
- 1177
: 1181 2 11 Ba
: 1185 2 11 ba
: 1189 2 11 ba
: 1191 16 14 uuu 
- 1336
: 1340 2 2 Not
: 1342 2 4 hing 
: 1344 2 2 much 
: 1346 2 4 could 
: 1348 2 6 hap
: 1350 4 6 pen 
- 1362
: 1404 2 8 Not
: 1406 2 9 hing 
: 1408 2 8 we 
: 1410 4 9 can't 
: 1414 4 10 shake 
- 1422
: 1464 1 2 Oh 
: 1466 2 2 we're 
: 1468 1 11 ab
: 1470 2 11 so
: 1472 2 9 lute 
: 1474 2 7 be
: 1476 2 9 gin
: 1478 3 2 ners 
- 1492
: 1530 2 2 With 
: 1532 1 11 not
: 1534 2 11 hing 
: 1536 2 9 much 
: 1538 2 7 at 
: 1540 5 9 stake 
- 1550
: 1594 1 -3 As 
: 1596 2 2 long 
: 1598 2 4 as 
: 1600 2 2 you're 
: 1602 2 4 still 
: 1604 2 6 smi
: 1606 5 6 ling 
- 1618
: 1658 2 7 There's 
: 1660 2 8 not
: 1662 2 9 hing 
: 1664 2 8 more 
: 1666 4 9 I 
: 1670 6 10 need 
- 1682
: 1722 2 2 I 
: 1724 1 11 ab
: 1726 2 11 so
: 1728 2 9 lute
: 1730 2 7 ly 
: 1732 2 9 love 
: 1734 5 2 you 
- 1740
: 1782 1 2 But 
: 1784 2 2 we're 
: 1786 1 11 ab
: 1788 2 11 so
: 1790 2 9 lute 
: 1792 2 7 be
: 1794 2 9 gin
: 1796 4 2 ners 
- 1807
: 1847 2 2 But 
: 1849 2 2 if 
: 1851 4 11 my 
: 1855 2 9 love 
: 1857 2 7 is 
: 1859 2 9 your 
: 1861 4 2 love 
- 1870
: 1912 1 2 We're 
: 1914 2 4 cer
: 1916 2 2 tain 
: 1918 2 4 to 
: 1920 2 5 suc
: 1922 6 2 ceed 
- 1930
: 1938 8 11 If 
: 1946 8 13 our 
: 1954 5 14 love 
: 1960 6 14 song 
- 1970
: 1974 1 14 Could 
: 1976 6 16 fly 
: 1982 2 14 o
: 1984 2 13 ver 
: 1986 5 14 moun
: 1992 7 14 tains 
- 2000
: 2006 1 14 Could 
: 2008 6 16 sail 
: 2014 2 14 o
: 2016 2 13 ver 
: 2018 5 14 heart
: 2024 7 14 aches 
- 2032
: 2040 4 14 Just 
: 2044 4 13 like 
: 2048 2 14 the 
: 2050 8 13 films 
- 2060
: 2066 8 11 There's 
: 2074 8 13 no 
: 2082 5 14 rea
: 2088 6 14 son 
- 2098
: 2102 1 14 To 
: 2104 6 16 feel 
: 2110 2 14 all 
: 2112 2 13 the 
: 2114 5 14 hard 
: 2120 7 14 times 
- 2130
: 2134 1 14 To 
: 2136 4 18 lay 
: 2140 4 16 down 
: 2144 2 14 the 
: 2146 5 14 hard 
: 2152 7 14 lines 
- 2160
: 2166 1 14 It's 
: 2168 4 14 ab
: 2172 2 13 so
: 2174 4 14 lute
: 2178 4 16 ly 
: 2182 28 16 tru
: 2210 16 14 uue 
E