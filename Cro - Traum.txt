#ARTIST:Cro
#TITLE:Traum
#MP3:Cro - Traum.mp3
#VIDEO:Cro - Traum.mp4
#COVER:Cro - Traum.jpg
#AUTHOR:Nicolai (aac)
#EDITION:Songs von aac
#GENRE:Hip-Hop & Rap
#LANGUAGE:German
#BPM:240,06
#GAP:15005
: 0 4 -3 Yeah,
: 8 1 -3  Ba
: 10 1 -3 by
: 12 2 0  nimm'
: 16 1 0  mei
: 18 1 0 ne
: 20 2 0  Hand!
- 23
: 24 1 0 Ich
: 26 1 0  hab
: 28 1 0  al
: 30 1 0 les
: 32 1 0  schon
: 34 1 0  ge
: 36 2 -1 packt.
- 39
: 40 1 -1 Komm
: 42 1 -1  wir
: 44 1 -1  bei
: 46 1 -1 de
: 48 1 -1  ge
: 50 1 -1 hen
: 52 2 0  weg
: 56 2 -3  von
: 60 8 -3  hier.
- 70
: 72 1 -3 Sieh
: 74 1 -3  der
: 76 2 0  Jet
: 80 1 -3  ist
: 82 1 -3  ge
: 84 4 0 tankt.
- 88
: 88 1 0 Ich
: 90 1 0  hab
: 92 2 0  Geld
: 96 1 0  auf
: 98 1 0  der
: 100 4 -1  Bank,
- 104
: 104 1 -1 Und
: 106 1 -1  noch
: 108 1 -1  je
: 110 1 -1 de
: 112 1 -1  Men
: 114 1 -1 ge
: 116 2 0  Plät
: 120 2 -3 ze
* 124 8 -3  hier.
- 134
: 138 1 -3 Und
: 140 1 0  im
: 142 1 0 mer
: 144 1 0  wenn
: 146 1 0  Du
: 148 2 0  ein
: 152 2 0 sam
: 156 2 0  bist
- 159
: 160 1 0 Komm'
: 162 1 0  ich
: 164 2 -1  rum'
: 168 1 -1  Du
: 170 1 -1  musst
: 172 1 -1  nie
: 174 1 -1  wie
: 176 1 -1 der
: 178 1 -1  al
: 180 2 0 lei
: 184 2 -3 ne
: 188 10 -3  sein.
- 200
: 202 1 -3 Denn
: 204 1 0  im
: 206 1 0 mer
: 208 1 0  wenn
: 210 1 0  ich
: 212 2 0  Dich
: 216 2 0  seh'
- 219
: 220 1 0 Macht
: 222 1 0  es
: 224 1 0  in
: 226 1 0  mir
* 228 2 -1  Tick
: 232 2 -1  Tick
: 236 2 -1  Boom,
- 239
: 240 1 -1 So
: 242 1 -1  wie
: 244 2 0  Dy
: 248 2 -3 na
: 252 6 -3 mite
: 260 6 0 ~.
- 268
: 272 2 -1 A
: 276 4 0 ha,
: 286 1 -5  und
: 288 1 -5  al
: 290 1 -5 le
: 292 2 -1  ande
: 296 2 -1 ren
: 300 4 -1  Girls
- 304
: 304 1 -1 Wä
: 306 1 -1 ren
: 308 2 0  gern
: 312 2 -3  wie
: 316 12 -3  du.
- 330
: 336 2 -1 A
* 340 4 0 ha,
: 350 1 -5  Denn
: 352 1 -5  du
: 354 1 -5  bist
: 356 2 -1  wun
: 360 2 -1 der
: 364 4 -1 schön
- 368
: 368 1 -1 Und
: 370 1 -1  ge
: 372 2 0 fähr
: 376 2 -3 lich
: 380 12 -3  klug.
- 394
: 400 2 2 Ey
: 404 6 2  jo!
: 416 1 2  Und
: 418 1 2  ich
: 420 2 2  hoff'
: 424 2 2  dass
: 428 1 2  du
: 430 2 0  mich
: 434 1 4  siehst
: 436 4 2 ~,
- 440
: 440 2 2 Ich
* 444 2 2  bin
: 448 1 2  ver
: 450 1 4 liebt
: 452 2 2 ~
- 455
: 456 2 2 Und
: 460 2 2  hab
: 464 2 0  kein'
: 468 2 4  Plan
: 472 2 2  ob
: 476 2 2  es
: 480 2 0  dich
: 484 18 2  gibt
- 503
: 504 2 0 Doch
: 508 2 0  manch
: 512 2 0 mal
* 516 4 4  träum'
: 524 4 2  ich
: 532 4 0  nur
: 540 4 2  von
: 546 3 -3  Dir
: 550 6 -5 ~.
- 558
: 560 1 2 Bit
: 562 1 2 te
: 564 2 2  sag
: 568 2 2  was
: 572 1 2  muss
: 574 2 0  ich
: 578 1 4  tun
: 580 2 2 ~
: 584 2 2  dass
: 588 1 2  Du
: 590 2 0  mich
: 594 4 2  hörst!
- 600
: 632 2 0 Denn
: 636 2 0  ich
: 640 2 0  wär
: 644 4 4  heut
: 652 4 2  so
: 660 4 0  gern
: 668 4 2  bei
: 674 3 -3  dir
: 678 6 -5 ~
- 686
: 688 1 0 Und
: 690 1 0  ich
: 692 2 2  glaub
: 696 2 2  ich
: 700 1 2  fänd
: 702 2 0  es
: 706 1 4  cool
* 708 2 2 ~
: 712 1 2  wenn
: 714 1 2  du
: 716 2 2  mir
: 720 1 0  ge
* 722 6 2 hörst.
- 730
: 760 2 0 Ich
: 764 2 0  fühl
: 768 2 0  mich
: 772 4 0  so
: 780 4 2  al
: 788 6 4 lein,
- 795
: 796 2 0 Weiß
: 800 2 0  nicht
: 804 4 -1  obs
: 810 4 -5  dich
: 816 6 -3  gibt.
- 823
: 824 1 0 Und
: 826 1 0  e
: 828 2 0 gal
: 832 2 0  wie
: 836 4 0  laut
: 844 4 2  ich
* 852 4 4  schrei,
: 860 4 0  sie
: 868 4 -1  hört
: 874 4 -5  mich
: 880 10 -3  nicht.
- 892
: 896 2 0 Doch
: 900 2 9  sie
: 904 2 7  ist
: 908 2 7  gra
: 912 2 4 de
: 916 2 4  ir
: 920 2 2 gend
: 924 2 2 wo
: 928 2 0  und
: 932 4 2  denkt
: 938 1 4  viel
: 940 2 2 leicht
: 944 2 0  an
: 948 8 -3  mich.
- 958
: 960 2 0 Hey
: 964 2 4  Ba
: 968 2 2 by
: 972 2 2  bit
: 976 2 0 te
: 980 2 4  schreib
: 984 2 2  wenn
: 988 2 2  es
: 992 2 0  dich
: 996 19 2  gibt
: 1016 7 4 ~!
- 1025
: 1032 1 0 Je
: 1034 1 0 den
: 1036 2 0  Tag
: 1040 1 0  un
: 1042 1 0 ter
: 1044 2 0 wegs
: 1048 1 0  und
: 1050 1 0  ich
: 1052 2 0  seh'
: 1056 1 0  vie
: 1058 1 0 le
: 1060 4 -1  Girls,
- 1064
: 1064 1 -1 A
: 1066 1 -1 ber
: 1068 1 -1  Ba
: 1070 1 -1 by
: 1072 1 -1  ey
: 1074 1 -1  ich
* 1076 2 0  find
: 1080 2 -3  dich
: 1084 4 -3  nicht.
- 1090
: 1094 1 -3 Und
: 1096 1 -3  es
: 1098 1 -3  gibt
: 1100 2 0  so
: 1104 1 0  vie
: 1106 1 0 le
: 1108 4 0  Girls
- 1112
: 1112 1 0 Die
: 1114 1 0  be
: 1116 1 0 haup
: 1118 1 0 ten
: 1120 1 0  sie
: 1122 1 0  wärn
: 1124 4 -1  Du,
- 1128
: 1128 1 -1 Doch
: 1130 1 -1  ich
: 1132 1 -1  sa
: 1134 1 -1 ge
: 1136 1 -1  man
: 1138 1 -1  das
: 1140 2 0  stimmt
: 1144 2 -3  doch
* 1148 6 -3  nicht.
- 1156
: 1158 1 -3 Doch
: 1160 1 -3  ich
: 1162 1 -3  hab
: 1164 2 0  echt
: 1168 1 0  kei
: 1170 1 0 nen
: 1172 2 0  Plan
: 1176 1 0  und
: 1178 1 0  ich
: 1180 2 0  frag
: 1184 1 0  mich
: 1186 1 0  ob
: 1188 2 -1  Du
- 1191
: 1192 1 -1 Ü
: 1194 1 -1 ber
: 1196 2 -1 haupt
: 1200 1 -1  mei
: 1202 1 -1 ne
: 1204 2 0  Spra
: 1208 2 -3 che
: 1212 6 -3  sprichst.
- 1220
: 1222 1 -3 Doch
: 1224 1 -3  Du
: 1226 1 -3  bist
: 1228 1 0  ei
: 1230 1 0 ne
: 1232 1 0  von
: 1234 1 0  de
: 1236 1 0 nen
: 1238 1 0  die
: 1240 1 0  man
: 1242 1 0  nicht
: 1244 1 0  su
: 1246 1 0 chen
* 1248 2 0  darf,
- 1251
: 1252 1 -1 Son
: 1254 1 -1 dern
: 1256 1 -1  ir
: 1258 1 -1 gend
: 1260 1 -1 wann
: 1262 1 -1  mal
: 1264 1 -1  auf
: 1266 1 -1  der
: 1268 2 0  Stra
: 1272 2 -3 ße
: 1276 6 -3  trifft
: 1284 6 0 ~.
- 1292
: 1296 2 -1 A
: 1300 4 0 ha,
: 1310 1 -5  und
: 1312 1 -5  Ba
: 1314 1 -5 by
: 1316 2 -1  ich
: 1320 2 -1  schrieb
: 1324 2 -1  je
: 1328 2 -1 des
: 1332 2 0  Lied
: 1336 2 -3  für
: 1340 8 -3  dich.
- 1350
* 1360 2 -1 A
* 1364 4 0 ha,
: 1374 1 -5  doch
: 1376 1 -5  bin
: 1378 1 -5  al
: 1380 2 -1 lei
: 1384 2 -1 ne
: 1388 2 -1  denn
: 1392 2 -1  sie
: 1396 2 0  sieht
: 1400 2 -3  mich
: 1404 12 -3  nicht.
- 1418
: 1424 2 -1 A
: 1428 4 0 ha,
: 1442 1 0  ich
: 1444 2 2  hoff'
: 1448 2 2  dass
: 1452 2 2  es
: 1456 1 0  ge
: 1458 1 4 schieht
: 1460 2 2 ~,
: 1464 2 2  ich
: 1468 2 2  bin
: 1472 1 0  ver
: 1474 1 4 liebt
: 1476 2 2 ~,
- 1479
: 1480 2 2 Doch
: 1484 2 2  hab
: 1488 2 0  kein'
: 1492 2 4  Plan
: 1496 2 2  ob
: 1500 2 2  es
: 1504 2 0  Dich
: 1508 18 2  gibt.
- 1527
: 1528 2 0 Doch
: 1532 2 0  manch
: 1536 2 0 mal
: 1540 4 4  träum'
: 1548 4 2  ich
: 1556 4 0  nur
: 1564 4 2  von
: 1570 3 -3  Dir
: 1574 6 -5 ~.
- 1582
: 1584 1 2 Bit
: 1586 1 2 te
: 1588 2 2  sag
: 1592 2 2  was
: 1596 1 2  muss
* 1598 2 0  ich
: 1602 1 4  tun
: 1604 2 2 ~
: 1608 2 2  dass
: 1612 1 2  Du
: 1614 2 0  mich
* 1618 4 2  hörst!
- 1624
: 1656 2 0 Denn
: 1660 2 0  ich
: 1664 2 0  wär
: 1668 4 4  heut
: 1676 4 2  so
: 1684 4 0  gern
: 1692 4 2  bei
: 1698 3 -3  dir
: 1702 6 -5 ~
- 1710
: 1712 1 0 Und
: 1714 1 0  ich
: 1716 2 2  glaub
: 1720 2 2  ich
: 1724 1 2  fänd
: 1726 2 0  es
: 1730 1 4  cool
: 1732 2 2 ~
: 1736 1 2  wenn
: 1738 1 2  du
: 1740 2 2  mir
: 1744 1 0  ge
: 1746 6 2 hörst.
- 1754
: 1784 2 0 Ich
: 1788 2 0  fühl
: 1792 2 0  mich
: 1796 4 0  so
: 1804 4 2  al
: 1812 6 4 lein,
- 1819
: 1820 2 0 Weiß
: 1824 2 0  nicht
* 1828 4 -1  obs
: 1834 4 -5  dich
: 1840 6 -3  gibt.
- 1847
: 1848 1 0 Und
: 1850 1 0  e
: 1852 2 0 gal
: 1856 2 0  wie
: 1860 4 0  laut
: 1868 4 2  ich
: 1876 4 4  schrei,
: 1884 4 0  sie
: 1892 4 -1  hört
: 1898 4 -5  mich
: 1904 10 -3  nicht.
- 1916
: 1920 2 0 Doch
: 1924 2 9  sie
: 1928 2 7  ist
: 1932 2 7  gra
: 1936 2 4 de
: 1940 2 4  ir
: 1944 2 2 gend
: 1948 2 2 wo
: 1952 2 0  und
: 1956 4 2  denkt
: 1962 1 4  viel
: 1964 2 2 leicht
: 1968 2 0  an
: 1972 8 -3  mich.
- 1982
: 1984 2 0 Hey
: 1988 2 4  Ba
: 1992 2 2 by
: 1996 2 2  bit
: 2000 2 0 te
: 2004 2 4  schreib
: 2008 2 2  wenn
: 2012 2 2  es
: 2016 2 0  dich
* 2020 19 2  gibt
: 2040 7 4 ~!
- 2049
: 2056 1 0 Ba
: 2058 1 0 by
: 2060 1 0  war
: 2062 1 0 te
: 2064 1 0  nicht
: 2066 1 0  so
: 2068 1 0  lan
: 2070 2 0 ge,
- 2072
: 2072 1 0 Denn
: 2074 1 0  ich
: 2076 1 0  bin
: 2078 1 0  nicht
: 2080 1 0  gerne
: 2082 1 0  al
: 2084 1 -1 lei
: 2086 2 -1 ne
- 2088
: 2088 1 -1 Und
: 2090 1 -1  be
: 2092 1 -1 mer
: 2094 1 -1 ke
: 2096 1 -1  je
: 2098 1 -1 de
: 2100 2 0  Nacht
: 2104 1 -3  mein
: 2106 2 0  Bett
: 2110 1 -3  ist
: 2112 2 0  zu
: 2116 2 -3  groß.
- 2119
: 2120 1 -3 Des
: 2122 1 -3 halb
: 2124 1 0  hätt'
: 2126 1 0  ich
: 2128 1 0  ger
: 2130 1 0 ne
: 2132 1 0  dich
: 2134 1 0  an
: 2136 1 0  mei
: 2138 1 0 ner
: 2140 1 0  Sei
* 2142 2 0 te.
- 2144
: 2144 1 0 Ich
: 2146 1 0  kann
: 2148 1 -1  nur
: 2150 1 -1  noch
: 2152 1 -1  an
: 2154 1 -1  dich
: 2156 1 -1  den
: 2158 2 -1 ken,
- 2160
: 2160 1 -1 Man
: 2162 1 -1  es
: 2164 2 0  lässt
: 2168 1 -3  mich
: 2170 2 0  ein
: 2174 1 -3 fach
: 2176 2 0  nicht
: 2180 6 -3  los!
- 2186
: 2186 1 -3 Und
: 2188 1 0  wenn
: 2190 1 0  du
: 2192 1 0  mich
: 2194 1 0  da
: 2196 1 0  drau
: 2198 1 0 ßen
: 2200 1 0  gra
: 2202 1 0 de
: 2204 2 0  hörst,
- 2206
: 2206 1 0 Dann
: 2208 1 0  bit
: 2210 1 0 te
: 2212 1 -1  war
: 2214 1 -1 te
: 2216 1 -1  kurz
: 2218 1 -1  auf
* 2220 2 -1  mich.
- 2222
: 2222 1 0 Ich
: 2224 1 0  bin
: 2226 1 0  di
: 2228 2 0 rekt
: 2232 1 -3  be
: 2234 2 0 reit
: 2238 1 -3  und
: 2240 2 0  fahr
: 2244 4 -3  los.
- 2248
: 2248 1 0 Doch
: 2250 1 0  wenn
: 2252 2 0  nicht,
: 2256 1 0  gehe
: 2258 1 0  ich
: 2260 2 0  ein
: 2264 1 0 sam
: 2266 2 0  ins
: 2270 4 0  Bett
- 2274
: 2274 1 -1 Und
: 2276 2 0  hoff'
: 2280 1 0  das
: 2282 1 0  ich
: 2284 2 0  gleich
: 2288 1 0  wie
: 2290 1 0 der
: 2292 2 0  penn'.
- 2295
: 2296 2 0 Denn
: 2300 2 0  manch
: 2304 2 0 mal
: 2308 4 4  träum'
: 2316 4 2  ich
: 2324 4 0  nur
: 2332 4 2  von
: 2338 3 -3  Dir
: 2342 6 -5 ~.
- 2350
: 2352 1 2 Bit
: 2354 1 2 te
: 2356 2 2  sag
: 2360 2 2  was
: 2364 1 2  muss
* 2366 2 0  ich
: 2370 1 4  tun
: 2372 2 2 ~
: 2376 2 2  dass
: 2380 1 2  Du
: 2382 2 0  mich
: 2386 4 2  hörst!
- 2392
: 2424 2 0 Denn
: 2428 2 0  ich
: 2432 2 0  wär
: 2436 4 4  heut
: 2444 4 2  so
: 2452 4 0  gern
: 2460 4 2  bei
: 2466 3 -3  dir
: 2470 6 -5 ~
- 2478
: 2480 1 0 Und
: 2482 1 0  ich
: 2484 2 2  glaub
: 2488 2 2  ich
: 2492 1 2  fänd
: 2494 2 0  es
: 2498 1 4  cool
: 2500 2 2 ~
: 2504 1 2  wenn
: 2506 1 2  du
: 2508 2 2  mir
: 2512 1 0  ge
: 2514 6 2 hörst.
- 2522
: 2552 2 0 Ich
: 2556 2 0  fühl
: 2560 2 0  mich
: 2564 4 0  so
: 2572 4 2  al
* 2580 6 4 lein,
- 2587
: 2588 2 0 Weiß
: 2592 2 0  nicht
: 2596 4 -1  obs
: 2602 4 -5  dich
: 2608 6 -3  gibt.
- 2615
: 2616 1 0 Und
: 2618 1 0  e
: 2620 2 0 gal
: 2624 2 0  wie
: 2628 4 0  laut
: 2636 4 2  ich
: 2644 4 4  schrei,
: 2652 4 0  sie
: 2660 4 -1  hört
: 2666 4 -5  mich
: 2672 10 -3  nicht.
- 2684
* 2688 2 0 Doch
* 2692 2 9  sie
: 2696 2 7  ist
: 2700 2 7  gra
: 2704 2 4 de
: 2708 2 4  ir
: 2712 2 2 gend
: 2716 2 2 wo
: 2720 2 0  und
: 2724 4 2  denkt
: 2730 1 4  viel
: 2732 2 2 leicht
: 2736 2 0  an
: 2740 8 -3  mich.
- 2750
: 2752 2 0 Hey
: 2756 2 4  Ba
: 2760 2 2 by
: 2764 2 2  bit
: 2768 2 0 te
: 2772 2 4  schreib
: 2776 2 2  wenn
: 2780 2 2  es
: 2784 2 0  dich
* 2788 19 2  gibt
* 2808 7 4 ~!

