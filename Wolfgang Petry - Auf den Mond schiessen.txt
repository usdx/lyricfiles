#TITLE:Auf den Mond schiessen
#ARTIST:Wolfgang Petry
#MP3:Wolfgang Petry - Auf Den Mond Schiessen.mp3
#COVER:Wolfgang Petry - Auf Den Mond Schiessen [CO].jpg
#BACKGROUND:Wolfgang Petry - Auf Den Mond Schiessen [BG].jpg
#BPM:69,5
#GAP:14150
#ENCODING:UTF8
: 0 2 12 Wenn
: 2 3 11  Du
: 5 2 12  mor
: 7 1 12 gens
: 8 1 11  früh
: 9 1 11  die
: 10 1 9  Trep
: 11 1 9 pe
: 12 1 11  putzt
- 14
: 16 2 12 Nichts
: 18 2 11  an
: 21 2 12  au
: 23 1 12 ßer
: 24 1 12  ei
: 25 1 12 nem
: 26 2 14  kur
: 28 2 14 zen
: 30 1 11  Len
: 31 1 9 den
: 32 2 7 schurz
- 36
: 42 1 9 Dreh'
: 43 1 9  ich
: 44 1 9  fast
: 45 2 9  durch.
- 49
: 64 2 12 Wie
: 66 3 11  gehst
: 69 2 12  Du
: 71 1 12  mit
: 72 1 11  mei
: 73 1 11 nem
: 74 1 9  Klein
: 75 1 9 geld
: 76 1 11  um?
- 78
: 80 2 12 Schmeißt
: 82 2 11  es
- 85
: 85 1 11 Mit
: 86 1 12  vol
: 87 2 12 len
: 89 2 14  Hän
: 91 1 14 den
: 92 1 14  aus
: 93 1 14  dem
: 94 1 11  Fen
: 95 1 9 ster
: 96 2 7  raus
- 100
: 105 1 7 Das
: 106 1 9  halt
: 107 1 9  ich
: 108 1 9  nicht
: 109 2 9  aus.
- 113
: 125 1 9 Daß
: 126 1 11  Du
: 127 2 12  beim
: 129 2 12   Nach
: 131 2 12 barn
: 133 2 11  Blu
: 135 2 12 men
: 137 2 12  klaust
- 139
: 139 2 12 Nehm'
: 141 2 11  ich
: 143 2 12  ja
: 145 2 12  grad'
: 147 2 12  noch
: 149 1 9  hin.
- 152
: 157 1 11 Auch
: 158 1 11  wenn
: 159 2 12  Du
: 161 2 12  ihn
: 163 2 12  zum
: 165 2 11  Früh
: 167 2 12 stück
: 169 2 12  holst
- 171
: 171 2 12 Kaum
: 173 1 12  daß
: 174 2 17  ich
: 176 2 16  ge
: 178 1 14 gan
: 179 2 12 gen
: 181 2 14  bin
- 185
* 192 2 7 Könnt
* 194 2 9  ich
* 196 2 11  Dich...
- 199
: 200 3 12 Auf
: 203 3 12  den
: 206 3 14  Mond
: 209 3 14  schie
: 212 2 16 ßen
- 215
: 216 3 17 Hin
: 219 3 16 ter
: 222 3 14 her
: 225 3 12  flie
: 228 1 9 gen.
- 230
: 232 3 7 Mich
: 235 3 12  nicht
: 238 3 14  ein
: 241 3 14 krie
: 244 2 16 gen
- 247
: 248 3 17 Wenn
: 251 3 16  wir
: 254 3 14  uns
: 257 3 12  lie
: 260 2 14 ben.
- 263
: 264 3 19 Von
: 267 3 19  da
: 270 3 19  o
: 273 3 14 ben
: 280 3 17  auf
: 283 3 16  die
: 286 3 14  Welt
: 289 3 12  pfei
: 292 1 9 fen
- 294
: 296 3 7 Und
: 299 2 12  mit
: 301 4 14  Dir
: 305 3 19  nur
: 308 2 19  noch
: 310 2 19  nach
: 312 2 17  den
: 314 2 17  Ster
: 316 1 16 nen
: 317 2 16  grei
: 319 3 14 fen.
- 324
: 344 2 12 Wa
: 346 2 11 rum
: 349 2 12  übst
: 351 1 12  Du
: 352 1 11  gra
: 353 1 11 de
: 354 1 9  dann
: 355 1 9  Kla
: 356 1 11 vier
- 358
: 360 2 12 Ganz
: 362 2 11  laut
: 365 2 12  im
: 367 1 12 mer
: 368 1 12  wenn
: 369 1 12  ich
: 370 2 14  mit
: 372 2 14  Dir
- 374
: 374 1 11 Re
: 375 1 9 den
: 376 2 7  will
: 385 1 7  und
: 386 1 9  zwar
: 387 1 9  ü
: 388 1 9 ber
: 389 2 9  Dich?
- 393
: 408 2 12 Wenn
: 410 2 11  mir
- 413
: 413 2 12 Ir
: 415 1 12 gend
: 416 1 11 wann
: 417 1 11  der
: 418 1 9  Kra
: 419 1 9 gen
: 420 1 11  platzt
- 422
: 424 2 12 Und
: 426 2 11  Du
- 429
: 429 2 12 Vor
: 431 1 12  mir
: 432 1 12  stehst
: 433 1 12  und
: 434 2 14  im
: 436 2 14 mer
: 438 1 11  lau
: 439 1 9 ter
: 440 2 7  lachst
- 444
: 449 1 7 Und
: 450 1 9  zwar
: 451 1 9  ü
: 452 1 9 ber
: 453 2 9  mich.
- 457
: 469 1 9 Wie
: 470 1 11  halt'
: 471 2 12  ich
: 473 2 12  nur
: 475 2 12  das
: 477 2 11  Hin
: 479 2 12  und
: 481 2 12  Her
- 483
: 483 2 12 Und
: 485 2 11  Dei
: 487 2 12 ne
: 489 2 12  Lau
: 491 2 12 nen
: 493 1 9  aus?
- 496
: 501 1 11 Ich
: 502 1 12  sag's
: 503 2 12  Dir
: 505 2 12  jetzt
: 507 2 12  zum
: 509 2 12  letz
: 511 2 12 ten
: 513 2 12  Mal
- 516
: 516 1 12 Ich
: 517 1 12  heiß'
: 518 2 17  Wolf
: 520 2 16 gang
: 522 1 14  und
: 523 2 12  nicht
: 525 2 14  Klaus.
- 529
* 536 2 7 Ich
* 538 2 9  könnt'
* 540 2 11  Dich...
- 543
: 544 3 12 Auf
: 547 3 12  den
: 550 3 14  Mond
: 553 3 14  schie
: 556 2 16 ßen
: 560 3 17  hin
: 563 3 16 ter
: 566 3 14 her
: 569 3 12  flie
: 572 1 9 gen.
- 574
: 576 3 7 Mich
: 579 3 12  nicht
: 582 3 14  ein
: 585 3 14 krie
: 588 2 16 gen
- 591
: 592 3 17 Wenn
: 595 3 16  wir
: 598 3 14  uns
: 601 3 12  lie
: 604 2 14 ben.
- 607
: 608 3 19 Von
: 611 3 19  da
: 614 3 19  o
: 617 3 14 ben
: 624 3 17  auf
: 627 3 16  die
: 630 3 14  Welt
: 633 3 12  pfei
: 636 1 9 fen
- 638
: 640 3 7 Und
: 643 2 12  mit
: 645 4 14  Dir
: 649 3 19  nur
: 652 2 19  noch
: 654 2 19  nach
: 656 2 17  den
: 658 2 17  Ster
: 660 1 16 nen
: 661 2 16  grei
: 663 3 14 fen.
- 668
* 760 2 7 Ich
* 762 2 9  könnt
* 764 2 11  Dich
- 767
: 768 3 12 Auf
: 771 3 12  den
: 774 3 14  Mond
: 777 3 14  schie
: 780 2 16 ßen
: 784 3 17  hin
: 787 3 16 ter
: 790 3 14 her
: 793 3 12  flie
: 796 1 9 gen.
- 798
: 800 3 7 Mich
: 803 3 12  nicht
: 806 3 14  ein
: 809 3 14 krie
: 812 2 16 gen
- 815
: 816 3 17 Wenn
: 819 3 16  wir
: 822 3 14  uns
: 825 3 12  lie
: 828 2 14 ben.
- 831
: 832 3 19 Von
: 835 3 19  da
: 838 3 19  o
: 841 3 14 ben
: 848 3 17  auf
: 851 3 16  die
: 854 3 14  Welt
: 857 3 12  pfei
: 860 1 9 fen
- 862
: 864 3 7 Und
: 867 2 12  mit
: 869 4 14  Dir
: 873 3 19  nur
: 876 2 19  noch
: 878 2 19  nach
: 880 2 17  den
: 882 2 17  Ster
: 884 1 16 nen
: 885 2 16  grei
: 887 1 14 fen.
- 888
* 888 2 7 Ich
* 890 2 9  könnt
* 892 2 11  Dich
- 895
: 896 3 12 Auf
: 899 3 12  den
: 902 3 14  Mond
: 905 3 14  schie
: 908 2 16 ßen
: 912 3 17  hin
: 915 3 16 ter
: 918 3 14 her
: 921 3 12  flie
: 924 1 9 gen.
- 926
: 928 3 7 Mich
: 931 3 12  nicht
: 934 3 14  ein
: 937 3 14 krie
: 940 2 16 gen
- 943
: 944 3 17 Wenn
: 947 3 16  wir
: 950 3 14  uns
: 953 3 12  lie
: 956 2 14 ben.
- 959
: 960 3 19 Von
: 963 3 19  da
: 966 3 19  o
: 969 3 14 ben
: 976 3 17  auf
: 979 3 16  die
: 982 3 14  Welt
: 985 3 12  pfei
: 988 1 9 fen
- 990
: 992 3 7 Und
: 995 2 12  mit
: 997 4 14  Dir
: 1001 3 19  nur
: 1004 2 19  noch
: 1006 2 19  nach
: 1008 2 17  den
: 1010 2 17  Ster
: 1012 1 16 nen
: 1013 2 16  grei
: 1015 3 14 fen.
E