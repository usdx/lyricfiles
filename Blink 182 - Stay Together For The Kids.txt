#TITLE:Stay Together For The Kids
#ARTIST:Blink 182
#MP3:Blink 182 - Stay Together For The Kids.mp3
#COVER:Blink 182 - Stay Together For The Kids [CO].jpg
#BACKGROUND:Blink 182 - Stay Together For The Kids [BG].jpg
#BPM:145
#GAP:12800
#ENCODING:UTF8
: 0 2 50 It's 
: 2 4 54 hard 
: 6 2 52 to 
: 8 6 50 wake 
: 14 10 50 up
- 26
: 30 2 45 When 
: 32 2 45 the 
: 34 4 54 shades 
: 38 2 52 have 
: 40 4 50 been 
: 44 4 45 pulled 
: 48 8 52 shut
- 58
: 64 2 45 This 
: 66 4 54 house 
: 70 2 52 is 
: 72 4 50 haunt
: 76 4 45 ed
- 80
: 80 2 45 It's 
: 82 4 54 so 
: 86 2 52 path
: 88 4 50 e
: 92 4 45 tic
- 96
: 96 2 45 It 
: 98 4 49 makes 
: 102 2 50 no 
: 104 4 52 sense 
: 108 2 49 at 
: 110 12 50 all
- 125
: 128 2 45 I'm 
: 130 4 54 ripe 
: 134 2 52 with 
: 136 6 50 things 
: 142 2 49 to 
: 144 8 50 say
- 154
: 156 2 45 The 
: 158 4 45 words 
: 162 4 54 rot 
: 166 2 52 and 
: 168 6 50 fall 
: 174 2 45 a
: 176 8 52 way
- 186
: 192 2 45 My 
: 194 2 54 stup
: 196 4 52 id 
: 200 8 50 poem 
: 208 2 50 could 
: 210 2 54 fix 
: 212 4 55 this 
: 216 8 50 home
- 224
: 224 2 50 I'd 
: 226 4 49 read 
: 230 2 50 it 
: 232 6 52 e
: 238 2 49 very
: 240 12 50 day
- 254
: 256 2 67 So 
: 258 4 67 here's 
: 262 2 67 your 
: 264 6 67 hol
: 270 2 69 i
: 272 16 66 day
- 290
: 294 4 62 Hope 
: 298 4 62 you 
: 304 2 62 enjoy 
: 306 4 67 it 
: 310 4 62 this 
: 314 6 62 time
- 320
: 320 2 62 You 
: 322 4 67 gave 
: 326 2 67 it 
: 328 6 67 all 
: 334 2 69 a
: 336 16 66 way
- 355
: 358 4 62 It 
: 362 4 62 was 
: 366 12 67 mine
- 380
: 384 2 62 So 
: 386 4 67 when 
: 390 2 67 you're 
: 392 4 67 dead 
: 396 4 62 and 
: 400 16 66 gone
- 418
: 422 4 62 Will 
: 426 6 62 you 
: 434 2 67 re
: 436 2 67 member 
: 438 4 62 this 
: 442 6 62 night
- 449
: 450 4 67 twen
: 454 2 67 ty 
: 456 4 67 years 
: 460 4 62 now 
: 464 16 66 lost
- 482
: 486 4 66 It's 
: 490 4 62 not 
: 494 4 62 right
- 588
: 640 2 50 Their 
: 642 4 54 an
: 646 2 52 ger 
: 648 4 50 hurts 
: 652 4 52 my 
: 656 10 50 ears
- 666
: 668 2 45 Been 
: 670 2 45 runn
: 672 2 45 ing 
: 674 4 54 strong 
: 678 2 52 for 
: 680 6 50 se
: 686 2 45 ven 
: 688 8 52 years
- 698
: 700 2 45 Ra
: 702 2 45 ther 
: 704 2 45 than 
: 706 4 54 fix 
: 710 2 52 the 
: 712 4 50 prob
: 716 4 45 lems
- 720
: 720 2 45 they 
: 722 4 54 never 
: 726 2 52 solv
: 728 4 50 ed 
: 732 4 45 them
- 736
: 736 2 45 It 
: 738 4 49 makes 
: 742 2 50 no 
: 744 4 52 sense 
: 748 2 49 at 
: 750 12 50 all
- 765
: 768 2 50 I 
: 770 4 54 see 
: 774 2 52 them 
: 776 6 50 e
: 782 2 52 very
: 784 8 50 day
- 793
: 796 2 45 We 
: 798 2 45 get 
: 800 2 45 a
: 802 4 54 long 
: 806 2 52 so 
: 808 4 50 why 
: 812 4 45 can't 
: 816 8 52 they
- 826
: 828 2 45 If 
: 830 2 45 this 
: 832 2 45 is 
: 834 2 54 what 
: 836 4 52 he 
: 840 6 50 wants
- 846
: 846 2 45 and 
: 848 2 45 it's 
: 850 2 54 what 
: 852 4 52 she 
: 856 8 50 wants
- 864
: 864 2 50 Then 
: 866 4 49 why's 
: 870 2 50 there 
: 872 4 52 so 
: 876 2 49 much 
: 878 12 50 pain
- 892
: 896 2 67 So 
: 898 4 67 here's 
: 902 2 67 your 
: 904 6 67 hol
: 910 2 69 i
: 912 16 66 day
- 930
: 934 4 62 hope 
: 938 4 62 you 
: 944 2 62 enjoy 
: 946 4 67 it 
: 950 4 62 this 
: 954 6 62 time
- 960
: 960 2 62 You 
: 962 4 67 gave 
: 966 2 67 it 
: 968 6 67 all 
: 974 2 69 a
: 976 16 66 way
- 996
: 998 4 62 It 
: 1002 4 62 was 
: 1006 12 67 mine
- 1020
: 1024 2 62 So 
: 1026 4 67 when 
: 1030 2 67 you're 
: 1032 4 67 dead 
: 1036 4 62 and 
: 1040 16 66 gone
- 1058
: 1062 4 62 Will 
: 1066 6 62 you 
: 1074 2 67 re
: 1076 2 67 member 
: 1078 4 62 this 
: 1082 6 62 night
- 1089
: 1090 4 67 twen
: 1094 2 67 ty 
: 1096 4 67 years 
: 1100 4 62 now 
: 1104 16 66 lost
- 1122
: 1126 4 66 It's 
: 1130 4 62 not 
: 1134 4 62 right
- 1288
: 1536 2 67 So 
: 1538 4 67 here's 
: 1542 2 67 your 
: 1544 6 67 hol
: 1550 2 69 i
: 1552 16 66 day
- 1572
: 1574 4 62 hope 
: 1578 4 62 you 
: 1584 2 62 enjoy 
: 1586 4 67 it 
: 1590 4 62 this 
: 1594 6 62 time
- 1600
: 1600 2 62 you 
: 1602 4 67 gave 
: 1606 2 67 it 
: 1608 6 67 all 
: 1614 2 69 a
: 1616 16 66 way
- 1636
: 1638 4 62 It 
: 1642 4 62 was 
: 1646 12 67 mine
- 1662
: 1664 2 62 So 
: 1666 4 67 when 
: 1670 2 67 you're 
: 1672 4 67 dead 
: 1676 4 62 and 
: 1680 16 66 gone
- 1700
: 1702 4 62 Will 
: 1706 6 62 you 
: 1714 2 67 re
: 1716 2 67 member 
: 1718 4 62 this 
: 1722 6 62 night
- 1729
: 1730 4 67 twen
: 1734 2 67 ty 
: 1736 4 67 years 
: 1740 4 62 now 
: 1744 16 66 lost
- 1762
: 1766 4 66 It's 
: 1770 4 62 not 
: 1774 6 62 right
- 1780
: 1798 4 66 It's 
: 1802 4 62 not 
: 1806 6 62 right
- 1818
: 1830 4 66 It's 
: 1834 4 62 not 
: 1838 24 67 right
- 1862
: 1862 4 66 It's 
: 1866 4 62 not 
: 1870 6 62 right
E