#TITLE:Whatever You Want
#ARTIST:Status Quo
#MP3:Status Quo - Whatever You Want.mp3
#VIDEO:Status Quo - Whatever You Want.mp4
#COVER:Status Quo - Whatever You Want.jpg
#BPM:244,46
#GAP:72090
#VIDEOGAP:7
#ENCODING:UTF8
: 0 2 7 What
: 3 3 7 ever
: 7 3 6  you
: 11 4 6  want
- 27
: 31 3 7 What
: 35 3 7 ever
: 39 3 6  you
: 43 6 6  like,
- 59
: 63 3 7 What
: 67 3 7 ever
: 71 3 6  you
: 75 4 6  say,
: 80 1 7  you
: 82 5 7  pay
: 88 2 6  your
: 91 2 6  mon
: 94 1 6 ey,
- 95
: 96 2 7 You
: 99 4 7  take
: 104 1 6  your
: 106 8 6  choice
- 123
: 127 3 6 What
: 131 2 6 ever
: 134 3 4  you
: 138 8 4  need,
- 156
: 160 3 6 What
: 164 2 6 ever
: 167 3 7  you
: 171 8 9  use,
- 188
: 192 3 7 What
: 196 2 7 ever
: 199 3 6  you
: 203 5 6  win,
- 220
: 224 3 7 What
: 228 2 7 ever
: 231 3 6  you
: 235 10 6  loose
- 253
: 256 2 7 You're
: 259 5 7  show
: 265 1 6 ing
: 267 4 6  of,
- 281
: 288 2 7 You're
: 291 5 7  show
: 297 2 6 ing
: 300 4 6  out
- 316
: 320 2 7 You
: 323 4 7  look
: 328 2 6  for
: 331 4 6  trou
: 336 2 7 ble,
- 338
: 340 3 7 Turn
: 344 3 6  a
: 348 4 6 round,
: 355 2 7  give
: 358 1 7  me
: 360 2 6  a
: 363 8 6  shout
- 380
: 384 2 6 I
: 387 4 6  take
: 392 2 4  it
: 395 8 4  all
- 412
: 416 2 6 You
: 419 4 6  squeeze
: 424 3 4  me
: 428 7 8  dry
- 444
: 448 2 7 And
: 451 5 7  now
: 457 2 6  to
: 460 3 6 day
: 464 4 7  you
: 469 3 7  could
: 473 2 6 n't
: 476 3 6  e
: 480 3 7 ven
: 484 3 7  say
: 488 3 6  good
: 492 7 6 bye
- 509
: 523 11 5 I
: 536 2 4  could
: 539 7 3  take
: 547 4 4  you
: 552 9 4  home
: 563 3 4  on
: 567 2 4  the
* 570 4 4  mid
* 575 7 3 night
* 583 9 4  train
: 594 3 4  a
: 598 10 2 gain
- 624
: 648 10 2 I
: 660 3 2  could
: 664 6 1  make
: 672 4 1  an
: 677 6 0  of
: 684 2 0 fer
* 687 6 0  you
* 694 8 2  can't
* 704 3 -1  re
: 709 15 4 fuse
- 740
: 764 3 7 What
: 768 2 7 ever
: 771 2 6  you
: 774 4 6  want
- 788
: 796 3 7 What
: 800 2 7 ever
: 803 2 6  you
: 806 6 6  like
- 823
: 827 3 7 What
: 831 2 7 ever
: 834 2 6  you
: 837 5 6  say,
* 843 2 7  you
* 846 4 7  pay
* 851 2 6  your
: 854 2 6  mo
: 857 2 6 ney,
- 859
: 860 2 7 you
: 863 3 7  take
: 867 2 6  your
: 870 8 6  choice
- 887
: 891 3 6 What
: 895 2 6 ever
: 898 3 4  you
: 902 7 4  need
- 918
: 922 3 6 What
: 926 2 6 ever
: 929 3 7  you
: 933 10 9  use,
- 951
: 954 3 7 What
: 958 2 7 ever
: 961 3 6  you
: 965 5 6  win,
- 982
: 986 3 7 What
: 990 2 7 ever
: 993 3 6  you
: 997 10 6  loose
- 1039
: 1303 3 7 You're
: 1307 4 7  show
: 1312 1 6 ing
: 1314 5 6  of
- 1331
: 1335 2 7 You're
: 1338 5 7  show
: 1344 2 6 ing
: 1347 4 6  out
- 1363
: 1367 2 7 You
: 1370 4 7  look
: 1375 2 6  for
: 1378 4 6  trou
: 1383 2 7 ble,
- 1385
: 1386 4 7 Turn
: 1391 3 6  a
: 1395 6 6 round,
: 1402 2 7  give
: 1405 2 7  me
: 1408 1 6  a
: 1410 7 6  shout
- 1427
: 1431 2 6  I
: 1434 5 6  take
: 1440 1 5  it
: 1442 7 5  all
- 1459
: 1463 3 6 You
: 1467 3 6  squeeze
: 1471 2 4  me
: 1474 8 8  dry
- 1491
: 1495 3 7 And
: 1499 3 7  now
: 1503 3 6  to
: 1507 3 6 day
: 1512 1 7  you
: 1514 4 7  could
: 1519 1 6 n't
: 1521 4 6  e
: 1526 2 7 ven
: 1529 5 7  say
: 1535 2 6  good
: 1539 6 6 bye
- 1555
: 1569 11 5 I
: 1582 2 4  could
: 1585 7 3  take
: 1593 4 4  you
: 1598 9 4  home
: 1609 3 4  on
: 1613 2 4  the
: 1616 4 4  mid
: 1621 7 3 night
: 1629 9 4 train
: 1640 3 4  a
: 1644 10 2 gain
- 1670
: 1694 10 2 I
: 1706 3 2  could
: 1710 6 1  make
: 1718 4 1  an
: 1723 6 0  of
: 1730 2 0 fer
: 1733 6 0  you
: 1740 8 2  can't
: 1750 3 -1  re
: 1755 15 4 fuse
- 1786
: 1810 3 7 What
: 1814 2 7 ever
: 1817 2 6  you
: 1820 4 6  want
- 1834
: 1841 3 7 What
: 1845 2 7 ever
: 1848 2 6  you
: 1851 4 6  like
- 1865
: 1872 3 7 What
: 1876 2 7 ever
: 1879 2 6  you
: 1882 7 6  say,
: 1890 1 7  you
: 1892 3 7  pay
: 1896 2 6  your
: 1899 2 6  mo
: 1902 2 6 ney
- 1904
: 1905 1 7 You
: 1907 4 7  take
: 1912 1 6  your
: 1914 8 6  choice
- 1931
: 1935 3 6 What
: 1939 2 6 ever
: 1942 3 4  you
: 1946 8 4  need
- 1963
: 1967 3 6 What
: 1971 2 6 ever
: 1974 3 7  you
: 1978 11 9  use
- 1996
: 1999 3 7 What
: 2003 2 7 ever
: 2006 2 6  you
: 2009 7 6  win
- 2026
: 2030 2 6 What
* 2033 3 6  ever
* 2037 3 4  you
: 2041 7 4  loose
- 2057
: 2061 3 4 What
: 2065 2 4 ever
: 2068 3 2  you
: 2072 8 2  want
- 2112
: 2172 3 7 What
: 2176 2 7 ever
: 2179 2 6  you
: 2182 8 6  want
E