#TITLE:Blood On The Dance Floor
#ARTIST:Michael Jackson
#MP3:Michael Jackson - Blood On The Dance Floor.mp3
#VIDEO:Michael Jackson - Blood On The Dance Floor.mp4
#COVER:Michael Jackson - Blood On The Dance Floor.jpg
#BPM:218
#GAP:36500
#VIDEOGAP:0.5
#ENCODING:UTF8
#LANGUAGE:ENGL
: 0 2 -7 She
: 4 2 -7  got
: 8 2 -7  your
: 12 4 -5  num
: 18 4 -9 ber!
- 29
: 32 2 -7 She
: 36 2 -7  know
: 40 2 -7  your
: 44 6 -5  game!
- 60
: 64 2 -7 Look
: 68 2 -7  what
: 72 2 -7  you're
: 76 4 -5  un
: 82 4 -9 der!
- 93
: 96 2 -7 It's
: 100 2 -7  so
: 104 4 -7  in
: 109 3 -5 sane!
- 124
: 128 2 -7 Since
: 132 2 -7  you
: 136 2 -7  se
: 140 4 -5 duced
: 146 4 -9  her
- 157
: 160 2 -7 how
: 164 2 -7  does
: 168 2 -7  it
: 172 6 -5  feel?
- 188
: 192 2 -7 To
: 196 2 -7  know
: 200 2 -7  that
: 204 5 -5  wo
: 210 4 -9 man
- 221
: 224 2 -7 is
: 228 2 -7  out
: 232 2 -7  to
: 236 4 -5  kill!
- 247
: 250 4 -9 E
: 254 1 -9 very
: 256 2 -9  night's
: 260 2 -9  stance
: 264 2 -9  is
: 268 1 -9  like
: 270 1 -9  takin'
: 272 2 -9  a
: 276 2 -9  chan
: 278 1 -9 ce!
- 279
: 280 1 -9 It's
: 282 2 -10  not
: 286 1 -10  a
: 288 1 -10 bout
: 290 1 -10  love
: 292 2 -10  and
: 296 2 -10  ro
: 300 1 -10 mance
- 301
: 302 1 -10 and now
: 304 2 -10  you're gon
: 308 2 -10 na
: 312 1 -10  get
: 314 4 -12  it!
- 347
: 378 4 -9 E
: 382 1 -9 very
: 384 2 -9  hot
: 388 2 -9  man
: 392 2 -9  is
: 396 2 -9  out
: 400 2 -9  tak
: 404 1 -9 in'
: 406 2 -9  a
: 410 2 -10  chance!
- 414
: 416 2 -10 It's
: 422 2 -10  not a
: 424 2 -10 bout
: 428 1 -10  love and
: 430 2 -10  ro
: 432 1 -10 mance
- 433
: 434 1 -10 and
: 436 1 -10  now
: 438 1 -10  you
: 440 1 -10  do
: 442 2 -10  re
: 444 2 -12 gret
: 448 2 -12  it!
- 479
: 510 2 8 To
: 514 2 10  escape
: 518 2 10  the
: 522 2 10  world
: 526 2 10  I've
: 530 2 10  got
: 534 2 8  to
- 536
: 538 2 8 enjoy
: 542 2 8  that
: 546 2 8  sim
: 550 2 5 ple
: 554 10 5  dance!
- 564
: 566 2 5 And
: 570 2 10  it
: 574 2 10  seemed
: 578 2 10  that
: 582 2 10  every
: 586 2 10 thing
: 590 2 8  was
- 592
: 594 2 8 on
: 598 2 5  my
: 602 10 5  side!
- 622
: 638 2 10 She
: 642 2 10  seemed
: 646 2 10  sin
: 650 2 10 cere
: 654 2 8  like
: 658 2 8  it
: 662 2 8  was
: 666 2 8  love
- 668
: 670 2 8 and
: 674 2 8  true
: 678 2 5  ro
: 682 8 5 mance!
- 692
: 694 2 5 And
: 698 2 10  now
: 702 2 10  she's
: 706 2 10  out
: 710 2 8  to
: 714 2 8  get
: 718 4 10  me
- 722
: 724 1 8 But,
: 726 2 10  I
: 730 4 12  just
: 736 2 10  can't
: 740 1 8  take
: 742 2 10  it!
- 744
: 746 4 12 Just
: 752 2 10  can't
: 756 1 8  break
: 758 2 10  it!
- 760
: 762 4 8 Su
: 768 2 8 sie
: 772 2 7  got
: 776 2 5  your
: 780 5 7  num
: 786 6 5 ber
- 792
: 794 4 8 and Su
: 800 2 8 sie
: 804 2 7  ain't
: 808 2 5  your
: 812 6 8  friend!
- 824
: 826 4 8 Look
: 832 2 8  who
: 836 2 7  took
: 840 2 5  you
: 844 5 7  un
: 850 6 5 der
- 856
: 858 5 8 with se
: 864 2 8 ven
: 868 2 8  in
: 872 2 5 ches
: 876 6 5  in!
- 888
: 890 4 8 Blood
: 896 2 8  is
: 900 2 7  on
: 904 2 5  the
: 908 4 7  dance
: 914 6 5  floor!
- 920
: 922 4 8 Blood
: 928 2 8  is
: 932 2 7  on
: 936 2 5  the
: 940 6 8  knife!
- 952
: 954 4 8 Su
: 960 2 8 sie
: 964 2 7  got
: 968 2 5  your
: 972 4 7  num
: 978 6 5 ber
- 984
: 986 5 8 and Su
: 992 2 8 sie
: 996 2 8  says
: 1000 2 5  it's
: 1004 6 5  right!
- 1020
: 1024 2 -7 She
: 1028 2 -7  got
: 1032 2 -7  your
: 1036 4 -5  num
: 1042 2 -5 ber!
- 1053
: 1056 2 -7 How
: 1060 2 -7  does
: 1064 2 -7  it
: 1068 4 -5  feel?
- 1084
: 1088 2 -7 To
: 1092 2 -7  know
: 1096 2 -7  the
: 1100 4 -5  stran
: 1106 2 -5 ger
- 1117
: 1120 1 -7 is
: 1122 1 -7  a
: 1124 2 -7 bout
: 1128 2 -7  to
: 1132 6 -5  kill!
- 1148
: 1152 2 -7 She
: 1156 2 -7  got
: 1160 2 -7  your
: 1164 4 -5  ba
: 1170 4 -5 by!
- 1181
: 1184 1 -7 It
: 1186 1 -7  hap
: 1188 2 -7 pen
: 1192 2 -7 ed
: 1196 6 -5  fast!
- 1212
: 1216 2 -7 If
: 1220 2 -7  you
: 1224 2 -7  could
: 1228 4 -5  on
: 1234 4 -5 ly
- 1245
: 1248 1 -7 e
: 1250 1 -7 ra
: 1252 2 -7 se
: 1256 2 -7  the
: 1260 6 -5  past!
- 1272
: 1274 3 -9 E
: 1278 1 -9 very
: 1280 2 -9  night's
: 1284 2 -9  stance
- 1286
: 1288 2 -9 is
: 1292 1 -9  like
: 1294 2 -9  tak
: 1297 1 -9 in' a
: 1300 1 -9  chance!
- 1301
: 1302 1 -9 It's
: 1304 1 -9  not
: 1306 2 -10  a
: 1310 1 -10 bout
: 1312 1 -10  love
: 1314 1 -10  and
: 1316 2 -10  ro
: 1320 2 -10 mance
- 1322
: 1324 1 -10 and now
: 1326 1 -10  you're
: 1328 2 -10  gon
: 1332 2 -10 na
: 1336 1 -10  get
: 1338 4 -12  it!
- 1371
: 1402 3 -9 E
: 1406 1 -9 very hot
: 1408 2 -9  man
: 1412 2 -9  is
: 1416 2 -9  out
: 1420 2 -9  takin'
: 1424 2 -9  a
: 1428 1 -9  chance!
- 1429
: 1430 2 -9 It's not
: 1434 2 -10  about
: 1438 2 -10  love
: 1444 2 -10  and
: 1448 2 -10  romance
- 1450
: 1452 2 -10 and now
: 1456 2 -10  you do
: 1460 2 -10  re
: 1462 2 -10 gret
: 1466 6 -12  it!
- 1501
: 1534 2 8 To
: 1538 2 10  escape
: 1542 2 10  the
: 1546 2 10  world
: 1550 2 10  I've got
: 1554 2 10  to
- 1556
: 1558 2 8 en
: 1562 2 8 joy
: 1566 2 8  this
: 1570 2 8  sim
: 1574 2 5 ple
: 1578 10 5  dance!
- 1588
: 1590 2 5 And
: 1594 2 10  seemed
: 1598 2 10  that
: 1602 2 10  e
: 1606 2 10 very
: 1610 2 10 thing
- 1612
: 1614 2 8 was
: 1618 2 8  on
: 1622 2 5  my
: 1626 10 5  side!
- 1646
: 1662 2 10 It
: 1666 2 10  seemed
: 1670 2 10  sin
: 1674 2 10 cere
: 1678 2 8  will
: 1682 2 8  it
- 1684
: 1686 2 8 will
: 1690 2 8  love
: 1694 2 8  and
: 1698 2 8  true
: 1702 2 5  ro
: 1706 8 5 mance?
- 1716
: 1718 2 5 And
: 1722 2 10  now
: 1726 2 10  she's
: 1730 2 10  out
: 1734 2 8  to
: 1738 2 8  get
: 1742 4 10  me!
- 1746
: 1748 1 8 But
: 1750 2 10  I
: 1754 4 12  just
: 1760 2 10  can't
: 1764 1 8  take
: 1766 2 10  it!
- 1768
: 1770 4 12 Just
: 1776 2 10  can't
: 1780 1 8  break
: 1782 2 10  it!
- 1784
: 1786 4 8 Su
: 1792 2 8 sie
: 1796 2 7  got
: 1800 2 5  your
: 1804 6 7  num
: 1811 5 5 ber
- 1816
: 1818 4 8 and Su
: 1824 2 8 sie
: 1828 2 7  ain't
: 1832 2 5  your
: 1836 6 8  friend!
- 1848
: 1850 4 8 Look
: 1856 2 8  who
: 1860 2 7  took
: 1864 2 5  you
: 1868 4 7  un
: 1874 6 5 der
- 1880
: 1882 4 8 with se
: 1888 2 8 ven
: 1892 2 8  in
: 1896 2 5 ches
: 1900 6 5  in!
- 1912
: 1914 4 8 Blood
: 1920 2 8  is
: 1924 2 7  on
: 1928 2 5  the
: 1932 4 7  dance
: 1938 6 5  floor!
- 1944
: 1946 4 8 Blood
: 1952 2 8  is
: 1956 2 7  on
: 1960 2 5  the
: 1964 6 8  knife!
- 1976
: 1978 4 8 Su
: 1984 2 8 sie
: 1988 2 7  got
: 1992 2 5  your
: 1996 4 7  num
: 2002 6 5 ber!
- 2008
: 2010 5 8 Su
: 2016 2 8 sie
: 2020 2 8  says
: 2024 2 5  it's
: 2028 6 5  right!
- 2040
: 2042 5 8 Su
: 2048 2 8 sie
: 2052 2 7  got
: 2056 2 5  your
: 2060 5 7  num
: 2066 6 5 ber!
- 2072
: 2074 4 8 Su
: 2080 2 8 sie
: 2084 2 7  ain't
: 2088 2 5  your
: 2092 6 8  friend!
- 2104
: 2106 4 8 Look
: 2112 2 8  who
: 2116 2 7  took
: 2120 2 5  you
: 2124 5 7  un
: 2130 6 5 der!
- 2136
: 2138 5 8 with se
: 2144 2 8 ven
: 2148 2 8  in
: 2152 2 5 ches
: 2156 6 5  in!
- 2168
: 2170 4 8 Blood
: 2176 2 8  is
: 2180 2 7  on
: 2184 2 5  the
: 2188 4 7  dance
: 2194 6 5  floor!
- 2200
: 2202 4 8 Blood
: 2208 2 8  is
: 2212 2 7  on
: 2216 2 5  the
: 2220 6 8  knife!
- 2232
: 2234 4 8 Su
: 2240 2 8 sie
: 2244 2 7  got
: 2248 2 5  your
: 2252 5 7  num
: 2258 6 5 ber!
- 2264
: 2266 5 8 Su
: 2272 1 8 sie
: 2274 1 8  says
: 2276 2 5  it's
: 2280 4 8  ri
: 2285 5 5 ght!
- 2292
: 2294 2 10 It was
: 2298 1 8  blood
- 2299
: 2300 2 10 on the
: 2304 1 8  dance
: 2306 18 5  floor!
- 2334
: 2342 2 5 It
: 2346 1 8  was
: 2348 8 12  blood
- 2356
: 2358 3 10 o
: 2362 1 8 n
: 2364 2 10  the
: 2368 1 8  dance
: 2370 12 5  floor!
- 2392
: 2406 2 5 It
: 2410 2 8  was
: 2414 6 12  blood
- 2420
: 2422 2 10 on
: 2426 2 8  the
: 2430 2 12  dance
: 2434 12 12  floor!
- 2456
: 2470 2 5 It
: 2474 2 8  was
: 2478 10 12  blood
: 2490 3 10  o
: 2494 1 8 n
: 2496 2 10  the
: 2500 2 8  dan
: 2503 3 5 ce
: 2508 6 8  floor!
- 2514
: 2516 1 8 And
: 2518 2 10  I
: 2522 4 12  just
: 2528 2 10  can't
: 2532 1 8  take
: 2534 2 10  it!
- 2536
: 2538 4 10 The
: 2544 2 12  girl
: 2548 1 10  won't
: 2550 2 8  break
: 2554 2 10  it!
E