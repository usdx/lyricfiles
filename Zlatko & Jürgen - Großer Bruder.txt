#TITLE:Großer Bruder
#ARTIST:Zlatko & Jürgen
#MP3:Zlatko & Jürgen - Großer Bruder.mp3
#COVER:Zlatko & Jürgen - Großer Bruder [CO].jpg
#BACKGROUND:Zlatko & Jürgen - Großer Bruder [BG].jpg
#BPM:274
#GAP:2150
#ENCODING:UTF8
#START:9
: 265 2 4 Wir
: 269 4 7  sind
: 277 4 7  schon
: 285 4 7  ech
: 293 2 4 te
: 297 3 7  Freun
: 305 3 7 de
- 310
: 328 2 7  Wir
: 333 5 9  zwei
: 341 3 9  sind
: 349 5 9  ei
: 357 3 7 ne
: 361 4 7  Macht
- 370
: 392 3 4 Wir
: 396 4 7  hal
: 404 4 7 ten
: 412 4 7  voll
: 420 2 7  zu
: 424 6 7 sam
: 432 3 7 men
- 440
: 456 2 7  auch
: 460 4 9  wenn
: 468 3 9  es
: 476 5 9  manch
: 484 2 9 mal
* 488 3 11  kracht
- 496
: 524 4 9 Kei
: 531 4 9 ner
: 539 5 11  kann
: 547 5 12  uns
: 555 4 7  wirk
: 563 3 4 lich
: 567 3 4  was
- 573
: 579 2 4  Wir
: 583 2 7  ver
: 587 4 12 ste
: 595 5 12 hen
: 603 4 11  uns
: 611 2 9  to
: 615 6 11 tal!
- 622
: 650 4 9  Wir
: 658 5 9  sind
: 666 5 11  schon
: 674 5 12  ein
: 682 5 7  star
: 690 3 4 kes
: 694 4 4  Team
- 700
: 702 2 4  Das
: 706 2 4  gibts
: 710 2 7  im
: 714 6 12  Le
: 722 3 12 ben
: 730 3 11  nur
: 734 4 14  ein
: 742 5 14 mal
- 750
: 762 4 7 Bist
: 770 4 7  mein
: 778 5 12  gro
: 786 3 12 ßer
: 790 4 11  Bru
: 798 3 11 der
: 802 3 9  du
: 806 3 9  bist
: 810 3 7  im
: 814 4 7 mer
: 822 6 4  da
- 830
: 842 5 12 Gro
: 849 2 12 ßer
: 853 4 11  Bru
: 861 3 11 der
: 865 3 9  und
: 869 3 9  ein
: 873 3 7  Freund
: 880 3 7  fürs
: 885 4 9  Le
: 893 4 11 ben
- 900
: 905 4 12 Gro
: 913 3 12 ßer
: 917 5 11  Bru
: 925 3 11 der
: 929 3 9  du
: 933 2 9  bist
: 937 2 7  im
: 941 5 7 mer
: 949 6 4  da
- 957
: 968 4 12 Gro
: 976 3 12 ßer
: 980 5 11  Bru
: 988 3 11 der
: 992 3 9  kannst
: 996 3 9  mir
: 1000 3 7  al
: 1004 5 7 les
: 1012 5 9  ge
: 1020 3 11 ben
- 1026
: 1032 5 16 Du
: 1040 6 16  bist
: 1048 5 14  im
: 1056 4 12 mer
: 1063 2 14  für
: 1067 4 11  mich
: 1075 8 7  da
- 1085
: 1091 3 7  E
: 1096 4 12 gal
: 1104 5 12  was
: 1112 4 11  auch
: 1120 2 9  pas
: 1124 7 11 siert
- 1139
: 1151 3 7 Bist
: 1155 3 7  mein
: 1159 4 9  gro
: 1166 4 9 ßer
: 1171 4 11  Bru
: 1178 3 11 der
: 1183 3 12  und
: 1190 4 11  du
: 1199 3 11  bist
: 1203 4 12  im
: 1210 3 12 mer
: 1215 5 14  bei
: 1222 6 12  mir
- 1243
: 1409 3 4 Wir
: 1413 4 7  zwei
: 1421 4 7  sind
: 1429 4 7  im
: 1437 2 4 mer
: 1441 5 7  ehr
: 1449 3 7 lich
- 1458
: 1473 3 7  Wir
: 1477 4 9  lü
: 1485 4 9 gen
: 1493 5 9  uns
: 1501 3 7  nie
: 1505 4 7  an
- 1514
: 1536 3 4 Wir
: 1540 4 7  können
: 1548 4 7  uns
: 1556 4 7  al
: 1564 3 4 les
: 1568 4 7  sa
: 1576 4 7 gen
- 1585
: 1596 3 5  Gera
: 1600 2 7 de
: 1604 4 9 aus
: 1612 4 9  von
: 1620 4 9  Mann
: 1628 3 12  zu
: 1632 4 11  Mann
- 1641
: 1667 4 9 Kei
: 1675 4 9 ner
: 1683 5 11  kann
: 1691 4 12  uns
: 1699 4 7  wirk
: 1707 3 4 lich
: 1711 4 4  was
- 1718
: 1722 3 4  Wir
: 1727 2 7  ver
: 1731 4 12 ste
: 1739 5 12 hen
: 1747 4 11  uns
: 1755 2 9  to
: 1759 6 11 tal
- 1770
: 1794 4 9 Wir
: 1802 4 9  sind
: 1810 4 11  schon
: 1818 4 12  ein
: 1826 5 7  star
: 1834 3 4 kes
: 1838 4 4  Team
- 1844
: 1846 3 4  Das
: 1850 3 4  gibt's
: 1854 2 7  im
: 1858 4 12  Le
: 1866 4 12 ben
: 1874 2 11  nur
: 1878 4 14  ein
: 1886 6 14 mal
- 1900
: 1905 4 7 Bist
: 1913 5 7  mein
: 1921 4 12  gro
: 1929 3 12 ßer
: 1933 5 11  Bru
: 1941 3 11 der
: 1945 2 9  du
: 1949 3 9  bist
: 1953 3 7  im
: 1957 3 7 mer
: 1965 5 4  da
- 1976
: 1984 5 12 Gro
: 1992 2 12 ßer
: 1997 5 11  Bru
: 2005 2 11 der
: 2009 3 9  und
: 2013 2 9  ein
: 2017 5 7  Freund
: 2025 3 7  fürs
: 2029 5 9  Le
: 2037 3 11 ben
- 2044
: 2048 4 12 Gro
: 2056 2 12 ßer
: 2060 4 11  Bru
: 2068 3 11 der
: 2072 3 9  du
: 2076 3 9  bist
: 2080 3 7  im
: 2084 3 7 mer
: 2092 4 4  da
- 2103
: 2111 5 12 Gro
: 2120 2 12 ßer
: 2124 5 11  Bru
: 2132 3 11 der
: 2136 3 9  kann
: 2140 3 9  mir
: 2144 3 7  al
: 2148 5 7 les
: 2156 6 9  ge
: 2164 3 11 ben
- 2170
: 2176 5 16 Du
: 2183 5 16  bist
: 2191 6 14  im
: 2199 4 12 mer
: 2207 2 14  für
: 2211 5 11  mich
: 2219 8 7  da
- 2230
: 2234 4 9  E
: 2239 5 12 gal
: 2247 5 12  was
: 2255 4 11  auch
: 2263 2 9  pas
: 2267 5 11 siert
- 2275
: 2295 2 7 Bist
: 2299 2 7  mein
: 2303 4 9  gro
: 2311 2 9 ßer
: 2315 5 11  Bru
: 2323 2 11 der
- 2326
: 2328 3 12  und
: 2334 4 11  du
: 2342 2 11  bist
: 2346 4 12  im
: 2354 3 12 mer
* 2358 6 14  bei
: 2366 10 12  mir
E