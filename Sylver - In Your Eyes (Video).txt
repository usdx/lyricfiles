#TITLE:In Your Eyes (Video)
#ARTIST:Sylver
#MP3:Sylver - In Your Eyes (Video).mp3
#VIDEO:Sylver - In Your Eyes (Video).mp4
#COVER:Sylver - In Your Eyes (Video).jpg
#BPM:92.9
#GAP:21400
#ENCODING:UTF8
#LANGUAGE:Englisch
#GENRE:Pop
#EDITION:UltraStar
: 0 1 5 I 
: 1 1 4 can 
: 2 1 5 still 
: 3 1 4 re
: 4 2 5 mem
: 6 1 4 ber, 
: 7 1 5 long 
: 9 1 7 a
: 11 3 4 go 
- 14
: 16 1 4 Li
: 17 1 2 ving 
: 18 1 4 in 
: 19 1 2 the 
: 20 2 4 fast 
: 22 1 2 lane, 
: 23 1 4 ne
: 25 1 4 ver 
: 27 3 5 slow 
- 30
: 35 1 4 And 
: 36 1 5 noth
: 37 1 4 ing 
: 38 1 5 that 
: 39 1 4 could 
: 40 2 5 come 
: 42 1 7 bet
: 43 3 4 ween 
- 46
: 48 1 0 me 
: 49 3 2 and 
: 53 2 0 you 
- 55
: 64 1 5 I 
: 65 1 4 still 
: 66 1 5 hear 
: 67 1 4 the 
: 68 2 5 words 
: 70 1 4 I 
: 71 1 5 thought 
: 73 1 7 were 
: 75 3 4 cheap 
- 78
: 80 1 4 You 
: 81 1 2 should 
: 82 1 4 al
: 83 1 2 ways 
: 84 2 4 look 
: 86 1 2 be
: 87 1 4 fore 
: 89 1 4 you 
: 91 3 5 leap 
- 94
: 98 1 -5 And 
: 99 1 -5 I 
: 100 1 5 ne
: 101 1 4 ver 
: 102 1 5 re
: 103 1 4 a
: 104 2 5 lised 
: 106 2 7 those 
: 108 2 4 words 
: 110 1 0 were 
: 111 1 0 oh 
: 113 2 2 so 
: 117 3 0 true 
- 120
: 131 1 0 I 
: 132 1 2 hope 
: 134 1 2 that 
: 135 1 0 you 
: 137 1 2 can 
: 139 2 4 hear 
: 141 2 4 me 
- 143
: 146 1 4 Though 
: 147 1 2 you're 
: 148 1 4 li
: 149 1 2 ving 
: 150 1 4 in 
: 151 1 2 a
: 152 1 4 no
: 153 1 4 ther 
: 156 3 5 world 
- 159
: 160 2 5 Thro
: 162 1 4 wing 
: 163 1 5 sha
: 165 1 4 dows 
: 167 2 5 u
: 169 1 7 pon 
: 171 9 7 earth 
- 180
: 184 2 4 In 
: 186 2 7 your 
: 188 4 9 eyes, 
- 192
: 200 1 11 I 
: 201 1 12 could 
: 203 2 11 drown 
: 205 1 7 and 
: 207 1 7 still 
: 209 1 9 su
: 211 2 7 rv
: 213 1 9 ive 
- 214
: 216 2 4 In 
: 218 2 4 your 
: 220 4 5 eyes, 
- 224
: 226 1 2 I 
: 227 1 2 could 
: 228 1 5 see 
: 229 1 5 how 
: 230 1 4 to 
: 231 1 5 live 
: 233 1 4 my 
: 235 2 2 li
: 237 1 4 fe 
- 238
: 248 2 4 But 
: 251 1 7 if 
: 253 2 9 on
: 256 2 9 ly 
: 258 1 7 I 
: 259 2 9 knew 
- 261
: 266 1 4 That 
: 269 2 11 chan
: 271 1 11 ces 
: 273 1 12 were 
: 275 2 11 few 
- 277
: 280 2 4 May
: 283 1 4 be 
: 285 3 5 I 
- 288
: 290 1 5 would 
: 292 2 5 still 
: 295 2 5 be 
: 298 1 4 with 
: 299 3 2 yo
: 303 2 4 u 
- 305
: 320 2 5 Se
: 322 1 5 veral 
: 323 2 5 thoughts 
: 325 2 4 I 
: 327 1 5 can't 
: 329 1 7 de
: 331 3 4 fine 
- 334
: 336 1 4 Li
: 337 1 2 ving 
: 338 1 4 on 
: 339 1 2 a 
: 340 2 4 dream, 
: 342 1 2 seems 
: 343 1 4 like 
: 345 1 4 a 
: 347 3 5 crime 
- 350
: 355 1 4 But 
: 356 1 5 e
: 357 1 4 very 
: 358 1 5 now 
: 359 1 4 and 
: 360 2 5 then 
: 362 2 7 it's 
: 364 3 4 hard 
- 367
: 368 1 0 to 
: 369 3 2 ig
: 373 2 0 nore 
- 375
: 383 1 4 'Cause 
: 384 1 5 on
: 385 1 4 ly 
: 386 1 5 in 
: 387 1 4 my 
: 388 2 5 dreams 
: 390 1 4 I 
: 391 1 5 find 
: 393 1 7 a 
: 395 2 4 clue 
- 397
: 398 1 4 when 
: 399 1 2 i 
: 400 1 4 try 
: 401 1 2 to 
: 402 1 4 build 
: 403 1 2 a 
: 404 2 4 me
: 406 1 2 mo
: 407 1 4 ry 
: 409 1 4 of 
: 411 3 5 you 
- 414
: 418 1 -5 I 
: 419 1 -5 know 
: 420 1 5 our 
: 421 1 4 love 
: 422 1 5 will 
: 423 1 4 be 
: 424 2 5 much 
: 426 2 7 stron
: 428 2 4 ger 
: 430 2 0 than 
: 433 2 2 be
: 437 3 0 fore 
- 440
: 451 1 0 I 
: 452 1 2 hope 
: 454 1 2 that 
: 455 1 0 you 
: 457 1 2 can 
: 459 2 4 hear 
: 461 2 4 me 
- 463
: 466 1 4 Though 
: 467 1 2 you're 
: 468 1 4 li
: 469 1 2 ving 
: 470 1 4 in 
: 471 1 2 a
: 472 1 4 no
: 473 1 4 ther 
: 476 3 5 world 
- 479
: 480 2 5 Thro
: 482 1 4 wing 
: 483 1 5 sha
: 485 1 4 dows 
: 487 2 5 u
: 489 1 7 pon 
: 491 9 7 earth 
- 500
: 504 2 4 In 
: 506 2 7 your 
: 508 4 9 eyes, 
- 512
: 520 1 11 I 
: 521 1 12 could 
: 523 2 11 drown 
: 525 1 7 and 
: 527 1 7 still 
: 529 1 9 su
: 531 2 7 rv
: 533 1 9 ive 
- 534
: 536 2 4 In 
: 538 2 4 your 
: 540 4 5 eyes, 
- 544
: 546 1 2 I 
: 547 1 2 could 
: 548 1 5 see 
: 549 1 5 how 
: 550 1 4 to 
: 551 1 5 live 
: 553 1 4 my 
: 555 2 2 li
: 557 1 4 fe 
- 558
: 568 2 4 But 
: 571 1 7 if 
: 573 2 9 on
: 576 2 9 ly 
: 578 1 7 I 
: 579 2 9 knew 
- 581
: 586 1 4 That 
: 589 2 11 chan
: 591 1 11 ces 
: 593 1 12 were 
: 595 2 11 few 
- 597
: 600 2 4 May
: 603 1 4 be 
: 605 3 5 I 
- 608
: 610 1 5 would 
: 612 2 5 still 
: 615 2 5 be 
: 618 1 4 with 
: 619 3 2 yo
: 623 2 4 u 
- 625
: 707 1 0 I 
: 708 1 2 hope 
: 710 1 2 that 
: 711 1 0 you 
: 713 1 2 can 
: 715 2 4 hear 
: 717 2 4 me 
- 719
: 722 1 4 Though 
: 723 1 2 you're 
: 724 1 4 li
: 725 1 2 ving 
: 726 1 4 in 
: 727 1 2 a
: 728 1 4 no
: 729 1 4 ther 
: 732 3 5 world 
- 735
: 736 2 5 Thro
: 738 1 4 wing 
: 739 1 5 sha
: 741 1 4 dows 
: 743 2 5 u
: 745 1 7 pon 
: 747 9 7 earth 
- 756
: 760 2 4 In 
: 762 2 7 your 
: 764 4 9 eyes, 
- 768
: 776 1 11 I 
: 777 1 12 could 
: 779 2 11 drown 
: 781 1 7 and 
: 783 1 7 still 
: 785 1 9 su
: 787 2 7 rv
: 789 1 9 ive 
- 790
: 792 2 4 In 
: 794 2 4 your 
: 796 4 5 eyes, 
- 800
: 802 1 2 I 
: 803 1 2 could 
: 804 1 5 see 
: 805 1 5 how 
: 806 1 4 to 
: 807 1 5 live 
: 809 1 4 my 
: 811 2 2 li
: 813 1 4 fe 
- 814
: 824 2 4 But 
: 827 1 7 if 
: 829 2 9 on
: 832 2 9 ly 
: 834 1 7 I 
: 835 2 9 knew 
- 837
: 842 1 4 That 
: 845 2 11 chan
: 847 1 11 ces 
: 849 1 12 were 
: 851 2 11 few 
- 853
: 856 2 4 May
: 859 1 4 be 
: 861 3 5 I 
- 864
: 866 1 5 would 
: 868 2 5 still 
: 871 2 5 be 
: 873 1 4 with 
: 875 3 2 yo
: 879 1 4 u 
- 880
: 881 2 4 Oh 
: 883 2 2 Ba
: 886 1 0 by 
- 887
: 888 2 4 In 
: 890 1 7 your 
: 892 4 9 eyes, 
- 896
: 904 1 11 I 
: 905 1 12 could 
: 907 2 11 drown 
: 909 1 7 and 
: 911 1 7 still 
: 913 1 9 su
: 915 2 7 rv
: 917 1 9 ive 
- 918
: 920 2 4 In 
: 922 2 4 your 
: 924 4 5 eyes, 
- 928
: 930 1 2 I 
: 931 1 2 could 
: 932 1 5 see 
: 933 1 5 how 
: 934 1 4 to 
: 935 1 5 live 
: 937 1 4 my 
: 939 2 2 li
: 941 1 4 fe 
- 942
: 952 2 4 But 
: 955 1 7 if 
: 957 2 9 on
: 960 2 9 ly 
: 962 1 7 I 
: 963 2 9 knew 
- 965
: 970 1 4 That 
: 973 2 11 chan
: 975 1 11 ces 
: 977 1 12 were 
: 979 2 11 few 
- 981
: 984 2 4 May
: 987 1 4 be 
: 989 3 5 I 
- 992
: 994 1 5 would 
: 996 2 5 still 
: 999 2 5 be 
: 1002 1 4 with 
: 1003 3 2 yo
: 1007 1 4 u 
- 1008
: 1009 1 4 Oh 
: 1011 2 2 Ba
: 1014 3 0 by 
E