#TITLE:Tom Dooley
#ARTIST:Kingston Trio
#EDITION:UltraStar 50s
#MP3:Kingston Trio - Tom Dooley.mp3
#COVER:Kingston Trio - Tom Dooley [CO].jpg
#BACKGROUND:Kingston Trio - Tom Dooley [BG].jpg
#BPM:118,2
#GAP:26980
: 36 2 59 Hang
: 38 4 59  down
: 42 1 59  your
: 43 4 61  head,
: 47 5 64  Tom
: 55 4 68  Doo
: 59 7 68 ley.
- 67
: 67 2 59 Hang
: 69 3 59  down
: 72 2 59  your
: 74 4 61  head
: 78 3 64  and
: 82 11 66  cry.
- 94
: 96 3 59 Hang
: 99 3 59  down
: 102 2 59  your
: 104 4 61  head,
: 108 6 64  Tom
: 116 4 66  Doo
: 120 6 66 ley.
- 127
: 127 2 66 Poor
: 129 3 66  boy,
: 132 2 68  you're
: 134 4 64  bound
: 138 2 61  to
: 140 12 64  die.
- 153
: 155 2 59 I
: 157 2 59  met
: 159 4 59  her
: 163 5 61  on
: 168 2 64  the
: 170 4 68  moun
: 174 10 68 tain,
- 185
: 187 3 59 there
: 190 5 59  I
: 195 2 61  took
: 197 4 64  her
: 201 14 66  life.
- 216
: 217 3 59 Met
: 220 4 59  her
: 224 4 61  on
: 228 3 64  the
: 231 4 66  moun
: 235 10 66 tain,
- 246
: 247 3 66 stabbed
: 250 5 68  her
: 255 2 64  with
: 257 4 61  my
: 261 15 64  knife.
- 277
: 278 2 59 Hang
: 280 4 59  down
: 284 1 59  your
: 285 4 61  head,
: 289 5 64  Tom
: 298 4 68  Doo
: 302 6 68 ley.
- 309
: 309 2 59 Hang
: 311 4 59  down
: 315 1 59  your
: 316 4 61  head
: 320 4 64  and
: 324 9 66  cry.
- 335
: 339 3 59 Hang
: 342 4 59  down
: 346 1 59  your
: 347 4 61  head,
: 351 5 64  Tom
: 359 4 66  Doo
: 363 5 66 ley.
- 369
: 369 3 66 Poor
: 372 4 66  boy,
: 376 1 68  you're
: 377 4 64  bound
: 381 2 61  to
: 383 12 64  die.
- 397
: 402 4 59 This
: 406 5 61  time
: 411 3 64  to
: 414 4 68 mor
: 418 10 68 row,
- 429
: 430 2 59 reck
: 432 5 59 on
: 437 4 61  where
: 441 3 64  I'll
: 444 15 66  be.
- 460
: 461 4 59 Hadn't
: 465 2 59  it
: 467 4 61  been
: 471 4 64  for
: 475 3 66  Gray
: 478 8 66 son,
- 487
: 488 2 66 I'd
: 490 2 66 've
: 492 2 66  been
: 494 5 66  in
: 499 2 64  Ten
: 501 4 61 nes
: 505 14 64 see.
- 520
: 522 2 59 Hang
: 524 4 59  down
: 528 2 59  your
: 530 4 61  head,
: 534 4 64  Tom
: 541 4 68  Doo
: 545 6 68 ley.
- 552
: 552 2 59 Hang
: 554 3 59  down
: 557 2 59  your
: 559 4 61  head
: 563 4 64  and
: 567 14 66  cry.
- 582
: 582 2 59 Hang
: 584 4 59  down
: 588 2 59  your
: 590 4 61  head,
: 594 5 64  Tom
: 601 4 66  Doo
: 605 7 66 ley.
- 612
: 612 2 66 Poor
: 614 3 66  boy,
: 617 2 68  you're
: 619 4 64  bound
: 623 2 61  to
: 625 7 64  die.
- 634
: 641 2 59 Hang
: 643 4 59  down
: 647 2 59  your
: 649 4 61  head,
: 653 4 64  Tom
: 660 4 68  Doo
: 664 7 68 ley.
- 671
: 671 2 59 Hang
: 673 4 59  down
: 677 2 59  your
: 679 3 61  head
: 682 4 64  and
: 686 12 66  cry.
- 699
: 701 2 59 Hang
: 703 4 59  down
: 707 1 59  your
: 708 4 61  head,
: 712 4 64  Tom
: 720 4 66  Doo
: 724 6 66 ley.
- 731
: 731 2 66 Poor
: 733 3 66  boy,
: 736 2 68  you're
: 738 4 64  bound
: 742 2 61  to
: 744 8 64  die.
- 754
: 763 3 59 This
: 766 6 61  time
: 772 2 64  to
: 774 3 68 mor
: 777 11 68 row,
- 789
: 790 2 59 reck
: 792 4 59 on
: 796 4 61  where
: 800 4 64  I'll
: 804 12 66  be.
- 818
: 820 3 59 Down
: 823 3 59  in
: 826 2 59  some
: 828 4 61  lone
: 832 2 64 some
: 834 4 66  val
: 838 10 66 ley,
- 849
: 851 4 66 hangin'
: 855 1 66  from
: 856 1 68  a
: 857 3 64  white
: 860 5 61  oak
: 865 13 64  tree.
- 879
: 881 2 59 Hang
: 883 4 59  down
: 887 1 59  your
: 888 4 61  head,
: 892 4 64  Tom
: 900 4 68  Doo
: 904 7 68 ley.
- 911
: 911 2 59 Hang
: 913 4 59  down
: 917 1 59  your
: 918 4 61  head
: 922 4 64  and
: 926 13 66  cry.
- 940
: 941 2 59 Hang
: 943 4 59  down
: 947 1 59  your
: 948 4 61  head,
: 952 5 64  Tom
: 960 4 66  Doo
: 964 6 66 ley.
- 971
: 971 2 66 Poor
: 973 4 66  boy,
: 977 1 68  you're
: 978 4 64  bound
: 982 2 61  to
: 984 14 64  die.
- 999
: 1001 2 59 Hang
: 1003 3 59  down
: 1006 1 59  your
: 1007 4 61  head,
: 1011 4 64  Tom
: 1019 4 68  Doo
: 1023 6 68 ley.
- 1030
: 1030 2 59 Hang
: 1032 3 59  down
: 1035 2 59  your
: 1037 4 61  head
: 1041 4 64  and
: 1045 13 66  cry.
- 1059
: 1059 2 59 Hang
: 1061 3 59  down
: 1064 2 59  your
: 1066 4 61  head,
: 1070 4 64  Tom
: 1077 4 66  Doo
: 1081 6 66 ley.
- 1088
: 1088 2 66 Poor
: 1090 4 66  boy,
: 1094 2 68  you're
: 1096 3 64  bound
: 1099 2 61  to
: 1101 14 64  die.
- 1116
: 1117 2 66 Poor
: 1119 4 66  boy,
: 1123 2 68  you're
: 1125 4 64  bound
: 1129 2 61  to
: 1131 14 64  die.
- 1146
: 1147 2 66 Poor
: 1149 4 66  boy,
: 1153 2 68  you're
: 1155 4 64  bound
: 1159 2 61  to
: 1161 14 64  die.
- 1176
: 1178 2 66 Poor
: 1180 4 66  boy,
: 1184 2 68  you're
: 1186 3 64  bound
: 1189 2 61  to
: 1197 8 64  die.
E
