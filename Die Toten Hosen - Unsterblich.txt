#TITLE:Unsterblich
#ARTIST:Die Toten Hosen
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
#MP3:Die Toten Hosen - Unsterblich.mp3
#COVER:Die Toten Hosen - Unsterblich [CO].jpg
#BACKGROUND:Die Toten Hosen - Unsterblich [BG].jpg
#BPM:296
#GAP:11800
: 0 7 0 Ich
: 8 7 0  will
: 16 7 3  mit
: 24 6 7  dir
: 31 7 7  für
: 39 5 5  im
: 44 5 3 mer
: 51 8 5  le
: 59 8 7 ben,
- 69
: 159 3 7 We
: 162 5 7 nigs
: 167 7 5 tens
: 176 5 3  in
: 184 6 5  die
: 192 6 7 ser
: 201 7 3  ei
: 209 6 2 nen
: 217 10 0  Nacht.
- 229
: 290 6 7 Lass
: 298 6 7  uns
: 306 6 7  jetzt
: 314 7 10  bei
: 323 4 10 de
: 330 7 7  kei
: 338 6 10 ne
: 346 16 12  Fra
: 362 5 8 gen
: 369 6 8  stel
: 375 7 5 len,
- 384
: 418 7 7 Weil
: 425 6 5  kei
: 431 6 3 ne
: 439 6 5  Ant
: 446 7 7 wort
: 458 4 3  für
: 463 7 2  uns
: 471 11 0  passt.
- 484
: 550 5 7 Mit
: 557 6 12  dir
: 565 6 14  hab
: 573 6 15  ich
: 581 6 14  die
: 588 6 12 ses
: 596 6 14  Ge
* 604 12 7 fühl,
- 618
: 678 6 7 Dass
: 686 6 12  wir
: 694 6 14  heut
: 702 12 15  Nacht
: 719 12 12  un
: 734 14 14 sterb
: 750 13 10 lich
: 766 13 12  sind.
- 781
: 808 6 7 E
: 815 7 12 gal,
: 824 7 14  was
: 833 6 15  uns
: 841 6 14  jetzt
: 848 7 12  noch
: 856 6 14  ge
: 864 14 7 schieht,
- 880
: 938 5 7 Ich
: 945 6 12  weiß,
: 953 7 14  dass
: 961 9 15  wir
: 977 11 12  un
: 993 14 14 sterb
: 1010 13 10 lich
: 1026 13 12  sind.
- 1041
: 1067 5 7 Wir
: 1074 7 5  könn
: 1082 7 3 ten
: 1091 6 5  auf
: 1099 5 7  'ner
: 1106 5 3  vol
: 1111 7 2 len
: 1123 7 3  Fahr
: 1131 12 0 bahn
: 1147 13 0  stehn,
- 1162
: 1196 7 7 Auf
: 1204 5 5  ei
: 1209 5 3 nem
: 1217 6 5  Dach
: 1224 9 7 first
: 1236 4 3  ba
: 1240 5 2 lan
: 1248 4 3 ci
: 1253 4 0 er'n.
- 1259
: 1302 5 8 Un
: 1308 6 8 sere
: 1317 6 7  Au
: 1324 7 7 gen
: 1332 5 7  wä
: 1337 5 7 ren
: 1344 8 10  zu
- 1354
: 1365 6 7  und
: 1373 5 10  wir
: 1380 6 12  zähl
: 1388 7 12 ten
: 1398 5 15  bis
: 1405 15 12  zehn,
- 1422
: 1453 6 7 Es
: 1461 4 5  wür
: 1465 4 5 de
: 1469 6 3  uns
: 1477 6 5  trotz
: 1485 6 7 dem
: 1493 6 3  nichts
: 1501 5 2  pas
: 1506 5 3 sie
: 1511 5 0 ren.
- 1518
: 1583 5 7 Denn
: 1590 7 12  mit
: 1599 6 14  dir
: 1607 6 15  hab
: 1615 6 14  ich
: 1623 7 12  das
: 1632 5 14  Ge
: 1639 14 7 fühl,
- 1655
: 1712 6 7 Dass
: 1720 6 12  wir
: 1729 6 14  heut
: 1737 12 15  Nacht
* 1753 10 12  un
* 1768 14 14 sterb
* 1785 12 10 lich
: 1801 14 12  sind.
- 1817
: 1843 6 7 E
: 1850 7 12 gal,
: 1859 6 14  was
: 1867 6 15  uns
: 1875 5 14  jetzt
: 1882 7 12  noch
: 1891 5 14  ge
: 1898 14 7 schieht,
- 1914
: 1972 7 7 Ich
: 1980 6 12  weiß,
: 1988 6 14  dass
: 1996 10 15  wir
* 2013 11 12  un
* 2029 12 14 sterb
* 2045 13 10 lich
: 2062 13 12  sind.
- 2077
: 2620 5 7 Wir
: 2627 4 5  ha
: 2632 4 5 ben
: 2637 5 3  uns
: 2644 7 5  ge
: 2652 6 7 gen
: 2659 6 3 sei
: 2666 8 2 tig
: 2675 10 3  leicht
: 2690 6 0  ge
: 2699 10 0 macht.
- 2711
: 2723 5 8 Sit
: 2730 5 8 zen
: 2740 11 7  auf
: 2754 11 7  'ner
: 2767 5 10  Wol
: 2774 5 10 ke
: 2795 5 10  und
: 2802 6 12  stür
: 2809 7 12 zen
: 2819 8 15  nie
: 2828 10 12  ab.
- 2840
: 2875 6 7 Hier
: 2883 8 5  geht's
: 2892 6 3  uns
: 2900 6 5  gut,
- 2907
: 2908 5 7  denn
: 2915 6 3  wir
: 2923 7 2  sind
: 2932 7 3  auf
: 2941 10 0  der
: 2954 11 0  Flucht,
- 2967
: 2980 6 7 bis
: 2988 4 7  die
: 2995 4 7  Son
: 3000 8 7 ne
- 3010
: 3012 6 7  uns
: 3020 5 7  am
: 3027 7 10  Mor
: 3036 6 10 gen
: 3044 6 7  wie
: 3052 5 10 der
: 3059 8 12  zu
: 3068 14 15 rück
: 3085 23 12 holt.
- 3110
: 3132 4 7 Ich
: 3136 4 7  hab
: 3141 6 12  nur
: 3149 6 14  mit
: 3157 6 15  dir
: 3165 5 14  die
: 3172 6 12 ses
: 3180 6 14  Ge
* 3188 12 7 fühl,
- 3202
: 3260 6 7 Dass
: 3268 5 12  wir
: 3275 6 14  heut
: 3283 10 15  Nacht
: 3300 11 12  un
: 3315 12 14 sterb
: 3331 11 10 lich
: 3345 14 12  sind.
- 3361
: 3386 6 7 Ich
: 3394 7 12  weiß,
: 3403 5 14  es
: 3410 6 15  kann
: 3418 6 14  uns
: 3426 6 12  nichts
: 3434 5 14  ge
: 3441 11 7 schehen,
- 3454
: 3492 9 7 Weil
: 3505 15 12  wir
: 3523 12 14  un
: 3538 12 15 sterb
: 3554 11 12 lich
: 3569 9 14  sind,
* 3587 11 15  un
* 3601 12 15 sterb
* 3616 11 14 lich
: 3630 7 14  si
: 3638 24 12 nd.
- 3664
: 3712 12 15 Un
: 3728 10 15 sterb
: 3742 12 14 lich
: 3757 6 14  si
: 3764 25 12 nd.
- 3791
: 3839 12 15 Un
* 3854 12 17 sterb
: 3869 13 15 lich
: 3884 26 12  sind.
E
