#TITLE:Broken Arrows
#ARTIST:Avicii
#MP3:Avicii - Broken Arrows.mp3
#VIDEO:Avicii - Broken Arrows.mp4
#COVER:Avicii - Broken Arrows.jpg
#BPM:232
#GAP:46910
#ENCODING:UTF8
#LANGUAGE:English
: 0 2 7 You
: 4 2 11  stripped
: 8 1 11  your
: 10 4 12  love
: 16 2 11  down
: 20 2 9  to
: 24 1 7  the
* 26 6 7  wire
- 34
: 60 2 4 Fi
: 64 2 7 re
: 68 2 7  shy
: 72 2 9  and
: 76 2 12  cold
: 80 1 4  a
: 82 2 4 lone
: 86 4 4  out
: 92 6 2 side
- 100
: 128 2 7 You
: 132 1 9  stripped
: 134 2 11  it
: 138 4 12  right
: 144 2 11  down
: 148 2 9  to
: 152 1 7  the
: 154 6 7  wire
- 162
: 186 1 4 But
: 188 2 4  I
: 192 2 7  see
: 196 2 7  you
* 200 1 7  be
* 202 4 11 hind
: 208 2 4  those
: 212 4 4  tired
: 218 1 4  eye
: 220 6 2 ~s
- 228
: 240 2 11 Now
: 244 2 9  as
: 248 2 7  you
: 252 4 7  wade
: 260 2 7  through
: 264 2 4  the
: 268 6 7  sha
: 276 4 7 dows
- 281
* 282 1 7 That
: 284 1 7  live
: 286 1 11  in
: 288 6 11  your
: 296 4 9  heart
- 301
: 302 2 -1 You'll
: 306 1 -1  fi
: 308 2 -3 ~nd
: 312 3 -5  the
: 321 2 -1  li
: 324 2 -3 ~ght
: 328 3 -5  that
: 336 6 -5  leads
: 344 1 -8  ho
: 346 4 -10 ~me
- 352
: 368 1 -3 'Cause
: 370 4 -3  I
: 376 2 -5  see
: 380 3 -5  you
: 388 6 -5  for
: 396 6 -5  you
- 403
: 404 3 -5 And
: 410 1 -5  your
: 412 1 -5  beau
: 414 1 -5 ti
: 416 6 -1 ful
* 424 6 -3  scars
- 431
: 432 1 6 So
: 434 1 11  ta
: 436 2 9 ~ke
: 440 3 7  my
: 451 1 11  ha
: 453 1 9 ~
: 455 3 7 ~nd,
: 460 4 7  don't
: 466 4 7  let
: 472 1 4  go
: 474 3 2 ~
- 479
: 500 2 12 'Cause
: 504 2 14  it's
: 508 16 19  not
: 528 1 19  too
: 530 4 14  late,
: 536 3 11  it's
- 541
: 548 10 14 Not
* 560 1 14  too
: 562 4 9  late,
: 568 3 7  I
- 573
: 576 2 9 I
: 580 2 11  see
: 584 2 11  the
: 588 2 7  hope
: 592 1 4  in
: 594 4 7  your
: 600 3 7  heart
- 605
: 628 3 12 And
: 636 2 19  some
: 640 14 19 times
* 656 1 19  you
: 658 4 14  lose
- 663
: 664 2 14 And
: 668 2 14  some
: 672 14 14 times
: 688 1 9  you're
: 690 4 9  shoo
: 696 2 7 ting
- 700
: 708 2 9 Bro
: 712 2 11 ken
: 716 2 9  ar
: 720 2 4 rows
: 724 2 7  in
: 728 1 4  the
* 730 4 7  dark
- 736
: 756 1 4 But
* 758 2 7  I
- 762
: 768 2 7 I
: 772 2 11  see
: 776 2 11  the
: 780 2 7  hope
: 784 1 4  in
: 786 4 7  your
: 792 3 7  heart
- 797
: 852 3 2 O
: 856 19 7 ~
: 876 1 7 ~
: 878 3 4 ~h
- 883
: 1376 2 7 I've
: 1380 2 11  seen
: 1384 1 11  the
: 1386 4 12  dark
: 1392 2 11 ness
: 1396 2 9  in
: 1400 1 7  the
: 1402 5 7  light
- 1409
: 1436 2 4 Kind
: 1440 1 7  of
: 1442 4 7  blue
: 1448 2 9  that
: 1452 2 11  leaves
: 1456 2 7  you
: 1460 2 4  lost
: 1464 2 4  and
: 1468 1 4  bli
: 1470 4 2 ~nd
- 1476
: 1506 1 4 The
* 1508 1 11  on
: 1510 2 14 ly
: 1514 4 16  thing
: 1520 2 14  that's
: 1524 2 14  black
: 1528 2 11  and
: 1532 1 11  whi
: 1534 4 9 ~te
- 1540
: 1556 1 14 Is
: 1558 2 18  that
: 1562 2 19  you
: 1566 2 18  don't
: 1570 2 18  have
: 1574 2 16  to
: 1578 2 16  walk
: 1582 2 14  a
: 1586 1 11 lo
: 1588 4 9 ~ne
: 1594 2 7  this
: 1598 4 7  time
- 1604
: 1616 1 11 We
: 1618 4 11  have
: 1624 2 7  to
: 1628 6 7  tear
: 1636 4 7  down
: 1642 1 7  the
: 1644 4 7  walls
- 1650
: 1652 4 7 That
: 1660 1 7  live
: 1662 1 9  in
* 1664 6 11  your
: 1672 4 9  heart
- 1678
: 1680 1 7 To
: 1682 4 9  find
: 1688 3 7  some
: 1697 1 11 o
: 1699 3 9 ~ne
: 1704 3 7  you
: 1712 5 7  call
: 1720 1 4  ho
: 1722 2 2 ~me
- 1726
: 1744 2 11 Now,
: 1748 2 9  you
: 1752 2 7  see
: 1756 4 7  me
: 1764 6 7  for
: 1772 6 7  me
- 1779
: 1780 3 -5 And
: 1786 1 -5  my
: 1788 1 -5  beau
: 1790 1 -5 ti
: 1792 6 -1 ful
* 1800 6 -3  scars
- 1807
: 1808 1 6 So
: 1810 2 11  ta
: 1813 2 9 ~ke
: 1816 3 7  my
: 1827 1 11  ha
: 1829 1 9 ~
: 1831 3 7 ~nd,
: 1836 4 7  don't
: 1842 4 7  let
: 1848 1 4  go
: 1850 3 2 ~
- 1855
: 1876 2 12 'Cause
: 1880 2 14  it's
: 1884 16 19  not
: 1904 1 19  too
: 1906 4 14  late,
: 1912 3 11  it's
- 1917
: 1924 10 14 Not
* 1936 1 14  too
: 1938 4 9  late,
: 1944 3 7  I
- 1949
: 1952 2 9 I
: 1956 2 11  see
: 1960 2 11  the
: 1964 2 7  hope
: 1968 1 4  in
: 1970 4 7  your
: 1976 5 7  hea
: 1982 1 11 ~
: 1984 4 11 ~rt
- 1990
: 2004 3 12 And
: 2012 2 19  some
: 2016 14 19 times
* 2032 1 19  you
: 2034 4 14  lose
- 2039
: 2040 2 14 And
: 2044 2 14  some
: 2048 14 14 times
: 2064 1 9  you're
: 2066 4 9  shoo
: 2072 2 7 ting
- 2076
: 2084 2 9 Bro
: 2088 1 11 ken
: 2090 4 9  ar
: 2096 2 4 rows
: 2100 2 7  in
: 2104 1 4  the
* 2106 4 7  dark
- 2112
: 2132 1 4 But
* 2134 2 7  I
- 2138
: 2144 2 7 I
: 2148 2 11  see
: 2152 2 11  the
: 2156 2 7  hope
: 2160 1 4  in
: 2162 4 7  your
: 2168 3 7  heart
- 2173
: 2228 3 2 O
: 2232 19 7 ~
: 2252 1 7 ~
: 2254 3 4 ~h
- 2259
: 2676 3 7 It's
: 2684 4 19  not
: 2692 1 18  too
: 2694 4 16 ~
: 2700 4 16  late,
: 2708 3 14  it's
: 2716 4 16  not
: 2724 1 14  too
: 2726 4 11 ~
: 2732 7 11  la
: 2740 3 9 ~
: 2744 3 7 ~te
- 2749
: 2752 2 9 I
: 2756 2 12  see
: 2760 2 12  the
: 2764 2 9  hope
: 2768 1 4  in
: 2770 4 7  your
: 2776 5 7  hea
: 2782 1 11 ~
: 2784 4 11 ~rt
- 2790
* 2804 4 7 Some
: 2812 4 19 times
: 2820 3 19  you
: 2824 2 16 ~'re
: 2828 4 16  lo
: 2836 4 14 sing
- 2842
: 2844 4 16 Some
: 2852 4 14 times
: 2860 4 11  shoo
: 2868 4 9 ting
- 2874
: 2884 2 9 Bro
: 2888 1 11 ken
: 2890 4 9  ar
: 2896 2 4 rows
: 2900 2 7  in
: 2904 1 4  the
* 2906 4 7  dark
- 2912
: 2932 3 2 O
: 2936 9 7 ~
: 2946 1 7 ~
: 2948 3 4 ~h
E