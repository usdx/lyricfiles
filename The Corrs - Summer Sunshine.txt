#ARTIST:The Corrs
#TITLE:Summer Sunshine
#MP3:The Corrs - Summer Sunshine.mp3
#VIDEO:The Corrs - Summer Sunshine.mp4
#COVER:The Corrs - Summer Sunshine.jpg
#AUTHOR:Euroivan
#LANGUAGE:English
#BPM:121
#GAP:4500
: 0 2 5 E
: 2 2 5 very
: 4 2 7 one's 
: 6 2 7 chang
: 8 2 5 ing, 
: 12 2 4 I 
: 14 2 5 stay 
: 16 2 7 the 
: 18 3 4 same
- 22
: 24 3 0 I'm 
: 32 2 5 a 
: 34 2 5 so
: 36 1 7 lo 
: 38 2 7 cel
: 40 2 5 lo 
: 44 1 4 out
: 46 2 5 side 
: 48 2 7 a 
: 50 4 4 chor
: 56 3 0 us
- 60
: 64 2 5 I've 
: 66 2 5 got 
: 68 1 7 a 
: 70 2 7 se
: 72 2 5 cret,
- 75
: 76 1 10 It's 
: 78 2 10 time 
: 80 1 7 for 
: 82 2 7 me 
: 84 1 5 to 
: 86 2 4 tell 
: 88 1 0 it
- 90
: 90 2 0 You've 
: 92 2 -2 been 
: 94 5 -3 keep
: 100 4 -2 ing 
: 104 4 2 me 
: 108 5 0 warm
- 114
: 128 1 5 Just 
: 130 2 5 sweet 
: 132 1 7 be
: 134 2 7 gin
: 136 2 5 nings 
: 140 1 4 and 
: 142 2 5 bit
: 144 2 7 ter 
: 146 4 4 en
: 152 3 0 dings
- 156
: 160 1 5 In 
: 162 2 5 cof
: 164 1 7 fee 
: 166 2 7 ci
: 168 2 5 ty, 
: 172 1 4 we 
: 174 2 5 bor
: 176 1 7 rowed 
: 178 5 4 hea
: 184 3 0 ven
- 188
: 192 1 5 Don't 
: 194 2 5 give 
: 196 1 7 it 
: 198 2 7 back,
- 202
: 204 1 10 I've 
: 206 2 10 nev
: 208 1 7 er 
: 210 2 7 felt 
: 212 2 5 so 
: 214 2 4 want
: 216 1 0 ed
- 218
: 218 2 0 Are 
: 220 2 -2 you 
: 222 5 -3 tak
: 228 4 -2 ing 
: 232 3 2 me 
: 236 5 0 home?
- 242
: 252 1 0 You 
: 254 2 2 tell 
: 256 2 0 me 
: 258 2 -2 you 
: 260 3 -3 have 
: 264 4 -2 to 
: 268 7 -5 go
- 276
: 280 2 0 In 
: 282 2 0 the 
* 286 8 9 heat 
: 294 6 7 of 
: 302 4 10 sum
: 306 2 9 mer 
: 308 3 5 sun
: 312 4 7 shine
- 316
: 316 2 5 I 
: 318 2 7 miss 
: 320 3 5 you 
: 330 3 0 like 
: 334 2 10 no
: 336 2 10 bo
: 338 2 9 dy 
: 340 2 5 else
- 343
: 344 4 7 In 
: 348 2 5 the 
* 350 8 7 heat 
: 359 4 5 of 
* 366 3 14 sum
: 370 2 12 mer 
: 372 4 7 sun
: 376 3 5 shine
- 380
: 380 1 5 I'll 
: 382 4 7 kiss 
: 386 3 5 you, 
: 394 4 0 and 
: 398 2 10 no
: 400 2 10 bo
: 402 2 9 dy 
: 404 3 5 needs 
: 408 3 7 to 
* 412 18 5 know
- 432
: 446 2 5 Now 
: 448 2 5 that 
: 450 2 7 you've 
: 452 2 7 loved 
: 454 2 5 me,
- 457
: 458 2 4 there's 
: 460 2 5 no 
: 462 2 7 re
: 464 3 4 tur
: 470 3 0 ning
- 474
: 478 2 5 I 
: 480 2 5 keep 
: 482 1 7 com
: 484 2 7 par
: 486 2 5 ing,
- 489
: 490 1 4 You're 
: 492 2 5 al
: 494 2 7 ways 
: 496 4 4 win
: 502 3 0 ning
- 506
: 510 2 5 I 
: 512 2 5 try 
: 514 1 7 to 
: 516 2 7 be 
: 518 2 5 strong
- 520
: 520 2 5 but 
: 522 1 10 you'll 
: 524 2 10 nev
: 526 1 7 er 
: 528 2 7 be 
: 530 1 5 more 
: 532 2 4 want
: 534 1 0 ed
- 536
: 536 2 0 Will 
: 538 2 -2 you 
: 540 5 -3 make 
: 546 4 -2 me 
: 550 4 2 at 
: 554 5 0 home?
- 560
: 570 1 0 Don't 
: 572 2 2 tell 
: 574 2 0 me 
: 576 2 -2 you 
: 578 3 -3 have 
: 582 4 -2 to 
: 586 7 -5 go
- 594
: 598 2 0 In 
: 600 2 0 the 
* 604 8 9 heat 
: 612 6 7 of 
: 620 4 10 sum
: 624 2 9 mer 
: 626 3 5 sun
: 630 4 7 shine
- 634
: 634 2 5 I 
: 636 2 7 miss 
: 638 3 5 you 
: 648 3 0 like 
: 652 2 10 no
: 654 2 10 bo
: 656 2 9 dy 
: 658 2 5 else
- 661
: 662 4 7 In 
: 666 2 5 the 
* 668 8 7 heat 
: 677 4 5 of 
* 684 3 14 sum
: 688 2 12 mer 
: 690 4 7 sun
: 694 3 5 shine
- 698
: 698 1 5 I 
: 700 4 7 kiss 
: 704 3 5 you, 
: 712 4 0 and 
: 716 2 10 no
: 718 2 10 bo
: 720 2 9 dy 
: 722 3 5 needs 
: 726 3 7 to 
* 730 18 5 know
- 750
: 828 2 5 To 
: 830 2 5 sweet 
: 832 1 7 be
: 834 2 7 gin
: 836 2 5 nings 
: 840 1 4 and 
: 842 2 5 bit
: 844 2 7 ter 
: 846 5 4 en
: 852 3 0 dings
- 856
: 860 2 5 In 
: 862 2 5 cof
: 864 1 7 fee 
: 866 2 7 ci
: 868 2 5 ty, 
: 872 2 4 we 
: 874 2 5 bor
: 876 2 7 rowed 
: 878 6 4 hea
: 884 4 0 ven
- 890
: 892 1 5 Don't 
: 894 2 5 give 
: 896 1 7 it 
: 898 3 7 back
- 902
: 906 1 7 Win
: 908 3 7 ter 
: 912 2 5 is 
: 914 4 4 com
: 918 2 0 ing 
: 920 2 -2 and I 
: 922 6 -3 need 
: 928 4 -2 to 
: 932 4 2 stay 
: 936 9 0 warm
- 946
F 966 4 0 The 
F 970 19 12 heat...
- 980
: 980 2 0 In 
: 982 2 0 the 
* 986 8 9 heat 
: 994 6 7 of 
: 1002 4 10 sum
: 1006 2 9 mer 
: 1008 3 5 sun
: 1012 4 7 shine
- 1016
: 1016 2 5 I 
: 1018 2 7 miss 
: 1020 3 5 you 
: 1030 3 0 like 
: 1034 2 10 no
: 1036 2 10 bo
: 1038 2 9 dy 
: 1040 2 5 else
- 1043
: 1044 4 7 In 
: 1048 2 5 the 
* 1050 8 7 heat 
: 1059 4 5 of 
* 1066 3 14 sum
: 1070 2 12 mer 
: 1072 4 7 sun
: 1076 3 5 shine
- 1080
: 1080 1 5 I 
: 1082 4 7 kiss 
: 1086 3 5 you, 
: 1094 4 0 and 
: 1098 2 10 no
: 1100 2 10 bo
: 1102 2 9 dy 
: 1104 3 5 else
- 1108
: 1108 2 0 In 
: 1110 2 0 the 
* 1114 8 9 heat 
: 1122 6 7 of 
: 1130 4 10 sum
: 1134 2 9 mer 
: 1136 3 5 sun
: 1140 4 7 shine
- 1144
: 1144 2 5 I 
: 1146 2 7 miss 
: 1148 3 5 you 
: 1158 3 0 like 
: 1162 2 10 no
: 1164 2 10 bo
: 1166 2 9 dy 
: 1168 2 5 else
- 1171
: 1172 4 7 In 
: 1176 2 5 the 
* 1178 8 7 heat 
: 1187 4 5 of 
* 1194 3 14 sum
: 1198 2 12 mer 
: 1200 4 7 sun
: 1204 3 5 shine
- 1208
: 1208 1 5 I 
: 1210 4 7 kiss 
: 1214 3 5 you, 
: 1222 4 0 and 
: 1226 1 10 no
: 1228 2 10 bo
: 1230 2 9 dy 
: 1232 3 5 needs 
: 1236 3 5 to 
* 1240 18 5 know

