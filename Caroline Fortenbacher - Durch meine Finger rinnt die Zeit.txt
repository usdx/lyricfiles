#TITLE:Durch meine Finger rinnt die Zeit
#ARTIST:Caroline Fortenbacher
#MP3:Caroline Fortenbacher - Durch meine Finger rinnt die Zeit.mp3
#COVER:Caroline Fortenbacher - Durch meine Finger rinnt die Zeit [CO].jpg
#BACKGROUND:Caroline Fortenbacher - Durch meine Finger rinnt die Zeit [BG].jpg
#BPM:139,95
#GAP:330
#ENCODING:UTF8
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
: 0 6 14 Mor
: 7 4 14 gens
: 11 3 16  nimmt
: 15 4 17  sie
- 21
: 27 2 17  ih
: 29 2 19 re
: 32 5 21  Ta
: 37 6 21 sche
- 44
: 44 3 22  und
: 47 4 21  geht
: 51 2 19  zur
: 53 6 17  Schu
: 59 2 16 le.
- 63
: 71 4 14 Winkt
: 75 2 14  noch
: 77 3 16  mal
: 80 4 17  rauf,
- 86
: 91 2 17 in
: 93 3 19  Ge
: 96 4 22 dan
: 100 2 21 ken
: 103 4 19  schon
: 107 4 17  ganz
: 112 19 21  weit.
- 133
: 144 7 14 Wenn
: 152 2 14  ich
: 154 3 16  sie
: 158 5 17  dann
: 170 3 17  nicht
: 173 3 19  mehr
: 176 6 21  seh',
- 183
: 183 4 21 Kämpf
: 188 3 22  ich
: 191 4 21  mit
: 195 3 19  den
: 198 4 17  Trä
: 202 3 16 nen.
- 207
: 211 2 14 Dann
: 213 3 22  ist
: 216 2 22  in
: 219 3 21  mir
: 223 4 21  so
: 227 5 19 viel
: 234 5 19  Trau
: 239 3 17 rig
: 242 11 17 keit
- 255
: 269 2 14 Dann
: 272 5 14  denk
: 277 5 22  ich:
- 283
: 283 2 22 Ir
: 285 2 21 gend
: 287 5 21 wann
: 292 2 19  geht
: 294 3 19  sie
- 299
: 301 2 12  für
: 304 3 12  im
: 307 12 21 mer.
- 321
: 332 3 14 Schon
: 335 7 14  jetzt
: 342 4 22  lebt
: 346 3 22  sie
: 349 3 21  in
: 352 2 21  ih
: 354 3 19 rer
- 358
: 359 3 19  ei
: 362 5 12 genen
: 367 11 12  Welt.
- 380
: 397 3 14 Wenn
: 400 4 14  wir
: 404 5 22  zu
: 409 4 22 sam
: 413 2 21 men
: 415 3 21  la
: 418 3 19 chen,
- 423
: 427 3 19 denk
: 430 2 12  ich
: 432 2 19  ma
: 434 4 21 nch
: 438 6 21 mal:
- 445
: 445 3 12 Wie
: 448 5 12  gut
: 453 4 19  sie
: 457 7 19  mir
: 464 2 17  ge
: 466 10 17 fällt!
- 478
: 488 2 9 Und
: 490 2 12  durch
: 492 2 17  mei
: 494 2 21 ne
: 496 2 21  Fin
: 498 5 19 ger
: 503 4 19  rinnt
: 507 2 17  die
: 509 5 17  Zeit.
- 515
: 516 4 24 Wenn
: 520 2 21  ich
: 523 2 17  die
: 525 6 16  Ta
: 531 4 24 ge
: 535 4 21  und
: 539 3 16  Mo
: 542 5 14 men
: 547 7 19 te
- 556
: 563 5 19 nur
: 568 3 17  hal
: 571 2 14 ten
: 573 4 12  könn
: 577 2 17 te.
- 581
: 583 2 9 Doch
: 585 2 9  durch
: 587 2 17  mei
: 589 2 21 ne
: 591 3 21  Fin
: 594 5 19 ger
: 599 4 19  rinnt
: 603 2 17  die
: 605 5 17  Zeit.
- 612
: 615 2 9 Mor
: 617 2 12 gen
: 619 2 17  schon
: 621 2 21  ist
: 623 2 21  heut
: 626 5 19  Ver
: 631 3 19 gan
: 634 4 17 gen
: 638 3 17 heit.
- 642
: 643 3 24 Ich
: 646 4 21  weiß,
: 650 3 17  bald
: 653 6 16  wird
: 659 4 24  sie
: 663 3 21  ei
: 667 2 16 ne
: 669 5 14  Frau
: 676 11 19  sein
- 689
: 691 5 19 und
: 696 2 17  ich
: 699 3 14  werd
: 703 3 12  grau
: 706 3 17  sein.
- 710
: 711 2 9 Denn
: 713 2 12  durch
: 715 2 17  mei
: 717 2 21 ne
: 719 3 21  Fin
: 722 4 19 ger
: 727 4 19  rinnt
: 731 2 17  die
: 733 9 17  Zeit.
- 744
: 1648 7 14 Mor
: 1655 4 14 gens
: 1659 3 16  sitz
: 1663 8 17  ich
- 1672
: 1672 4 17  ne
: 1676 2 19 ben
: 1679 8 21  ihr
- 1688
: 1688 3 21 und
: 1691 2 22  sie
: 1693 6 21  isst
: 1699 2 19  ihr
: 1702 3 17  Früh
: 1705 5 16 stück.
- 1712
: 1719 2 14 Noch
: 1722 2 14  nicht
: 1724 3 16  ganz
: 1728 3 17  wach,
- 1733
: 1739 2 17 wech
: 1741 3 19 seln
: 1744 3 22  wir
: 1748 3 21  oft
: 1751 4 19  kaum
: 1755 4 17  ein
: 1759 28 21  Wort.
- 1789
: 1792 6 14 Kaum
: 1799 4 14  ist
: 1803 2 16  sie
: 1805 6 17  weg,
- 1813
: 1817 3 17 tut's
: 1821 2 19  mir
: 1823 8 21  leid
- 1832
: 1832 3 21 und
: 1835 2 22  ich
: 1837 6 21  fühl
: 1843 2 19  mich
: 1846 4 17  schul
: 1850 3 16 dig.
- 1855
: 1858 4 14 Wie
: 1862 4 22 der
: 1866 3 22  ein
: 1869 2 21  Mo
: 1871 4 21 ment
- 1877
: 1881 2 19  ver
: 1883 4 19 lor'n
: 1887 2 17  und
: 1890 11 17  fort.
- 1903
: 1917 2 14 Ich
: 1919 4 14  wollt
: 1923 3 22  mir
: 1926 5 22  dir
- 1931
: 1931 3 21  so
: 1934 4 21  vie
: 1938 3 19 les
: 1943 6 19  un
: 1949 2 12 ter
: 1951 7 12 neh
: 1958 19 21 men:
- 1979
: 1981 2 14 Ans
: 1984 4 14  Meer
: 1988 5 22  fahr'n,
- 1993
: 1993 3 22 in
: 1996 3 21  den
: 1999 4 21  Ber
: 2003 2 19 gen
: 2005 6 19  wan
: 2011 3 12 dern
: 2014 10 12  geh'n.
- 2026
: 2043 5 14 Doch
: 2051 2 14  ir
: 2053 4 22 gend
: 2057 2 22 wie
- 2060
: 2060 2 21  kam
: 2062 4 21  mei
: 2066 3 19 stens
: 2069 5 19  was
- 2075
: 2075 4 22  da
: 2079 8 22 zwi
: 2087 3 21 schen.
- 2091
: 2091 4 21 Heut
: 2095 4 21  kann
: 2099 3 19  ich's
: 2103 4 19  nicht
: 2107 2 17  ver
: 2109 11 17 steh'n.
- 2122
: 2135 2 9 Und
: 2137 2 12  durch
: 2139 2 17  mei
: 2141 2 21 ne
: 2143 2 21  Fin
: 2145 5 19 ger
: 2151 4 19  rinnt
: 2155 2 17  die
: 2157 6 17  Zeit.
- 2163
: 2163 4 24 Wenn
: 2167 4 21  ich
: 2171 2 17  die
: 2173 5 16  Ta
: 2179 4 24 ge
: 2183 4 21  und
: 2187 2 16  Mo
: 2189 5 14 men
: 2194 12 19 te
- 2208
: 2211 3 19 nur
: 2215 3 17  hal
: 2218 3 14 ten
: 2221 4 12  könn
: 2225 4 17 te.
- 2230
: 2231 2 9 Doch
: 2233 2 12  durch
: 2235 2 17  mei
: 2237 1 21 ne
: 2239 2 21  Fin
: 2241 6 19 ger
: 2247 4 19  rinnt
: 2251 2 17  die
: 2253 6 17  Zeit.
- 2261
: 2263 2 9 Mor
: 2265 2 12 gen
: 2267 2 17  schon
: 2269 2 21  ist
: 2271 2 21  heut
: 2274 5 19  Ver
: 2279 4 19 gan
: 2283 2 17 gen
: 2285 5 17 heit.
- 2291
: 2292 2 24 Ich
: 2295 3 21  weiß,
: 2298 3 17  bald
: 2301 5 16  wird
: 2307 3 24  sie
: 2311 3 21  ei
: 2314 2 16 ne
: 2317 5 14  Frau
: 2323 10 19  sein
- 2335
: 2341 2 19 und
: 2343 3 17  ich
: 2347 3 14  werd
: 2351 2 12  grau
: 2353 4 17  sein.
- 2358
: 2358 2 9 Denn
: 2360 2 12  durch
: 2362 2 17  mei
: 2364 2 21 ne
: 2366 3 21  Fin
: 2369 6 19 ger
: 2375 4 19  rinnt
: 2379 2 17  die
: 2381 13 17  Zeit.
- 2395
: 2395 4 14 Ich
: 2399 4 14  wollt
: 2403 4 22  der
: 2407 6 22  Film
: 2413 2 21  des
: 2415 4 21  Le
: 2419 3 19 bens
- 2422
: 2422 7 19  blie
: 2429 2 12 be
: 2431 2 12  steh
: 2433 4 21 ~
: 2437 17 21 'n
- 2456
: 2461 2 14 Die
: 2463 3 14  Hoff
: 2467 4 22 nung
: 2471 5 22  ist
: 2477 2 21  ver
: 2479 2 21 ge
: 2481 2 19 bens,
- 2483
: 2483 7 19 denn
: 2491 2 12  die
: 2494 6 12  Zeit
- 2501
: 2503 2 9  rinnt
: 2505 2 12  mir
: 2507 2 17  durch
: 2509 2 21  die
: 2511 2 22  Fin
: 2513 21 24 ger.
- 2536
: 2815 7 14 Mor
: 2822 5 14 gens
: 2827 3 16  nimmt
: 2830 4 17  sie
- 2836
: 2845 2 17  ih
: 2847 3 19 re
: 2850 5 21  Ta
: 2855 7 21 sche
- 2863
: 2864 4 22 und
: 2868 3 21  geht
: 2871 3 19  zur
: 2874 5 17  Schu
: 2879 4 16 le.
- 2885
: 2893 5 14 Winkt
: 2898 2 14  noch
: 2900 3 16  mal
: 2903 4 17  rauf,
- 2909
: 2917 3 17 in
: 2920 3 19  Ge
: 2923 6 22 dan
: 2929 3 21 ken
: 2935 6 19  schon
: 2941 6 17  ganz
: 2948 15 21  weit.
E