#TITLE:Blue Suede Shoes
#ARTIST:Elvis Presley
#LANGUAGE:Englisch
#EDITION:SingStar Legends
#GENRE:Pop
#YEAR:1968
#MP3:Elvis Presley - Blue Suede Shoes.mp3
#COVER:Elvis Presley - Blue Suede Shoes [CO].jpg
#VIDEO:Elvis Presley - Blue Suede Shoes [VD#0].avi
#VIDEOGAP:0
#BPM:350
#GAP:6857,14
: 0 2 52 Well
: 2 2 54  it's
: 6 2 52  ah
: 9 4 57  one
: 15 2 57  for
: 19 2 57  the
: 22 2 60  mo
: 26 6 60 ney
- 34
: 36 5 57 Two
: 44 3 57  for
: 48 2 57  the
: 51 4 60  show
- 57
: 65 3 57 Three
: 71 2 57  to
: 73 8 57  get
: 82 2 61  rea
: 86 2 61 dy
: 89 3 64  now
- 93
: 95 7 67 Go
: 102 5 62  cat
: 110 5 59  go
- 116
: 117 3 57 But
: 121 3 60  don't
: 124 3 57 ~
: 129 3 57 you
- 134 135
* 152 2 64 Step
: 156 2 64  on
: 158 4 62  my
: 163 4 60  blue
: 169 4 57  suede
: 180 3 57  shoes
- 185 207
: 227 2 61 Well
: 230 4 62  you
: 234 2 63  can
: 237 3 64  do
: 242 2 64  a
: 245 2 64 ny
: 249 3 64 thing
- 254
: 257 2 52 But
: 260 3 52  lay
: 265 4 57  off
: 270 2 57  of
: 273 4 59  my
: 281 3 60  blue
: 288 3 57  suede
: 296 5 57  shoes
- 303 323
: 343 2 52 Well
: 346 4 54  you
: 350 3 52  can
: 353 5 57  knock
: 360 3 57  me
: 365 11 60  down
- 378
: 382 2 57 Step
: 386 2 57  in
: 389 3 57  my
* 393 4 52  face
: 402 5 57  ah
- 409
: 411 2 57 Slan
: 415 2 57 der
: 418 3 57  my
: 426 6 60  name
- 433
: 433 3 57 All
: 436 2 55 ~
: 439 2 57 o
: 444 2 57 ver
: 447 3 57  the
: 452 2 54  place
- 456 457
: 467 3 57 Do
: 471 2 57  a
: 474 3 57 ny
: 478 11 60 thing
- 490
: 491 2 57 That
: 494 2 55  you
: 497 5 57  wan
: 504 3 57 na
: 509 2 60  do
: 513 6 64  ah
- 521
: 523 5 57 Uh
: 530 8 57  huh
: 540 2 61  ho
: 543 2 61 ney
- 546
: 547 4 64 Lay
* 551 7 67  off
: 558 2 67  of
: 560 6 64  my
: 568 6 60  shoes
- 575
: 575 3 57 And
: 580 3 60  don't
: 583 2 57 ~
: 587 3 60 you
- 592 593
: 611 2 64 Step
: 614 2 64  on
: 616 3 62  my
: 621 5 60  blue
: 628 6 57  suede
: 638 5 57  shoes
- 645 666
: 686 2 60 Well
: 689 3 62  you
: 692 2 63  can
: 696 2 64  do
: 699 2 64  a
: 702 2 64 ny
: 706 4 64 thing
- 712
: 714 2 52 But
: 717 2 52  lay
: 720 5 57  off
: 728 2 57  of
: 732 2 59  my
: 738 6 60  blue
: 745 4 57  suede
: 752 5 57  shoes
- 759 760
: 782 4 64 Hey
: 787 3 63  ya
: 791 3 60  ya
: 795 3 57  ya
- 800 820
: 840 6 57 Steal
: 847 3 57  my
: 852 4 52  car
- 858 859
* 870 5 57 Drink
: 877 6 57  my
: 885 1 60  li
: 888 2 60 quor
- 891
: 891 3 57 From
: 895 3 55  an
: 898 6 57  old
: 907 5 57  fruit
: 915 4 52  jar
- 921
: 926 2 55 Ah
: 929 3 57  do
: 933 2 57  a
: 936 3 57 ny
: 941 8 60 thing
- 951
: 957 2 54 You
: 960 4 57  wan
: 966 3 57 na
: 970 5 60  do
- 976
: 976 2 57 Ah
: 979 2 57  ah
: 982 2 57  ah
: 986 4 57  ah
* 993 6 57  ah
: 1001 2 61  ho
: 1004 2 61 ney
- 1007
: 1009 5 64 Lay
: 1014 6 67  off
: 1022 7 67  my
: 1031 5 67  shoes
- 1037
: 1039 3 66 And
: 1042 4 67  don't
: 1049 3 57  you
- 1054 1055
: 1073 2 64 Step
: 1077 2 64  on
: 1080 3 62  my
: 1083 5 60  blue
: 1091 7 57  suede
: 1100 6 57  shoes
- 1108 1129
: 1149 2 61 Well
: 1152 2 62  you
: 1155 2 63  can
: 1159 2 64  do
: 1162 2 64  a
: 1165 2 64 ny
: 1169 5 64 thing
- 1175
: 1176 2 52 But
: 1179 2 54  lay
: 1184 6 57  off
: 1192 2 57  of
: 1195 3 59  my
: 1201 4 60  blue
: 1208 6 57  suede
: 1215 3 57  shoes
- 1219
F 1220 2 36 Play
F 1223 2 36  it
F 1226 2 36  one
F 1229 2 36  time
F 1232 2 36  baby
- 1236 1597
: 1617 4 57 Burn
: 1623 5 57  my
: 1631 6 60  house
- 1639
: 1645 6 57 Steal
: 1652 2 57  my
: 1656 6 52  car
- 1664 1665
: 1676 3 57 Drink
: 1682 5 57  my
: 1690 2 60  li
: 1693 2 61 quor
- 1696
: 1696 2 57 From
: 1701 2 54  an
* 1704 4 57  old
: 1711 6 57  fruit
: 1719 5 55  jar
- 1726 1727
: 1735 2 57 Do
: 1738 4 57  a
: 1742 2 57 ny
: 1746 5 60 thing
- 1753
: 1758 1 57 That
: 1760 2 55  you
: 1763 6 57  wan
: 1771 3 57 na
: 1775 4 60  do
* 1780 5 57  ah
- 1786
: 1788 2 57 But
: 1791 4 57  uh
: 1799 5 57  huh
: 1807 2 61  ho
: 1811 2 61 ney
- 1814
: 1814 4 64 Lay
: 1819 6 67  off
: 1826 3 67  of
: 1830 5 67  my
: 1836 6 67  shoes
- 1843
: 1844 3 64 And
: 1848 4 67  don't
: 1855 3 57  you
- 1860 1861
: 1878 2 64 Step
: 1882 3 64  on
: 1885 3 62  my
: 1890 5 60  blue
: 1897 5 57  suede
: 1907 6 57  shoes
: 1915 3 62  al
: 1919 3 57 right
- 1924 1938
: 1958 3 61 You
: 1962 3 61  can
: 1966 3 62  do
: 1970 2 64  a
: 1974 2 64 ny
: 1977 2 64 thing
- 1981
: 1985 2 54 But
: 1988 2 54  lay
: 1992 5 57  off
: 2000 1 55  of
: 2003 3 59  my
: 2008 1 57  blue
: 2009 4 60 ~
: 2016 5 57 suede
: 2024 3 57  shoes
- 2028
F 2028 3 36 One
F 2032 4 36  time
F 2038 5 36  baby
- 2045 2046
: 2071 2 55 Well
: 2074 4 57  it's
: 2078 4 55  a
- 2084
: 2089 3 57 Blue,
: 2097 3 57  blue
: 2105 1 55  my
: 2108 3 57  blue
: 2115 6 57  suede
* 2124 7 60  shoes
: 2132 2 55  ah
- 2136 2137
: 2146 4 57 Blue,
: 2154 4 57  blue
: 2161 1 55  my
: 2165 4 57  blue
: 2172 7 57  suede
: 2182 5 60  shoes
: 2190 2 62  ba
: 2194 2 74 by
- 2198
: 2205 2 57 Blue,
: 2212 5 57  blue
: 2219 1 55  my
: 2223 4 57  blue
: 2230 8 57  suede
: 2240 6 60  shoes
- 2248 2249
: 2262 3 57 Blue,
: 2270 5 57  blue
: 2277 2 55  my
: 2281 4 57  blue
: 2289 4 57  suede
- 2295 2296
: 2304 2 64 You
: 2308 2 64  can
: 2311 2 64  do
: 2315 2 64  a
: 2318 2 64 ny
: 2322 4 64 thing
- 2328
: 2331 2 52 But
: 2334 2 54  lay
: 2337 5 57  off
: 2345 2 57  of
: 2348 3 59  my
: 2356 4 60  blue
: 2363 4 57  suede
: 2370 6 57  shoes
- 2378
F 2386 4 36 Why
F 2393 2 36  ya
F 2396 2 36  ya
F 2400 6 36  yada
- 2408 2409
: 2419 2 55 Well
: 2423 2 57  it's
: 2426 3 55  a
- 2431
: 2436 3 57 Blue,
: 2443 7 57  blue
: 2451 2 55  my
: 2454 5 57  blue
: 2461 8 57  suede
: 2471 7 60  shoes
: 2478 2 55  ah
- 2482 2483
: 2493 5 57 Blue,
: 2501 5 57  blue
: 2508 2 55  my
: 2511 5 57  blue
: 2519 5 57  suede
: 2530 6 60  shoes
: 2537 2 60  ba
: 2540 2 74 by
- 2544
: 2552 3 57 Blue,
: 2558 6 57  blue
: 2565 2 55  my
: 2569 5 57  blue
: 2578 7 57  suede
: 2587 6 60  shoes
: 2593 2 57  ah
- 2597 2598
* 2608 3 57 Blue,
: 2615 7 57  blue
: 2623 3 55  my
: 2627 6 57  blue
: 2635 6 57  suede
- 2643
: 2651 3 63 You
: 2655 2 64  can
: 2659 2 64  do
: 2662 3 64  a
: 2666 3 64 ny
: 2670 3 64 thing
- 2675
: 2677 2 64 But
: 2680 2 64  lay
: 2685 5 67  off
: 2695 5 62  my
: 2703 4 60  blue
: 2710 4 57  suede
* 2717 21 57  shoes
: 2738 3 54 ~
: 2741 4 52 ~
E
