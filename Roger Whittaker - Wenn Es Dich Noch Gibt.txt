#TITLE:Wenn Es Dich Noch Gibt
#ARTIST:Roger Whittaker
#MP3:Roger Whittaker - Wenn Es Dich Noch Gibt.mp3
#VIDEO:Roger Whittaker - Wenn Es Dich Noch Gibt.mp4
#COVER:Roger Whittaker - Wenn Es Dich Noch Gibt.jpg
#BPM:243,93
#GAP:2704,5
#ENCODING:UTF8
#PREVIEWSTART:29,208
#LANGUAGE:German
#GENRE:General Easy Listening
#YEAR:1980
#AUTHOR:fepo+monal
: 0 4 2 Wenn
: 7 3 5  es
: 14 3 5  dich
: 19 3 7  noch
: 27 13 5  gibt
- 42
: 62 4 2 Sag
: 70 4 5  wo
: 78 3 10  ich
: 83 4 9  dich
: 91 10 7  fin
: 105 7 5 de
- 114
: 138 3 2 Ich
: 144 4 5  muss
: 151 4 5  dich
: 158 3 5  wie
: 163 3 7 der
: 171 8 3  sehn
- 181
: 191 4 5 Su
: 197 3 5 che
: 202 2 5  dich
: 206 2 3  ü
: 210 6 3 ber
: 218 9 2 all
- 229
: 287 5 2 Wenn
* 295 3 5  es
* 302 3 5  dich
* 307 3 7  noch
: 315 10 5  gibt
- 327
: 352 4 2 Gib
: 359 3 5  mir
: 367 3 10  ei
: 372 3 9 ne
: 378 9 7  Chan
: 393 7 5 ce
- 402
: 427 2 2 Es
: 431 3 5  war
: 438 4 5  doch
: 445 5 5  schön
: 452 3 7  mit
: 458 7 3  uns
- 467
: 480 5 5 A
: 487 2 5 ber
: 491 2 5  es
: 495 2 3  war
: 500 3 3  ein
: 506 11 2 mal
- 519
: 577 4 0 Ich
: 588 5 2  ging
: 598 8 5  fort
- 608
: 618 3 3 Ob
: 624 3 3 wohl
: 629 3 2  ich
: 634 3 0  doch
: 639 4 2  glück
: 645 3 2 lich
: 650 4 5  war
: 657 4 7  bei
: 662 5 10  dir
- 669
: 702 5 0 Doch
: 713 5 2  mein
: 725 8 5  Traum
: 742 3 3  von
: 747 4 3  Frei
: 754 4 2 heit
: 760 2 0  war
: 765 4 2  stär
: 772 6 2 ker
- 780
: 830 6 0 Ich
: 842 5 2  war
: 854 6 5  dumm,
: 870 4 3  nun
: 878 4 3  komm
: 885 3 2  ich
: 890 3 0  zu
: 894 5 2 rück
: 902 3 2  und
: 907 4 7  an
: 913 3 7  der
: 918 6 10  Tür
- 926
: 932 5 10 Steht
: 939 3 9  ein
: 946 5 10  frem
: 953 3 14 der
: 958 29 12  Na
: 990 4 10 me
- 996
: 1035 3 10 Und
: 1040 4 9  da
: 1047 4 7 rum
- 1053
: 1056 4 2 Wenn
: 1063 3 5  es
: 1070 3 5  dich
: 1076 3 7  noch
: 1083 7 5  gibt
- 1092
: 1118 7 2 Hüll
: 1127 4 5  dich
: 1134 3 10  nicht
: 1140 3 9  in
: 1146 12 7  Schwei
: 1163 6 5 gen
- 1171
: 1196 2 2 Ich
: 1200 4 2  lieb
: 1207 4 5  dich
: 1215 3 5  im
: 1220 3 7 mer
: 1227 5 3  noch
- 1234
: 1247 6 5 Willst
: 1255 2 5  du
: 1259 2 5  mir
: 1263 2 3  nicht
: 1267 4 3  ver
: 1275 8 2 zeihn
- 1285
: 1640 6 0 Man
: 1652 5 2  sagt
: 1663 7 5  mir
- 1672
: 1680 3 3 Für
: 1685 3 3  dich
: 1692 2 3  wär
: 1696 3 0  es
: 1704 4 2  bes
: 1711 4 2 ser
: 1717 3 7  wenn
: 1723 3 7  ich
: 1728 5 10  geh
- 1735
: 1768 7 0 Jetzt
: 1781 4 2  wo
: 1790 7 5  grad
: 1808 3 3  die
: 1813 6 3  Wun
: 1821 4 2 den
: 1827 3 0  ver
: 1832 4 2 heilt
: 1840 6 2  sind
- 1848
: 1896 6 0 Willst
: 1909 5 2  auch
: 1920 5 5  du
- 1927
: 1937 3 2 Auch
: 1942 3 3  du,
: 1949 2 2  dass
: 1953 3 0  ich
: 1960 3 2  dich
: 1968 3 2  nie
: 1973 5 7  wie
: 1981 3 7 der
: 1985 6 10  seh,
- 1993
: 1998 4 -2 Dass
: 2005 3 -2  kann
: 2010 3 -2  ich
: 2015 2 -2  ni
: 2018 2 2 ~cht
* 2023 27 0  glau
: 2057 10 -2 ben
- 2069
: 2089 18 2 Oh
: 2109 2 3  oh
: 2112 3 2  oh
: 2117 11 0  oh
- 2130
: 2164 3 9 Und
: 2169 6 9  da
: 2178 4 7 rum
- 2184
: 2186 4 4 Wenn
* 2193 3 7  es
* 2200 3 7  dich
* 2205 4 9  noch
: 2212 8 7  gibt
- 2222
: 2248 5 4 Lass
: 2257 4 7  es
: 2264 4 12  so
: 2270 4 11  nicht
: 2278 11 9  en
: 2293 8 7 den
- 2303
: 2326 2 0 Ich
: 2330 3 7  lieb
: 2337 4 7  dich
: 2345 3 7  im
: 2350 3 9 mer
: 2357 8 5  noch
- 2367
: 2377 6 7 Willst
: 2385 2 7  du
: 2389 2 7  mir
: 2393 3 5  nicht
: 2398 3 5  ver
: 2405 9 4 zeihn
- 2416
: 2453 3 4 Ich
: 2458 4 7  lieb
: 2465 3 7  dich
: 2473 3 7  im
: 2478 4 9 mer
: 2485 6 5  noch
- 2493
: 2505 5 7 Willst
: 2512 3 7  du
: 2517 2 7  mir
: 2521 2 5  nicht
: 2525 4 5  ver
: 2533 11 4 zeihn
- 2546
: 2582 2 4 Viel
: 2585 6 7 leicht
: 2593 5 7  kann
: 2601 3 7  das
: 2606 8 9  für
: 2617 9 5  uns
- 2628
: 2644 4 7 Ein
: 2650 5 7  neu
: 2657 3 7 er
: 2665 10 5  An
: 2678 12 5 fang
: 2697 25 4  sein
E