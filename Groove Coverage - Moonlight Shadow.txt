#TITLE:Moonlight Shadow
#ARTIST:Groove Coverage
#MP3:Groove Coverage - Moonlight Shadow.mp3
#VIDEO:Groove Coverage - Moonlight Shadow [VD#0].mp4
#COVER:Groove Coverage - Moonlight Shadow [CO].jpg
#BPM:281
#GAP:45450
#VIDEOGAP:0
#ENCODING:UTF8
#LANGUAGE:English
#EDITION:[SC]-Songs
#START:42
: 2 3 -1 The
: 6 2 1 ~
: 11 6 2  last
: 22 2 2  that
: 27 2 2  e
: 31 2 4 ver
: 35 3 2  she
: 39 2 1 ~
: 43 14 -1  saw
: 60 5 -1  him,
- 67
: 75 3 1 car
: 79 3 1 ried
: 83 3 2  a
* 87 3 4 way
* 92 3 2 ~
: 100 3 2  by
: 104 2 4  a
: 108 5 6  moon
: 115 5 6 light
: 123 3 4  sha
: 127 5 -3 dow,
- 134
: 136 3 -3 He
: 140 5 2  passed
: 148 5 2  on
: 156 3 2  wor
: 160 3 4 ried
: 164 3 2  and
: 168 3 1 ~
: 172 13 -1  war
: 188 5 -1 ning,
- 195
: 204 3 1 car
: 208 3 1 ried
: 212 3 2  a
* 216 3 4 way
* 220 3 2 ~
: 228 3 2  by
: 232 3 4  a
: 236 6 6  moon
: 244 6 6 light
: 252 4 4  sha
: 256 6 -3 dow,
- 264
: 268 5 6 Lost
: 276 3 6  in
: 280 3 9  a
: 284 3 7  rid
: 288 3 7 dle
: 292 4 6  that
: 300 3 4  Sa
: 304 3 4 tur
: 308 3 6 day
: 312 5 4  night,
- 319
* 332 10 6 far
: 343 3 4  a
: 347 6 2 way
: 356 3 2  on
: 360 2 -1  the
: 364 3 4  o
: 368 4 6 ther
: 376 3 4  side
: 380 3 2 ~,
- 385
: 389 2 2 he
: 392 3 4  was
: 396 6 6  caught
: 404 2 6  in
: 408 3 9  the
: 412 3 7  mid
: 416 3 7 dle
- 420
: 420 3 6  of
: 424 3 2  a
: 428 3 4  des
: 432 3 4 pe
: 436 3 6 rate
: 440 3 4  fight
: 444 4 2 ~,
- 450
: 453 3 2 and
: 457 3 4  she
: 461 3 6  could
: 465 2 6 n't
: 469 5 4  find
: 476 3 2  how
: 480 3 1  to
: 485 5 -1  push
: 493 11 4  through.
- 506
: 2092 12 2 Four
: 2107 3 2  a.
: 2111 3 4 m.
: 2115 3 2  in
: 2119 3 1  the
* 2123 14 -1  mor
: 2139 6 -1 ning,
- 2147
: 2155 3 1 car
: 2159 3 1 ried
: 2163 3 2  a
: 2167 7 4 way
: 2179 3 2  by
: 2183 2 4  a
: 2187 6 6  moon
: 2195 5 6 light
: 2203 3 4  sha
: 2207 4 -3 dow,
- 2213
: 2215 3 -3 I
: 2219 5 2  watched
: 2227 5 2  your
* 2235 3 2  v
* 2239 3 4 i
* 2243 3 2 si
* 2247 3 1 on
: 2251 8 -1  for
: 2268 7 -1 ming,
- 2277
: 2284 3 1 car
: 2288 3 1 ried
: 2292 3 2  a
: 2296 6 4 way
: 2308 3 2  by
: 2312 3 4  a
: 2316 6 6  moon
: 2324 5 6 light
: 2331 3 4  sha
: 2335 5 -3 dow,
- 2342
: 2347 5 6 Star
: 2355 4 9  was
: 2364 3 7  glo
: 2368 3 7 win'
: 2372 2 6  in
: 2376 2 2  a
: 2380 3 4  sil
: 2384 3 4 ve
: 2388 3 6 ry
: 2392 7 4  night,
- 2401
: 2412 9 6 far
: 2423 3 4  a
: 2427 7 2 way
: 2436 3 2  on
: 2440 3 -1  the
: 2444 3 4  o
: 2448 6 6 ther
: 2456 3 4  side
: 2460 4 2 ~,
- 2466
: 2468 3 2 Will
: 2472 3 4  you
: 2476 6 6  come
: 2484 6 9  to
: 2492 6 7  talk
: 2500 4 6  to
: 2505 8 4  me
: 2516 4 1  this
: 2524 4 -3  night,
- 2530
: 2532 3 -1 but
: 2536 3 1  she
: 2540 3 2  could
: 2544 2 2 n't
: 2548 3 1  find
: 2552 2 -1 ~
- 2555
: 2556 3 -1  how
: 2560 3 1  to
: 2564 6 2  push
: 2572 12 4  through.
E