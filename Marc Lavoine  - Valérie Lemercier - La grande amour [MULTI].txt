#TITLE:La grande amour
#ARTIST:Marc Lavoine - Valérie Lemercier
#LANGUAGE:French
#GENRE:Pop
#YEAR:2009
#MP3:Marc Lavoine - Valérie Lemercier - La grande amour.mp3
#COVER:Marc Lavoine - Valérie Lemercier - La grande amour.jpg
#VIDEO:Marc Lavoine - Valérie Lemercier - La grande amour.mp4
#BPM:312,81
#GAP:10087
#DUETSINGERP1:Marc Lavoine
#DUETSINGERP2:Valérie Lemercier
P 1
: 0 3 0 C'est
: 5 2 -3  la
: 10 10 -1  grande
: 22 10 7  a
: 34 18 4 mour
- 72
: 95 3 0 C'est
: 100 4 -3  ma
: 107 2 -1  vie
: 120 4 7  de
: 131 7 4  cha
: 143 4 0 que
: 155 13 -1  jour
- 178
: 192 4 0 C'est
: 197 2 -3  la
: 203 10 -1  grande
* 216 3 7  a
: 227 15 4 mour
- 262
: 287 4 0 Pas
: 293 2 -3  be
: 297 7 -1 soin
: 311 6 -4  de
: 322 10 -3  long
* 335 7 4  dis
: 348 15 2 cours
- 373
: 383 3 0 C'est
: 388 2 -1  la
: 394 11 4  grande
: 407 6 4  a
: 418 18 -3 mour
- 477
: 959 3 5 J'ai
: 965 3 5 me
: 971 7 5  la
: 981 11 5  Grande
: 995 19 5  Our
: 1019 9 4 se
- 1038
: 1055 4 -1 Et
: 1060 3 -1  la
: 1066 6 4  Grande
: 1076 7 4  Mu
: 1085 12 0 raille
- 1117
: 1150 4 5 Dans
: 1157 2 5  la
: 1162 10 5  gran
: 1176 7 5 de
: 1187 19 5  cour
: 1212 8 4 se
- 1231
: 1235 17 -1 Je
: 1259 11 0  tombe
: 1283 7 2  je
* 1294 4 7  dé
* 1300 14 4 raille
- 1324
: 1331 19 -1 Je
* 1356 7 0  vis
: 1366 3 -3  je
: 1373 4 -1  vaille
: 1378 30 -1 ~
- 1414
: 1416 2 -6 Que
: 1420 5 -6  vaille
: 1426 24 -1 ~
- 1460
: 1487 4 0 C'est
: 1493 1 -3  la
: 1498 11 -1  grande
: 1511 6 7  a
: 1522 22 4 mour
- 1554
: 1583 3 0 Le
: 1588 3 -3  grand
: 1595 5 -1  saut
- 1604
: 1606 7 7 Et
: 1617 6 4  le
: 1628 8 0  grand
: 1641 12 -1  soir
- 1694
: 1872 2 0 His
: 1877 4 0 sez
* 1882 4 4  la
* 1894 6 4  grand
* 1906 12 -3  voile
- 1938
: 1966 4 5 J'ai
: 1972 4 5 me
: 1978 8 5  la
: 1989 11 5  Grande
: 2004 18 5  Our
: 2027 10 4 se
- 2047
: 2062 3 -1 Le
: 2067 3 -1  grand
* 2075 4 4  sa
* 2085 5 4 mou
: 2093 15 0 raď
- 2128
: 2157 4 5 Dans
: 2163 3 5  la
: 2169 11 5  gran
: 2182 7 5 de
: 2194 20 5  cour
: 2219 10 4 se
- 2239
: 2243 18 -1 Je
: 2267 12 0  tombe
: 2290 10 2  je
* 2302 3 7  m'en
: 2309 15 4 taille
- 2335
: 2339 19 -1 Je
: 2363 7 0  vis
: 2374 2 -3  je
: 2379 5 -1  vaille
: 2385 12 -1 ~
- 2407
: 2423 2 -6 Que
: 2428 4 -6  vaille
: 2433 17 -1 ~
- 2491
* 2782 4 0 Pas
: 2788 3 -3  be
: 2793 9 -1 soin
: 2807 9 -4  de
: 2818 9 -3  long
: 2831 7 4  dis
: 2842 17 2 cours
- 2900
: 2973 5 5 J'ai
: 2980 4 5 me
: 2986 8 5  la
: 2997 11 5  Grande
: 3011 19 5  Our
: 3035 9 4 se
- 3054
: 3071 4 -1 Et
: 3076 2 -1  le
: 3082 9 4  grand
: 3094 2 4  Ver
: 3100 15 0 sailles
- 3135
: 3165 4 5 Dans
: 3171 2 5  la
: 3178 10 5  gran
: 3191 5 5 de
: 3203 19 5  cour
: 3227 9 4 se
- 3247
: 3251 17 -1 Je
: 3275 10 0  tombe
: 3298 7 2  je
: 3310 2 7  dé
: 3316 12 4 faille
- 3338
: 3346 18 -1 Je
: 3370 10 0  vis
: 3383 2 -3  je
: 3388 4 -1  vaille
: 3393 11 -1 ~
- 3414
: 3431 2 -6 Que
: 3436 5 -6  vaille
: 3442 22 -1 ~
- 3474
: 3503 3 0 C'est
: 3508 2 -3  la
: 3514 10 -1  grande
: 3526 8 7  a
: 3538 22 4 mour
- 3570
: 3597 2 0 Le
: 3604 4 -3  plus
: 3610 7 -1  grand
* 3623 5 7  des
* 3635 7 4  sen
* 3646 10 0 ti
* 3659 10 -1 ments
- 3710
: 3886 2 0 Dieu
: 3892 3 -3  que
: 3897 3 4  l'a
: 3903 4 4 mour
: 3909 6 4  est
: 3920 30 -3  grand
- 3991
: 4318 3 0 Pas
: 4324 2 -3  be
: 4329 5 -1 soin
: 4343 7 -4  de
: 4354 9 -3  long
: 4368 5 4  dis
: 4379 10 2 cours
- 4399
: 4414 4 0 C'est
: 4420 2 -3  la
: 4425 11 4  grande
: 4438 6 4  a
: 4449 23 -3 mour
- 4513
: 4798 3 0 Pas
: 4804 2 -3  be
: 4809 7 -1 soin
: 4823 6 -4  de
: 4834 6 -3  long
: 4852 3 4  dis
: 4858 15 2 cours
- 4883
: 4894 4 0 C'est
: 4900 2 -3  la
: 4904 11 4  grande
: 4917 6 4  a
: 4929 23 -3 mour
P 2
: 479 4 0 C'est
: 485 2 -3  la
: 490 10 -1  grande
* 503 4 7  a
: 514 24 4 mour
- 548
: 575 2 0 La
: 579 5 -3  grande
: 587 8 -1  roue
* 599 4 7  de
* 610 10 4  mon
* 624 4 0  des
* 635 9 -1 tin
- 654
: 671 4 0 C'est
: 677 2 -3  la
: 682 10 7  grande
* 695 6 7  a
: 706 28 4 mour
- 744
: 767 2 0 Plus
: 773 2 -3  fort
: 779 7 -1  que
: 790 6 -4  le
: 803 3 -3  grand
* 815 4 4  cha
* 825 10 2 grin
- 845
: 862 4 0 Don
: 868 2 0 ne
* 874 8 4  moi
: 887 6 4  la
: 898 18 -3  main
- 936
: 959 3 5 J'ai
: 965 3 5 me
: 971 7 5  la
: 981 11 5  Grande
: 995 19 5  Our
: 1019 9 4 se
- 1038
: 1055 4 -1 Et
: 1060 3 -1  la
: 1066 6 4  Grande
: 1076 7 4  Mu
: 1085 12 0 raille
- 1117
: 1150 4 5 Dans
: 1157 2 5  la
: 1162 10 5  gran
: 1176 7 5 de
: 1187 19 5  cour
: 1212 8 4 se
- 1231
: 1235 17 -1 Je
: 1259 11 0  tombe
: 1283 7 2  je
* 1294 4 7  dé
* 1300 14 4 raille
- 1324
: 1331 19 -1 Je
: 1356 7 0  vis
: 1366 3 -3  je
* 1373 4 -1  vaille
: 1378 30 -1 ~
- 1414
: 1416 2 -6 Que
: 1420 5 -6  vaille
: 1426 24 -1 ~
- 1491
: 1606 7 7 Et
: 1617 6 4  le
: 1628 8 0  grand
: 1641 12 -1  soir
- 1663
: 1679 4 0 C'est
: 1685 2 -3  la
: 1690 11 7  grande
: 1703 6 7  a
: 1714 20 4 mour
- 1744
: 1773 4 0 Sur
: 1780 2 -3  la
: 1785 4 -1  gran
: 1798 6 -4 de
: 1810 8 -3  ba
* 1822 3 4 lan
: 1834 11 2 çoire
- 1886
: 1966 4 5 J'ai
: 1972 4 5 me
: 1978 8 5  la
: 1989 11 5  Grande
: 2004 18 5  Our
: 2027 10 4 se
- 2047
: 2062 3 -1 Le
: 2067 3 -1  grand
* 2075 4 4  sa
* 2085 5 4 mou
: 2093 15 0 raď
- 2128
: 2157 4 5 Dans
: 2163 3 5  la
: 2169 11 5  gran
: 2182 7 5 de
: 2194 20 5  cour
: 2219 10 4 se
- 2239
: 2243 18 -1 Je
: 2267 12 0  tombe
: 2290 10 2  je
* 2302 3 7  m'en
: 2309 15 4 taille
- 2335
: 2339 19 -1 Je
: 2363 7 0  vis
* 2374 2 -3  je
: 2379 5 -1  vaille
: 2385 12 -1 ~
- 2407
: 2423 2 -6 Que
: 2428 4 -6  vaille
: 2433 17 -1 ~
- 2491
: 2878 5 0 C'est
: 2885 2 0  la
: 2889 9 4  grande
: 2903 7 4  a
: 2913 22 -3 mour
- 2945
: 2973 5 5 J'ai
: 2980 4 5 me
: 2986 8 5  la
: 2997 11 5  Grande
: 3011 19 5  Our
: 3035 9 4 se
- 3054
: 3071 4 -1 Et
: 3076 2 -1  le
: 3082 9 4  grand
: 3094 2 4  Ver
: 3100 15 0 sailles
- 3135
: 3165 4 5 Dans
: 3171 2 5  la
: 3178 10 5  gran
: 3191 5 5 de
: 3203 19 5  cour
: 3227 9 4 se
- 3247
: 3251 17 -1 Je
: 3275 10 0  tombe
: 3298 7 2  je
: 3310 2 7  dé
: 3316 12 4 faille
- 3338
: 3346 18 -1 Je
: 3370 10 0  vis
: 3383 2 -3  je
: 3388 4 -1  vaille
: 3393 11 -1 ~
- 3414
: 3431 2 -6 Que
: 3436 5 -6  vaille
: 3442 22 -1 ~
- 3505
: 3695 4 0 C'est
: 3701 2 -3  la
: 3705 11 7  grande
: 3719 8 7  a
: 3731 21 4 mour
- 3762
: 3791 3 0 Tous
: 3796 4 -3  les
: 3803 4 -1  deux
: 3816 6 -4  sur
: 3826 4 -3  grand
: 3839 3 4  é
: 3851 6 2 cran
- 3867
: 3886 2 0 Dieu
: 3892 3 -3  que
: 3897 3 4  l'a
: 3903 4 4 mour
: 3909 6 4  est
: 3920 30 -3  grand
- 3991
: 4414 4 0 C'est
: 4420 2 -3  la
: 4425 11 4  grande
: 4438 6 4  a
: 4449 23 -3 mour
- 4513
: 4894 4 0 C'est
: 4900 2 -3  la
: 4904 11 4  grande
: 4917 6 4  a
: 4929 23 -3 mour
E
