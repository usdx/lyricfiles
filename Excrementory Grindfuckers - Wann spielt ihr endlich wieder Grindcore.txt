#TITLE:Wann spielt ihr endlich wieder Grindcore?
#ARTIST:Excrementory Grindfuckers
#LANGUAGE:Deutsch
#EDITION:[SC]-Songs
#MP3:Excrementory Grindfuckers - Wann spielt ihr endlich wieder Grindcore.mp3
#COVER:Excrementory Grindfuckers - Wann spielt ihr endlich wieder Grindcore [CO].jpg
#BACKGROUND:Excrementory Grindfuckers - Wann spielt ihr endlich wieder Grindcore [BG].jpg
#BPM:218,2
#GAP:5040
: 5 5 7 Einst
: 11 3 7  zo
: 15 3 7 gen
: 19 6 7  wir
: 26 5 7  Grind
: 32 3 7 fuc
: 35 3 7 kers
: 41 8 7  aus
: 50 2 5  zu
: 53 4 5  tö
: 58 9 4 ten,
- 69
: 93 2 4 mit
: 96 3 4  ei
: 99 3 4 nem
: 103 6 4  Sound,
: 111 3 0  un
: 115 5 9 sag
: 122 3 9 bar
: 126 8 9  hart
: 135 2 7  und
: 140 9 7  schnell.
- 151
: 174 4 7 Die
: 179 4 7  Knüp
: 183 4 7 pel-
: 188 5 7 Kon
: 193 4 7 kur
: 198 5 7 ren
: 203 4 7 ten
: 208 9 7  war'n
: 218 2 5  in
: 221 5 5  Nö
: 226 8 4 ten,
- 236
: 258 4 4 wir
: 263 3 4  klan
: 266 3 4 gen
: 270 3 4  un
: 274 6 0 schlag
: 281 6 7 bar
: 288 5 7  un
: 293 4 7 kom
: 297 5 -1 mer
: 302 15 0 ziell.
- 319
: 343 3 4 Wir
: 347 4 4  war'n
: 352 3 4  die
* 356 6 4  Grind
* 362 4 4 core-
: 367 5 4 Su
: 373 2 4 per
: 376 6 4 stars
- 384
: 386 3 4 und
: 389 3 4  hat
: 392 2 4 ten
: 396 7 4  Fans
: 405 4 4  im
: 410 4 4  Ü
: 415 2 4 ber
: 418 7 4 maß.
- 427
: 430 4 7 Nach
: 435 4 7  mehr
: 441 3 7  neu
: 444 2 7 em
: 448 8 7  Grind
: 458 3 7  habt
: 463 3 7  ihr
: 467 2 9  ge
: 470 14 9 fleht!
- 486
: 513 2 4 Doch
: 516 4 4  an
: 521 4 4  Grind
: 526 3 4 fuc
: 529 5 4 kers
: 536 6 4  nagt
: 542 2 4  die
: 545 7 4  Zeit,
- 553
: 555 2 4 es
: 558 5 4  bleibt
: 564 3 4  nur
: 568 3 4  Mit
: 571 4 4 tel
: 576 6 4 mä
: 583 3 4 ßig
: 587 5 4 keit.
- 592
: 592 2 4 Und
: 594 2 4  al
: 596 2 4 le
: 599 6 7  fra
: 605 5 7 gen,
: 614 4 4  wie's
: 618 3 5  um
: 621 4 5  Ex
: 625 3 7 cre
: 628 2 5 men
: 630 4 4 to
: 634 3 2 ry
: 639 15 0  steht:
- 657
: 682 5 12 Wann
: 688 11 12  spielt
: 700 3 12  ihr
: 703 5 11  end
: 709 5 11 lich
: 715 4 9  wie
: 719 3 9 der
* 723 7 7  Grind
* 731 10 0 core?
- 743
: 763 5 4 Ein
* 769 6 4  Grind
* 775 4 4 core
: 780 3 4  wie
: 783 2 0  er
: 788 6 9  frü
: 794 4 9 her
: 799 3 9  ein
: 803 4 7 mal
: 810 11 7  war?
- 823
F 836 12 7 Ja,
: 849 5 5  mit
: 855 10 4  Blast
: 865 5 4 beats
: 871 2 4  und
: 874 3 4  Ge
: 878 6 7 knüp
: 885 4 7 pel
: 890 3 7  oh
: 893 3 9 ne
: 897 7 4  En
: 904 10 0 de
- 916
: 929 3 0 und
: 933 3 0  nicht
: 937 4 0  so'n
: 942 5 5  Schla
: 948 5 5 ger-
: 954 3 5 Pim
: 957 2 5 pel-
: 961 7 2 Scheiß
- 969
: 969 2 2 wie
: 972 2 2  im
: 975 4 0  letz
: 980 2 -1 ten
: 983 9 0  Jahr!
- 994
: 1024 2 7 Ihr
: 1026 4 7  liebt
: 1032 3 7  Stak
: 1035 5 7 ka
: 1040 4 7 to-
: 1045 4 7 Riffs
: 1052 3 5  und
: 1057 7 5  Him's
: 1066 1 5  Ge
: 1067 7 5 grun
: 1074 12 4 ze,
- 1088
: 1105 3 4 doch
: 1109 4 4  uns
: 1114 2 4  in
: 1116 4 4 t'res
: 1122 4 0 siert
: 1127 6 9  nur
: 1134 3 9  das
: 1139 4 9  schnel
: 1144 3 7 le
: 1148 11 7  Geld.
- 1161
: 1187 3 7 D´rum
: 1191 3 7  stie
: 1194 4 7 gen
: 1199 3 7  wir
: 1204 4 7  um
: 1209 5 7  auf
: 1215 5 7  Schla
: 1221 5 7 ger-
: 1228 3 5 Ver
: 1231 5 5 hun
: 1237 12 4 ze,
- 1251
: 1267 4 4 weil
: 1272 4 4  uns
: 1277 3 4  der
: 1281 4 4  Main
: 1286 4 0 stream-
: 1291 6 7 Sta
: 1297 4 7 tus
: 1302 5 7  gut
: 1308 2 -1  ge
: 1311 11 0 fällt!
- 1324
: 1348 5 4 Grind
: 1354 3 4 fuc
: 1357 4 4 kers
: 1362 4 4  sind
: 1367 5 4  nun
: 1373 6 4  aus
: 1379 3 4 ge
: 1382 7 4 whimpt,
- 1391
: 1392 2 4 h--rt
: 1394 3 4  mitt
: 1398 3 4 ler
: 1402 5 4 wei
: 1407 3 4 le
: 1411 8 5  je
: 1420 3 4 des
: 1424 5 4  Kind.
- 1431
: 1433 2 7 Auf
: 1436 4 7  VI
: 1440 4 7 VA
: 1445 5 7  läuft
: 1451 4 7  jetzt
: 1457 4 7  un
: 1461 3 7 ser
: 1465 5 7  Schla
: 1471 4 9 ger-
: 1476 12 9 Brei.
- 1490
: 1514 3 4 Wir
: 1518 3 4  bie
: 1521 3 4 dern
: 1526 4 4  uns
: 1531 6 4  halt
: 1538 3 4  all
: 1541 2 4 em
: 1544 5 4  an
- 1551
: 1556 2 4 und
: 1559 3 4  ver
: 1563 4 4 kau
: 1568 3 4 fen
: 1574 2 4  an
: 1577 6 5  Je
: 1584 3 4 der
: 1587 6 4 mann.
- 1594
: 1596 2 7 Die
: 1599 7 7  Fans
- 1607
: 1608 5 2 sehn´n
: 1614 2 2  sich
: 1617 2 4  die
: 1620 3 5  al
: 1623 4 7 ten
: 1629 4 5  Zei
: 1633 2 4 ten
: 1636 2 2  her
: 1639 13 0 bei.
- 1655
: 1681 4 12 Wann
: 1686 9 12  spielt
: 1696 4 12  ihr
: 1701 5 11  end
: 1706 4 11 lich
: 1711 4 9  wie
: 1715 4 9 der
* 1720 7 7  Grind
* 1727 11 0 core?
- 1740
: 1747 3 4 Ein
* 1750 4 4  Grind
* 1754 4 4 core
: 1758 3 4  wie
: 1761 2 0  er
: 1764 3 9  frü
: 1767 3 9 her
: 1770 3 9  ein
: 1773 3 7 mal
: 1777 14 7  war?
- 1793
F 1795 7 7 Ja,
: 1803 3 5  mit
: 1806 3 4  Blast
: 1809 3 4 beats
: 1812 2 4  und
: 1815 3 4  Ge
: 1818 2 7 knüp
: 1820 3 7 pel
: 1823 4 7  oh
: 1827 3 4 ne
: 1830 5 2  En
: 1835 5 0 de
- 1842
: 1849 3 0 und
: 1852 2 0  nicht
: 1854 2 0  so´n
: 1857 4 5  Schla
: 1861 4 5 ger-
: 1866 3 5 Pim
: 1869 2 4 pel-
: 1872 4 2 Scheiß
- 1877
: 1877 2 2  wie
: 1879 2 2  im
: 1881 3 0  letz
: 1884 3 -1 ten
: 1887 12 0  Jahr!
- 1901
: 1912 3 7 Heut´
: 1916 2 7  la
: 1918 2 7 gern
: 1921 4 7  wir
: 1925 2 7  im
: 1928 2 7  Kel
: 1930 3 7 ler
: 1934 2 7  die
: 1937 3 5  Mil
: 1940 5 5 lio
: 1945 8 4 nen
- 1955
: 1966 2 4 und
: 1969 3 4  bas
: 1972 3 4 teln
: 1976 2 4  nur
: 1978 3 0  noch
: 1982 3 9  sel
: 1985 3 9 ten
: 1989 2 9  an
: 1991 2 7  den
: 1994 13 7  Songs.
- 2009
: 2017 3 7 Wir
: 2020 3 7  müs
: 2023 3 7 sen
: 2026 3 7  uns
: 2029 3 7  ja
- 2033
: 2033 3 7  schließ
: 2036 3 7 lich
: 2040 3 7  auch
: 2043 3 5  mal
: 2048 5 5  scho
: 2053 7 4 nen!
- 2062
: 2072 2 4 Für
: 2074 2 4  uns
: 2077 3 4  ar
: 2080 3 4 bei
: 2083 2 0 ten
: 2086 3 7  gro
: 2089 2 7 ße
: 2092 3 7  Ak
: 2096 4 -1 tien
: 2100 11 0 fonds.
- 2113
: 2123 2 4 Ge
: 2125 2 4 nie
: 2127 2 4 ßen
: 2130 5 4  Wein
: 2136 2 4  im
: 2139 2 4  Swim
: 2141 2 4 ming-
: 2143 6 4 Pool,
- 2150
: 2150 2 4 für
* 2152 3 4  Grind
* 2155 3 4 core
: 2158 3 4  sind
: 2162 2 4  wir
: 2164 3 5  eh
: 2167 2 4  zu
: 2169 4 4  cool!
- 2174
: 2175 3 7 Scheiß
: 2179 6 7  drauf,
- 2186
: 2186 2 7  ich
: 2188 2 7  hab
: 2190 2 7  mein
: 2192 2 7  Kon
: 2194 3 7 to
: 2198 1 7  in
: 2200 3 9  der
: 2204 12 9  Schweiz!
- 2218
: 2229 2 4 Die
: 2231 3 4  Fans
: 2235 2 4  fra
: 2237 2 4 gen
: 2240 2 4  sich:
: 2244 4 4  "War´s
: 2248 1 4  das
: 2249 5 4  schon?"
- 2255
: 2256 2 4 Und
: 2258 2 4  wer
: 2261 2 4  be
: 2263 3 4 steigt
* 2267 2 4  den
* 2271 2 4  Grind
: 2273 2 4 core-
: 2275 4 4 Thron?
- 2280
: 2281 3 7  Wir
: 2284 6 7  nicht!
- 2292
: 2294 1 5  Und
: 2295 2 5  es
: 2297 3 7  tut
: 2300 3 5  uns
: 2304 3 4  wirk
: 2307 5 2 lich
: 2312 11 0  Leid!
- 2325
: 2336 2 12 Wir
: 2340 5 12  spie
: 2345 3 12 len
: 2349 3 11  nie
: 2353 2 11  mehr
: 2355 2 9  wie
: 2357 4 9 der
* 2362 4 7  Grind
* 2367 9 0 core!
- 2378
: 2386 2 4 Der
* 2389 3 4  Grind
* 2393 3 4 core
: 2396 2 4  ist
: 2399 2 0  nicht
: 2402 3 9  mehr
: 2405 4 9  das
: 2409 3 9  was
: 2412 3 7  er
: 2416 13 7  war!
- 2431
: 2434 7 7 Ja,
: 2442 4 5  Grind
: 2446 4 4 fuc
: 2450 3 4 kers
: 2454 3 4  sind
: 2457 2 4  Kom
: 2459 5 7 merz
- 2464
: 2464 3 7  und
: 2467 2 7  wol
: 2469 2 9 len
: 2473 5 4  Kne
: 2479 6 0 te
- 2487
: 2493 1 0 und
: 2495 2 0  brau
: 2498 2 0 chen
: 2500 4 5  nicht
- 2504
: 2504 2 5  die
: 2507 3 5  gan
: 2511 3 4 zen
: 2515 2 2  Fans
: 2518 3 2  vom
: 2522 3 0  letz
: 2525 3 -1 ten
: 2529 13 0  Jahr!
- 2544
: 2546 2 0 ...und
: 2549 2 0  brau
: 2552 3 0 chen
: 2555 5 5  nicht
- 2560
: 2560 2 5  die
: 2563 2 5  gan
: 2566 2 4 zen
: 2569 4 2  Fans
: 2574 4 2  vom
: 2579 3 0  letz
: 2583 2 -1 ten
: 2586 13 0 Jahr!
E
