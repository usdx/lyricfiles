#TITLE:I Don't Like It, I Love It
#ARTIST:Flo Rida feat. Robin Thicke & Verdine White
#MP3:Flo Rida feat. Robin Thicke & Verdine White - I Don't Like It, I Love It.mp3
#VIDEO:Flo Rida feat. Robin Thicke & Verdine White - I Don't Like It, I Love It.mp4
#COVER:Flo Rida feat. Robin Thicke & Verdine White - I Don't Like It, I Love It.jpg
#BPM:236
#GAP:7583
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Hip-Hop & Rap
#EDITION:Songs von aac
#AUTHOR:Nicolai (aac)
: 0 1 -10 I
: 2 4 -7  don't
: 8 4 -7  like
: 14 2 -10  it,
- 18
: 36 2 -12 I
: 40 2 -3  love
: 44 1 -3  it,
: 46 2 -5  love
: 50 1 -5  it,
: 52 4 -7  love
: 60 2 -10  it,
: 68 1 -5  uh
: 70 6 -8  oh
- 78
: 100 2 -10 So
: 104 3 -3  good
: 110 3 -5  it
* 116 8 -7  hurts
- 126
: 128 1 -10 I
: 130 4 -7  don't
: 136 4 -7  want
: 142 2 -10  it,
- 146
: 164 2 -12 I
: 168 2 -3  got
: 172 1 -3 ta,
: 174 2 -5  got
: 178 1 -5 ta
: 180 4 -7  have
: 188 2 -10  it,
: 196 1 -5  uh
: 198 6 -8  oh
- 206
: 230 1 -10 When
: 232 2 -7  I
: 236 2 -5  can
: 240 2 -5  finds
: 244 1 -7  the
: 246 4 -3  words
: 252 2 -5  I
: 255 2 -7  just
: 259 4 -5  go
- 265
: 320 1 -3 I
: 322 4 0  don't
* 328 4 2  like
: 334 2 0  it,
- 338
: 348 3 -3 No
: 352 2 -5 ~,
: 356 2 -3  I
: 360 4 -5  love
: 366 2 -7  it
- 370
: 448 1 -3 I
: 450 4 0  don't
: 456 4 2  like
: 462 2 0  it,
- 466
: 476 3 -3 No
: 480 2 -5 ~,
: 484 2 -3  I
* 488 4 -5  love
: 494 2 -7  it
- 498
: 520 4 -3 All
: 528 4 -3  out,
: 536 2 -3  turn
: 540 1 -3  the
: 542 2 -3  beat
: 546 2 -10  up
- 550
: 556 2 -3 Hey
: 560 2 -3  now
: 564 2 -3  I'm
: 568 2 -3  glad
: 572 1 -3  to
: 574 2 -3  meet
: 578 2 -10  ya
- 582
: 588 2 -3 Turn
: 592 2 -3  up
: 596 2 -3  girl,
: 600 2 -3  blow
: 604 1 -3  the
: 606 2 -3  spea
: 610 2 -10 ker
- 613
: 614 1 -3 Yeah
: 616 2 -3  up,
: 620 1 -3  think
: 622 1 -3  a
* 624 1 -10 bout
* 626 1 -10  it
* 628 2 -10  now,
: 632 2 -5  blow
: 636 1 -3  the
: 638 2 -3  spea
: 642 2 -10 ker
- 646
: 648 2 -5 I'll
: 652 2 -3  speak
: 656 2 -10  lou
: 660 1 -10 der,
: 662 2 -5  let's
: 666 2 -3  get
: 670 2 -10  wild
: 674 1 -10  to
: 676 2 -10 night
- 679
: 680 1 -5 Bil
: 682 1 -3 lio
: 684 2 -3 naire
: 688 2 -10  bot
: 692 1 -10 tles,
: 694 2 -5  we
: 698 2 -3  just
: 702 2 -10  down
: 706 1 -10  em
: 708 2 -10  like
- 711
: 712 2 -5 Ain't
: 716 2 -3  no
: 720 2 -10  pro
: 724 1 -10 blem,
: 726 2 -5  all
: 730 2 -3  my
: 734 2 -10  roads
: 738 1 -10  are
: 740 2 -10  right
- 744
: 748 2 -5 All
: 752 2 -3  right,
: 756 2 -4  all
: 760 4 -3  right
- 766
: 768 1 -10 I
: 770 2 -7  don't
* 776 4 -7  like
: 782 2 -10  it,
: 788 2 -3  I
: 792 4 -10  love
: 798 2 -10  it
- 801
: 802 1 -10 I
: 804 1 -10  got
: 806 1 -10  a
: 808 1 -7 no
: 810 1 -7 ther
: 812 1 -7  co
: 814 1 -7 min'
: 816 2 -7  in
: 820 2 -3  my
: 824 4 -10  bud
: 830 2 -10 get
- 833
: 834 1 -10 I
: 836 1 -10  got
: 838 1 -10  a
: 840 1 -7  a
: 842 1 -7 na
: 844 1 -7 con
: 846 1 -7 da
: 848 2 -7  in
: 852 2 -3  my
: 856 4 -10  truck
: 862 2 -10  fit
- 866
: 868 2 -3 Don't
: 872 4 -10  push
: 878 2 -10  it,
: 884 2 -3  don't
: 888 4 -10  push
: 894 2 -10  it
- 897
: 898 1 -3 Cause
: 900 1 -3  I'
: 902 1 -3 ma
: 904 1 -3  hit
: 906 1 -3  it
: 908 1 -3  til
: 910 1 -3  I
: 912 2 -3  jack
* 916 2 -10 pot,
: 924 2 -3  that's
: 928 2 -10  right
- 931
: 932 2 -3 Wax
: 936 2 -3  on
: 940 1 -3  ba
: 942 1 -3 by
: 944 2 -3  wax
: 948 2 -10  off,
: 956 2 -3  act
: 960 2 -10  right
- 963
: 964 1 -3 You
: 966 1 -3  can
: 968 1 -3  put
: 970 1 -3  it
: 972 1 -3  on
: 974 1 -3  the
: 976 2 -3  black
: 980 2 -10  card,
: 988 2 -3  all
: 992 2 -10  night
- 994
: 994 1 -10 And
: 996 2 -10  I'll
: 1000 2 -5  spend
: 1004 2 -3  it,
: 1008 2 -10  I'll
: 1012 2 -5  spend
: 1016 2 -3  it
- 1020
: 1024 1 -10 I
: 1026 4 -7  don't
* 1032 4 -7  like
: 1038 2 -10  it,
- 1042
: 1060 2 -12 I
: 1064 2 -3  love
: 1068 1 -3  it,
: 1070 2 -5  love
: 1074 1 -5  it,
: 1076 4 -7  love
: 1084 2 -10  it,
: 1092 1 -5  uh
: 1094 6 -8  oh
- 1102
: 1124 2 -10 So
: 1128 3 -3  good
: 1134 3 -5  it
: 1140 8 -7  hurts
- 1150
: 1152 1 -10 I
: 1154 4 -7  don't
: 1160 4 -7  want
: 1166 2 -10  it,
- 1170
: 1188 2 -12 I
* 1192 2 -3  got
* 1196 1 -3 ta,
: 1198 2 -5  got
: 1202 1 -5 ta
: 1204 4 -7  have
: 1212 2 -10  it,
: 1220 1 -5  uh
: 1222 6 -8  oh
- 1230
: 1254 1 -10 When
: 1256 2 -7  I
: 1260 2 -5  can
: 1264 2 -5  finds
: 1268 1 -7  the
: 1270 4 -3  words
: 1276 2 -5  I
: 1279 2 -7  just
: 1283 4 -5  go
- 1289
: 1344 1 -3 I
: 1346 4 0  don't
* 1352 4 2  like
* 1358 2 0  it,
- 1362
: 1372 3 -3 No
: 1376 2 -5 ~,
: 1380 2 -3  I
: 1384 4 -5  love
: 1390 2 -7  it
- 1394
: 1472 1 -3 I
: 1474 4 0  don't
: 1480 4 2  like
: 1486 2 0  it,
- 1490
: 1500 3 -3 No
: 1504 2 -5 ~,
* 1508 2 -3  I
: 1512 4 -5  love
: 1518 2 -7  it
- 1522
: 1544 4 -3 All
: 1552 4 -3  night,
: 1560 2 -3  let
: 1564 1 -3  me
: 1566 2 -3  group
: 1570 4 -10  ya
- 1576
: 1580 2 -3 Dance
: 1584 1 -3  with
: 1586 1 -3  me,
: 1588 2 -3  turn
: 1592 2 -3  down
: 1596 1 -3  for
: 1598 2 -3  who
: 1602 4 -10  girl
- 1608
: 1610 1 -3 A
: 1612 1 -3 no
: 1614 1 -3 ther
: 1616 1 -3  run
: 1618 1 -3 ner
: 1620 1 -3  help
: 1622 1 -3  us
: 1624 2 -3  step
: 1628 1 -3  the
: 1630 2 -3  moves
: 1634 2 -10  up
- 1637
: 1638 2 -3 Yeah,
: 1642 1 -3  bet
: 1644 2 -3  that
: 1648 2 -3  round
: 1652 1 -3  need
: 1654 1 -3  a
: 1656 1 -3  mea
: 1658 1 -3 sure
: 1660 1 -3  or
: 1662 2 -3  ru
* 1666 2 -10 ler
- 1670
: 1672 1 -5 Ce
: 1674 1 -3 le
: 1676 2 -3 brate
: 1680 2 -10  life
: 1684 1 -10  and
: 1686 2 -5  I'll
: 1690 2 -3  pay
: 1694 1 -10  for
: 1696 2 -10  it
- 1699
: 1700 1 -10 That
: 1702 1 -10  Ca
: 1704 1 -5 va
: 1706 1 -3 lli
: 1708 2 -3  nice
: 1712 2 -10  next
: 1716 1 -10  to
: 1718 2 -5  my
: 1722 2 -3  Tom
: 1726 2 -10  Ford
- 1730
: 1732 2 -10 Yeah
: 1736 1 -5  par
: 1738 1 -3 ty
: 1740 2 -3  all
: 1744 2 -10  night,
: 1748 1 -10  let's
: 1750 2 -5  all
: 1754 2 -3  a
: 1758 2 -10 board
- 1762
: 1764 4 -10 Let's
* 1772 1 -5  all
* 1774 1 -3  a
* 1776 2 -5 board,
: 1780 1 -5  all
: 1782 1 -3  a
: 1784 4 -3 board
- 1790
: 1792 1 -10 I
: 1794 2 -7  don't
: 1800 4 -7  like
: 1806 2 -10  it,
: 1812 2 -3  I
: 1816 4 -10  love
: 1822 2 -10  it
- 1826
: 1828 1 -10 And
: 1830 1 -10  them
: 1832 1 -7  o
: 1834 1 -7 ther
: 1836 2 -7  girl
: 1840 2 -7  they
: 1844 2 -3  can't
: 1848 4 -10  touch
: 1854 2 -10  it
- 1858
: 1860 1 -10 Com
: 1862 1 -10 pe
: 1864 1 -7 ti
: 1866 1 -7 tion,
: 1868 1 -7  that's
: 1870 1 -7  a
: 1872 2 -7  whole
: 1876 1 -7  no
: 1878 1 -3 ther
: 1880 4 -10  sub
: 1886 2 -10 ject
- 1889
: 1890 1 -10 I
: 1892 1 -5  wan
: 1894 1 -3 na
: 1896 2 -10  walk
: 1900 1 -10  it
: 1902 2 -10  out
: 1908 2 -3  in
: 1912 4 -10  pu
: 1918 2 -10 blic
- 1922
: 1924 1 -3 You
: 1926 1 -3  a
: 1928 2 -3  star
: 1932 1 -3  ba
: 1934 1 -3 by,
: 1936 2 -3  just
* 1940 2 -10  know,
: 1948 2 -3  let's
: 1952 2 -10  go
- 1955
: 1956 1 -3 To
: 1958 1 -3  the
: 1960 1 -3  man
: 1962 1 -3 sion
: 1964 1 -3  or
: 1966 1 -3  the
: 1968 2 -3  con
: 1972 2 -10 do,
: 1980 2 -3  let's
: 1984 2 -10  go
- 1987
: 1988 1 -3 Per
: 1990 1 -3 fect
: 1992 2 -3  time
: 1996 1 -3  got
: 1998 1 -3 ta
: 2000 1 -3  let
: 2002 1 -3  it
: 2004 2 -10  flow,
: 2012 2 -3  you
: 2016 2 -10  know
- 2019
: 2020 2 -10 I'm
: 2024 2 -5  wat
: 2028 2 -3 ching,
: 2032 2 -10  I'm
: 2036 2 -5  wat
: 2040 2 -3 ching,
- 2044
: 2048 1 -10 I
: 2050 4 -7  don't
: 2056 4 -7  like
: 2062 2 -10  it,
- 2066
: 2084 2 -12 I
: 2088 2 -3  love
: 2092 1 -3  it,
: 2094 2 -5  love
: 2098 1 -5  it,
: 2100 4 -7  love
: 2108 2 -10  it,
: 2116 1 -5  uh
: 2118 6 -8  oh
- 2126
: 2148 2 -10 So
: 2152 3 -3  good
: 2158 3 -5  it
: 2164 8 -7  hurts
- 2174
: 2176 1 -10 I
: 2178 4 -7  don't
: 2184 4 -7  want
: 2190 2 -10  it,
- 2194
: 2212 2 -12 I
: 2216 2 -3  got
: 2220 1 -3 ta,
: 2222 2 -5  got
: 2226 1 -5 ta
* 2228 4 -7  have
: 2236 2 -10  it,
: 2244 1 -5  uh
: 2246 6 -8  oh
- 2254
: 2278 1 -10 When
: 2280 2 -7  I
: 2284 2 -5  can
: 2288 2 -5  finds
: 2292 1 -7  the
: 2294 4 -3  words
: 2300 2 -5  I
: 2303 2 -7  just
: 2307 4 -5  go
- 2313
: 2368 1 -3 I
: 2370 4 0  don't
: 2376 4 2  like
: 2382 2 0  it,
- 2386
: 2396 3 -3 No
: 2400 2 -5 ~,
: 2404 2 -3  I
: 2408 4 -5  love
: 2414 2 -7  it
- 2418
: 2496 1 -3 I
: 2498 4 0  don't
: 2504 4 2  like
: 2510 2 0  it,
- 2514
: 2524 3 -3 No
: 2528 2 -5 ~,
: 2532 2 -3  I
: 2536 4 -5  love
: 2542 2 -7  it
- 2546
: 2568 2 -5 Meet
: 2572 2 -7  me
: 2576 2 -5  at
: 2580 2 -7  the
: 2584 2 -5  stu
: 2588 1 -3 di
: 2590 4 -3 o
- 2596
: 2600 1 -5 Ban
: 2602 1 -7 ga
: 2604 2 -5 rang
: 2608 2 -5  just
: 2612 2 -7  like
: 2616 2 -5  Ru
: 2620 1 -3 ffi
: 2622 4 -3 o
- 2628
: 2636 1 -5 Feel
: 2638 1 -7  the
: 2640 2 -5  base,
: 2644 1 -5  let
: 2646 1 -7  ya
: 2648 2 -5  boo
: 2652 1 -3 ty
* 2654 4 -3  go
- 2659
: 2660 2 0 I
: 2664 2 0  wan
: 2668 2 0 na
: 2672 1 0  get
: 2674 2 0  in
: 2678 4 2 side
: 2684 2 -3  it
- 2688
: 2700 1 -5 Run
: 2702 1 -7  a
: 2704 2 -5 way
: 2708 1 -5  for
: 2710 1 -7  a
: 2712 2 -5  few
: 2716 1 -3 ~
: 2718 4 -3  days
- 2724
: 2728 1 -5 Thinkin'
: 2730 1 -7 ~
: 2732 2 -5  bout
: 2736 2 -5  love,
: 2740 1 -5  baby
: 2742 1 -7 ~
: 2744 1 -5  tou
: 2746 2 -3 ~
: 2750 2 -3 che
- 2754
: 2764 2 -5 Tied
: 2768 2 -5  up
: 2772 1 -5  like
: 2774 1 -7  a
: 2776 1 -5  shoe
: 2778 2 -3 ~
: 2782 2 -3  lace
- 2785
: 2786 1 -7 I
: 2788 2 -5  don't
: 2792 4 -5  like
: 2798 2 -3  it,
: 2802 1 -7  I
: 2804 2 -5  don't
: 2808 4 -5  like
: 2814 2 -3  it
- 2818
: 2880 1 -3 I
: 2882 4 0  don't
: 2888 4 2  like
: 2894 2 0  it,
- 2898
: 2908 3 -3 No
: 2912 2 -5 ~,
: 2916 2 -3  I
: 2920 4 -5  love
: 2926 2 -7  it
- 2930
: 3008 1 -3 I
: 3010 4 0  don't
: 3016 4 2  like
: 3022 2 0  it,
- 3026
: 3036 3 -3 No
: 3040 2 -5 ~,
: 3044 2 -3  I
: 3048 4 -5  love
: 3054 2 -7  it
- 3058
: 3136 1 -3 I
: 3138 4 0  don't
: 3144 4 2  like
: 3150 2 0  it,
- 3154
: 3164 3 -3 No
: 3168 2 -5 ~,
: 3172 2 -3  I
: 3176 4 -5  love
: 3182 2 -7  it
- 3186
: 3264 1 -3 I
: 3266 4 0  don't
* 3272 4 2  like
* 3278 2 0  it,
- 3282
: 3292 3 -3 No
: 3296 2 -5 ~,
: 3300 2 -3  I
: 3304 4 -5  love
: 3310 2 -7  it
- 3314
: 3316 2 0 I
: 3320 3 0  love
: 3324 1 -2 ~
: 3326 2 -3 ~
: 3330 6 -3  it
E