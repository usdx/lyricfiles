#TITLE:Master and servant
#ARTIST:Depeche Mode
#MP3:Depeche Mode - Master and servant.mp3
#VIDEO:Depeche Mode - Master and servant.avi
#COVER:Depeche Mode - Master and servant [CO].jpg
#BACKGROUND:Depeche Mode - Master and servant [BG].jpg
#BPM:253,2
#GAP:31170
#ENCODING:UTF8
#EDITION:UltraStar Depeche Mode
: 0 2 10 There's
: 4 2 10  a
: 8 2 8  new
: 12 8 10  game
- 20
: 20 2 -2 We
: 24 2 -2  like
: 28 2 -2  to
: 32 3 3  play
: 37 4 5  you
: 44 9 1  see
- 55
: 60 2 8 A
: 64 5 10  game
: 72 4 10  with
: 80 3 10  ad
: 84 4 5 ded
- 90
: 92 3 5 re
* 96 3 8 a
: 100 3 5 ~
: 104 2 3 li
: 108 10 5 ty
- 120
: 124 2 5 You
: 128 2 10  treat
: 132 3 10  me
: 136 2 10  like
: 140 2 8  a
: 144 6 10  dog
- 151
: 152 2 -2 Get
: 156 2 -2  me
: 160 3 3  down
: 164 3 3  on
: 168 2 5  my
* 172 8 6  knees
- 182
: 200 5 10 We
: 208 3 8  call
: 212 5 10  it
- 219
: 224 4 3 mas
: 231 3 3 ter
: 236 2 5  and
: 240 3 1  ser
: 244 4 3 vant
- 250
: 264 5 10 We
: 272 3 8  call
: 276 4 10  it
- 282
: 288 4 3 mas
: 295 3 3 ter
* 300 2 5  and
: 304 3 1  ser
: 308 4 3 vant
- 314
: 440 2 8 It's
: 444 2 8  a
: 448 3 10  lot
: 452 3 10  like
: 456 2 8 ~
: 460 9 10  life
- 471
: 476 2 -2 This
: 480 3 3  play
: 484 2 3  bet
: 488 3 5 ween
: 492 2 5  the
: 496 5 1  sheets
- 503
: 508 2 5 With
: 512 3 10  you
: 516 3 10  on
: 520 2 8  the
* 524 9 10  top
- 535
: 540 3 5 and
: 544 2 8  me
: 548 2 5  un
: 552 2 3 der
: 556 9 5 neath
- 567
: 572 2 5 For
: 576 3 10 get
: 580 3 10  all
: 584 2 8  a
: 588 8 10 bout
- 598
: 604 2 -2 e
: 608 3 3 qua
: 612 5 5 li
* 620 10 6 ty
- 632
: 648 4 10 Let's
: 656 6 10  play
- 664
* 672 4 3 mas
: 679 3 3 ter
: 684 2 5  and
: 688 3 1  ser
: 692 4 3 vant
- 698
: 712 4 10 Let's
: 720 6 10  play
- 728
: 736 4 3 mas
: 743 3 3 ter
: 748 2 5  and
: 752 3 1  ser
: 756 4 3 vant
- 762
: 824 2 3 It's
: 828 2 1  a
: 832 2 3  lot
: 836 3 3  like
: 840 2 1 ~
* 844 8 3  life
- 854
: 860 2 1 And
: 864 4 3  that's
: 872 2 3  what's
: 876 2 5  ap
: 880 3 1 pea
: 884 3 0 ling
- 888
: 888 2 3 If
: 892 3 1  you
: 896 2 3 ~
: 900 4 3  des
: 908 8 3 pise
- 918
: 924 2 1 that
: 928 3 3  throw
: 932 2 3 aw
: 936 5 5 ay
: 944 3 1  feel
: 948 3 0 ing
- 952
: 952 2 -2 From
: 956 2 -4  dis
: 960 3 -2 pos
: 964 2 -2 ab
: 968 2 -2 le
* 972 8 1  fun
- 982
: 1020 2 1 Then
: 1024 2 0  this
: 1028 2 0  is
: 1032 2 0  the
: 1036 13 3  one
- 1051
: 1224 3 10 Do
: 1228 2 8 mi
: 1232 4 10 na
: 1239 3 -2 tion's
- 1243
: 1244 2 -2 the
: 1248 3 3  name
: 1252 2 3  of
: 1256 2 5  the
: 1260 7 1  game
- 1269
: 1276 2 5 In
: 1280 3 10  bed
: 1284 2 10  or
: 1288 2 8  in
: 1292 9 10  life
- 1303
: 1308 2 5 They're
: 1312 3 8  both
: 1316 2 5  just
: 1320 2 3  the
: 1324 7 5  same
- 1332
: 1332 2 5 Ex
: 1336 2 5 cept
: 1340 2 5  in
: 1344 2 10  one
: 1348 2 10  you're
: 1352 3 8  ful
: 1356 6 10 filled
- 1364
: 1368 2 -2 At
: 1372 2 -2  the
: 1376 3 3  end
: 1380 2 3  of
: 1384 2 5  the
* 1388 10 6  day
- 1400
: 1416 4 10 Let's
: 1424 5 10  play
- 1431
: 1440 4 3 mas
: 1447 3 3 ter
: 1452 2 5  and
: 1456 3 1  ser
: 1460 4 3 vant
- 1466
: 1480 4 10 Let's
: 1488 5 10  play
- 1495
: 1504 4 3 mas
: 1511 3 3 ter
: 1516 2 5  and
: 1520 3 1  ser
* 1524 4 3 vant
- 1530
: 2208 3 0 Mas
: 2215 3 0 ter
: 2220 2 0  and
: 2225 3 3  ser
: 2230 3 0 vant
- 2235
: 2360 2 5 It's
: 2364 2 3  a
: 2368 3 5  lot
: 2372 3 5  like
: 2376 2 3 ~
: 2380 8 5  life
- 2390
: 2396 2 3 And
: 2400 4 5  that's
: 2408 2 5  what's
: 2412 2 7  ap
: 2416 3 3 pea
: 2420 4 2 ling
- 2424
: 2424 2 0 If
: 2428 3 0  you
: 2432 2 5 ~
: 2436 3 5  des
: 2444 8 5 pise
- 2454
: 2460 2 3 that
: 2464 3 5  throw
: 2468 2 5 aw
: 2472 4 7 ay
: 2480 3 3  feel
: 2484 2 2 ing
- 2487
: 2488 2 0 From
: 2492 2 -2  dis
: 2496 3 0 pos
: 2500 2 0 ab
: 2504 2 0 le
: 2508 10 3  fun
- 2520
: 2556 2 3 Then
: 2560 2 2  this
: 2564 2 2  is
: 2568 2 2  the
* 2572 16 5  one
- 2590
: 2760 4 7 Let's
: 2768 6 0  play
- 2776
: 2784 4 7 mas
: 2791 3 5 ter
: 2796 2 3  and
: 2800 3 5  ser
: 2804 4 7 vant
- 2810
* 2824 3 7 Come
: 2828 7 0  on
- 2837
: 2848 4 5 mas
: 2855 3 5 ter
: 2860 2 0  and
: 2864 3 3  ser
: 2868 4 0 vant
- 2874
: 2888 4 7 Let's
: 2896 5 0  play
- 2903
* 2912 4 7 mas
: 2919 3 5 ter
: 2924 2 3  and
: 2928 3 5  ser
: 2932 4 7 vant
- 2938
: 2952 3 7 Come
: 2956 8 0  on
- 2966
: 2976 4 5 mas
: 2983 3 5 ter
: 2988 2 0  and
: 2992 3 3  ser
: 2996 4 0 vant
- 3002
* 3016 4 7 Let's
: 3024 6 0  play
- 3032
: 3040 4 7 mas
: 3047 3 5 ter
: 3052 2 3  and
: 3056 3 5  ser
: 3060 4 7 vant
- 3066
: 3080 3 7 Come
* 3084 7 0  on
- 3093
: 3104 4 5 mas
: 3111 3 5 ter
: 3116 2 0  and
: 3120 3 3  ser
: 3124 4 0 vant
E