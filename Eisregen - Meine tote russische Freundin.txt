#TITLE:Meine tote russische Freundin
#ARTIST:Eisregen
#EDITION:[SC]-Songs
#LANGUAGE:Deutsch
#MP3:Eisregen - Meine tote russische Freundin.mp3
#COVER:Eisregen - Meine tote russische Freundin [CO].jpg
#BACKGROUND:Eisregen - Meine tote russische Freundin [BG].jpg
#BPM:200
#GAP:36000
F 0 3 0 Die
F 3 4 0  Woh
F 8 4 0 nung
F 13 4 0  in
F 18 5 0  der
F 23 5 0  In
F 28 2 0 nen
F 32 4 0 stadt
- 37
F 37 4 -4 Zu
F 42 5 -4  der
F 48 5 -4  nur
F 53 4 -4  ich
F 58 3 -4  den
F 62 4 -4  Schlüs
F 67 4 -4 sel
F 72 4 -4  hab
- 77
F 77 5 -6 Nie
F 82 5 -6 mals
F 87 4 -6  darf
F 92 4 -4  mei
F 97 2 -4 ne
F 101 5 -4  Frau
F 107 3 -5  er
F 111 3 -6 fah
F 114 3 -6 ren
- 118
F 119 3 -6 Wo
F 123 5 -5 hin
F 128 3 -4  ich
F 132 6 -5  geh'
F 138 5 -5  in
F 143 4 -5  all
F 148 3 -5  den
F 152 3 -8  Jah
F 155 11 -7 ren
- 168
F 197 5 -8 Die
F 203 4 -7  Woh
F 208 4 -7 nung
F 213 3 -6  in
F 218 5 -6  der
F 223 4 -6  In
F 228 3 -5 nen
F 233 8 -6 stadt
- 243
F 278 4 -6 Ist
F 283 5 -7  stets
F 289 4 -7  ver
F 294 4 -8 dun
F 298 6 -8 kelt,
F 313 5 -6  Nacht
F 319 4 -6  und
F 323 13 -5  Tag
- 338
F 357 3 -9 Dort
F 362 5 -9  drin,
F 367 3 -9  im
F 372 5 -6  dunk
F 377 4 -6 len
F 382 4 -6  Däm
F 387 5 -6 mer
F 392 4 -6 licht
- 397
F 397 3 -9 Da
F 402 3 -9  war
F 407 5 -9 test
F 412 4 -6  du
F 418 5 -6  ganz
F 423 4 -6  sehn
F 428 4 -6 süch
F 433 4 -7 tig
- 438
F 438 4 -6 Wie
F 443 4 -5  konn
F 448 4 -5 te
F 453 4 -6  so
F 458 4 -6 was
F 463 5 -6  nur
F 468 4 -6  be
F 473 2 -6 gin
F 475 3 -7 nen
- 478
F 478 5 -3 Ich
F 483 3 -9  kann
F 488 4 -10  mich
F 493 5 -10  noch
F 498 5 -10  ganz
F 503 4 -10  gut
F 508 3 -1  ent
F 512 3 -3 sin
F 515 4 -3 nen
- 521
F 558 4 -6 In
F 563 4 -6  ei
F 568 5 -6 ner
F 573 5 -6  war
F 578 3 -6 men
F 583 4 -6  Som
F 588 5 -6 mer
F 593 5 -8 nacht
- 600
F 638 5 -6 Hast
F 643 4 -6  du
F 648 4 -6  mir
F 653 3 -6  neu
F 658 4 -6 es
F 663 5 -5  Glück
F 668 3 -7  ge
F 672 16 -8 bracht
- 690
F 777 3 -4 Du
F 782 6 -3  stiegst
F 788 4 -5  zu
F 793 5 -6  mir
F 798 3 -5  in
F 803 3 -4  mei
F 808 4 -4 nen
F 813 3 -6  Wa
F 816 1 -6 gen
- 818
F 818 3 -5 Dann
F 823 4 -4  sind
F 828 4 -5  wir
F 833 6 -7  rasch
F 839 3 -8  hier
F 843 4 -9 her
F 848 2 -6  ge
F 851 4 -6 fah
F 855 2 -6 ren
- 857
F 857 5 -5 Und
F 862 4 -5  als
F 868 4 -5  wir
F 873 5 -6  dann
F 878 3 -6  im
F 883 5 -5  Bet
F 888 3 -5 te
F 892 3 -5  la
F 895 3 -6 gen
- 898
F 898 3 -3 Da
F 903 6 -5  nahm
F 909 3 -5  das
F 913 3 -7  Schick
F 918 4 -8 sal
F 923 5 -3  sei
F 928 2 -6 nen
F 933 4 -6  Lauf
- 938
F 938 5 -5 Und
F 943 4 -6  ich
F 948 4 -5  biss
F 954 4 -6  dir
F 959 3 -5  die
F 963 5 -4  Keh
F 968 3 -5 le
F 973 4 -6  auf
- 977
F 977 3 -5 Und
F 982 5 -4  schnell
F 988 4 -6  er
F 993 5 -6 losch
F 999 2 -7  des
F 1003 5 -3  Le
F 1008 4 -4 bens
F 1013 4 -6  Glut
- 1018
F 1018 4 -4 Das
F 1023 4 -3  La
F 1028 4 -4 ken
F 1033 4 -6  sog
F 1038 4 -6  sich
F 1043 4 -4  voll
F 1048 4 -5  von
F 1053 5 -6  Blut
- 1058
F 1058 5 -6 Das
F 1063 3 -4  gan
F 1068 4 -5 ze
F 1073 5 -7  Zim
F 1078 4 -8 mer
F 1083 5 -3  stank
F 1088 4 -7  da
F 1093 4 -6 nach
- 1098
F 1099 4 -6 Als
F 1103 3 -4  ich
F 1108 4 -5  mich
F 1114 4 -6  ü
F 1118 4 -5 ber
F 1123 3 -4  dir
F 1127 4 -4  er
F 1132 18 -7 brach...
- 1152
: 1259 4 0 Ich
: 1264 2 0  kotz
: 1267 2 0 te
: 1270 4 2  mir
: 1275 3 2  die
: 1280 4 3  See
: 1285 2 5 le
: 1288 2 3  he
: 1291 5 2 raus
- 1298
: 1302 4 0 Doch
: 1307 2 0  dann
: 1310 2 0  zog
: 1313 3 2  ich
: 1318 2 2  mich
: 1322 5 3  nack
: 1327 3 5 i
: 1330 3 3 g
: 1334 3 2  aus
- 1339
: 1341 2 7 Schon
: 1344 4 7  mei
: 1350 4 7 ne
: 1355 5 7  Mut
: 1360 4 7 ter
: 1365 4 8  hat
: 1371 2 8  ge
: 1376 6 7 sagt:
- 1383
: 1385 2 5 Be
: 1387 4 3 en
: 1392 4 3 de,
: 1397 4 7  was
: 1402 4 7  du
: 1407 3 7  an
: 1410 2 8 ge
: 1412 3 7 fan
: 1415 3 3 gen
: 1419 4 2  hast.
- 1425
: 1514 4 12 Du
: 1519 3 7  warst
: 1523 2 7  ein
: 1526 4 7  net
: 1531 3 7 ter
: 1535 5 8  Zeit
: 1541 3 10 ver
: 1546 9 7 treib
- 1556
: 1558 4 3 Doch
: 1562 3 3  dann
: 1566 2 3  wur
: 1569 3 2 de
: 1573 3 2  zu
: 1578 5 3  kalt
: 1584 4 5  dein
: 1589 8 2  Leib
- 1599
: 1601 3 7 Ich
: 1605 4 7  leg
: 1611 4 7 te
: 1616 3 7  mich
: 1621 4 8  ne
: 1626 3 8 ben
: 1631 5 7  dich
- 1638
: 1644 3 3 Blut
: 1648 3 3  trock
: 1651 1 5 net
: 1653 5 7  schnell
: 1659 3 7  auf
: 1664 9 7  dem
F 1680 4 7  Ge
F 1685 14 7 sicht
- 1701
F 1730 4 -6 Am
F 1735 5 -5  nächs
F 1740 5 -5 ten
F 1746 5 -6  Mor
F 1751 2 -6 gen,
F 1754 2 -5  in
F 1756 3 -5  al
F 1760 2 -5 ler
F 1765 5 -6  Früh
- 1770
F 1770 5 -5 Gab
F 1775 4 -4  ich
F 1781 4 -5  mir
F 1786 5 -7  wirk
F 1791 4 -8 lich
F 1796 4 -3  al
F 1800 2 -6 le
F 1804 3 -8  Mü
F 1807 3 -8 he
- 1810
F 1810 5 -5 Dich
F 1815 4 -4  her
F 1820 4 -5 zu
F 1825 3 -6 rich
F 1828 2 -6 ten
F 1831 2 -4  für
F 1833 2 -5  die
F 1836 2 -4  E
F 1840 4 -5 wig
F 1845 5 -6 keit
- 1851
F 1851 3 -6 Auf
F 1855 5 -4  dass
F 1861 4 -5  du
F 1866 3 -7  im
F 1871 4 -8 mer
F 1875 3 -3  bei
F 1880 5 -6  mir
F 1885 5 -6  bleibst
- 1890
F 1890 4 -5 Ich
F 1895 4 -4  hab
F 1900 4 -5  dich
F 1905 5 -6  ganz
F 1911 4 -5  gut
F 1916 4 -4  hin
F 1920 3 -5 ge
F 1924 5 -6 kriegt
- 1930
F 1931 3 -5 Und
F 1935 5 -5  was
F 1941 4 -5  jetzt
F 1946 3 -8  auf
F 1951 4 -9  dem
F 1956 4 -2  Bo
F 1960 4 -6 den
F 1965 4 -6  liegt
- 1970
F 1970 5 -5 Das
F 1975 4 -4  kann
F 1981 4 -5  halt
F 1986 4 -6  nur
F 1991 4 -5  kein
F 1996 4 -4  Wort
F 2000 3 -5  mehr
F 2005 2 -6  sa
F 2008 2 -6 gen
- 2010
F 2010 5 -2 Doch
F 2015 3 -2  man
F 2020 4 -8  kann
F 2025 5 -8  halt
F 2030 3 -7  nicht
F 2035 3 -4  al
F 2038 4 -6 les
F 2044 4 -6  ha
F 2048 9 -5 ben...
- 2059
: 2130 5 0 So
: 2136 4 0  ver
: 2141 5 2 ging
: 2147 2 2  die
: 2152 5 3  Zeit
: 2158 3 5  mit
: 2162 7 2  dir
- 2171
: 2174 3 0 Ich
: 2178 3 0  den
: 2181 2 0 ke,
: 2184 4 2  du
: 2189 4 2  bist
: 2195 4 3  gern
: 2200 4 5  bei
: 2205 4 2  mir
- 2211
: 2214 2 7 Ich
: 2217 4 7  kauf
: 2222 4 7 te
: 2227 4 7  dir
: 2232 4 7  ein
: 2238 4 8  Le
: 2243 4 8 der
: 2248 4 7 mie
: 2253 3 3 der
- 2256
: 2256 2 0 Ein
: 2259 3 3  Hauch
: 2264 3 3  von
: 2269 5 2  Sün
: 2275 4 2 de,
: 2280 4 3  gar
: 2285 4 5  nicht
: 2290 4 3  bie
: 2295 3 0 der
- 2299
: 2301 4 12 Das
: 2306 3 7  trägst
: 2310 1 7  du
: 2312 3 7  ganz
: 2317 5 7  für
: 2323 4 8  mich
: 2328 4 10  al
: 2333 8 7 lein
- 2341
: 2341 2 -5 Ein
: 2344 4 0  Grund,
: 2349 3 0  um
: 2354 4 2  häu
: 2359 5 2 fig
: 2365 5 3  hier
: 2371 1 5  z
: 2372 2 3 u
: 2375 5 2  sein
- 2381
: 2381 3 7 In
: 2384 2 7  der
: 2387 4 7  Woh
: 2392 3 7 nung
: 2397 5 7  in
: 2403 4 7  der
: 2408 4 8  In
: 2413 3 7 nen
: 2418 6 7 stadt
- 2425
: 2426 2 3 Zu
: 2429 4 3  der
: 2434 3 3  ich
: 2438 2 3  al
: 2441 4 2 lein
: 2446 2 2  den
: 2450 4 3  Schlüs
: 2455 5 3 sel
F 2471 32 0  hab
E
