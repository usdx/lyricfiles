#TITLE:On écrit sur les murs
#ARTIST:Worlds Apart
#MP3:Worlds Apart   On écrit sur les murs.mp3
#VIDEO:Worlds Apart   On écrit sur les murs.mp4
#COVER:Worlds Apart   On écrit sur les murs.jpg
#BPM:236
#GAP:10050
#VIDEOGAP:0
#ENCODING:UTF8
: 0 9 7 Par
: 11 8 7 tout,
: 28 3 2  au
: 32 3 0 tour
: 36 3 0  de
: 40 8 0  nous,
- 55
: 58 1 2 Y a
: 61 1 2  des
: 63 1 2  si
: 66 4 2 gnes
: 72 3 3  d'es
: 76 7 2 poir,
- 89
: 92 3 -2 Dans
: 96 4 -2  les
: 101 3 -3  re
: 105 10 -5 gards,
- 122
: 125 5 -3 Don
: 131 2 -7 nons
: 135 2 6  leurs
: 139 3 6  éc
: 143 7 6 rits,
- 155
: 157 5 0 Car
: 165 1 0  dans
: 168 3 0  la
: 174 5 2  nuit
- 182
: 184 3 1 Tout
: 188 6 0  s'ef
: 195 7 8 face
- 217
: 248 3 -1 Memes
: 252 5 -1  leurs
: 258 8 8  traces
- 281
: 317 2 -3 On
: 321 3 9  éc
: 325 6 11 rit
: 333 2 9  sur
: 337 2 9  les
: 341 2 9  murs,
- 343
: 345 2 11 Le
: 349 2 12  nom
: 353 2 12  de
: 357 2 11  ceux
: 361 2 12  qu'on
: 365 7 8  aime,
- 378
: 381 2 9 Des
: 385 3 12  mes
: 389 6 14 sages
: 397 2 16  pour
: 401 2 14  les
: 405 2 12  jours
: 409 2 14  a
: 413 7 16  ve
: 421 7 15 nir
- 438
: 445 2 16 On
: 449 3 17  éc
: 453 6 19 rit
: 461 2 19  sur
: 465 2 17  les
: 469 2 17  murs,
- 471
: 473 2 14 A
: 477 3 14  l'en
: 481 2 16 cre
: 485 6 16  de
: 493 3 16  no
: 497 2 12 ~s
: 501 6 11  veines,
- 507
: 509 2 9 On
: 513 3 9  des
: 517 6 9 sine
: 531 2 10  tout
: 535 2 9  c'que
: 539 2 9  l'on
: 543 3 10  vou
: 547 6 10 drait
: 555 7 8  dire,
- 572
: 576 2 9 On
: 580 3 9  éc
: 584 6 11 rit
: 592 2 11  sur
: 596 2 9  les
: 600 2 9  murs,
- 602
: 604 2 11 La
: 608 3 12  for
: 612 2 12 ce
: 616 2 11  de
: 620 2 12  nos
: 624 7 11  reves,
- 637
: 640 2 9 Nos
: 644 3 12  es
: 648 6 14 poirs
: 656 2 16  en
: 660 2 14  forme
: 664 2 12  de
: 668 3 14  graf
: 672 5 14 fi
: 680 7 15 tis
- 697
: 709 2 16 On
: 713 3 17  éc
: 717 6 19 rit
: 725 2 17  sur
: 729 2 16  les
: 733 2 16  murs
- 735
: 737 2 14 Pour
: 741 2 14  que
: 745 3 15  l'a
: 749 6 16 mour
: 757 3 9  se
: 761 2 9 ~
: 765 6 8  leve,
- 771
: 773 2 9 Un
: 777 2 9  beau
: 781 6 11  jour
: 789 2 10  sur
: 793 2 9  le
: 797 2 9  monde
: 801 3 11  en
: 805 7 9 dor
: 813 7 8 mi
- 830
: 848 10 9 Des
: 860 14 3  mots,
: 876 3 10  seu
: 880 2 0 lement
: 884 6 1  gra
: 892 6 2 vés,
- 905
: 908 1 6 Pour
: 910 1 0  ne
: 912 1 2  pas
: 914 5 4  ou
: 920 3 3 bli
: 924 7 0 er,
- 937
: 940 2 -2 Pour
: 944 6 0  tout
: 952 3 6  chan
: 956 7 -5 ger,
- 968
: 970 5 -3 Mé
: 976 3 2 lan
: 980 2 6 geons
: 984 3 6  de
: 988 6 6 main,
- 1003
: 1006 2 1 Dans
: 1009 2 0  un
: 1012 7 0  re
: 1020 6 2 frain,
- 1028
: 1030 2 1 Nos
: 1034 3 2  vi
: 1040 7 10 sages
- 1062
* 1093 3 -2 Mé
* 1097 5 -2 tis
* 1103 7 7 sage
- 1125
: 1163 2 -3 On
: 1167 3 9  éc
: 1171 6 11 rit
: 1179 2 9  sur
: 1183 2 9  les
: 1187 2 9  murs,
- 1189
: 1191 2 11 Le
: 1195 2 12  nom
: 1199 2 12  de
: 1203 2 11  ceux
: 1207 2 12  qu'on
: 1211 7 8  aime,
- 1224
: 1227 2 9 Des
: 1231 3 12  mes
: 1235 6 14 sages
: 1243 2 16  pour
: 1247 2 14  les
: 1251 2 12  jours
: 1255 2 14  a
: 1259 7 16  ve
: 1267 7 15 nir,
- 1284
: 1291 2 16 On
: 1295 3 17  éc
: 1299 6 19 rit
: 1307 2 19  sur
: 1311 2 17  les
: 1315 2 17  murs,
- 1317
: 1319 2 14 A
: 1323 3 14  l'en
: 1327 2 16 cre
: 1331 6 16  de
: 1339 3 16  nos
: 1343 2 12 ~
: 1347 6 11  veines,
- 1353
: 1355 2 9 On
: 1359 3 9  des
: 1363 6 9 sine
: 1377 2 10  tout
: 1381 2 9  c'que
: 1385 2 9  l'on
: 1389 3 10  vou
: 1393 6 10 drait
: 1401 7 8  dire
- 1418
* 1422 2 9 On
* 1426 3 9  éc
* 1430 6 11 rit
* 1438 2 11  sur
: 1442 2 9  les
: 1446 2 9  murs,
- 1448
: 1450 2 11 La
: 1454 3 12  for
: 1458 2 12 ce
: 1462 2 11  de
: 1466 2 12  nos
: 1470 7 11  reves,
- 1483
: 1486 2 9 Nos
: 1490 3 12  es
: 1494 6 14 poirs
: 1502 2 16  en
: 1506 2 14  forme
: 1510 2 12  de
: 1514 3 14  graf
: 1518 5 14 fi
: 1526 7 15 tis,
- 1543
: 1555 2 16 On
: 1559 3 17  éc
: 1563 6 19 rit
: 1571 2 17  sur
: 1575 2 16  les
: 1579 2 16  murs,
- 1581
: 1583 2 14 Pour
: 1587 2 14  que
: 1591 3 15  l'a
: 1595 6 16 mour
: 1603 3 9  se
: 1607 2 9 ~
: 1611 6 8  leve,
- 1617
: 1619 2 9 Un
: 1623 2 9  beau
: 1627 6 11  jour
: 1635 2 10  sur
: 1639 2 9  le
: 1643 2 9  monde
: 1647 3 11  en
: 1651 7 9 dor
: 1659 7 8 mi
- 1697
: 2209 2 -3 On
: 2213 3 9  éc
: 2217 6 11 rit
: 2225 2 9  sur
: 2229 2 9  les
: 2233 2 9  murs,
- 2235
: 2237 2 11 Le
: 2241 2 12  nom
: 2245 2 12  de
: 2249 2 11  ceux
: 2253 2 12  qu'on
: 2257 7 8  aime,
- 2270
: 2273 2 9 Des
: 2277 3 12  mes
: 2281 6 14 sages
: 2289 2 16  pour
: 2293 2 14  les
: 2297 2 12  jours
: 2301 2 14  a
: 2305 7 16  ve
: 2313 7 15 nir
- 2330
: 2337 2 16 On
: 2341 3 17  éc
: 2345 6 19 rit
: 2353 2 19  sur
: 2357 2 17  les
: 2361 2 17  murs,
- 2363
: 2365 2 14 A
: 2369 3 14  l'en
: 2373 2 16 cre
: 2377 6 16  de
: 2385 3 16  nos
: 2389 2 12 ~
: 2393 6 11  veines,
- 2399
: 2401 2 9 On
: 2405 3 9  des
: 2409 6 9 sine
: 2423 2 10  tout
: 2427 2 9  c'que
: 2431 2 9  l'on
: 2435 3 10  vou
: 2439 6 10 drait
: 2447 7 8  dire,
- 2464
: 2468 2 9 On
: 2472 3 9  éc
: 2476 6 11 rit
: 2484 2 11  sur
: 2488 2 9  les
: 2492 2 9  murs,
- 2494
: 2496 2 11 La
: 2500 3 12  for
: 2504 2 12 ce
: 2508 2 11  de
: 2512 2 12  nos
: 2516 7 11  reves,
- 2529
: 2532 2 9 Nos
: 2536 3 12  es
: 2540 6 14 poirs
: 2548 2 16  en
: 2552 2 14  forme
: 2556 2 12  de
: 2560 3 14  graf
: 2564 5 14 fi
: 2572 7 15 tis
- 2589
: 2601 2 16 On
: 2605 3 17  éc
: 2609 6 19 rit
: 2617 2 17  sur
: 2621 2 16  les
: 2625 2 16  murs,
- 2627
: 2629 2 14 Pour
: 2633 2 14  que
: 2637 3 15  l'a
: 2641 6 16 mour
: 2649 3 9  se
: 2653 2 9 ~
: 2657 6 8  leve,
- 2663
: 2664 3 11 Un
: 2668 3 11  beau
: 2673 11 10  jour
: 2696 3 11  sur
: 2700 3 11  le
: 2704 9 14  monde,
* 2752 7 16  en
* 2760 7 16 dor
* 2768 17 16 mi
E