#TITLE:Kreuzberger Nachte
#ARTIST:Gebruder Blattschuss
#MP3:Gebruder Blattschuss - Kreuzberger Nachte.mp3
#VIDEO:Gebruder Blattschuss - Kreuzberger Nachte [VD#0].avi
#COVER:Gebruder Blattschuss - Kreuzberger Nachte [CO].jpg
#BPM:340
#GAP:16897,06
#VIDEOGAP:0
#ENCODING:UTF8
#EDITION:SingStar Sch DE
: 0 3 62 Ich
: 6 3 60  sitz
: 12 3 60  schon
: 18 4 59  seit
- 23
: 24 4 57 'Ner
: 30 3 60  Stun
: 38 4 60 de
: 42 4 59  ziem
: 47 4 57 lich
: 53 5 60  dumm
- 60
: 94 4 59 Al
: 100 3 60 lein
: 106 3 60  am
: 109 6 59  mei
: 117 4 60 nem
- 122
: 123 3 60 Knei
: 129 4 60 pen
: 135 3 59 tisch
: 140 3 57  he
* 146 5 55 rum
- 153
: 187 2 59 Ich
: 193 3 59  trin
: 199 3 59 ke
: 205 4 57  schnell
- 210
: 210 4 55 Ob
: 216 3 59 wohl
: 222 3 59  ich's
: 228 4 57  nicht
: 233 4 55  ver
: 239 6 59 trag
- 247
: 269 1 59 Weil
: 272 1 59  ich
: 275 4 59  we
: 280 3 57 der
: 286 3 55  vol
: 291 3 55 le
- 296
: 303 3 57 Noch
: 308 3 55  lee
: 314 3 55 re
: 320 4 57  Glä
: 325 4 55 ser
* 331 7 52  mag
- 340
: 371 3 60 Da
: 377 3 60  plötz
: 383 3 60 lich
- 387
: 388 3 60 Set
: 394 4 57 zen
: 400 3 60  sich
: 406 4 60  sechs
: 412 3 59  Mann
: 417 4 57  zu
* 421 6 60  mir
- 429
: 457 3 60 Und
: 463 3 60  be
: 469 1 60 stel
: 472 2 60 len
: 476 5 60  laut
: 486 3 60 stark
- 490
: 492 3 60 ''Bring'
: 498 3 60  Se
: 501 3 59  mal
: 509 3 57  drei
: 515 8 55  Bier''
- 525
: 544 3 59 Ich
: 550 3 59  seh
: 555 2 59  schon
: 561 3 59  dop
: 567 3 59 pelt
- 572
: 578 3 59 Und
: 584 3 59  das
: 590 3 59  aus
: 595 3 57  gu
: 601 4 55 tem
: 607 5 59  Grund
- 614
: 642 3 59 Denn
: 648 3 57  in
* 653 4 55  Eck
: 665 3 55 knei
: 671 3 55 pen
- 675
: 676 3 55 Geht
: 682 4 55  es
: 688 4 57  nun
: 694 4 59  mal
: 700 5 60  rund
- 707
: 747 7 60 Kreuz
: 757 4 59 ber
: 763 4 57 ger
: 770 7 60  Näch
: 781 3 59 te
: 787 3 57  sind
: 793 5 60  lang
- 800
: 839 7 60 Kreuz
: 851 4 59 ber
: 856 4 57 ger
: 862 6 60  Näch
: 874 4 59 te
: 879 5 57  sind
: 885 6 55  lang
- 893
: 932 7 59 Erst
: 943 4 57  fang'
: 948 4 55  se
: 955 7 59  janz
: 966 3 57  lang
: 972 4 55 sam
: 978 6 59  an
- 986
: 1013 4 57 A
: 1019 3 55 ber
: 1024 6 59  dann
- 1032
: 1059 4 59 A
: 1065 3 62 ber
* 1071 6 60  dann
- 1079
: 1113 3 60 Jetzt
: 1119 3 60  fragt
: 1124 3 60  mich
- 1128
: 1130 4 59 Doch
: 1136 4 57  so'n
: 1142 3 60  Typ
: 1148 4 60  ob
: 1154 4 59  ich
: 1159 3 57  stu
: 1163 7 60 dier
- 1172
: 1195 3 57 Ich
: 1200 3 57  sag
- 1204
: 1206 4 59 ''Ja
: 1212 3 60  Wirt
: 1218 3 60 schafts
: 1223 3 59 po
: 1227 4 57 li
: 1232 3 60 tik
- 1237
: 1241 3 60 Drum
: 1247 4 59  sitz
: 1252 4 57  ich
* 1258 6 55  hier''
- 1266
: 1301 5 58 Da
: 1307 3 59  sacht
: 1312 3 59  der
: 1318 4 57  dass
- 1323
: 1324 4 55 Er
: 1330 3 59  von
: 1336 3 59  der
: 1341 4 57  Zei
: 1347 4 55 tung
: 1353 6 59  wär
- 1361
: 1389 2 59 Und
: 1394 3 59  da
: 1400 3 59  wär
: 1406 3 59  er
- 1410
: 1411 3 59 Der
: 1417 3 59  Lo
* 1423 8 59 kal
: 1434 4 57 re
: 1440 4 55 dak
: 1445 5 52 teur
: 1451 4 55 ~
- 1457
: 1486 3 60 Ein
: 1492 3 60  Rent
: 1498 1 60 ner
: 1501 5 60  ruft
- 1508
: 1510 3 64 ''Ihr
: 1516 3 64  solltet
: 1522 4 64  euch
: 1533 3 64  was
: 1539 5 64  schäm'''
- 1546
: 1571 3 57 Ein
: 1576 3 57  and
: 1581 3 59 rer
: 1587 3 60  meint
- 1591
: 1592 3 60 Das
: 1598 3 60  lä
: 1603 3 60 ge
: 1609 2 60  al
: 1612 4 60 les
: 1620 3 59  am
: 1627 4 57  Sys
* 1633 6 55 tem
- 1641
: 1662 4 58 Das
: 1668 3 59  ist
: 1674 3 59  so
: 1680 3 59  krank
: 1686 4 59  wie
- 1691
: 1692 4 57 Mei
: 1698 4 55 ne
: 1704 3 59  Le
: 1710 4 59 ber
: 1716 4 57  sag
: 1721 4 55  ich
: 1727 6 59  barsch
- 1735
: 1756 3 59 Die
: 1762 3 59  zwölf
: 1768 4 57  Se
: 1774 3 55 mes
: 1779 3 55 ter
- 1783
: 1785 4 55 War'n
: 1791 3 55  doch
: 1797 3 55  nicht
- 1801
: 1802 4 55 So
: 1808 3 57  ganz
: 1814 3 59  um
* 1820 5 60 sonst
- 1827
: 1867 9 60 Kreuz
: 1879 4 59 ber
: 1885 4 57 ger
: 1892 7 60  Näch
: 1903 3 59 te
: 1909 3 57  sind
: 1915 5 60  lang
- 1922
: 1961 7 60 Kreuz
: 1973 4 59 ber
: 1978 4 57 ger
: 1984 6 60  Näch
: 1996 4 59 te
: 2001 5 57  sind
: 2007 6 55  lang
- 2015
: 2054 7 59 Erst
: 2065 4 57  fang'
: 2070 4 55  se
: 2077 7 59  janz
: 2088 3 57  lang
: 2094 4 55 sam
: 2100 6 59  an
- 2108
: 2135 4 57 A
: 2141 3 55 ber
: 2146 6 59  dann
- 2154
: 2182 4 59 A
: 2188 3 62 ber
: 2194 6 60  dann
- 2202
* 2242 7 60 Kreuz
: 2252 4 59 ber
: 2258 4 57 ger
: 2265 7 60  Näch
: 2276 3 59 te
: 2282 3 57  sind
: 2288 5 60  lang
E