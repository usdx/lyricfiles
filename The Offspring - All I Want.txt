#TITLE:All I Want
#ARTIST:The Offspring
#MP3:The Offspring - All I Want.mp3
#COVER:The Offspring - All I Want [CO].jpg
#BACKGROUND:The Offspring - All I Want [BG].jpg
#BPM:362
#GAP:470
#ENCODING:UTF8
#LANGUAGE:Englisch
#EDITION:[SC]-Songs
: 1 7 9 Ya
: 9 6 8  Ya
: 16 6 6  Ya
: 24 6 4  Ya
: 32 6 6  Ya
- 40
: 165 4 1 Day
: 170 5 1  af
: 176 4 -1 ter
: 181 8 1  day
- 189
: 189 6 4 your
: 197 5 2  home
: 203 9 2  life's
: 212 2 1  a
: 215 2 2  wre
: 217 3 1 ~
: 220 7 -1 ck.
- 227
: 227 3 1 The
: 231 4 1  pow
: 236 6 1 ers
: 243 4 -1  that
: 247 7 1  be
- 255
: 255 7 4 just
: 263 6 6  breathe
: 269 7 4  down
: 276 4 4  your
: 280 4 6  ne
: 284 6 4 ~ck.
- 291
: 293 4 1 You
: 298 3 1  get
: 302 6 1  no
: 308 5 -1  re
: 313 5 1 spect,
- 319
: 321 7 4 you
: 329 4 2  get
: 334 6 2  no
: 341 4 1  re
: 346 3 2 li
: 349 2 1 ~
: 351 3 -1 ef.
- 355
: 357 3 -1 You
: 362 4 1  got
: 367 4 1 ta
: 373 4 -1  speak
: 379 6 1  up
- 386
: 388 4 4 and
: 393 5 6  yell
: 399 6 4  out
: 407 4 4  your
: 412 3 6  pie
: 415 4 4 ~ce!
- 419
: 419 4 4 So
: 426 4 9  back
: 432 7 9  off
: 440 4 9  your
: 445 10 6  rules,
- 457
: 461 4 9 back
: 466 6 9  off
: 473 4 9  your
: 478 6 6  jive
- 485
: 485 6 4 'cause
: 492 2 1  I'm
: 495 5 4  sick
: 501 5 4  of
: 508 5 4  not
: 514 9 4  liv
: 523 7 9 ing
: 531 3 8  to
: 534 6 8  stay
: 543 4 8  a
: 547 7 9 li
: 555 8 8 ~ve.
- 564
: 565 4 9 Leave
: 570 8 9  me
: 579 2 9  a
: 582 7 6 lone!
- 591
: 596 3 6 Not
: 600 4 9  ask
: 605 3 9 ing
: 611 3 9  a
: 615 5 6  lot!
- 621
: 623 4 4 I
: 628 4 1  don't
: 633 5 4  wan
: 640 6 4 na
: 649 8 4  be
: 658 9 9  con
: 667 8 8 trolled!
- 677
: 682 3 6 That's
: 686 7 8  all
: 694 7 9  I
* 702 37 9  want!
- 741
: 753 7 8 All
: 761 7 9  I
* 770 33 9  wa
: 805 13 8 ~nt!
- 820
: 823 7 8 All
: 832 4 9  I
* 839 34 9  want!
- 875
: 891 6 8 All
: 899 6 9  I
* 907 33 9  want!
- 941
: 942 6 9 Ya
: 949 6 8  Ya
: 957 6 6  Ya
: 965 6 4  Ya
: 973 6 6  Ya
- 981
: 1106 4 1 How
: 1111 5 1  ma
: 1118 3 -1 ny
: 1122 7 1  times
: 1130 8 4  is
: 1140 3 2  it
: 1144 5 2  go
: 1150 4 1 nna
: 1155 2 2  ta
: 1158 3 1 ~
: 1162 3 -1 ke?
- 1166
: 1167 3 1 Till
: 1170 6 1  some
: 1177 7 1 one
: 1185 2 -1  a
: 1188 8 1 round
: 1197 7 4  you
: 1205 5 6  hears
: 1211 5 4  what
: 1217 3 4  you
: 1221 5 6  sa
: 1227 7 4 ~y?
- 1234
: 1234 2 1 You've
: 1237 5 1  tried
: 1243 6 1  be
: 1250 3 -1 ing
: 1254 7 1  cool,
- 1263
: 1265 4 4 you
: 1270 5 2  feel
: 1276 6 2  like
: 1283 3 4  a
: 1287 3 2  li
: 1290 3 1 ~
: 1293 4 -1 e.
- 1298
: 1300 3 1 You've
: 1304 3 1  played
: 1309 5 1  by
: 1315 4 -1  their
: 1320 8 1  rules,
- 1328
: 1328 3 4 now
: 1332 3 4  it's
: 1336 3 6  their
: 1341 5 4  turn
: 1347 3 4  to
: 1351 4 6  tr
: 1356 3 4 ~y.
- 1360
: 1360 6 4 So
: 1367 3 9  back
: 1372 7 9  off
: 1380 4 9  your
: 1385 9 6  rules,
- 1396
: 1401 5 9 back
: 1407 7 9  off
: 1415 3 9  your
: 1420 6 6  jive
- 1428
: 1430 4 4 'cause
: 1434 4 1  I'm
: 1439 4 4  sick
: 1444 5 4  of
: 1450 4 4  not
: 1455 6 4  li
: 1462 7 9 ving
: 1470 4 8  to
: 1475 8 8  stay
: 1484 4 8  a
: 1489 7 9 li
: 1497 5 8 ~ve.
- 1503
: 1505 4 9 Leave
: 1510 6 9  me
: 1517 3 9  a
: 1521 11 6 lone,
- 1533
: 1535 4 6 not
: 1540 5 9  ask
: 1546 6 9 ing
: 1553 3 9  a
: 1557 7 6  lot.
- 1564
: 1564 6 4 Just
: 1571 3 1  don't
: 1575 8 4  wan
: 1584 5 4 na
: 1591 7 4  be
: 1600 8 9  con
: 1609 8 8 trolled.
- 1619
: 1623 3 6 That's
: 1627 7 8  all
: 1635 5 9  I
* 1643 37 9  want!
- 1682
: 1695 7 8 All
: 1703 6 9  I
* 1712 32 9  wa
* 1745 8 8 ~nt!
- 1755
: 1763 8 8 All
: 1772 6 9  I
* 1779 33 9  want!
- 1814
: 1831 7 8 All
: 1839 7 9  I
* 1848 32 9  wa
* 1881 8 8 ~nt!
- 1891
: 1911 3 1 I
: 1915 5 2  said
: 1921 7 2  it
: 1929 3 2  be
: 1933 8 4 fore,
- 1942
: 1944 1 6 I'll
: 1948 5 6  say
: 1954 7 6  it
: 1962 4 4  a
: 1967 8 4 gain:
- 1976
: 1976 4 -1 If
: 1982 4 2  you
: 1987 2 2  could
: 1990 6 4  just
: 1997 4 4  lis
: 2002 4 4 ten
- 2006
: 2006 5 4 then
: 2011 3 4  it
: 2014 8 6  might
: 2023 6 6  make
* 2030 48 6  se
* 2080 43 8 ~nse.
- 2125
: 2136 6 4 So
: 2144 4 9  back
: 2149 7 9  off
: 2157 3 9  your
: 2161 9 6  rules,
- 2172
: 2177 3 9 back
: 2181 7 9  off
: 2189 3 9  your
: 2193 7 6  jive
- 2200
: 2200 7 4 'cause
: 2208 3 1  I'm
: 2212 4 4  sick
: 2217 4 4  of
: 2224 3 4  not
: 2229 6 4  li
: 2236 7 9 ving
: 2244 3 8  to
: 2248 8 8  stay
: 2257 4 8  a
: 2262 6 9 li
: 2270 7 8 ~ve.
- 2278
: 2278 4 9 Leave
: 2283 6 9  me
: 2291 3 9  a
: 2295 8 6 lone!
- 2305
: 2310 2 9 Not
: 2312 5 9  as
: 2318 6 9 king
: 2325 4 9  a
: 2329 6 6  lot,
- 2336
: 2338 3 1 I
: 2342 4 1  don't
: 2347 7 4  wan
: 2355 6 4 na
: 2363 7 4  be
: 2371 7 9  con
: 2380 8 8 trolled.
- 2390
: 2392 4 6 That's
: 2397 7 8  all
: 2405 6 9  I
* 2412 35 9  want!
- 2449
: 2464 7 8 All
: 2472 6 9  I
* 2479 18 9  wa
* 2498 12 4 ~
* 2511 9 6 nt!
- 2522
: 2531 6 8 All
: 2538 5 9  I
* 2545 66 9  wa
* 2613 17 8 ~nt!
- 2632
: 2645 7 9 Ya
: 2653 7 8  Ya
: 2661 7 6  Ya
: 2669 7 4  Ya
: 2677 7 6  Ya
E