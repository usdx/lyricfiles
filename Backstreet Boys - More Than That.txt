#TITLE:More Than That
#ARTIST:Backstreet Boys
#MP3:Backstreet Boys - More Than That.mp3
#VIDEO:Backstreet Boys - More Than That.mp4
#COVER:Backstreet Boys - More Than That[CO].jpg
#BPM:320
#GAP:19800
#ENCODING:UTF8
#LANGUAGE:English
#GENRE:Pop
#YEAR:2000
: 0 5 0 I
: 6 5 2  can
: 13 8 4  see
: 22 5 2  that
: 28 9 0  you've
: 39 8 -1  been
: 49 24 -1  cry
: 73 8 -3 ing
- 83
: 126 7 0 You
: 134 7 2  can't
: 143 7 4  hide
: 151 5 2  it
: 157 8 0  with
: 168 3 2  a
: 173 16 2  lie
- 191
: 256 7 0 What's
: 264 4 2  the
: 269 8 4  use
: 278 6 2  in
: 285 8 0  you
: 295 10 -1  de
: 305 26 -1 ny
: 331 8 -3 ing
- 341
: 361 6 -5 That
: 368 20 -5  what
: 392 5 -3  you
: 398 23 -3  have
: 425 4 -1  is
: 430 28 -1  wrong
- 460
: 509 3 -8 I
: 513 5 0  heard
: 519 6 2  him
: 527 7 4  pro
: 534 6 2 mise
: 541 8 0  you
: 550 13 -1  for
: 563 28 -1 ev
: 591 10 -3 er
- 603
: 641 5 0 But
: 647 8 2  for
: 655 4 4 ev
: 659 7 2 er's
: 669 8 0  come
: 678 15 2  and
: 697 32 4  gone
- 731
: 769 5 0 Ba
: 774 6 2 by,
: 783 5 4  he
: 790 4 2  would
: 796 11 7  say
: 809 10 4  what
: 819 14 4 ev
: 833 20 2 er
- 855
: 873 7 -5 it
: 881 23 -5  takes
: 905 7 -3  to
: 913 23 -3  keep
: 937 7 -1  you
: 945 32 -1  blind
- 979
: 997 3 -8 To
: 1001 4 -6  the
: 1006 23 -5  truth
: 1033 4 -5  be
: 1037 23 -3 tween
: 1064 5 2  the
: 1070 32 2  lines
- 1104
: 1121 4 4 O
: 1125 4 4 h
: 1129 9 6 ~
: 1139 11 7  I
: 1152 7 -1  will
: 1161 23 0  love
: 1185 7 7  you
: 1193 24 7  more
: 1218 8 7  than
: 1227 12 7  that
- 1241
: 1249 8 4 I
: 1258 5 6  won't
: 1264 16 7  say
: 1281 7 -1  the
: 1290 22 0  words,
- 1313
: 1313 7 7 then
: 1321 23 7  take
: 1345 8 9  them
: 1354 22 9  back
- 1378
: 1393 14 7 Don't
: 1409 7 -1  give
: 1418 23 0  lone
: 1441 10 7 li
: 1451 21 7 ness
: 1476 4 6  a
: 1482 21 6  chance
- 1505
: 1517 2 4 Ba
: 1519 4 6 by
: 1524 6 7  list
: 1530 4 6 en
: 1536 7 7  to
: 1544 6 6  me
: 1551 8 7  when
: 1560 4 6  I
: 1566 19 4  say
- 1587
: 1601 6 4 I
: 1608 6 6  will
: 1615 8 7  love
: 1624 5 6  you
: 1630 10 4  more
: 1641 6 2  than
: 1648 29 4  that
- 1679
: 1729 6 0 Ba
: 1735 7 2 by
: 1743 6 4  you
: 1750 8 2  de
: 1758 7 0 serve
: 1766 12 -1  much
: 1779 28 -1  bet
: 1807 10 -3 ter
- 1819
: 1857 6 0 What's
: 1864 6 2  the
: 1871 8 4  use
: 1880 7 2  in
: 1888 9 0  hold
: 1897 7 2 ing
: 1905 20 2  on
- 1927
: 1985 5 0 Don't
: 1991 5 2  you
: 1997 10 4  see
: 2008 6 2  it's
: 2015 8 0  now
: 2025 9 -1  or
: 2035 29 -1  nev
: 2064 5 -3 er
- 2071
: 2091 5 -5 'cause
: 2097 24 -5  I
: 2122 5 -3  just
: 2128 23 -3  can't
: 2153 6 -1  be
: 2160 18 -1  friends
- 2180
: 2214 5 -8 Ba
: 2219 4 -6 by
: 2225 26 -5  know
: 2251 5 -3 ing
: 2257 23 -3  in
: 2283 5 2  the
: 2289 12 2  end
- 2303
: 2337 18 4 that
: 2356 11 7  I
: 2369 7 -1  will
: 2378 23 0  love
: 2402 7 7  you
: 2410 24 7  more
: 2435 8 7  than
: 2444 12 7  that
- 2458
: 2467 8 4 I
: 2476 5 6  won't
: 2482 16 7  say
: 2499 7 -1  the
: 2508 22 0  words,
- 2531
: 2531 7 7 then
: 2539 23 7  take
: 2563 8 9  them
: 2572 22 9  back
- 2596
: 2612 14 7 Don't
: 2628 7 -1  give
: 2637 23 0  lone
: 2660 10 7 li
: 2670 21 7 ness
: 2695 4 6  a
: 2701 21 6  chance
- 2724
: 2735 2 4 Ba
: 2737 4 6 by
: 2742 6 7  list
: 2748 4 6 en
: 2754 7 7  to
: 2762 6 6  me
: 2769 8 7  when
: 2778 4 6  I
: 2784 19 4  say
- 2805
: 2817 30 7 Hey
: 2865 3 0  There's
: 2869 3 2  not
: 2877 3 0  a
: 2881 11 2  day
: 2893 3 0  that
: 2897 6 2  pas
: 2903 8 0 ses
: 2913 4 -3  by
- 2919
: 2925 3 0 I
: 2929 3 2  don't
: 2933 7 4  won
: 2940 4 2 der
: 2945 10 4  why
: 2956 4 2  we
: 2961 7 4  have
: 2968 4 2 n't
: 2973 16 0  tried
- 2991
: 2993 3 0 It's
: 2997 5 2  not
: 3003 5 0  too
: 3009 11 2  late
: 3021 2 0  to
: 3024 8 2  change
: 3033 7 0  your
: 3041 12 -3  mind
- 3054
: 3055 5 0 So
: 3061 5 5  take
: 3067 3 4  my
: 3071 16 5  hand,
: 3099 5 2  don't
: 3105 7 5  say
: 3115 9 7  good
: 3124 32 9 bye
- 3158
: 3189 12 7 I
: 3203 8 -1  will
: 3212 21 0  love
: 3234 9 7  you
: 3244 22 7  more
: 3269 6 6  than
: 3276 10 6  that
- 3288
: 3301 6 4 I
: 3308 7 6  won't
: 3316 15 7  say
: 3332 6 -1  the
: 3339 21 0  words,
- 3361
: 3363 5 7 then
: 3372 20 7  take
: 3396 16 9  them
: 3413 16 11  back
- 3431
: 3433 10 7 Oh
: 3444 11 7  I
: 3457 7 -1  will
: 3466 23 0  love
: 3490 7 7  you
: 3498 24 7  more
: 3523 8 6  than
: 3532 12 6  that
- 3546
: 3555 8 4 I
: 3564 5 6  won't
: 3570 16 7  say
: 3587 7 -1  the
: 3596 22 0  words,
- 3619
: 3619 7 7 then
: 3627 23 7  take
: 3651 8 9  them
: 3660 22 9  back
- 3684
: 3698 14 7 Don't
: 3714 7 -1  give
: 3723 23 0  lone
: 3746 10 7 li
: 3756 21 7 ness
: 3781 4 6  a
: 3787 21 6  chance
- 3810
: 3823 2 4 Ba
: 3825 4 6 by
: 3830 6 7  lis
: 3836 4 6 ten
: 3842 7 7  to
: 3850 6 6  me
: 3857 8 7  when
: 3866 4 6  I
: 3872 19 4  say
- 3893
: 3908 3 4 I
: 3913 7 6  will
: 3921 7 7  love
: 3929 7 6  you
: 3937 7 4  more
- 3945
: 3946 10 7 Oh
: 3957 11 7  I
: 3970 7 -1  will
: 3979 23 0  love
: 4003 7 7  you
: 4011 24 7  more
: 4036 8 6  than
: 4045 12 6  that
- 4059
: 4069 8 4 I
: 4078 5 6  won't
: 4084 16 7  say
: 4101 7 -1  the
: 4110 22 0  words,
- 4133
: 4133 7 7 then
: 4141 23 7  take
: 4165 8 9  them
: 4174 22 9  back
- 4198
: 4213 14 7 Don't
: 4229 7 -1  give
: 4238 23 0  lone
: 4261 10 7 li
: 4271 21 7 ness
: 4296 4 6  a
: 4302 21 6  chance
- 4325
: 4335 2 4 Ba
: 4337 4 6 by
: 4342 6 7  lis
: 4348 4 6 ten
: 4354 7 7  to
: 4362 6 6  me
: 4369 8 7  when
: 4378 4 6  I
: 4384 19 4  say
- 4405
: 4419 6 4 I
: 4426 6 6  will
: 4433 8 7  love
: 4442 5 6  you
: 4448 10 4  more
: 4459 6 2  than
: 4466 29 4  that
E