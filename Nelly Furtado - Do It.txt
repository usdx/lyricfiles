#TITLE:Do It
#ARTIST:Nelly Furtado
#LANGUAGE:Englisch
#GENRE:Pop
#MP3:Nelly Furtado - Do It.mp3
#COVER:Nelly+Furtado+-+Do+It.jpg
#VIDEO:Nelly Furtado - Do it.avi
#BPM:125
#GAP:16500
: 0 4 8 You're
: 4 4 8  standing
: 8 3 8  at
: 11 2 8  the
: 13 5 8  door
- 18
: 19 2 8 I'm
: 21 1 7 ~
: 22 3 8  falling
: 25 2 8  to
: 27 2 8  the
: 29 3 8  floor
- 32
: 33 3 9 You
: 36 2 9  look
: 38 4 9  even
: 42 3 9  better
: 45 5 9  than
: 50 2 8 ~
: 53 2 9  you
: 55 2 9  did
: 60 3 9  before
: 63 3 8 ~
- 66
: 67 2 9 I'm
: 69 3 9  staring
: 72 1 8 ~
: 73 2 9  at
: 75 2 9  my
: 77 4 9  feet
- 81
: 81 4 9 Wondering
: 85 4 8  if
: 89 2 8  I
: 91 2 8  can
: 93 2 8  do
: 95 2 8  this
- 97
: 99 2 9 It's
: 101 4 9  been
: 105 2 9  a
: 107 4 9  while
: 111 2 9  but
: 113 7 9  I couldn't
: 122 3 9  forget
: 126 2 9  you
: 128 2 8 ~
- 130
* 132 2 10 Just
* 134 2 10  a
* 136 2 10  little
* 138 2 10  look
* 140 2 10  has
* 142 2 10  got
* 144 2 10  me
* 146 2 10  feeling
* 148 1 10  things
- 149
* 149 2 10 Just
* 151 2 10  a
* 153 2 10  little
* 155 2 10  touch
* 157 2 10  has
* 159 2 10  got
* 161 2 10  me
* 163 1 10  seeing
* 164 1 10  things
- 165
* 165 2 10 Just
* 167 1 10  a
* 168 2 10  little
* 170 2 10  taste
* 172 2 10  has
* 174 2 10  got
* 176 1 10  me
* 177 2 10  off
* 179 1 10  the
* 180 1 10  chains
- 181
* 181 2 10 Doing
* 183 2 10  things
* 185 2 10  that
* 187 1 10  I
* 188 2 10  don't
* 190 2 10  want
* 192 2 10  to
- 195
: 197 2 8 Do
: 199 2 9  it
: 202 2 8  like
: 204 2 8  you
: 206 2 8  do
: 208 2 9  it
: 211 2 7  to
: 213 2 7  me
- 215
: 217 2 7 I'm
: 219 1 7  burning
: 220 1 8 ~
* 221 3 8  up
* 224 4 7 ~
- 228
: 229 2 8 Do
: 231 2 9  it
: 234 2 8  like
: 236 2 8  you
: 238 2 8  do
: 240 2 9  it
: 243 2 7  to
: 245 2 7  me
- 247
: 248 1 7 it's
: 249 1 8  not
: 250 2 8  enough
: 252 6 7 ~
- 258
: 260 2 8 Do
: 262 2 9  it
: 265 2 8  like
* 267 2 8  you
: 269 2 8  do
: 271 2 9  it
: 274 2 7  to
: 276 2 7  me
: 279 1 7  just
: 280 1 8  open
: 281 2 8  up
: 283 6 7 ~
- 289
: 289 3 6 Don't
: 292 3 6  you
: 295 3 6  know
* 298 6 6  how
: 304 6 6  much
: 310 3 6  I
: 313 6 7  want
: 319 4 8  you
- 323
: 324 4 8 We're
: 328 4 8  sitting
: 332 3 8  real
: 335 2 8  close
* 337 5 8  and
- 342
: 343 2 8 I
: 345 1 7  can
: 346 3 8  feel
: 349 2 8  your
: 351 2 8  breath
- 353
: 354 2 8 I
: 356 2 8  wanna
* 358 2 8  touch
: 360 2 8  your
: 362 2 8  hand
: 364 5 8  but
: 369 2 8  I
: 371 2 8  lay
: 373 4 8  back
: 377 4 7 ~
- 385
: 387 2 7 Cuz
: 390 2 7  you
: 393 2 7  know
: 396 1 7  this
: 397 1 8 ~
- 398
: 399 2 7 thing
: 402 2 7  could
: 405 2 7  sprial
: 408 2 7  in
: 411 2 7  the
: 414 2 7  night
- 416
: 417 2 7 I've
: 420 2 7  changed
: 422 1 6 ~
: 423 3 7 ~
: 427 2 7  my
- 429
: 430 2 7 mind
: 433 2 7  I'm
: 436 2 7  ready
: 439 2 7  for
: 442 2 7  you
: 445 2 7  this
: 448 2 7  time
- 450
* 451 2 10 Just
* 453 2 10  a
* 455 2 10  little
* 457 2 10  look
* 459 2 10  has
* 461 2 10  got
* 463 2 10  me
* 465 2 10  feeling
* 467 1 10  things
- 468
* 469 2 10 Just
* 471 2 10  a
* 473 2 10  little
* 475 2 10  touch
* 477 2 10  has
* 479 2 10  got
* 481 1 10  me
* 482 1 10  seeing
* 483 2 10  things
- 485
* 485 2 10 Just
* 487 2 10  a
* 489 2 10  little
* 491 2 10  taste
* 493 1 10  has
* 494 2 10  got
* 496 1 10  me
* 497 1 10  off
* 498 2 10  the
* 500 2 10  chains
- 502
* 502 1 10 Doing
* 503 2 10  things
* 505 2 10  that
* 507 1 10  I
* 508 4 10  don't want
* 512 2 10  to
- 514
: 516 2 8 Do
: 518 2 9  it
: 521 2 8  like
* 523 2 8  you
: 525 2 8  do
: 527 2 9  it
: 530 2 7  to
: 532 2 7  me
- 534
: 536 2 7 I'm
: 538 1 7  burning
: 539 1 8 ~
: 540 3 8  up
- 546
: 548 2 8 Do
: 550 2 9  it
: 553 2 8  like
: 555 2 8  you
: 557 2 8  do
: 559 2 9  it
: 562 2 7  to
: 564 2 7  me
- 566
: 568 1 8 it's
: 569 2 8  not
: 571 6 7  enough
- 579
: 581 2 9 Do
: 584 2 8  it
: 586 2 8  like
: 588 2 8  you
: 590 2 9  do
: 593 2 7  it
: 595 2 7  to
: 598 1 7  me
: 599 1 8  just
: 600 2 8  open
: 602 6 7  up
- 608
: 608 3 6 Don't
: 611 3 6  you
: 614 3 6  know
: 617 6 6  how
: 623 6 6  much
: 629 3 6  I
: 632 6 7  want
: 638 4 9  you
- 642
: 644 2 8 Do
: 646 2 9  it
: 649 2 8  like
: 651 2 8  you
: 653 2 8  do
: 655 2 9  it
: 658 2 7  to
: 660 2 7  me
- 662
: 664 2 7 I'm
: 666 1 7  burning
: 667 1 8 ~
: 668 3 8  up
- 674
: 676 2 8 Do
: 678 2 9  it
: 681 2 8  like
: 683 2 8  you
: 685 2 8  do
: 687 2 9  it
: 690 2 7  to
: 692 2 7  me
- 694
: 696 1 8 it's
: 697 2 8  not
: 699 6 7  enough
- 707
: 709 2 9 Do
: 712 2 8  it
: 714 2 8  like
: 716 2 8  you
: 718 2 9  do
: 721 2 7  it
: 723 2 7  to
: 726 1 7  me
: 727 1 8  just
: 728 2 8  open
: 730 6 7  up
- 736
: 736 3 6 Don't
: 739 3 6  you
: 742 3 6  know
: 745 6 6  how
: 751 6 6  much
: 757 3 6  I
: 760 6 7  want
: 766 4 8  you
- 772
: 774 3 10 We're
: 778 4 10  sitting
: 782 5 9 ~
: 788 3 10  real
: 792 5 10  close
: 799 2 10  I
: 802 3 10  can
: 806 3 10  feel
: 811 2 10  your
: 815 4 10  breath
- 820
: 822 2 11 I
: 825 6 11  wanna
: 832 7 11  touch
: 840 3 11  your
: 844 7 11  hand
: 852 4 11  but
: 857 3 11  I
: 861 2 11  lay
: 863 1 10 ~
: 864 2 11 ~
: 867 7 11  back
: 874 6 10 ~
- 888
: 901 3 10 We're
: 905 3 10  sitting
: 908 4 9 ~
: 916 5 10  real
: 922 3 10  close
: 926 5 10  I
: 933 2 10  can
: 936 3 10  feel
: 940 3 10  your
: 945 2 10  breath
- 949
: 951 2 11 I
: 954 6 11  wanna
: 961 7 11  touch
: 969 3 11  your
: 973 7 11  hand
- 980
: 981 4 11 I
: 986 3 11  cannot
: 990 2 11 ~
: 992 1 10  fight
: 993 3 11 ~
: 996 6 11  it
: 1002 4 10 ~
: 1006 10 11 ~
: 1016 4 10  off
: 1020 2 9 ~
: 1022 2 8 ~
- 1040
: 1156 2 8 Do
: 1158 2 9  it
: 1161 2 8  like
: 1163 2 8  you
: 1165 2 8  do
: 1167 2 9  it
: 1170 2 7  to
: 1172 2 7  me
- 1174
: 1176 2 7 I'm
: 1178 1 7  burning
: 1179 1 8  up
- 1184
: 1186 2 6 Do
: 1188 2 8  it
: 1190 2 9  like
: 1193 2 8  you
: 1195 2 8  do
: 1197 2 8  it
: 1199 2 9  to
: 1201 2 7  me
- 1204
: 1206 2 8 it's
: 1208 1 8  not
: 1209 2 8  enough
- 1218
: 1221 2 7 Do
: 1223 2 9  it
: 1225 2 8  like
: 1227 2 8  you
: 1229 2 8  do
: 1231 2 8  it
: 1233 2 8  to
: 1235 1 8  me
: 1237 1 8  just
: 1238 2 8  open
: 1240 4 8  up
: 1244 4 7 ~
- 1248
: 1249 3 6 Don't
: 1252 3 6  you
: 1255 6 6  know
: 1261 6 6  how
: 1267 3 6  much
: 1270 6 7  I
: 1276 4 9  want
: 1280 2 11  you
- 1283
: 1285 2 8 Do
: 1287 2 9  it
: 1290 2 8  like
: 1292 2 8  you
: 1294 2 8  do
: 1296 2 9  it
: 1299 2 7  to
: 1301 2 7  me
- 1303
: 1305 2 7 I'm
: 1307 1 7  burning
: 1308 1 8  up
- 1313
: 1315 2 8 Do
: 1317 2 9  it
: 1319 2 8  like
: 1322 2 8  you
: 1324 2 8  do
: 1326 2 8  it
: 1328 2 9  to
: 1330 2 7  me
- 1333
: 1335 2 8 it's
: 1337 1 8  not
: 1338 2 8  enough
- 1347
: 1350 2 8 Do
: 1352 2 9  it
: 1354 2 8  like
: 1356 2 8  you
: 1358 2 8  do
: 1360 2 8  it
: 1362 2 8  to
: 1364 1 8  me
: 1366 1 8  just
: 1367 2 8  open
: 1369 4 8  up
- 1376
: 1378 3 6 Don't
: 1381 3 6  you
: 1384 6 6  know
: 1390 6 6  how
: 1396 3 6  much
: 1399 6 7  I
: 1405 4 9  want
: 1409 2 11  you
- 1427
F 1450 2 6 Yeah,
F 1458 3 6  yeah
- 1477
F 1514 2 6 Yeah
F 1522 3 6  yeah
E
