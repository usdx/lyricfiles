#TITLE:Wie am ersten Tag
#ARTIST:Die Ärzte
#MP3:Die Ärzte - Wie am ersten Tag.mp3
#VIDEO:Die Ärzte - Wie am ersten Tag.avi
#COVER:Die Ärzte - Wie am ersten Tag [CO].jpg
#BPM:350
#GAP:950
#VIDEOGAP:-1,7
#ENCODING:UTF8
#LANGUAGE:German
#GENRE:Rock
#CREATOR:Eggman
: 1 10 64 Hey
: 11 6 62  du
: 17 7 60  bleib
: 24 17 57  steh'n!
- 43
: 52 5 60 Ich
: 57 12 62  weiß,
: 69 3 64  wo
: 72 8 62 hin
: 80 8 60  du
: 88 19 57  gehst!
- 109
: 131 4 64 Du
: 135 12 62  brauchst
: 147 8 60  nicht
: 155 7 57  so
: 162 5 57  zu
: 167 8 57  tun,
- 176
: 177 9 60  als
: 186 9 62  ob
: 195 5 64  du
: 200 8 62  nicht
: 208 7 60  ver
: 215 27 59 stehst.
- 244
: 299 6 57 Du
: 305 8 57  bist
: 313 7 57  auf
: 320 13 57  dem
: 333 6 59  Weg
: 339 5 60  zu
: 344 13 55  ihr,
- 359
: 362 9 64  sie
: 371 4 64  ge
: 375 9 65 hör
: 384 7 64 te
: 391 8 62  mal
: 399 10 64  zu
: 409 13 62  mir.
- 424
: 491 8 57 Ges
: 499 5 57 tern
: 504 8 57  hab'
: 512 9 57  ich
: 521 8 59  sie
: 529 7 60  er
: 536 14 55 kannt,
- 552
: 555 6 64 sie
: 561 8 64  ging
: 569 8 65  mit
: 577 7 64  dir
: 584 8 62  Hand
: 592 8 64  in
: 600 22 62  Hand.
- 624
: 643 8 64 Für
: 651 9 62  mich
: 660 6 60  hat
: 666 8 57  sie
: 674 10 57  heut'
- 684
: 684 8 57  kei
: 692 3 60 ne
: 695 10 62  Zeit
: 705 10 64  mehr,
: 715 7 62  es
: 722 8 60  ist
: 730 14 57  Schluss.
- 746
: 771 5 64 Gib
: 776 7 62  ihr
: 783 13 60  zum
: 796 8 57  Ab
: 804 7 57 schied
- 811
: 811 8 57  von
: 819 8 60  mir
: 827 9 62  ei
: 836 5 64 nen
: 841 8 62  letz
: 849 8 60 ten
: 857 23 57  Kuss.
- 882
: 936 5 57 Dass
: 941 7 57  ich
: 948 5 57  ge
: 953 8 57 weint
: 961 10 57  hab,
: 971 8 59  sag'
: 979 6 60  ihr
: 985 11 57  nicht,
- 998
: 1006 6 57  auch
: 1012 9 57  nicht,
: 1021 7 57  dass
: 1028 8 57  mein
: 1036 8 59  Herz
: 1044 5 60  zer
: 1049 13 57 bricht,
- 1064
: 1068 8 57 sag
: 1076 9 57  nicht,
: 1085 5 57  dass
: 1090 10 57  ich's
: 1100 8 59  nicht
: 1108 4 60  er
: 1112 16 59 trag',
- 1130
: 1132 8 59 sag
: 1140 5 59  ihr
: 1145 8 60  nur
: 1153 8 59  dass
: 1161 7 57  ich
: 1168 8 59  sie
: 1176 24 57  mag.
- 1202
: 1220 8 64 Ich
: 1228 7 62  mag
: 1235 9 60  sie
: 1244 9 57  noch
: 1253 4 57  ge
: 1257 7 57 nau
: 1264 9 60 so,
: 1273 9 62  wie
: 1282 7 64  am
: 1289 8 62  ers
: 1297 7 60 ten
: 1304 22 57  Tag.
- 1328
: 1348 8 64 Ich
: 1356 8 62  mag
: 1364 9 60  sie
: 1373 8 57  noch
: 1381 4 57  ge
: 1385 7 57 nau
: 1392 9 60 so,
: 1401 9 62  wie
: 1410 7 64  am
: 1417 7 62  ers
: 1424 9 60 ten
: 1433 21 57  Tag.
- 1456
: 1476 8 64 Ich
: 1484 8 62  mag
: 1492 8 60  sie
: 1500 9 57  noch
: 1509 4 57  ge
: 1513 7 57 nau
: 1520 9 60 so,
: 1529 9 62  wie
: 1538 7 64  am
: 1545 8 62  ers
: 1553 7 60 ten
: 1560 20 57  Tag.
- 1582
: 1596 10 64 Ich
: 1610 10 62  mag
: 1620 8 60  sie
: 1628 9 57  noch
: 1637 4 57  ge
: 1641 7 57 nau
: 1648 10 60 so,
: 1658 7 62  wie
: 1665 8 64  am
: 1673 8 62  ers
: 1681 8 60 ten
: 1689 20 57  Tag.
- 1711
: 1796 8 52 Vor
: 1804 8 50  ei
: 1812 8 48 nem
: 1820 10 45  Jahr
: 1830 8 45  war
: 1838 4 45  ich
: 1842 7 48  a
: 1849 12 50 llein,
: 1861 4 52  da
: 1865 9 50  traf
: 1874 8 48  ich
: 1882 18 45  sie.
- 1902
: 1926 7 52 Ich
: 1933 9 50  hab
: 1942 4 48  ge
: 1946 11 45 tanzt
: 1957 5 45  mit
: 1962 9 45  ihr
: 1971 10 48  und
: 1981 5 50  hat
: 1986 11 52 te
: 1997 7 50  wei
: 2004 8 48 che
: 2012 20 47  Knie.
- 2034
: 2089 5 45 Wir
: 2094 8 45  tanz
: 2102 5 45 ten
: 2107 8 45  bis
: 2115 11 45  spät
: 2126 6 47  in
: 2132 6 48  die
: 2138 11 43  Nacht,
- 2150
: 2151 5 52 dann
: 2156 6 52  hab
: 2162 7 52  ich
: 2169 9 53  sie
: 2178 7 52  nach
: 2185 9 50  Haus
: 2194 10 52  ge
: 2204 20 50 bracht.
- 2226
: 2292 9 45 Und
: 2301 7 45  dann
: 2308 7 45  vor
: 2315 7 47  ih
: 2322 8 48 rer
: 2330 12 43  Tür
- 2343
: 2344 5 52  be
: 2349 6 52 kam
: 2355 7 52  ich
: 2362 8 53  ei
: 2370 8 52 nen
: 2378 8 50  Kuss
: 2386 8 52  da
: 2394 24 50 für.
- 2420
: 2438 7 64 Wir
: 2445 8 62  war'n
: 2453 8 60  ver
: 2461 9 57 liebt,
- 2470
: 2470 7 57  doch
: 2477 6 57  a
: 2483 7 60 lles
: 2490 9 62  das
: 2499 8 64  ist
: 2507 10 62  jetzt
: 2517 9 60  vor
: 2526 14 57 bei,
- 2542
: 2566 7 64 denn
: 2573 6 62  sie
: 2579 11 60  liebt
: 2590 8 57  dich
: 2598 8 57  und
: 2606 5 57  da
: 2611 7 60 rum
: 2618 9 62  ge
: 2627 8 64 be
: 2635 6 62  ich
: 2641 8 60  sie
: 2649 23 57  frei.
- 2674
: 2729 6 57  Dass
: 2735 6 57  ich
: 2741 5 57  ge
: 2746 8 57 weint
: 2754 12 57  hab,
: 2766 7 59  sag
: 2773 6 60  ihr
: 2779 13 57  nicht,
- 2794
: 2800 6 57 auch
: 2806 9 57  nicht,
: 2815 6 57  dass
: 2821 8 57  mein
: 2829 9 59  Herz
: 2838 5 60  zer
: 2843 13 57 bricht,
- 2858
: 2861 8 57 sag
: 2869 10 57  nicht,
: 2879 6 57  dass
: 2885 8 57  ich's
: 2893 9 59  nicht
: 2902 5 60  er
: 2907 15 59 trag',
- 2923
: 2925 7 59 sag
: 2932 7 59  ihr
: 2939 8 60  nur,
: 2947 9 59  dass
: 2956 6 57  ich
: 2962 10 59  sie
: 2981 25 57  mag.
- 3008
: 3014 8 64 Ich
: 3022 8 62  mag
: 3030 9 60  sie
: 3039 7 57  noch
: 3046 4 57  ge
: 3050 8 57 nau
: 3058 9 60 so,
: 3067 8 62  wie
: 3075 8 64  am
: 3083 8 62  ers
: 3091 8 60 ten
: 3099 21 57  Tag.
- 3122
: 3142 7 64 Ich
: 3149 8 62  mag
: 3157 9 60  sie
: 3166 9 57  noch
: 3175 4 57  ge
: 3179 6 57 nau
: 3185 10 60 so,
: 3195 8 62  wie
: 3203 8 64  am
: 3211 8 62  ers
: 3219 7 60 ten
: 3226 20 57  Tag.
- 3248
: 3269 8 64 Ich
: 3277 8 62  mag
: 3285 8 60  sie
: 3293 10 57  noch
: 3303 4 57  ge
: 3307 7 57 nau
: 3314 10 60 so,
: 3324 8 62  wie
: 3332 7 64  am
: 3339 8 62  ers
: 3347 7 60 ten
: 3354 23 57  Tag.
- 3379
: 3400 7 64 Ich
: 3407 7 62  mag
: 3414 8 60  sie
: 3422 9 57  noch
: 3431 4 57  ge
: 3435 7 57 nau
: 3442 10 60 so,
: 3452 8 62  wie
: 3460 7 64  am
: 3467 8 62  ers
: 3475 7 60 ten
: 3482 26 57  Tag.
- 3510
: 3557 10 47 Ich
: 3567 8 47  geh'
: 3575 8 49  nach
: 3583 7 50  Hau
: 3590 5 49 se
- 3595
: 3595 8 47  und
: 3603 9 50  da
: 3612 9 52  schlie
: 3621 6 52 ße
: 3627 8 52  ich
: 3635 7 54  mich
: 3642 23 52  ein.
- 3667
: 3685 9 47 Ich
: 3694 9 47  weiß,
: 3703 7 49  nie
: 3710 6 50  wie
: 3716 6 49 der
: 3722 9 47  wer
: 3731 9 50 de
: 3740 11 52  ich
: 3751 7 52  der
: 3758 9 52 sel
: 3767 4 54 be
: 3771 16 52  sein.
- 3789
: 3822 6 55 Doch
: 3828 11 55  ich
: 3839 4 55  bit
: 3843 7 55 te
: 3850 11 57  dich,
- 3861
: 3861 8 55  wenn
: 3869 7 54  sie
: 3876 10 54  mal
: 3886 8 54  nach
: 3894 8 50  mir
: 3902 21 47  fragt,
- 3925
: 3950 8 52 sag
: 3958 4 54  ihr
: 3962 12 55  nur,
: 3975 8 52  dass
: 3983 7 54  ich
: 3990 9 55  sie
: 3999 28 55  mag.
- 4029
: 4039 8 64 Ich
: 4047 8 62  mag
: 4055 8 60  sie
: 4063 9 57  noch
: 4072 4 57  ge
: 4076 6 57 nau
: 4082 10 60 so,
: 4092 8 62  wie
: 4100 8 64  am
: 4108 8 62  ers
: 4116 8 60 ten
: 4124 20 57  Tag.
- 4146
: 4168 7 64 Ich
: 4175 8 62  mag
: 4183 9 60  sie
: 4192 8 57  noch
: 4200 4 57  ge
: 4204 7 57 nau
: 4211 9 60 so,
: 4220 9 62  wie
: 4229 7 64  am
: 4236 8 62  ers
: 4244 7 60 ten
: 4251 29 57  Tag.
- 4282
: 4333 10 52 Sag
: 4343 5 54  ihr
: 4348 12 55  nur,
: 4360 8 52  dass
: 4368 7 54  ich
: 4375 9 55  sie
: 4384 32 55  mag -
- 4418
: 4457 4 57 ge
: 4461 6 60 nau
: 4467 9 62 so,
: 4476 8 64  wie
: 4484 8 62  am
: 4492 8 60  ers
: 4500 7 57 ten
: 4507 25 57  Tag.
- 4534
: 4679 8 64 Ich
: 4687 8 62  mag
: 4695 8 60  sie
: 4703 9 57  noch
: 4712 4 57  ge
: 4716 7 57 nau
: 4723 9 60 so,
: 4732 9 62  wie
: 4741 7 64  am
: 4748 8 62  ers
: 4756 8 60 ten
: 4764 24 57  Tag.
- 4790
: 4936 8 64 Ich
: 4944 7 62  mag
: 4951 9 60  sie
: 4960 9 57  noch
: 4969 4 57  ge
: 4973 8 57 nau
: 4981 9 60 so,
: 4990 8 62  wie
: 4998 8 64  am
: 5006 7 62  ers
: 5013 8 60 ten
: 5021 42 69  Tag!
E