#TITLE:Bück dich
#ARTIST:Rammstein
#MP3:Rammstein - Bück dich.mp3
#VIDEO:Rammstein - Bück dich.avi
#COVER:Rammstein - Bück dich.jpg
#BPM:74,5
#GAP:6560
#ENCODING:UTF8
#LANGUAGE:Deutsch
#GENRE:Metal
#EDITION:UltraStar Rammstein
: 0 2 -10 Bück
: 2 2 -10  dich
: 6 2 -10  be
: 8 1 -10 fehl
: 9 2 -10  ich
: 11 3 -10  dir
- 15
: 16 1 -10 wen
: 17 2 -10 de
: 19 1 -10  dein
: 20 2 -10  Ant
: 22 2 -10 litz
: 24 2 -10  ab
: 26 1 -10  von
: 27 3 -10  mir
- 31
: 32 2 -10 dein
: 34 1 -10  Ge
: 35 1 -10 sicht
: 38 2 -10  ist
: 40 2 -10  mir
: 42 1 -10  e
: 43 3 -10 gal
- 47
: 48 2 -10 Bück
: 50 2 -10  dich
- 55
: 190 2 -7  Ein
: 192 2 -10  Zwei
: 194 2 -7 bei
: 196 2 -7 ner
: 198 2 -7  auf
: 200 1 -5  al
: 201 2 -7 len
: 203 3 -8  Vie
: 206 2 -7 ren
- 208
: 210 2 -7 Ich
: 212 2 -8  füh
: 214 2 -7 re
: 216 2 -5  ihn
: 218 1 -7  spa
: 219 2 -8 zie
: 221 1 -7 ren
: 222 2 -7  Im
- 224
: 224 2 -7 Pass
: 226 2 -7 gang
: 230 2 -7  den
: 232 1 -7  Flur
: 233 3 -7  ent
: 236 2 -7 lang
- 239
: 242 2 4 Ich
: 244 1 4  bin
: 245 2 4  ent
: 247 5 4 täuscht
- 254
: 256 2 -7 Jetzt
: 258 1 -7  Kommt
: 259 1 -7  er
: 260 2 -8  rück
: 262 2 -7 wärts
: 264 2 -5  mir
: 266 2 -7  ent
: 268 2 -8 ge
: 270 2 -7 gen
- 273
: 274 1 -7 Ho
: 275 1 -7 nig
: 276 2 -8  bleibt
: 278 2 -7  am
: 280 2 -5  Strumpf
: 282 2 -7 band
: 284 2 -8  kle
: 286 2 -7 ben
- 289
: 290 2 2 Ich
: 292 1 2  bin
: 293 2 2  ent
: 295 5 2 täuscht
- 304
: 306 2 4 to
: 308 1 4 tal
: 309 2 4  ent
: 311 5 4 täuscht
- 318
: 320 2 -10 Bück
: 322 2 -10  dich
- 328
: 336 2 -10 Bück
: 338 2 -10  dich
- 350
: 352 2 -10 Bück
: 354 2 -10  dich
- 366
: 368 2 -10 Bück
: 370 2 -10  dich
- 382
: 384 2 -10 Das
: 386 2 -10  Ge
: 388 2 -7 sicht
: 398 1 -7  in
: 399 1 -7 teres
- 400
: 400 2 -7 siert
: 404 2 -8  mich
: 408 4 -10  nicht
- 470
: 478 2 -7 Der
: 480 2 -10  Zwei
: 482 2 -7 bei
: 484 2 -7 ner
: 486 2 -7  hat
: 488 1 -5  sich
: 489 2 -7  ge
: 491 3 -7 bückt
- 495
: 498 1 -7 in
: 499 1 -7  ein
: 500 2 -8  gu
: 502 2 -7 tes
: 504 2 -5  Licht
: 506 1 -7  ge
: 507 3 -8 rückt
: 510 2 0  zeig
- 512
: 512 2 0 ich
: 514 2 0  ihm
: 516 2 0  was
: 518 2 0  man
: 520 1 0  ma
: 521 2 0 chen
: 523 3 0  kann
- 527
: 529 1 2 und
: 530 1 2  fang
: 531 1 2  da
: 532 2 2 bei
: 534 2 2  zu
: 536 2 5  wei
: 538 1 2 nen
: 539 3 2  an
: 542 2 2  Der
- 544
: 544 2 2 Zwei
: 546 2 2 fuß
: 548 1 2  stam
: 549 3 2 melt
: 552 2 2  ein
: 554 1 2  Ge
: 555 3 2 bet
: 559 1 2  aus
- 560
: 560 2 4 Angst
: 562 1 4  weil
: 563 2 4  es
: 565 3 7  mir
: 568 2 4  schlech
: 570 1 4 ter
: 571 3 4  geht
- 575
: 577 1 4 ver
: 578 1 4 sucht
: 579 1 4  sich
: 580 2 4  tie
: 582 2 4 fer
: 584 2 4  noch
: 586 2 4  zu
: 588 1 4  büc
: 589 3 4 ken
- 592
: 592 2 9 Trä
: 594 2 2 nen
: 596 2 2  lau
: 598 2 2 fen
: 600 2 4  hoch
: 602 2 2  den
: 604 1 2  Rüc
: 605 1 2 ken
- 607
: 608 2 -10 Bück
: 610 2 -10  dich
- 614
: 624 2 -10 Bück
: 626 2 -10  dich
- 630
: 640 2 -10 Bück
: 642 2 -10  dich
- 646
: 656 2 -10 Bück
: 658 2 -10  dich
- 780
: 800 2 -10 Bück
: 802 2 -10  dich
: 806 2 -10  be
: 808 1 -10 fehl
: 809 2 -10  ich
: 811 3 -10  dir
- 815
: 816 1 -10 Wen
: 817 1 -10 de
: 819 1 -10  dein
: 820 2 -10  Ant
: 822 2 -10 litz
: 824 2 -10  ab
: 826 1 -10  von
: 827 3 -10  mir
- 831
: 832 2 -10 Dein
: 834 2 -10  Ge
: 836 2 -10 sicht
: 838 2 -10  ist
: 840 1 -10  mir
: 841 2 -10  e
: 843 3 -10 gal
- 847
: 848 2 -10 Bück
: 850 2 -10  dich
: 856 1 -10  noch
: 857 2 -10  ein
: 859 3 -10 mal
- 863
: 864 2 -10  Bück
: 866 2 -10 dich
- 878
: 880 2 -10 Bück
: 882 2 -10  dich
- 894
: 896 2 -10 Bück
: 898 2 -10  dich
- 910
: 912 2 -10 Bück
: 914 2 -10  dich
- 926
: 928 2 -10 Bück
: 930 2 -10  dich
E