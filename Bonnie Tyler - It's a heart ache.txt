#TITLE:It's a heart ache
#ARTIST:Bonnie Tyler
#MP3:Bonnie Tyler - It's a heart ache.mp3
#COVER:Bonnie Tyler - It's a heart ache [CO].jpg
#BACKGROUND:Bonnie Tyler - It's a heart ache [BG].jpg
#BPM:118
#GAP:8000
: -1 3 76 It's
: 3 3 76  a
: 6 7 72  heart
: 13 5 72 ache
- 26
: 30 2 72 No
: 32 2 74 thing
: 34 3 76  but
: 37 1 77  a
: 38 7 79  heart
: 45 6 79 ache
- 58
: 62 3 81 Hits
: 65 2 79  you
: 67 2 77  when
: 69 2 76  it's
: 71 6 77  too
: 77 7 77  late
- 89
: 95 3 79 Hits
: 98 2 77  you
: 100 2 76  when
: 102 2 74  you're
: 104 16 76  down
- 124
: 128 4 76 It's
: 132 3 76  a
: 136 6 72  fool's
: 142 6 72  game
- 156
: 160 2 72 No
: 162 2 74 thing
: 164 2 76  but
: 166 2 77  a
: 168 7 79  fool's
: 175 8 79  game
- 185
: 189 5 81 Stan
: 194 3 79 ding
: 197 2 77  in
: 199 1 76  the
: 200 6 77  cold
: 206 8 77  rain
- 219
: 223 4 79 Feel
: 227 2 77 ing
: 229 2 76  like
: 231 2 74  a
: 233 16 76  clown
- 252
: 257 4 76 It's
: 261 3 76  a
: 264 7 72  heart
: 271 6 72 ache
- 282
: 288 3 72 No
: 291 2 74 thing
: 293 2 76  but
: 295 2 77  a
: 297 6 79  heart
: 303 7 79 ache
- 315
: 319 3 81 Lo
: 322 2 79 ving
: 324 2 77  till
: 326 2 76  your
: 328 6 77  arms
: 334 8 77  break
- 348
: 352 2 79 Then
: 354 2 77  he'll
: 356 3 76  let
: 359 2 74  you
: 361 17 76  down
- 382
: 387 2 79 It
: 389 3 79  ain't
: 392 5 81  right
: 398 2 81  with
: 400 4 81  love
: 404 2 79  to
: 406 8 79  share
- 416
: 419 2 79 When
: 421 2 79  you
: 423 6 79  find
: 429 3 79  he
: 432 4 79  does
: 436 3 77 n't
: 439 9 76  care
- 450
: 452 4 76  for
: 456 6 74  you
- 471
: 483 2 79 It
: 485 3 79  ain't
: 488 7 81  wise
: 495 2 81  to
: 497 3 81  need
: 500 3 79  some
: 503 8 79 one
- 513
: 515 2 79 As
: 517 2 79  much
: 519 2 79  as
: 521 5 79  I
: 526 2 79  de
: 528 5 77 pen
: 533 8 76 ding on
- 547
: 551 7 74  you
- 560
: 576 4 76 It's
: 580 3 76  a
: 583 7 72  heart
: 590 7 72 ache
- 602
: 608 2 72 No
: 610 2 74 thing
: 612 2 76  but
: 614 2 77  a
: 616 6 79  heart
: 622 6 79 ache
- 634
: 639 3 81 Hits
: 642 2 79  you
: 644 2 77  when
: 646 2 76  it's
: 648 6 77  too
: 654 8 77  late
- 668
: 672 2 79 Hits
: 674 2 77  you
: 676 2 76  when
: 678 2 74  you're
: 680 16 76  down
- 700
: 704 4 76 It's
: 708 3 76  a
: 711 7 72  fool's
: 718 7 72  game
- 729
: 735 2 72 No
: 737 3 74 thing
: 740 2 76  but
: 742 2 77  a
: 744 6 79  fool's
: 750 8 79  game
- 761
: 765 5 81 Stan
: 770 2 79 ding
: 772 2 77  in
: 774 2 76  the
: 776 6 77  cold
: 782 8 77  rain
- 794
: 798 4 79 Feel
: 802 2 77 ing
: 804 2 76  like
: 806 2 74  a
: 808 17 76  clown
- 899
: 963 2 79 It
: 965 3 79  ain't
: 968 6 81  right
: 974 2 81  with
: 976 4 81  love
: 980 2 79  to
: 982 7 79  share
- 991
: 994 3 79 When
: 997 2 79  you
: 999 7 79  find
: 1006 2 79  he
: 1008 4 79  does
: 1012 2 77 n't
: 1014 11 76  care
- 1026
: 1028 4 76  for
: 1032 5 74  you
- 1046
: 1059 2 79 It
: 1061 2 79  ain't
: 1063 8 81  wise
: 1071 1 81  to
: 1072 4 81  need
: 1076 3 79  some
: 1079 7 79 one
- 1088
: 1090 2 79 As
: 1092 3 79  much
: 1095 2 79  as
: 1097 5 79  I
: 1102 2 79  de
: 1104 4 79 pen
: 1108 3 77 ding
: 1111 9 76  on,
- 1122
: 1126 9 74  you
- 1140
: 1152 4 76 It's
: 1156 3 76  a
: 1159 7 72  heart
: 1166 6 72 ache
- 1179
: 1183 3 72 No
: 1186 2 74 thing
: 1188 2 76  but
: 1190 2 77  a
: 1192 7 79  heart
: 1199 6 79 ache
- 1209
: 1215 3 81 Love
: 1218 2 79 ing
: 1220 2 77  when
: 1222 2 76  you'r
: 1224 6 77  arms
: 1230 7 77  brake
- 1244
: 1246 3 76  when
: 1249 4 74  you're
: 1253 19 76  down
- 1277
: 1297 3 79 It's
: 1300 1 79  a
: 1301 5 81  fool's
: 1306 11 79  game
- 1320
: 1323 6 79 Stand
: 1329 2 79 ing
: 1331 3 79  in
: 1334 1 79  a
: 1335 4 81  cold
: 1339 12 79  rain
- 1352
: 1372 4 79 Feel
: 1376 2 81 ing
: 1378 3 79  like
: 1381 20 79  a clown
- 1403
: 1407 4 79 It's
: 1411 2 79  a
: 1413 18 81  heart
: 1431 17 79 ache
E
