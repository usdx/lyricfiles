#TITLE:J'aime les filles
#ARTIST:Jacques Dutronc
#MP3:Jacques Dutronc-09-J'aime les filles.mp3
#VIDEO:J_aime les filles - Jacques Dutronc.mp4
#COVER:jaime-les-filles-maxi.jpg
#BPM:232
#GAP:15739
#VIDEOGAP:8
#ENCODING:UTF8
: 0 2 16 J'ai
: 4 2 16 me
: 8 2 15  les
: 14 6 14  filles
: 28 2 5  de
: 32 4 6  chez
: 40 4 9  Cas
: 46 10 8 tel -
- 62
: 64 2 16 J'ai
: 68 1 15 me
: 70 2 14  les
: 76 6 13  filles
: 92 1 3  de
: 94 4 4  chez
: 100 4 8  Ré
: 106 14 6 gine -
- 120
: 122 4 16 J'ai
: 128 1 16 me
: 130 2 15  les
: 136 12 14  filles
: 152 2 5  qu'on
: 156 2 6  voit
: 160 4 9  dans
: 166 10 8  "Elle" -
- 180
: 182 2 16 J'ai
: 186 1 13 me
: 188 2 14  les
: 194 12 16  filles
: 210 1 14  des
: 212 4 13  ma
: 218 6 14 ga
: 226 14 16 zines -
- 242
: 244 4 16 J'ai
: 250 1 16 me
: 252 2 15  les
: 258 12 14  filles
: 274 2 5  de
: 278 2 6  chez
: 284 4 9  Re
: 290 14 8 nault -
- 304
: 306 4 16 J'ai
: 312 1 15 me
: 314 2 14  les
: 320 12 13  filles
: 336 1 4  de
: 338 2 4  chez
: 344 2 4  Ci
: 346 4 8 tro
: 352 12 6 ën -
- 364
: 366 4 16 J'ai
: 372 2 16 me
: 376 2 15  les
: 380 12 14  filles
: 396 2 6  des
: 400 2 6  hauts
: 404 8 9  four
: 414 12 8 neaux -
- 426
: 428 4 16 J'ai
: 434 1 13 me
: 436 2 14  les
: 442 12 16  filles
: 458 1 16  qui
: 460 4 16  tra
: 466 2 14 vaillent
: 472 1 13  a
: 474 1 14  la
: 476 12 16  chaîne -
- 498
: 518 2 14 Si
: 522 2 14  vous
: 526 2 13  etes
: 530 2 14  comme
: 536 8 16  ca,
: 550 2 14  té
: 554 2 14 lé
: 558 2 13 pho
: 562 2 14 nez
: 566 10 13  moi -
- 591
: 610 1 16 Si
: 612 2 16  vous
: 618 2 16  etes
: 622 2 15  comme
: 628 18 14  ci,
: 674 2 12  té
: 678 2 13 lé
: 682 2 12 pho
: 685 2 13 nez
: 690 8 9  me -
- 713
: 732 6 16 J'ai
: 738 2 16 me
: 742 2 15  les
: 746 16 14  filles
F 768 2 6  qui
: 772 4 9  ra
: 778 6 8 dotent -
- 790
: 792 2 16 J'ai
: 796 1 15 me
: 798 6 14  les
: 806 16 13  filles
: 824 6 4  a
: 832 4 8  pa
: 838 16 6 pa -
- 854
: 856 2 16 J'ai
: 860 1 16 me
: 862 4 15  les
: 870 14 14  filles
F 888 4 6  ri
: 894 8 9 go
: 904 12 8 lotes -
- 916
: 918 2 16 J'ai
: 920 2 13 me
: 924 2 14  les
: 930 14 16  filles
: 948 2 13  sans
: 954 6 14  pa
: 962 12 16 pa -
- 974
: 976 4 16 J'ai
: 980 2 16 me
: 984 2 15  les
: 990 14 14  filles
: 1008 4 6  de
: 1016 4 9  Mé
: 1022 16 8 geve -
- 1040
: 1042 2 16 J'ai
: 1044 2 13 me
: 1048 2 14  les
: 1054 10 16  filles
: 1066 2 14  de
: 1072 4 13  St
: 1078 4 11 Tro
: 1084 16 9 pez -
- 1100
: 1102 2 16 J'ai
: 1106 1 16 me
: 1108 2 15  les
: 1114 12 14  filles
: 1130 1 6  qui
: 1132 2 6  font
: 1138 6 9  la
: 1148 12 8  greve -
- 1162
: 1164 4 16 J'ai
: 1170 1 13 me
: 1172 2 14  les
: 1178 10 16  filles
: 1192 2 14  qui
: 1196 2 13  vont
: 1202 4 14  cam
: 1208 18 16 per-
- 1256
: 1464 2 16 J'ai
: 1468 2 16 me
: 1472 2 15  les
: 1478 12 14  filles
: 1492 2 5  de
: 1496 4 6  la
: 1504 4 9  Ro
: 1510 10 8 chelle -
- 1522
: 1524 4 16 J'ai
: 1530 1 13 me
: 1532 2 14  les
: 1538 12 16  filles
: 1554 1 14  de
: 1556 4 13  Ca
: 1562 6 11 ma
: 1570 14 9 ret -
- 1586
: 1588 4 16 J'ai
: 1594 1 16 me
: 1596 2 15  les
: 1602 12 14  filles
: 1618 2 5  in
: 1621 4 6 tel
: 1626 8 9 lec
: 1636 14 8 tuelles -
- 1650
: 1652 2 16 J'ai
: 1656 1 13 me
: 1658 2 14  les
: 1664 10 16  filles
: 1678 2 14  qui
: 1682 2 13  m'font
: 1688 4 14  mar
: 1694 12 16 rer -
- 1706
: 1708 4 16 J'ai
: 1714 1 16 me
: 1716 2 15  les
: 1722 12 14  filles
: 1738 1 5  qui
: 1740 4 6  font
: 1748 2 9  vieille
: 1754 14 8  France -
- 1770
: 1772 4 16 J'ai
: 1778 1 13 me
: 1780 2 14  les
: 1786 10 16  filles
: 1800 2 14  des
: 1804 4 13  ci
: 1810 6 11 né
: 1818 12 9 mas -
- 1830
: 1832 4 16 J'ai
: 1838 1 16 me
: 1840 2 15  les
: 1846 12 14  filles
: 1862 1 6  de
: 1864 4 6  l'as
: 1870 8 9 sis
: 1880 12 8 tance -
- 1894
: 1896 4 16 J'ai
: 1902 1 13 me
: 1904 2 14  les
: 1910 10 16  filles
: 1924 2 14  dans
: 1928 4 13  l'em
: 1934 4 14 bar
: 1940 18 16 ras -
- 2000
: 2196 2 16 Si
: 2200 4 16  vous
: 2206 2 16  etes
: 2210 2 15  comme
: 2214 8 14  ca,
: 2262 2 16  té
: 2266 3 16 lé
: 2270 1 13 pho
: 2272 2 14 nez
: 2276 10 16  moi -
- 2301
: 2320 1 16 Si
: 2322 4 16  vous
: 2328 2 16  etes
: 2332 2 15  comme
: 2336 6 14  ci,
: 2384 1 13  té
: 2386 3 13 lé
: 2390 3 12 pho
: 2394 2 13 nez
: 2398 4 9  me
E