#TITLE:Baba O´Riley
#ARTIST:The Who
#MP3:The Who - Baba O´Riley.mp3
#COVER:The Who - Baba O´Riley [CO].jpg
#BACKGROUND:The Who - Baba O´Riley [BG].jpg
#BPM:120
#GAP:75100
: 0 2 81 Out
: 2 4 81  here
: 6 2 79  in
: 8 2 77  the
: 10 7 77  fields
- 21
: 33 2 81 I
: 35 3 84  fight
: 39 2 86  for
: 41 2 81  my
: 43 2 79  me
: 45 6 77 als
- 55
: 68 2 81 I
: 70 2 81  get
: 72 4 81  my
: 76 4 84  back
: 80 4 81  in
: 84 7 79 to
: 91 2 77  my
: 93 4 79  liv
: 97 6 81 ing
- 107
: 134 2 81 I
: 136 3 81  don't
: 139 2 79  need
: 141 2 77  to
: 143 5 77  fight
- 152
: 167 2 81 To
: 169 5 84  pro
: 174 2 77 ve
: 176 2 77  I'm
: 178 7 77  right
- 189
: 203 2 81 I
: 205 4 81  don't
: 209 2 81  ne
: 211 3 79 ~
: 214 3 77 ed
- 217
: 217 2 74 to
: 219 4 77  be
: 223 3 77  for
: 226 4 79 gi
: 230 5 81 ven
- 235
: 235 3 81 Yeah,
: 238 3 81  Yeah,
: 241 3 81  Yeah,
: 244 3 79  Ye
: 247 3 77 ~ah,
: 250 4 79  Yeah,
: 254 5 77  Yeah
- 263
: 492 9 76 Don't
: 501 16 77  cry
- 519
: 521 5 72 Don't
: 526 6 76  raise
: 532 3 77  your
: 535 13 79  eye
- 552
: 557 2 72 It's
: 559 8 77  on
: 567 8 81 ly
: 575 8 82  teen
: 583 9 81 age
: 592 4 79  waste
: 596 17 76 land
- 617
: 628 3 77  Sal
: 631 2 72 ly,
: 633 4 77  take
: 637 2 72  my
: 639 5 77  hand
- 648
: 658 2 72 We'll
: 660 3 81  tra
: 663 2 84 vel
: 665 4 81  south
: 669 3 79  cross
: 672 6 77 land
- 682
: 689 2 77 Put
: 691 4 77  out
: 695 2 72  the
: 697 2 77  fi
: 699 2 81 re
- 702
: 703 2 70 And
: 705 4 77  don't
: 709 2 77  look
: 711 6 77  past
: 717 2 65  my
: 719 5 67  shoul
: 724 4 69 der
- 732
: 756 2 72 The
: 758 3 81  ex
: 761 1 84 o
: 762 3 81 dus
: 765 3 79  is
: 768 6 77  here
- 778
: 789 2 65 The
: 791 2 81  hap
: 793 2 84 py
: 795 4 81  ones
: 799 2 79  are
: 801 2 77  ne
: 803 3 79 ar
- 810
: 819 2 77 Let's
: 821 2 77  get
: 825 2 72  to
: 827 2 77 geth
: 829 2 81 er
- 832
: 833 2 72 Be
: 835 3 77 fore
: 838 2 74  we
: 840 4 77  get
: 844 4 65  much
: 848 4 67  ol
: 852 5 69 der
- 861
: 940 4 74 Teen
: 944 4 77  age
: 948 4 79  waste
: 952 8 81 land
- 964
: 967 2 79 It's
: 969 2 77  on
: 971 1 79 ly
: 972 5 77  teen
: 977 3 77 age
: 980 4 81  waste
: 984 7 72 land
- 995
: 1005 4 77 Teen
: 1009 4 77 age
: 1013 4 79  waste
: 1017 6 81 land
- 1023
: 1023 4 77 Oh,
: 1027 6 79  yeah
- 1035
: 1037 4 77 Teen
: 1041 4 77 age
: 1045 4 81  waste
: 1049 6 72 land
- 1059
: 1072 2 84 They're
: 1074 3 84  all
: 1077 4 84  wast
: 1081 4 76 ed!
E
